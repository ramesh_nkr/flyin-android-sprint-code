/***********************************************************************************
 * The MIT License (MIT)
 * <p/>
 * Copyright (c) 2014 Robin Chutaux
 * <p/>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p/>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p/>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 ***********************************************************************************/
package com.calendarlistview.library;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.flyin.bookings.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

@SuppressWarnings("ALL")
public class DayPickerView extends RecyclerView {
    protected Context mContext;
    protected com.calendarlistview.library.SimpleMonthAdapter mAdapter;
    private DatePickerController mController;
    protected int mCurrentScrollState = 0;
    protected long mPreviousScrollPosition;
    protected int mPreviousScrollState = 0;
    private TypedArray typedArray;
    private OnScrollListener onScrollListener;

    public DayPickerView(Context context) {
        this(context, null);
    }

    public DayPickerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DayPickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            typedArray = context.obtainStyledAttributes(attrs, R.styleable.DayPickerView);
            setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            init(context);
        }
    }

    public void setController(DatePickerController mController, String selectedDate, int tripType,
                              String roundTripSelectedDepartureDate, String roundTripSelectedReturnDate,
                              boolean isDepartureSelected, String disabledDates,
                              boolean isArabicSelected, boolean isHotelDateSelected) {
        this.mController = mController;
        setUpAdapter(selectedDate, tripType, roundTripSelectedDepartureDate, roundTripSelectedReturnDate,
                isDepartureSelected, disabledDates, isArabicSelected, isHotelDateSelected);
        setAdapter(mAdapter);
    }

    public void init(Context paramContext) {
        setLayoutManager(new LinearLayoutManager(paramContext));
        mContext = paramContext;
        setUpListView();
        onScrollListener = new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final SimpleMonthView child = (SimpleMonthView) recyclerView.getChildAt(0);
                if (child == null) {
                    return;
                }
                mPreviousScrollPosition = dy;
                mPreviousScrollState = mCurrentScrollState;
            }
        };
    }

    protected void setUpAdapter(String selectedDate, int tripType, String roundTripSelectedDepartureDate,
                                String roundTripSelectedReturnDate, boolean isDepartureSelected, String disabledDates, boolean isArabicSelected, boolean isHotelDateSelected) {
        if (mAdapter == null) {
            mAdapter = new SimpleMonthAdapter(getContext(), mController, typedArray, selectedDate, tripType,
                    roundTripSelectedDepartureDate, roundTripSelectedReturnDate, disabledDates, isArabicSelected, isDepartureSelected, isHotelDateSelected);
        }
        mAdapter.notifyDataSetChanged();
        try {
            if (tripType == 1) {
                if (!selectedDate.isEmpty() || !selectedDate.equalsIgnoreCase("")) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    Date endDate = null;
                    try {
                        endDate = format.parse(selectedDate);
                        System.out.println(endDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar startCalendar = new GregorianCalendar();
                    startCalendar.setTime(new Date());
                    Calendar endCalendar = new GregorianCalendar();
                    endCalendar.setTime(endDate);
                    int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
                    int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
                    smoothScrollToPosition(diffMonth);
                }
                if (!disabledDates.isEmpty() || !disabledDates.equalsIgnoreCase("")) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    Date disableDate = null;
                    try {
                        disableDate = format.parse(disabledDates);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar startCalendar = new GregorianCalendar();
                    startCalendar.setTime(new Date());
                    Calendar endCalendar = new GregorianCalendar();
                    endCalendar.setTime(disableDate);
                    int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
                    int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
                    smoothScrollToPosition(diffMonth);
                }
            } else if (tripType == 2) {
                String selectedDateRoundTrip = "";
                if (isDepartureSelected) {
                    selectedDateRoundTrip = roundTripSelectedDepartureDate;
                } else {
                    selectedDateRoundTrip = roundTripSelectedReturnDate;
                }
                if (selectedDateRoundTrip.isEmpty()) {
                    return;
                }
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date endDate = null;
                try {
                    endDate = format.parse(selectedDateRoundTrip);
                    System.out.println(endDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar startCalendar = new GregorianCalendar();
                startCalendar.setTime(new Date());
                Calendar endCalendar = new GregorianCalendar();
                endCalendar.setTime(endDate);
                int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
                int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
                smoothScrollToPosition(diffMonth);
            }
        } catch (Exception e) {
        }
    }

    protected void setUpListView() {
        setVerticalScrollBarEnabled(false);
        setOnScrollListener(onScrollListener);
        setFadingEdgeLength(0);
    }

    public SimpleMonthAdapter.SelectedDays<SimpleMonthAdapter.CalendarDay> getSelectedDays() {
        return mAdapter.getSelectedDays();
    }

    protected DatePickerController getController() {
        return mController;
    }

    protected TypedArray getTypedArray() {
        return typedArray;
    }
}