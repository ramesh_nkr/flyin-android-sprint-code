package com.flyin.bookings;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.dialog.TravellerListDialog;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.model.TravellerDetailsBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class HotelBookingPassengerInfoActivity extends AppCompatActivity {
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private ArrayList<TravellerDetailsBean> travellerArrayList = null;
    private boolean isUserSignIn = false;
    private boolean isAirportAvailable = false;
    private int selectedUserPosition = -1;
    private TravellerDetailsBean travellersBean = null;
    private static final int SEARCH_MOBILE_CODE = 30;
    private TextView countryCodeNumberText = null;
    private ImageView countryImage = null;
    private TextView passengerTitle = null;
    private EditText passengerFirstName = null;
    private EditText passengerLastName = null;
    private EditText passengerMobile = null;
    private EditText passengerEmail = null;
    private TextView headerLabelText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_passenger_info);
        travellerArrayList = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isUserSignIn = bundle.getBoolean(Constants.IS_USER_LOGGED, false);
            selectedUserPosition = Integer.parseInt(bundle.getString(Constants.SELECTED_USER_POSITION, ""));
            //travellerArrayList = (ArrayList<TravellerDetailsBean>) getIntent().getSerializableExtra("traveller_list");
        }
        travellerArrayList.addAll(Singleton.getInstance().fetchTravellerArrayList);
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_MEDIUM;
        if (Utils.isArabicLangSelected(HotelBookingPassengerInfoActivity.this)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        Typeface titleFace = Typeface.createFromAsset(getAssets(), fontTitle);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontText);
        travellersBean = Singleton.getInstance().travellersInfoBean;
        TextView travellerAccount = (TextView) findViewById(R.id.traveller_account_text);
        TextView orLabelText = (TextView) findViewById(R.id.or_text_view);
        TextView titleHeader = (TextView) findViewById(R.id.passenger_title_header);
        passengerTitle = (TextView) findViewById(R.id.passenger_title);
        TextView firstNameHeader = (TextView) findViewById(R.id.passenger_first_name_header);
        TextView lastNameHeader = (TextView) findViewById(R.id.passenger_last_name_header);
        TextView mobileHeader = (TextView) findViewById(R.id.passenger_mobile_number_header);
        TextView emailHeader = (TextView) findViewById(R.id.passenger_email_address_header);
        countryCodeNumberText = (TextView) findViewById(R.id.country_code_number_text);
        passengerFirstName = (EditText) findViewById(R.id.passenger_first_name);
        passengerLastName = (EditText) findViewById(R.id.passenger_last_name);
        passengerMobile = (EditText) findViewById(R.id.passenger_mobile_number);
        passengerEmail = (EditText) findViewById(R.id.passenger_email_address);
        LinearLayout singInLayout = (LinearLayout) findViewById(R.id.sing_in_layout);
        LinearLayout travellerAccountLayout = (LinearLayout) findViewById(R.id.traveller_account_text_layout);
        LinearLayout titleLayout = (LinearLayout) findViewById(R.id.title_layout);
        LinearLayout passengerMobileLayout = (LinearLayout) findViewById(R.id.passenger_mobile_number_layout);
        LinearLayout countryCodeLayout = (LinearLayout) findViewById(R.id.country_code_layout);
        countryImage = (ImageView) findViewById(R.id.image);
        LinearLayout emailMobileLayout = (LinearLayout) findViewById(R.id.email_mobile_layout);
        Button doneButton = (Button) findViewById(R.id.done_button);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(titleFace);
        titleHeader.append(Utils.getMandatoryField());
        firstNameHeader.append(Utils.getMandatoryField());
        lastNameHeader.append(Utils.getMandatoryField());
        emailHeader.append(Utils.getMandatoryField());
        mobileHeader.append(Utils.getMandatoryField());
        if (Utils.isArabicLangSelected(HotelBookingPassengerInfoActivity.this)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                passengerTitle.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerFirstName.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerLastName.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerMobile.setTextDirection(View.TEXT_DIRECTION_LTR);
                passengerEmail.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerMobileLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                titleHeader.setGravity(Gravity.START);
                titleHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                firstNameHeader.setGravity(Gravity.START);
                firstNameHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                lastNameHeader.setGravity(Gravity.START);
                lastNameHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                emailHeader.setGravity(Gravity.START);
                emailHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                mobileHeader.setGravity(Gravity.START);
                mobileHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
            travellerAccount.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            orLabelText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            titleHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            firstNameHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerFirstName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            lastNameHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerLastName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            emailHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerEmail.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            mobileHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerMobile.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            doneButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
        }
        travellerAccount.setTypeface(textFace);
        orLabelText.setTypeface(titleFace);
        titleHeader.setTypeface(titleFace);
        passengerTitle.setTypeface(titleFace);
        firstNameHeader.setTypeface(titleFace);
        passengerFirstName.setTypeface(titleFace);
        lastNameHeader.setTypeface(titleFace);
        passengerLastName.setTypeface(titleFace);
        emailHeader.setTypeface(titleFace);
        passengerEmail.setTypeface(titleFace);
        mobileHeader.setTypeface(titleFace);
        passengerMobile.setTypeface(titleFace);
        doneButton.setTypeface(textFace);
        if (isUserSignIn) {
            singInLayout.setVisibility(View.VISIBLE);
        } else {
            singInLayout.setVisibility(View.GONE);
        }
        if (selectedUserPosition == 0) {
            emailMobileLayout.setVisibility(View.VISIBLE);
        } else {
            emailMobileLayout.setVisibility(View.GONE);
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        travellerAccountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog travellerDialog = new TravellerListDialog(HotelBookingPassengerInfoActivity.this, Constants.PASSENGER_LIST, travellerArrayList, "", null, isAirportAvailable);
                travellerDialog.setCancelable(true);
                travellerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (travellerDialog != null) {
                            if (!travellerDialog.selectedListItem.equalsIgnoreCase("")) {
                                setAccountPassengerData(travellerDialog.selectedItemPosition);
                            }
                        }
                    }
                });
                travellerDialog.show();
            }
        });
        passengerTitle.setText(travellersBean.getTitle());
        titleLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog titleDialog = new TravellerListDialog(HotelBookingPassengerInfoActivity.this, Constants.TITLE_SELECTION, null, "", null, false);
                titleDialog.setCancelable(true);
                titleDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (titleDialog != null) {
                            if (!titleDialog.selectedListItem.equalsIgnoreCase("")) {
                                passengerTitle.setText(titleDialog.selectedListItem);
                            }
                        }
                    }
                });
                titleDialog.show();
            }
        });
        passengerFirstName.setText(travellersBean.getFirstName());
        passengerFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                travellersBean.setFirstName(passengerFirstName.getText().toString());
            }
        });
        passengerLastName.setText(travellersBean.getLastName());
        passengerLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                travellersBean.setLastName(passengerLastName.getText().toString());
            }
        });
        passengerEmail.setText(travellersBean.getEmail());
        passengerEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                travellersBean.setEmail(passengerEmail.getText().toString());
            }
        });
        if (travellersBean.getmResId() == 0) {
            countryImage.setImageResource(R.drawable.ksa);
            travellersBean.setPhoneCode("+966");
        } else {
            countryImage.setImageResource(travellersBean.getmResId());
        }
        countryCodeNumberText.setText(travellersBean.getPhoneCode());
        countryCodeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(HotelBookingPassengerInfoActivity.this, SearchCountryActivity.class);
                intent1.putExtra(Constants.IS_MOBILE_NUMBER_CLICKED, true);
                String selectedPhoneCode = travellersBean.getPhoneCode();
                if (!selectedPhoneCode.isEmpty()) {
                    intent1.putExtra(Constants.SELECTED_VALUE, selectedPhoneCode);
                }
                startActivityForResult(intent1, SEARCH_MOBILE_CODE);
            }
        });
        passengerMobile.setText(travellersBean.getPhoneNo());
        passengerMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                travellersBean.setPhoneNo(passengerMobile.getText().toString());
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.flight_hotel_result).setVisibility(View.VISIBLE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_view_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_label);
        RelativeLayout passengerInfoLayout = (RelativeLayout) mCustomView.findViewById(R.id.passenger_info_layout);
        headerLabelText = (TextView) mCustomView.findViewById(R.id.city_names_text);
        passengerInfoLayout.setVisibility(View.GONE);
        backText.setTypeface(titleFace);
        headerLabelText.setTypeface(titleFace);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (Utils.isArabicLangSelected(HotelBookingPassengerInfoActivity.this)) {
            backText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            headerLabelText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.about_header_text_size));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
            headerLabelText.setGravity(Gravity.END);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                headerLabelText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            }
        }
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        mActionBar.setCustomView(mCustomView, layout);
        mActionBar.setDisplayShowCustomEnabled(true);
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        if (travellersBean.getFirstName().isEmpty() && travellersBean.getLastName().isEmpty()) {
            headerLabelText.setText(travellersBean.getTravellerHeader());
        } else {
            headerLabelText.setText(travellersBean.getFirstName() + " " + travellersBean.getLastName());
        }
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travellersBean.getFirstName().isEmpty()) {
                    displayErrorMessage(getResources().getString(R.string.label_err_fname_msg));
                    return;
                }
                if (!Utils.isValidName(Utils.getNameWithoutChar(travellersBean.getFirstName()))) {
                    displayErrorMessage(travellersBean.getTravellerHeader() + " - " +
                            getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_firstName));
                    return;
                }
                if (travellersBean.getLastName().isEmpty()) {
                    displayErrorMessage(getResources().getString(R.string.label_err_lname_msg));
                    return;
                }
                if (!Utils.isValidName(Utils.getNameWithoutChar(travellersBean.getLastName()))) {
                    displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.ErrInvalidNameMsg)
                            + " " + getString(R.string.label_traveller_lastName));
                    return;
                }
                if (Utils.getNameWithoutChar(travellersBean.getFirstName()).equalsIgnoreCase(Utils.getNameWithoutChar(travellersBean.getLastName()))) {
                    displayErrorMessage(travellersBean.getTravellerHeader() + " - " +
                            getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_lastName));
                    return;
                }
                travellersBean.setTravellerHeaderName(travellersBean.getFirstName() + " " + travellersBean.getLastName());
                headerLabelText.setText(travellersBean.getTravellerHeaderName());
                if (selectedUserPosition == 0) {
                    if (travellersBean.getEmail().isEmpty()) {
                        displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_email_message));
                        return;
                    }
                    if (!Utils.isValidEmail(travellersBean.getEmail())) {
                        displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_email_valid_message));
                        return;
                    }
                    if (travellersBean.getPhoneNo().isEmpty() || !Utils.isValidMobile(travellersBean.getPhoneNo())) {
                        displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_err_phone_msg));
                        return;
                    }
                    travellersBean.setTravellerEmail(travellersBean.getEmail());
                } else {
                    travellersBean.setTravellerEmail("");
                }
                Intent it = getIntent();
                setResult(RESULT_OK, it);
                finish();
            }
        });

    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private void setAccountPassengerData(int selectedPosition) {
        String passengerTitle = "";
        String actualTitle = Utils.checkStringVal(travellerArrayList.get(selectedPosition).getTitle());
        if (travellersBean.getPassengerType().equalsIgnoreCase("ADT")) {
            if (actualTitle.equalsIgnoreCase(getString(R.string.child_title)) || actualTitle.equalsIgnoreCase("")) {
                passengerTitle = getString(R.string.adult_title);
            } else {
                passengerTitle = actualTitle;
            }
        } else if (travellersBean.getPassengerType().equalsIgnoreCase("CHD") || travellersBean.getPassengerType().equalsIgnoreCase("INF")) {
            if (actualTitle.equalsIgnoreCase(getString(R.string.adult_title)) || actualTitle.equalsIgnoreCase(getString(R.string.label_mrs)) || actualTitle.equalsIgnoreCase("")) {
                passengerTitle = getString(R.string.child_title);
            } else {
                passengerTitle = actualTitle;
            }
        }
        travellersBean.setTitle(passengerTitle);
        travellersBean.setFirstName(Utils.checkStringVal(travellerArrayList.get(selectedPosition).getFirstName()));
        travellersBean.setLastName(Utils.checkStringVal(travellerArrayList.get(selectedPosition).getLastName()));
        travellersBean.setEmail(Utils.checkStringVal(travellerArrayList.get(selectedPosition).getEmail().toLowerCase()));
        travellersBean.setPhoneNo(Utils.checkStringVal(travellerArrayList.get(selectedPosition).getPhoneNo()));
//        travellersBean.setPhoneCode(Utils.checkStringVal(travellerArrayList.get(selectedPosition).getPhoneCode()));
//        if(travellersBean.getPhoneCode() != null || !travellersBean.getPhoneCode().equalsIgnoreCase("")){
//            InputStream inputStream = null;
//            try {
//                inputStream = getApplicationContext().getAssets().open("country_codes.txt");
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            CSVFile csvFile = new CSVFile(inputStream);
//            String countryFlagName = csvFile.getCountryFlagName(travellerArrayList.get(selectedPosition).getPhoneCode());
//            Utils.printMessage(TAG, "countryFlagName "+countryFlagName);
//            int resId = getResources().getIdentifier(countryFlagName.toLowerCase(), "drawable", getPackageName());
//            travellersBean.setmResId(resId);
//        }
        updateUserDetails();
    }

    private void updateUserDetails() {
        passengerTitle.setText(travellersBean.getTitle());
        passengerFirstName.setText(travellersBean.getFirstName());
        passengerLastName.setText(travellersBean.getLastName());
        passengerEmail.setText(travellersBean.getEmail());
        passengerMobile.setText(travellersBean.getPhoneNo());
        travellersBean.setTravellerHeaderName(travellersBean.getFirstName() + " " + travellersBean.getLastName());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SEARCH_MOBILE_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String countryCode = data.getStringExtra(Constants.SELECTED_COUNTRY_CODE);
                    if (!countryCode.equalsIgnoreCase("")) {
                        int flagCode = Integer.parseInt(data.getStringExtra(Constants.SELECTED_COUNTRY_FLAG));
                        travellersBean.setPhoneCode(countryCode);
                        travellersBean.setmResId(flagCode);
                        countryImage.setImageResource(flagCode);
                        countryCodeNumberText.setText(countryCode);
                    }
                }
            }
        }
    }
}
