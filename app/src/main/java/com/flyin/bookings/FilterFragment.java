package com.flyin.bookings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@SuppressWarnings("ALL")
public class FilterFragment extends Fragment {
    private LinearLayout district_layout = null;
    private LinearLayout accommandation_layout = null;
    private LinearLayout deal_layout = null;
    private LinearLayout hotel_chain_layout = null;
    private LinearLayout star_layout = null;
    private RelativeLayout price_layout = null;
    private ArrayList<HotelModel> finallist;
    private HashMap<String, ArrayList<HotelModel>> districtList;
    private Set districtPosition;
    private HashMap<String, ArrayList<HotelModel>> accommandationList;
    private Set accommandationPosition;
    private HashMap<String, ArrayList<HotelModel>> hotelChainList;
    private Set hotelChainPosition;
    private HashMap<String, ArrayList<HotelModel>> starList;
    private Set starPosition;
    private static final String TAG = "FilterFragment";
    private ArrayList<HotelModel> dealList;
    private boolean selectDeal;
    private ArrayList<HotelModel> filterlist;
    private String nights, header_date, minPrice, maxPrice;
    private int childs, adults;
    private TextView minvalue = null;
    private TextView maxvalue = null;
    private RangeSeekBar seekBarInteger;
    private SharedPreferences.Editor editor;
    private SharedPreferences sharedpreferences;
    private int check_class;
    private TextView reset = null;
    private TextView apply = null;
    private TextView price = null;
    private TextView starRating = null;
    private TextView deals = null;
    private TextView accomandation = null;
    private TextView distric = null;
    private TextView hotelChain = null;
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private String selectedCurrency = "";
    private Typeface textFace = null;
    private boolean isArabicLang = false;
    private ArrayList<String> adultList;
    private ArrayList<String> childList;
    private HashMap<String, ArrayList<String>> childCount;
    private LinearLayout backLayout = null;
    private Activity mActivity;
    private boolean star_check = false, price_check = false, check_accending = true;
    private boolean iSstar_check = false, iSprice_check = false;
    private String arrowStaus = "";
    private String hotelCheckInDate = "";
    private String hotelCheckOutDate = "";

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        sharedpreferences = mActivity.getSharedPreferences(Constants.FILTERPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        districtList = new HashMap<>();
        districtPosition = new HashSet<>();
        accommandationList = new HashMap<>();
        accommandationPosition = new HashSet<>();
        hotelChainPosition = new HashSet<>();
        hotelChainList = new HashMap<>();
        starList = new HashMap<>();
        starPosition = new HashSet<>();
        dealList = new ArrayList<>();
        filterlist = new ArrayList<>();
        finallist = new ArrayList<>();
        check_class = getArguments().getInt("check_class");
        finallist = getArguments().getParcelableArrayList("list");
        nights = getArguments().getString("night_count");
        header_date = getArguments().getString("checkin");
        adults = getArguments().getInt("adult_count", 0);
        childs = getArguments().getInt("child_count", 0);
        adultList = getArguments().getStringArrayList("adults_list");
        childList = getArguments().getStringArrayList("childs_list");
        childCount = (HashMap<String, ArrayList<String>>) getArguments().getSerializable("children_list");
        price_check = getArguments().getBoolean("price_check", false);
        star_check = getArguments().getBoolean("star_check", false);
        iSprice_check = getArguments().getBoolean("iSprice_check", false);
        iSstar_check = getArguments().getBoolean("iSstar_check", false);
        check_accending = getArguments().getBoolean("check_accending", false);
        arrowStaus = getArguments().getString("arrowStaus");
        hotelCheckInDate = getArguments().getString("hotelCheckInDate", "");
        hotelCheckOutDate = getArguments().getString("hotelCheckOutDate", "");
        findViews(view);
        Gson gson = new Gson();
        String districtGson = sharedpreferences.getString("districtSet", null);
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        selectedCurrency = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        Utils.printMessage(TAG, "selectedCurrency : " + selectedCurrency);
        if (districtGson != null && districtGson.length() > 0) {
            districtPosition = gson.fromJson(districtGson, Set.class);
            Utils.printMessage(TAG, "districtPosition size" + districtPosition.size());
        }
        districLoop();
        String starGson = sharedpreferences.getString("starset", null);
        if (starGson != null && starGson.length() > 0) {
            starPosition = gson.fromJson(starGson, Set.class);
            Utils.printMessage(TAG, "starPosition size" + starPosition.size());
        }
        starLoop();
        String accomndGson = sharedpreferences.getString("accomSet", null);
        if (accomndGson != null && accomndGson.length() > 0) {
            accommandationPosition = gson.fromJson(accomndGson, Set.class);
            Utils.printMessage(TAG, "accommandatio size" + accommandationPosition.size());
        }
        accomandLoop();
        String hotelChainGson = sharedpreferences.getString("hotelChainset", null);
        if (hotelChainGson != null && hotelChainGson.length() > 0) {
            hotelChainPosition = gson.fromJson(hotelChainGson, Set.class);
            Utils.printMessage(TAG, "hotelChainPosition size" + hotelChainPosition.size());
        }
        chainLoop();
        selectDeal = sharedpreferences.getBoolean("dealList", false);
        Utils.printMessage(TAG, "deal value  :: " + selectDeal);
        dealLoop();
        backLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        reset.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                editor.clear();
                editor.commit();
                Fragment fragment;
                if (check_class == 1) {
                    fragment = new HotelListFragment();
                } else {
                    fragment = new MapFragment();
                }
                Bundle bundle = new Bundle();
                bundle.putString("night_count", nights);
                bundle.putInt("child_count", childs);
                bundle.putInt("adult_count", adults);
                bundle.putString("checkin", header_date);
                bundle.putParcelableArrayList("list", finallist);
                bundle.putParcelableArrayList("final_list", finallist);
                bundle.putString("country", finallist.get(0).getCountry());
                bundle.putSerializable("children_list", childCount);
                bundle.putStringArrayList("adults_list", new ArrayList<>(adultList));
                bundle.putStringArrayList("childs_list", new ArrayList<>(childList));
                bundle.putString("city", finallist.get(0).getCity() + ", " + finallist.get(0).getCountry());
                bundle.putString("arrowStaus", "");
                bundle.putBoolean("price_check", false);
                bundle.putBoolean("star_check", false);
                bundle.putBoolean("iSprice_check", false);
                bundle.putBoolean("iSstar_check", false);
                bundle.putBoolean("check_accending", true);
                bundle.putString("hotelCheckInDate", hotelCheckInDate);
                bundle.putString("hotelCheckOutDate", hotelCheckOutDate);
                fragment.setArguments(bundle);
                ((MainSearchActivity) mActivity).pushFragment(fragment);
            }
        });
        apply.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                String accomSet = gson.toJson(accommandationPosition);
                String districtSet = gson.toJson(districtPosition);
                String starset = gson.toJson(starPosition);
                String hotelChainset = gson.toJson(hotelChainPosition);
                editor.putBoolean("dealList", selectDeal);
                editor.putString("starset", starset);
                editor.putString("districtSet", districtSet);
                editor.putString("hotelChainset", hotelChainset);
                editor.putString("accomSet", accomSet);
                editor.putString("minPrice", minPrice);
                editor.putString("maxPrice", maxPrice);
                editor.commit();
                filterlist.clear();
                if (districtPosition != null && districtPosition.size() > 0) {
                    Iterator itr = districtPosition.iterator();
                    while (itr.hasNext()) {
                        String keys = (String) itr.next();
                        filterlist.addAll(districtList.get(keys));
                    }
                } else {
                    filterlist.addAll(finallist);
                }
                ArrayList<HotelModel> localList = new ArrayList<>();
                if (accommandationPosition != null && accommandationPosition.size() > 0) {
                    localList.clear();
                    for (HotelModel model : filterlist) {
                        if (model == null || model.getHotel_type() == null)
                            continue;
                        Iterator itr = accommandationPosition.iterator();
                        while (itr.hasNext()) {
                            String keys = (String) itr.next();
                            Utils.printMessage(TAG, "keys: " + keys + "type" + model.getHotel_type());
                            if (model.getHotel_type().equalsIgnoreCase(
                                    "" + keys)) {
                                localList.add(model);
                                break;
                            }
                        }
                    }
                    filterlist.clear();
                    filterlist.addAll(localList);
                }
                if (hotelChainPosition != null && hotelChainPosition.size() > 0) {
                    localList.clear();
                    for (HotelModel model : filterlist) {
                        if (model == null || model.getChainName() == null)
                            continue;
                        Iterator itr = hotelChainPosition.iterator();
                        while (itr.hasNext()) {
                            String keys = (String) itr.next();
                            if (model.getChainName().equalsIgnoreCase(
                                    "" + keys)) {
                                localList.add(model);
                                break;
                            }
                        }
                    }
                    filterlist.clear();
                    filterlist.addAll(localList);
                }
                Utils.printMessage(TAG, "starPosition size" + starPosition.size());
                if (starPosition != null && starPosition.size() > 0) {
                    localList.clear();
                    for (HotelModel model : filterlist) {
                        if (model == null)
                            continue;
                        Iterator itr = starPosition.iterator();
                        while (itr.hasNext()) {
                            String keys = (String) itr.next();
                            Utils.printMessage(TAG, "star count:" + model.getStar() + "keys:" + keys);
                            if (model.getStar().equalsIgnoreCase(keys)) {
                                localList.add(model);
                                Utils.printMessage(TAG, "size" + localList.size());
                                break;
                            }
                        }
                    }
                    filterlist.clear();
                    Utils.printMessage(TAG, "star list size" + localList.size());
                    filterlist.addAll(localList);
                }
                Utils.printMessage(TAG, "after star filterlist" + filterlist.size());
                if (selectDeal) {
                    localList.clear();
                    for (HotelModel model : filterlist) {
                        Iterator itr = dealList.iterator();
                        while (itr.hasNext()) {
                            HotelModel keys = (HotelModel) itr.next();
                            if (model.getHuid().equalsIgnoreCase(keys.getHuid())) {
                                localList.add(model);
                                break;
                            }
                        }
                    }
                    filterlist.clear();
                    filterlist.addAll(localList);
                }
                Utils.printMessage(TAG, "filterlist size" + filterlist.size());
                try {
                    if (minPrice != null && minPrice.length() > 0) {
                        for (Iterator<HotelModel> iterator = filterlist.iterator(); iterator.hasNext(); ) {
                            Utils.printMessage(TAG, "filterlist" + filterlist.size());
                            String value = iterator.next().getP();
                            if (value != null && Double.parseDouble(value) < Double.parseDouble(minPrice)
                                    || Double.parseDouble(value) > Double.parseDouble(maxPrice)) {
                                iterator.remove();
                            }
                        }
                    }
                } catch (Exception e) {
                    Utils.printMessage(TAG, "exception" + e.getMessage());
                }
                Utils.printMessage(TAG, "filterlist:" + filterlist.size() + "finallist:" + finallist.size());
                if (filterlist.size() > 0) {
                    Fragment fragment;
                    if (check_class == 1) {
                        fragment = new HotelListFragment();
                    } else {
                        fragment = new MapFragment();
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString("night_count", nights);
                    bundle.putString("checkin", header_date);
                    bundle.putInt("child_count", childs);
                    bundle.putInt("adult_count", adults);
                    bundle.putParcelableArrayList("list", filterlist);
                    bundle.putSerializable("children_list", childCount);
                    bundle.putParcelableArrayList("final_list", finallist);
                    bundle.putString("country", filterlist.get(0).getCountry());
                    bundle.putStringArrayList("adults_list", new ArrayList<>(adultList));
                    bundle.putStringArrayList("childs_list", new ArrayList<>(childList));
                    bundle.putString("city", filterlist.get(0).getCity() + ", " + filterlist.get(0).getCountry());
                    bundle.putString("search_fragment", "");
                    bundle.putBoolean("price_check", price_check);
                    bundle.putBoolean("star_check", star_check);
                    bundle.putBoolean("iSprice_check", iSprice_check);
                    bundle.putBoolean("iSstar_check", iSstar_check);
                    bundle.putBoolean("check_accending", check_accending);
                    bundle.putString("arrowStaus", arrowStaus);
                    bundle.putString("hotelCheckInDate", hotelCheckInDate);
                    bundle.putString("hotelCheckOutDate", hotelCheckOutDate);
                    fragment.setArguments(bundle);
                    ((MainSearchActivity) mActivity).pushFragment(fragment);
                } else {
                    displayErrorMessage(getString(R.string.label_no_hotel_found));
                }
            }
        });
        price.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                buttonDrawable();
                layoutVisibility();
                price.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.priceactive, 0, 0);
                price.setTextColor(mActivity.getResources().getColor(R.color.accent_color_dark));
                price_layout.setVisibility(View.VISIBLE);
                pricefilter();
            }
        });
        starRating.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                buttonDrawable();
                layoutVisibility();
                starRating.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.starratingactive, 0, 0);
                starRating.setTextColor(mActivity.getResources().getColor(R.color.accent_color_dark));
                star_layout.setVisibility(View.VISIBLE);
                if (starList.size() > 0 && star_layout.getChildCount() == 0) {
                    Map<String, ArrayList<HotelModel>> map = new TreeMap<String, ArrayList<HotelModel>>(starList);
                    Iterator itr = map.keySet().iterator();
                    Map<String, ArrayList<HotelModel>> newMap = new TreeMap(Collections.reverseOrder());
                    newMap.putAll(map);
                    Iterator iterator = newMap.keySet().iterator();
                    Utils.printMessage(TAG, "MapSet :: " + map.keySet() + " NewMapSet :: " + newMap.keySet());
                    while (iterator.hasNext()) {
                        String keys = (String) iterator.next();
                        Utils.printMessage(TAG, "star :: " + keys + " size :: " + map.get(keys).size() + " new map " + newMap.get(keys).size());
                        starfilter(keys, map.get(keys).size());
                    }
                }
            }
        });
        deals.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                buttonDrawable();
                layoutVisibility();
                deals.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.dealsactive, 0, 0);
                deals.setTextColor(mActivity.getResources().getColor(R.color.accent_color_dark));
                deal_layout.setVisibility(View.VISIBLE);
                if (dealList.size() > 0 && deal_layout.getChildCount() == 0) {
                    dealfilter(getString(R.string.label_deal));
                }
            }
        });
        accomandation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                buttonDrawable();
                layoutVisibility();
                accomandation.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.accomdtypeactive, 0, 0);
                accomandation.setTextColor(mActivity.getResources().getColor(R.color.accent_color_dark));
                accommandation_layout.setVisibility(View.VISIBLE);
                if (accommandationList.size() > 0 && accommandation_layout.getChildCount() == 0) {
                    Iterator itr = accommandationList.keySet().iterator();
                    while (itr.hasNext()) {
                        String keys = (String) itr.next();
                        accommadtionfilter(keys);
                    }
                }
            }
        });
        distric.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                buttonDrawable();
                layoutVisibility();
                distric.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.districtactive, 0, 0);
                distric.setTextColor(mActivity.getResources().getColor(R.color.accent_color_dark));
                district_layout.setVisibility(View.VISIBLE);
                if (districtList.size() > 0 && district_layout.getChildCount() == 0) {
                    Iterator itr = districtList.keySet().iterator();
                    while (itr.hasNext()) {
                        String keys = (String) itr.next();
                        Utils.printMessage(TAG, "keys" + keys);
                        distric(keys, districtList.get(keys).size());
                    }
                }
            }
        });
        hotelChain.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                buttonDrawable();
                layoutVisibility();
                hotelChain.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.hotelchainactive, 0, 0);
                hotelChain.setTextColor(mActivity.getResources().getColor(R.color.accent_color_dark));
                hotel_chain_layout.setVisibility(View.VISIBLE);
                if (hotelChainList.size() > 0 && hotel_chain_layout.getChildCount() == 0) {
                    Iterator itr = hotelChainList.keySet().iterator();
                    while (itr.hasNext()) {
                        String keys = (String) itr.next();
                        hotelChainfilter(keys);
                    }
                }
            }
        });
        price.performClick();
        return view;
    }

    private void findViews(View view) {
        district_layout = (LinearLayout) view.findViewById(R.id.district_layout);
        accommandation_layout = (LinearLayout) view.findViewById(R.id.accommondation_layout);
        deal_layout = (LinearLayout) view.findViewById(R.id.deal_layout);
        price_layout = (RelativeLayout) view.findViewById(R.id.price_layout);
        hotel_chain_layout = (LinearLayout) view.findViewById(R.id.hotel_chain_layout);
        star_layout = (LinearLayout) view.findViewById(R.id.star_layout);
        TextView imgBack = (TextView) view.findViewById(R.id.img_back);
        reset = (TextView) view.findViewById(R.id.reset);
        apply = (TextView) view.findViewById(R.id.apply);
        price = (TextView) view.findViewById(R.id.price);
        starRating = (TextView) view.findViewById(R.id.star_rating);
        deals = (TextView) view.findViewById(R.id.deals);
        accomandation = (TextView) view.findViewById(R.id.accomandation);
        distric = (TextView) view.findViewById(R.id.distric);
        hotelChain = (TextView) view.findViewById(R.id.hotel_chain);
        errorView = (RelativeLayout) view.findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) view.findViewById(R.id.errorMessageText);
        minvalue = (TextView) view.findViewById(R.id.min_value);
        maxvalue = (TextView) view.findViewById(R.id.max_value);
        seekBarInteger = (RangeSeekBar) view.findViewById(R.id.seekBar);
        backLayout = (LinearLayout) view.findViewById(R.id.back_layout);
        String regularText = Constants.FONT_ROBOTO_REGULAR;
        isArabicLang = false;
        if (Utils.isArabicLangSelected(mActivity)) {
            isArabicLang = true;
            regularText = Constants.FONT_DROIDKUFI_REGULAR;
            imgBack.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            reset.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            apply.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            price.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            starRating.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            deals.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            accomandation.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            distric.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            hotelChain.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            minvalue.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            maxvalue.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            imgBack.setLayoutParams(params);
        }
        textFace = Typeface.createFromAsset(mActivity.getAssets(), regularText);
        imgBack.setTypeface(textFace);
        reset.setTypeface(textFace);
        apply.setTypeface(textFace);
        price.setTypeface(textFace);
        starRating.setTypeface(textFace);
        deals.setTypeface(textFace);
        accomandation.setTypeface(textFace);
        distric.setTypeface(textFace);
        hotelChain.setTypeface(textFace);
        minvalue.setTypeface(textFace);
        maxvalue.setTypeface(textFace);
        errorMessageText.setTypeface(textFace);
    }

    public void distric(final String name, int count) {
        TextView districtName;
        TextView districtCount;
        final ImageView districtSelect;
        View layout2 = LayoutInflater.from(mActivity).inflate(R.layout.district_layout, district_layout, false);
        districtName = (TextView) layout2.findViewById(R.id.district_name);
        districtCount = (TextView) layout2.findViewById(R.id.district_count);
        districtSelect = (ImageView) layout2.findViewById(R.id.district_select);
        if (isArabicLang) {
            districtCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            districtName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        districtCount.setTypeface(textFace);
        districtName.setTypeface(textFace);
        if (districtPosition.contains(name)) {
            districtSelect.setVisibility(View.VISIBLE);
        } else {
            districtSelect.setVisibility(View.INVISIBLE);
        }
        districtName.setText(name);
        districtCount.setText("(" + count + ")");
        layout2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (districtSelect.getVisibility() == View.VISIBLE) {
                    districtSelect.setVisibility(View.INVISIBLE);
                    districtPosition.remove(name);
                } else {
                    districtSelect.setVisibility(View.VISIBLE);
                    districtPosition.add(name);
                }
            }
        });
        district_layout.addView(layout2);
    }

    public void accommadtionfilter(final String name) {
        TextView districtName;
        final ImageView districtSelect;
        View layout2 = LayoutInflater.from(mActivity).inflate(R.layout.accommodation_layout, accommandation_layout, false);
        districtName = (TextView) layout2.findViewById(R.id.district_name);
        if (isArabicLang) {
            districtName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        districtName.setTypeface(textFace);
        districtSelect = (ImageView) layout2.findViewById(R.id.district_select);
        if (accommandationPosition.contains(name)) {
            districtSelect.setVisibility(View.VISIBLE);
        } else {
            districtSelect.setVisibility(View.INVISIBLE);
        }
        districtName.setText(name);
        layout2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (districtSelect.getVisibility() == View.VISIBLE) {
                    districtSelect.setVisibility(View.INVISIBLE);
                    accommandationPosition.remove(name);
                } else {
                    districtSelect.setVisibility(View.VISIBLE);
                    accommandationPosition.add(name);
                }
            }
        });
        accommandation_layout.addView(layout2);
    }

    public void hotelChainfilter(final String name) {
        Utils.printMessage(TAG, "Chain Text :: " + name);
        TextView districtName;
        final ImageView districtSelect;
        View layout2 = LayoutInflater.from(mActivity).inflate(R.layout.accommodation_layout, accommandation_layout, false);
        districtName = (TextView) layout2.findViewById(R.id.district_name);
        districtSelect = (ImageView) layout2.findViewById(R.id.district_select);
        if (isArabicLang) {
            districtName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        districtName.setTypeface(textFace);
        if (hotelChainPosition.contains(name)) {
            districtSelect.setVisibility(View.VISIBLE);
        } else {
            districtSelect.setVisibility(View.INVISIBLE);
        }
        districtName.setText(name);
        layout2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (districtSelect.getVisibility() == View.VISIBLE) {
                    districtSelect.setVisibility(View.INVISIBLE);
                    hotelChainPosition.remove(name);
                } else {
                    districtSelect.setVisibility(View.VISIBLE);
                    hotelChainPosition.add(name);
                }
            }
        });
        hotel_chain_layout.addView(layout2);
    }

    public void starfilter(final String star, int count) {
        TextView value;
        TextView districtCount;
        TextView priceName;
        TextView starUnrate;
        RatingBar rate;
        HotelModel price;
        final ImageView districtSelect;
        View layout2 = LayoutInflater.from(mActivity).inflate(R.layout.star_layout, star_layout, false);
        value = (TextView) layout2.findViewById(R.id.price_value);
        starUnrate = (TextView) layout2.findViewById(R.id.star_unrate_label);
        districtCount = (TextView) layout2.findViewById(R.id.district_count);
        priceName = (TextView) layout2.findViewById(R.id.price_name);
        districtSelect = (ImageView) layout2.findViewById(R.id.district_select);
        if (isArabicLang) {
            districtCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            value.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            priceName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            starUnrate.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        districtCount.setTypeface(textFace);
        value.setTypeface(textFace);
        priceName.setTypeface(textFace);
        starUnrate.setTypeface(textFace);
        priceName.setText(getString(R.string.label_price_start_from));
        rate = (RatingBar) layout2.findViewById(R.id.ratingBar);
        if (starPosition.contains(star)) {
            districtSelect.setVisibility(View.VISIBLE);
        } else {
            districtSelect.setVisibility(View.INVISIBLE);
        }
        ArrayList<HotelModel> list = starList.get(star);
        price = Collections.min(list, new StarComp());
        value.setText(Utils.convertUserSelectionCurrencyWithDecimal(Double.parseDouble(price.getP()), selectedCurrency, mActivity));
        if (star.equalsIgnoreCase("0")) {
            rate.setVisibility(View.GONE);
            starUnrate.setVisibility(View.VISIBLE);
        } else {
            rate.setRating(Float.parseFloat(star));
            starUnrate.setVisibility(View.GONE);
        }
        if (count > 1 && count < 11) {
            districtCount.setText(count + " " + getString(R.string.label_star_two_ten_hotels_text));
        } else if (count == 1) {
            districtCount.setText(count + " " + getString(R.string.label_star_one_hotels_text));
        } else {
            districtCount.setText(count + " " + getString(R.string.label_star_eleven_hotels_text));
        }
        layout2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (districtSelect.getVisibility() == View.VISIBLE) {
                    districtSelect.setVisibility(View.INVISIBLE);
                    starPosition.remove(star);
                } else {
                    districtSelect.setVisibility(View.VISIBLE);
                    starPosition.add(star);
                }
            }
        });
        star_layout.addView(layout2);
    }

    public void pricefilter() {
        HotelModel pricelow = Collections.min(finallist, new StarComp());
        minPrice = pricelow.getP();
        HotelModel pricehigh = Collections.max(finallist, new StarComp());
        maxPrice = pricehigh.getP();
        seekBarInteger.setRangeValues(Double.parseDouble(minPrice), Double.parseDouble(maxPrice));
        String minVal = sharedpreferences.getString("minPrice", null);
        String maxVal = sharedpreferences.getString("maxPrice", null);
        if (minVal != null && maxVal != null) {
            minPrice = minVal;
            maxPrice = maxVal;
            Utils.printMessage(TAG, "min :: " + minPrice + " max :: " + maxPrice);
            seekBarInteger.setSelectedMaxValue(Double.parseDouble(maxPrice));
            seekBarInteger.setSelectedMinValue(Double.parseDouble(minPrice));
        }
        if (isArabicLang) {
            minvalue.setText(Utils.convertUserSelectionCurrencyWithDecimal(Double.parseDouble(maxPrice), selectedCurrency, mActivity));
            maxvalue.setText(Utils.convertUserSelectionCurrencyWithDecimal(Double.parseDouble(minPrice), selectedCurrency, mActivity));
        } else {
            minvalue.setText(Utils.convertUserSelectionCurrencyWithDecimal(Double.parseDouble(minPrice), selectedCurrency, mActivity));
            maxvalue.setText(Utils.convertUserSelectionCurrencyWithDecimal(Double.parseDouble(maxPrice), selectedCurrency, mActivity));
        }
        maxPrice = "" + Double.parseDouble(maxPrice);
        minPrice = "" + Double.parseDouble(minPrice);
        seekBarInteger.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Double>() {
            @Override
            public void onRangeSeekBarValuesChanged(
                    RangeSeekBar<?> bar, Double minValue,
                    Double maxValue) {
                String priceResulMin = Utils.convertUserSelectionCurrencyWithDecimal(minValue, selectedCurrency, mActivity);
                String priceResulMax = Utils.convertUserSelectionCurrencyWithDecimal(maxValue, selectedCurrency, mActivity);
                if (isArabicLang) {
                    minvalue.setText(priceResulMax);
                    maxvalue.setText(priceResulMin);
                } else {
                    minvalue.setText(priceResulMin);
                    maxvalue.setText(priceResulMax);
                }
                Utils.printMessage(TAG, "value :: " + minValue + "  " + maxValue);
                minPrice = "" + minValue;
                maxPrice = "" + maxValue;
            }
        });
    }

    public void dealfilter(final String name) {
        TextView districtName;
        final ImageView districtSelect;
        View layout2 = LayoutInflater.from(mActivity).inflate(R.layout.accommodation_layout, accommandation_layout, false);
        districtName = (TextView) layout2.findViewById(R.id.district_name);
        districtSelect = (ImageView) layout2.findViewById(R.id.district_select);
        if (isArabicLang) {
            districtName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        districtName.setTypeface(textFace);
        if (selectDeal) {
            districtSelect.setVisibility(View.VISIBLE);
        } else {
            districtSelect.setVisibility(View.INVISIBLE);
        }
        districtName.setText(name);
        layout2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (districtSelect.getVisibility() == View.VISIBLE) {
                    districtSelect.setVisibility(View.INVISIBLE);
                    selectDeal = false;
                } else {
                    districtSelect.setVisibility(View.VISIBLE);
                    selectDeal = true;
                }
            }
        });
        deal_layout.addView(layout2);
    }

    public void layoutVisibility() {
        deal_layout.setVisibility(View.GONE);
        price_layout.setVisibility(View.GONE);
        star_layout.setVisibility(View.GONE);
        district_layout.setVisibility(View.GONE);
        accommandation_layout.setVisibility(View.GONE);
        hotel_chain_layout.setVisibility(View.GONE);
    }

    public void buttonDrawable() {
        deals.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.dealsnormal, 0, 0);
        price.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.pricenormal, 0, 0);
        starRating.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.starratingnormal, 0, 0);
        distric.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.districtnormal, 0, 0);
        accomandation.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.accomdtypenormal, 0, 0);
        hotelChain.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.hotelchainnormal, 0, 0);
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(mActivity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(mActivity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                }
            }, 2000);
        }
    }

    public void districLoop() {
        for (HotelModel model : finallist) {
            if (model.getDistric() != null && !model.getDistric().equalsIgnoreCase("null") && model.getDistric().length() > 0) {
                ArrayList<HotelModel> list = districtList.get(model.getDistric());
                if (list == null) {
                    list = new ArrayList<>();
                    districtList.put(model.getDistric(), list);
                }
                list.add(model);
            }
        }
    }

    public void chainLoop() {
        for (HotelModel model : finallist) {
            if (model.getChainName() != null && !model.getChainName().equalsIgnoreCase("null") && model.getChainName().length() > 0) {
                ArrayList<HotelModel> list = hotelChainList.get(model.getChainName());
                if (list == null) {
                    list = new ArrayList<>();
                    hotelChainList.put(model.getChainName(), list);
                }
                list.add(model);
            }
        }
    }

    public void accomandLoop() {
        for (HotelModel model : finallist) {
            if (model.getHotel_type() != null && !model.getHotel_type().equalsIgnoreCase("null") && model.getHotel_type().length() > 0) {
                ArrayList<HotelModel> list = accommandationList.get(model.getHotel_type());
                if (list == null) {
                    list = new ArrayList<>();
                    accommandationList.put(model.getHotel_type(), list);
                }
                list.add(model);
            }
        }
    }

    public void starLoop() {
        for (HotelModel model : finallist) {
            String star = model.getStar();
            if (star == null || star.length() == 0)
                star = "0";
            ArrayList<HotelModel> list = starList.get(star);
            if (list == null) {
                list = new ArrayList<>();
                starList.put(star, list);
            }
            list.add(model);
        }
    }

    public void dealLoop() {
        for (HotelModel model : finallist) {
            if (Double.parseDouble(model.getP()) < Double.parseDouble(model.getWdp())) {
                dealList.add(model);
            }
        }
    }

    class StarComp implements Comparator<HotelModel> {
        @Override
        public int compare(HotelModel e1, HotelModel e2) {
            if (Double.parseDouble(e1.getP()) == Double.parseDouble(e2.getP())) {
                return 0;
            }
            if (Double.parseDouble(e1.getP()) < Double.parseDouble(e2.getP())) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }
}
