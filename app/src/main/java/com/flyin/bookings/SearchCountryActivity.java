package com.flyin.bookings;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.flyin.bookings.adapter.SearchCountryAdapter;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Country;
import com.flyin.bookings.util.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class SearchCountryActivity extends AppCompatActivity {
    private SearchCountryAdapter mAdapter = null;
    private TextView cancelCountryText = null;
    private EditText searchCountryEditText = null;
    private LinearLayout searchCountryTextLayout = null;
    private LinearLayout searchCountryScreenLayout = null;
    private static boolean isArabicLang = false;
    private ArrayList<Country> countryList = null;
    private ArrayList<Country> tempCountryList = null;
    private boolean isPhoneNumberClicked = false;
    private static final String TAG = "SearchCountryActivity";
    private String selectedCountryName = "";
    private String selectedCountryIsoCode = "";
    private String selectedCountryCode = "";
    private int selectedCountryFlag = 0;
    private String selectedValue = "";
    private ProgressDialog barProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_country);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isPhoneNumberClicked = bundle.getBoolean(Constants.IS_MOBILE_NUMBER_CLICKED, false);
            selectedValue = bundle.getString(Constants.SELECTED_VALUE, "");
        }
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        TextView searchCountryText = (TextView) findViewById(R.id.search_country_title);
        cancelCountryText = (TextView) findViewById(R.id.cancel_country_text);
        searchCountryEditText = (EditText) findViewById(R.id.search_country_editText);
        searchCountryTextLayout = (LinearLayout) findViewById(R.id.search_country_textView);
        searchCountryScreenLayout = (LinearLayout) findViewById(R.id.search_country_screen_layout);
        ListView countryListView = (ListView) findViewById(R.id.search_country_listView);
        countryList = new ArrayList<>();
        tempCountryList = new ArrayList<>();
        if (Utils.isArabicLangSelected(SearchCountryActivity.this)) {
            isArabicLang = true;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                searchCountryEditText.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
            searchCountryText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            searchCountryEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            cancelCountryText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontTitle);
        searchCountryText.setTypeface(textFace);
        cancelCountryText.setTypeface(textFace);
        initCodes(SearchCountryActivity.this);
        searchCountryTextLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchCountryTextLayout.setVisibility(View.GONE);
                searchCountryScreenLayout.setVisibility(View.VISIBLE);
                searchCountryEditText.requestFocus();
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        });
        searchCountryEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                handleSearch(searchCountryEditText.getText().toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
        cancelCountryText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchCountryTextLayout.setVisibility(View.VISIBLE);
                searchCountryScreenLayout.setVisibility(View.GONE);
                searchCountryEditText.setText("");
                final InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.hideSoftInputFromWindow(cancelCountryText.getWindowToken(), 0);
            }
        });
        mAdapter = new SearchCountryAdapter(this, countryList, isPhoneNumberClicked);
        countryListView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        countryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
                String countryName = countryList.get(position).getName();
                for (int i = 0; i < countryList.size(); i++) {
                    if (!countryName.equalsIgnoreCase(countryList.get(i).getName())) {
                        countryList.get(i).setSelected(false);
                    }
                }
                selectedCountryName = countryList.get(position).getName();
                selectedCountryIsoCode = countryList.get(position).getCountryISO();
                selectedCountryCode = countryList.get(position).getCountryCodeStr();
                selectedCountryFlag = countryList.get(position).getResId();
                countryList.get(position).setSelected(true);
                mAdapter.notifyDataSetChanged();
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.calendar_actionbar).setVisibility(View.VISIBLE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_to_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_header);
        Button doneButton = (Button) mCustomView.findViewById(R.id.select_button);
        backText.setTypeface(textFace);
        doneButton.setTypeface(tf);
        if (isArabicLang) {
            doneButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            backText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = getIntent();
                it.putExtra(Constants.SELECTED_COUNTRY_NAME, selectedCountryName);
                it.putExtra(Constants.SELECTED_COUNTRY_ISO_CODE, selectedCountryIsoCode);
                it.putExtra(Constants.SELECTED_COUNTRY_CODE, selectedCountryCode);
                it.putExtra(Constants.SELECTED_COUNTRY_FLAG, String.valueOf(selectedCountryFlag));
                setResult(RESULT_OK, it);
                finish();
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        });
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        mActionBar.setCustomView(mCustomView, layout);
        mActionBar.setDisplayShowCustomEnabled(true);
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    protected void initCodes(Activity activity) {
        showProgress();
        new AsyncPhoneInitTask(activity).execute();
    }

    protected class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {
        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
            ArrayList<Country> data = new ArrayList<Country>();
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("country_codes.txt"), "UTF-8"));
                String line;
                while ((line = reader.readLine()) != null) {
                    Country c = new Country(mContext, line, selectedValue, isPhoneNumberClicked);
                    data.add(c);
                }
            } catch (IOException e) {
                Utils.printMessage(TAG, "error :: " + e.getMessage());
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Country> data) {
            countryList.addAll(data);
            tempCountryList.addAll(data);
            closeProgress();
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Handling Search Country name
     **/
    private void handleSearch(String searchText) {
        countryList.clear();
        if (searchText.isEmpty()) {
            countryList.addAll(tempCountryList);
        } else {
            for (Country item : tempCountryList) {
                if (item.getName() != null) {
                    String countryName = item.getName();
                    if (!countryName.equalsIgnoreCase("null") || !countryName.equalsIgnoreCase("")) {
                        String countryText = countryName.toLowerCase();
                        if (countryText.contains(searchText.toLowerCase())) {
                            countryList.add(item);
                            continue;
                        }
                    }
                }
            }
        }
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void showProgress() {
        if (barProgressDialog == null) {
            barProgressDialog = new ProgressDialog(this);
            barProgressDialog.setMessage(getString(R.string.label_wait_please_message));
            barProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            barProgressDialog.show();
        }
    }

    private void closeProgress() {
        if (barProgressDialog != null) {
            barProgressDialog.dismiss();
            barProgressDialog = null;
        }
    }
}
