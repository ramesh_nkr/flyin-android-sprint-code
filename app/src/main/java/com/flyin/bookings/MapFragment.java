package com.flyin.bookings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("ALL")
public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private ArrayList<HotelModel> hotelList = null;
    private ImageView image_list, nearImage;
    private String people, nights;
    private TextView hotel_name, hotel_price;
    private RatingBar hotel_star;
    private ArrayList<Marker> markers;
    private RelativeLayout bottomLayout;
    private int adults, childs;
    private ArrayList<HotelModel> finalhotelList;
    private String header_date;
    private String country = "";
    private String city = "";
    private static final String TAG = "MapFragment";
    private int selectedHotelPosition = 0;
    private GoogleMap googleMap = null;
    private ArrayList<String> adultList;
    private ArrayList<String> childList;
    private int lastMarkerIndex = 0;
    private HashMap<String, ArrayList<String>> childCount;
    private String randomNumber = "";
    private Activity mActivity;
    private boolean star_check = false, price_check = false, check_accending = true;
    private boolean iSstar_check = false, iSprice_check = false;
    private String arrowStaus = "";
    private String hotelCheckInDate = "";
    private String hotelCheckOutDate = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hotel_listing_map_final, container, false);
        TextView back = (TextView) view.findViewById(R.id.img_back);
        nearImage = (ImageView) view.findViewById(R.id.img_hotel_thumbnail);
        image_list = (ImageView) view.findViewById(R.id.img_list_view);
        ImageView img_filter = (ImageView) view.findViewById(R.id.img_filter);
        TextView date = (TextView) view.findViewById(R.id.txt_date);
        TextView txt_people = (TextView) view.findViewById(R.id.txt_people);
        TextView txt_night = (TextView) view.findViewById(R.id.txt_nights);
        TextView close = (TextView) view.findViewById(R.id.close);
        TextView btn_book_hotel_map = (TextView) view.findViewById(R.id.btn_book_hotel_map);
        TextView text_city = (TextView) view.findViewById(R.id.txt_city);
        TextView text_country = (TextView) view.findViewById(R.id.txt_country);
        hotel_name = (TextView) view.findViewById(R.id.txt_hotel_title);
        TextView hotel_distance = (TextView) view.findViewById(R.id.distance_from_location);
        hotel_star = (RatingBar) view.findViewById(R.id.ratingBar_hotel_map);
        hotel_price = (TextView) view.findViewById(R.id.txt_hotel_price);
        bottomLayout = (RelativeLayout) view.findViewById(R.id.bottomLayout);
        EditText search = (EditText) view.findViewById(R.id.txt_search_hotel_name);
        LinearLayout backLayout = (LinearLayout) view.findViewById(R.id.back_layout);
        String fontRegularBold = Constants.FONT_ROBOTO_BOLD;
        String fontRegularLight = Constants.FONT_ROBOTO_LIGHT;
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(mActivity)) {
            fontRegularBold = Constants.FONT_DROIDKUFI_BOLD;
            fontRegularLight = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
            search.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            back.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            btn_book_hotel_map.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            text_city.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.textView_size));
            date.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            txt_people.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            txt_night.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            back.setLayoutParams(params);
        }
        hotelList = getArguments().getParcelableArrayList("list");
        header_date = getArguments().getString("checkin");
        nights = getArguments().getString("night_count");
        adults = getArguments().getInt("adult_count", 0);
        childs = getArguments().getInt("child_count", 0);
        adultList = getArguments().getStringArrayList("adults_list");
        childList = getArguments().getStringArrayList("childs_list");
        childCount = (HashMap<String, ArrayList<String>>) getArguments().getSerializable("children_list");
        price_check = getArguments().getBoolean("price_check", false);
        star_check = getArguments().getBoolean("star_check", false);
        iSprice_check = getArguments().getBoolean("iSprice_check", false);
        iSstar_check = getArguments().getBoolean("iSstar_check", false);
        check_accending = getArguments().getBoolean("check_accending", false);
        arrowStaus = getArguments().getString("arrowStaus");
        hotelCheckInDate = getArguments().getString("hotelCheckInDate");
        hotelCheckOutDate = getArguments().getString("hotelCheckOutDate");
        people = "" + (childs + adults);
        if (getArguments().containsKey("final_list")) {
            finalhotelList = getArguments().getParcelableArrayList("final_list");
        } else {
            finalhotelList = hotelList;
        }
        errorView = (RelativeLayout) view.findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) view.findViewById(R.id.errorMessageText);
        Typeface regularLight = Typeface.createFromAsset(mActivity.getAssets(), fontRegularLight);
        Typeface regularBold = Typeface.createFromAsset(mActivity.getAssets(), fontRegularBold);
        Typeface regularFont = Typeface.createFromAsset(mActivity.getAssets(), fontRegular);
        hotel_name.setTypeface(regularBold);
        hotel_distance.setTypeface(regularLight);
        close.setTypeface(regularBold);
        btn_book_hotel_map.setTypeface(regularBold);
        text_city.setTypeface(regularBold);
        text_country.setTypeface(regularBold);
        date.setTypeface(regularLight);
        txt_people.setTypeface(regularLight);
        txt_night.setTypeface(regularLight);
        back.setTypeface(regularFont);
        search.setTypeface(regularLight);
        hotel_price.setTypeface(regularFont);
        hotel_distance.setVisibility(View.GONE);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        city = getArguments().getString("city");
        String[] city_data = city.split(",");
        text_city.setText(city);
        if (city_data.length == 1) {
            country = city_data[0];
            text_country.setVisibility(View.GONE);
        } else {
            country = city_data[1];
            text_country.setVisibility(View.GONE);
        }
        randomNumber = getArguments().getString(Constants.BOOKING_RANDOM_NUMBER, "");
        date.setText(header_date);
        txt_people.setText(people);
        txt_night.setText(nights);
        mapFragment.getMapAsync(MapFragment.this);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_list.performClick();
            }
        });
        image_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new HotelListFragment();
                Bundle bundle = new Bundle();
                bundle.putString("night_count", "" + nights);
                bundle.putString("checkin", header_date);
                bundle.putInt("child_count", childs);
                bundle.putInt("adult_count", adults);
                bundle.putParcelableArrayList("list", hotelList);
                bundle.putParcelableArrayList("final_list", finalhotelList);
                bundle.putStringArrayList("adults_list", new ArrayList<>(adultList));
                bundle.putStringArrayList("childs_list", new ArrayList<>(childList));
                bundle.putSerializable("children_list", childCount);
                bundle.putString("city", city);
                bundle.putBoolean("price_check", price_check);
                bundle.putBoolean("star_check", star_check);
                bundle.putBoolean("iSprice_check", iSprice_check);
                bundle.putBoolean("iSstar_check", iSstar_check);
                bundle.putBoolean("check_accending", check_accending);
                bundle.putString("arrowStaus", arrowStaus);
                bundle.putString("hotelCheckInDate", hotelCheckInDate);
                bundle.putString("hotelCheckOutDate", hotelCheckOutDate);
                bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                fragment.setArguments(bundle);
                ((MainSearchActivity) mActivity).pushFragment(fragment);
            }
        });
        if (hotelList.size() > 0) {
            setNearHotel(hotelList.get(0), 0);
        } else
            bottomLayout.setVisibility(View.GONE);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomLayout.setVisibility(View.GONE);
            }
        });
        btn_book_hotel_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                if (editor != null) {
                    editor.putBoolean(Constants.IS_ROOM_INFO_UPDATED, false);
                    editor.putString("room_object", "");
                    editor.putString("update_in_date", "");
                    editor.putString("update_stay_nights", "");
                    editor.apply();
                }
                Fragment fragment = new HotelDetailFragment();
                try {
                    bundle.putParcelable("model", (Parcelable) hotelList.get(selectedHotelPosition).clone());
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
                bundle.putStringArrayList("adults_list", adultList);
                bundle.putStringArrayList("childs_list", childList);
                bundle.putSerializable("children_list", childCount);
                bundle.putString("header_date", header_date);
                bundle.putInt("adult_count", adults);
                bundle.putInt("child_count", childs);
                bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                bundle.putString("isMapFragment", "fromMapFrag");
                fragment.setArguments(bundle);
                ((MainSearchActivity) mActivity).pushFragment(fragment);
            }
        });
        img_filter.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Fragment fragment = new FilterFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("list", finalhotelList);
                bundle.putString("night_count", "" + nights);
                bundle.putString("persion", "" + people);
                bundle.putString("checkin", header_date);
                bundle.putInt("adult_count", adults);
                bundle.putInt("child_count", childs);
                bundle.putInt("check_class", 2);
                bundle.putStringArrayList("adults_list", new ArrayList<>(adultList));
                bundle.putStringArrayList("childs_list", new ArrayList<>(childList));
                bundle.putSerializable("children_list", childCount);
                bundle.putString("hotelCheckInDate", hotelCheckInDate);
                bundle.putString("hotelCheckOutDate", hotelCheckOutDate);
                fragment.setArguments(bundle);
                ((MainSearchActivity) mActivity).pushFragment(fragment);
            }
        });
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        if (!pref.getString("no_hotel_msg", "").isEmpty()) {
            displayErrorMessage(pref.getString("no_hotel_msg", ""));
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("no_hotel_msg", "");
            editor.apply();
        }
        return view;
    }

    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView != null) {
            if (errorView.getAlpha() == 0) {
                errorView.setTranslationY(-60);
                errorView.setVisibility(View.VISIBLE);
                errorView.animate().translationY(0).alpha(1)
                        .setDuration(mActivity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                if (mActivity != null) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (errorView != null && mActivity != null && mActivity.getResources() != null) {
                                errorView.animate().alpha(0)
                                        .setDuration(mActivity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                            }
                        }
                    }, 2000);
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Bundle bundle = getArguments();
        if (bundle == null) {
            Utils.printMessage(TAG, "No lat longs in bundle found");
            return;
        }
        this.googleMap = googleMap;
        callMarker(googleMap);
    }

    public Bitmap addMarker(HotelModel model, boolean isSelectedMarker) {
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap bmp = Bitmap.createBitmap(180, 180, conf);
        Canvas canvas1 = new Canvas(bmp);
        Paint color = new Paint();
        int textScaledSize = mActivity.getResources().getDimensionPixelSize(R.dimen.flight_duration_text);
        int priceTextScale = mActivity.getResources().getDimensionPixelSize(R.dimen.flight_moreDetails);
        int firstxPosScaleSize = mActivity.getResources().getDimensionPixelSize(R.dimen.signInPage_view_gap);
        int firstyPosScaleSize = mActivity.getResources().getDimensionPixelSize(R.dimen.contact_email_margin);
        int xPosScaleSize = mActivity.getResources().getDimensionPixelSize(R.dimen.userDetails_margin);
        int yPosScaleSize = mActivity.getResources().getDimensionPixelSize(R.dimen.flag_height);
        int imageScaledSize = mActivity.getResources().getDimensionPixelSize(R.dimen.margin_size);
        int textYScaledSize = mActivity.getResources().getDimensionPixelSize(R.dimen.search_margin);
        color.setTextSize(priceTextScale);
        color.setColor(Color.WHITE);
        Paint color2 = new Paint();
        color2.setTextSize(textScaledSize);
        color2.setColor(Color.WHITE);
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(mActivity)) {
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
        }
        Typeface regularFont = Typeface.createFromAsset(mActivity.getAssets(), fontRegular);
        color.setTypeface(regularFont);
        color2.setTypeface(regularFont);
        if (isSelectedMarker) {
            canvas1.drawBitmap(BitmapFactory.decodeResource(mActivity.getResources(), R.drawable.callout2), 0, 0, color);
        } else {
            canvas1.drawBitmap(BitmapFactory.decodeResource(mActivity.getResources(), R.drawable.callout1), 0, 0, color);
        }
        if (model.getStar().equalsIgnoreCase("0")) {
            canvas1.drawText(getString(R.string.label_star_unrate), xPosScaleSize, textYScaledSize, color2);
            canvas1.drawText(Utils.convertPriceToUserSelectionHotel(Double.parseDouble(model.getP()), model.getCur(), mActivity), xPosScaleSize, yPosScaleSize, color);
        } else {
            canvas1.drawText(model.getStar(), imageScaledSize, imageScaledSize, color2);
            canvas1.drawBitmap(BitmapFactory.decodeResource(mActivity.getResources(), R.drawable.smallstar), firstxPosScaleSize, firstyPosScaleSize, color);
            canvas1.drawText(Utils.convertPriceToUserSelectionHotel(Double.parseDouble(model.getP()), model.getCur(), mActivity), xPosScaleSize, yPosScaleSize, color);
        }
        return bmp;
    }

    public void setNearHotel(final HotelModel model, int position) {
        ViewTreeObserver vto = nearImage.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                nearImage.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = nearImage.getMeasuredHeight();
                int finalWidth = nearImage.getMeasuredWidth();
                String url = Constants.IMAGE_SCALING_URL + finalWidth + "x" + finalHeight + Constants.IMAGE_QUALITY + "" + model.getImage();
                try {
                    Glide.with(mActivity).load(url).placeholder(R.drawable.imagethumb).into(nearImage);
                } catch (Exception e) {
                }
                return true;
            }
        });
        hotel_name.setText(model.getHna());
        double price = Double.parseDouble(model.getP());
        hotel_price.setText(Utils.convertPriceToUserSelectionHotel(price, model.getCur(), mActivity));
        int star = Integer.parseInt(model.getStar());
        hotel_star.setRating(star);
        updateMarker(position);
    }

    private void updateMarker(int position) {
        selectedHotelPosition = position;
        if (markers == null || markers.size() == 0) {
            return;
        }
        try {
            Marker marker = markers.get(position);
            marker.remove();
            HotelModel hotelModel = hotelList.get(position);
            Marker newMarker = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng((Double.parseDouble(hotelModel.getLatitude())),
                            (Double.parseDouble(hotelModel.getLongitude()))))
                    .icon(BitmapDescriptorFactory.fromBitmap(addMarker(hotelModel, true)))
                    .anchor(0.5f, 1).snippet("" + position));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(markers.get(position).getPosition(), 17));
            markers.set(position, newMarker);
        } catch (Exception e) {
        }
        try {
            Marker marker = markers.get(lastMarkerIndex);
            marker.remove();
            HotelModel hotelModel = hotelList.get(lastMarkerIndex);
            Marker newMarker = googleMap
                    .addMarker(new MarkerOptions()
                            .position(new LatLng((Double.parseDouble(hotelModel.getLatitude())),
                                    (Double.parseDouble(hotelModel.getLongitude()))))
                            .icon(BitmapDescriptorFactory.fromBitmap(addMarker(hotelModel, false)))
                            .anchor(0.5f, 1).snippet("" + lastMarkerIndex));
            markers.set(lastMarkerIndex, newMarker);
        } catch (Exception e) {
        }
        lastMarkerIndex = position;
    }

    public void callMarker(GoogleMap googleMap) {
        if (hotelList != null) {
            markers = new ArrayList<>();
            for (int i = 0; i < hotelList.size(); i++) {
                boolean isSelectedMarker = false;
                if (i == 0) {
                    isSelectedMarker = true;
                }
                HotelModel hotelModel = hotelList.get(i);
                try {
                    Marker marker = googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng((Double.parseDouble(hotelModel.getLatitude())),
                                    (Double.parseDouble(hotelModel.getLongitude()))))
                            .icon(BitmapDescriptorFactory.fromBitmap(addMarker(hotelModel, isSelectedMarker)))
                            .anchor(0.5f, 1).snippet("" + i));
                    markers.add(marker);
                } catch (Exception e) {
                }
            }
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(markers.get(0).getPosition(), 17));
            googleMap.setOnMarkerClickListener(this);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        selectedHotelPosition = Integer.parseInt(marker.getSnippet());
        setNearHotel(hotelList.get(selectedHotelPosition), selectedHotelPosition);
        return true;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }
}
