package com.flyin.bookings;


import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

public class HotelInfoFragment extends Fragment {
    private HotelModel model;
    private Typeface regularFont;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hotel_info, container, false);
        model = getArguments().getParcelable("model");
        TextView exterior_header = (TextView) view.findViewById(R.id.extrior_header);
        TextView exterior_text = (TextView) view.findViewById(R.id.extrior_text);
        TextView lobby_header = (TextView) view.findViewById(R.id.lobby_header);
        TextView lobby_text = (TextView) view.findViewById(R.id.lobby_text);
        TextView room_header = (TextView) view.findViewById(R.id.rooms_header);
        TextView room_text = (TextView) view.findViewById(R.id.rooms_text);
        TextView cancel_header = (TextView) view.findViewById(R.id.cancel_policy_header);
        TextView cancel_text = (TextView) view.findViewById(R.id.cancel_policy_text);
        TextView child_header = (TextView) view.findViewById(R.id.child_policy_header);
        TextView child_text = (TextView) view.findViewById(R.id.child_policy_text);
        TextView pet_header = (TextView) view.findViewById(R.id.pet_header);
        TextView pet_text = (TextView) view.findViewById(R.id.pet_text);
        TextView activity_header = (TextView) view.findViewById(R.id.activity);
        TextView sevice_header = (TextView) view.findViewById(R.id.service);
        TextView general_header = (TextView) view.findViewById(R.id.general);
        TextView cardHeaderName = (TextView) view.findViewById(R.id.card_header);
        TextView hotelPolicyHeader = (TextView) view.findViewById(R.id.hotel_policy_header);
        View acrivity_view = view.findViewById(R.id.view1);
        View service_view = view.findViewById(R.id.view2);
        View general_view = view.findViewById(R.id.view3);
        LinearLayout top_feature_left = (LinearLayout) view.findViewById(R.id.left_top_features);
        LinearLayout top_feature_right = (LinearLayout) view.findViewById(R.id.right_top_features);
        LinearLayout service_left = (LinearLayout) view.findViewById(R.id.left_services);
        LinearLayout service_right = (LinearLayout) view.findViewById(R.id.right_services);
        LinearLayout activity_left = (LinearLayout) view.findViewById(R.id.left_activities);
        LinearLayout activity_right = (LinearLayout) view.findViewById(R.id.right_activities);
        String fontRegularBold = Constants.FONT_ROBOTO_BOLD;
        String fontRegularMedium = Constants.FONT_ROBOTO_MEDIUM;
        String fontRegularLight = Constants.FONT_ROBOTO_LIGHT;
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(getActivity())) {
            fontRegularBold = Constants.FONT_DROIDKUFI_BOLD;
            fontRegularLight = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegularMedium = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
        }
        Typeface regularLight = Typeface.createFromAsset(getActivity().getAssets(), fontRegularLight);
        Typeface regularMedium = Typeface.createFromAsset(getActivity().getAssets(), fontRegularMedium);
        Typeface regularBold = Typeface.createFromAsset(getActivity().getAssets(), fontRegularBold);
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), fontRegular);
        activity_header.setTypeface(regularBold);
        sevice_header.setTypeface(regularBold);
        general_header.setTypeface(regularBold);
        exterior_header.setTypeface(regularMedium);
        lobby_header.setTypeface(regularMedium);
        room_header.setTypeface(regularMedium);
        cancel_header.setTypeface(regularMedium);
        child_header.setTypeface(regularMedium);
        pet_header.setTypeface(regularMedium);
        cardHeaderName.setTypeface(regularMedium);
        hotelPolicyHeader.setTypeface(regularMedium);
        exterior_text.setTypeface(regularLight);
        lobby_text.setTypeface(regularLight);
        room_text.setTypeface(regularLight);
        cancel_text.setTypeface(regularLight);
        child_text.setTypeface(regularLight);
        pet_text.setTypeface(regularLight);
        if (model.getExterior() != null && !model.getExterior().equalsIgnoreCase("null")) {
            exterior_text.setText(model.getExterior());
        } else {
            exterior_header.setVisibility(View.GONE);
            exterior_text.setVisibility(View.GONE);
        }
        if (model.getLobby() != null && !model.getLobby().equalsIgnoreCase("null")) {
            lobby_text.setText(model.getLobby());
        } else {
            lobby_header.setVisibility(View.GONE);
            lobby_text.setVisibility(View.GONE);
        }
        if (model.getRooms() != null && !model.getRooms().equalsIgnoreCase("null")) {
            room_text.setText(model.getRooms());
        } else {
            room_header.setVisibility(View.GONE);
            room_text.setVisibility(View.GONE);
        }
        if (model.getCancel_policy() != null && !model.getCancel_policy().equalsIgnoreCase("null")) {
            cancel_text.setText(model.getCancel_policy());
        } else {
            cancel_header.setVisibility(View.GONE);
            cancel_text.setVisibility(View.GONE);
        }
        if (model.getChild_policy() != null && !model.getChild_policy().equalsIgnoreCase("null")) {
            child_text.setText(model.getChild_policy());
        } else {
            child_header.setVisibility(View.GONE);
            child_text.setVisibility(View.GONE);
        }
        if (model.getPet() != null && !model.getPet().equalsIgnoreCase("null")) {
            pet_text.setText(model.getPet());
        } else {
            pet_header.setVisibility(View.GONE);
            pet_text.setVisibility(View.GONE);
        }
        String activity[] = model.getActivity();
        if (activity != null && activity.length > 0) {
            setText(activity, activity_left, activity_right);
        } else {
            acrivity_view.setVisibility(View.GONE);
            activity_header.setVisibility(View.GONE);
            activity_left.setVisibility(View.GONE);
            activity_right.setVisibility(View.GONE);
        }
        String service[] = model.getService();
        if (service != null && service.length > 0) {
            setText(service, service_left, service_right);
        } else {
            service_left.setVisibility(View.GONE);
            service_right.setVisibility(View.GONE);
            service_view.setVisibility(View.GONE);
            sevice_header.setVisibility(View.GONE);
        }
        String general[] = model.getTopFeatures();
        if (general != null && general.length > 0) {
            setText(general, top_feature_left, top_feature_right);
        } else {
            general_header.setVisibility(View.GONE);
            general_view.setVisibility(View.GONE);
            top_feature_left.setVisibility(View.GONE);
            top_feature_right.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void setText(String[] data, LinearLayout left, LinearLayout right) {
        for (int i = 0; i < data.length; i++) {
            TextView text = new TextView(getActivity());
            text.setText(data[i]);
            text.setTypeface(regularFont);
            text.setCompoundDrawablePadding(10);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                text.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.greentick, 0, 0, 0);
            } else {
                text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.greentick, 0, 0, 0);
            }
            if (i % 2 == 0) {
                left.addView(text);
            } else {
                right.addView(text);
            }
        }
    }
}
