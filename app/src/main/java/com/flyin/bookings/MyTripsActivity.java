package com.flyin.bookings;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ConstantConditions")
public class MyTripsActivity extends AppCompatActivity {
    private Typeface tf;
    private EditText searchPNRText;
    private TabLayout tabLayout;
    private boolean isArabicLang;
    private RelativeLayout errorView;
    private TextView errorMessageText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mytrips);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        isArabicLang = false;
        searchPNRText = (EditText) findViewById(R.id.search_flight_text);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        if (Utils.isArabicLangSelected(MyTripsActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            isArabicLang = true;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                searchPNRText.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
        }
        tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontText);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabText();
        searchPNRText.setTypeface(textFace);
        errorMessageText.setTypeface(textFace);
        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
                                               @Override
                                               public void onTabSelected(TabLayout.Tab tab) {
                                                   super.onTabSelected(tab);
                                                   if (tab.getPosition() == 0) {
                                                       tabLayout.getTabAt(0).setCustomView(null);
                                                       TextView tabOne = (TextView) LayoutInflater.from(MyTripsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabOne.setText(getString(R.string.label_home_flights));
                                                       tabOne.setTypeface(tf);
                                                       if (isArabicLang) {
                                                           tabOne.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabOne.setTextColor(getResources().getColor(R.color.user_profile_email_color));
                                                       tabOne.setBackgroundResource(R.drawable.selected_trips_tab);
                                                       tabLayout.getTabAt(0).setCustomView(tabOne);
                                                   } else {
                                                       tabLayout.getTabAt(1).setCustomView(null);
                                                       TextView tabTwo = (TextView) LayoutInflater.from(MyTripsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabTwo.setText(getString(R.string.label_home_hotels));
                                                       tabTwo.setTypeface(tf);
                                                       if (isArabicLang) {
                                                           tabTwo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabTwo.setTextColor(getResources().getColor(R.color.user_profile_email_color));
                                                       tabTwo.setBackgroundResource(R.drawable.selected_trips_tab);
                                                       tabLayout.getTabAt(1).setCustomView(tabTwo);
                                                   }
                                               }

                                               @Override
                                               public void onTabUnselected(TabLayout.Tab tab) {
                                                   super.onTabUnselected(tab);
                                                   if (tab.getPosition() == 0) {
                                                       tabLayout.getTabAt(0).setCustomView(null);
                                                       TextView tabOne = (TextView) LayoutInflater.from(MyTripsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabOne.setText(getString(R.string.label_home_flights));
                                                       tabOne.setTypeface(tf);
                                                       if (isArabicLang) {
                                                           tabOne.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabOne.setTextColor(getResources().getColor(R.color.white_color));
                                                       tabOne.setBackgroundResource(R.drawable.unselected_trips_tab);
                                                       tabLayout.getTabAt(0).setCustomView(tabOne);
                                                   } else {
                                                       tabLayout.getTabAt(1).setCustomView(null);
                                                       TextView tabTwo = (TextView) LayoutInflater.from(MyTripsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabTwo.setText(getString(R.string.label_home_hotels));
                                                       tabTwo.setTypeface(tf);
                                                       if (isArabicLang) {
                                                           tabTwo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabTwo.setTextColor(getResources().getColor(R.color.white_color));
                                                       tabTwo.setBackgroundResource(R.drawable.unselected_trips_tab);
                                                       tabLayout.getTabAt(1).setCustomView(tabTwo);
                                                   }
                                               }

                                               @Override
                                               public void onTabReselected(TabLayout.Tab tab) {
                                                   super.onTabReselected(tab);
                                               }
                                           }
        );
        searchPNRText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (tabLayout.getSelectedTabPosition() == 0) {
                    // flights
                    Intent intent = new Intent();
                    intent.setAction("com.flyin.bookings.SEARCH_FLIGHTS");
                    intent.putExtra("message", searchPNRText.getText().toString());
                    sendBroadcast(intent);
                } else {
                    //hotels
                    Intent intent = new Intent();
                    intent.setAction("com.flyin.bookings.SEARCH_HOTELS");
                    intent.putExtra("message", searchPNRText.getText().toString());
                    sendBroadcast(intent);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_flight, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_my_trips_header);
        mTitleText.setTypeface(tf);
        backText.setTypeface(textFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(MyTripsActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        mActionBar.setCustomView(mCustomView, layout);
        mActionBar.setDisplayShowCustomEnabled(true);
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    private void setupTabText() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText(getString(R.string.label_home_flights));
        tabOne.setTypeface(tf);
        if (isArabicLang) {
            tabOne.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        tabOne.setTextColor(getResources().getColor(R.color.user_profile_email_color));
        tabOne.setBackgroundResource(R.drawable.selected_trips_tab);
        tabLayout.getTabAt(0).setCustomView(tabOne);
        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText(getString(R.string.label_home_hotels));
        tabTwo.setTypeface(tf);
        if (isArabicLang) {
            tabTwo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        tabTwo.setTextColor(getResources().getColor(R.color.white_color));
        tabTwo.setBackgroundResource(R.drawable.unselected_trips_tab);
        tabLayout.getTabAt(1).setCustomView(tabTwo);
    }

    public void displayErrorMessage(String message, boolean isSuccess) {
        if (isSuccess) {
            errorMessageText.setText(getResources().getString(R.string.label_bnpl_cancel_booking, getString(R.string.label_booking_cancelled), getString(R.string.label_booking_cancelled_desc)));
            errorView.setBackgroundResource(R.color.success_message_background);
        } else {
            errorMessageText.setText(message);
            errorView.setBackgroundResource(R.color.error_message_background);
        }
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = this.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = this.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(this.getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private FlightTripsTab flightTripsDetailsTab = null;
    private HotelTripsTab hotelTripsDetailsTab = null;

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        flightTripsDetailsTab = new FlightTripsTab();
        hotelTripsDetailsTab = new HotelTripsTab();
        adapter.addFragment(flightTripsDetailsTab, getString(R.string.label_home_flights));
        adapter.addFragment(hotelTripsDetailsTab, getString(R.string.label_home_hotels));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (tabLayout.getSelectedTabPosition() == 0) {
            if (flightTripsDetailsTab != null) {
                flightTripsDetailsTab.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            if (hotelTripsDetailsTab != null) {
                hotelTripsDetailsTab.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
