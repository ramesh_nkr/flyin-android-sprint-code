package com.flyin.bookings;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.dialog.AddTravellerDialog;
import com.flyin.bookings.dialog.HotelListLoadingDialog;
import com.flyin.bookings.model.CityModel;
import com.flyin.bookings.model.CurrencyBean;
import com.flyin.bookings.model.FhRoomPassengerDetailsBean;
import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.model.RoomModel;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.model.TripAdvisorBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.GPSTracker;
import com.flyin.bookings.util.RandomKeyGeneration;
import com.flyin.bookings.util.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

@SuppressWarnings("deprecation")
public class HotelSearchFragment extends Fragment implements AsyncTaskListener, TextWatcher {
    private static final String TAG = "HotelSearchFragment";
    private Typeface titleFace, textFace;
    private TextView destination = null;
    private TextView dateInfoText = null;
    private TextView hotelCheckoutText = null;
    private TextView travellerInfoText = null;
    private TextView hotelName = null;
    private String hotelCityName = "";
    private RelativeLayout loadingViewLayout = null;
    private int SEARCH_FROM_AIRPORT = 1;
    private int ROUND_TRIP = 15;
    private int SEARCH_HOTEL = 12;
    private int SEARCH_HOTEL_DETAILS = 2;
    private String cityId = "";
    private String hotelId = "";
    private String searchType = "";
    private String hotelCheckInDate = "";
    private String hotelCheckOutDate = "";
    private boolean isArabicLang = false;
    private AddTravellerDialog addTravellerDialog = null;
    private HotelListLoadingDialog hotelListLoadingDialog = null;
    private ArrayList<FhRoomPassengerDetailsBean> roomPassengerArrayList = null;
    private boolean isClassTypeNeed = false;
    private String cabinType = "";
    private int totalAdultCnt = 2;
    private int totalChildCnt = 0;
    private int totalPassengerCount = 2;
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private int night_count = 0;
    private ImageView errorImage = null;
    private Button searchButton = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private ArrayList<String> huidlist = null;
    private ArrayList<HotelModel> hotelList = null;
    private ArrayList<String> adult_count_list = null;
    private ArrayList<String> childList = null;
    private ArrayList<String> occupancy_adult_count_list = null;
    private HashMap<String, ArrayList<String>> child_count_list = null;
    private HashMap<String, ArrayList<String>> occupancy_child_count_list = null;
    private ArrayList<Integer> errorlist = null;
    private String from_date = "";
    private String to_date = "";
    private GPSTracker gps = null;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private static final int GET_CURRENCY_DATA = 3;
    private String savedCurrency = "";
    private int room_count = 1;
    private static final int GET_CITY = 11;
    private ArrayAdapter adapter;
    private ArrayList<String> listdata;
    private ArrayList<CityModel> modelListdata;
    private int searchCount = 0;
    private boolean dealsNearMeClicked = false;
    private String dealsNearCityId = "";
    private ArrayList<TripAdvisorBean> tripAdvisorAry = new ArrayList<>();
    private int SEARCH_HOTEL_TRIP = 20;
    private String currentDate;
    private LayoutInflater layoutInflater = null;
    private Activity mActivity;
    private String randomNumber = "";
    private String serach_jsn = "";
    private int SEARCH_HOTEL_DEALSNEARBY = 12;
    private ImageView hotelViewBgImage = null;
    private MyReceiver myReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hotel_search, container, false);
        ((MainSearchActivity) mActivity).showToolbar();
        Calendar calendar = Calendar.getInstance();
        huidlist = new ArrayList<>();
        hotelList = new ArrayList<>();
        adult_count_list = new ArrayList<>();
        childList = new ArrayList<>();
        occupancy_adult_count_list = new ArrayList<>();
        child_count_list = new HashMap<>();
        occupancy_child_count_list = new HashMap<>();
        errorlist = new ArrayList<>();
        listdata = new ArrayList<>();
        modelListdata = new ArrayList<>();
        isArabicLang = false;
        adapter = new ArrayAdapter<>(mActivity, R.layout.search_text, listdata);
        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        TextView hotelCountText = (TextView) view.findViewById(R.id.hotels_count_text);
        TextView hotelHeaderText = (TextView) view.findViewById(R.id.hotels_header_text);
        TextView dealsNearText = (TextView) view.findViewById(R.id.deals_near_text);
        hotelName = (TextView) view.findViewById(R.id.edittext_hotel);
        destination = (TextView) view.findViewById(R.id.dates_header_text);
        dateInfoText = (TextView) view.findViewById(R.id.dates_info_text);
        hotelCheckoutText = (TextView) view.findViewById(R.id.hotel_checkout_text);
        TextView travellerHeaderText = (TextView) view.findViewById(R.id.traveller_header_text);
        travellerInfoText = (TextView) view.findViewById(R.id.traveller_info_text);
        LinearLayout hotelCitySearchLayout = (LinearLayout) view.findViewById(R.id.hotel_city_search_layout);
        LinearLayout hotelDatesLayout = (LinearLayout) view.findViewById(R.id.hotel_dates_layout);
        LinearLayout travellerDetailsLayout = (LinearLayout) view.findViewById(R.id.traveller_details_layout);
        Button searchHotelButton = (Button) view.findViewById(R.id.search_hotels_button);
        loadingViewLayout = (RelativeLayout) view.findViewById(R.id.loading_view_layout);
        ImageView dealsNearImg = (ImageView) view.findViewById(R.id.deals_near_img);
        LinearLayout dealsNearMeLayout = (LinearLayout) view.findViewById(R.id.deals_near_me_layout);
        hotelViewBgImage = (ImageView) view.findViewById(R.id.hotel_view_bg_image);
        dateInfoText.setText(getString(R.string.label_hotel_search_check_in));
        hotelCheckoutText.setText(getString(R.string.label_hotel_search_check_out));
        layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (Utils.isArabicLangSelected(mActivity)) {
            isArabicLang = true;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            dealsNearImg.setRotation(270);
            hotelCountText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.contact_number_size));
            hotelHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.contact_text));
            dealsNearText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            hotelName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            destination.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            dateInfoText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_airline_name_text));
            hotelCheckoutText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_airline_name_text));
            travellerHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            travellerInfoText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_airline_name_text));
            searchHotelButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.contact_text));
            hotelName.setGravity(Gravity.START);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                hotelName.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            param.setMargins(0, -5, 0, 0);
            dateInfoText.setLayoutParams(param);
            hotelCheckoutText.setLayoutParams(param);
        }
        Typeface tf = Typeface.createFromAsset(mActivity.getAssets(), fontPath);
        titleFace = Typeface.createFromAsset(mActivity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(mActivity.getAssets(), fontText);
        hotelCountText.setTypeface(tf);
        hotelHeaderText.setTypeface(textFace);
        dealsNearText.setTypeface(tf);
        hotelName.setTypeface(titleFace);
        destination.setTypeface(titleFace);
        dateInfoText.setTypeface(titleFace);
        travellerHeaderText.setTypeface(titleFace);
        travellerInfoText.setTypeface(titleFace);
        searchHotelButton.setTypeface(tf);
        hotelCheckoutText.setTypeface(titleFace);
        errorView = (RelativeLayout) view.findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) view.findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(titleFace);
        hotelCitySearchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, HotelSearchPanel.class);
                mActivity.startActivityForResult(intent, SEARCH_FROM_AIRPORT);
            }
        });
        hotelDatesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(mActivity, CalendarDisplayActivity.class);
                it.putExtra(Constants.TRIP_TYPE, 2);
                it.putExtra(Constants.SELECTED_DEPARTURE_DATE, hotelCheckInDate);
                it.putExtra(Constants.SELECTED_RETURN_DATE, hotelCheckOutDate);
                it.putExtra(Constants.IS_DEPARTURE_SELECTED, true);
                it.putExtra(Constants.IS_HOTEL_DATE_SELECTED, true);
                mActivity.startActivityForResult(it, ROUND_TRIP);
            }
        });
        roomPassengerArrayList = new ArrayList<>();
        adult_count_list.clear();
        childList.clear();
        if (roomPassengerArrayList.size() != 0) {
            checkPassengerCount();
        } else {
            FhRoomPassengerDetailsBean fhPassengerDetailsBean = new FhRoomPassengerDetailsBean();
            fhPassengerDetailsBean.setAdultCnt(2);
            fhPassengerDetailsBean.setChildCnt(0);
            fhPassengerDetailsBean.setInfantCnt(0);
            fhPassengerDetailsBean.getChildAgeArray().add(2);
            fhPassengerDetailsBean.getChildAgeArray().add(2);
            fhPassengerDetailsBean.getChildAgeArray().add(2);
            fhPassengerDetailsBean.getChildAgeArray().add(2);
            fhPassengerDetailsBean.getChildAgeArray().add(2);
            fhPassengerDetailsBean.setExpanded(false);
            roomPassengerArrayList.add(fhPassengerDetailsBean);
            travellerInfoText.setText(1 + " " + getString(R.string.label_search_page_room) + " , " + totalPassengerCount + " " + getString(R.string.label_search_flight_travellers));
        }
        refreshBackground();
        travellerDetailsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTravellerDialog = new AddTravellerDialog(mActivity, roomPassengerArrayList, cabinType, isClassTypeNeed);
                addTravellerDialog.setCancelable(true);
                addTravellerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (addTravellerDialog != null) {
                            if (addTravellerDialog.roomPassengerArrayList.size() != 0) {
                                int adultCnt = 0;
                                int childCnt = 0;
                                int infantCnt = 0;
                                for (int i = 0; i < roomPassengerArrayList.size(); i++) {
                                    adultCnt = adultCnt + roomPassengerArrayList.get(i).getAdultCnt();
                                    childCnt = childCnt + roomPassengerArrayList.get(i).getChildCnt();
                                    infantCnt = infantCnt + roomPassengerArrayList.get(i).getInfantCnt();
                                }
                                totalAdultCnt = adultCnt;
                                totalChildCnt = childCnt;
                                totalPassengerCount = adultCnt + childCnt;
                                cabinType = addTravellerDialog.classTypeCode;
                                String roomsCountText = "";
                                String adtCountText = "";
                                String chdCountText = "";
                                if (addTravellerDialog.roomPassengerArrayList.size() > 1) {
                                    roomsCountText = getString(R.string.label_multiple_rooms);
                                } else {
                                    roomsCountText = getString(R.string.label_search_page_room);
                                }
                                if (totalPassengerCount > 1) {
                                    adtCountText = getString(R.string.label_search_flight_travellers);
                                } else {
                                    adtCountText = getString(R.string.label_add_traveller);
                                }
                                travellerInfoText.setText(roomPassengerArrayList.size() + " " + roomsCountText + " , " + totalPassengerCount + " " + adtCountText);
                            }
                        }
                    }
                });
                addTravellerDialog.show();
                Window window = addTravellerDialog.getWindow();
                int width = (int) (mActivity.getResources().getDisplayMetrics().widthPixels * 1.0);
                int height = (int) (mActivity.getResources().getDisplayMetrics().heightPixels * 0.85);
                if (window != null) {
                    window.setLayout(width, height);
                }
            }
        });
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter("com.flyin.bookings.ERROR_MESSAGE");
        mActivity.registerReceiver(myReceiver, intentFilter);
        searchHotelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
//                Intent intent = new Intent(getActivity(), HotelConfirmationScreen.class);
//                intent.putExtra("BOOKING_ID", "B2C238543");
//                intent.putExtra(Constants.USER_EMAIL, pref.getString(Constants.MEMBER_EMAIL, ""));
//                intent.putExtra(Constants.USER_FULL_NAME, "Raghavendra Dabbidi");
//                startActivity(intent);
                if (hotelName.getText().toString().isEmpty()) {
                    displayErrorMessage(getString(R.string.hotel_name_error));
                    return;
                }
                if (TextUtils.isEmpty(hotelCheckInDate)) {
                    displayErrorMessage(getString(R.string.check_in_error));
                    return;
                }
                if (hotelCheckInDate.equalsIgnoreCase(hotelCheckOutDate)) {
                    displayErrorMessage(getString(R.string.hotel_same_date_error));
                    return;
                }
                if (checkDateDiff() > 30) {
                    displayErrorMessage(mActivity.getString(R.string.more_than_30_night_error));
                    return;
                }
                handleHotelSearch();
            }
        });
        dealsNearMeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealsNearMeClicked = true;
                searchCount = 0;
                searchType = "CITY";
                if (latitude == 0.0 && longitude == 0.0) {
                    checkLatLongValues();
                } else {
                    String jsn = Json(false);
                    if (jsn != null) {
                        handleSearchRequestDeals(jsn);
                    }
                }
            }
        });
        SharedPreferences sharedpreferences = mActivity.getSharedPreferences(Constants.FILTERPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor edtr = sharedpreferences.edit();
        edtr.clear();
        edtr.apply();
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        if (!pref.getString("no_hotel_msg", "").isEmpty()) {
            Utils.printMessage(TAG, "No Hotels Found with Hotel Search ===== ");
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("no_hotel_msg", "");
            editor.apply();
            cityId = pref.getString("selected_city_id", "");
            hotelCheckInDate = pref.getString("first_check_date", "");
            hotelCheckOutDate = pref.getString("first_check_out", "");
            hotelCityName = pref.getString("selected_city_name", "");
            searchType = "CITY";
            checkDateDiff();
            String jsn = makeJson();
            if (jsn != null) {
                handleSearchRequest_(jsn);
            }
            serach_jsn = noHotelJson();
        }
        return view;
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.ERROR_MESSAGE")) {
                String type = "";
                if (intent.hasExtra("type")) {
                    type = intent.getStringExtra("type");
                }
                if (type.equalsIgnoreCase("success")) {
                    displaySuccessMessage(intent.getStringExtra("message"), true);
                } else {
                    displaySuccessMessage(intent.getStringExtra("message"), false);
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.unregisterReceiver(myReceiver);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == mActivity.RESULT_OK) {
                if (requestCode == SEARCH_FROM_AIRPORT) {
                    if (data != null) {
                        String seledctedCode = data.getStringExtra(Constants.SELECTED_CITY_CODE);
                        String seledctedCity = data.getStringExtra(Constants.SELECTED_CITY);
                        String selectedHotelId = data.getStringExtra(Constants.SELECTED_HOTEL_ID);
                        String searchLang = data.getStringExtra(Constants.SEARCH_LANG);
                        String cityName = data.getStringExtra(Constants.SELECTED_HOTEL_NAME);
                        cityId = seledctedCode;
                        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("selected_city_id", cityId);
                        editor.putString("selected_city_name", seledctedCity);
                        editor.apply();
                        hotelName.setText(seledctedCity);
                        Utils.printMessage(TAG, "seledctedCity " + seledctedCity + " cityID :: " + cityId);
                        hotelId = selectedHotelId;
                        searchType = data.getStringExtra(Constants.SERACH_TYPE);
                    }
                } else if (requestCode == ROUND_TRIP) {
                    if (data != null) {
                        int day = data.getExtras().getInt(Constants.DEPARTURE_DAY, 0);
                        int month = data.getExtras().getInt(Constants.DEPARTURE_MONTH, 0);
                        int year = data.getExtras().getInt(Constants.DEPARTURE_YEAR, 0);
                        hotelCheckInDate = year + "-" + month + "-" + day;
                        from_date = Utils.convertToJourneyDate(hotelCheckInDate, mActivity);
                        String checkInDate = Utils.formatDateToUserDateFormat(day + "-" + month + "-" + year, mActivity);
                        day = data.getExtras().getInt(Constants.RETURN_DAY, 0);
                        month = data.getExtras().getInt(Constants.RETURN_MONTH, 0);
                        year = data.getExtras().getInt(Constants.RETURN_YEAR, 0);
                        hotelCheckOutDate = year + "-" + month + "-" + day;
                        to_date = Utils.convertToJourneyDate(hotelCheckOutDate, mActivity);
                        String checkOutDate = Utils.formatDateToUserDateFormat(day + "-" + month + "-" + year, mActivity);
                        dateInfoText.setText(checkInDate);
                        hotelCheckoutText.setText(checkOutDate);
                        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("first_check_date", hotelCheckInDate);
                        editor.putString("first_check_out", hotelCheckOutDate);
                        editor.apply();
                        if (!TextUtils.isEmpty(hotelCheckInDate)) {
                            destination.setText(getString(R.string.label_hotel_search_check_in) + " - " + getString(R.string.label_hotel_search_check_out));
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    /**
     * Adding number of Rooms into Rooms Object
     **/
    private JSONArray addRoomsArray(int roomSize, ArrayList<FhRoomPassengerDetailsBean> passengerList) {
        occupancy_adult_count_list.clear();
        occupancy_child_count_list.clear();
        JSONArray rmArr = new JSONArray();
        int noOfAdults = 0;
        int noOfChildrens = 0;
        if (roomSize == 1) {
            noOfAdults = passengerList.get(0).getAdultCnt();
            noOfChildrens = passengerList.get(0).getChildCnt();
        } else {
            for (int i = 0; i < roomSize; i++) {
                int occupancy = passengerList.get(i).getAdultCnt() + passengerList.get(i).getChildCnt();
                if (noOfAdults < occupancy) {
                    noOfAdults = occupancy;
                }
            }
        }
        int passengerID = 0;
        for (int i = 0; i < roomSize; i++) {
            JSONObject roomDetailsObj = new JSONObject();
            JSONArray paxArr = new JSONArray();
            ArrayList<String> age_child = new ArrayList<>();
            try {
                totalAdultCnt = noOfAdults;
                totalChildCnt = noOfChildrens;
                roomDetailsObj.put("na", noOfAdults);
                roomDetailsObj.put("nc", noOfChildrens);
                for (int j = 0; j < noOfAdults; j++) {
                    JSONObject paxDetailObj = new JSONObject();
                    passengerID++;
                    paxDetailObj.put("age", 45);
                    paxDetailObj.put("id", passengerID);
                    paxArr.put(paxDetailObj);
                }
                occupancy_adult_count_list.add(String.valueOf(noOfAdults));
                if (noOfChildrens > 0) {
                    for (int j = 0; j < noOfChildrens; j++) {
                        JSONObject paxDetailObj = new JSONObject();
                        passengerID++;
                        paxDetailObj.put("age", passengerList.get(i).getChildAgeArray().get(j));
                        paxDetailObj.put("id", passengerID);
                        paxArr.put(paxDetailObj);

                        age_child.add(String.valueOf(passengerList.get(i).getChildAgeArray().get(j)));
                    }
                }
                occupancy_child_count_list.put("room" + String.valueOf(i + 1), age_child);
                roomDetailsObj.put("pax", paxArr);
            } catch (JSONException e) {
            }
            rmArr.put(roomDetailsObj);
        }
        for (int i = 0; i < roomSize; i++) {
            int childCount = 0;
            ArrayList<String> age_child = new ArrayList<>();
            childCount = passengerList.get(i).getChildCnt();
            adult_count_list.add(String.valueOf(passengerList.get(i).getAdultCnt()));
            if (childCount > 0) {
                childList.add(String.valueOf(passengerList.get(i).getChildCnt()));
            }
            if (childCount > 0) {
                for (int j = 0; j < childCount; j++) {
                    age_child.add(String.valueOf(passengerList.get(i).getChildAgeArray().get(j)));
                }
            }
            child_count_list.put("room" + String.valueOf(i + 1), age_child);
        }
        return rmArr;
    }

    /**
     * Displaying Error message
     **/
    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setBackgroundResource(R.color.error_message_background);
            errorView.setTranslationY(-90);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(mActivity.getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (errorView != null) {
                            errorView.animate().alpha(0)
                                    .setDuration(mActivity.getResources().getInteger(android.R.integer.config_shortAnimTime));
                        }
                    } catch (Exception e) {
                    }
                }
            }, 2000);
        }
    }

    private int checkDateDiff() {
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(hotelCheckInDate);
            Date date2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(hotelCheckOutDate);
            night_count = (int) ((date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000));
            return night_count;
        } catch (Exception e) {
        }
        return 0;
    }

    private int checkSecondDateDiff(String inDate, String outDate) {
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(inDate);
            Date date2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(outDate);
            int night_count = (int) ((date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000));
            return night_count;
        } catch (Exception e) {
        }
        return 0;
    }

    private void showLoading() {
        View loadingView = layoutInflater.inflate(R.layout.activity_loading_screen, null);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView waitText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setTypeface(textFace);
        waitText.setTypeface(textFace);
        loadingText.setText(mActivity.getResources().getString(R.string.label_hold_message));
        waitText.setText(mActivity.getResources().getString(R.string.searching_deals_message));
        if (isArabicLang) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            waitText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.removeAllViews();
        loadingViewLayout.addView(loadingView);
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "data is :: " + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            closeProgress();
            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
            if (errorView != null) {
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(titleFace);
                errorDescriptionText.setTypeface(titleFace);
                searchButton.setTypeface(titleFace);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadingViewLayout.removeAllViews();
                        closeProgress();
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            }
        } else if (serviceType == SEARCH_HOTEL_DETAILS) {
            try {
                if (data.equals("[]")) {
                    closeProgress();
                    final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                    if (errorView != null) {
                        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                        errorText = (TextView) errorView.findViewById(R.id.error_text);
                        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                        searchButton = (Button) errorView.findViewById(R.id.search_button);
                        errorText.setTypeface(titleFace);
                        errorDescriptionText.setTypeface(titleFace);
                        searchButton.setTypeface(titleFace);
                        searchButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadingViewLayout.removeAllViews();
                                closeProgress();
                            }
                        });
                        loadErrorType(Constants.WRONG_ERROR_PAGE);
                        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        loadingViewLayout.addView(errorView);
                    }
                    if (dealsNearMeClicked) {
                        dealsNearMeClicked = false;
                        hotelCheckInDate = "";
                        hotelCheckOutDate = "";
                        dateInfoText.setText(getString(R.string.label_hotel_search_check_in));
                        hotelCheckoutText.setText(getString(R.string.label_hotel_search_check_out));
                    }
                } else {
                    hotelList = new ArrayList<>();
                    JSONArray arr = new JSONArray(data);
                    for (int i = 0; i < arr.length(); i++) {
                        try {
                            HotelModel model = new HotelModel();
                            JSONObject obj = arr.optJSONObject(i);
                            String error = obj.optString("error");
                            if (error.equalsIgnoreCase("data not found")) {
                                errorlist.add(i);
                                continue;
                            }
                            model.setHuid(obj.optString("huid"));
                            // Utils.printMessage(TAG, " huid ::" + obj.optString("huid"));
                            JSONObject hotel = obj.optJSONObject("hotel");
                            JSONObject bacickInfo = hotel.optJSONObject("basicInfo");
                            JSONObject geocode = bacickInfo.optJSONObject("geoCode");
                            model.setLatitude(geocode.optString("latitude"));
                            model.setLongitude(geocode.optString("longitude"));
                            JSONObject distric = bacickInfo.optJSONObject("district");
                            JSONObject contact = bacickInfo.optJSONObject("contactInfo");
                            model.setHemail(contact.optString("email"));
                            model.setHfaxCode(contact.optString("faxCode"));
                            model.setHfaxNo(contact.optString("fax"));
                            model.setHphoneCode(contact.optString("phoneCode"));
                            model.setHphoneNo(contact.optString("phone"));
                            JSONObject city = bacickInfo.optJSONObject("city");
                            model.setDistric(distric.getString("name"));
                            model.setCity(city.optString("name"));
                            model.setCityId(city.optString("id"));
                            JSONObject chainName = bacickInfo.optJSONObject("chain");
                            model.setChainName(chainName.getString("name"));
                            JSONObject hotelType = bacickInfo.optJSONObject("hotelType");
                            if (hotelType != null) {
                                Iterator<String> hotel_key = hotelType.keys();
                                String hotel_type = "";
                                while (hotel_key.hasNext()) {
                                    String key = hotel_key.next();
                                    try {
                                        if (hotel_type.length() > 0) {
                                            hotel_type = hotel_type + ",";
                                        }
                                        hotel_type = hotel_type + hotelType.getString(key);
                                    } catch (JSONException e) {
                                    } catch (Exception e) {
                                    }
                                }
                                model.setHotel_type(hotel_type);
                            }
                            model.setRoom_count("" + room_count);
                            JSONObject country = bacickInfo.optJSONObject("country");
                            model.setCountry(country.optString("name"));
                            model.setImage(bacickInfo.optString("mainImage"));
                            model.setStar(bacickInfo.optString("starRating"));
                            if (bacickInfo.optString("rank") == null || bacickInfo.optString("rank").equals("null"))
                                model.setRank(999999);
                            else
                                model.setRank(Integer.parseInt(bacickInfo.optString("rank")));
                            model.setFullAddress(bacickInfo.optString("addressText"));
                            model.setHna(bacickInfo.optString("name"));
                            JSONObject imageObj = hotel.optJSONObject("images");
                            JSONArray imageArr = imageObj.optJSONArray("others");
                            String[] imagelist = new String[imageArr.length()];
                            for (int j = 0; j < imageArr.length(); j++) {
                                imagelist[j] = imageArr.getString(j);
                            }
                            model.setImagelist(imagelist);
                            JSONObject policy = hotel.optJSONObject("policy");
                            String cancel_policy = policy.optString("cancellationPolicy");
                            if (cancel_policy != null) {
                                model.setCancel_policy(cancel_policy);
                            }
                            String child_policy = policy.optString("childPolicy");
                            if (child_policy != null) {
                                model.setChild_policy(child_policy);
                            }
                            String pet_policy = policy.optString("petPolicy");
                            if (pet_policy != null) {
                                model.setPet(pet_policy);
                            }
                            String check_out = policy.optString("check_out");
                            if (check_out != null) {
                                model.setCheck_out(check_out);
                            }
                            String check_in = policy.optString("check_in");
                            if (check_in != null) {
                                model.setCheck_in(check_in);
                            }
                            JSONObject description = hotel.optJSONObject("descriptions");
                            String lobby = description.optString("lobby");
                            if (lobby != null) {
                                model.setLobby(lobby);
                            }
                            String exterior = description.optString("exterior");
                            if (exterior != null) {
                                model.setExterior(exterior);
                            }
                            String rooms = description.optString("aboutHotel");
                            if (rooms != null) {
                                model.setRooms(rooms);
                            }
                            JSONObject facilities = hotel.optJSONObject("facilities");
                            JSONObject activity = facilities.optJSONObject("activities");
                            int k = 0;
                            if (activity != null) {
                                Iterator<String> activity_key = activity.keys();
                                String[] activitylist = new String[activity.length()];
                                while (activity_key.hasNext()) {
                                    String key = activity_key.next();
                                    try {
                                        String value = activity.getString(key);
                                        activitylist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setActivity(activitylist);
                            }
                            JSONObject general = facilities.optJSONObject("general");
                            int l = 0;
                            if (general != null) {
                                Iterator<String> activity_key = general.keys();
                                String[] generalList = new String[general.length()];
                                while (activity_key.hasNext()) {
                                    String key = activity_key.next();
                                    try {
                                        String value = general.getString(key);
                                        generalList[l] = value;
                                    } catch (JSONException e) {
                                    }
                                    l++;
                                }
                                model.setTopFeatures(generalList);
                            }
                            JSONObject service = facilities.optJSONObject("services");
                            if (service != null) {
                                Iterator<String> service_key = service.keys();
                                String[] servicelist = new String[service.length()];
                                k = 0;
                                while (service_key.hasNext()) {
                                    String key = service_key.next();
                                    try {
                                        String value = service.getString(key);
                                        servicelist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setService(servicelist);
                            }
                            JSONObject activities = facilities.optJSONObject("activities");
                            if (activities != null) {
                                Iterator<String> general_key = activities.keys();
                                String[] generallist = new String[activities.length()];
                                k = 0;
                                while (general_key.hasNext()) {
                                    String key = general_key.next();
                                    try {
                                        String value = activities.getString(key);
                                        generallist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setGeneral(generallist);
                            }
                            hotelList.add(model);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    errorlist.clear();
                    hotelCityName = hotelList.get(0).getCity() + ", " + hotelList.get(0).getCountry();
                    Utils.printMessage(TAG, "hotel city name ::" + hotelCityName);
                    SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    if (hotelList.size() > 0) {
                        Fragment fragment;
                        Bundle bundle = new Bundle();
                        Singleton.getInstance().roomPassengerInfoList.clear();
                        Singleton.getInstance().roomPassengerInfoList = new ArrayList<>();
                        for (int i = 0; i < roomPassengerArrayList.size(); i++) {
                            FhRoomPassengerDetailsBean bean1 = roomPassengerArrayList.get(i);
                            FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
                            bean.setAdultCnt(bean1.getAdultCnt());
                            bean.setChildAge(bean1.getChildAge());
                            bean.setAge(bean1.getAge());
                            bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
                            bean.setChildCnt(bean1.getChildCnt());
                            bean.setClassType(bean1.getClassType());
                            bean.setInfantCnt(bean1.getInfantCnt());
                            Singleton.getInstance().roomPassengerInfoList.add(bean);
                        }
                        bundle.putBoolean("isFirstPage", true);
                        editor.putBoolean("serviceCall", true).apply();
                        if (searchType.equalsIgnoreCase("CITY") || searchType.equalsIgnoreCase("AIRPORT")) {
                            fragment = new HotelListFragment();
                            bundle.putString("cityId", cityId);
                            bundle.putString("hotelId", hotelId);
                            bundle.putInt("totalPassengerCount", totalPassengerCount);
                            bundle.putString("night_count", String.valueOf(night_count));
                            bundle.putString("checkin", Utils.convertToJourneyDate(hotelCheckInDate, mActivity) + " - " + Utils.convertToJourneyDate(hotelCheckOutDate, mActivity));
                            bundle.putString("hotelCheckInDate", hotelCheckInDate);
                            bundle.putString("hotelCheckOutDate", hotelCheckOutDate);
                            bundle.putString("country", hotelList.get(0).getCountry());
                            String selectedCity = "";
                            if (dealsNearMeClicked) {
                                selectedCity = mActivity.getString(R.string.near_by_hotels);
                                dealsNearMeClicked = false;
                                hotelCheckInDate = "";
                                hotelCheckOutDate = "";
                                dateInfoText.setText(getString(R.string.label_hotel_search_check_in));
                                hotelCheckoutText.setText(getString(R.string.label_hotel_search_check_out));
                            } else {
                                if (!(hotelName.getText().toString()).isEmpty()) {
                                    selectedCity = hotelName.getText().toString();
                                } else {
                                    selectedCity = hotelCityName;
                                }
                                Utils.printMessage(TAG, "hotelCityName :: " + hotelCityName + " getText :: " + hotelName.getText().toString());
                            }
                            bundle.putString("jsn", serach_jsn);
                            bundle.putString("city", selectedCity);
                            bundle.putParcelableArrayList("list", hotelList);
                            bundle.putParcelableArrayList("final_list", hotelList);
                            bundle.putStringArrayList("adults_list", new ArrayList<>(adult_count_list));
                            bundle.putStringArrayList("childs_list", new ArrayList<>(childList));
                            bundle.putSerializable("children_list", new HashMap(child_count_list));
                            bundle.putSerializable("tripAdvisor_array", tripAdvisorAry);
                            bundle.putInt("adult_count", totalAdultCnt);
                            bundle.putInt("child_count", totalChildCnt);
                            bundle.putBoolean("isFirstPage", true);
                            bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                            Utils.printMessage(TAG, "Executing city and airport");
                        } else {
                            editor.putBoolean("serviceCall", true).apply();
                            bundle.putParcelable("model", hotelList.get(0));
                            Utils.printMessage(TAG, "Room cnt : " + hotelList.get(0).getRoom_count());
                            bundle.putString("header_date", from_date + " - " + to_date);
                            bundle.putInt("adult_count", totalAdultCnt);
                            bundle.putInt("child_count", totalChildCnt);
                            bundle.putBoolean("isMainPage", true);
                            bundle.putBoolean("isFirstPage", true);
                            bundle.putStringArrayList("adults_list", new ArrayList<>(adult_count_list));
                            bundle.putStringArrayList("childs_list", new ArrayList<>(childList));
                            bundle.putSerializable("children_list", new HashMap(child_count_list));
                            bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                            Utils.printMessage(TAG, "Executing hotel");
                            fragment = new HotelDetailFragment();
                        }
                        fragment.setArguments(bundle);
                        ((MainSearchActivity) mActivity).pushFragment(fragment);
                        loadingViewLayout.removeAllViews();
                        closeProgress();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (serviceType == SEARCH_HOTEL_DEALSNEARBY) {
            String jsn = "";
            Utils.printMessage(TAG, "SEARCH_HOTEL_DEALSNEARBY :: " + data);
            try {
                JSONObject obj = new JSONObject(data);
                JSONObject searchRSObj = obj.optJSONObject("searchRS");
                if (searchRSObj.has("err")) {
                    closeProgress();
                    try {
                        Utils.printMessage(TAG, "search count :: " + searchCount);
                        if (searchCount == 1) {
                            if (dealsNearMeClicked) {
                                searchType = "CITY";
                                jsn = Json(false);
                                hotelCheckInDate = "";
                                hotelCheckOutDate = "";
                                dateInfoText.setText(getString(R.string.label_hotel_search_check_in));
                                hotelCheckoutText.setText(getString(R.string.label_hotel_search_check_out));
                            } else {
                                jsn = Json(true);
                                searchType = "CITY";
                            }
                        } else {
                            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                            errorText = (TextView) errorView.findViewById(R.id.error_text);
                            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                            searchButton = (Button) errorView.findViewById(R.id.search_button);
                            errorText.setTypeface(titleFace);
                            errorDescriptionText.setTypeface(titleFace);
                            searchButton.setTypeface(titleFace);
                            searchButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    loadingViewLayout.removeAllViews();
                                }
                            });
                            loadErrorType(Constants.RESULT_ERROR);
                            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            loadingViewLayout.addView(errorView);

                        }
                    } catch (Exception e) {
                    }
                } else {
                    JSONArray arr = searchRSObj.getJSONArray("hotel");
                    JSONObject rcObj = arr.optJSONObject(0);
                    JSONArray rcArr = rcObj.getJSONArray("rc");
                    dealsNearCityId = rcArr.optJSONObject(0).optJSONObject("ht").optString("cid");
                    serach_jsn = Json(false);
                    handleSearchRequest_(makeJson());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (serviceType == GET_CITY) {
                listdata.clear();
                modelListdata.clear();
                Utils.printMessage(TAG, "GET_CITY" + data);
                if (data != null) {
                    try {
                        JSONObject object = new JSONObject(data);
                        JSONArray cityObject = object.optJSONArray("cities");
                        for (int i = 0; i < cityObject.length(); i++) {
                            CityModel model = new CityModel();
                            JSONObject payloadObject = cityObject.optJSONObject(i);
                            JSONObject payload = payloadObject.optJSONObject("payload");
                            String city_name = payload.optString("cityName");
                            String city_id = payload.optString("cityId");
                            String country = payload.optString("countryName");
                            listdata.add(city_name + ", " + country);
                            model.setCityId(city_id);
                            model.setCityName(city_name);
                            model.setCountryName(country);
                            model.setHotelId("");
                            model.setType("CITY");
                            modelListdata.add(model);
                        }
                        JSONArray hotelObject = object.optJSONArray("hotels");
                        for (int i = 0; i < hotelObject.length(); i++) {
                            CityModel model = new CityModel();
                            JSONObject payloadObject = hotelObject.optJSONObject(i);
                            JSONObject payload = payloadObject.optJSONObject("payload");
                            String hotel_name = payload.optString("hotelName");
                            String city_name = payload.optString("cityName");
                            String city_id = payload.optString("cityId");
                            String country = payload.optString("countryName");
                            String hotel_id = payload.optString("uniqueId");
                            listdata.add(city_name + ", " + country);
                            model.setCityId(city_id);
                            model.setCityName(city_name);
                            model.setCountryName(country);
                            model.setHotelId(hotel_id);
                            model.setType("HOTEL");
                            modelListdata.add(model);
                        }
                        JSONArray airportObject = object.optJSONArray("airports");
                        for (int i = 0; i < airportObject.length(); i++) {
                            CityModel model = new CityModel();
                            JSONObject payloadObject = airportObject.optJSONObject(i);
                            String aName = payloadObject.getString("text"); //airport name
                            JSONObject payload = payloadObject.optJSONObject("payload");
                            String airportCode = payload.optString("airportCode");
                            String cityId = payload.optString("cityId");
                            listdata.add(aName);
                            model.setCityId(cityId);
                            model.setCityName(aName);
                            model.setCountryName(aName);
                            model.setHotelId("");
                            model.setType("AIRPORT");
                            modelListdata.add(model);
                        }
                        adapter = new ArrayAdapter<>(mActivity, R.layout.search_text, listdata);
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                    } catch (Exception e) {
                    }
                }
            } else if (serviceType == SEARCH_HOTEL) {
                Utils.printMessage(TAG, "SEARCH_HOTEL :: " + data);
                searchCount++;
                String resultData = parsedaa(data);
                Utils.printMessage(TAG, "check :: " + resultData);
                if (!resultData.equalsIgnoreCase("fail") && !resultData.equalsIgnoreCase("success") && !resultData.equalsIgnoreCase("")) {
                    if (resultData != null) {
                        handleSearchRequest(resultData);
                    }
                } else if (resultData.equalsIgnoreCase("success")) {
                    if (dealsNearMeClicked) {
                        searchType = "CITY";
                        hotelCheckInDate = "";
                        hotelCheckOutDate = "";
                        dateInfoText.setText(getString(R.string.label_hotel_search_check_in));
                        hotelCheckoutText.setText(getString(R.string.label_hotel_search_check_out));
                    }
                    final String jsn = makeJson();
                    final String jsonTrip = hotelJsonTripAdvisor();
                    if (jsonTrip != null) {
                        boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                        if (isInternetPresent) {
                            new HTTPAsync(mActivity, HotelSearchFragment.this, Constants.TRIP_ADVISOR_URL, "", jsonTrip,
                                    SEARCH_HOTEL_TRIP, HTTPAsync.METHOD_POST).execute();
                        } else {
                            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                            errorText = (TextView) errorView.findViewById(R.id.error_text);
                            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                            searchButton = (Button) errorView.findViewById(R.id.search_button);
                            errorText.setTypeface(titleFace);
                            errorDescriptionText.setTypeface(titleFace);
                            searchButton.setTypeface(titleFace);
                            searchButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                                    if (isInternetPresent) {
                                        loadingViewLayout.removeAllViews();
                                        new HTTPAsync(mActivity, HotelSearchFragment.this, Constants.TRIP_ADVISOR_URL, "",
                                                jsonTrip, SEARCH_HOTEL_TRIP, HTTPAsync.METHOD_POST).execute();
                                    }
                                }
                            });
                            loadErrorType(Constants.NETWORK_ERROR);
                            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            loadingViewLayout.addView(errorView);
                        }
                    }
                }
            } else if (serviceType == SEARCH_HOTEL_DETAILS) {
                Utils.printMessage(TAG, "Secondary Search ::" + data);
                try {
                    JSONArray arr = new JSONArray(data);
                    for (int i = 0; i < arr.length(); i++) {
                        try {
                            HotelModel model = hotelList.get(i);
                            JSONObject obj = arr.optJSONObject(i);
                            String error = obj.optString("error");
                            if (error.equalsIgnoreCase("data not found")) {
                                errorlist.add(i);
                                final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                                errorText = (TextView) errorView.findViewById(R.id.error_text);
                                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                                searchButton = (Button) errorView.findViewById(R.id.search_button);
                                errorText.setTypeface(titleFace);
                                errorDescriptionText.setTypeface(titleFace);
                                searchButton.setTypeface(titleFace);
                                searchButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        loadingViewLayout.removeAllViews();
                                    }
                                });
                                loadErrorType(Constants.RESULT_ERROR);
                                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT,
                                        ViewGroup.LayoutParams.MATCH_PARENT));
                                loadingViewLayout.addView(errorView);
                                continue;
                            }
                            JSONObject hotel = obj.optJSONObject("hotel");
                            JSONObject bacickInfo = hotel.optJSONObject("basicInfo");
                            JSONObject geocode = bacickInfo.optJSONObject("geoCode");
                            model.setLatitude(geocode.optString("latitude"));
                            model.setLongitude(geocode.optString("longitude"));
                            JSONObject distric = bacickInfo.optJSONObject("district");
                            JSONObject contact = bacickInfo.optJSONObject("contactInfo");
                            model.setHemail(contact.optString("email"));
                            model.setHfaxCode(contact.optString("faxCode"));
                            model.setHfaxNo(contact.optString("fax"));
                            model.setHphoneCode(contact.optString("phoneCode"));
                            model.setHphoneNo(contact.optString("phone"));
                            JSONObject city = bacickInfo.optJSONObject("city");
                            model.setDistric(distric.getString("name"));
                            model.setCity(city.optString("name"));
                            model.setCityId(city.optString("id"));
                            JSONObject chainName = bacickInfo.optJSONObject("chain");
                            model.setChainName(chainName.getString("name"));
                            JSONObject hotelType = bacickInfo.optJSONObject("hotelType");
                            if (hotelType != null) {
                                Iterator<String> hotel_key = hotelType.keys();
                                String hotel_type = "";
                                while (hotel_key.hasNext()) {
                                    String key = hotel_key.next();
                                    try {
                                        if (hotel_type.length() > 0) {
                                            hotel_type = hotel_type + ",";
                                        }
                                        hotel_type = hotel_type + hotelType.getString(key);
                                    } catch (JSONException e) {
                                    } catch (Exception e) {
                                    }
                                }
                                model.setHotel_type(hotel_type);
                            }
                            model.setRoom_count("" + room_count);
                            JSONObject country = bacickInfo.optJSONObject("country");
                            model.setCountry(country.optString("name"));
                            model.setImage(bacickInfo.optString("mainImage"));
                            model.setStar(bacickInfo.optString("starRating"));
                            if (bacickInfo.optString("rank") == null || bacickInfo.optString("rank").equals("null"))
                                model.setRank(999999);
                            else
                                model.setRank(Integer.parseInt(bacickInfo.optString("rank")));
                            model.setFullAddress(bacickInfo.optString("addressText"));
                            model.setHna(bacickInfo.optString("name"));
                            JSONObject imageObj = hotel.optJSONObject("images");
                            JSONArray imageArr = imageObj.optJSONArray("others");
                            String[] imagelist = new String[imageArr.length()];
                            for (int j = 0; j < imageArr.length(); j++) {
                                imagelist[j] = imageArr.getString(j);
                            }
                            model.setImagelist(imagelist);
                            JSONObject policy = hotel.optJSONObject("policy");
                            String cancel_policy = policy.optString("cancellationPolicy");
                            if (cancel_policy != null) {
                                model.setCancel_policy(cancel_policy);
                            }
                            String child_policy = policy.optString("childPolicy");
                            if (child_policy != null) {
                                model.setChild_policy(child_policy);
                            }
                            String pet_policy = policy.optString("petPolicy");
                            if (pet_policy != null) {
                                model.setPet(pet_policy);
                            }
                            String check_out = policy.optString("check_out");
                            if (check_out != null) {
                                model.setCheck_out(check_out);
                            }
                            String check_in = policy.optString("check_in");
                            if (check_in != null) {
                                model.setCheck_in(check_in);
                            }
                            JSONObject description = hotel.optJSONObject("descriptions");
                            String lobby = description.optString("lobby");
                            if (lobby != null) {
                                model.setLobby(lobby);
                            }
                            String exterior = description.optString("exterior");
                            if (exterior != null) {
                                model.setExterior(exterior);
                            }
                            String rooms = description.optString("aboutHotel");
                            if (rooms != null) {
                                model.setRooms(rooms);
                            }
                            JSONObject facilities = hotel.optJSONObject("facilities");
                            JSONObject activity = facilities.optJSONObject("activities");
                            int k = 0;
                            if (activity != null) {
                                Iterator<String> activity_key = activity.keys();
                                String[] activitylist = new String[activity.length()];
                                while (activity_key.hasNext()) {
                                    String key = activity_key.next();
                                    try {
                                        String value = activity.getString(key);
                                        activitylist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setActivity(activitylist);
                            }
                            JSONObject general = facilities.optJSONObject("general");
                            int l = 0;
                            if (general != null) {
                                Iterator<String> activity_key = general.keys();
                                String[] generalList = new String[general.length()];
                                while (activity_key.hasNext()) {
                                    String key = activity_key.next();
                                    try {
                                        String value = general.getString(key);
                                        generalList[l] = value;
                                    } catch (JSONException e) {
                                    }
                                    l++;
                                }
                                model.setTopFeatures(generalList);
                            }
                            JSONObject service = facilities.optJSONObject("services");
                            if (service != null) {
                                Iterator<String> service_key = service.keys();
                                String[] servicelist = new String[service.length()];
                                k = 0;
                                while (service_key.hasNext()) {
                                    String key = service_key.next();
                                    try {
                                        String value = service.getString(key);
                                        servicelist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setService(servicelist);
                            }
                            JSONObject activities = facilities.optJSONObject("activities");
                            if (activities != null) {
                                Iterator<String> general_key = activities.keys();
                                String[] generallist = new String[activities.length()];
                                k = 0;
                                while (general_key.hasNext()) {
                                    String key = general_key.next();
                                    try {
                                        String value = activities.getString(key);
                                        generallist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setGeneral(generallist);
                            }
                            //trip advisor
                            if (!model.getRoomlist().get(0).getFree_cancel_date().isEmpty()) {
                                DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
                                Date dateBefore = null;
                                try {
                                    dateBefore = simpleDateFormat.parse(model.getRoomlist().get(0).getFree_cancel_date());
                                } catch (Exception e) {
                                }
                                SimpleDateFormat finalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                String finalDate = finalFormat.format(dateBefore);
                                String CancellationDateDiff = Utils.flightSearchDateDifference(finalDate, currentDate);
                                model.setBnplDateDiff(Integer.parseInt(CancellationDateDiff));
                            } else {
                                model.setBnplDateDiff(0);
                            }
                            for (int z = 0; z < tripAdvisorAry.size(); z++) {
                                if (model.getHuid().equalsIgnoreCase(tripAdvisorAry.get(z).getHuid())) {
                                    model.setReviewCount(tripAdvisorAry.get(z).getNumReview());
                                    model.setReviewImgUrl(tripAdvisorAry.get(z).getRatingImageUrl());
                                    break;
                                }
                            }

                            hotelList.set(i, model);
                        } catch (Exception e) {
                        }
                    }
                    for (int j = 0; j < errorlist.size(); j++) {
                        hotelList.remove(errorlist.get(j) - j);
                    }
                    errorlist.clear();
                    hotelCityName = hotelList.get(0).getCity() + ", " + hotelList.get(0).getCountry();
                    Utils.printMessage(TAG, "hotel city name ::" + hotelCityName);
                    if (hotelList.size() > 0) {
                        Fragment fragment;
                        Bundle bundle = new Bundle();
                        Singleton.getInstance().roomPassengerInfoList.clear();
                        Singleton.getInstance().roomPassengerInfoList = new ArrayList<>();
                        for (int i = 0; i < roomPassengerArrayList.size(); i++) {
                            FhRoomPassengerDetailsBean bean1 = roomPassengerArrayList.get(i);
                            FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
                            bean.setAdultCnt(bean1.getAdultCnt());
                            bean.setChildAge(bean1.getChildAge());
                            bean.setAge(bean1.getAge());
                            bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
                            bean.setChildCnt(bean1.getChildCnt());
                            bean.setClassType(bean1.getClassType());
                            bean.setInfantCnt(bean1.getInfantCnt());
                            Singleton.getInstance().roomPassengerInfoList.add(bean);
                        }
                        if (searchType.equalsIgnoreCase("CITY") || searchType.equalsIgnoreCase("AIRPORT")) {
                            fragment = new HotelListFragment();
                            bundle.putString("night_count", String.valueOf(night_count));
                            bundle.putString("country", hotelList.get(0).getCountry());
                            String selectedCity = "";
                            if (dealsNearMeClicked) {
                                selectedCity = mActivity.getString(R.string.near_by_hotels);
                                dealsNearMeClicked = false;
                            } else {
                                selectedCity = hotelName.getText().toString();
                            }
                            bundle.putString("city", selectedCity);
                            bundle.putParcelableArrayList("list", hotelList);
                            bundle.putParcelableArrayList("final_list", hotelList);
                            bundle.putStringArrayList("adults_list", new ArrayList<>(adult_count_list));
                            bundle.putStringArrayList("childs_list", new ArrayList<>(childList));
                            bundle.putSerializable("children_list", new HashMap(child_count_list));
                            bundle.putString("checkin", from_date + " - " + to_date);
                            bundle.putSerializable("tripAdvisor_array", tripAdvisorAry);
                            bundle.putInt("adult_count", totalAdultCnt);
                            bundle.putInt("child_count", totalChildCnt);
                            bundle.putBoolean("isFirstPage", true);
                            bundle.putString("hotelCheckInDate", hotelCheckInDate);
                            bundle.putString("hotelCheckOutDate", hotelCheckOutDate);
                            bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                            Utils.printMessage(TAG, "Executing city and airport");
                        } else {
                            bundle.putParcelable("model", hotelList.get(0));
                            Utils.printMessage(TAG, "Room cnt : " + hotelList.get(0).getRoom_count());
                            bundle.putString("header_date", from_date + " - " + to_date);
                            bundle.putInt("adult_count", totalAdultCnt);
                            bundle.putInt("child_count", totalChildCnt);
                            bundle.putBoolean("isMainPage", true);
                            bundle.putBoolean("isFirstPage", true);
                            bundle.putStringArrayList("adults_list", new ArrayList<>(adult_count_list));
                            bundle.putStringArrayList("childs_list", new ArrayList<>(childList));
                            bundle.putSerializable("children_list", new HashMap(child_count_list));
                            bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                            Utils.printMessage(TAG, "Executing hotel");
                            bundle.putString("checkin", Utils.convertToJourneyDate(hotelCheckInDate, mActivity) + " - " + Utils.convertToJourneyDate(hotelCheckOutDate, mActivity));
                            bundle.putString("hotelCheckInDate", hotelCheckInDate);
                            bundle.putString("hotelCheckOutDate", hotelCheckOutDate);
                            fragment = new HotelDetailFragment();
                        }
                        fragment.setArguments(bundle);
                        ((MainSearchActivity) mActivity).pushFragment(fragment);
                        loadingViewLayout.removeAllViews();
                        closeProgress();
                    }
                } catch (JSONException e) {
                } catch (Exception e) {
                }
            } else if (serviceType == GET_CURRENCY_DATA) {
                SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                savedCurrency = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
                Utils.printMessage(TAG, "SAVED CURRENCY :: " + savedCurrency);
                Utils.printMessage(TAG, "DATA:: " + data);
                Gson gson = new Gson();
                CurrencyBean currencyBean;
                try {
                    JSONObject obj = new JSONObject(data);
                    if (obj.has("currencyInfos")) {
                        ArrayList<CurrencyBean> list = new ArrayList<>();
                        JSONArray arr = obj.getJSONArray("currencyInfos");
                        for (int i = 0; i < arr.length(); i++) {
                            if (arr.getJSONObject(i).has("currency")) {
                                currencyBean = new CurrencyBean();
                                currencyBean.setCurrency(arr.getJSONObject(i).getString("currency"));
                                currencyBean.setBufferRate(arr.getJSONObject(i).getString("bufferRate"));
                                currencyBean.setExchangeRate(arr.getJSONObject(i).getString("exchangeRate"));
                                list.add(currencyBean);
                            }
                        }
                        String jsonInString = gson.toJson(list);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString(Constants.ALL_CURRENCY, jsonInString);
                        editor.apply();
                    }
                } catch (Exception e) {
                }
            } else if (serviceType == SEARCH_HOTEL_TRIP) {
                Utils.printMessage(TAG, "Secondary Search trip::" + data);
                try {
                    JSONArray arr = new JSONArray(data);
                    for (int j = 0; j < arr.length(); j++) {
                        if (arr.getJSONObject(j).has("ratingObj")) {
                            TripAdvisorBean tripAdvisorBean = new TripAdvisorBean();
                            tripAdvisorBean.setHuid(arr.getJSONObject(j).get("huid").toString());
                            if (arr.getJSONObject(j).getJSONObject("ratingObj").has("num_reviews"))
                                tripAdvisorBean.setNumReview(arr.getJSONObject(j).getJSONObject("ratingObj").getString("num_reviews"));
                            if (arr.getJSONObject(j).getJSONObject("ratingObj").has("rating_image_url"))
                                tripAdvisorBean.setRatingImageUrl(arr.getJSONObject(j).getJSONObject("ratingObj").getString("rating_image_url"));
                            tripAdvisorAry.add(tripAdvisorBean);
                        }
                    }
                    final String jsn = makeJson();
                    if (jsn != null) {
                        boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                        if (isInternetPresent) {
                            new HTTPAsync(mActivity, HotelSearchFragment.this, Constants.HOTEL_INFO_RQ_URL, "", jsn,
                                    SEARCH_HOTEL_DETAILS, HTTPAsync.METHOD_POST).execute();
                        } else {
                            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                            errorText = (TextView) errorView.findViewById(R.id.error_text);
                            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                            searchButton = (Button) errorView.findViewById(R.id.search_button);
                            errorText.setTypeface(titleFace);
                            errorDescriptionText.setTypeface(titleFace);
                            searchButton.setTypeface(titleFace);
                            searchButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                                    if (isInternetPresent) {
                                        loadingViewLayout.removeAllViews();
                                        new HTTPAsync(mActivity, HotelSearchFragment.this, Constants.HOTEL_INFO_RQ_URL,
                                                "", jsn, SEARCH_HOTEL_DETAILS, HTTPAsync.METHOD_POST).execute();
                                    }
                                }
                            });
                            loadErrorType(Constants.NETWORK_ERROR);
                            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            loadingViewLayout.addView(errorView);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        if (loadingViewLayout != null)
            loadingViewLayout.removeAllViews();
    }

    private void handleHotelSearch() {
        String jsn = null;
        if (searchType == null) {
            return;
        }
        if (searchType.equalsIgnoreCase("CITY")) {
            if (cityId != null) {
                dealsNearMeClicked = false;
                jsn = makeJson();
                serach_jsn = Json(true);
                if (jsn != null) {
                    handleSearchRequest_(jsn);
                }
            }
        } else if (searchType.equalsIgnoreCase("HOTEL")) {
            Utils.printMessage(TAG, "hotelId :: " + hotelId);
            if (hotelId != null) {
                Bundle bundle = new Bundle();
                SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Constants.IS_ROOM_INFO_UPDATED, false).apply();
                Utils.printMessage(TAG, "hotelId :: " + hotelId);
                Singleton.getInstance().roomPassengerInfoList.clear();
                Singleton.getInstance().roomPassengerInfoList = new ArrayList<>();
                for (int i = 0; i < roomPassengerArrayList.size(); i++) {
                    FhRoomPassengerDetailsBean bean1 = roomPassengerArrayList.get(i);
                    FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
                    bean.setAdultCnt(bean1.getAdultCnt());
                    bean.setChildAge(bean1.getChildAge());
                    bean.setAge(bean1.getAge());
                    bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
                    bean.setChildCnt(bean1.getChildCnt());
                    bean.setClassType(bean1.getClassType());
                    bean.setInfantCnt(bean1.getInfantCnt());
                    Singleton.getInstance().roomPassengerInfoList.add(bean);
                }
                if (hotelId != null) {
                    dealsNearMeClicked = false;
                    searchCount = 0;
                    jsn = hotelJson();
                    if (jsn != null) {
                        editor.putString(Constants.REQUEST_JSON, jsn).apply();
                        Fragment fragment = new HotelDetailFragment();
                        bundle.putString("search_jsn", jsn);
                        bundle.putString("hotel_id", hotelId);
                        bundle.putInt("total_room_cnt", roomPassengerArrayList.size());
                        bundle.putString("header_date", from_date + " - " + to_date);
                        bundle.putInt("adult_count", totalAdultCnt);
                        bundle.putInt("child_count", totalChildCnt);
                        bundle.putBoolean("isMainPage", true);
                        bundle.putBoolean("isFirstPage", true);
                        bundle.putStringArrayList("adults_list", new ArrayList<>(adult_count_list));
                        bundle.putStringArrayList("childs_list", new ArrayList<>(childList));
                        bundle.putSerializable("children_list", new HashMap(child_count_list));
                        bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                        bundle.putString("hotelCheckInDate", hotelCheckInDate);
                        bundle.putString("hotelCheckOutDate", hotelCheckOutDate);
                        Utils.printMessage(TAG, "Executing hotel");
                        fragment.setArguments(bundle);
                        ((MainSearchActivity) mActivity).pushFragment(fragment);
                        loadingViewLayout.removeAllViews();
                        closeProgress();
                    }
                }
            }
        } else if (searchType.equalsIgnoreCase("AIRPORT")) {
            if (cityId != null) {
                dealsNearMeClicked = false;
                serach_jsn = Json(true);
                handleSearchRequest_(makeJson());
            }
        }
    }

    private String parsedaa(String data) {
        hotelList.clear();
        huidlist.clear();
        String jsn = "";
        try {
            JSONObject obj = new JSONObject(data);
            JSONObject searchRSObj = obj.optJSONObject("searchRS");
            if (searchRSObj.has("err")) {
                closeProgress();
                try {
                    Utils.printMessage(TAG, "search count :: " + searchCount);
                    if (searchCount == 1) {
                        if (dealsNearMeClicked) {
                            searchType = "CITY";
                            jsn = Json(false);
                            hotelCheckInDate = "";
                            hotelCheckOutDate = "";
                            dateInfoText.setText(getString(R.string.label_hotel_search_check_in));
                            hotelCheckoutText.setText(getString(R.string.label_hotel_search_check_out));
                        } else {
                            jsn = Json(true);
                            searchType = "CITY";
                        }
                        return jsn;
                    } else {
                        final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                        errorText = (TextView) errorView.findViewById(R.id.error_text);
                        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                        searchButton = (Button) errorView.findViewById(R.id.search_button);
                        errorText.setTypeface(titleFace);
                        errorDescriptionText.setTypeface(titleFace);
                        searchButton.setTypeface(titleFace);
                        searchButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadingViewLayout.removeAllViews();
                            }
                        });
                        loadErrorType(Constants.RESULT_ERROR);
                        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        loadingViewLayout.addView(errorView);
                        return "fail";
                    }
                } catch (Exception e) {
                }
            } else {
                JSONArray arr = searchRSObj.getJSONArray("hotel");
                for (int i = 0; i < arr.length(); i++) {
                    HotelModel model = new HotelModel();
                    JSONObject rcObj = arr.optJSONObject(i);
                    model.setCheck_in_date(hotelCheckInDate);
                    model.setCheck_out_date(hotelCheckOutDate);
                    model.setHotelJson(rcObj.toString());
                    JSONArray rcArr = rcObj.getJSONArray("rc");
                    for (int j = 0; j < rcArr.length(); j++) {
                        ArrayList<RoomModel> roomlist = new ArrayList<>();
                        JSONObject rObj = rcArr.optJSONObject(j);
                        model.setDur(rObj.optString("dur"));
                        JSONObject pObj = rObj.optJSONObject("p");
                        model.setP(pObj.optString("val"));
                        model.setCur(pObj.optString("cur"));
                        JSONObject wdObj = rObj.optJSONObject("wdp");
                        model.setWdp(wdObj.optString("val"));
                        JSONObject htObj = rObj.optJSONObject("ht");
                        model.setHuid(htObj.optString("uid"));
                        dealsNearCityId = htObj.optString("cid");
                        ArrayList<String> adultcount = occupancy_adult_count_list;
                        model.setAdultcountlist(adultcount);
                        model.setChild_count_list(occupancy_child_count_list);
                        JSONArray roomArray = rObj.getJSONArray("room");
                        for (int k = 0; k < roomArray.length(); k++) {
                            RoomModel roomModel = new RoomModel();
                            JSONObject roomObj = roomArray.optJSONObject(k);
                            roomModel.setNight_count(roomObj.optString("dur"));
                            JSONObject pRoomObj = roomObj.optJSONObject("p");
                            roomModel.setPrice(pRoomObj.optString("val"));
                            roomModel.setCurrency(pRoomObj.optString("cur"));
                            JSONObject wdpRoomObj = roomObj.optJSONObject("wdp");
                            roomModel.setWdp(wdpRoomObj.optString("val"));
                            roomModel.setBrakfast(roomObj.optString("mn"));
                            roomModel.setRoom_type(roomObj.optString("rn"));
                            JSONObject piRoomObj = roomObj.optJSONObject("pi");
                            try {
                                if (piRoomObj.has("FreeCancellationDate")) {
                                    roomModel.setFree_cancel_date(piRoomObj.optString("FreeCancellationDate"));
                                } else {
                                    roomModel.setFree_cancel_date("");
                                }
                            } catch (Exception e) {
                                Utils.printMessage(TAG, "Free canclDate Err :: " + e.getMessage());
                            }
                            roomModel.setRoom_count("" + roomArray.length());
                            roomlist.add(roomModel);
                        }
                        model.setRoomlist(roomlist);
                        model.setDate(hotelCheckInDate);
                    }
                    huidlist.add(model.getHuid());
                    hotelList.add(model);
                }
                return "success";
            }
        } catch (JSONException e) {
        }
        return jsn;
    }

    private String makeJson() {
        String result = null;
        if (mActivity != null) {
            final SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            String language = preferences.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
            JSONObject source = new JSONObject();
            JSONObject crt = new JSONObject();
            JSONObject hcp = new JSONObject();
            JSONObject finaldata = new JSONObject();
            String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
            String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
            String timeStamp = currentDateFormat + "T" + currentTimeFormat;
            try {
                source.put("device", Constants.DEVICE_TYPE);
                source.put("clientId", Constants.HOTEL_CLIENT_ID);
                source.put("clientSecret", Constants.HOTEL_CLIENT_SECRET);
                source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
                source.put("accessToken", Utils.getRequestAccessToken(mActivity));
                source.put("timeStamp", timeStamp);
                JSONArray mJSONArray = new JSONArray(huidlist);
                if (dealsNearMeClicked) {
                    crt.put("hcid", dealsNearCityId);
                } else {
                    crt.put("hcid", cityId);
                }
                crt.put("language", language);
                crt.put("huid", mJSONArray);
                crt.put("pt", "B|F");
                hcp.put("source", source);
                hcp.put("crt", crt);
                finaldata.put("hcp", hcp);
                return finaldata.toString();
            } catch (JSONException e) {
            } catch (Exception e) {
            }
        }
        return result;
    }

    private String Json(boolean cityIdRequired) {
        JSONObject obj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String userSelectedCurr = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        RandomKeyGeneration rndKeyGeneration = new RandomKeyGeneration();
        randomNumber = rndKeyGeneration.generateRandomString();
        try {
            JSONObject source = new JSONObject();
            source = Utils.getHotelSearchAppSettingData(source, mActivity);
            source.put("clientId", Constants.HOTEL_CLIENT_ID);
            source.put("clientSecret", Constants.HOTEL_CLIENT_SECRET);
            source.put("paxNationality", Utils.getUserCountry(mActivity));
            source.put("echoToken", randomNumber);
            source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
            source.put("accessToken", Utils.getRequestAccessToken(mActivity));
            source.put("timeStamp", timeStamp);
            source.put("currency", userSelectedCurr);
            source.put("groupType", groupType);
            JSONObject sourceRQ = new JSONObject();
            sourceRQ.put("source", source);
            if (cityIdRequired) {
                JSONObject sc = new JSONObject();
                sc.put("ad", Utils.formatDateToServerDateFormat(hotelCheckInDate));
                sc.put("dur", night_count);
                sc.put("cid", cityId);
                sc.put("skm", "false");
                sourceRQ.put("sc", sc);
                JSONObject rms = new JSONObject();
                room_count = roomPassengerArrayList.size();
                rms.put("rm", addRoomsArray(roomPassengerArrayList.size(), roomPassengerArrayList));
                sourceRQ.put("rms", rms);
            } else {
                if (latitude == 0.0 && longitude == 0.0) {
                    checkLatLongValues();
                    return null;
                }
                JSONObject sc = new JSONObject();
                //Production
                hotelCheckInDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
                hotelCheckOutDate = Utils.addOneDayToCalendar(hotelCheckInDate);
//                //Test
//                String inDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
//                hotelCheckInDate = Utils.addDaysToCalendar(inDate);
//                hotelCheckOutDate = Utils.addOneDayToCalendar(hotelCheckInDate);
                from_date = Utils.convertToJourneyDate(hotelCheckInDate, mActivity);
                to_date = Utils.convertToJourneyDate(hotelCheckOutDate, mActivity);
                sc.put("ad", hotelCheckInDate);
                checkDateDiff();
                sc.put("dur", String.valueOf(night_count));
                sc.put("skm", "false");
                sourceRQ.put("sc", sc);
                JSONObject gc = new JSONObject();
                gc.put("lat", String.valueOf(latitude));
                gc.put("lon", String.valueOf(longitude));
                gc.put("rds", Constants.HOTEL_GPS_RDS);
                sourceRQ.put("gc", gc);
                roomPassengerArrayList.clear();
                FhRoomPassengerDetailsBean fhPassengerDetailsBean = new FhRoomPassengerDetailsBean();
                fhPassengerDetailsBean.setAdultCnt(2);
                fhPassengerDetailsBean.setChildCnt(0);
                fhPassengerDetailsBean.setInfantCnt(0);
                fhPassengerDetailsBean.getChildAgeArray().add(2);
                fhPassengerDetailsBean.getChildAgeArray().add(2);
                fhPassengerDetailsBean.getChildAgeArray().add(2);
                fhPassengerDetailsBean.getChildAgeArray().add(2);
                fhPassengerDetailsBean.getChildAgeArray().add(2);
                fhPassengerDetailsBean.setExpanded(false);
                totalPassengerCount = 2;
                travellerInfoText.setText(1 + " " + getString(R.string.label_search_page_room) + " , " + totalPassengerCount + " " + getString(R.string.label_search_flight_travellers));
                roomPassengerArrayList.add(fhPassengerDetailsBean);
                JSONObject rms = new JSONObject();
                room_count = roomPassengerArrayList.size();
                rms.put("rm", addRoomsArray(roomPassengerArrayList.size(), roomPassengerArrayList));
                sourceRQ.put("rms", rms);
            }
            obj.put("searchRQ", sourceRQ);
        } catch (Exception e) {
        }
        return obj.toString();
    }

    private String noHotelJson() {
        JSONObject obj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String userSelectedCurr = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        String selectedCityId = pref.getString("selected_city_id", "");
        String firstCheckDate = pref.getString("first_check_date", "");
        String firstCheckOut = pref.getString("first_check_out", "");
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        RandomKeyGeneration rndKeyGeneration = new RandomKeyGeneration();
        randomNumber = rndKeyGeneration.generateRandomString();
        try {
            JSONObject source = new JSONObject();
            source = Utils.getHotelSearchAppSettingData(source, mActivity);
            source.put("clientId", Constants.HOTEL_CLIENT_ID);
            source.put("clientSecret", Constants.HOTEL_CLIENT_SECRET);
            source.put("paxNationality", Utils.getUserCountry(mActivity));
            source.put("echoToken", randomNumber);
            source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
            source.put("accessToken", Utils.getRequestAccessToken(mActivity));
            source.put("timeStamp", timeStamp);
            source.put("currency", userSelectedCurr);
            source.put("groupType", groupType);
            JSONObject sourceRQ = new JSONObject();
            sourceRQ.put("source", source);
            JSONObject sc = new JSONObject();
            sc.put("ad", Utils.formatDateToServerDateFormat(firstCheckDate));
            sc.put("dur", checkSecondDateDiff(firstCheckDate, firstCheckOut));
            sc.put("cid", selectedCityId);
            sc.put("skm", "false");
            sourceRQ.put("sc", sc);
            JSONObject rms = new JSONObject();
            ArrayList<FhRoomPassengerDetailsBean> roomPassengerArrayList = new ArrayList<>();
            for (int i = 0; i < Singleton.getInstance().roomPassengerInfoList.size(); i++) {
                FhRoomPassengerDetailsBean bean1 = Singleton.getInstance().roomPassengerInfoList.get(i);
                FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
                bean.setAdultCnt(bean1.getAdultCnt());
                bean.setChildAge(bean1.getChildAge());
                bean.setAge(bean1.getAge());
                bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
                bean.setChildCnt(bean1.getChildCnt());
                bean.setClassType(bean1.getClassType());
                bean.setInfantCnt(bean1.getInfantCnt());
                roomPassengerArrayList.add(bean);
            }
            room_count = roomPassengerArrayList.size();
            rms.put("rm", addRoomsArray(roomPassengerArrayList.size(), roomPassengerArrayList));
            sourceRQ.put("rms", rms);
            obj.put("searchRQ", sourceRQ);
        } catch (Exception e) {
        }
        return obj.toString();
    }

    private String hotelJson() {
        if (hotelId != null) {
            String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
            String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
            String timeStamp = currentDateFormat + "T" + currentTimeFormat;
            JSONObject obj = new JSONObject();
            SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            String groupType = "null";
            if (pref.getBoolean(Constants.isFanClub, false)) {
                groupType = "ALHILAL";
            }
            RandomKeyGeneration rndKeyGeneration = new RandomKeyGeneration();
            randomNumber = rndKeyGeneration.generateRandomString();
            try {
                JSONObject source = new JSONObject();
                source = Utils.getHotelSearchAppSettingData(source, mActivity);
                source.put("clientId", Constants.HOTEL_CLIENT_ID);
                source.put("clientSecret", Constants.HOTEL_CLIENT_SECRET);
                source.put("paxNationality", Utils.getUserCountry(mActivity));
                source.put("echoToken", randomNumber);
                source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
                source.put("accessToken", Utils.getRequestAccessToken(mActivity));
                source.put("timeStamp", timeStamp);
                source.put("groupType", Utils.checkStringValue(groupType));
                JSONObject sourceRQ = new JSONObject();
                sourceRQ.put("source", source);
                JSONObject sc = new JSONObject();
                sc.put("ad", Utils.formatDateToServerDateFormat(hotelCheckInDate));
                sc.put("dur", night_count);
                sc.put("uids", hotelId);
                sc.put("skm", "false");
                sourceRQ.put("sc", sc);
                JSONObject rms = new JSONObject();
                rms.put("rm", addRoomsArray(roomPassengerArrayList.size(), roomPassengerArrayList));
                sourceRQ.put("rms", rms);
                obj.put("searchRQ", sourceRQ);
                room_count = roomPassengerArrayList.size();
            } catch (Exception e) {
            }
            return obj.toString();
        }
        return null;
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(mActivity.getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_network_error_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(mActivity.getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_no_results_found_message_hotels));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
        if (isInternetPresent) {
            if (mActivity != null) {
                new HTTPAsync(mActivity, HotelSearchFragment.this, Constants.HOTEL_NAME_SEARCH,
                        "" + s, GET_CITY, 1).execute();
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    private void handleSearchRequest_(final String jsn) {
        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.REQUEST_JSON_HOTEL_CONTENT, jsn);
        editor.putBoolean(Constants.IS_ROOM_INFO_UPDATED, false);
        editor.apply();
        Utils.printMessage(TAG, "REQUEST_JSON_HOTEL_CONTENT :: " + jsn);
        boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
        if (isInternetPresent) {
            showProgress();
            new HTTPAsync(mActivity, HotelSearchFragment.this, Constants.HOTEL_INFO_RQ_URL, "", jsn,
                    SEARCH_HOTEL_DETAILS, HTTPAsync.METHOD_POST).execute();
        } else {
            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(titleFace);
            errorDescriptionText.setTypeface(titleFace);
            searchButton.setTypeface(titleFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                    if (isInternetPresent) {
                        loadingViewLayout.removeAllViews();
                        closeProgress();
                        new HTTPAsync(mActivity, HotelSearchFragment.this, Constants.HOTEL_INFO_RQ_URL, "", jsn,
                                SEARCH_HOTEL_DETAILS, HTTPAsync.METHOD_POST).execute();
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
    }

    private void handleSearchRequestDeals(final String jsn) {
        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.REQUEST_JSON, jsn);
        editor.putBoolean(Constants.IS_ROOM_INFO_UPDATED, false);
        editor.apply();
        Utils.printMessage(TAG, "jsn::" + jsn);
        boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
        if (isInternetPresent) {
            showProgress();
            new HTTPAsync(mActivity, HotelSearchFragment.this, Constants.HOTEL_SEARCH_URL, "", jsn,
                    SEARCH_HOTEL_DEALSNEARBY, HTTPAsync.METHOD_POST).execute();
        } else {
            closeProgress();
            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(titleFace);
            errorDescriptionText.setTypeface(titleFace);
            searchButton.setTypeface(titleFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                    if (isInternetPresent) {
                        loadingViewLayout.removeAllViews();
                        closeProgress();
                        new HTTPAsync(mActivity, HotelSearchFragment.this, Constants.HOTEL_SEARCH_URL, "", jsn,
                                SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
    }

    private void handleSearchRequest(final String jsn) {
        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.REQUEST_JSON, jsn);
        editor.putBoolean(Constants.IS_ROOM_INFO_UPDATED, false);
        editor.apply();
        Utils.printMessage(TAG, "jsn::" + jsn);
        boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
        if (isInternetPresent) {
            showLoading();
            new HTTPAsync(mActivity, HotelSearchFragment.this, Constants.HOTEL_SEARCH_URL, "", jsn,
                    SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
        } else {
            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(titleFace);
            errorDescriptionText.setTypeface(titleFace);
            searchButton.setTypeface(titleFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                    if (isInternetPresent) {
                        loadingViewLayout.removeAllViews();
                        new HTTPAsync(mActivity, HotelSearchFragment.this, Constants.HOTEL_SEARCH_URL, "", jsn,
                                SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
    }

    private void checkPassengerCount() {
        int adultCnt = 0;
        int childCnt = 0;
        int infantCnt = 0;
        for (int i = 0; i < roomPassengerArrayList.size(); i++) {
            adultCnt = adultCnt + roomPassengerArrayList.get(i).getAdultCnt();
            childCnt = childCnt + roomPassengerArrayList.get(i).getChildCnt();
            infantCnt = infantCnt + roomPassengerArrayList.get(i).getInfantCnt();
        }
        totalAdultCnt = adultCnt;
        totalChildCnt = childCnt;
        totalPassengerCount = adultCnt + childCnt;
        String roomsCountText = "";
        String adtCountText = "";
        if (roomPassengerArrayList.size() > 1) {
            roomsCountText = getString(R.string.label_multiple_rooms);
        } else {
            roomsCountText = getString(R.string.label_search_page_room);
        }
        if (totalPassengerCount > 1) {
            adtCountText = getString(R.string.label_search_flight_travellers);
        } else {
            adtCountText = getString(R.string.label_add_traveller);
        }
        travellerInfoText.setText(roomPassengerArrayList.size() + " " + roomsCountText + " , " + totalPassengerCount + " " + adtCountText);
    }

    private String hotelJsonTripAdvisor() {
        String result = null;
        if (mActivity != null) {
            JSONObject source = new JSONObject();
            JSONObject crt = new JSONObject();
            JSONObject hcp = new JSONObject();
            JSONObject finaldata = new JSONObject();
            String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
            String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
            String timeStamp = currentDateFormat + "T" + currentTimeFormat;
            try {
                source.put("device", Constants.DEVICE_TYPE);
                source.put("clientId", Constants.HOTEL_CLIENT_ID);
                source.put("clientSecret", Constants.HOTEL_CLIENT_SECRET);
                source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
                source.put("accessToken", Utils.getRequestAccessToken(mActivity));
                source.put("timeStamp", timeStamp);
                JSONArray mJSONArray = new JSONArray(huidlist);
                if (dealsNearMeClicked) {
                    crt.put("hcid", dealsNearCityId);
                } else {
                    crt.put("hcid", cityId);
                }
                crt.put("huid", mJSONArray);
                hcp.put("source", source);
                hcp.put("crt", crt);
                finaldata.put("hcp", hcp);
                return finaldata.toString();
            } catch (JSONException e) {
            } catch (Exception e) {
            }
        }
        return result;
    }

    private static void requestPermission(final Context context) {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION)) {
            new AlertDialog.Builder(context)
                    .setMessage(context.getResources().getString(R.string.permission_storage))
                    .setPositiveButton(R.string.label_selected_flight_message, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                    2);
                        }
                    }).show();
        } else {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    2);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Utils.printMessage(TAG, "onRequestPermissionsResult::" + grantResults.length);
        switch (requestCode) {
            case 2: {
                if (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    Utils.printMessage(TAG, "permission granted");
                } else {
                    Utils.printMessage(TAG, "permission granted failure");
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                return;
            }
        }
    }

    private void checkLatLongValues() {
        if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                ) {
            requestPermission(mActivity);
            return;
        }
        gps = new GPSTracker(mActivity);
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            String json = Json(false);
            if (json != null) {
                handleSearchRequestDeals(json);
            }
        } else {
            gps.showSettingsAlert();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshBackground();
    }

    public void refreshBackground() {
        try {
            SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            if (preferences.getBoolean(Constants.isFanClub, false)) {
                if (hotelViewBgImage != null) {
                    hotelViewBgImage.setImageResource(R.drawable.alhilalbg);
                    hotelViewBgImage.setColorFilter(ContextCompat.getColor(getContext(), R.color.search_image_bg_color));
                }
            } else {
                if (hotelViewBgImage != null) {
                    hotelViewBgImage.setImageResource(R.drawable.hotel_bg_image);
                    hotelViewBgImage.setColorFilter(ContextCompat.getColor(getContext(), R.color.search_screen_bg_color));
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    private void showProgress() {
        if (hotelListLoadingDialog == null) {
            hotelListLoadingDialog = new HotelListLoadingDialog(mActivity);
            hotelListLoadingDialog.setCancelable(false);
            hotelListLoadingDialog.show();
            Window window = hotelListLoadingDialog.getWindow();
            int width = (int) (mActivity.getResources().getDisplayMetrics().widthPixels * 0.85);
            window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
        }
    }

    private void closeProgress() {
        if (hotelListLoadingDialog != null) {
            hotelListLoadingDialog.dismiss();
            hotelListLoadingDialog = null;
        }
    }

    private void displaySuccessMessage(String message, boolean isSucessMessage) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            if (isSucessMessage) {
                errorView.setBackgroundResource(R.color.success_message_background);
            } else {
                errorView.setBackgroundResource(R.color.error_message_background);
            }
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(mActivity.getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(mActivity.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }
}
