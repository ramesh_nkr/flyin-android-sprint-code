package com.flyin.bookings;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.adapter.MyTripsAdapter;
import com.flyin.bookings.listeners.AirlinesInterface;
import com.flyin.bookings.model.BookingsObject;
import com.flyin.bookings.model.CancellationPolicyBean;
import com.flyin.bookings.model.EntityDetailsObjectBean;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.FlightDetailsObjectBean;
import com.flyin.bookings.model.FlightsDataBean;
import com.flyin.bookings.model.GeneralDetailsObjectBean;
import com.flyin.bookings.model.PassengerDetailsObjectBean;
import com.flyin.bookings.model.PassengersBean;
import com.flyin.bookings.model.PersonalObjectBean;
import com.flyin.bookings.model.PriceBean;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.AirlinesData;
import com.flyin.bookings.util.AirportData;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

@SuppressWarnings("ALL")
public class FlightTripsTab extends Fragment implements AsyncTaskListener {
    private ListView flightListView;
    private MyTripsAdapter myTripsAdapter;
    private ArrayList<BookingsObject> tempBookingsArrayList = new ArrayList<>();
    private ArrayList<BookingsObject> bookingMainArray = new ArrayList<>();
    private String selectedLang;
    private LayoutInflater inflater;
    private RelativeLayout loadingViewLayout;
    private View loadingView;
    private Typeface textFace;
    private String requestTripDetailsJSON = "";
    private static final int GET_TRIP_DETAILS = 1;
    private static final String TAG = "FlightTripsTab";
    private boolean isInternetPresent = false;
    private ImageView errorImage;
    private Button searchButton;
    private TextView errorText, errorDescriptionText;
    private MyReceiver myReceiver;
    private IntentFilter intentFilter;
    private Activity mActivity;
    private ArrayList<String> airportArrayList = new ArrayList<>();
    private ArrayList<String> airlineArrayList = new ArrayList<>();
    private HashMap<String, FlightAirportName> airportNamesMap = new HashMap<>();
    private HashMap<String, String> airLineNamesMap = new HashMap<>();
    private AirportData airportData = new AirportData();
    private AirlinesData airlinesData = new AirlinesData();

    public FlightTripsTab() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myReceiver = new MyReceiver();
        intentFilter = new IntentFilter("com.flyin.bookings.SEARCH_FLIGHTS");
        mActivity.registerReceiver(myReceiver, intentFilter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_flight_trips, container, false);
        isInternetPresent = Utils.isConnectingToInternet(mActivity);
        loadingViewLayout = (RelativeLayout) view.findViewById(R.id.loading_view_layout);
        requestTripDetailsJSON = tripDetailsTask();
        selectedLang = Constants.LANGUAGE_ENGLISH_CODE;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(mActivity)) {
            selectedLang = Constants.LANGUAGE_ARABIC_CODE;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        textFace = Typeface.createFromAsset(mActivity.getAssets(), fontText);
        flightListView = (ListView) view.findViewById(R.id.my_trips_list);
        myTripsAdapter = new MyTripsAdapter(mActivity, tempBookingsArrayList);
        flightListView.setAdapter(myTripsAdapter);
        flightListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
                Singleton.getInstance().selectedTripFlightObject = tempBookingsArrayList.get(position);
                Intent intent = new Intent(mActivity, FlightTripDetailsActivity.class);
                intent.putExtra(Constants.AIRPORT_NAME_HASHMAP, airportNamesMap);
                intent.putExtra(Constants.AIRLINE_NAME_HASHMAP, airLineNamesMap);
                startActivity(intent);
            }
        });
        if (isInternetPresent) {
            getTripDetailsRequestFromServer(requestTripDetailsJSON);
        } else {
            inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(mActivity);
                    if (isInternetPresent) {
                        loadingViewLayout.removeView(errorView);
                        getTripDetailsRequestFromServer(requestTripDetailsJSON);
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
        return view;
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.SEARCH_FLIGHTS")) {
                if (bookingMainArray.size() == 0) {
                    return;
                } else {
                    if (intent.getStringExtra("message") != null) {
                        handleSearch(intent.getStringExtra("message"));
                    }
                }
            }
        }
    }

    private void handleSearch(String searchText) {
        tempBookingsArrayList.clear();
        if (searchText.isEmpty()) {
            tempBookingsArrayList.addAll(bookingMainArray);
        } else {
            for (BookingsObject bookings : bookingMainArray) {
                if (bookings.getEntityDetailsObjectBean().getFlightDetailsBean().getGdsPnr() != null) {
                    String pnr = bookings.getEntityDetailsObjectBean().getFlightDetailsBean().getGdsPnr();
                    if (!pnr.equalsIgnoreCase("null") || !pnr.equalsIgnoreCase("")) {
                        if (pnr.contains(searchText.toUpperCase())) {
                            tempBookingsArrayList.add(bookings);
                            continue;
                        }
                    }
                }
                if (bookings.getGeneralDetailsObjectBean().getFlyinCode() != null) {
                    String tripId = bookings.getGeneralDetailsObjectBean().getFlyinCode();
                    if (!tripId.equalsIgnoreCase("null") || !tripId.equalsIgnoreCase("")) {
                        if (tripId.contains(searchText.toUpperCase())) {
                            tempBookingsArrayList.add(bookings);
                            continue;
                        }
                    }
                }
                if (bookings.getGeneralDetailsObjectBean().getFlyinCode() != null) {
                    String flyinCode = bookings.getGeneralDetailsObjectBean().getFlyinCode();
                    if (!flyinCode.equalsIgnoreCase("null") || !flyinCode.equalsIgnoreCase("")) {
                        if (flyinCode.contains(searchText.toUpperCase())) {
                            tempBookingsArrayList.add(bookings);
                            continue;
                        }
                    }
                }
                String journeyType = "";
                ArrayList<FlightsDataBean> flightsArr = new ArrayList<FlightsDataBean>();
                flightsArr = bookings.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList();
                for (FlightsDataBean flight : flightsArr) {
                    if (!journeyType.equalsIgnoreCase("R")) {
                        journeyType = flight.getJourneyType();
                        break;
                    }
                }
                String arrivalAirportCode = "";
                String arrivalCityName = "";
                if (journeyType.equalsIgnoreCase("O") || journeyType.equalsIgnoreCase("M")) {
                    arrivalAirportCode = flightsArr.get(flightsArr.size() - 1).getArrivalAirport();
                    arrivalCityName = flightsArr.get(flightsArr.size() - 1).getArrivalAirportName();
                } else {
                    for (FlightsDataBean flight : flightsArr) {
                        if (flight.getLegOrder().equalsIgnoreCase("1")) {
                            arrivalAirportCode = flight.getArrivalAirport();
                            arrivalCityName = flightsArr.get(flightsArr.size() - 1).getArrivalAirportName();
                            break;
                        }
                    }
                }
                if ((arrivalCityName.toLowerCase()).contains(searchText) || arrivalAirportCode.contains(searchText.toUpperCase())) {
                    tempBookingsArrayList.add(bookings);
                }
            }
        }
        myTripsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.unregisterReceiver(myReceiver);
    }

    private String tripDetailsTask() {
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(mActivity));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(mActivity));
            mainJSON.put("source", sourceJSON);
//            mainJSON.put("customerId", pref.getString(Constants.MEMBER_EMAIL, ""));
//            mainJSON.put("fromDate", "");
//            mainJSON.put("toDate", "");
//            mainJSON.put("referenceNo", "");
            mainJSON.put("searchType", "all_bookings");
//            mainJSON.put("referenceType", Utils.checkStringValue("null"));
            mainJSON.put("recordType", "Only Flight");
            mainJSON.put("userUniqueId", pref.getString(Constants.USER_UNIQUE_ID, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void getTripDetailsRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(mActivity, this, Constants.MY_TRIPS_URL, "", requestJSON,
                GET_TRIP_DETAILS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "Flights DATA:: " + data);
        JSONObject obj = null;
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            showWrongErr();
        } else {
            ParseJSONTask task = new ParseJSONTask();
            task.execute(data);
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(textFace);
        descriptionText.setVisibility(View.GONE);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        try {
            loadingViewLayout.removeView(loadingView);
            loadingView = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int checkOrderCount(String value, ArrayList<String> orderType) {
        int stopsCount = 0;
        for (int i = 0; i < orderType.size(); i++) {
            if (value.equalsIgnoreCase(orderType.get(i))) {
                stopsCount++;
            }
        }
        return stopsCount;
    }

    private void loadErrorType(int type) {

        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(mActivity.getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_network_error_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(mActivity.getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_no_results_found_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private class ParseJSONTask extends AsyncTask<String, Void, String> {
        JSONObject obj = null;

        @Override
        protected String doInBackground(String... urls) {
            try {
                obj = new JSONObject(urls[0]);
                Utils.printMessage(TAG, "Flights DATA:: " + obj);
                if (obj.has("bookingDetails")) {
                    if (obj.getJSONObject("bookingDetails").has("bookings")) {
                        JSONArray bookingsArr = obj.getJSONObject("bookingDetails").getJSONArray("bookings");
                        ArrayList<BookingsObject> tempBookingsArr = new ArrayList<>();
                        for (int i = 0; i < bookingsArr.length(); i++) {
                            BookingsObject bookingsObject = new BookingsObject();
                            if (bookingsArr.getJSONObject(i).has("entityDetails")) {
                                EntityDetailsObjectBean entityDetailsObjectBean = new EntityDetailsObjectBean();
                                if (bookingsArr.getJSONObject(i).getJSONObject("entityDetails").has("flightDetails")) {
                                    FlightDetailsObjectBean flightDetailsObjectBean = new FlightDetailsObjectBean();
                                    JSONArray flightsArray = bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("flightDetails").getJSONArray("flights");
                                    ArrayList<FlightsDataBean> flightsDataBeanArrayList = new ArrayList<>();
                                    for (int j = 0; j < flightsArray.length(); j++) {
                                        JSONObject flightsObj = flightsArray.getJSONObject(j);
                                        FlightsDataBean flightsDataBean = new FlightsDataBean();
                                        flightsDataBean.setDepartureAirport(flightsObj.getString("departureAirport"));
                                        flightsDataBean.setArrivalAirport(flightsObj.getString("arrivalAirport"));
                                        flightsDataBean.setOperatingAirline(flightsObj.getString("operatingAirline"));
                                        flightsDataBean.setMarketingAirline(flightsObj.getString("marketingAirline"));
                                        flightsDataBean.setDuration(flightsObj.getString("duration"));
                                        flightsDataBean.setDepartureDate(flightsObj.getString("departureDate"));
                                        flightsDataBean.setArrivalDate(flightsObj.getString("arrivalDate"));
                                        flightsDataBean.setEquipmentType(flightsObj.getString("equipmentType"));
                                        flightsDataBean.setClassType(flightsObj.getString("classType"));
                                        flightsDataBean.setTerminalinfo(flightsObj.getString("terminalinfo"));
                                        flightsDataBean.setBaggageInfo(flightsObj.getString("baggageInfo"));
                                        flightsDataBean.setAirlinePNR(flightsObj.getString("airlinePNR"));
                                        flightsDataBean.setJourneyType(flightsObj.getString("journeyType"));
                                        flightsDataBean.setJourneyOrder(flightsObj.getString("journeyOrder"));
                                        flightsDataBean.setLegOrder(flightsObj.getString("legOrder"));
                                        flightsDataBean.setFlightNumber(flightsObj.getString("flightNumber"));
                                        String orderType = "";
                                        ArrayList<String> orderTypeArr = new ArrayList<String>();
                                        for (int k = 0; k < flightsArray.length(); k++) {
                                            orderType = flightsArray.getJSONObject(k).getString("journeyType");
                                            if (orderType.equalsIgnoreCase("M")) {
                                                if (flightsDataBean.getLegOrder().equalsIgnoreCase(flightsArray.getJSONObject(k).getString("journeyOrder"))) {
                                                    orderTypeArr.add(orderType);
                                                }
                                            } else {
                                                orderTypeArr.add(orderType);
                                            }
                                        }
                                        flightsDataBean.setStopsCount((checkOrderCount(flightsObj.getString("journeyType"), orderTypeArr)) - 1);
                                        flightsDataBeanArrayList.add(flightsDataBean);
                                        airportArrayList.add(flightsObj.getString("departureAirport"));
                                        airportArrayList.add(flightsObj.getString("arrivalAirport"));
                                        airlineArrayList.add(flightsObj.getString("operatingAirline"));

                                    }
                                    flightDetailsObjectBean.setFlightsDataBeanArrayList(flightsDataBeanArrayList);
                                    bookingsObject.setFlightDepartDate(Utils.dateFormatToMinutesFormat(flightsDataBeanArrayList.get(0).getDepartureDate()));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("flightDetails").has("cancellationPolicy")) {
                                        CancellationPolicyBean cancellationPolicyBean = new CancellationPolicyBean();
                                        ArrayList<String> notesArray = new ArrayList<>();
                                        for (int j = 0; j < bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("flightDetails").getJSONObject("cancellationPolicy").getJSONArray("notes").length(); j++) {
                                            notesArray.add(bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("flightDetails").getJSONObject("cancellationPolicy").getJSONArray("notes").getString(j));
                                        }
                                        cancellationPolicyBean.setNotesArray(notesArray);
                                        flightDetailsObjectBean.setCancellationPolicyObjectBean(cancellationPolicyBean);
                                    }
                                    flightDetailsObjectBean.setGdsPnr(bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("flightDetails").getString("gdsPnr"));
                                    entityDetailsObjectBean.setFlightDetailsBean(flightDetailsObjectBean);
                                }
                                entityDetailsObjectBean.setHotelDetails(bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getString("hotelDetails"));
                                bookingsObject.setEntityDetailsObjectBean(entityDetailsObjectBean);
                            }
                            if (bookingsArr.getJSONObject(i).has("generalDetails")) {
                                GeneralDetailsObjectBean generalDetailsObjectBean = new GeneralDetailsObjectBean();
                                generalDetailsObjectBean.setBookingStatus(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("bookingStatus"));
                                generalDetailsObjectBean.setFlyinCode(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("flyinCode"));
                                generalDetailsObjectBean.setBookingDate(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("bookingDate"));
                                generalDetailsObjectBean.setCustomerId(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("customerId"));
                                bookingsObject.setGeneralDetailsObjectBean(generalDetailsObjectBean);
                            }
                            if (bookingsArr.getJSONObject(i).has("passengerDetails")) {
                                PassengerDetailsObjectBean passengerDetailsObjectBean = new PassengerDetailsObjectBean();
                                if (bookingsArr.getJSONObject(i).getJSONObject("passengerDetails").has("passengers")) {
                                    JSONArray passengerArray = bookingsArr.getJSONObject(i).getJSONObject("passengerDetails").getJSONArray("passengers");
                                    ArrayList<PassengersBean> passengersBeanArrayList = new ArrayList<>();
                                    for (int j = 0; j < passengerArray.length(); j++) {
                                        PassengersBean passengersBean = new PassengersBean();
                                        passengersBean.setMealPreference(passengerArray.getJSONObject(j).getString("mealPreference"));
                                        passengersBean.setSeatPreference(passengerArray.getJSONObject(j).getString("seatPreference"));
                                        passengersBean.setSpecialPreference(passengerArray.getJSONObject(j).getString("specialPreference"));
                                        if (passengerArray.getJSONObject(j).has("personalDetails")) {
                                            PersonalObjectBean personalDetailsObjectBeans = new PersonalObjectBean();
                                            personalDetailsObjectBeans.setTitle(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("title"));
                                            personalDetailsObjectBeans.setFirstName(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("firstName"));
                                            personalDetailsObjectBeans.setMiddleName(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("middleName"));
                                            personalDetailsObjectBeans.setLastName(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("lastName"));
                                            personalDetailsObjectBeans.setTicketNo(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("ticketNo"));
                                            passengersBean.setPersonalDetailsObjectBean(personalDetailsObjectBeans);
                                        }
                                        passengersBeanArrayList.add(passengersBean);
                                        passengerDetailsObjectBean.setPassengersBeanArrayList(passengersBeanArrayList);
                                    }
                                    bookingsObject.setPassengerDetailsObjectBean(passengerDetailsObjectBean);
                                }
                            }
                            if (bookingsArr.getJSONObject(i).has("priceDetails")) {
                                PriceBean priceBean = new PriceBean();
                                if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").has("price"))
                                    priceBean.setTotal(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("total"));
                                priceBean.setTax(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("tax"));
                                priceBean.setBaseFare(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("baseFare"));
                                priceBean.setUserCurrency(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("userCurrency"));
                                try {
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("cancellationCharge"))
                                        priceBean.setCancellationCharge(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("cancellationCharge"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("reIssueCharge"))
                                        priceBean.setReIssueCharge(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("reIssueCharge"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("extraBaggageAmount"))
                                        priceBean.setExtraBaggageAmount(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("extraBaggageAmount"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("couponDiscount"))
                                        priceBean.setCouponDiscount(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("couponDiscount"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("disountPrice"))
                                        priceBean.setDisountPrice(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("disountPrice"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("qitafAmount"))
                                        priceBean.setQitafAmount(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("qitafAmount"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("loyaltydetails")) {
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").has("earnpoint")) {
                                            priceBean.setEarnPoints(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").getString("earnpoint"));
                                        }
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").has("redeempoint")) {
                                            priceBean.setRedeemPoint(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").getString("redeempoint"));
                                        }
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").has("rdmdiscountusr")) {
                                            priceBean.setRdmDiscountusr(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").getString("rdmdiscountusr"));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.getMessage();
                                }
                                bookingsObject.setPriceBean(priceBean);
                            }
                            tempBookingsArr.add(bookingsObject);
                        }
                        Collections.sort(tempBookingsArr, new Comparator<BookingsObject>() {
                            @Override
                            public int compare(BookingsObject e1, BookingsObject e2) {
                                Long id1 = e1.getFlightDepartDate();
                                Long id2 = e2.getFlightDepartDate();
                                return id2.compareTo(id1);
                            }
                        });
                        bookingMainArray.addAll(tempBookingsArr);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            if (airportArrayList.size() > 0) {
                airportData.resultData(mActivity, airportArrayList, new AirlinesInterface<HashMap<String, FlightAirportName>>() {
                    @Override
                    public void onSuccess(HashMap<String, FlightAirportName> response) {
                        if (response.size() > 0) {
                            airportNamesMap.putAll(response);
                            DataSetup();
                        }
                    }

                    @Override
                    public void onFailed(String message) {
                        closeLoading();
                        inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View errorView = inflater.inflate(R.layout.view_error_response, null);
                        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                        errorText = (TextView) errorView.findViewById(R.id.error_text);
                        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                        searchButton = (Button) errorView.findViewById(R.id.search_button);
                        errorText.setTypeface(textFace);
                        errorDescriptionText.setTypeface(textFace);
                        searchButton.setTypeface(textFace);
                        searchButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadingViewLayout.removeView(errorView);
                                mActivity.finish();
                            }
                        });
                        loadErrorType(Constants.WRONG_ERROR_PAGE);
                        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        loadingViewLayout.addView(errorView);
                    }
                });
            } else {
                showWrongErr();
            }
            if (airlineArrayList.size() > 0) {
                airlinesData.resultData(mActivity, airlineArrayList, new AirlinesInterface<HashMap<String, String>>() {
                    @Override
                    public void onSuccess(HashMap<String, String> response) {
                        if (response.size() > 0) {
                            airLineNamesMap.putAll(response);
                            DataSetup();
                        }
                    }

                    @Override
                    public void onFailed(String message) {
                        inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View errorView = inflater.inflate(R.layout.view_error_response, null);
                        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                        errorText = (TextView) errorView.findViewById(R.id.error_text);
                        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                        searchButton = (Button) errorView.findViewById(R.id.search_button);
                        errorText.setTypeface(textFace);
                        errorDescriptionText.setTypeface(textFace);
                        searchButton.setTypeface(textFace);
                        searchButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadingViewLayout.removeView(errorView);
                                mActivity.finish();
                            }
                        });
                        loadErrorType(Constants.WRONG_ERROR_PAGE);
                        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        loadingViewLayout.addView(errorView);
                    }
                });
            } else {
                showWrongErr();
            }
        }
    }

    private void DataSetup() {
        if (airportNamesMap.size() > 0 && airLineNamesMap.size() > 0) {
            for (int i = 0; i < bookingMainArray.size(); i++) {
                ArrayList<FlightsDataBean> flightsDataBean = bookingMainArray.get(i).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList();
                for (int j = 0; j < flightsDataBean.size(); j++) {
                    FlightsDataBean bean = flightsDataBean.get(j);
                    flightsDataBean.get(j).setArrivalAirportName(airportNamesMap.get(bean.getArrivalAirport()).getCityName());
                    flightsDataBean.get(j).setDepartureAirportName(airportNamesMap.get(bean.getDepartureAirport()).getCityName());
                }
            }
            tempBookingsArrayList.addAll(bookingMainArray);
            myTripsAdapter.notifyDataSetChanged();
            closeLoading();
        }
    }

    private void showWrongErr() {
        inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View errorView = inflater.inflate(R.layout.view_error_response, null);
        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
        errorText = (TextView) errorView.findViewById(R.id.error_text);
        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
        searchButton = (Button) errorView.findViewById(R.id.search_button);
        errorText.setTypeface(textFace);
        errorDescriptionText.setTypeface(textFace);
        searchButton.setTypeface(textFace);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingViewLayout.removeView(errorView);
                mActivity.finish();
            }
        });
        loadErrorType(Constants.WRONG_ERROR_PAGE);
        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(errorView);
    }
}
