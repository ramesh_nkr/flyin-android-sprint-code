package com.flyin.bookings;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.listeners.TokenUpdateListener;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONObject;

import java.util.Calendar;

public class ChangePasswordActivity extends AppCompatActivity implements AsyncTaskListener, TokenUpdateListener {
    private Typeface textFace = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private EditText oldPassword, newPassword, confirmPassword;
    private Button searchButton = null;
    private ImageView errorImage = null;
    private RelativeLayout loadingViewLayout = null;
    private boolean isInternetPresent = false;
    private LayoutInflater inflater = null;
    private View loadingView = null;
    private static final int GET_CHANGE_PASSWORD_RESPONSE = 1;
    private static final String TAG = "ChangePasswordActivity";
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private boolean isAppReLaunched = false;
    private RefreshTokenService refreshTokenData = new RefreshTokenService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        isInternetPresent = Utils.isConnectingToInternet(ChangePasswordActivity.this);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        oldPassword = (EditText) findViewById(R.id.old_password);
        newPassword = (EditText) findViewById(R.id.new_password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        TextView userName = (TextView) findViewById(R.id.user_name_header);
        Button saveButton = (Button) findViewById(R.id.save_button);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        if (Utils.isArabicLangSelected(ChangePasswordActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                oldPassword.setTextDirection(View.TEXT_DIRECTION_RTL);
                newPassword.setTextDirection(View.TEXT_DIRECTION_RTL);
                confirmPassword.setTextDirection(View.TEXT_DIRECTION_RTL);
                oldPassword.setGravity(Gravity.END);
                newPassword.setGravity(Gravity.END);
                confirmPassword.setGravity(Gravity.END);
            }
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textFace = Typeface.createFromAsset(getAssets(), fontTitle);
        userName.setTypeface(textFace);
        oldPassword.setTypeface(textFace);
        newPassword.setTypeface(textFace);
        confirmPassword.setTypeface(textFace);
        saveButton.setTypeface(tf);
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        userName.setText(getString(R.string.label_welcome_title) + ", " + pref.getString(Constants.MEMBER_EMAIL, ""));
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (oldPassword.getText().toString().isEmpty()) {
                    displayErrorMessage(getString(R.string.label_error_old_password), false);
                    return;
                }
                if (newPassword.getText().toString().isEmpty()) {
                    displayErrorMessage(getString(R.string.label_error_new_password), false);
                    return;
                }
                if (confirmPassword.getText().toString().isEmpty()) {
                    displayErrorMessage(getString(R.string.label_error_confirm_password), false);
                    return;
                }
                if (!confirmPassword.getText().toString().equalsIgnoreCase(newPassword.getText().toString())) {
                    displayErrorMessage(getString(R.string.label_error_different_passwords), false);
                    return;
                }
//                SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
//                long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
//                String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
//                if (tokenTime == -1) {
//                    getTokenFromServer();
//                } else {
//                    long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
//                    if (diff > Long.parseLong(expireIn)) {
//                        getTokenFromServer();
//                    }
//                }
                final String passwordRequestJSON = handleChangePasswordTask();
                isInternetPresent = Utils.isConnectingToInternet(ChangePasswordActivity.this);
                if (isInternetPresent) {
                    //refreshTokenData.checkServiceCall(ChangePasswordActivity.this, ChangePasswordActivity.this);
                    showLoading();
                    HTTPAsync async = new HTTPAsync(ChangePasswordActivity.this, ChangePasswordActivity.this, Constants.CHANGE_PASSWORD, "", passwordRequestJSON,
                            GET_CHANGE_PASSWORD_RESPONSE, HTTPAsync.METHOD_POST);
                    async.execute();
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(textFace);
                    errorDescriptionText.setTypeface(textFace);
                    searchButton.setTypeface(textFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(ChangePasswordActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                showLoading();
                                HTTPAsync async = new HTTPAsync(ChangePasswordActivity.this, ChangePasswordActivity.this, Constants.CHANGE_PASSWORD, "", passwordRequestJSON,
                                        GET_CHANGE_PASSWORD_RESPONSE, HTTPAsync.METHOD_POST);
                                async.execute();
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_user_change_password);
        mTitleText.setTypeface(tf);
        backText.setTypeface(textFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(ChangePasswordActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
    }

    private String handleChangePasswordTask() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(ChangePasswordActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(ChangePasswordActivity.this));
            mainJSON.put("source", sourceJSON);
            mainJSON.put("emailId", pref.getString(Constants.MEMBER_EMAIL, ""));
            mainJSON.put("currentPassword", oldPassword.getText().toString());
            mainJSON.put("newPassword", newPassword.getText().toString());
            mainJSON.put("reTypePassword", confirmPassword.getText().toString());
            //mainJSON.put("userUniqueId", pref.getString(Constants.USER_UNIQUE_ID, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "DATA:: " + data);
        closeLoading();
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        } else {
            try {
                JSONObject obj = new JSONObject();
                obj = new JSONObject(data);
                if (obj.has("status")) {
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("FAILURE")) {
                        displayErrorMessage(getString(R.string.label_error_invalid_credentials), false);
                        return;
                    } else {
                        displayErrorMessage(getString(R.string.label_password_updated), true);
                        oldPassword.setText("");
                        newPassword.setText("");
                        confirmPassword.setText("");
                        confirmPassword.clearFocus();
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void displayErrorMessage(String message, boolean isSuccess) {
        errorMessageText.setText(message);
        if (isSuccess) {
            errorView.setBackgroundResource(R.color.success_message_background);
        } else {
            errorView.setBackgroundResource(R.color.error_message_background);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(textFace);
        descriptionText.setVisibility(View.GONE);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isAppReLaunched) {
            Utils.printMessage(TAG, "App Successfully Re Launched");
            isAppReLaunched = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Utils.isAppIsInBackground(ChangePasswordActivity.this)) {
            isAppReLaunched = true;
        }
    }

    @Override
    public void dataResponse(String message, String callType) {
        if (message.equalsIgnoreCase("Success")) {
            String passwordRequestJSON = handleChangePasswordTask();
            showLoading();
            HTTPAsync async = new HTTPAsync(ChangePasswordActivity.this, ChangePasswordActivity.this,
                    Constants.CHANGE_PASSWORD, "", passwordRequestJSON, GET_CHANGE_PASSWORD_RESPONSE, HTTPAsync.METHOD_POST);
            async.execute();
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
    }
}
