package com.flyin.bookings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.dialog.SignInDialog;
import com.flyin.bookings.listeners.TokenUpdateListener;
import com.flyin.bookings.model.CurrencyBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;
import com.google.gson.Gson;
import com.webengage.sdk.android.User;
import com.webengage.sdk.android.WebEngage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

@SuppressWarnings("ALL")
public class MainSearchActivity extends AppCompatActivity implements AsyncTaskListener, TokenUpdateListener {
    public ArrayList<Fragment> stack = new ArrayList<>();
    boolean isArabicLang;
    Toolbar toolbar;
    private ImageView menu, app_icon;
    private TextView signIn = null;
    private TabLayout tabLayout;
    private SignInDialog dialog;
    private boolean isSingInClicked = true;
    private SharedPreferences preferences = null;
    private RelativeLayout loadingViewLayout = null;
    private String savedCurrency = "";
    private ImageView errorImage = null;
    private Button searchButton = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private static final int GET_ACCESS_TOKEN = 1;
    private static final int GET_CURRENCY_DATA = 2;
    private static final int VERSION_UPDATE_SERVICE = 3;
    private static final int USER_LOGOUT_SERVICE = 4;
    private static final int GET_REFRESH_TOKEN = 5;
    private static final String TAG = "MainSearchActivity";
    private Typeface titleFace, textFace;
    private boolean isInternetPresent = false;
    private int loginDetailsSuccess = -1;
    private RefreshTokenService refreshTokenData = new RefreshTokenService();
    private ProgressDialog barProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Constants.IS_APP_LAUNCH)) {
                if (bundle.getBoolean(Constants.IS_APP_LAUNCH, false)) {
                    //checkForUpdate();
                }
            }
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initialLayout();
            }
        }, 200);
        //displayAlertDialog(true, true, "New update available (2.1)", "Make your next trip more easier, faster and get new features .", "");
    }

    private void checkForUpdate() {
        HTTPAsync async = new HTTPAsync(MainSearchActivity.this, MainSearchActivity.this,
                Constants.VERSION_UPDATE_URL, "", "", VERSION_UPDATE_SERVICE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void initialLayout() {
        menu = (ImageView) toolbar.findViewById(R.id.menu_drawer);
        app_icon = (ImageView) toolbar.findViewById(R.id.app_name);
        signIn = (TextView) toolbar.findViewById(R.id.sign_in);
        setSupportActionBar(toolbar);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
//        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
//        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
//        if (tokenTime == -1) {
//            handleRefreshToken();
//        } else {
//            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
//            if (diff > Long.parseLong(expireIn)) {
//                handleRefreshToken();
//            }
//        }
        isArabicLang = false;
        String fontTitle = Constants.FONT_ROBOTO_MEDIUM;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(MainSearchActivity.this)) {
            isArabicLang = true;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
            signIn.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        titleFace = Typeface.createFromAsset(getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(getAssets(), fontText);
        Typeface regularFace = Typeface.createFromAsset(getAssets(), fontRegular);
        signIn.setTypeface(regularFace);
        menu.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainSearchActivity.this,
                        MenuActivity.class);
                startActivity(intent);
            }
        });
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new SignInDialog(MainSearchActivity.this);
                isInternetPresent = Utils.isConnectingToInternet(MainSearchActivity.this);
                if (isInternetPresent) {
                    handleSignInSignOutFnctn();
                } else {
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(textFace);
                    errorDescriptionText.setTypeface(textFace);
                    searchButton.setTypeface(textFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(MainSearchActivity.this);
                            if (isInternetPresent) {
                                handleSignInSignOutFnctn();
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        });
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        pushFragment(new HotelSearchFragment());
        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
                                               @Override
                                               public void onTabSelected(TabLayout.Tab tab) {
                                                   int imgPadding = (int) getResources().getDimension(R.dimen.userDetails_margin);
                                                   super.onTabSelected(tab);
                                                   if (tab.getPosition() == 0) {
                                                       tabLayout.getTabAt(0).setCustomView(null);
                                                       TextView tabOne = (TextView) LayoutInflater.from(MainSearchActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabOne.setText(getString(R.string.label_search_flights));
                                                       tabOne.setTypeface(titleFace);
                                                       if (isArabicLang) {
                                                           tabOne.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                           tabOne.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arabic_flightselected, 0);
                                                       } else {
                                                           tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.flightselected, 0, 0, 0);
                                                       }
                                                       tabOne.setCompoundDrawablePadding(imgPadding);
                                                       tabOne.setTextColor(Utils.getColor(MainSearchActivity.this, R.color.text_select_color));
                                                       tabLayout.getTabAt(0).setCustomView(tabOne);
                                                   } else {
                                                       tabLayout.getTabAt(1).setCustomView(null);
                                                       TextView tabTwo = (TextView) LayoutInflater.from(MainSearchActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabTwo.setText(getString(R.string.label_hotels));
                                                       tabTwo.setTypeface(titleFace);
                                                       if (isArabicLang) {
                                                           tabTwo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                           tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.hotelselected, 0);
                                                       } else {
                                                           tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hotelselected, 0, 0, 0);
                                                       }
                                                       tabTwo.setCompoundDrawablePadding(imgPadding);
                                                       tabTwo.setTextColor(Utils.getColor(MainSearchActivity.this, R.color.text_select_color));
                                                       tabLayout.getTabAt(1).setCustomView(tabTwo);
                                                   }
                                               }

                                               @Override
                                               public void onTabUnselected(TabLayout.Tab tab) {
                                                   super.onTabUnselected(tab);
                                                   int imgPadding = (int) getResources().getDimension(R.dimen.userDetails_margin);
                                                   if (tab.getPosition() == 0) {
                                                       tabLayout.getTabAt(0).setCustomView(null);
                                                       TextView tabOne = (TextView) LayoutInflater.from(MainSearchActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabOne.setText(getString(R.string.label_search_flights));
                                                       tabOne.setTypeface(titleFace);
                                                       if (isArabicLang) {
                                                           tabOne.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                           tabOne.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arabic_flightnormal, 0);
                                                       } else {
                                                           tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.flightnormal, 0, 0, 0);
                                                       }
                                                       tabOne.setCompoundDrawablePadding(imgPadding);
                                                       tabOne.setTextColor(Utils.getColor(MainSearchActivity.this, R.color.white_color));
                                                       tabLayout.getTabAt(0).setCustomView(tabOne);
                                                   } else {
                                                       tabLayout.getTabAt(1).setCustomView(null);
                                                       TextView tabTwo = (TextView) LayoutInflater.from(MainSearchActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabTwo.setText(getString(R.string.label_hotels));
                                                       tabTwo.setTypeface(titleFace);
                                                       if (isArabicLang) {
                                                           tabTwo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                           tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.hotelnormal, 0);
                                                       } else {
                                                           tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hotelnormal, 0, 0, 0);
                                                       }
                                                       tabTwo.setCompoundDrawablePadding(imgPadding);
                                                       tabTwo.setTextColor(Utils.getColor(MainSearchActivity.this, R.color.white_color));
                                                       tabLayout.getTabAt(1).setCustomView(tabTwo);
                                                   }
                                               }

                                               @Override
                                               public void onTabReselected(TabLayout.Tab tab) {
                                                   super.onTabReselected(tab);
                                               }
                                           }
        );
        checkUserAccount();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String accessTkn = pref.getString(Constants.ACCESS_TOKEN, "");
        if (!accessTkn.equalsIgnoreCase("")) {
            String tempCurrency = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
            String currencyAvailable = pref.getString(Constants.ALL_CURRENCY, "");
            if (!savedCurrency.equalsIgnoreCase(tempCurrency) || currencyAvailable.equalsIgnoreCase("")) {
                loadCurrencyData();
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
        }
        return true;
    }

    public void pushFragment(Fragment frag) {
        if (frag instanceof HotelListFragment) {
            findViewById(R.id.tab_with_viewpager_view).setVisibility(View.GONE);
            loadingViewLayout.setVisibility(View.GONE);
            Fragment searchFrag = stack.get(0);
            stack.clear();
            stack.add(searchFrag);
        } else if (frag instanceof MapFragment) {
            Iterator itr = stack.iterator();
            while (itr.hasNext()) {
                Fragment fragment = (Fragment) itr.next();
                if (fragment instanceof HotelSearchFragment || fragment instanceof HotelListFragment)
                    continue;
                itr.remove();
            }
        }
        boolean isFirstPage = false;
        Bundle bundle = frag.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isMainPage")) {
                if (bundle.getBoolean("isMainPage", false)) {
                    findViewById(R.id.tab_with_viewpager_view).setVisibility(View.GONE);
                    loadingViewLayout.setVisibility(View.GONE);
                    Fragment searchFrag = stack.get(0);
                    stack.clear();
                    stack.add(searchFrag);
                }
            }
            if (bundle.containsKey("isFirstPage")) {
                isFirstPage = bundle.getBoolean("isFirstPage");
            }
        }
        if (stack != null) {
            stack.add(frag);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.home_container, frag);
            try {
                ft.commitAllowingStateLoss();
            } catch (Exception e) {
            }
            if (isFirstPage) {
                try {
                    getSupportFragmentManager().executePendingTransactions();
                } catch (Exception e) {
                }
            }
            // shouldDisplayHomeUp();
        }
    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//        try {
//            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
//            childFragmentManager.setAccessible(true);
//            childFragmentManager.set(this, null);
//        } catch (NoSuchFieldException e) {
//            throw new RuntimeException(e);
//        } catch (IllegalAccessException e) {
//            throw new RuntimeException(e);
//        }
//    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        checkUserAccount();
    }

    public void popFrag() {
        if (stack == null) {
            super.onBackPressed();
            return;
        }
        Utils.printMessage(TAG, "stack size" + stack.size());
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (stack.size() > 1) {
            Fragment removingFrag = stack.get(stack.size() - 1);
            stack.remove(stack.size() - 1);
            Fragment fragment = stack.get(stack.size() - 1);
            if (fragment instanceof HotelSearchFragment) {
                Utils.printMessage(TAG, ":: HotelSearchFragment ::");
                SharedPreferences sharedpreferences = getSharedPreferences(Constants.FILTERPREFERENCES, Context.MODE_PRIVATE);
                sharedpreferences.edit().clear().commit();
                findViewById(R.id.tab_with_viewpager_view).setVisibility(View.VISIBLE);
                loadingViewLayout.setVisibility(View.VISIBLE);
            }
            Utils.printMessage(TAG, "fragment::" + fragment);
            FragmentTransaction ft = fragmentManager.beginTransaction();
            Utils.printMessage(TAG, "Removing::" + removingFrag.isRemoving());
            ft.replace(R.id.home_container, fragment);
            ft.commitAllowingStateLoss();
//            getSupportFragmentManager().executePendingTransactions();
        } else if (stack.size() == 1) {
            super.onBackPressed();
            return;
        }
    }

    public void hideToolbar() {
        toolbar.setVisibility(View.GONE);
    }

    public void showToolbar() {
        toolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        popFrag();
    }

    /**
     * Request for Access token
     **/
    private void getTokenFromServer() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(MainSearchActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(MainSearchActivity.this, MainSearchActivity.this, Constants.CLIENT_AUTHENTICATION,
                "", obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void loadCurrencyData() {
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(MainSearchActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(MainSearchActivity.this));
            mainJSON.put("source", sourceJSON);
            mainJSON.put("baseCurrency", "SAR");
            mainJSON.put("type", "All");
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(MainSearchActivity.this, MainSearchActivity.this, Constants.CURRENCY_URL,
                "", mainJSON.toString(), GET_CURRENCY_DATA, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, " === DATA RESPONSE ==== " + data);
        if (serviceType == GET_CURRENCY_DATA) {
            SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
            savedCurrency = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
            Utils.printMessage(TAG, "SAVED CURRENCY :: " + savedCurrency);
            try {
                JSONObject obj = new JSONObject(data);
                if (obj.has("currencyInfos")) {
                    JSONArray arr = obj.getJSONArray("currencyInfos");
                    for (int i = 0; i < arr.length(); i++) {
                        if (arr.getJSONObject(i).has("currency")) {
                            if (arr.getJSONObject(i).getString("currency").equalsIgnoreCase(savedCurrency)) {
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString(Constants.SELECTED_CURRENCY_VALUE, arr.getJSONObject(i).getString("exchangeRate"));
                                editor.putString(Constants.SELECTED_CURRENCY_BUFFER_RATE, arr.getJSONObject(i).getString("bufferRate"));
                                editor.apply();
                                break;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.printMessage(TAG, "Exception occured.." + e.getMessage());
            }
            Gson gson = new Gson();
            CurrencyBean currencyBean;
            try {
                JSONObject obj = new JSONObject(data);
                if (obj.has("currencyInfos")) {
                    ArrayList<CurrencyBean> list = new ArrayList<>();
                    JSONArray arr = obj.getJSONArray("currencyInfos");
                    for (int i = 0; i < arr.length(); i++) {
                        if (arr.getJSONObject(i).has("currency")) {
                            currencyBean = new CurrencyBean();
                            currencyBean.setCurrency(arr.getJSONObject(i).getString("currency"));
                            currencyBean.setBufferRate(arr.getJSONObject(i).getString("bufferRate"));
                            currencyBean.setExchangeRate(arr.getJSONObject(i).getString("exchangeRate"));
                            list.add(currencyBean);
                        }
                    }
                    String jsonInString = gson.toJson(list);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString(Constants.ALL_CURRENCY, jsonInString);
                    editor.apply();
                }
            } catch (Exception e) {
            }
        } else if (serviceType == GET_ACCESS_TOKEN) {
            if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
                handleSomeThingWrongErr();
            } else {
                JSONObject obj = null;
                try {
                    obj = new JSONObject(data);
                    String accessToken = Utils.checkStringVal(obj.getString("accessToken"));
                    if (!accessToken.equalsIgnoreCase("null") && !accessToken.equalsIgnoreCase("")) {
                        String expireIn = obj.getString("expireIn");
                        String refreshToken = obj.getString("refreshToken");
                        String timeZone = "";
                        if (obj.has("tzone")) {
                            timeZone = obj.getString("tzone");
                        }
                        long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                        preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(Constants.ACCESS_TOKEN, accessToken);
                        editor.putString(Constants.EXPIRE_IN, expireIn);
                        editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                        editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                        editor.putString(Constants.SERVER_TIME_MILLI, timeZone);
                        editor.commit();
                        if (!accessToken.equalsIgnoreCase("")) {
                            String tempCurrency = preferences.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
                            if (!savedCurrency.equalsIgnoreCase(tempCurrency)) {
                                loadCurrencyData();
                            }
                        }
                    } else {
                        handleSomeThingWrongErr();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (serviceType == VERSION_UPDATE_SERVICE) {
            try {
                JSONObject object = new JSONObject(data);
                if (object.has("android")) {
                    String versionCode = BuildConfig.VERSION_NAME;
                    if (object.getJSONObject("android").has("version")) {
                        if (versionCode.equalsIgnoreCase(object.getJSONObject("android").getString("version"))) {
                            return; // both versions are same
                        }
                    }
                    if (object.getJSONObject("android").has("app_availability")) {
                        if (!object.getJSONObject("android").getBoolean("app_availability")) {
                            String msg = "";
                            if (Utils.isArabicLangSelected(MainSearchActivity.this)) {
                                msg = object.getJSONObject("android").getString("message_ar");
                            } else {
                                msg = object.getJSONObject("android").getString("message_en");
                            }
                            displayAlertDialog(object.getJSONObject("android").getBoolean("app_availability"), true, "", msg, "");
                        } else {
                            if (object.getJSONObject("android").has("force_update")) {
                                String title = "";
                                String message = "";
                                if (Utils.isArabicLangSelected(MainSearchActivity.this)) {
                                    title = object.getJSONObject("android").getJSONObject("ar").getString("title");
                                    message = object.getJSONObject("android").getJSONObject("ar").getString("message");
                                } else {
                                    title = object.getJSONObject("android").getJSONObject("en").getString("title");
                                    message = object.getJSONObject("android").getJSONObject("en").getString("message");
                                }
                                displayAlertDialog(true, object.getJSONObject("android").getBoolean("force_update"), title, message, object.getJSONObject("android").getString("url"));
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
        } else if (serviceType == USER_LOGOUT_SERVICE) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(data);
                String statusVal = obj.getString("status");
                if (statusVal.equalsIgnoreCase("SUCCESS")) {
                    SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                    pref.edit().putString(Constants.MEMBER_EMAIL, "").apply();
                    pref.edit().putString(Constants.MEMBER_ACCESS_TOKEN, "").apply();
                    pref.edit().putString(Constants.MEMBER_TOKEN_TYPE, "").apply();
                    pref.edit().putString(Constants.USER_UNIQUE_ID, "").apply();
                    pref.edit().putBoolean(Constants.isFanClub, false).apply();
                    pref.edit().putBoolean(Constants.IS_REWARDS_ENABLED, false).apply();
                    checkUserAccount();
                    flightSearchFragment.refreshBackground();
                    hotelSearchFragment.refreshBackground();
                    closeLoading();
                } else {
                    closeLoading();
                }
            } catch (Exception e) {
                e.printStackTrace();
                closeLoading();
            }
        } else if (serviceType == GET_REFRESH_TOKEN) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(data);
                String accessToken = Utils.checkStringVal(obj.getString("accessToken"));
                if (!accessToken.equalsIgnoreCase("null") && !accessToken.equalsIgnoreCase("")) {
                    String expireIn = obj.getString("expireIn");
                    String refreshToken = obj.getString("refreshToken");
                    String timeZone = "";
                    if (obj.has("tzone")) {
                        timeZone = obj.getString("tzone");
                    }
                    long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                    preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Constants.ACCESS_TOKEN, accessToken);
                    editor.putString(Constants.EXPIRE_IN, expireIn);
                    editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                    editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                    editor.putString(Constants.SERVER_TIME_MILLI, timeZone);
                    editor.commit();
                } else {
                    handleSomeThingWrongErr();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void displayAlertDialog(Boolean appAvailability, Boolean forceUpdate, String title, String message, String url) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        if (appAvailability) {
            alertDialogBuilder.setPositiveButton(getString(R.string.update_lbl),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            final String appPackageName = getPackageName();
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                    });
            if (!forceUpdate) {
                alertDialogBuilder.setNegativeButton(getString(R.string.remind_me_later_lbl), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
            }
        }
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
//        alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLUE);
//        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.BLUE);
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
    }

    /**
     * Loading Error Based on response
     **/
    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private FlightSearchFragment flightSearchFragment = null;
    private HotelSearchFragment hotelSearchFragment = null;

    /**
     * Set View pager to the Screen
     **/
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        flightSearchFragment = new FlightSearchFragment();
        hotelSearchFragment = new HotelSearchFragment();
        adapter.addFragment(flightSearchFragment, getString(R.string.label_search_flights));
        adapter.addFragment(hotelSearchFragment, getString(R.string.label_hotels));
        viewPager.setAdapter(adapter);
    }

    /**
     * Adding View pager
     **/
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setupTabIcons() {
        int imgPadding = (int) getResources().getDimension(R.dimen.userDetails_margin);
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText(getString(R.string.label_search_flights));
        tabOne.setTypeface(titleFace);
        if (isArabicLang) {
            tabOne.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            tabOne.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arabic_flightselected, 0);
        } else {
            tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.flightselected, 0, 0, 0);
        }
        tabOne.setTextColor(Utils.getColor(MainSearchActivity.this, R.color.text_select_color));
        tabOne.setCompoundDrawablePadding(imgPadding);
        tabLayout.getTabAt(0).setCustomView(tabOne);
        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText(getString(R.string.label_hotels));
        tabTwo.setTypeface(titleFace);
        if (isArabicLang) {
            tabTwo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.hotelnormal, 0);
        } else {
            tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hotelnormal, 0, 0, 0);
        }
        tabTwo.setTextColor(Utils.getColor(MainSearchActivity.this, R.color.white_color));
        tabTwo.setCompoundDrawablePadding(imgPadding);
        tabLayout.getTabAt(1).setCustomView(tabTwo);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void checkUserAccount() {
        if (signIn != null) {
            SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
            String memberAccessToken = pref.getString(Constants.MEMBER_ACCESS_TOKEN, "");
            if (!memberAccessToken.equalsIgnoreCase("")) {
                signIn.setText(getResources().getString(R.string.label_action_sign_out));
                isSingInClicked = false;
                flightSearchFragment.refreshBackground();
                hotelSearchFragment.refreshBackground();
            } else if (memberAccessToken.equalsIgnoreCase("")) {
                signIn.setText(getResources().getString(R.string.label_action_signin));
                isSingInClicked = true;
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Constants.MEMBER_EMAIL, "").apply();
                editor.putString(Constants.MEMBER_TOKEN_TYPE, "").apply();
                editor.putString(Constants.USER_UNIQUE_ID, "").apply();
                editor.putBoolean(Constants.isFanClub, false).apply();
                editor.putBoolean(Constants.IS_REWARDS_ENABLED, false).apply();
                flightSearchFragment.refreshBackground();
                hotelSearchFragment.refreshBackground();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (loginDetailsSuccess == 1) {
            dialog.passFacebookResult(requestCode, resultCode, data);
        }
        if (stack != null && stack.size() > 0) {
            Fragment searchFrag = stack.get(stack.size() - 1);
            if (searchFrag instanceof HotelSearchFragment) {
                if (tabLayout.getSelectedTabPosition() == 0) {
                    if (flightSearchFragment != null) {
                        flightSearchFragment.onActivityResult(requestCode, resultCode, data);
                    }
                } else {
                    if (hotelSearchFragment != null) {
                        hotelSearchFragment.onActivityResult(requestCode, resultCode, data);
                    }
                }
            } else {
                if (searchFrag != null) {
                    searchFrag.onActivityResult(requestCode, resultCode, data);
                }
            }
            Utils.printMessage(TAG, "searchFrag::" + searchFrag);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // below line to be commented to prevent crash on nougat.
        //
        //super.onSaveInstanceState(outState);
    }

    private void handleSignInSignOutFnctn() {
        final SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        if (!isSingInClicked) {
            //checkRefreshTokenTime("Logout");
            handleLogoutFunction();
            dialog.callFacebookLogout();
            User weUser = WebEngage.get().user();
            weUser.logout();
            signIn.setText(getResources().getString(R.string.label_action_signin));
            isSingInClicked = true;
            flightSearchFragment.refreshBackground();
            hotelSearchFragment.refreshBackground();
        } else {
            dialog.setCancelable(false);
            loginDetailsSuccess = 1;
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    String memberAccessToken = pref.getString(Constants.MEMBER_ACCESS_TOKEN, "");
                    if (!memberAccessToken.equalsIgnoreCase("")) {
                        signIn.setText(getString(R.string.label_action_sign_out));
                        isSingInClicked = false;
                    } else {
                        signIn.setText(getString(R.string.label_action_signin));
                        isSingInClicked = true;
                    }
                    loginDetailsSuccess = -1;
                    dialog = null;
                    flightSearchFragment.refreshBackground();
                    hotelSearchFragment.refreshBackground();
                }
            });
            dialog.show();
        }
    }

    private void handleLogoutFunction() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject obj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", pref.getString(Constants.MEMBER_ACCESS_TOKEN, ""));
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("timeStamp", timeStamp);
            obj.put("source", sourceJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        showLoading();
        HTTPAsync async = new HTTPAsync(MainSearchActivity.this, MainSearchActivity.this, Constants.USER_LOGOUT,
                "", obj.toString(), USER_LOGOUT_SERVICE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void handleRefreshToken() {
        JSONObject obj = new JSONObject();
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.REFRESH_GRANT_TYPE);
            obj.put("refreshToken", pref.getString(Constants.REFRESH_TOKEN, ""));
            if (!pref.getString(Constants.MEMBER_ACCESS_TOKEN, "").equalsIgnoreCase("")) {
                obj.put("mtoken", pref.getString(Constants.MEMBER_ACCESS_TOKEN, ""));
            }
        } catch (Exception e) {
        }
        HTTPAsync async = new HTTPAsync(MainSearchActivity.this, MainSearchActivity.this,
                Constants.CLIENT_REFRESH_TOKEN, "", obj.toString(), GET_REFRESH_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void handleSomeThingWrongErr() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View errorView = inflater.inflate(R.layout.view_error_response, null);
        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
        errorText = (TextView) errorView.findViewById(R.id.error_text);
        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
        searchButton = (Button) errorView.findViewById(R.id.search_button);
        errorText.setTypeface(textFace);
        errorDescriptionText.setTypeface(textFace);
        searchButton.setTypeface(textFace);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loadErrorType(Constants.WRONG_ERROR_PAGE);
        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(errorView);
    }

    private void checkRefreshTokenTime(String callType) {
        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
        if (tokenTime == -1) {
            refreshTokenData.checkServiceCall(MainSearchActivity.this, MainSearchActivity.this, callType);
        } else {
            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
            if (diff > Long.parseLong(expireIn)) {
                refreshTokenData.checkServiceCall(MainSearchActivity.this, MainSearchActivity.this, callType);
            }
        }
    }

    @Override
    public void dataResponse(String message, String callType) {
        if (message.equalsIgnoreCase("Success")) {
            if (callType.equalsIgnoreCase("Logout")) {
                handleSignInSignOutFnctn();
            }
        } else {
            handleSomeThingWrongErr();
        }
    }

    private void showLoading() {
        if (barProgressDialog == null) {
            barProgressDialog = new ProgressDialog(MainSearchActivity.this);
            barProgressDialog.setMessage(getResources().getString(R.string.label_wait_please_message));
            barProgressDialog.setProgressStyle(barProgressDialog.STYLE_SPINNER);
            barProgressDialog.setCancelable(false);
            barProgressDialog.show();
        }
    }

    private void closeLoading() {
        if (barProgressDialog != null) {
            barProgressDialog.dismiss();
            barProgressDialog = null;
        }
    }
}
