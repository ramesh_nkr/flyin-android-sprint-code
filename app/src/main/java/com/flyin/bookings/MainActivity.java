package com.flyin.bookings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.flyin.bookings.model.AirMealList;
import com.flyin.bookings.model.CountryNamesList;
import com.flyin.bookings.util.CSVFile;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.DbHandler;
import com.flyin.bookings.util.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;

import java.io.InputStream;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private boolean isNew = true;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main_main);
        String fontPath = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_DROIDKUFI_REGULAR;
        TextView englishLanguage = (TextView) findViewById(R.id.english_language);
        TextView arabicLanguage = (TextView) findViewById(R.id.arabic_language);
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontText);
        englishLanguage.setTypeface(tf);
        arabicLanguage.setTypeface(textFace);
        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey("isNew")) {
                isNew = b.getBoolean("isNew");
            }
        }
//        FirebaseCrash.report(new Exception("My first Android non-fatal error"));
//        FirebaseCrash.log("MainActivity created");
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "123");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Rakesh");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        Utils.printMessage(" MainActivity mFirebaseAnalytics", "mFirebaseAnalytics" );
         findViewById(R.id.english_lang).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
                editor.apply();
                Intent intent = new Intent(MainActivity.this, MainSearchActivity.class);
                intent.putExtra(Constants.IS_APP_LAUNCH, true);
                startActivity(intent);
            }
        });
        findViewById(R.id.arabic_lang).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String languageToLoad = "ar";
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Constants.USER_SELECTED_LANGUAGE, "ar_SA");
                editor.apply();
                finish();
                Intent intent = new Intent(MainActivity.this, MainSearchActivity.class);
                intent.putExtra(Constants.IS_APP_LAUNCH, true);
                startActivity(intent);
            }
        });
        if (preferences.getBoolean(Constants.IS_FIRST_TIME_LAUNCH, true)) {
            boolean isSuccess = loadAirMeal();
            loadCountryNames();
            if (isSuccess) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Constants.IS_FIRST_TIME_LAUNCH, false);
                editor.apply();
            }
        } else {
            SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
            String selectedLanguage = pref.getString(Constants.USER_SELECTED_LANGUAGE, "");
            if (selectedLanguage.isEmpty()) {
                return;
            }
            if (selectedLanguage.equalsIgnoreCase(Constants.LANGUAGE_ENGLISH_CODE)) {
                String languageToLoad = "en";
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                }
                String languageToLoad = "ar";
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            }
            finish();
            Intent intent = new Intent(MainActivity.this, MainSearchActivity.class);
            intent.putExtra(Constants.IS_APP_LAUNCH, isNew);
            startActivity(intent);
        }
//        DbHandler dbHandler = new DbHandler(MainActivity.this);
//        dbHandler.getDatabase();
    }

    public boolean loadCountryNames() {
        InputStream inputStream = getResources().openRawResource(R.raw.countrynames);
        CSVFile csvFile = new CSVFile(inputStream);
        List<CountryNamesList> countryNamesList = csvFile.access();
        DbHandler dbHandler = new DbHandler(MainActivity.this);
        return dbHandler.addCountryNamesList(countryNamesList);
    }

    public boolean loadAirMeal() {
        InputStream inputStream = getResources().openRawResource(R.raw.airmeal);
        CSVFile csvFile = new CSVFile(inputStream);
        List<AirMealList> airMealList = csvFile.load();
        DbHandler dbHandler = new DbHandler(MainActivity.this);
        return dbHandler.addAirMealList(airMealList);
    }
}