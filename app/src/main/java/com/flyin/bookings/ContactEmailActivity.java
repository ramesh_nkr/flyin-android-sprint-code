package com.flyin.bookings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONObject;

public class ContactEmailActivity extends AppCompatActivity implements AsyncTaskListener {
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private Button submitButton = null;
    private Button searchButton = null;
    private EditText firstNameEditText, lastNameEditText, emailEditText, phoneEditText, subjectEditText, messageEditText;
    private Typeface textFace = null;
    private RelativeLayout loadingViewLayout = null;
    private boolean isInternetPresent = false;
    private LayoutInflater inflater = null;
    private ImageView errorImage = null;
    private static final int GET_RESPONSE = 1;
    private static final String TAG = "ContactEmailActivity";
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private MyReceiver myReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus_email);
        isInternetPresent = Utils.isConnectingToInternet(ContactEmailActivity.this);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_LIGHT;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        TextView contactFirstHeader = (TextView) findViewById(R.id.contact_first_header);
        TextView contactNumber = (TextView) findViewById(R.id.contact_number);
        TextView contactSecondHeader = (TextView) findViewById(R.id.contact_second_header);
        TextView contactEmail = (TextView) findViewById(R.id.email_contact_text);
        submitButton = (Button) findViewById(R.id.contact_submit_button);
        firstNameEditText = (EditText) findViewById(R.id.contact_first_name_editText);
        lastNameEditText = (EditText) findViewById(R.id.contact_last_name_editText);
        emailEditText = (EditText) findViewById(R.id.contact_mail_editText);
        phoneEditText = (EditText) findViewById(R.id.contact_mobile_editText);
        subjectEditText = (EditText) findViewById(R.id.contact_subject_editText);
        messageEditText = (EditText) findViewById(R.id.contact_message_editText);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        firstNameEditText.requestFocus();
        if (Utils.isArabicLangSelected(ContactEmailActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                firstNameEditText.setTextDirection(View.TEXT_DIRECTION_RTL);
                lastNameEditText.setTextDirection(View.TEXT_DIRECTION_RTL);
                emailEditText.setTextDirection(View.TEXT_DIRECTION_RTL);
                phoneEditText.setTextDirection(View.TEXT_DIRECTION_RTL);
                subjectEditText.setTextDirection(View.TEXT_DIRECTION_RTL);
                messageEditText.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
            contactFirstHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            contactNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_header));
            contactSecondHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            contactEmail.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            firstNameEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            lastNameEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            emailEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            phoneEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            subjectEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            messageEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            submitButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textFace = Typeface.createFromAsset(getAssets(), fontTitle);
        Typeface titleFace = Typeface.createFromAsset(getAssets(), fontText);
        contactFirstHeader.setTypeface(tf);
        contactNumber.setTypeface(tf);
        contactSecondHeader.setTypeface(tf);
        contactEmail.setTypeface(tf);
        submitButton.setTypeface(tf);
        firstNameEditText.setTypeface(titleFace);
        lastNameEditText.setTypeface(titleFace);
        emailEditText.setTypeface(titleFace);
        phoneEditText.setTypeface(titleFace);
        subjectEditText.setTypeface(titleFace);
        messageEditText.setTypeface(titleFace);
        firstNameEditText.setHint(Utils.getMandatoryContactFormFieldText(getString(R.string.label_traveller_firstName)));
        lastNameEditText.setHint(Utils.getMandatoryContactFormFieldText(getString(R.string.label_traveller_lastName)));
        emailEditText.setHint(Utils.getMandatoryContactFormFieldText(getString(R.string.label_traveller_email)));
        phoneEditText.setHint(Utils.getMandatoryContactFormFieldText(getString(R.string.label_traveller_mobile)));
        subjectEditText.setHint(Utils.getMandatoryContactFormFieldText(getString(R.string.label_contact_email_subject)));
        messageEditText.setHint(Utils.getMandatoryContactFormFieldText(getString(R.string.label_contact_email_message)));
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (firstNameEditText.getText().toString().isEmpty()) {
                    displayErrorMessage(getString(R.string.label_err_fname_msg));
                    errorView.setBackgroundResource(R.color.error_message_background);
                    return;
                }
                if (!Utils.isValidName(firstNameEditText.getText().toString())) {
                    displayErrorMessage(getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_firstName));
                    errorView.setBackgroundResource(R.color.error_message_background);
                    return;
                }
                if (lastNameEditText.getText().toString().isEmpty()) {
                    displayErrorMessage(getString(R.string.label_err_lname_msg));
                    errorView.setBackgroundResource(R.color.error_message_background);
                    return;
                }
                if (!Utils.isValidName(lastNameEditText.getText().toString())) {
                    displayErrorMessage(getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_lastName));
                    errorView.setBackgroundResource(R.color.error_message_background);
                    return;
                }
                if (emailEditText.getText().toString().isEmpty()) {
                    displayErrorMessage(getString(R.string.label_error_email_message));
                    errorView.setBackgroundResource(R.color.error_message_background);
                    return;
                }
                if (!Utils.isValidEmail(emailEditText.getText().toString())) {
                    displayErrorMessage(getString(R.string.label_error_email_valid_message));
                    errorView.setBackgroundResource(R.color.error_message_background);
                    return;
                }
                if (phoneEditText.getText().toString().isEmpty() || !Utils.isValidMobile(phoneEditText.getText().toString())) {
                    displayErrorMessage(getString(R.string.label_err_phone_msg));
                    errorView.setBackgroundResource(R.color.error_message_background);
                    return;
                }
                if (subjectEditText.getText().toString().isEmpty() || subjectEditText.getText().toString().trim().equals("")) {
                    displayErrorMessage(getString(R.string.label_err_subject_msg));
                    errorView.setBackgroundResource(R.color.error_message_background);
                    return;
                }
                if (messageEditText.getText().toString().isEmpty() || messageEditText.getText().toString().trim().equals("")) {
                    displayErrorMessage(getString(R.string.label_err_message_msg));
                    errorView.setBackgroundResource(R.color.error_message_background);
                    return;
                }
                final String requestJSON = handleContactEmailTask();
                isInternetPresent = Utils.isConnectingToInternet(ContactEmailActivity.this);
                if (isInternetPresent) {
                    getRequestFromServer(requestJSON);
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(textFace);
                    errorDescriptionText.setTypeface(textFace);
                    searchButton.setTypeface(textFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(ContactEmailActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                getRequestFromServer(requestJSON);
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        });
        contactNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:920025959"));
                startActivity(intent);
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_user_contact);
        mTitleText.setTypeface(tf);
        backText.setTypeface(titleFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(ContactEmailActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(titleFace);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter("com.flyin.bookings.ERROR_MESSAGE");
        registerReceiver(myReceiver, intentFilter);
    }

    private String handleContactEmailTask() {
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject dataJSON = new JSONObject();
            dataJSON.put("firstName", firstNameEditText.getText().toString());
            dataJSON.put("lastName", lastNameEditText.getText().toString());
            dataJSON.put("phone", phoneEditText.getText().toString());
            dataJSON.put("email", emailEditText.getText().toString().toLowerCase());
            dataJSON.put("subject", subjectEditText.getText().toString().trim());
            dataJSON.put("message", messageEditText.getText().toString().trim());
            mainJSON.put("data", dataJSON);
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void getRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(ContactEmailActivity.this, ContactEmailActivity.this, Constants.CONTACT_EMAIL, "",
                requestJSON, GET_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        closeLoading();
        Utils.printMessage(TAG, "DATA:: " + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            displayErrorMessage(getString(R.string.label_something_went_wrong));
            errorView.setBackgroundResource(R.color.error_message_background);
        } else {
            JSONObject obj = new JSONObject();
            try {
                obj = new JSONObject(data);
                if (obj.has("status")) {
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        displayErrorMessage(getString(R.string.label_request_sent_msg));
                        errorView.setBackgroundResource(R.color.success_message_background);
                        firstNameEditText.setText("");
                        lastNameEditText.setText("");
                        emailEditText.setText("");
                        phoneEditText.setText("");
                        subjectEditText.setText("");
                        messageEditText.setText("");
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        submitButton.setText(getString(R.string.label_contact_email_submitting_form));
    }

    private void closeLoading() {
        submitButton.setText(getString(R.string.label_contact_email_submit_btn));
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.ERROR_MESSAGE")) {
                displayErrorMessage(intent.getStringExtra("message"));
            }
        }
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver);
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }
}