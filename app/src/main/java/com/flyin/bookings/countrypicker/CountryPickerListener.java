package com.flyin.bookings.countrypicker;

public interface CountryPickerListener {
    public void onSelectCountry(String name, String code);
}
