package com.flyin.bookings;

import android.content.Context;
import android.content.SharedPreferences;

import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONObject;

public class ServerTimeService implements AsyncTaskListener {
    private static final int GET_SERVER_TIME = 1;
    private SharedPreferences preferences = null;

    public void checkServiceCall(Context context) {
        preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        updateServerTime(context);
    }

    private void updateServerTime(Context context) {
        JSONObject obj = new JSONObject();
        try {
            preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            JSONObject sourceObj = new JSONObject();
            sourceObj.put("device", Constants.DEVICE_TYPE);
            sourceObj.put("accessToken", preferences.getString(Constants.ACCESS_TOKEN, ""));
            sourceObj.put("clientId", Constants.CLIENT_ID);
            sourceObj.put("clientSecret", Constants.CLIENT_SECRET);
            sourceObj.put("isocountry", Utils.getUserCountry(context));
            obj.put("source", sourceObj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(context, ServerTimeService.this, Constants.SERVER_TIME_URL, "",
                obj.toString(), GET_SERVER_TIME, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage("ServerTimeService", "DATA RESPONSE ==== " + data);
        JSONObject obj = null;
        try {
            obj = new JSONObject(data);
            if (obj.has("accessToken")) {
                String timeZone = "";
                if (obj.has("tzone")) {
                    timeZone = obj.getString("tzone");
                }
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Constants.SERVER_TIME_MILLI, timeZone).apply();
                if (!timeZone.isEmpty()) {
                    Long localMilliSeconds = System.currentTimeMillis();
                    Long resultMilli = Long.parseLong(timeZone) - localMilliSeconds;
                    int diff = resultMilli.intValue();
                    editor.putInt(Constants.SERVER_TIME_DIFF, diff).apply();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {

    }
}
