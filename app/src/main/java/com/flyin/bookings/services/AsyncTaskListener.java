package com.flyin.bookings.services;

public interface AsyncTaskListener {
    void onTaskComplete(String data, int serviceType);

    void onTaskCancelled(String data, int serviceType);
}
