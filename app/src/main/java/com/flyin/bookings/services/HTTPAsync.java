package com.flyin.bookings.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;

import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class HTTPAsync extends AsyncTask<Void, Void, String> {
    public static final String TAG = "HTTPAsync";
    private AsyncTaskListener mCallback = null;
    private String mUrl = null;
    private String mJsonString;
    private int serviceType;
    private int method;
    public static final int METHOD_GET = 1;
    public static final int METHOD_POST = 2;
    public static final int METHOD_PUT = 3;
    private Context context;

    public HTTPAsync(Context context, AsyncTaskListener callback, String baseURL, String logURL,
                     String jsonString, int serviceType, int method) {
        this.context = context;
        mCallback = callback;
        mUrl = baseURL + logURL;
        mJsonString = jsonString;
        this.serviceType = serviceType;
        this.method = method;
    }

    public HTTPAsync(Context context, AsyncTaskListener callback, String baseURL, String logURL,
                     int serviceType, int method) {
        mCallback = callback;
        mUrl = baseURL + logURL;
        this.serviceType = serviceType;
        this.method = method;
    }

    @Override
    protected String doInBackground(Void... params) {
        String result = "";
        switch (method) {
            case METHOD_GET:
                result = httpRestGetCall(mUrl);
                break;
            case METHOD_POST:
                result = httpRestPostCall(mUrl, mJsonString);
                break;
            case METHOD_PUT:
                result = null;
                break;
        }
        return result;
    }

    private String httpRestPostCall(String urlStr, String obj) {
        URL url;
        URLConnection urlConn;
        DataOutputStream printout;
        DataInputStream input;
        int timeoutConnection = 45 * 1000;
        Utils.printMessage(TAG, "URL ::" + urlStr);
        Utils.printMessage(TAG, "PAYLOAD ::" + obj);
        try {
            SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            url = new URL(urlStr);
            urlConn = url.openConnection();
            urlConn.setDoInput(true);
            urlConn.setDoOutput(true);
            urlConn.setUseCaches(false);
            String deviceMacId = Utils.getMacAddress();
            int diffVal = pref.getInt(Constants.SERVER_TIME_DIFF, 0);
            Long milliSeconds = System.currentTimeMillis() + diffVal;
            Utils.printMessage(TAG, "Milli Sec : "+milliSeconds);
            String convertedTime = String.valueOf(milliSeconds);
            String authorizationVal = Constants.CLIENT_ID + ":#" + Constants.CLIENT_SECRET + ":#" + convertedTime;
            String agentInfoVal = "android; U; CPU Android OS " + Build.VERSION.RELEASE + "; MAC " + deviceMacId + "; IP " + Utils.getIPAddress(true) + "; Time " + convertedTime;
            urlConn.setRequestProperty("Content-Type", "application/json");
            urlConn.setRequestProperty("Authorization", Utils.encryptInputVal(authorizationVal));
            if (!pref.getString(Constants.ACCESS_TOKEN, "").isEmpty()) {
                urlConn.setRequestProperty("Token", pref.getString(Constants.ACCESS_TOKEN, ""));
            }
            if (!pref.getString(Constants.MEMBER_ACCESS_TOKEN, "").isEmpty()) {
                urlConn.setRequestProperty("Otoken", pref.getString(Constants.MEMBER_ACCESS_TOKEN, ""));
            }
            urlConn.setRequestProperty("AgentInfo", agentInfoVal);
            if (!pref.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
                urlConn.setRequestProperty("Uid", pref.getString(Constants.USER_UNIQUE_ID, ""));
            }
            urlConn.setRequestProperty("Did", deviceMacId);
            Utils.printMessage(TAG, "Authorization :: " + Utils.encryptInputVal(authorizationVal));
            Utils.printMessage(TAG, "Token : " + pref.getString(Constants.ACCESS_TOKEN, ""));
            Utils.printMessage(TAG, "Otoken : " + pref.getString(Constants.MEMBER_ACCESS_TOKEN, ""));
            Utils.printMessage(TAG, "AgentInfo : " + agentInfoVal);
            Utils.printMessage(TAG, "Uid : " + pref.getString(Constants.USER_UNIQUE_ID, ""));
            Utils.printMessage(TAG, "Did : " + deviceMacId);
            urlConn.setReadTimeout(timeoutConnection);
            urlConn.connect();
            // Send POST output.
            printout = new DataOutputStream(urlConn.getOutputStream());
            byte[] data = obj.getBytes("UTF-8");
            printout.write(data);
            printout.flush();
            printout.close();
            // Receive the response from the server
            InputStream in = new BufferedInputStream(urlConn.getInputStream());
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
        return "";
    }

    private String httpRestGetCall(String urlStr) {
        URL url;
        int resCode = -1;
        URLConnection urlConn;
        try {
            url = new URL(urlStr);
            urlConn = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) urlConn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            Utils.printMessage(TAG, "URL ::" + urlStr);
            httpConn.connect();
            resCode = httpConn.getResponseCode();
            if (resCode == HttpURLConnection.HTTP_OK) {
                InputStream in = new BufferedInputStream(
                        urlConn.getInputStream());
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(in));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                return result.toString();
            }
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
        return "";
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result != null && mCallback != null) {
            mCallback.onTaskComplete(result, serviceType);
        } else {
            mCallback.onTaskCancelled(null, serviceType);
        }
    }
}
