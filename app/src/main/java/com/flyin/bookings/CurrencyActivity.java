package com.flyin.bookings;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

public class CurrencyActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView qmrSelectImage = null;
    private ImageView omrSelectImage = null;
    private ImageView kwdSelectImage = null;
    private ImageView egpSelectImage = null;
    private ImageView bhdSelectImage = null;
    private ImageView aedSelectImage = null;
    private ImageView eurSelectImage = null;
    private ImageView usdSelectImage = null;
    private ImageView sarSelectImage = null;
    private ImageView gbpSelectImage = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_LIGHT;
        String fontLight = Constants.FONT_ROBOTO_LIGHT;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        LinearLayout britishPoundLayout = (LinearLayout) findViewById(R.id.currency_gbp);
        LinearLayout saudiRiyalLayout = (LinearLayout) findViewById(R.id.currency_sar);
        LinearLayout dollarLayout = (LinearLayout) findViewById(R.id.currency_usd);
        LinearLayout euroLayout = (LinearLayout) findViewById(R.id.currency_eur);
        LinearLayout dirhamLayout = (LinearLayout) findViewById(R.id.currency_aed);
        LinearLayout bahrainDinarLayout = (LinearLayout) findViewById(R.id.currency_bhd);
        LinearLayout egyptPoundLayout = (LinearLayout) findViewById(R.id.currency_egp);
        LinearLayout kuwaitDinarLayout = (LinearLayout) findViewById(R.id.currency_kwd);
        LinearLayout omanRiyalLayout = (LinearLayout) findViewById(R.id.currency_omr);
        LinearLayout qatarRiyalLayout = (LinearLayout) findViewById(R.id.currency_qmr);
        TextView currencyBritishPound = (TextView) findViewById(R.id.currency_british_pound);
        TextView currencySaudiRiyal = (TextView) findViewById(R.id.currency_saudi_riyal);
        TextView currencyDollar = (TextView) findViewById(R.id.currency_dollar);
        TextView currencyEuro = (TextView) findViewById(R.id.currency_euro);
        TextView currencyDirham = (TextView) findViewById(R.id.currency_dirham);
        TextView currencyBahrainDinar = (TextView) findViewById(R.id.currency_bahrain_dinar);
        TextView currencyEgyptPound = (TextView) findViewById(R.id.currency_egypt_pound);
        TextView currencyKuwaitDinar = (TextView) findViewById(R.id.currency_kuwait_dinar);
        TextView currencyOmanRiyal = (TextView) findViewById(R.id.currency_oman_riyal);
        TextView currencyQatarRiyal = (TextView) findViewById(R.id.currency_qatar_riyal);
        TextView currencyBritishPoundCode = (TextView) findViewById(R.id.currency_british_pound_code);
        TextView currencySaudiRiyalCode = (TextView) findViewById(R.id.currency_saudi_riyal_code);
        TextView currencyDollarCode = (TextView) findViewById(R.id.currency_dollar_code);
        TextView currencyEuroCode = (TextView) findViewById(R.id.currency_euro_code);
        TextView currencyDirhamCode = (TextView) findViewById(R.id.currency_dirham_code);
        TextView currencyBahrainDinarCode = (TextView) findViewById(R.id.currency_bahrain_code);
        TextView currencyEgyptPoundCode = (TextView) findViewById(R.id.currency_egypt_code);
        TextView currencyKuwaitDinarCode = (TextView) findViewById(R.id.currency_kuwait_dinar_code);
        TextView currencyOmanRiyalCode = (TextView) findViewById(R.id.currency_oman_riyal_code);
        TextView currencyQatarRiyalCode = (TextView) findViewById(R.id.currency_qatar_riyal_code);
        if (Utils.isArabicLangSelected(CurrencyActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontLight = Constants.FONT_ROBOTO_LIGHT;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontTitle);
        Typeface lightFace = Typeface.createFromAsset(getAssets(), fontLight);
        Typeface titleFace = Typeface.createFromAsset(getAssets(), fontText);
        currencyBritishPound.setTypeface(textFace);
        currencySaudiRiyal.setTypeface(textFace);
        currencyDollar.setTypeface(textFace);
        currencyEuro.setTypeface(textFace);
        currencyDirham.setTypeface(textFace);
        currencyBahrainDinar.setTypeface(textFace);
        currencyEgyptPound.setTypeface(textFace);
        currencyKuwaitDinar.setTypeface(textFace);
        currencyOmanRiyal.setTypeface(textFace);
        currencyQatarRiyal.setTypeface(textFace);
        currencyBritishPoundCode.setTypeface(lightFace);
        currencySaudiRiyalCode.setTypeface(lightFace);
        currencyDollarCode.setTypeface(lightFace);
        currencyEuroCode.setTypeface(lightFace);
        currencyDirhamCode.setTypeface(lightFace);
        currencyBahrainDinarCode.setTypeface(lightFace);
        currencyEgyptPoundCode.setTypeface(lightFace);
        currencyKuwaitDinarCode.setTypeface(lightFace);
        currencyOmanRiyalCode.setTypeface(lightFace);
        currencyQatarRiyalCode.setTypeface(lightFace);
        britishPoundLayout.setOnClickListener(this);
        saudiRiyalLayout.setOnClickListener(this);
        dollarLayout.setOnClickListener(this);
        euroLayout.setOnClickListener(this);
        dirhamLayout.setOnClickListener(this);
        bahrainDinarLayout.setOnClickListener(this);
        egyptPoundLayout.setOnClickListener(this);
        kuwaitDinarLayout.setOnClickListener(this);
        omanRiyalLayout.setOnClickListener(this);
        qatarRiyalLayout.setOnClickListener(this);
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_currency_page_title);
        mTitleText.setTypeface(tf);
        backText.setTypeface(titleFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(CurrencyActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        qmrSelectImage = (ImageView) findViewById(R.id.qmrSelectImage);
        omrSelectImage = (ImageView) findViewById(R.id.omrSelectImage);
        kwdSelectImage = (ImageView) findViewById(R.id.kwdSelectImage);
        egpSelectImage = (ImageView) findViewById(R.id.egpSelectImage);
        bhdSelectImage = (ImageView) findViewById(R.id.bhdSelectImage);
        aedSelectImage = (ImageView) findViewById(R.id.aedSelectImage);
        eurSelectImage = (ImageView) findViewById(R.id.eurSelectImage);
        usdSelectImage = (ImageView) findViewById(R.id.usdSelectImage);
        sarSelectImage = (ImageView) findViewById(R.id.sarSelectImage);
        gbpSelectImage = (ImageView) findViewById(R.id.gbpSelectImage);
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String savedCurrency = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        resetAllChecks();
        switch (savedCurrency) {
            case "GBP":
                gbpSelectImage.setVisibility(View.VISIBLE);
                break;
            case "QAR":
                qmrSelectImage.setVisibility(View.VISIBLE);
                break;
            case "OMR":
                omrSelectImage.setVisibility(View.VISIBLE);
                break;
            case "KWD":
                kwdSelectImage.setVisibility(View.VISIBLE);
                break;
            case "EGP":
                egpSelectImage.setVisibility(View.VISIBLE);
                break;
            case "BHD":
                bhdSelectImage.setVisibility(View.VISIBLE);
                break;
            case "AED":
                aedSelectImage.setVisibility(View.VISIBLE);
                break;
            case "EUR":
                eurSelectImage.setVisibility(View.VISIBLE);
                break;
            case "USD":
                usdSelectImage.setVisibility(View.VISIBLE);
                break;
            case "SAR":
                sarSelectImage.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private void resetAllChecks() {
        qmrSelectImage.setVisibility(View.INVISIBLE);
        omrSelectImage.setVisibility(View.INVISIBLE);
        kwdSelectImage.setVisibility(View.INVISIBLE);
        egpSelectImage.setVisibility(View.INVISIBLE);
        bhdSelectImage.setVisibility(View.INVISIBLE);
        aedSelectImage.setVisibility(View.INVISIBLE);
        eurSelectImage.setVisibility(View.INVISIBLE);
        usdSelectImage.setVisibility(View.INVISIBLE);
        sarSelectImage.setVisibility(View.INVISIBLE);
        gbpSelectImage.setVisibility(View.INVISIBLE);
    }

    public void onClick(View v) {
        resetAllChecks();
        switch (v.getTag().toString()) {
            case "GBP":
                gbpSelectImage.setVisibility(View.VISIBLE);
                break;
            case "QAR":
                qmrSelectImage.setVisibility(View.VISIBLE);
                break;
            case "OMR":
                omrSelectImage.setVisibility(View.VISIBLE);
                break;
            case "KWD":
                kwdSelectImage.setVisibility(View.VISIBLE);
                break;
            case "EGP":
                egpSelectImage.setVisibility(View.VISIBLE);
                break;
            case "BHD":
                bhdSelectImage.setVisibility(View.VISIBLE);
                break;
            case "AED":
                aedSelectImage.setVisibility(View.VISIBLE);
                break;
            case "EUR":
                eurSelectImage.setVisibility(View.VISIBLE);
                break;
            case "USD":
                usdSelectImage.setVisibility(View.VISIBLE);
                break;
            case "SAR":
                sarSelectImage.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.SELECTED_CURRENCY_CODE, v.getTag().toString());
        editor.apply();
        finish();
    }
}


