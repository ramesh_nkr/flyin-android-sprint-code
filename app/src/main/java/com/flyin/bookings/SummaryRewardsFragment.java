package com.flyin.bookings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

@SuppressWarnings("deprecation")
public class SummaryRewardsFragment extends Fragment {
    private Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rewards_summary_fragment, container, false);
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        TextView availablePointsTextViewHeader = (TextView) view.findViewById(R.id.reward_available_points_textview);
        TextView availablePointsTextHeader = (TextView) view.findViewById(R.id.reward_available_points_text);
        TextView rewardIdTextView = (TextView) view.findViewById(R.id.reward_id_textview);
        TextView rewardIdText = (TextView) view.findViewById(R.id.reward_id_text);
        TextView memberSinceTextView = (TextView) view.findViewById(R.id.reward_member_textview);
        TextView memberSinceText = (TextView) view.findViewById(R.id.reward_member_text);
        TextView customerClassTextView = (TextView) view.findViewById(R.id.reward_customerclass_textview);
        TextView customerClassText = (TextView) view.findViewById(R.id.reward_customerclass_text);
        TextView pendingPointsTextView = (TextView) view.findViewById(R.id.reward_pendingpts_textview);
        TextView pendingPointsText = (TextView) view.findViewById(R.id.reward_pendingpts_text);
        if (Utils.isArabicLangSelected(mActivity)) {
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_DROIDKUFI_BOLD;
            availablePointsTextViewHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            availablePointsTextHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            rewardIdTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            rewardIdText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            memberSinceTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            memberSinceText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            customerClassTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            customerClassText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            pendingPointsTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            pendingPointsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        Typeface textFace = Typeface.createFromAsset(mActivity.getAssets(), fontText);
        Typeface boldFace = Typeface.createFromAsset(mActivity.getAssets(), fontBold);
        availablePointsTextViewHeader.setTypeface(boldFace);
        availablePointsTextHeader.setTypeface(boldFace);
        rewardIdTextView.setTypeface(textFace);
        rewardIdText.setTypeface(textFace);
        memberSinceTextView.setTypeface(textFace);
        memberSinceText.setTypeface(textFace);
        customerClassTextView.setTypeface(textFace);
        customerClassText.setTypeface(textFace);
        pendingPointsTextView.setTypeface(textFace);
        pendingPointsText.setTypeface(textFace);
        if (RewardsActivity.loyaltyDataBean.getLoyaltyBasicInfoBean() != null) {
            availablePointsTextHeader.setText(String.valueOf(RewardsActivity.loyaltyDataBean.getLoyaltyBasicInfoBean().getAvailablePoints()));
        } else {
            availablePointsTextHeader.setText("0");
        }
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        rewardIdText.setText(pref.getString(Constants.USER_UNIQUE_ID, ""));
        if (RewardsActivity.loyaltyDataBean.getLoyaltyBasicInfoBean() != null) {
            memberSinceText.setText(Utils.convertLoyaltyMemberDateFormat(RewardsActivity.loyaltyDataBean.getLoyaltyBasicInfoBean().getMemberSince(), mActivity));
            String memberType = null;
            if (RewardsActivity.loyaltyDataBean.getLoyaltyBasicInfoBean().getTier().equalsIgnoreCase("S")) {
                memberType = mActivity.getString(R.string.label_silver);
            } else if (RewardsActivity.loyaltyDataBean.getLoyaltyBasicInfoBean().getTier().equalsIgnoreCase("G")) {
                memberType = mActivity.getString(R.string.label_gold);
            } else if (RewardsActivity.loyaltyDataBean.getLoyaltyBasicInfoBean().getTier().equalsIgnoreCase("P")) {
                memberType = mActivity.getString(R.string.label_platinum);
            } else if (RewardsActivity.loyaltyDataBean.getLoyaltyBasicInfoBean().getTier().equalsIgnoreCase("B")) {
                memberType = mActivity.getString(R.string.label_bronze);
            }
            customerClassText.setText(memberType);
            pendingPointsText.setText(String.valueOf(RewardsActivity.loyaltyDataBean.getLoyaltyBasicInfoBean().getPendingPoints()));
        } else {
            pendingPointsText.setText("0");
        }
        return view;
    }
}
