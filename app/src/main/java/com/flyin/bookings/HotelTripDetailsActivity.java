package com.flyin.bookings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.model.HotelDataBean;
import com.flyin.bookings.model.HotelMoreInfoObject;
import com.flyin.bookings.model.PassengersBean;
import com.flyin.bookings.model.PriceBean;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class HotelTripDetailsActivity extends AppCompatActivity implements AsyncTaskListener {
    private TextView errorText, errorDescriptionText;
    private Typeface titleFace, textFace;
    private RelativeLayout loadingViewLayout;
    private ArrayList<PassengersBean> passengersBeanArrayList = null;
    private boolean isArabicLang;
    private String taskType = "";
    private ImageView StarRating[] = new ImageView[6];
    private static final int GET_CANCEL_RESPONSE = 1;
    private static final int GET_AMEND_RESPONSE = 2;
    private boolean isInternetPresent = false;
    private LayoutInflater inflater;
    private MyReceiver myReceiver;
    private View loadingView;
    private ImageView errorImage;
    private Button searchButton;
    private static final String TAG = "HotelTripDetailsActivity";
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private String booingIdNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_confirmation);
        isArabicLang = false;
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        String fontRobotoBold = Constants.FONT_ROBOTO_BOLD;
        passengersBeanArrayList = new ArrayList<>();
        ImageView hotelImage = (ImageView) findViewById(R.id.hotel_image_view);
        hotelImage.setImageDrawable(null);
        TextView hotelNameText = (TextView) findViewById(R.id.hotel_name_text);
        TextView hotelDescriptionText = (TextView) findViewById(R.id.hotel_description);
        TextView bookingNumberText = (TextView) findViewById(R.id.hotel_booking_number_text);
        TextView bookingNumber = (TextView) findViewById(R.id.hotel_booking_number);
        TextView tripIdText = (TextView) findViewById(R.id.trip_id_text);
        TextView tripIdNumber = (TextView) findViewById(R.id.trip_id);
        TextView checkInText = (TextView) findViewById(R.id.check_in_text);
        TextView checkInDate = (TextView) findViewById(R.id.check_in);
        TextView checkInTime = (TextView) findViewById(R.id.check_in_time);
        TextView checkOutText = (TextView) findViewById(R.id.check_out_text);
        TextView checkOutDate = (TextView) findViewById(R.id.check_out);
        TextView checkOutTime = (TextView) findViewById(R.id.check_out_time);
        TextView reservationText = (TextView) findViewById(R.id.reservation_details_text);
        TextView personName = (TextView) findViewById(R.id.person_name);
        TextView reservationDetails = (TextView) findViewById(R.id.reservation_details);
        TextView totalPriceText = (TextView) findViewById(R.id.total_price_text);
        TextView totalPrice = (TextView) findViewById(R.id.total_price);
        TextView cancellationText = (TextView) findViewById(R.id.cancellation_text);
        TextView cancellationData = (TextView) findViewById(R.id.cancellation_policy);
        TextView cancelBookingText = (TextView) findViewById(R.id.cancel_booking_text);
        TextView amendBookingText = (TextView) findViewById(R.id.amend_booking_text);
        TextView shareText = (TextView) findViewById(R.id.share_text);
        StarRating[1] = (ImageView) findViewById(R.id.one_star);
        StarRating[2] = (ImageView) findViewById(R.id.two_star);
        StarRating[3] = (ImageView) findViewById(R.id.three_star);
        StarRating[4] = (ImageView) findViewById(R.id.four_star);
        StarRating[5] = (ImageView) findViewById(R.id.five_star);
        ImageView cancelBookingImage = (ImageView) findViewById(R.id.cancel_booking_image);
        ImageView amendBookingImage = (ImageView) findViewById(R.id.amend_booking_image);
        LinearLayout cancelBookingLayout = (LinearLayout) findViewById(R.id.cancel_booking_layout);
        LinearLayout amendBookingLayout = (LinearLayout) findViewById(R.id.amend_booking_layout);
        LinearLayout bookingsLayout = (LinearLayout) findViewById(R.id.bookings_layout);
        ImageView homeLogo = (ImageView) findViewById(R.id.home_logo);
        ImageView facebookLogo = (ImageView) findViewById(R.id.facebook_logo);
        ImageView twitterLogo = (ImageView) findViewById(R.id.twitter_logo);
        ImageView googlePlusLogo = (ImageView) findViewById(R.id.google_plus_logo);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        LinearLayout couponLayout = (LinearLayout) findViewById(R.id.coupon_layout);
        TextView numberOfCoupon = (TextView) findViewById(R.id.coupon_amount);
        TextView numberOfCouponText = (TextView) findViewById(R.id.coupon_textview);
        LinearLayout qitafDiscountLayout = (LinearLayout) findViewById(R.id.qitaf_discount_layout);
        TextView qitafDiscountText = (TextView) findViewById(R.id.qitaf_discount_text);
        TextView qitafDiscountValue = (TextView) findViewById(R.id.qitaf_discount);
        LinearLayout rewardsRedeemedLayout = (LinearLayout) findViewById(R.id.flyin_rewards_redeem_layout);
        TextView rewardsRedeemedHeader = (TextView) findViewById(R.id.flyin_rewards_redeem_text);
        TextView rewardsRedeemedValue = (TextView) findViewById(R.id.flyin_rewards_redeem_value);
        if (Utils.isArabicLangSelected(HotelTripDetailsActivity.this)) {
            isArabicLang = true;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_DROIDKUFI_BOLD;
            fontRobotoBold = Constants.FONT_DROIDKUFI_BOLD;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                bookingNumber.setTextDirection(View.TEXT_DIRECTION_RTL);
                tripIdNumber.setTextDirection(View.TEXT_DIRECTION_RTL);
                totalPrice.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
            hotelNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_name_text));
            hotelDescriptionText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            bookingNumberText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            bookingNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            tripIdText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            tripIdNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            checkInText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            checkInDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            checkInTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            checkOutText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            checkOutDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            checkOutTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            reservationText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            personName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            reservationDetails.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            totalPriceText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            totalPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            cancellationText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            cancellationData.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            cancelBookingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            amendBookingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            shareText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            numberOfCouponText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            numberOfCoupon.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            qitafDiscountText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            qitafDiscountValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            rewardsRedeemedHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            rewardsRedeemedValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            cancelBookingImage.setRotation(180);
            amendBookingImage.setRotation(180);
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        titleFace = Typeface.createFromAsset(getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(getAssets(), fontText);
        Typeface boldFace = Typeface.createFromAsset(getAssets(), fontBold);
        Typeface regularBold = Typeface.createFromAsset(getAssets(), fontRobotoBold);
        hotelNameText.setTypeface(boldFace);
        hotelDescriptionText.setTypeface(titleFace);
        bookingNumberText.setTypeface(textFace);
        bookingNumber.setTypeface(titleFace);
        tripIdText.setTypeface(textFace);
        tripIdNumber.setTypeface(titleFace);
        checkInText.setTypeface(textFace);
        checkInDate.setTypeface(titleFace);
        checkInTime.setTypeface(titleFace);
        checkOutText.setTypeface(textFace);
        checkOutDate.setTypeface(titleFace);
        checkOutTime.setTypeface(titleFace);
        reservationText.setTypeface(textFace);
        personName.setTypeface(titleFace);
        reservationDetails.setTypeface(titleFace);
        totalPriceText.setTypeface(boldFace);
        totalPrice.setTypeface(regularBold);
        cancellationText.setTypeface(tf);
        cancellationData.setTypeface(titleFace);
        cancelBookingText.setTypeface(textFace);
        amendBookingText.setTypeface(textFace);
        shareText.setTypeface(textFace);
        numberOfCouponText.setTypeface(titleFace);
        numberOfCoupon.setTypeface(titleFace);
        qitafDiscountText.setTypeface(titleFace);
        qitafDiscountValue.setTypeface(titleFace);
        rewardsRedeemedHeader.setTypeface(titleFace);
        rewardsRedeemedValue.setTypeface(titleFace);
        PriceBean priceBean = Singleton.getInstance().selectedTripFlightObject.getPriceBean();
        if (Singleton.getInstance().selectedTripFlightObject.getGeneralDetailsObjectBean().getBookingStatus().equalsIgnoreCase("Confirmed")) {
            bookingsLayout.setVisibility(View.VISIBLE);
        } else {
            bookingsLayout.setVisibility(View.GONE);
        }
        cancelBookingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskType = "Cancellation";
                final String requestJSON = bookingDetailsTask();
                isInternetPresent = Utils.isConnectingToInternet(HotelTripDetailsActivity.this);
                if (isInternetPresent) {
                    getCancelRequestFromServer(requestJSON);
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(titleFace);
                    errorDescriptionText.setTypeface(titleFace);
                    searchButton.setTypeface(titleFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(HotelTripDetailsActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                getCancelRequestFromServer(requestJSON);
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        });
        amendBookingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskType = "Amendment";
                final String requestJSON = bookingDetailsTask();
                isInternetPresent = Utils.isConnectingToInternet(HotelTripDetailsActivity.this);
                if (isInternetPresent) {
                    getAmendRequestFromServer(requestJSON);
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(titleFace);
                    errorDescriptionText.setTypeface(titleFace);
                    searchButton.setTypeface(titleFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(HotelTripDetailsActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                getAmendRequestFromServer(requestJSON);
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        });
        HotelDataBean hotelsDataBean;
        try {
            if (Singleton.getInstance().selectedTripFlightObject.getEntityDetailsObjectBean() == null) {
                hotelsDataBean = new HotelDataBean();
            } else {
                hotelsDataBean = Singleton.getInstance().selectedTripFlightObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean();
            }
        } catch (Exception e) {
            hotelsDataBean = new HotelDataBean();
        }
        HotelMoreInfoObject moreInfoObj = Singleton.getInstance().selectedTripFlightObject.getEntityDetailsObjectBean().getHotelMoreInfo();
        String imagePath = moreInfoObj.getImagePath();
        if (!imagePath.equalsIgnoreCase("")) {
            String hotelImagePath = imagePath.replaceAll(" ", "%20");
            Glide.with(HotelTripDetailsActivity.this).load(hotelImagePath).placeholder(R.drawable.imagethumb_search).into(hotelImage);
            hotelImage.setScaleType(ImageView.ScaleType.FIT_XY);
        } else {
            hotelImage.setImageResource(R.drawable.people);
            hotelImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
        hotelNameText.setText(moreInfoObj.getName());
        if (!moreInfoObj.getRating().equalsIgnoreCase("")) {
            int starRating = Integer.parseInt(moreInfoObj.getRating());
            if (starRating > 0) {
                for (int i = 1; i <= starRating; i++) {
                    StarRating[i].setVisibility(View.VISIBLE);
                }
            }
        }
        String hotelCheckInTime = "";
        String hotelCheckOutTime = "";
        if (!moreInfoObj.getCheckInTime().equalsIgnoreCase("")) {
            hotelCheckInTime = "(" + getResources().getString(R.string.label_from_text) + " " + moreInfoObj.getCheckInTime() + ")";
            checkInTime.setText(hotelCheckInTime);
        } else {
            checkInTime.setVisibility(View.GONE);
        }
        if (!moreInfoObj.getCheckOutTime().equalsIgnoreCase("")) {
            hotelCheckOutTime = "(" + getResources().getString(R.string.label_until_text) + " " + moreInfoObj.getCheckOutTime() + ")";
            checkOutTime.setText(hotelCheckOutTime);
        } else {
            checkOutTime.setVisibility(View.GONE);
        }
        hotelDescriptionText.setText(moreInfoObj.getAddress());
        String hotelReservation = hotelsDataBean.getHotelReservationNumber();
        if (!hotelReservation.equalsIgnoreCase("")) {
            String[] hotelNumber = hotelReservation.split("-");
            if (Singleton.getInstance().selectedTripFlightObject.getGeneralDetailsObjectBean().getBookingStatus().equalsIgnoreCase("Booked")) {
                bookingNumber.setVisibility(View.GONE);
            } else {
                bookingNumber.setText(hotelNumber[1]);
            }
//            if(hotelNumber.length > 2){
//                bookingNumber.setText(hotelNumber[1] + " " + hotelNumber[2]);
//            }else {
//                bookingNumber.setText(hotelNumber[1]);
//            }
        } else {
            bookingNumber.setText("");
        }
        booingIdNumber = hotelsDataBean.getReferenceNo();
        tripIdNumber.setText(booingIdNumber);
        String checkInDateFormat = Utils.convertToHotelCheckDateAndTime(hotelsDataBean.getCheckInDate(), HotelTripDetailsActivity.this);
        String checkOutDateFormat = Utils.convertToHotelCheckDateAndTime(hotelsDataBean.getCheckOutDate(), HotelTripDetailsActivity.this);
        checkInDate.setText(checkInDateFormat);
        checkOutDate.setText(checkOutDateFormat);
        personName.setText(Utils.checkStringVal(Singleton.getInstance().selectedTripFlightObject.getPassengerDetailsObjectBean().getPassengersBeanArrayList().get(0).getPersonalDetailsObjectBean().getFirstName()) + " " + Utils.checkStringVal(Singleton.getInstance().selectedTripFlightObject.getPassengerDetailsObjectBean().getPassengersBeanArrayList().get(0).getPersonalDetailsObjectBean().getLastName()));
        String stayNights = "";
        if (Integer.parseInt(hotelsDataBean.getNights()) == 1) {
            stayNights = hotelsDataBean.getNights() + " " + getResources().getString(R.string.label_one_night);
        } else if (Integer.parseInt(hotelsDataBean.getNights()) > 1 && Integer.parseInt(hotelsDataBean.getNights()) < 11) {
            stayNights = hotelsDataBean.getNights() + " " + getResources().getString(R.string.label_below_ten_night);
        } else {
            stayNights = hotelsDataBean.getNights() + " " + getResources().getString(R.string.label_more_nights);
        }
        String totalRooms = "";
        if (!hotelsDataBean.getRooms().equalsIgnoreCase("")) {
            int numberOfRooms = Integer.parseInt(hotelsDataBean.getRooms());
            if (numberOfRooms == 1) {
                totalRooms = hotelsDataBean.getRooms() + " " + getResources().getString(R.string.label_single_room);
            } else if (numberOfRooms >= 2 && 10 <= numberOfRooms) {
                totalRooms = hotelsDataBean.getRooms() + " " + getResources().getString(R.string.label_multiple_rooms);
            } else {
                totalRooms = hotelsDataBean.getRooms() + " " + getResources().getString(R.string.label_more_than_ten_rooms);
            }
        }
        reservationDetails.setText(totalRooms + ", " + stayNights + " " + hotelsDataBean.getRoomType());
        Double userPayablePrice = 0.0;
        String currencyName = "";
        if (priceBean.getCouponDiscount() != null && !priceBean.getCouponDiscount().isEmpty()) {
            if (Double.parseDouble(Singleton.getInstance().selectedTripFlightObject.getPriceBean().getCouponDiscount()) == 0) {
                couponLayout.setVisibility(View.GONE);
                userPayablePrice = Double.parseDouble(Singleton.getInstance().selectedTripFlightObject.getPriceBean().getTotal());
            } else {
                couponLayout.setVisibility(View.VISIBLE);
                userPayablePrice = Double.parseDouble(Singleton.getInstance().selectedTripFlightObject.getPriceBean().getTotal()) - Double.parseDouble(priceBean.getCouponDiscount());
                if (Singleton.getInstance().selectedTripFlightObject.getPriceBean().getUserCurrency().equalsIgnoreCase("SAR")) {
                    currencyName = getString(R.string.label_SAR_currency_name);
                    if (isArabicLang) {
                        numberOfCoupon.setText(priceBean.getCouponDiscount() + " " + currencyName);
                    } else {
                        numberOfCoupon.setText(currencyName + " " + priceBean.getCouponDiscount());
                    }
                } else {
                    currencyName = Singleton.getInstance().selectedTripFlightObject.getPriceBean().getUserCurrency();
                    numberOfCoupon.setText(currencyName + " " + priceBean.getCouponDiscount());
                }
            }
        } else {
            userPayablePrice = Double.parseDouble(Singleton.getInstance().selectedTripFlightObject.getPriceBean().getTotal());
            couponLayout.setVisibility(View.GONE);
        }
        String finalPaidPrice = String.format(Locale.ENGLISH, "%.2f", userPayablePrice);
        if (Singleton.getInstance().selectedTripFlightObject.getPriceBean().getUserCurrency().equalsIgnoreCase("SAR")) {
            currencyName = getString(R.string.label_SAR_currency_name);
            if (isArabicLang) {
                totalPrice.setText(finalPaidPrice + " " + currencyName);
            } else {
                totalPrice.setText(currencyName + " " + finalPaidPrice);
            }
        } else {
            currencyName = Singleton.getInstance().selectedTripFlightObject.getPriceBean().getUserCurrency();
            totalPrice.setText(currencyName + " " + finalPaidPrice);
        }
        cancellationData.setText(moreInfoObj.getCancellationPolicy());
        passengersBeanArrayList = Singleton.getInstance().selectedTripFlightObject.getPassengerDetailsObjectBean().getPassengersBeanArrayList();
        homeLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHomeClick();
            }
        });
        facebookLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://www.facebook.com/flyin.com"));
                startActivity(intent);
            }
        });
        twitterLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://twitter.com/flyincom"));
                startActivity(intent);
            }
        });
        googlePlusLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://plus.google.com/+flyin/posts"));
                startActivity(intent);
            }
        });
        if (priceBean.getQitafAmount() != null && !priceBean.getQitafAmount().isEmpty()) {
            if (Double.parseDouble(Singleton.getInstance().selectedTripFlightObject.getPriceBean().getQitafAmount()) > 0) {
                qitafDiscountLayout.setVisibility(View.VISIBLE);
                currencyName = getString(R.string.label_SAR_currency_name);
                if (isArabicLang) {
                    qitafDiscountValue.setText(priceBean.getQitafAmount() + " " + currencyName);
                } else {
                    qitafDiscountValue.setText(currencyName + " " + priceBean.getQitafAmount());
                }
            } else {
                qitafDiscountLayout.setVisibility(View.GONE);
            }
        } else {
            qitafDiscountLayout.setVisibility(View.GONE);
        }
        if (priceBean.getRdmDiscountusr() != null && !priceBean.getRdmDiscountusr().isEmpty()) {
            if (Double.parseDouble(Singleton.getInstance().selectedTripFlightObject.getPriceBean().getRdmDiscountusr()) > 0) {
                rewardsRedeemedLayout.setVisibility(View.VISIBLE);
                if (Singleton.getInstance().selectedTripFlightObject.getPriceBean().getUserCurrency().equalsIgnoreCase("SAR")) {
                    currencyName = getString(R.string.label_SAR_currency_name);
                    if (isArabicLang) {
                        rewardsRedeemedValue.setText(Double.parseDouble(priceBean.getRdmDiscountusr()) + " " + currencyName);
                    } else {
                        rewardsRedeemedValue.setText(currencyName + " " + Double.parseDouble(priceBean.getRdmDiscountusr()));
                    }
                } else {
                    currencyName = Singleton.getInstance().selectedTripFlightObject.getPriceBean().getUserCurrency();
                    rewardsRedeemedValue.setText(currencyName + " " + Double.parseDouble(priceBean.getRdmDiscountusr()));
                }
            } else {
                rewardsRedeemedLayout.setVisibility(View.GONE);
            }
        } else {
            rewardsRedeemedLayout.setVisibility(View.GONE);
        }
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.home_back_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_home_text);
        ImageView first_home_logo = (ImageView) mCustomView.findViewById(R.id.first_home_logo);
        first_home_logo.setImageResource(R.mipmap.flyin_home);
        first_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHomeClick();
            }
        });
        mCustomView.findViewById(R.id.sing_in_button).setVisibility(View.GONE);
        ImageView homeIcon = (ImageView) mCustomView.findViewById(R.id.home_icon);
        homeIcon.setVisibility(View.VISIBLE);
        if (Utils.isArabicLangSelected(HotelTripDetailsActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backText.setTypeface(titleFace);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter("com.flyin.bookings.ERROR_MESSAGE");
        registerReceiver(myReceiver, intentFilter);
    }

    private void onHomeClick() {
        Intent intent = new Intent(HotelTripDetailsActivity.this, MainSearchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private String bookingDetailsTask() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(HotelTripDetailsActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(HotelTripDetailsActivity.this));
            String passengerName = (passengersBeanArrayList.get(0).getPersonalDetailsObjectBean().getFirstName() + " " + passengersBeanArrayList.get(0).getPersonalDetailsObjectBean().getLastName());
            mainJSON.put("source", sourceJSON);
            mainJSON.put("bookingType", "Hotel");
            mainJSON.put("taskType", taskType);
            mainJSON.put("bookingId", booingIdNumber);
            mainJSON.put("registerCustomerId", pref.getString(Constants.MEMBER_EMAIL, ""));
            mainJSON.put("leadPaxName", passengerName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void getCancelRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(HotelTripDetailsActivity.this, HotelTripDetailsActivity.this,
                Constants.CANCEL_BOOKING, "", requestJSON, GET_CANCEL_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void getAmendRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(HotelTripDetailsActivity.this, HotelTripDetailsActivity.this,
                Constants.CANCEL_BOOKING, "", requestJSON, GET_AMEND_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        closeLoading();
        if (serviceType == GET_CANCEL_RESPONSE) {
            Utils.printMessage(TAG, "Cancel DATA:: " + data);
            if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
                inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = inflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(textFace);
                errorDescriptionText.setTypeface(textFace);
                searchButton.setTypeface(textFace);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isInternetPresent = Utils.isConnectingToInternet(HotelTripDetailsActivity.this);
                        if (isInternetPresent) {
                            loadingViewLayout.removeView(errorView);
                            finish();
                        }
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            } else {
                try {
                    JSONObject obj = new JSONObject();
                    obj = new JSONObject(data);
                    if (obj.has("status")) {
                        String status = obj.getString("status");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            displayErrorMessage(getResources().getString(R.string.CancelSuccessLabel));
                            errorView.setBackgroundResource(R.color.success_message_background);
                            return;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (serviceType == GET_AMEND_RESPONSE) {
            Utils.printMessage(TAG, "Amend DATA:: " + data);
            if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
                inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = inflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(textFace);
                errorDescriptionText.setTypeface(textFace);
                searchButton.setTypeface(textFace);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isInternetPresent = Utils.isConnectingToInternet(HotelTripDetailsActivity.this);
                        if (isInternetPresent) {
                            loadingViewLayout.removeView(errorView);
                            finish();
                        }
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            } else {
                try {
                    JSONObject obj = new JSONObject();
                    obj = new JSONObject(data);
                    if (obj.has("status")) {
                        String status = obj.getString("status");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            displayErrorMessage(getResources().getString(R.string.CancelSuccessLabel));
                            errorView.setBackgroundResource(R.color.success_message_background);
                            return;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(titleFace);
        descriptionText.setVisibility(View.GONE);
        if (isArabicLang) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    private void loadErrorType(int type) {

        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(getResources().getString(R.string.label_network_error_message));
                searchButton.setText(getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getResources().getString(R.string.label_no_results_found_message));
                searchButton.setText(getResources().getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    public class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.ERROR_MESSAGE")) {
                displayErrorMessage(intent.getStringExtra("message"));
            }
        }
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver);
    }
}