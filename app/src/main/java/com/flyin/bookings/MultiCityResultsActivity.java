package com.flyin.bookings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.adapter.FlightMultiTripAdapter;
import com.flyin.bookings.listeners.AirlinesInterface;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.AirItineraryObjectBean;
import com.flyin.bookings.model.AirItineraryPricingInfoBean;
import com.flyin.bookings.model.FSDataBean;
import com.flyin.bookings.model.FilterAirlineSelectionItem;
import com.flyin.bookings.model.FilterAirportSelectionItem;
import com.flyin.bookings.model.FilterRefundSelectionItem;
import com.flyin.bookings.model.FilterStopSelectionItem;
import com.flyin.bookings.model.FilterTimeSelectionItem;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.ItinTotalFareObjectBean;
import com.flyin.bookings.model.OriginDestinationOptionBean;
import com.flyin.bookings.model.PFBDObjectBean;
import com.flyin.bookings.model.PassengerFareObjectBean;
import com.flyin.bookings.model.PricedItineraryObject;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.model.TaxObjectBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.AirlinesData;
import com.flyin.bookings.util.AirportData;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MultiCityResultsActivity extends AppCompatActivity implements AsyncTaskListener, OnCustomItemSelectListener {
    private Typeface regularFace;
    private String requestJSON = "";
    private boolean isArabicLang = false;
    private boolean isInternetPresent = false;
    private String source = "";
    private String destination = "";
    private String secondSource = "";
    private String secondDestination = "";
    private String thirdSource = "";
    private String thirdDestination = "";
    private String journeyDetails = "";
    private boolean isNonStopChecked = false;
    private boolean multiCityNonStopChecked = false;
    private int legCountInMT = 0;
    private static final int GET_SEARCH_RESULTS = 1;
    private static final int GET_FARE_RULE_RESULTS = 2;
    private static final int FLIGHT_PRICE_FILTER = 3;
    private static final int SEARCH_FLIGHT_RESULT = 4;
    private LayoutInflater inflater;
    private ImageView errorImage = null;
    private Button searchButton = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private RelativeLayout headerViewLayout = null;
    private View loadingView = null;
    private static final String TAG = "MultiCityResultsActivity";
    private String tripType = "";
    private ArrayList<PricedItineraryObject> mainPricedItineraryArray = null;
    private ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList = null;
    private ArrayList<PricedItineraryObject> earliestItineraryObjectArrayList = null;
    private ArrayList<PricedItineraryObject> fastestItineraryObjectArrayList = null;
    private ArrayList<FilterStopSelectionItem> stopsArray = null;
    private ArrayList<FilterAirlineSelectionItem> airlineArray = null;
    private ArrayList<FilterAirportSelectionItem> airportListArray = null;
    private ArrayList<FilterAirportSelectionItem> tempArray1 = null;
    private ArrayList<FilterAirportSelectionItem> tempArray2 = null;
    private ArrayList<FilterAirportSelectionItem> tempArray3 = null;
    private ArrayList<FilterTimeSelectionItem> timeArray = null;
    private ArrayList<FilterTimeSelectionItem> dapTimeListArray = null;
    private ArrayList<FilterTimeSelectionItem> aapTimeListArray = null;
    private ArrayList<FilterRefundSelectionItem> refundStatusArray = null;
    private ArrayList<String> airportArrayList = new ArrayList<>();
    private ArrayList<String> airlineArrayList = new ArrayList<>();
    private HashMap<String, FlightAirportName> airportNamesMap = new HashMap<>();
    private HashMap<String, String> airLineNamesMap = new HashMap<>();
    private AirportData airportData = new AirportData();
    private AirlinesData airlinesData = new AirlinesData();
    private RecyclerView multiRecyclerView;
    private FlightMultiTripAdapter mAdapter;
    private String totalPassengersCount = "";
    private String selectedClass = "";
    private TextView totalFlightCount = null;
    private TextView earliestFlightCount = null;
    private TextView fastestFlightCount = null;
    private LinearLayout totalCountLayout = null, totalCountSubLayout = null;
    private LinearLayout earliestCountLayout = null, earliestCountSubLayout = null;
    private LinearLayout fastestCountLayout = null, fastestCountSubLayout;
    private View totalFlightView = null;
    private View earliestFlightView = null;
    private View fastestFlightView = null;
    private String minValue, maxValue;
    private View noResultView = null;
    private boolean isFilterApplied = false;
    private boolean isEarliestClicked = false;
    private boolean isFastestClicked = false;
    private TextView errorMessageText = null;
    private RelativeLayout errorView = null;
    private String aiJSONObject = "";
    private String aipiJSONObject = "";
    private String firstFlightDate = "";
    private String secondFlightDate = "";
    private String thirdFlightDate = "";
    private ArrayList<PricedItineraryObject> forwardPIArrayList = null;
    private String randomNumber = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_trip_results);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Constants.JSON_REQUEST)) {
                requestJSON = bundle.getString(Constants.JSON_REQUEST, "");
            }
            tripType = bundle.getString(Constants.TRIP_TYPE, "");
            source = bundle.getString(Constants.FORWARD_SOURCE, "");
            destination = bundle.getString(Constants.FORWARD_DESTINATION, "");
            secondSource = bundle.getString(Constants.SECOND_FORWARD_SOURCE, "");
            secondDestination = bundle.getString(Constants.SECOND_FORWARD_DESTINATION, "");
            thirdSource = bundle.getString(Constants.THIRD_FORWARD_SOURCE, "");
            thirdDestination = bundle.getString(Constants.THIRD_FORWARD_DESTINATION, "");
            journeyDetails = bundle.getString(Constants.FORWARD_DETAILS, "");
            isNonStopChecked = bundle.getBoolean(Constants.ONE_WAY_NONSTOP_CHECKED, false);
            multiCityNonStopChecked = bundle.getBoolean(Constants.MULTI_TRIP_NONSTOP_CHECKED, false);
            legCountInMT = bundle.getInt(Constants.MULTI_TRIP_LEG_COUNT, 0);
            totalPassengersCount = bundle.getString(Constants.TOTAL_PASSENGERS_COUNT, "1");
            selectedClass = bundle.getString(Constants.SELECTED_CLASS_TYPE, "");
            firstFlightDate = bundle.getString(Constants.FIRST_FLIGHT_DATE, "");
            secondFlightDate = bundle.getString(Constants.SECOND_FLIGHT_DATE, "");
            thirdFlightDate = bundle.getString(Constants.THIRD_FLIGHT_DATE, "");
            randomNumber = bundle.getString(Constants.BOOKING_RANDOM_NUMBER, "");
            Singleton.getInstance().refundStatusArrayList.clear();
            Singleton.getInstance().resultStopsArrayList.clear();
            Singleton.getInstance().resultAirlinesArrayList.clear();
            Singleton.getInstance().resultAirportsArrayList.clear();
            Singleton.getInstance().firstAirportsArrayList.clear();
            Singleton.getInstance().secondAirportsArrayList.clear();
            Singleton.getInstance().thirdAirportsArrayList.clear();
            Singleton.getInstance().resultTimeArrayList.clear();
            Singleton.getInstance().departureTimeArrayList.clear();
            Singleton.getInstance().arrivalTimeArrayList.clear();
        }
        isArabicLang = false;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(MultiCityResultsActivity.this)) {
            isArabicLang = true;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        isInternetPresent = Utils.isConnectingToInternet(MultiCityResultsActivity.this);
        mainPricedItineraryArray = new ArrayList<>();
        pricedItineraryObjectArrayList = new ArrayList<>();
        earliestItineraryObjectArrayList = new ArrayList<>();
        fastestItineraryObjectArrayList = new ArrayList<>();
        forwardPIArrayList = new ArrayList<>();
        stopsArray = new ArrayList<>();
        airlineArray = new ArrayList<>();
        airportListArray = new ArrayList<>();
        tempArray1 = new ArrayList<>();
        tempArray2 = new ArrayList<>();
        tempArray3 = new ArrayList<>();
        timeArray = new ArrayList<>();
        dapTimeListArray = new ArrayList<>();
        aapTimeListArray = new ArrayList<>();
        refundStatusArray = new ArrayList<>();
        TextView backText = (TextView) findViewById(R.id.back_text);
        TextView firstSrcName = (TextView) findViewById(R.id.first_src_name);
        TextView secondSrcName = (TextView) findViewById(R.id.second_src_name);
        TextView thirdSrcName = (TextView) findViewById(R.id.third_src_name);
        TextView dateText = (TextView) findViewById(R.id.date_text);
        TextView totalPassengers = (TextView) findViewById(R.id.total_passengers);
        TextView classType = (TextView) findViewById(R.id.class_type);
        totalFlightCount = (TextView) findViewById(R.id.total_flight_count);
        earliestFlightCount = (TextView) findViewById(R.id.earliest_flight_count);
        fastestFlightCount = (TextView) findViewById(R.id.fastest_flight_count);
        totalFlightView = findViewById(R.id.total_flight_view);
        earliestFlightView = findViewById(R.id.earliest_flight_view);
        fastestFlightView = findViewById(R.id.fastest_flight_view);
        totalCountLayout = (LinearLayout) findViewById(R.id.total_count_layout);
        earliestCountLayout = (LinearLayout) findViewById(R.id.earliest_count_layout);
        fastestCountLayout = (LinearLayout) findViewById(R.id.fastest_count_layout);
        totalCountSubLayout = (LinearLayout) findViewById(R.id.total_count_sub_layout);
        earliestCountSubLayout = (LinearLayout) findViewById(R.id.earliest_count_sub_layout);
        fastestCountSubLayout = (LinearLayout) findViewById(R.id.fastest_count_sub_layout);
        LinearLayout filterLayout = (LinearLayout) findViewById(R.id.filter_layout);
        TextView filterText = (TextView) findViewById(R.id.filter_text);
        LinearLayout backLayout = (LinearLayout) findViewById(R.id.back_layout);
        headerViewLayout = (RelativeLayout) findViewById(R.id.header_view_layout);
        multiRecyclerView = (RecyclerView) findViewById(R.id.multi_flight_list);
        regularFace = Typeface.createFromAsset(getAssets(), fontTitle);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(regularFace);
        if (isArabicLang) {
            errorMessageText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        backText.setTypeface(regularFace);
        firstSrcName.setTypeface(regularFace);
        secondSrcName.setTypeface(regularFace);
        thirdSrcName.setTypeface(regularFace);
        dateText.setTypeface(regularFace);
        totalPassengers.setTypeface(regularFace);
        classType.setTypeface(regularFace);
        totalFlightCount.setTypeface(regularFace);
        earliestFlightCount.setTypeface(regularFace);
        fastestFlightCount.setTypeface(regularFace);
        filterText.setTypeface(regularFace);
        if (isArabicLang) {
            backText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            firstSrcName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            secondSrcName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            thirdSrcName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            dateText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            totalPassengers.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            classType.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            totalFlightCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            earliestFlightCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            fastestFlightCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            filterText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
        }
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.hide();
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (isArabicLang) {
            firstSrcName.setText(destination + " - " + source);
        } else {
            firstSrcName.setText(source + " - " + destination);
        }
        if (!secondSource.equalsIgnoreCase("")) {
            if (isArabicLang) {
                secondSrcName.setText(secondDestination + " - " + secondSource + ", ");
            } else {
                secondSrcName.setText(", " + secondSource + " - " + secondDestination);
            }
        } else {
            secondSrcName.setVisibility(View.GONE);
        }
        if (!thirdSource.equalsIgnoreCase("")) {
            if (isArabicLang) {
                thirdSrcName.setText(thirdDestination + " - " + thirdSource + " ,");
            } else {
                thirdSrcName.setText(", " + thirdSource + " - " + thirdDestination);
            }
        } else {
            thirdSrcName.setVisibility(View.GONE);
        }
        dateText.setText(journeyDetails);
        totalPassengers.setText(totalPassengersCount);
        String selectedClassType = "";
        if (selectedClass.equalsIgnoreCase("Business")) {
            selectedClassType = getString(R.string.label_search_flight_business);
        } else if (selectedClass.equalsIgnoreCase("First")) {
            selectedClassType = getString(R.string.label_search_flight_first);
        } else {
            selectedClassType = getString(R.string.label_search_flight_economy);
        }
        classType.setText(selectedClassType);
        filterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loadingView != null) {
                    return;
                } else {
                    if (mainPricedItineraryArray.size() == 0) {
                        return;
                    } else {
                        Intent intent = new Intent(MultiCityResultsActivity.this, MultiFlightFilterActivity.class);
                        intent.putExtra(Constants.TRIP_TYPE, tripType);
                        intent.putExtra(Constants.FORWARD_SOURCE, source);
                        intent.putExtra(Constants.FORWARD_DESTINATION, destination);
                        intent.putExtra(Constants.SECOND_FORWARD_SOURCE, secondSource);
                        intent.putExtra(Constants.SECOND_FORWARD_DESTINATION, secondDestination);
                        intent.putExtra(Constants.THIRD_FORWARD_SOURCE, thirdSource);
                        intent.putExtra(Constants.THIRD_FORWARD_DESTINATION, thirdDestination);
                        intent.putExtra(Constants.PRICE_SELECTED_MINIMUM_VALUE, minValue);
                        intent.putExtra(Constants.PRICE_SELECTED_MAXIMUM_VALUE, maxValue);
                        intent.putExtra(Constants.ONE_WAY_NONSTOP_CHECKED, isNonStopChecked);
                        intent.putExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, multiCityNonStopChecked);
                        intent.putExtra(Constants.AIRLINE_NAME_HASHMAP, airLineNamesMap);
                        intent.putExtra(Constants.AIRPORT_NAME_HASHMAP, airportNamesMap);
                        startActivityForResult(intent, FLIGHT_PRICE_FILTER);
                        //overridePendingTransition(R.anim.translate_left_side, R.anim.translate_right_side);
                    }
                }
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(
                getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        multiRecyclerView.setLayoutManager(mLayoutManager);
        totalCountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleFilterResults();
                pricedItineraryObjectArrayList.clear();
                pricedItineraryObjectArrayList.addAll(forwardPIArrayList);
                handleDefaultFlights();
                updateHeaderMessage();
            }
        });
        earliestCountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEarliestClicked = true;
                totalCountLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
                earliestCountLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.white_color));
                fastestCountLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
                totalCountSubLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
                earliestCountSubLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.white_color));
                fastestCountSubLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
                totalFlightView.setVisibility(View.INVISIBLE);
                earliestFlightView.setVisibility(View.VISIBLE);
                fastestFlightView.setVisibility(View.INVISIBLE);
                totalFlightCount.setTextColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_header_label_color));
                earliestFlightCount.setTextColor(Utils.getColor(MultiCityResultsActivity.this, R.color.result_compo_duration));
                fastestFlightCount.setTextColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_header_label_color));
                handleEarliestFlights();
            }
        });
        fastestCountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFastestClicked = true;
                totalCountLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
                earliestCountLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
                fastestCountLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.white_color));
                totalCountSubLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
                earliestCountSubLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
                fastestCountSubLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.white_color));
                totalFlightView.setVisibility(View.INVISIBLE);
                earliestFlightView.setVisibility(View.INVISIBLE);
                fastestFlightView.setVisibility(View.VISIBLE);
                totalFlightCount.setTextColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_header_label_color));
                earliestFlightCount.setTextColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_header_label_color));
                fastestFlightCount.setTextColor(Utils.getColor(MultiCityResultsActivity.this, R.color.result_compo_duration));
                handleNonStopFlights();
            }
        });
        if (isInternetPresent) {
            getRequestFromServer(requestJSON);
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(regularFace);
            errorDescriptionText.setTypeface(regularFace);
            searchButton.setTypeface(regularFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(MultiCityResultsActivity.this);
                    if (isInternetPresent) {
                        headerViewLayout.removeView(errorView);
                        getRequestFromServer(requestJSON);
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            headerViewLayout.addView(errorView);
        }
    }

    private void getRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(MultiCityResultsActivity.this, MultiCityResultsActivity.this, Constants.SEARCH_RQ_URL, "",
                requestJSON, GET_SEARCH_RESULTS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView waitText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setTypeface(regularFace);
        waitText.setTypeface(regularFace);
        if (isArabicLang) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            waitText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        headerViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        try {
            headerViewLayout.removeView(loadingView);
            loadingView = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            closeLoading();
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(regularFace);
            errorDescriptionText.setTypeface(regularFace);
            searchButton.setTypeface(regularFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    headerViewLayout.removeView(errorView);
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            headerViewLayout.addView(errorView);
        } else {
            if (serviceType == GET_SEARCH_RESULTS) {
                boolean isConversionDone = parseJSON(data);
                if (isConversionDone) {
                    Utils.printMessage(TAG, "SIZE: " + forwardPIArrayList.size());
                    getAirportDetails();
                    handleEarliestFlights();
                    handleNonStopFlights();
                    pricedItineraryObjectArrayList.clear();
                    pricedItineraryObjectArrayList.addAll(forwardPIArrayList);
                    updateStopsFlights(stopsArray, multiCityNonStopChecked);
                    updateHeaderMessage();
                }
            } else if (serviceType == GET_FARE_RULE_RESULTS) {
                JSONObject resultDataObj = null;
                String tdrResult = "";
                try {
                    resultDataObj = new JSONObject(data);
                    tdrResult = resultDataObj.getJSONObject("fareruleRS").getString("tdr");
                } catch (Exception e) {
                    Utils.printMessage(TAG, "Exception occured::" + e.getMessage());
                }
                if (tdrResult == null || tdrResult.equalsIgnoreCase("")) {
                    closeLoading();
                    displayErrorMessage(getResources().getString(R.string.label_selected_flight_error_message));
                    return;
                } else {
                    Intent intent = new Intent(MultiCityResultsActivity.this, TripSummaryActivity.class);
                    intent.putExtra(Constants.JSON_RESULT, data);
                    intent.putExtra(Constants.TRIP_TYPE, String.valueOf(tripType));
                    intent.putExtra(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                    intent.putExtra(Constants.AIPI_JSON_OBJECT, aipiJSONObject);
                    intent.putExtra(Constants.AI_JSON_OBJECT, aiJSONObject);
                    intent.putExtra(Constants.FORWARD_SOURCE, source);
                    intent.putExtra(Constants.FORWARD_DESTINATION, destination);
                    intent.putExtra(Constants.SECOND_FORWARD_SOURCE, secondSource);
                    intent.putExtra(Constants.SECOND_FORWARD_DESTINATION, secondDestination);
                    intent.putExtra(Constants.THIRD_FORWARD_SOURCE, thirdSource);
                    intent.putExtra(Constants.THIRD_FORWARD_DESTINATION, thirdDestination);
                    intent.putExtra(Constants.FIRST_FLIGHT_DATE, firstFlightDate);
                    intent.putExtra(Constants.SECOND_FLIGHT_DATE, secondFlightDate);
                    intent.putExtra(Constants.THIRD_FLIGHT_DATE, thirdFlightDate);
                    startActivityForResult(intent, SEARCH_FLIGHT_RESULT);
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private boolean parseJSON(String data) {
        boolean isConversionDone = false;
        mainPricedItineraryArray.clear();
        Singleton.getInstance().resultFlightsArrayList.clear();
        pricedItineraryObjectArrayList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(data);
            if (obj.has("searchRS")) {
                if (obj.getJSONObject("searchRS").has("pi")) {
                    JSONArray piArray = obj.getJSONObject("searchRS").getJSONArray("pi");
                    for (int i = 0; i < piArray.length(); i++) {
                        PricedItineraryObject piObject = new PricedItineraryObject();
                        if (piArray.getJSONObject(i).has("ai")) {
                            if (piArray.getJSONObject(i).getJSONObject("ai").has("odos")) {
                                if (piArray.getJSONObject(i).getJSONObject("ai").getJSONObject("odos").has("odo")) {
                                    JSONArray odoArray = piArray.getJSONObject(i).getJSONObject("ai").getJSONObject("odos").getJSONArray("odo");
                                    AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                                    aiBean.setDirId(piArray.getJSONObject(i).getJSONObject("ai").getString("dirId"));
                                    long segmentFlightDuration = -1;
                                    Integer segmentDepartureTime = -1;
                                    ArrayList<OriginDestinationOptionBean> odoArrayList = new ArrayList<>();
                                    for (int j = 0; j < odoArray.length(); j++) {
                                        OriginDestinationOptionBean odoBean = new OriginDestinationOptionBean();
                                        odoBean.setRefNum(odoArray.getJSONObject(j).getString("refNum"));
                                        odoBean.setRph(odoArray.getJSONObject(j).getString("rph"));
                                        odoBean.setTt(odoArray.getJSONObject(j).getString("tt"));
                                        odoBean.setRs(odoArray.getJSONObject(j).getString("rs"));
                                        odoBean.setCb(odoArray.getJSONObject(j).getString("cb"));
                                        odoBean.setPt(odoArray.getJSONObject(j).getString("pt"));
                                        ArrayList<FSDataBean> fsDataBeanArrayList = new ArrayList<>();
                                        String flightDuration = "";
                                        for (int k = 0; k < odoArray.getJSONObject(j).getJSONArray("fs").length(); k++) {
                                            JSONObject fsObj = odoArray.getJSONObject(j).getJSONArray("fs").getJSONObject(k);
                                            FSDataBean fsDataBean = new FSDataBean();
                                            fsDataBean.setDap(fsObj.getString("dap"));
                                            fsDataBean.setDt(fsObj.getString("dt"));
                                            fsDataBean.setAap(fsObj.getString("aap"));
                                            fsDataBean.setAt(fsObj.getString("at"));
                                            fsDataBean.setOal(fsObj.getString("oal"));
                                            fsDataBean.setMal(fsObj.getString("mal"));
                                            fsDataBean.setEq(fsObj.getString("eq"));
                                            fsDataBean.setCt(fsObj.getString("ct"));
                                            fsDataBean.setDur(fsObj.getString("dur"));
                                            fsDataBean.setFdur(fsObj.getString("fdur"));
                                            fsDataBean.setSt(fsObj.getString("st"));
                                            fsDataBean.setSi(fsObj.getString("si"));
                                            fsDataBean.setTi(fsObj.getString("ti"));
                                            fsDataBean.setFi(fsObj.getString("fi"));
                                            fsDataBean.setComment(fsObj.getString("comment"));
                                            fsDataBean.setAs(fsObj.getString("as"));
                                            fsDataBean.setFc(fsObj.getString("fc"));
                                            fsDataBean.setTofi(fsObj.getString("tofi"));
                                            fsDataBean.setFn(fsObj.getString("fn"));
                                            fsDataBean.setIs(fsObj.getString("is"));
                                            fsDataBean.setSq(fsObj.getString("sq"));
                                            fsDataBean.setArdt(fsObj.getString("ardt"));
                                            fsDataBean.setDdt(fsObj.getString("ddt"));
                                            fsDataBeanArrayList.add(fsDataBean);
                                            flightDuration = fsObj.getString("dur");
                                        }
                                        long durationInMinutes = -1;
                                        String[] durArr = flightDuration.split(":");
                                        if (durArr.length > 0) {
                                            durationInMinutes = (Integer.parseInt(durArr[0]) * 60) + Integer.parseInt(durArr[1]);
                                        }
                                        odoBean.setFlightDuration(durationInMinutes);
                                        odoBean.setSegmentDepartureTime(Utils.dateFormaterToMinutes(fsDataBeanArrayList.get(0).getDdt()));
                                        odoBean.setFsDataBeanArrayList(fsDataBeanArrayList);
                                        if (odoBean.getRs().equalsIgnoreCase("Refundable")) {
                                            odoBean.setRefundType(1.0);
                                        } else {
                                            odoBean.setRefundType(2.0);
                                        }
                                        odoArrayList.add(odoBean);
                                        if (segmentFlightDuration == -1) {
                                            segmentFlightDuration = durationInMinutes;
                                            segmentDepartureTime = Utils.dateFormaterToMinutes(fsDataBeanArrayList.get(0).getDdt());
                                        } else {
                                            if (segmentFlightDuration > durationInMinutes) {
                                                segmentFlightDuration = durationInMinutes;
                                                segmentDepartureTime = Utils.dateFormaterToMinutes(fsDataBeanArrayList.get(0).getDdt());
                                            }
                                        }
                                    }
                                    aiBean.setSegmentDepartureTime(segmentDepartureTime);
                                    aiBean.setSegmentFlightDuration(segmentFlightDuration);
                                    aiBean.setOriginDestinationOptionBeanArrayList(odoArrayList);
                                    piObject.setAiBean(aiBean);
                                }
                            }
                        }
                        if (piArray.getJSONObject(i).has("aipi")) {
                            AirItineraryPricingInfoBean aipiBean = new AirItineraryPricingInfoBean();
                            if (piArray.getJSONObject(i).getJSONObject("aipi").has("itf")) {
                                ItinTotalFareObjectBean itfBean = new ItinTotalFareObjectBean();
                                itfBean.setBf(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("bf"));
                                itfBean.settTx(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("tTx"));
                                itfBean.settSF(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("tSF"));
                                itfBean.setDisc(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("disc"));
                                itfBean.settFare(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("tFare"));
                                itfBean.setIc(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("ic"));
                                itfBean.setUsfc(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("usfc"));
                                aipiBean.setItfObject(itfBean);
                            }
                            aipiBean.setFq(piArray.getJSONObject(i).getJSONObject("aipi").getString("fq"));
                            JSONArray pfbdArray = piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("pfbds").getJSONArray("pfbd");
                            ArrayList<PFBDObjectBean> pfbdObjectBeanArrayList = new ArrayList<>();
                            for (int j = 0; j < pfbdArray.length(); j++) {
                                PFBDObjectBean pfbdObjectBean = new PFBDObjectBean();
                                pfbdObjectBean.setpType(pfbdArray.getJSONObject(j).getString("pType"));
                                pfbdObjectBean.setQty(pfbdArray.getJSONObject(j).getString("qty"));
                                pfbdObjectBean.setUsfcp(pfbdArray.getJSONObject(j).getString("usfcp"));
                                ArrayList<String> fbcArray = new ArrayList<>();
                                if (pfbdArray.getJSONObject(j).has("fbc")) {
                                    try {
                                        if (!pfbdArray.getJSONObject(j).isNull("fbc")) {
                                            for (int k = 0; k < pfbdArray.getJSONObject(j).getJSONArray("fbc").length(); k++) {
                                                fbcArray.add(pfbdArray.getJSONObject(j).getJSONArray("fbc").getString(k));
                                            }
                                            pfbdObjectBean.setFbcArray(fbcArray);
                                        }
                                    } catch (Exception e) {
                                    }
                                }
                                PassengerFareObjectBean passengerFareObjectBean = new PassengerFareObjectBean();
                                passengerFareObjectBean.setPbf(pfbdArray.getJSONObject(j).getJSONObject("pf").getString("pbf"));
                                passengerFareObjectBean.setPtFare(pfbdArray.getJSONObject(j).getJSONObject("pf").getString("ptFare"));
                                ArrayList<TaxObjectBean> taxObjectBeanArrayList = new ArrayList<>();
                                for (int k = 0; k < pfbdArray.getJSONObject(j).getJSONObject("pf").getJSONArray("txs").length(); k++) {
                                    TaxObjectBean bean = new TaxObjectBean();
                                    bean.setAmt(pfbdArray.getJSONObject(j).getJSONObject("pf").getJSONArray("txs").getJSONObject(k).getString("amt"));
                                    bean.setTc(pfbdArray.getJSONObject(j).getJSONObject("pf").getJSONArray("txs").getJSONObject(k).getString("tc"));
                                    taxObjectBeanArrayList.add(bean);
                                }
                                passengerFareObjectBean.setTaxObjectBeanArrayList(taxObjectBeanArrayList);
                                pfbdObjectBean.setPassengerFareObjectBean(passengerFareObjectBean);
                                pfbdObjectBeanArrayList.add(pfbdObjectBean);
                            }
                            aipiBean.setPfbdObjectBeanArrayList(pfbdObjectBeanArrayList);
                            piObject.setAipiBean(aipiBean);
                        }
                        mainPricedItineraryArray.add(piObject);
                        isConversionDone = true;
                    }
                }
                if (obj.getJSONObject("searchRS").has("errors")) {
                    displayResultError();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            isConversionDone = false;
        }
        Singleton.getInstance().resultFlightsArrayList = mainPricedItineraryArray;
        forwardPIArrayList.clear();
        for (int i = 0; i < mainPricedItineraryArray.size(); i++) {
            PricedItineraryObject pricedItineraryObject = new PricedItineraryObject();
            AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
            aiBean.setDirId(mainPricedItineraryArray.get(i).getAiBean().getDirId());
            aiBean.setOriginDestinationOptionBeanArrayList(mainPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList());
            pricedItineraryObject.setAiBean(aiBean);
            pricedItineraryObject.setAipiBean(mainPricedItineraryArray.get(i).getAipiBean());
            forwardPIArrayList.add(pricedItineraryObject);
        }
        sortByDuration();
        pricedItineraryObjectArrayList.addAll(forwardPIArrayList);
        if (pricedItineraryObjectArrayList.size() > 0) {
            loadData(pricedItineraryObjectArrayList);
            getAirportDetails();
            Utils.getFlightStopValues(this, tripType, isNonStopChecked, multiCityNonStopChecked, pricedItineraryObjectArrayList);
            stopsArray = Singleton.getInstance().resultStopsArrayList;
        } else {
            displayResultError();
        }
        return isConversionDone;
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private void displayResultError() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View errorView = inflater.inflate(R.layout.view_error_response, null);
        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
        errorText = (TextView) errorView.findViewById(R.id.error_text);
        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
        searchButton = (Button) errorView.findViewById(R.id.search_button);
        errorText.setTypeface(regularFace);
        errorDescriptionText.setTypeface(regularFace);
        searchButton.setTypeface(regularFace);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                headerViewLayout.removeView(errorView);
            }
        });
        loadErrorType(Constants.RESULT_ERROR);
        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        headerViewLayout.addView(errorView);
    }

    private void updateStopsFlights(ArrayList<FilterStopSelectionItem> stopsArray, boolean isNonStopChecked) {
        if (isNonStopChecked) {
            for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                String currentStopType = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFsDataBeanArrayList().get(0).getSq();
                if (currentStopType.equalsIgnoreCase("null")) {
                    pricedItineraryObjectArrayList.remove(i);
                } else if (!currentStopType.equalsIgnoreCase("0")) {
                    pricedItineraryObjectArrayList.remove(i);
                }
            }
            if (pricedItineraryObjectArrayList.size() == 0) {
                pricedItineraryObjectArrayList.addAll(forwardPIArrayList);
            }
            isFilterApplied = true;
        } else {
            boolean isStopsNoneSelected = true;
            ArrayList<Integer> selectedStopsList = new ArrayList<>();
            if (stopsArray.size() != 0) {
                for (FilterStopSelectionItem stopVar : stopsArray) {
                    if (stopVar.isSelected()) {
                        isStopsNoneSelected = false;
                        selectedStopsList.add(stopVar.getStopType());
                    }
                }
            }
            if (!isStopsNoneSelected) {
                for (FilterStopSelectionItem selectedVar : stopsArray) {
                    if (!selectedVar.isSelected()) {
                        for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                            ArrayList<Integer> selectedList = new ArrayList<>();
                            OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                            for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                selectedList.add(Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq()));
                            }
                            if (checkStopsList(selectedStopsList, selectedList).isEmpty()) {
                                pricedItineraryObjectArrayList.remove(i);
                            }
                        }
                    }
                }
            }
        }
    }

    private void loadData(ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList) {
        airlineArrayList.clear();
        airportArrayList.clear();
        for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
            for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                for (int k = 0; k < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getFsDataBeanArrayList().size(); k++) {
                    airlineArrayList.add(pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getFsDataBeanArrayList().get(k).getMal());
                    airportArrayList.add(pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getFsDataBeanArrayList().get(k).getAap());
                    airportArrayList.add(pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getFsDataBeanArrayList().get(k).getDap());
                }
            }
        }
    }

    private void getAirportDetails() {
        airlinesData.resultData(MultiCityResultsActivity.this, airlineArrayList, new AirlinesInterface<HashMap<String, String>>() {
            @Override
            public void onSuccess(HashMap<String, String> response) {
                if (response.size() > 0) {
                    airLineNamesMap.clear();
                    Utils.printMessage(TAG, "DATA::" + response.size());
                    airLineNamesMap.putAll(response);
                    int totalCnt = Integer.parseInt(totalPassengersCount);
                    mAdapter = new FlightMultiTripAdapter(MultiCityResultsActivity.this, pricedItineraryObjectArrayList, airportNamesMap,
                            airLineNamesMap, totalCnt, MultiCityResultsActivity.this);
                    multiRecyclerView.setAdapter(mAdapter);
                    closeLoading();
                } else {
                    closeLoading();
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(regularFace);
                    errorDescriptionText.setTypeface(regularFace);
                    searchButton.setTypeface(regularFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                            headerViewLayout.removeView(errorView);
                        }
                    });
                    loadErrorType(Constants.WRONG_ERROR_PAGE);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    headerViewLayout.addView(errorView);
                }
            }

            @Override
            public void onFailed(String message) {
                closeLoading();
                Utils.printMessage(TAG, "DATA::" + message);
                displayResultError();
            }
        });

        airportData.resultData(MultiCityResultsActivity.this, airportArrayList, new AirlinesInterface<HashMap<String, FlightAirportName>>() {
            @Override
            public void onSuccess(HashMap<String, FlightAirportName> response) {
                if (response.size() > 0) {
                    airportNamesMap.clear();
                    Utils.printMessage(TAG, "DATA::" + response.size());
                    airportNamesMap.putAll(response);
                }
            }

            @Override
            public void onFailed(String message) {
                Utils.printMessage(TAG, "DATA::" + message);
                displayResultError();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FLIGHT_PRICE_FILTER) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    isFilterApplied = true;
                    handleDefaultFlights();
                    pricedItineraryObjectArrayList.clear();
                    minValue = data.getStringExtra(Constants.PRICE_MINIMUM_VALUE);
                    maxValue = data.getStringExtra(Constants.PRICE_MAXIMUM_VALUE);
                    isNonStopChecked = data.getBooleanExtra(Constants.ONE_WAY_NONSTOP_CHECKED, false);
                    multiCityNonStopChecked = data.getBooleanExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, false);
                    double minVal = Double.parseDouble(minValue);
                    double maxVal = Double.parseDouble(maxValue);
                    for (int i = 0; i < forwardPIArrayList.size(); i++) {
                        double price = Double.parseDouble(forwardPIArrayList.get(i).getAipiBean().getItfObject().gettFare());
                        if (minVal <= price && maxVal >= price) {
                            pricedItineraryObjectArrayList.add(forwardPIArrayList.get(i));
                        }
                    }
                    refundStatusArray = Singleton.getInstance().refundStatusArrayList;
                    boolean isRefundSelected = true;
                    boolean isNonRefundSelected = true;
                    if (refundStatusArray.size() == 2) {
                        for (FilterRefundSelectionItem refundVar : refundStatusArray) {
                            if (refundVar.isSelected()) {
                                if (refundVar.getRefundType() == 0) {
                                    isRefundSelected = false;
                                } else if (refundVar.getRefundType() == 1) {
                                    isNonRefundSelected = false;
                                }
                            }
                        }
                    }
                    if (!isRefundSelected && isNonRefundSelected) {
                        for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                            OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                            if (!bean.getRs().equalsIgnoreCase("Refundable")) {
                                pricedItineraryObjectArrayList.remove(i);
                            }
                        }
                    } else if (isRefundSelected && !isNonRefundSelected) {
                        for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                            OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                            if (!bean.getRs().equalsIgnoreCase("Non Refundable")) {
                                pricedItineraryObjectArrayList.remove(i);
                            }
                        }
                    }
                    Utils.getFlightStopValues(this, tripType, isNonStopChecked, multiCityNonStopChecked, pricedItineraryObjectArrayList);
                    stopsArray = Singleton.getInstance().resultStopsArrayList;
                    boolean isStopsNoneSelected = true;
                    ArrayList<Integer> selectedStopsList = new ArrayList<>();
                    if (stopsArray.size() != 0) {
                        for (FilterStopSelectionItem stopVar : stopsArray) {
                            if (stopVar.isSelected()) {
                                isStopsNoneSelected = false;
                                selectedStopsList.add(stopVar.getStopType());
                            }
                        }
                    }
                    if (!isStopsNoneSelected) {
                        for (FilterStopSelectionItem selectedVar : stopsArray) {
                            if (!selectedVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<Integer> selectedList = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                        selectedList.add(Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq()));
                                    }
                                    if (checkStopsList(selectedStopsList, selectedList).isEmpty()) {
                                        pricedItineraryObjectArrayList.remove(i);
                                    }
                                }
                            }
                        }
                    }
                    airlineArray = Singleton.getInstance().resultAirlinesArrayList;
                    boolean isAirlineNoneSelected = true;
                    ArrayList<String> selectedAirlineList = new ArrayList<>();
                    for (FilterAirlineSelectionItem airlineVar : airlineArray) {
                        if (airlineVar.isSelected()) {
                            isAirlineNoneSelected = false;
                            selectedAirlineList.add(airlineVar.getMal());
                        }
                    }
                    if (!isAirlineNoneSelected) {
                        for (FilterAirlineSelectionItem selectedAirlineVar : airlineArray) {
                            if (!selectedAirlineVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airlineList = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                        airlineList.add(bean.getFsDataBeanArrayList().get(0).getMal());
                                    }
                                    if (checkAirlineList(selectedAirlineList, airlineList).isEmpty()) {
                                        pricedItineraryObjectArrayList.remove(i);
                                    }
                                }
                            }
                        }
                    }
                    airportListArray = Singleton.getInstance().resultAirportsArrayList;
                    tempArray1 = Singleton.getInstance().firstAirportsArrayList;
                    tempArray2 = Singleton.getInstance().secondAirportsArrayList;
                    tempArray3 = Singleton.getInstance().thirdAirportsArrayList;
                    boolean isNoneSelectedInSection1 = true;
                    boolean isNoneSelectedInSection2 = true;
                    boolean isNoneSelectedInSection3 = true;
                    ArrayList<String> firstAirportList = new ArrayList<>();
                    ArrayList<String> secondAirportList = new ArrayList<>();
                    ArrayList<String> thirdAirportList = new ArrayList<>();
                    for (FilterAirportSelectionItem airportItems : airportListArray) {
                        if (airportItems.isSelected()) {
                            if (airportItems.getAirportType() == 1) {
                                isNoneSelectedInSection1 = false;
                                firstAirportList.add(airportItems.getAirportCode());
                            } else if (airportItems.getAirportType() == 2) {
                                isNoneSelectedInSection2 = false;
                                secondAirportList.add(airportItems.getAirportCode());
                            } else {
                                isNoneSelectedInSection3 = false;
                                thirdAirportList.add(airportItems.getAirportCode());
                            }
                        }
                    }
                    if (!isNoneSelectedInSection1) {
                        for (FilterAirportSelectionItem selectedAirportVar : tempArray1) {
                            if (!selectedAirportVar.isSelected()) {
                                int airportType = selectedAirportVar.getAirportType();
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airportCodeArr = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                        airportCodeArr.add(bean.getFsDataBeanArrayList().get(k).getDap());
                                    }
                                    if (airportType == 1) {
                                        if (checkAirlineList(firstAirportList, airportCodeArr).isEmpty()) {
                                            pricedItineraryObjectArrayList.remove(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!isNoneSelectedInSection2) {
                        for (FilterAirportSelectionItem selectedAirportVar : tempArray2) {
                            if (!selectedAirportVar.isSelected()) {
                                int airportType = selectedAirportVar.getAirportType();
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airportCodeArr = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                        airportCodeArr.add(bean.getFsDataBeanArrayList().get(k).getAap());
                                    }
                                    if (airportType == 2) {
                                        if (checkAirlineList(secondAirportList, airportCodeArr).isEmpty()) {
                                            pricedItineraryObjectArrayList.remove(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!isNoneSelectedInSection3) {
                        for (FilterAirportSelectionItem selectedAirportVar : tempArray3) {
                            int airportType = selectedAirportVar.getAirportType();
                            if (!selectedAirportVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airportCodeArr = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                        airportCodeArr.add(bean.getFsDataBeanArrayList().get(k).getDap());
                                    }
                                    if (airportType == 3) {
                                        if (checkAirlineList(thirdAirportList, airportCodeArr).isEmpty()) {
                                            pricedItineraryObjectArrayList.remove(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    timeArray = Singleton.getInstance().resultTimeArrayList;
                    dapTimeListArray = Singleton.getInstance().departureTimeArrayList;
                    aapTimeListArray = Singleton.getInstance().arrivalTimeArrayList;
                    boolean dapFlightLayoutSelected = true;
                    boolean aapFlightLayoutSelected = true;
                    ArrayList<Integer> departTimeArr = new ArrayList<>();
                    ArrayList<Integer> arrivalTimeArr = new ArrayList<>();
                    for (int i = 0; i < timeArray.size(); i++) {
                        for (FilterTimeSelectionItem flightTimeSelected : dapTimeListArray) {
                            if (flightTimeSelected.isSelected()) {
                                dapFlightLayoutSelected = false;
                                if (!departTimeArr.contains(flightTimeSelected.getTimeZone())) {
                                    departTimeArr.add(flightTimeSelected.getTimeZone());
                                }
                            }
                        }
                        for (FilterTimeSelectionItem flightTimeSelected : aapTimeListArray) {
                            if (flightTimeSelected.isSelected()) {
                                aapFlightLayoutSelected = false;
                                if (!arrivalTimeArr.contains(flightTimeSelected.getTimeZone())) {
                                    arrivalTimeArr.add(flightTimeSelected.getTimeZone());
                                }
                            }
                        }
                    }
                    if (!dapFlightLayoutSelected) {
                        for (FilterTimeSelectionItem selectedTimeVar : dapTimeListArray) {
                            if (!selectedTimeVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    int dapFlightTime = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                                    if (!departTimeArr.contains(dapFlightTime)) {
                                        pricedItineraryObjectArrayList.remove(i);
                                    }
                                }
                            }
                        }
                    }
                    if (!aapFlightLayoutSelected) {
                        for (FilterTimeSelectionItem selectedTimeVar : aapTimeListArray) {
                            if (!selectedTimeVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    int aapFlightTime = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(bean.getFsDataBeanArrayList().size() - 1).getArdt());
                                    if (!arrivalTimeArr.contains(aapFlightTime)) {
                                        pricedItineraryObjectArrayList.remove(i);
                                    }
                                }
                            }
                        }
                    }
                    Utils.printMessage(TAG, "Apply Button Pi list size :: " + pricedItineraryObjectArrayList.size());
                    if (pricedItineraryObjectArrayList.size() == 0) {
                        handleFilterResults();
                        pricedItineraryObjectArrayList.addAll(forwardPIArrayList);
                        updateHeaderMessage();
                        displayErrorMessage(getString(R.string.label_no_flights_available));
                        return;
                    } else {
                        updateHeaderMessage();
                    }
                }
            }
            if (resultCode == RESULT_CANCELED) {
                if (data != null) {
                    handleDefaultFlights();
                    isNonStopChecked = data.getBooleanExtra(Constants.ONE_WAY_NONSTOP_CHECKED, false);
                    multiCityNonStopChecked = data.getBooleanExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, false);
                    pricedItineraryObjectArrayList.clear();
                    pricedItineraryObjectArrayList.addAll(forwardPIArrayList);
                    minValue = data.getStringExtra(Constants.PRICE_MINIMUM_VALUE);
                    maxValue = data.getStringExtra(Constants.PRICE_MAXIMUM_VALUE);
                    double minVal = Double.parseDouble(minValue);
                    double maxVal = Double.parseDouble(maxValue);
                    pricedItineraryObjectArrayList.clear();
                    for (int i = 0; i < forwardPIArrayList.size(); i++) {
                        double price = Double.parseDouble(forwardPIArrayList.get(i).getAipiBean().getItfObject().gettFare());
                        if (minVal <= price && maxVal >= price) {
                            pricedItineraryObjectArrayList.add(forwardPIArrayList.get(i));
                        }
                    }
                    updateHeaderMessage();
                    Utils.printMessage(TAG, "Reset Button Pi list size :: " + pricedItineraryObjectArrayList.size());
                    if (pricedItineraryObjectArrayList.size() != 0) {
                        headerViewLayout.removeView(noResultView);
                        noResultView = null;
                    }
                }
            }
        } else if (requestCode == SEARCH_FLIGHT_RESULT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    headerViewLayout.removeView(loadingView);
                    loadingView = null;
                }
            } else if (resultCode == Constants.CHANGE_FLIGHT) {
                if (loadingView != null) {
                    headerViewLayout.removeView(loadingView);
                    loadingView = null;
                }
            }
        }
    }

    private void updateHeaderMessage() {
        totalFlightCount.setText(String.valueOf(forwardPIArrayList.size()));
        totalFlightCount.append(Utils.addFlightTextField(getString(R.string.label_flights), MultiCityResultsActivity.this));
        fastestFlightCount.setText(String.valueOf(fastestItineraryObjectArrayList.size()));
        fastestFlightCount.append(Utils.addFlightHoverTextField(getString(R.string.label_fastest), MultiCityResultsActivity.this));
        earliestFlightCount.setText(String.valueOf(earliestItineraryObjectArrayList.size()));
        earliestFlightCount.append(Utils.addFlightHoverTextField(getString(R.string.label_earliest), MultiCityResultsActivity.this));
        int totalCnt = Integer.parseInt(totalPassengersCount);
        mAdapter = new FlightMultiTripAdapter(MultiCityResultsActivity.this, pricedItineraryObjectArrayList, airportNamesMap,
                airLineNamesMap, totalCnt, MultiCityResultsActivity.this);
        multiRecyclerView.setAdapter(mAdapter);
    }

    private void handleDefaultFlights() {
        totalCountLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.white_color));
        earliestCountLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
        fastestCountLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
        totalCountSubLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.white_color));
        earliestCountSubLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
        fastestCountSubLayout.setBackgroundColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_view_bg));
        totalFlightView.setVisibility(View.VISIBLE);
        earliestFlightView.setVisibility(View.INVISIBLE);
        fastestFlightView.setVisibility(View.INVISIBLE);
        totalFlightCount.setTextColor(Utils.getColor(MultiCityResultsActivity.this, R.color.result_compo_duration));
        earliestFlightCount.setTextColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_header_label_color));
        fastestFlightCount.setTextColor(Utils.getColor(MultiCityResultsActivity.this, R.color.multi_header_label_color));
    }

    private void handleEarliestFlights() {
        try {
            if (earliestItineraryObjectArrayList.size() == 0) {
                for (int i = 0; i < forwardPIArrayList.size(); i++) {
                    OriginDestinationOptionBean bean = forwardPIArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                    int departureTimeVal = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                    if (departureTimeVal == 2) {
                        earliestItineraryObjectArrayList.add(forwardPIArrayList.get(i));
                    }
                }
                updateEarliestFlt();
            } else {
                updateEarliestFlt();
            }
        } catch (Exception e) {
            Utils.printMessage(TAG, "Earliest Err");
        }
    }

    private void updateEarliestFlt() {
        if (isEarliestClicked) {
            earliestFlightCount.setText(String.valueOf(earliestItineraryObjectArrayList.size()));
            earliestFlightCount.append(Utils.addFlightTextField(getString(R.string.label_earliest), MultiCityResultsActivity.this));
        } else {
            earliestFlightCount.setText(String.valueOf(earliestItineraryObjectArrayList.size()));
            earliestFlightCount.append(Utils.addFlightHoverTextField(getString(R.string.label_earliest), MultiCityResultsActivity.this));
        }
        totalFlightCount.setText(String.valueOf(forwardPIArrayList.size()));
        totalFlightCount.append(Utils.addFlightHoverTextField(getString(R.string.label_flights), MultiCityResultsActivity.this));
        fastestFlightCount.setText(String.valueOf(fastestItineraryObjectArrayList.size()));
        fastestFlightCount.append(Utils.addFlightHoverTextField(getString(R.string.label_fastest), MultiCityResultsActivity.this));
        int totalCnt = Integer.parseInt(totalPassengersCount);
        pricedItineraryObjectArrayList.clear();
        pricedItineraryObjectArrayList.addAll(earliestItineraryObjectArrayList);
        mAdapter = new FlightMultiTripAdapter(MultiCityResultsActivity.this, pricedItineraryObjectArrayList, airportNamesMap,
                airLineNamesMap, totalCnt, MultiCityResultsActivity.this);
        multiRecyclerView.setAdapter(mAdapter);
    }

    private void handleNonStopFlights() {
        try {
            if (fastestItineraryObjectArrayList.size() == 0) {
                for (int i = 0; i < forwardPIArrayList.size(); i++) {
                    boolean isFastestAvailable = false;
                    for (int j = 0; j < 1; j++) {
                        OriginDestinationOptionBean bean = forwardPIArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                        if (Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq()) > 0) {
                            isFastestAvailable = true;
                            break;
                        } else {
                            isFastestAvailable = false;
                        }
                    }
                    if (!isFastestAvailable) {
                        fastestItineraryObjectArrayList.add(forwardPIArrayList.get(i));
                    }
                }
                if (fastestItineraryObjectArrayList.size() == 0) {
                    for (int i = 0; i < forwardPIArrayList.size(); i++) {
                        boolean isFastestAvailable = false;
                        for (int j = 0; j < 1; j++) {
                            OriginDestinationOptionBean bean = forwardPIArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                            if (Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq()) > 1) {
                                isFastestAvailable = true;
                                break;
                            } else {
                                isFastestAvailable = false;
                            }
                        }
                        if (!isFastestAvailable) {
                            fastestItineraryObjectArrayList.add(forwardPIArrayList.get(i));
                        }
                    }
                }
                updateFastestFlt();
            } else {
                updateFastestFlt();
            }
        } catch (Exception e) {
            Utils.printMessage(TAG, "Earliest Err");
        }
    }

    private void updateFastestFlt() {
        if (isFastestClicked) {
            fastestFlightCount.setText(String.valueOf(fastestItineraryObjectArrayList.size()));
            fastestFlightCount.append(Utils.addFlightTextField(getString(R.string.label_fastest), MultiCityResultsActivity.this));
        } else {
            fastestFlightCount.setText(String.valueOf(fastestItineraryObjectArrayList.size()));
            fastestFlightCount.append(Utils.addFlightHoverTextField(getString(R.string.label_fastest), MultiCityResultsActivity.this));
        }
        totalFlightCount.setText(String.valueOf(forwardPIArrayList.size()));
        totalFlightCount.append(Utils.addFlightHoverTextField(getString(R.string.label_flights), MultiCityResultsActivity.this));
        earliestFlightCount.setText(String.valueOf(earliestItineraryObjectArrayList.size()));
        earliestFlightCount.append(Utils.addFlightHoverTextField(getString(R.string.label_earliest), MultiCityResultsActivity.this));
        pricedItineraryObjectArrayList.clear();
        pricedItineraryObjectArrayList.addAll(fastestItineraryObjectArrayList);
        int totalCnt = Integer.parseInt(totalPassengersCount);
        mAdapter = new FlightMultiTripAdapter(MultiCityResultsActivity.this, pricedItineraryObjectArrayList, airportNamesMap,
                airLineNamesMap, totalCnt, MultiCityResultsActivity.this);
        multiRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(int position) {
        PricedItineraryObject pricedItineraryObject = pricedItineraryObjectArrayList.get(position);
        String firstArrivalTime = "";
        String secondDepartureTime = "";
        String secondArrivalTime = "";
        String thirdDepartureTime = "";
        for (int i = 0; i < pricedItineraryObject.getAiBean().getOriginDestinationOptionBeanArrayList().size(); i++) {
            OriginDestinationOptionBean odoObject = pricedItineraryObject.getAiBean().getOriginDestinationOptionBeanArrayList().get(i);
            if (i == 0) {
                firstArrivalTime = odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getArdt();
            } else if (i == 1) {
                secondDepartureTime = odoObject.getFsDataBeanArrayList().get(0).getDdt();
                secondArrivalTime = odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getArdt();
            } else if (i == 2) {
                thirdDepartureTime = odoObject.getFsDataBeanArrayList().get(0).getDdt();
            }
        }
        if (!secondSource.isEmpty()) {
            int durationInMinutes = Utils.getJourneyDifferenceTime(firstArrivalTime, secondDepartureTime);
            if (durationInMinutes < Constants.TIME_DIFFERENCE_MINUTES) {
                displayErrorMessage(getResources().getString(R.string.label_time_difference_error_msg));
                return;
            }
        }
        if (!thirdSource.isEmpty()) {
            int durationInMinutes = Utils.getJourneyDifferenceTime(secondArrivalTime, thirdDepartureTime);
            if (durationInMinutes < Constants.TIME_DIFFERENCE_MINUTES) {
                displayErrorMessage(getResources().getString(R.string.label_time_difference_error_msg));
                return;
            }
        }
        handleCheckAvailabilityRQ(position);
    }

    @Override
    public void onSelectedItemClick(int position, int type) {

    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private void handleCheckAvailabilityRQ(int position) {
        final String requestJsonData = handleFareRQRequest(position);
        Utils.printMessage(TAG, "requestJsonData : " + requestJsonData);
        if (isInternetPresent) {
            showLoading();
            HTTPAsync async = new HTTPAsync(MultiCityResultsActivity.this, MultiCityResultsActivity.this, Constants.FARE_RQ_URL,
                    "", requestJsonData, GET_FARE_RULE_RESULTS, HTTPAsync.METHOD_POST);
            async.execute();
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(regularFace);
            errorDescriptionText.setTypeface(regularFace);
            searchButton.setTypeface(regularFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(MultiCityResultsActivity.this);
                    if (isInternetPresent) {
                        headerViewLayout.removeView(errorView);
                        showLoading();
                        HTTPAsync async = new HTTPAsync(MultiCityResultsActivity.this, MultiCityResultsActivity.this,
                                Constants.FARE_RQ_URL, "", requestJsonData, GET_FARE_RULE_RESULTS, HTTPAsync.METHOD_POST);
                        async.execute();
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            headerViewLayout.addView(errorView);
        }
    }

    private String handleFareRQRequest(int position) {
        JSONObject finalObj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        try {
            JSONObject mainJSON = new JSONObject();
            JSONObject sourceObj = new JSONObject();
            sourceObj = Utils.getAppSettingData(sourceObj, MultiCityResultsActivity.this);
            sourceObj.put("isoCurrency", "SAR");
            sourceObj.put("echoToken", randomNumber);
            sourceObj.put("timeStamp", timeStamp);
            sourceObj.put("clientId", Constants.CLIENT_ID);
            sourceObj.put("clientSecret", Constants.CLIENT_SECRET);
            sourceObj.put("accessToken", Utils.getRequestAccessToken(MultiCityResultsActivity.this));
            sourceObj.put("groupType", groupType);
            mainJSON.put("source", sourceObj);
            JSONArray arr = new JSONArray();
            for (int i = 0; i < pricedItineraryObjectArrayList.get(position).getAiBean().getOriginDestinationOptionBeanArrayList().size(); i++) {
                arr.put(createFSObject(pricedItineraryObjectArrayList.get(position).getAiBean().getOriginDestinationOptionBeanArrayList().get(i)));
            }
            JSONObject aiJson = new JSONObject();
            aiJson.put("dirId", pricedItineraryObjectArrayList.get(position).getAiBean().getDirId());
            JSONObject odoArrObj = new JSONObject();
            odoArrObj.put("odo", arr);
            aiJson.put("odos", odoArrObj);
            JSONObject pfbdObj = new JSONObject();
            pfbdObj.put("pfbd", buildPFBDArray(position));
            JSONObject itfObj = new JSONObject();
            itfObj.put("bf", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().getBf()));
            itfObj.put("tTx", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().gettTx()));
            itfObj.put("tSF", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().gettSF()));
            itfObj.put("disc", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().getDisc()));
            itfObj.put("tFare", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().gettFare()));
            itfObj.put("ic", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().getIc()));
            itfObj.put("usfc", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().getUsfc()));
            JSONObject aipiObject = new JSONObject();
            aipiObject.put("itf", itfObj);
            aipiObject.put("pfbds", pfbdObj);
            aipiObject.put("fq", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getFq()));
            mainJSON.put("extra", "");
            mainJSON.put("source", sourceObj);
            mainJSON.put("ai", aiJson);
            mainJSON.put("aipi", aipiObject);
            mainJSON.put("ptq", Singleton.getInstance().ptqArr);
            aipiJSONObject = aipiObject.toString();
            aiJSONObject = aiJson.toString();
            finalObj.put("fareruleRQ", mainJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalObj.toString();
    }

    private JSONObject createFSObject(OriginDestinationOptionBean odoBean) {
        JSONArray array = new JSONArray();
        for (int i = 0; i < odoBean.getFsDataBeanArrayList().size(); i++) {
            FSDataBean dataBean = odoBean.getFsDataBeanArrayList().get(i);
            JSONObject detailsObj = new JSONObject();
            try {
                detailsObj.put("dap", Utils.checkStringValue(dataBean.getDap()));
                detailsObj.put("dt", Utils.checkStringValue(dataBean.getDt()));
                detailsObj.put("aap", Utils.checkStringValue(dataBean.getAap()));
                detailsObj.put("at", Utils.checkStringValue(dataBean.getAt()));
                detailsObj.put("oal", Utils.checkStringValue(dataBean.getOal()));
                detailsObj.put("mal", Utils.checkStringValue(dataBean.getMal()));
                detailsObj.put("eq", Utils.checkStringValue(dataBean.getEq()));
                detailsObj.put("ct", Utils.checkStringValue(dataBean.getCt()));
                detailsObj.put("dur", Utils.checkStringValue(dataBean.getDur()));
                detailsObj.put("fdur", Utils.checkStringValue(dataBean.getFdur()));
                detailsObj.put("st", Utils.checkStringValue(dataBean.getSt()));
                detailsObj.put("si", Utils.checkStringValue(dataBean.getSi()));
                detailsObj.put("ti", Utils.checkStringValue(dataBean.getTi()));
                detailsObj.put("fi", Utils.checkStringValue(dataBean.getFi()));
                detailsObj.put("comment", Utils.checkStringValue(dataBean.getComment()));
                detailsObj.put("as", Utils.checkStringValue(dataBean.getAs()));
                detailsObj.put("fc", Utils.checkStringValue(dataBean.getFc()));
                detailsObj.put("tofi", Utils.checkStringValue(dataBean.getTofi()));
                detailsObj.put("fn", Utils.checkStringValue(dataBean.getFn()));
                detailsObj.put("sq", Utils.checkStringValue(dataBean.getSq()));
                detailsObj.put("is", Utils.checkStringValue(dataBean.getIs()));
                detailsObj.put("ardt", Utils.checkStringValue(dataBean.getArdt()));
                detailsObj.put("ddt", Utils.checkStringValue(dataBean.getDdt()));
                array.put(detailsObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        JSONObject odoObj = new JSONObject();
        try {
            odoObj.put("refNum", Utils.checkStringValue(odoBean.getRefNum()));
            odoObj.put("rph", Utils.checkStringValue(odoBean.getRph()));
            odoObj.put("fs", array);
            odoObj.put("tt", Utils.checkStringValue(odoBean.getTt()));
            odoObj.put("rs", Utils.checkStringValue(odoBean.getRs()));
            odoObj.put("cb", Utils.checkStringValue(odoBean.getCb()));
            odoObj.put("pt", Utils.checkStringValue(odoBean.getPt()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return odoObj;
    }

    private JSONArray buildPFBDArray(int position) {
        JSONArray pfbdArray = new JSONArray();
        for (PFBDObjectBean pfbdObjBean : pricedItineraryObjectArrayList.get(position).getAipiBean().getPfbdObjectBeanArrayList()) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("pType", Utils.checkStringValue(pfbdObjBean.getpType()));
                obj.put("qty", Utils.checkStringValue(pfbdObjBean.getQty()));
                obj.put("usfcp", Utils.checkStringValue(pfbdObjBean.getUsfcp()));
                JSONArray array = new JSONArray();
                for (String str : pfbdObjBean.getFbcArray()) {
                    array.put(str);
                }
                obj.put("fbc", array);
                JSONObject pfObj = new JSONObject();
                pfObj.put("pbf", pfbdObjBean.getPassengerFareObjectBean().getPbf());
                pfObj.put("ptFare", pfbdObjBean.getPassengerFareObjectBean().getPtFare());
                JSONArray txArray = new JSONArray();
                for (TaxObjectBean tBean : pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList()) {
                    JSONObject tObj = new JSONObject();
                    tObj.put("amt", Utils.checkStringValue(tBean.getAmt()));
                    tObj.put("tc", Utils.checkStringValue(tBean.getTc()));
                    txArray.put(tObj);
                }
                pfObj.put("txs", txArray);
                obj.put("pf", pfObj);
                pfbdArray.put(obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return pfbdArray;
    }

    private ArrayList<String> checkAirlineList(ArrayList<String> selectedList, ArrayList<String> beanList) {
        List<String> firstList = new ArrayList<>(selectedList);
        List<String> secondList = new ArrayList<>(beanList);
        ArrayList<String> resultList = new ArrayList<>(secondList);
        resultList.retainAll(firstList);
        return resultList;
    }

    private ArrayList<Integer> checkStopsList(ArrayList<Integer> selectedList, ArrayList<Integer> beanList) {
        List<Integer> firstList = new ArrayList<>(selectedList);
        List<Integer> secondList = new ArrayList<>(beanList);
        ArrayList<Integer> resultList = new ArrayList<>(secondList);
        resultList.retainAll(firstList);
        return resultList;
    }

    private void handleFilterResults() {
        if (isFilterApplied) {
            Singleton.getInstance().refundStatusArrayList.clear();
            Singleton.getInstance().resultStopsArrayList.clear();
            Singleton.getInstance().resultAirlinesArrayList.clear();
            Singleton.getInstance().resultAirportsArrayList.clear();
            Singleton.getInstance().firstAirportsArrayList.clear();
            Singleton.getInstance().secondAirportsArrayList.clear();
            Singleton.getInstance().thirdAirportsArrayList.clear();
            Singleton.getInstance().resultTimeArrayList.clear();
            Singleton.getInstance().departureTimeArrayList.clear();
            Singleton.getInstance().arrivalTimeArrayList.clear();
            multiCityNonStopChecked = false;
            minValue = null;
            maxValue = null;
            isFilterApplied = false;
        }
    }

    private void sortByDuration() {
        Collections.sort(forwardPIArrayList, new Comparator<PricedItineraryObject>() {
            @Override
            public int compare(PricedItineraryObject e1, PricedItineraryObject e2) {
                int resultVal = 0;
                String id1 = e1.getAipiBean().getItfObject().gettFare();
                String id2 = e2.getAipiBean().getItfObject().gettFare();
                Double price1 = 0.0;
                Double price2 = 0.0;
                try {
                    price1 = Double.parseDouble(id1);
                    price2 = Double.parseDouble(id2);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                int result = price1.compareTo(price2);
                if (result > 0) {
                    resultVal = result;
                } else if (result < 0) {
                    resultVal = result;
                } else {
                    Long firstDur = Long.valueOf(e1.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getSegmentDepartureTime());
                    Long secondDur = Long.valueOf(e2.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getSegmentDepartureTime());
                    resultVal = firstDur.compareTo(secondDur);
//                    Long firstDur = Long.valueOf(e1.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getSegmentDepartureTime());
//                    Long secondDur = Long.valueOf(e2.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getSegmentDepartureTime());
//                    int durResult = firstDur.compareTo(secondDur);
//                    if(durResult > 0){
//                        resultVal = durResult;
//                    }else if(durResult < 0){
//                        resultVal = durResult;
//                    }else {
//                        Long firstFlight = e1.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFlightDuration();
//                        Long secondFlight = e2.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFlightDuration();
//                        resultVal = firstFlight.compareTo(secondFlight);
//                    }
                }
                return resultVal;
            }
        });
    }
}
