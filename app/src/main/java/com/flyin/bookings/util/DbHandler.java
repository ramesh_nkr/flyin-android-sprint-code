package com.flyin.bookings.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.os.Environment;

import com.flyin.bookings.model.AirMealList;
import com.flyin.bookings.model.CityModel;
import com.flyin.bookings.model.CountryNamesList;
import com.flyin.bookings.model.PredectiveSearchModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DbHandler extends SQLiteOpenHelper {
    private static final String TAG = "DbHandler";
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "AirportsList.db";
    private static final String TABLE_NAME = "airport";
    private static final String RECENT_TABLE_NAME = "recentAirport";
    private static final String AIRLINE_TABLE_NAME = "airlineData";
    private static final String COUNTRY_NAMES_TABLE_NAME = "countryNames";
    private static final String AIR_MEAL_TABLE_NAME = "airMealData";
    private static final String AIRPORT_TABLE_NAME = "airportNameData";
    private static final String RECENT_HOTELS_TABLE_NAME = "recentHotels";
    private static final String Z_PK = "z_pk";
    private static final String Z_ENT = "z_ent";
    private static final String Z_OPT = "z_opt";
    private static final String AIRPORT_ID = "airport_id";
    private static final String AIRPORT_CODE = "airport_code";
    private static final String AIRPORT_NAME = "airport_name";
    private static final String AIRPORT_CITY = "airport_city";
    private static final String AIRPORT_CITY_NAME = "airport_city_name";
    private static final String COUNTRY_NAME = "country_name";
    private static final String COUNTRY_LANGUAGE = "language";
    private static final String AIRLINE_CODE = "airline_code";
    private static final String AIRLINE_NAME = "airline_name";
    private static final String LANGUAGE = "language";
    private static final String COUNTRY_ID = "country_id";
    private static final String COUNTRY_NAME_ARAB = "country_name_arab";
    private static final String COUNTRY_SHORT_CODE = "country_short_code";
    private static final String COUNTRY_CODE = "country_code";
    private static final String AIR_MEAL_CODE = "air_meal_code";
    private static final String AIR_MEAL_NAME = "air_meal_name";
    private static final String HOTEL_ID = "hotel_id";
    private static final String HOTEL_CITY_NAME = "hotel_city_name";
    private static final String HOTEL_CITY_ID = "hotel_city_id";
    private static final String HOTEL_COUNTRY_NAME = "hotel_country_name";
    private static final String HOTEL_SEARCH_TYPE = "hotel_predictive_search_type";
    private static final String HOTEL_LANGUAGE = "hotelLanguage";
    private static final String HOTEL_NAME_ID = "hotel_name_id";
    private static final String ALL_AIRPORT = "all_airport";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String MISSPELLED_CITY_NAME = "misspelled_city_name";
    private static final String ISO_COUNTRY_CODE_TWO = "iso_country_code_two";
    private static final String ISO_COUNTRY_CODE_THREE = "iso_country_code_three";
    private Context context;

    public DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + AIRPORT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + AIRPORT_CODE + " TEXT, "
                + AIRPORT_NAME + " TEXT, "
                + AIRPORT_CITY + " TEXT, "
                + COUNTRY_NAME + " TEXT, "
                + COUNTRY_LANGUAGE + " TEXT, " +
                ALL_AIRPORT + " TEXT, " +
                LATITUDE + " REAL, " +
                LONGITUDE + " REAL," +
                MISSPELLED_CITY_NAME + " TEXT, " +
                ISO_COUNTRY_CODE_TWO + " TEXT, " +
                ISO_COUNTRY_CODE_THREE + " TEXT)";
        String CREATE_RECENT_TABLE = "CREATE TABLE IF NOT EXISTS " + RECENT_TABLE_NAME + " (" + AIRPORT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + AIRPORT_CODE + " TEXT, " + AIRPORT_NAME + " TEXT, " + AIRPORT_CITY + " TEXT, " + AIRPORT_CITY_NAME + " TEXT, " + COUNTRY_NAME + " TEXT, " + COUNTRY_LANGUAGE + " TEXT)";
        String CREATE_AIRLINE_TABLE = "CREATE TABLE IF NOT EXISTS " + AIRLINE_TABLE_NAME + " (" + AIRLINE_CODE + " TEXT, " + AIRLINE_NAME + " TEXT, " + LANGUAGE + " TEXT)";
        String CREATE_COUNTRY_TABLE = "CREATE TABLE IF NOT EXISTS " + COUNTRY_NAMES_TABLE_NAME + " (" + COUNTRY_ID + " TEXT, " + COUNTRY_NAME + " TEXT, " + COUNTRY_NAME_ARAB + " TEXT, " + COUNTRY_SHORT_CODE + " TEXT, " + COUNTRY_CODE + " TEXT)";
        String CREATE_AIR_MEAL_TABLE = "CREATE TABLE IF NOT EXISTS " + AIR_MEAL_TABLE_NAME + " (" + AIR_MEAL_CODE + " TEXT, " + AIR_MEAL_NAME + " TEXT, " + LANGUAGE + " TEXT)";
        String CREATE_AIRPORT_TABLE = "CREATE TABLE IF NOT EXISTS " + AIRPORT_TABLE_NAME + " (" + Z_PK + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Z_ENT + " INTEGER, " + Z_OPT + " INTEGER, " + AIRPORT_CODE + " TEXT, " + AIRPORT_NAME + " TEXT, " + AIRPORT_CITY + " TEXT, " + COUNTRY_NAME + " TEXT, " + LANGUAGE + " TEXT)";
        String CREATE_RECENT_HOTELS_TABLE = "CREATE TABLE IF NOT EXISTS " + RECENT_HOTELS_TABLE_NAME + " (" + HOTEL_ID +
                " INTEGER PRIMARY KEY AUTOINCREMENT, " + HOTEL_CITY_ID + " TEXT, " + HOTEL_CITY_NAME + " TEXT, "
                + HOTEL_COUNTRY_NAME + " TEXT, " + HOTEL_SEARCH_TYPE + " TEXT, "
                + HOTEL_LANGUAGE + " TEXT, " + HOTEL_NAME_ID + " TEXT)";
        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_RECENT_TABLE);
        db.execSQL(CREATE_AIRLINE_TABLE);
        db.execSQL(CREATE_COUNTRY_TABLE);
        db.execSQL(CREATE_AIR_MEAL_TABLE);
        db.execSQL(CREATE_AIRPORT_TABLE);
        db.execSQL(CREATE_RECENT_HOTELS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void addRecentAirport(String airportCode, String airportName, String airportCity, String countryName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues airportTable = new ContentValues();
        airportTable.put(AIRPORT_CODE, airportCode);
        airportTable.put(AIRPORT_NAME, airportName);
        airportTable.put(AIRPORT_CITY, airportCity);
        airportTable.put(AIRPORT_CITY_NAME, "");
        airportTable.put(COUNTRY_NAME, countryName);
        airportTable.put(COUNTRY_LANGUAGE, "");
        db.insert(RECENT_TABLE_NAME, null, airportTable);
        db.close();
    }

    public void addRecentHotel(CityModel model, String keyboardLang) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues airportTable = new ContentValues();

        Utils.printMessage(TAG, "model hotel id : " + model.getHotelId());

        airportTable.put(HOTEL_CITY_ID, model.getCityId());
        airportTable.put(HOTEL_CITY_NAME, model.getCityName());
        airportTable.put(HOTEL_COUNTRY_NAME, model.getCountryName());
        airportTable.put(HOTEL_SEARCH_TYPE, model.getType());
        airportTable.put(HOTEL_LANGUAGE, keyboardLang);
        airportTable.put(HOTEL_NAME_ID, model.getHotelId());
        db.insert(RECENT_HOTELS_TABLE_NAME, null, airportTable);
        db.close();
    }

    public ArrayList<CityModel> getRecentHotelsList(String selectedLang) {
        ArrayList<CityModel> recentList = new ArrayList<>();
        String selectedQuery = "select * from " + RECENT_HOTELS_TABLE_NAME + " WHERE " + HOTEL_LANGUAGE + " = '" + selectedLang + "'" + " group by " + HOTEL_CITY_NAME + " order by " + HOTEL_CITY_ID + " desc limit 0,10";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectedQuery, null);
        if (cursor.moveToFirst()) {
            do {
                CityModel recentItem = new CityModel();
                recentItem.setHotelId("" + cursor.getInt(6));
                recentItem.setCityId(cursor.getString(1));
                recentItem.setCityName(cursor.getString(2));
                recentItem.setCountryName(cursor.getString(3));
                recentItem.setType(cursor.getString(4));
                recentItem.setHotelId(cursor.getString(6));
                recentList.add(recentItem);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return recentList;
    }

    public boolean addAirMealList(List<AirMealList> airMealList) {
        boolean isAccess = false;
        try {
            String sql = "INSERT INTO " + AIR_MEAL_TABLE_NAME + " VALUES (?, ?, ?);";
            SQLiteDatabase db = this.getWritableDatabase();
            SQLiteStatement statement = db.compileStatement(sql);
            deleteAirMealList();
            db.beginTransaction();
            for (int i = 0; i < airMealList.size(); i++) {
                AirMealList item = airMealList.get(i);
                statement.clearBindings();
                statement.bindString(1, item.getAirMealCode());
                statement.bindString(2, item.getAirMealName());
                statement.bindString(3, item.getLanguage());
                statement.execute();
            }
            db.setTransactionSuccessful();
            db.endTransaction();

            isAccess = true;
        } catch (Exception e) {
            e.printStackTrace();
            isAccess = false;
            Utils.printMessage(TAG, "Exception occured.." + e.getMessage());
        }
        return isAccess;
    }

    public void deleteAirMealList() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + AIR_MEAL_TABLE_NAME);
    }

    public ArrayList<AirMealList> getAirMealList() {
        ArrayList<AirMealList> airMealList = new ArrayList<AirMealList>();
        String selectedLang = Constants.LANGUAGE_ENGLISH_CODE;
        if (Utils.isArabicLangSelected(context)) {
            selectedLang = Constants.LANGUAGE_ARABIC_CODE;
        }
        String selectedQuery = "select air_meal_name from airMealData where (`language` = '" + selectedLang + "')";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectedQuery, null);
        if (cursor.moveToFirst()) {
            do {
                AirMealList mealList = new AirMealList();
                mealList.setAirMealName(cursor.getString(0));
                airMealList.add(mealList);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return airMealList;
    }

    public boolean addCountryNamesList(List<CountryNamesList> countryNamesList) {
        boolean isAdded = false;
        try {
            String sql = "INSERT INTO " + COUNTRY_NAMES_TABLE_NAME + " VALUES (?, ?, ?, ?, ?);";
            SQLiteDatabase db = this.getWritableDatabase();
            SQLiteStatement statement = db.compileStatement(sql);
            deleteCountryNamesList();
            db.beginTransaction();
            for (int i = 0; i < countryNamesList.size(); i++) {
                CountryNamesList item = countryNamesList.get(i);
                statement.clearBindings();
                statement.bindString(1, item.getCountryId());
                statement.bindString(2, item.getCountryName());
                statement.bindString(3, item.getCountryNameInArab());
                statement.bindString(4, item.getCountryShortCode());
                statement.bindString(5, item.getCountryCode());
                statement.execute();
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            isAdded = true;
        } catch (Exception e) {
            e.printStackTrace();
            isAdded = false;
            Utils.printMessage(TAG, "Exception occured.." + e.getMessage());
        }
        return isAdded;
    }

    public void deleteCountryNamesList() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + COUNTRY_NAMES_TABLE_NAME);

    }

    public ArrayList<CountryNamesList> getCountryNameList() {
        ArrayList<CountryNamesList> countryNameList = new ArrayList<CountryNamesList>();
        String selectedQuery = "";
        selectedQuery = "select country_name from countryNames";
        if (Utils.isArabicLangSelected(context)) {
            selectedQuery = "select country_name_arab from countryNames";
        }
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectedQuery, null);
        if (cursor.moveToFirst()) {
            do {
                CountryNamesList countryList = new CountryNamesList();
                countryList.setCountryName(cursor.getString(0));
                countryNameList.add(countryList);
            } while (cursor.moveToNext());
        }
        if (countryNameList.size() > 0) {
            Collections.sort(countryNameList, new Comparator<CountryNamesList>() {
                @Override
                public int compare(CountryNamesList s1, CountryNamesList s2) {
                    return s1.getCountryName().compareToIgnoreCase(s2.getCountryName());
                }
            });
        }
        cursor.close();
        db.close();
        return countryNameList;
    }

    public String getCountryName(String airportCode, String language) {
        String countryName = "";
        String countQuery = "SELECT  " + COUNTRY_NAME + " FROM " + TABLE_NAME + " where "
                + AIRPORT_CODE + "='" + airportCode + "' and " + LANGUAGE + " = '" + language + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor.moveToFirst()) {
            countryName = cursor.getString(0);
        }
        cursor.close();
        db.close();
        return countryName;
    }

    public String getAirMealCode(String airMeal, String language) {
        String airMealName = "";
        String countQuery = "SELECT  " + AIR_MEAL_CODE + " FROM " + AIR_MEAL_TABLE_NAME + " where "
                + AIR_MEAL_NAME + "='" + airMeal + "' and " + LANGUAGE + " = '" + language + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor.moveToFirst()) {
            airMealName = cursor.getString(0);
        }
        cursor.close();
        db.close();
        return airMealName;
    }

    public String getCountryCode(String countryName) {
        String passportCountryName = "";
        String countQuery = "";
        countQuery = "SELECT  " + COUNTRY_SHORT_CODE + " FROM " + COUNTRY_NAMES_TABLE_NAME + " where " + COUNTRY_NAME + "='" + countryName + "'";
        if (Utils.isArabicLangSelected(context)) {
            countQuery = "SELECT  " + COUNTRY_SHORT_CODE + " FROM " + COUNTRY_NAMES_TABLE_NAME + " where " + COUNTRY_NAME_ARAB + "='" + countryName + "'";
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor.moveToFirst()) {
            passportCountryName = cursor.getString(0);
        }
        cursor.close();
        db.close();
        return passportCountryName;
    }

    public String getCountryNameText(String countryName) {
        String passportCountryName = "";
        String countQuery = "";
        countQuery = "SELECT  " + COUNTRY_NAME + " FROM " + COUNTRY_NAMES_TABLE_NAME + " where " + COUNTRY_SHORT_CODE + "='" + countryName + "'";
        if (Utils.isArabicLangSelected(context)) {
            countQuery = "SELECT  " + COUNTRY_NAME_ARAB + " FROM " + COUNTRY_NAMES_TABLE_NAME + " where " + COUNTRY_SHORT_CODE + "='" + countryName + "'";
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor.moveToFirst()) {
            passportCountryName = cursor.getString(0);
        }
        cursor.close();
        db.close();
        return passportCountryName;
    }

    public String getCountryNameInEnglish(String countryName) {
        String englishCountryName = "";
        String countQuery = "";
        if (Utils.isArabicLangSelected(context)) {
            countQuery = "SELECT  " + COUNTRY_NAME + " FROM " + COUNTRY_NAMES_TABLE_NAME + " where " + COUNTRY_NAME_ARAB + "='" + countryName + "'";
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor.moveToFirst()) {
            englishCountryName = cursor.getString(0);
        }
        cursor.close();
        db.close();
        return englishCountryName;
    }

    public String getArabCountryNames(String countryName) {
        String selectedCountryName = "";
        String countQuery = "";
        if (Utils.isArabicLangSelected(context)) {
            countQuery = "SELECT  " + COUNTRY_NAME_ARAB + " FROM " + COUNTRY_NAMES_TABLE_NAME + " where " + COUNTRY_NAME + "='" + countryName + "'";
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor.moveToFirst()) {
            selectedCountryName = cursor.getString(0);
        }
        cursor.close();
        db.close();
        return selectedCountryName;
    }

    public int getAirportListCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count;
    }

    public void getDatabase() {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();
            if (sd.canWrite()) {
                String currentDBPath = "/data/data/com.flyin.bookings/databases/AirportsList.db";
                String backupDBPath = "backupname.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);
                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<PredectiveSearchModel> getRecentAirportsListPredict() {
        ArrayList<PredectiveSearchModel> recentList = new ArrayList<PredectiveSearchModel>();
        String selectedQuery = "select * from " + RECENT_TABLE_NAME + " group by airport_code order by airport_id desc limit 0,10"; //"SELECT  * FROM " + RECENT_TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectedQuery, null);
        if (cursor.moveToFirst()) {
            do {
                PredectiveSearchModel recentItem = new PredectiveSearchModel();
                recentItem.setAirportCode(cursor.getString(1));
                recentItem.setAirportName(cursor.getString(2));
                recentItem.setCityName(cursor.getString(3));
                recentItem.setCountryName(cursor.getString(5));
                recentList.add(recentItem);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return recentList;
    }
}