package com.flyin.bookings.util;

import android.annotation.SuppressLint;

import com.flyin.bookings.model.AmenitiesBean;

import java.util.HashMap;

public class AmenitiesConstants {
    public static final HashMap<String, AmenitiesBean> wifiHashMap = new HashMap<String, AmenitiesBean>() {
        {
            put("Yes", new AmenitiesBean("Wi-Fi", "واي فاي", "wifi"));
            put("Charge", new AmenitiesBean("Charge for Wi-Fi", "برسوم إضافية", "wifi"));
            put("Chance of Wifi", new AmenitiesBean("Chance of Wi-Fi", "فرصة واي فاي", "wifi"));
        }
    };

    public static final HashMap<String, AmenitiesBean> avodVideoHashMap = new HashMap<String, AmenitiesBean>() {
        {
            put("On-Demand TV", new AmenitiesBean("On-Demand TV", "قنوات عند الطلب", "avod"));
            put("Seatback TV", new AmenitiesBean("Seatback TV", "شاشة خلف المقعد", "avod"));
            put("Overhead TV", new AmenitiesBean("Overhead TV", "شاشة علوية", "avod"));
            put("Portable Device", new AmenitiesBean("Portable Device", "جهاز منتقل", "avod"));
            put("Shared Screen", new AmenitiesBean("Shared Screen", "شاشة مشتركة", "avod"));
        }
    };
    public static final HashMap<String, AmenitiesBean> avodChargeVideoHashMap = new HashMap<String, AmenitiesBean>() {
        {
            put("On-Demand TV", new AmenitiesBean("On-Demand TV (Charged)", "قنوات عند الطلب (برسوم إضافية)", "avod"));
            put("Seatback TV", new AmenitiesBean("Seatback TV (Charged)", "شاشة خلف المقعد (برسوم إضافية)", "avod"));
            put("Overhead TV", new AmenitiesBean("Overhead TV (Charged)", "شاشة علوية (برسوم إضافية)", "avod"));
            put("Portable Device", new AmenitiesBean("Portable Device (Charged)", "جهاز منتقل (برسوم إضافية)", "avod"));
            put("Shared Screen", new AmenitiesBean("Shared Screen (Charged)", "شاشة مشتركة (برسوم إضافية)", "avod"));
        }
    };
    public static final HashMap<String, AmenitiesBean> powerTypeHashMap = new HashMap<String, AmenitiesBean>() {
        {
            put("Power", new AmenitiesBean("Power", "منفذ كهربائي", "power_plug"));
            put("USB", new AmenitiesBean("USB", "يو اس بي", "usb"));
            put("Power & USB", new AmenitiesBean("Power & USB", "منفذ كهربائي& يو اس بي", "usb_and_plug"));
        }
    };
    public static final HashMap<String, AmenitiesBean> mealsHashMap = new HashMap<String, AmenitiesBean>() {
        {
            put("Yes", new AmenitiesBean("Fresh Meals", "وجبات طازجة", "fresh_meal"));
            put("Charge", new AmenitiesBean("Fresh Meals with Charge", "وجبات طازجة برسوم اضافية", "fresh_meal"));
        }
    };
    public static final HashMap<String, AmenitiesBean> infantCareHashMap = new HashMap<String, AmenitiesBean>() {
        {
            put("Yes", new AmenitiesBean("Infant Care", "رعاية الرضع", "infant"));
            put("Charge", new AmenitiesBean("Infant Care with Charge", "رعاية الرضع  (برسوم إضافية)", "infant"));
        }
    };
    public static final HashMap<String, AmenitiesBean> alocoholHashMap = new HashMap<String, AmenitiesBean>() {
        {
            put("Yes", new AmenitiesBean("Beverages", "المشروبات", "beverages"));
            put("Charge", new AmenitiesBean("Beverages With Charge", "مشروبات بقيمة إضافية", "beverages"));
        }
    };
    public static final HashMap<String, AmenitiesBean> mobileHashMap = new HashMap<String, AmenitiesBean>() {
        {
            put("Yes", new AmenitiesBean("Mobile", "موبايل", "mobile_service"));
        }
    };
    public static final HashMap<String, AmenitiesBean> prayerAreaHashMap = new HashMap<String, AmenitiesBean>() {
        {
            put("Yes", new AmenitiesBean("Prayer Area", "مصلى", "masjid"));
        }
    };
    public static final HashMap<String, AmenitiesBean> showerAreaHashMap = new HashMap<String, AmenitiesBean>() {
        {
            put("Yes", new AmenitiesBean("Shower", "منطقة إستحمام", "shower"));
        }
    };
    public static final HashMap<String, AmenitiesBean> seatTypeHashMap = new HashMap<String, AmenitiesBean>() {
        {
            put("Standard", new AmenitiesBean("Standard", "قياسي عادي", "seat_standard"));
            put("Roomier", new AmenitiesBean("Roomier", "قياسي بمساحة للقدمين", "seat_roomier"));
            put("Erosiz", new AmenitiesBean("Erosiz", "قياسي ممتاز", "seat_tight"));
            put("Flat bed seat", new AmenitiesBean("Flat bed seat", "مقعد يتحول إلى سرير", "seat_eight"));
            put("Recliner", new AmenitiesBean("Recliner", "مقعد ينحني بصفة شبه كاملة", "seat_five"));
            put("Angle flat", new AmenitiesBean("Angle flat", "مقعد يتمدد بصفة كاملة", "seat_seven"));
            put("Cradle sleeper", new AmenitiesBean("Cradle sleeper", "مقعد قياسي متمدد", "seat_six"));
            put("Full flat pod", new AmenitiesBean("Full flat pod", "مقعد متمدد ذات خصوصية", "seat_nine"));
            put("Full flat", new AmenitiesBean("Full flat", "مقعد يتمدد بشكل كامل", "seat_eight"));
            put("Full bed", new AmenitiesBean("Full bed", "مقعد يتحول إلى سرير متمدد", "seat_eight"));
            put("Full flat bed", new AmenitiesBean("Full flat bed", "مقعد يتحول إلى سرير كامل", "seat_nine"));
            put("open suit with full flat bed", new AmenitiesBean("Open suite with full flat bed", "جناح مفتوح مع مقعد يتحول إلى سرير كامل", "seat_eight"));
            put("closed suit with full flat bed", new AmenitiesBean("Closed suite with full flat bed", "جناح مغلق مع مقعد يتحول إلى سرير كامل", "seat_nine"));
            put("Super First Class suites", new AmenitiesBean("Super First Class suites", "جناح مغلق كبير و فاخر مع مقعد يتحول إلى سرير كامل", "seat_nine"));
            put("Seat + bed suites", new AmenitiesBean("Seat + bed suites", "جناح مغلق فاخر مع صالة و غرفة نوم و دورة مياة خاصة", "seat_nine"));
        }
    };
    @SuppressLint("UseSparseArrays")
    public static final HashMap<Integer, AmenitiesBean> bodyTypeHashMap = new HashMap<Integer, AmenitiesBean>() {
        {
            put(1, new AmenitiesBean("Double Deck Wide Body Aircraft With Two Aisles", "مقصورة الطائرة واسعة بطابقين ذات ممرين", "airline_body"));
            put(2, new AmenitiesBean("Wide Body Aircraft With Two Aisles", "مقصورة الطائرة واسعة ذات ممرين", "airline_body"));
            put(3, new AmenitiesBean("Narrow Body Aircraft With Single Aisle", "مقصورة الطائرة صغيرة ذات ممر واحد", "airline_body"));
            put(4, new AmenitiesBean("Turboprops", "Turboprops", "airline_body"));
            put(5, new AmenitiesBean("Regional Jets", "Regional Jets", "airline_body"));
        }
    };
}
