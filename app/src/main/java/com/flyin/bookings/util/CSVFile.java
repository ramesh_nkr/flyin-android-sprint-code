package com.flyin.bookings.util;

import com.flyin.bookings.model.AirMealList;
import com.flyin.bookings.model.AirlineList;
import com.flyin.bookings.model.CountryNamesList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVFile {
    InputStream inputStream;
    private static final String TAG = "CSVFile";

    public CSVFile(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public List add() {
        List<AirlineList> airlineList = new ArrayList<AirlineList>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                if (csvLine.isEmpty()) {
                    break;
                }
                String[] row = csvLine.split(",");
                AirlineList value = new AirlineList();
                value.setAirlineCode(row[0]);
                value.setAirlineName(row[1]);
                value.setLanguage(row[2]);
                airlineList.add(value);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return airlineList;
    }

    public List access() {
        List<CountryNamesList> countryNamesList = new ArrayList<CountryNamesList>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                if (csvLine.isEmpty()) {
                    break;
                }
                String[] row = csvLine.split(",");
                CountryNamesList value = new CountryNamesList();
                value.setCountryId(row[0]);
                value.setCountryName(row[1]);
                value.setCountryNameInArab(row[2]);
                value.setCountryShortCode(row[3]);
                value.setCountryCode(row[4]);
                countryNamesList.add(value);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return countryNamesList;
    }

    public List load() {
        List<AirMealList> airMealList = new ArrayList<AirMealList>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                if (csvLine.isEmpty()) {
                    break;
                }
                String[] row = csvLine.split(",");
                AirMealList value = new AirMealList();
                value.setAirMealCode(row[0]);
                value.setAirMealName(row[1]);
                value.setLanguage(row[2]);
                airMealList.add(value);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return airMealList;
    }

    public ArrayList<String> get_frequentnameList(ArrayList<String> airlinecodeList, boolean isArabicLang) {
        ArrayList<String> ffpArrayList = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                String[] row = csvLine.split(",");
                String airline_code = row[0];
                for (int i = 0; i < airlinecodeList.size(); i++) {
                    if (airline_code.equals(airlinecodeList.get(i))) {
                        if (isArabicLang) {
                            for (int j = 2; j < 3; j++) {
                                if (row[j] != "") {
                                    ffpArrayList.add(row[j]);
                                }
                            }
                        } else {
                            for (int j = 1; j < 2; j++) {
                                if (row[j] != "") {
                                    ffpArrayList.add(row[j]);
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ffpArrayList;
    }

    public String getMealType(String selectedMealName, String language) {
        String mealType = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                String[] row = csvLine.split(",");
                String englishMealName = row[0];
                String arabicMealName = row[1];
                boolean state = Boolean.parseBoolean(row[2]);
                if (englishMealName.equalsIgnoreCase(selectedMealName)) {
                    if (language.equalsIgnoreCase(Constants.LANGUAGE_ARABIC_CODE)) {
                        mealType = arabicMealName;
                    } else {
                        mealType = englishMealName;
                    }
                    break;
                }
            }
        } catch (IOException ex) {

        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {

            }
        }
        return mealType;
    }

    public String getAircraftName(String selectedAircraft) {
        String aircraft = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                String[] row = csvLine.split(",");
                String aircraftNumber = row[0];
                String aircraftName = row[1];
                if (aircraftNumber.equalsIgnoreCase(selectedAircraft)) {
                    aircraft = aircraftName;
                    break;
                }
            }
        } catch (IOException ex) {

        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {

            }
        }
        return aircraft;
    }

    public String getCountryFlagName(String countryCode) {
        String resultCountryCode = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                String[] row = csvLine.split(",");
                String matchedCountryCode = row[3];
                if (matchedCountryCode.equalsIgnoreCase(countryCode.toLowerCase())) {
                    resultCountryCode = row[1];
                    break;
                }
            }
        } catch (IOException ex) {
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }
        return resultCountryCode;
    }

    public String getFlightAirMeal(String selectedMealName, String selectedLanguage) {
        String mealType = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                if (csvLine.isEmpty()) {
                    break;
                }
                String[] row = csvLine.split(",");
                String mealCode = row[0];
                String mealName = row[1];
                String language = row[2];
                if (selectedLanguage.equalsIgnoreCase(language)) {
                    if (mealName.equalsIgnoreCase(selectedMealName)) {
                        mealType = mealCode;
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mealType;
    }
}