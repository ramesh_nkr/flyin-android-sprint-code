package com.flyin.bookings.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.flyin.bookings.listeners.AirlinesInterface;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AirportData implements AsyncTaskListener {
    private HashMap<String, FlightAirportName> airportNamesMap = new HashMap<>();
    AirlinesInterface<HashMap<String, FlightAirportName>> arrayListAirlinesInterface;

    @Override
    public void onTaskComplete(String data, int serviceType) {
        if (serviceType == 5) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(data);
                String status = obj.getString("status");
                if (status.equalsIgnoreCase("SUCCESS")) {
                    airportNamesMap.clear();
                    if (obj.has("data")) {
                        JSONArray dataArr = obj.getJSONArray("data");
                        for (int i = 0; i < dataArr.length(); i++) {
                            FlightAirportName flightAirportName = new FlightAirportName();
                            flightAirportName.setCityCode(dataArr.getJSONObject(i).getString("cc"));
                            flightAirportName.setCityName(Utils.decodeUnicode(dataArr.getJSONObject(i).getString("cn").trim()));
                            flightAirportName.setAirpotName(Utils.decodeUnicode(dataArr.getJSONObject(i).getString("an").trim()));
                            airportNamesMap.put(dataArr.getJSONObject(i).getString("iata"), flightAirportName);
                        }
                        arrayListAirlinesInterface.onSuccess(airportNamesMap);
                    }
                } else {
                    arrayListAirlinesInterface.onFailed("Failure");
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
    }

    private void onServiceCall(ArrayList<String> arrayList, Context context,
                               AirlinesInterface<HashMap<String, FlightAirportName>> arrayListAirlinesInterface) {
        String json = makeJsonAirport(arrayList, context);
        HTTPAsync async = new HTTPAsync(context, AirportData.this, Constants.AIRPORT_SEARCH_URL,
                "", json, 5, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String makeJsonAirport(ArrayList<String> airportArrayList, Context contex) {
        final SharedPreferences preferences = contex.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String language = preferences.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
        JSONObject mainJSON = new JSONObject();
        try {
            JSONArray mDataArray = new JSONArray();
            for (int i = 0; i < airportArrayList.size(); i++) {
                boolean isContains = false;
                for (int j = 0; j < mDataArray.length(); j++) {
                    if (mDataArray.get(j).toString().toLowerCase().equalsIgnoreCase(airportArrayList.get(i).toLowerCase())) {
                        isContains = true;
                        break;
                    }
                }
                if (!isContains) {
                    mDataArray.put(airportArrayList.get(i));
                }
            }
            mainJSON.put("ac", mDataArray);
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
            mainJSON.put("lang", language);
        } catch (Exception e) {
        }
        return mainJSON.toString();
    }

    public void resultData(Context context, ArrayList<String> array,
                           AirlinesInterface<HashMap<String, FlightAirportName>> arrayListAirlinesInterface) {
        try {
            onServiceCall(array, context, arrayListAirlinesInterface);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.arrayListAirlinesInterface = arrayListAirlinesInterface;
    }
}
