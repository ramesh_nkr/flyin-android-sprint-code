package com.flyin.bookings.util;

import android.content.Context;

public class Country {
    private String mName;
    private String mCountryISO;
    private int mCountryCode;
    private String mCountryCodeStr;
    private int mPriority;
    private int mResId;
    private boolean isSelected;

    public Country(Context context, String str, String selectedValue, boolean isPhoneNumberClicked) {
        String[] data = str.split(",");
        //Utils.printMessage("Country", "str data is :: "+str);
        mCountryISO = data[2];
        mCountryCode = Integer.parseInt(data[3]);
        DbHandler dbHandler = new DbHandler(context);
        if (Utils.isArabicLangSelected(context)) {
            mName = dbHandler.getArabCountryNames(data[0]);
        } else {
            mName = data[0];
        }
        mCountryCodeStr = "+" + data[3];
        if (data.length > 3) {
            mPriority = Integer.parseInt(data[3]);
        }
        String fileName = data[1];
        mResId = context.getApplicationContext().getResources().getIdentifier(fileName.toLowerCase(), "drawable", context.getApplicationContext().getPackageName());
        if (!selectedValue.equalsIgnoreCase("")) {
            if (isPhoneNumberClicked) {
                if (selectedValue.equalsIgnoreCase(mCountryCodeStr)) {
                    setSelected(true);
                }
            } else {
                if (selectedValue.equalsIgnoreCase(mName)) {
                    setSelected(true);
                }
            }
        }
    }

    public String getName() {
        return mName;
    }

    public String getCountryISO() {
        return mCountryISO;
    }

    public int getCountryCode() {
        return mCountryCode;
    }

    public String getCountryCodeStr() {
        return mCountryCodeStr;
    }

    public int getPriority() {
        return mPriority;
    }

    public int getResId() {
        return mResId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
