package com.flyin.bookings.util;

import android.content.Context;

import com.flyin.bookings.listeners.AirlinesInterface;
import com.flyin.bookings.model.PredectiveSearchModel;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;

import org.json.JSONObject;

import java.util.ArrayList;

public class LocationSearchData implements AsyncTaskListener {
    AirlinesInterface<ArrayList<PredectiveSearchModel>> arrayListAirlinesInterface;
    ArrayList<PredectiveSearchModel> predectiveSearchList = new ArrayList<>();

    @Override
    public void onTaskComplete(String data, int serviceType) {
        if (serviceType == 5) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(data);
                String status = obj.getString("status");
                if (status.equalsIgnoreCase("SUCCESS")) {
                    predectiveSearchList.clear();
                    if (obj.has("data")) {
                        JSONObject object = new JSONObject(obj.get("data").toString());
                        PredectiveSearchModel predectiveSearchModel = new PredectiveSearchModel();
                        predectiveSearchModel.setAirportCode(object.getString("IATACode"));
                        predectiveSearchModel.setAirportName(Utils.convert(object.getString("airportName")));
                        predectiveSearchModel.setCityId(object.getInt("cityId"));
                        predectiveSearchModel.setCityName(Utils.convert(object.getString("cityName")));
                        predectiveSearchModel.setCountryCode(object.getString("countryCode"));
                        predectiveSearchModel.setCountryName(Utils.convert(object.getString("countryName")));
                        predectiveSearchList.add(predectiveSearchModel);
                    }
                    if (predectiveSearchList.size() > 0) {
                        arrayListAirlinesInterface.onSuccess(predectiveSearchList);
                    }
                } else {
                    arrayListAirlinesInterface.onFailed("");
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
    }

    private void onServiceCall(String json, Context context) {
        HTTPAsync async = new HTTPAsync(context, LocationSearchData.this, Constants.GEOLOCATION_SEARCH_URL,
                "", json, 5, HTTPAsync.METHOD_POST);
        async.execute();
    }


    public void resultData(Context context, String json, AirlinesInterface<ArrayList<PredectiveSearchModel>> arrayListAirlinesInterface) {
        try {
            onServiceCall(json, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.arrayListAirlinesInterface = arrayListAirlinesInterface;
    }
}
