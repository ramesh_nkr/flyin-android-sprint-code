package com.flyin.bookings.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.flyin.bookings.ServerTimeService;

public class TimeChangedReceiver extends BroadcastReceiver {
    private ServerTimeService serverTimeData = new ServerTimeService();

    @Override
    public void onReceive(Context context, Intent intent) {
        serverTimeData.checkServiceCall(context);
    }
}
