package com.flyin.bookings.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.flyin.bookings.listeners.AirlinesInterface;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AirlinesData implements AsyncTaskListener, AirlinesInterface {
    HashMap<String, String> airLineNamesMap = new HashMap<String, String>();
    AirlinesInterface<HashMap<String, String>> arrayListAirlinesInterface;

    @Override
    public void onTaskComplete(String data, int serviceType) {
        if (serviceType == 5) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(data);
                String status = obj.getString("status");
                if (status.equalsIgnoreCase("SUCCESS")) {
                    airLineNamesMap.clear();
                    if (obj.has("data")) {
                        JSONArray dataArr = obj.getJSONArray("data");
                        for (int i = 0; i < dataArr.length(); i++) {
                            airLineNamesMap.put(dataArr.getJSONObject(i).getString("acode"), Utils.decodeUnicode(dataArr.getJSONObject(i).getString("an").trim()));
                        }
                        arrayListAirlinesInterface.onSuccess(airLineNamesMap);
                    }
                } else {
                    arrayListAirlinesInterface.onFailed("failue");
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
    }

    private void onServiceCall(ArrayList<String> arrayList, Context context,
                               AirlinesInterface<HashMap<String, String>> arrayListAirlinesInterface) {
        String json = makeJsonAirLine(arrayList, context);
        HTTPAsync async = new HTTPAsync(context, AirlinesData.this, Constants.AIRLINE_SEARCH_URL,
                "", json, 5, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String makeJsonAirLine(ArrayList<String> arrayList, Context context) {
        final SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String language = preferences.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
        JSONObject mainJSON = new JSONObject();
        try {
            JSONArray mDataArray = new JSONArray();
            for (int i = 0; i < arrayList.size(); i++) {
                boolean isContains = false;
                for (int j = 0; j < mDataArray.length(); j++) {
                    if (mDataArray.get(j).toString().equalsIgnoreCase(arrayList.get(i))) {
                        isContains = true;
                        break;
                    }
                }
                if (!isContains) {
                    mDataArray.put(arrayList.get(i));
                }
            }
            mainJSON.put("ac", mDataArray);
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
            mainJSON.put("lang", language);
        } catch (Exception e) {
        }
        return mainJSON.toString();
    }

    public void resultData(Context context, ArrayList<String> array, AirlinesInterface<HashMap<String, String>> arrayListAirlinesInterface) {
        try {
            onServiceCall(array, context, arrayListAirlinesInterface);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.arrayListAirlinesInterface = arrayListAirlinesInterface;
    }

    @Override
    public void onSuccess(Object response) {
    }

    @Override
    public void onFailed(String message) {
    }
}
