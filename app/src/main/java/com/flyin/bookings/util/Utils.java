package com.flyin.bookings.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.flyin.bookings.R;
import com.flyin.bookings.model.CurrencyBean;
import com.flyin.bookings.model.FilterAirlineSelectionItem;
import com.flyin.bookings.model.FilterStopSelectionItem;
import com.flyin.bookings.model.OriginDestinationOptionBean;
import com.flyin.bookings.model.PricedItineraryObject;
import com.flyin.bookings.model.Singleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.MODE_PRIVATE;

@SuppressWarnings("ALL")
public class Utils {
    private static final String TAG = "Utils";
    private static final String HASH_ALGORITHM = "HmacSHA256";
    private static ArrayList<CurrencyBean> currencyList;

    public static SpannableStringBuilder getMandatoryFieldText(String inputText) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        String colored = " *";
        builder.append(inputText);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return builder;
    }

    public static SpannableStringBuilder getMandatoryContactFormFieldText(String inputText) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        String colored = " *";
        builder.append(inputText);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.GRAY), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return builder;
    }

    public static Spannable getMandatoryField() {
        Spannable symbol = new SpannableString(" *");
        symbol.setSpan(new ForegroundColorSpan(Color.RED), 0, symbol.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return symbol;
    }

    public static Spannable addTitleField(String inputText) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(Color.GRAY), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addDurationField(String inputText) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(Color.BLACK), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addCountryCodeField(String inputText) {
        Spannable textField = new SpannableString(" (" + inputText + ") ");
        textField.setSpan(new ForegroundColorSpan(Color.GRAY), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addFlightAmenitiesField(String inputText, Activity activity) {
        Spannable textField = new SpannableString(" (" + inputText + ")  ");
        textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.merchandise_data_color)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addRoomNameField(String inputText, Activity activity) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.hotel_room_name_color)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addBookingReferenceField(String inputText, Activity activity) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.hotel_booking_reference_color)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addTripDetailsField(String inputText, Context context) {
        Spannable textField = new SpannableString("( " + context.getResources().getString(R.string.label_based_on) + " " + inputText + " " + context.getResources().getString(R.string.label_review) + " )");
        textField.setSpan(new ForegroundColorSpan(Color.BLACK), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable adduserTripAdField(String inputText, Context context) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(Color.GRAY), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addFlightTextField(String inputText, Activity activity) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.flight_name_color)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addFlightHoverTextField(String inputText, Activity activity) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.multi_header_label_color)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addRefundTextField(String inputText, Activity activity) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.refund_text_color)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addNonRefundTextField(String inputText, Activity activity) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.non_refund_color)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addRewardsEnrollField(String inputText, Activity activity) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.loyalty_enroll_terms_conditions)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textField.setSpan(new UnderlineSpan(), 0, textField.length(), 0);
        return textField;
    }

    public static String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toUpperCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toUpperCase(Locale.US);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "SA";
    }

    public static String formatDateToTime(String dateTime, Activity activity) {
        String resultStr = "";
        if (dateTime != null) {
            String[] separated = dateTime.split("T");
            if (separated.length == 2) {
                String[] timeseparated = separated[1].split(":");
                if (timeseparated.length == 3) {
                    resultStr = timeseparated[0] + ":" + timeseparated[1];
                }
            }
        }
        return resultStr;
    }

    public static String formatDateToDay(String dateTime, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        DateFormat dayFormat = new SimpleDateFormat("E", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("dd, yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getDayName(dayFormat.format(date), activity) + " " + getYearName(monthFormat.format(date), activity) + " " + targetFormat.format(date);
    }

    public static String formatDateToDate(String dateTime, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat dayFormat = new SimpleDateFormat("E", Locale.ENGLISH);
        DateFormat dayDateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMMMMM", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getDayName(dayFormat.format(date), activity) + " " + dayDateFormat.format(date) + " " + getYearName(monthFormat.format(date), activity);
    }

    public static String formatTimeStampToDate(String dateTime, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat yearFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String formattedDate = dateFormat.format(date) + " " + getYearName(monthFormat.format(date), activity) + " " + yearFormat.format(date);
        return formattedDate;
    }

    public static String formatTimeStampToPassportDate(String dateTime) {
        DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

    public static String formatTripTimeStampToDate(String dateTime, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat yearFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String formattedDate = dateFormat.format(date) + " " + getYearName(monthFormat.format(date), activity) + " " + yearFormat.format(date);
        return formattedDate;
    }

    public static String formatDurationToString(String duration, Activity activity) {
        String resultStr = "";
        try {
            String[] s = duration.split(":");
            int hrValue = Integer.parseInt(s[0]);
            int minValue = Integer.parseInt(s[1]);
            if (hrValue == 1 && minValue == 1) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.SHLbl) + " " + minValue + "" + activity.getResources().getString(R.string.SMLbl) + " ";
            } else if ((hrValue > 1 && hrValue < 11) && minValue == 1) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.PHsLbl) + " " + minValue + "" + activity.getResources().getString(R.string.SMLbl) + " ";
            } else if (hrValue > 11 && minValue == 1) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.MoreHsLbl) + " " + minValue + "" + activity.getResources().getString(R.string.SMLbl) + " ";
            } else if (hrValue == 1 && (minValue > 1 && minValue < 11)) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.SHLbl) + " " + minValue + "" + activity.getResources().getString(R.string.PMsLbl) + " ";
            } else if ((hrValue > 1 && hrValue < 11) && (minValue > 1 && minValue < 11)) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.PHsLbl) + " " + minValue + "" + activity.getResources().getString(R.string.PMsLbl) + " ";
            } else if (hrValue > 11 && (minValue > 1 && minValue < 11)) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.MoreHsLbl) + " " + minValue + "" + activity.getResources().getString(R.string.PMsLbl) + " ";
            } else if (hrValue == 1 && minValue > 11) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.SHLbl) + " " + minValue + "" + activity.getResources().getString(R.string.MoreMsLbl) + " ";
            } else if ((hrValue > 1 && hrValue < 11) && minValue > 11) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.PHsLbl) + " " + minValue + "" + activity.getResources().getString(R.string.MoreMsLbl) + " ";
            } else if (hrValue > 11 && minValue > 11) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.MoreHsLbl) + " " + minValue + "" + activity.getResources().getString(R.string.MoreMsLbl) + " ";
            } else {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.SHLbl) + " " + minValue + "" + activity.getResources().getString(R.string.SMLbl) + " ";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultStr;
    }

    public static String formatDurationToNewTimeFormat(String duration, Activity activity) {
        String resultStr = "";
        try {
            String[] s = duration.split(":");
            int hrValue = Integer.parseInt(s[0]);
            int minValue = Integer.parseInt(s[1]);
            if (hrValue == 1 && minValue == 1) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.SHLbl) + ":" + minValue + "" + activity.getResources().getString(R.string.SMLbl);
            } else if ((hrValue > 1 && hrValue < 11) && minValue == 1) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.PHsLbl) + ":" + minValue + "" + activity.getResources().getString(R.string.SMLbl);
            } else if (hrValue > 11 && minValue == 1) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.MoreHsLbl) + ":" + minValue + "" + activity.getResources().getString(R.string.SMLbl);
            } else if (hrValue == 1 && (minValue > 1 && minValue < 11)) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.SHLbl) + ":" + minValue + "" + activity.getResources().getString(R.string.PMsLbl);
            } else if ((hrValue > 1 && hrValue < 11) && (minValue > 1 && minValue < 11)) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.PHsLbl) + ":" + minValue + "" + activity.getResources().getString(R.string.PMsLbl);
            } else if (hrValue > 11 && (minValue > 1 && minValue < 11)) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.MoreHsLbl) + ":" + minValue + "" + activity.getResources().getString(R.string.PMsLbl);
            } else if (hrValue == 1 && minValue > 11) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.SHLbl) + ":" + minValue + "" + activity.getResources().getString(R.string.MoreMsLbl);
            } else if ((hrValue > 1 && hrValue < 11) && minValue > 11) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.PHsLbl) + ":" + minValue + "" + activity.getResources().getString(R.string.MoreMsLbl);
            } else if (hrValue > 11 && minValue > 11) {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.MoreHsLbl) + ":" + minValue + "" + activity.getResources().getString(R.string.MoreMsLbl);
            } else {
                resultStr = hrValue + "" + activity.getResources().getString(R.string.SHLbl) + ":" + minValue + "" + activity.getResources().getString(R.string.SMLbl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultStr;
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String formatDateToString(String date, Activity activity) {
        String dateMonth = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            Date varDate = dateFormat.parse(date);
            Date varMonth = monthFormat.parse(date);
            dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
            monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
            dateMonth = dateFormat.format(varDate) + " " + getYearName(monthFormat.format(varMonth), activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateMonth;
    }

    public static String formatDateToServerDateFormat(String date) {
        String dateMonth = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            Date varDate = dateFormat.parse(date);
            dateMonth = dateFormat.format(varDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateMonth;
    }

    public static String formatDateToMonth(String date, Activity activity) {
        String dateMonth = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", Locale.ENGLISH);
        SimpleDateFormat monthFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", Locale.ENGLISH);
        try {
            Date varDate = dateFormat.parse(date);
            Date varMonth = monthFormat.parse(date);
            dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
            monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
            dateMonth = dateFormat.format(varDate) + " " + getYearName(monthFormat.format(varMonth), activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateMonth;
    }

    //Convert timestamp to HH:MM 24 hour format
    public static int dateFormaterToMinutes(String date) {
        String timeInMin = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        try {
            Date varDate = dateFormat.parse(date);
            dateFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            timeInMin = dateFormat.format(varDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int val = 0;
        try {
            String[] timeArr = timeInMin.split(":");
            val = (Integer.parseInt(timeArr[0]) * 60) + Integer.parseInt(timeArr[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return val;
    }

    public static int dateFormatToMinutes(String date) {
        String timeInMin = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        try {
            Date varDate = dateFormat.parse(date);
            dateFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            timeInMin = dateFormat.format(varDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int val = 0;
        try {
            String[] timeArr = timeInMin.split(":");
            val = (Integer.parseInt(timeArr[0]) * 60) + Integer.parseInt(timeArr[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int timeZone = 0;
        if (val < 300) {
            timeZone = 1;
        } else if (val >= 300 && val < 720) {
            timeZone = 2;
        } else if (val >= 720 && val < 1080) {
            timeZone = 3;
        } else {
            timeZone = 4;
        }
        return timeZone;
    }

    public static Object checkStringValue(String value) {
        if (value.equalsIgnoreCase("null")) {
            return JSONObject.NULL;
        } else {
            return value;
        }
    }

    public static String checkStringVal(String value) {
        if (value.equalsIgnoreCase("null")) {
            return "";
        } else {
            return value;
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public final static boolean isValidName(String firstName) {
        if (firstName == null) {
            return false;
        } else {
            if (firstName.length() < 3) {
                return false;
            } else {
                return firstName.matches("^([a-zA-Z]+( [a-zA-Z]+)?|[-]+[a-zA-Z])*$");
            }
        }
    }
    //"^([a-zA-Z]+[.]?[ ]?|[-]+[a-zA-Z]?)*$"

    public static String getOnlyStrings(String s) {
        Pattern pattern = Pattern.compile("[^a-z A-Z]");
        Matcher matcher = pattern.matcher(s);
        String number = matcher.replaceAll("");
        return number;
    }

    public static String getNameWithoutChar(String name) {
        String resultName = name.replaceAll("[- ]", "");
        return resultName;
    }

    public final static boolean isValidName(Context context, String firstName) {
        if (firstName == null) {
            return false;
        } else {
            return firstName.matches(Utils.isArabicLangSelected(context) ? "[\u0600-\u06FF]+" : "^[a-zA-Z]+( [a-zA-Z]+)*$");
        }
    }

    public final static boolean isValidMobile(String phone) {
        if (phone == null) {
            return false;
        } else {
            if (phone.length() < Constants.MOBILE_NO_MIN_LENGTH || phone.length() > Constants.MOBILE_NO_MAX_LENGTH) {
                return false;
            } else {
                return android.util.Patterns.PHONE.matcher(phone).matches();
            }
        }
    }

    public static boolean isValidPassport(String passportNumber, int inputType) {
        if (passportNumber == null) {
            return false;
        } else {
            if (inputType == Constants.PASSPOR_NUMBER) {
                if (passportNumber.length() < Constants.PASSPORT_MIN_LENGTH || passportNumber.length() > Constants.PASSPORT_MAX_LENGTH) {
                    return false;
                }
            } else if (inputType == Constants.NATIONAL_ID) {
                if (passportNumber.length() < Constants.NATIONAL_ID_MIN_LENGTH) {
                    return false;
                }
            } else {
                if (inputType == Constants.IQAMA_MIN_LENGTH) {
                    return false;
                }
            }
            return passportNumber.matches("^[a-zA-Z0-9]*$");
        }
    }

    public static boolean isValidBookingNumber(String bookingNumber) {
        if (bookingNumber.trim() == null) {
            return false;
        } else {
            if (bookingNumber.trim().length() < 7 && bookingNumber.trim().length() > 11) {
                return false;
            } else {
                return bookingNumber.trim().matches("^[a-zA-Z0-9]*$");
            }
        }
    }

    public static String formatTripDateToTime(String dateTime, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("hh:mm", Locale.ENGLISH);
        DateFormat clockFormat = new SimpleDateFormat("a", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat yearFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        Date date = null;
        String timeOfClock = "";
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (clockFormat.format(date).equalsIgnoreCase("AM")) {
            timeOfClock = activity.getResources().getString(R.string.AMTimeLbl);
        } else if (clockFormat.format(date).equalsIgnoreCase("PM")) {
            timeOfClock = activity.getResources().getString(R.string.PMTimeLbl);
        }
        return targetFormat.format(date) + " " + timeOfClock + " " + dateFormat.format(date) + "-" + getYearName(monthFormat.format(date), activity) + "-" + yearFormat.format(date);
    }

    public static String formatTripDateToTimeFormat(String dateTime, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("hh:mm", Locale.ENGLISH);
        DateFormat timeFormat = new SimpleDateFormat("a", Locale.ENGLISH);
        Date date = null;
        String clockTime = "";
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (timeFormat.format(date).equalsIgnoreCase("AM")) {
            clockTime = activity.getResources().getString(R.string.AMTimeLbl);
        } else if (timeFormat.format(date).equalsIgnoreCase("PM")) {
            clockTime = activity.getResources().getString(R.string.PMTimeLbl);
        }
        String formattedDate = targetFormat.format(date) + " " + clockTime;
        return formattedDate.toUpperCase();
    }

    public static String formatTripDateToDayFormat(String dateTime, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", Locale.ENGLISH);
        DateFormat dayFormat = new SimpleDateFormat("E", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("dd, yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getDayName(dayFormat.format(date), activity) + " " + getYearName(monthFormat.format(date), activity) + " " + targetFormat.format(date);
    }

    public static String getRequestAccessToken(Context context) {
        String accessToken = "";
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String memberAccessToken = preferences.getString(Constants.MEMBER_ACCESS_TOKEN, "");
        if (memberAccessToken.equalsIgnoreCase("")) {
            accessToken = preferences.getString(Constants.ACCESS_TOKEN, "");
        } else {
            accessToken = preferences.getString(Constants.MEMBER_ACCESS_TOKEN, "");
        }
        return accessToken;
    }

    public static String convertPriceToUserSelection(Double currentPrice, Context context) {
        String totalPrice = "";
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyValue = pref.getString(Constants.SELECTED_CURRENCY_VALUE, "1");
        String selectedCurrencyCode = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        String bufferRateValue = pref.getString(Constants.SELECTED_CURRENCY_BUFFER_RATE, "0");
        Double bufferPrice = Double.parseDouble(selectedCurrencyValue) + ((Double.parseDouble(selectedCurrencyValue) * Double.parseDouble(bufferRateValue)) / 100);
        Double calculatedPrice = currentPrice / bufferPrice;
        int finalPrice = (int) Math.ceil(calculatedPrice);
        if (Utils.isArabicLangSelected(context)) {
            if (selectedCurrencyCode.equalsIgnoreCase("SAR")) {
                totalPrice = finalPrice + " " + context.getString(R.string.label_SAR_currency_name);
            } else {
                totalPrice = selectedCurrencyCode + " " + finalPrice;
            }
        } else {
            totalPrice = selectedCurrencyCode + " " + finalPrice;
        }
        return totalPrice;
    }

    public static String convertPriceToUserSelectedPrice(Double currentPrice, Context context) {
        String totalPrice = "";
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyValue = pref.getString(Constants.SELECTED_CURRENCY_VALUE, "1");
        String selectedCurrencyCode = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        String bufferRateValue = pref.getString(Constants.SELECTED_CURRENCY_BUFFER_RATE, "0");
        Double bufferPrice = Double.parseDouble(selectedCurrencyValue) + ((Double.parseDouble(selectedCurrencyValue) * Double.parseDouble(bufferRateValue)) / 100);
        Double calculatedPrice = currentPrice / bufferPrice;
        int finalPrice = (int) Math.ceil(calculatedPrice);
        if (Utils.isArabicLangSelected(context)) {
            if (selectedCurrencyCode.equalsIgnoreCase("SAR")) {
                totalPrice = Math.abs(finalPrice) + " " + context.getString(R.string.label_SAR_currency_name);
            } else {
                totalPrice = selectedCurrencyCode + " " + Math.abs(finalPrice);
            }
        } else {
            totalPrice = selectedCurrencyCode + " " + Math.abs(finalPrice);
        }
        return totalPrice;
    }

    public static String convertonly_PriceToUserSelectionPrice(Double currentPrice, Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyValue = pref.getString(Constants.SELECTED_CURRENCY_VALUE, "1");
        String bufferRateValue = pref.getString(Constants.SELECTED_CURRENCY_BUFFER_RATE, "0");
        Double bufferPrice = Double.parseDouble(selectedCurrencyValue) + ((Double.parseDouble(selectedCurrencyValue) * Double.parseDouble(bufferRateValue)) / 100);
        Double calculatedPrice = currentPrice / bufferPrice;
        return String.format(Locale.ENGLISH, "%.2f", calculatedPrice);
    }

    public static String convertDecimalPriceToUserPrice(Double currentPrice, Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyValue = pref.getString(Constants.SELECTED_CURRENCY_VALUE, "1");
        String bufferRateValue = pref.getString(Constants.SELECTED_CURRENCY_BUFFER_RATE, "0");
        Double bufferPrice = Double.parseDouble(selectedCurrencyValue) + ((Double.parseDouble(selectedCurrencyValue) * Double.parseDouble(bufferRateValue)) / 100);
        Double calculatedPrice = currentPrice / bufferPrice;
        return "" + Math.ceil(calculatedPrice);
    }

    public static String convertDecimalToUserPrice(Double currentPrice, Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyValue = pref.getString(Constants.SELECTED_CURRENCY_VALUE, "1");
        String bufferRateValue = pref.getString(Constants.SELECTED_CURRENCY_BUFFER_RATE, "0");
        Double bufferPrice = Double.parseDouble(selectedCurrencyValue) + ((Double.parseDouble(selectedCurrencyValue) * Double.parseDouble(bufferRateValue)) / 100);
        Double calculatedPrice = currentPrice / bufferPrice;
        return String.format(Locale.ENGLISH, "%.2f", calculatedPrice);
    }

    public static String multiTripFilterPriceToUserSelectionPrice(Double currentPrice, Context context) {
        String totalPrice = "";
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyValue = pref.getString(Constants.SELECTED_CURRENCY_VALUE, "1");
        String bufferRateValue = pref.getString(Constants.SELECTED_CURRENCY_BUFFER_RATE, "0");
        Double bufferPrice = Double.parseDouble(selectedCurrencyValue) + ((Double.parseDouble(selectedCurrencyValue) * Double.parseDouble(bufferRateValue)) / 100);
        Double calculatedPrice = currentPrice / bufferPrice;
        String selectedCurrencyCode = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        if (Utils.isArabicLangSelected(context)) {
            if (selectedCurrencyCode.equalsIgnoreCase("SAR")) {
                totalPrice = String.format(Locale.ENGLISH, "%.2f", calculatedPrice) + " " + context.getString(R.string.label_SAR_currency_name);
            } else {
                totalPrice = selectedCurrencyCode + " " + String.format(Locale.ENGLISH, "%.2f", calculatedPrice);
            }
        } else {
            totalPrice = selectedCurrencyCode + " " + String.format(Locale.ENGLISH, "%.2f", calculatedPrice);
        }
        return totalPrice;
    }

    public static String convertPriceToUserSelectionHotel(Double xmlPrice, String xmlCCode, Context context) {
        String totalPrice = "";
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyCode = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        if (currencyList == null) {
            String data = pref.getString(Constants.ALL_CURRENCY, "");
            Type listType = new TypeToken<ArrayList<CurrencyBean>>() {
            }.getType();
            currencyList = new Gson().fromJson(data, listType);
        }

        if (currencyList == null) {
            return selectedCurrencyCode + " " + xmlCCode;
        }

        CurrencyBean bean = new CurrencyBean();
        bean.setCurrency(selectedCurrencyCode);
        CurrencyBean model = currencyList.get(currencyList.indexOf(bean));
        String selectedExchangeRate = model.getExchangeRate();
        String selectedCBufferRateValue = model.getBufferRate();
        bean = new CurrencyBean();
        bean.setCurrency(xmlCCode);
        CurrencyBean xmlModel = currencyList.get(currencyList.indexOf(bean));
        String xmlBufferRate = xmlModel.getBufferRate();
        String xmlExchangeRate = xmlModel.getExchangeRate();
        Double finalPrice = 0.0;
        if (xmlCCode.equals(selectedCurrencyCode)) {
            finalPrice = xmlPrice;
        } else if (!selectedCurrencyCode.equals("SAR") && xmlCCode.equals("SAR")) {
            if (xmlBufferRate.equals("0")) {
                finalPrice = xmlPrice * (1 / Double.parseDouble(selectedCBufferRateValue));
            } else {
                Double finalROE = ((Double.parseDouble(selectedCBufferRateValue) / 100) * Double.parseDouble(selectedExchangeRate)) + Double.parseDouble(selectedExchangeRate);
                finalPrice = xmlPrice / finalROE;
            }
        } else if (selectedCurrencyCode.equals("SAR") && !xmlCCode.equals("SAR")) {
            if (xmlBufferRate.equals("0")) {
                finalPrice = xmlPrice * Double.parseDouble(xmlExchangeRate);
            } else {
                Double PRICE = ((Double.parseDouble(xmlBufferRate) / 100) * Double.parseDouble(xmlExchangeRate)) + Double.parseDouble(xmlExchangeRate);
                finalPrice = xmlPrice * PRICE;
            }
        } else if (!selectedCurrencyCode.equals("SAR") && !xmlCCode.equals("SAR")) {
            double tempPrice = 0;
            double SARPrice = 0;
            if (xmlBufferRate.equals("0")) {
                SARPrice = xmlPrice * Double.parseDouble(xmlExchangeRate);
            } else {
                tempPrice = ((Double.parseDouble(xmlBufferRate) / 100) * Double.parseDouble(xmlExchangeRate)) + Double.parseDouble(xmlExchangeRate);
                SARPrice = xmlPrice * tempPrice;
            }
            if (selectedCBufferRateValue.equals("0")) {
                finalPrice = SARPrice * (1 / Double.parseDouble(selectedCBufferRateValue));
            } else {
                Double finalROE = ((Double.parseDouble(selectedCBufferRateValue) / 100) * Double.parseDouble(selectedExchangeRate)) + Double.parseDouble(selectedExchangeRate);
                finalPrice = SARPrice / finalROE;
            }
        }
        if (Utils.isArabicLangSelected(context)) {
            if (selectedCurrencyCode.equalsIgnoreCase("SAR")) {
                totalPrice = (int) Math.round(finalPrice) + " " + context.getString(R.string.label_SAR_currency_name);
            } else {
                totalPrice = selectedCurrencyCode + " " + (int) Math.round(finalPrice);
            }
        } else {
            totalPrice = selectedCurrencyCode + " " + (int) Math.round(finalPrice);
        }
        return totalPrice;
    }

    public static Double convertPriceToUserSelectionWithDecimal(Double xmlPrice, String xmlCCode, Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyCode = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        if (currencyList == null) {
            String data = pref.getString(Constants.ALL_CURRENCY, "");
            Type listType = new TypeToken<ArrayList<CurrencyBean>>() {
            }.getType();
            currencyList = new Gson().fromJson(data, listType);
        }
        if (currencyList == null) {
            return Double.parseDouble(xmlCCode);
        }
        CurrencyBean bean = new CurrencyBean();
        bean.setCurrency(selectedCurrencyCode);
        CurrencyBean model = currencyList.get(currencyList.indexOf(bean));
        String selectedExchangeRate = model.getExchangeRate();
        String selectedCBufferRateValue = model.getBufferRate();
        bean = new CurrencyBean();
        bean.setCurrency(xmlCCode);
        CurrencyBean xmlModel = currencyList.get(currencyList.indexOf(bean));
        String xmlBufferRate = xmlModel.getBufferRate();
        String xmlExchangeRate = xmlModel.getExchangeRate();
        Double finalPrice = 0.0;
        if (xmlCCode.equals(selectedCurrencyCode)) {
            finalPrice = xmlPrice;
        } else if (!selectedCurrencyCode.equals("SAR") && xmlCCode.equals("SAR")) {
            if (xmlBufferRate.equals("0")) {
                finalPrice = xmlPrice * (1 / Double.parseDouble(selectedCBufferRateValue));
            } else {
                Double finalROE = ((Double.parseDouble(selectedCBufferRateValue) / 100) * Double.parseDouble(selectedExchangeRate)) + Double.parseDouble(selectedExchangeRate);
                finalPrice = xmlPrice / finalROE;
            }
        } else if (selectedCurrencyCode.equals("SAR") && !xmlCCode.equals("SAR")) {
            if (xmlBufferRate.equals("0")) {
                finalPrice = xmlPrice * Double.parseDouble(xmlExchangeRate);
            } else {
                Double PRICE = ((Double.parseDouble(xmlBufferRate) / 100) * Double.parseDouble(xmlExchangeRate)) + Double.parseDouble(xmlExchangeRate);
                finalPrice = xmlPrice * PRICE;
            }
        } else if (!selectedCurrencyCode.equals("SAR") && !xmlCCode.equals("SAR")) {
            double tempPrice = 0;
            double SARPrice = 0;
            if (xmlBufferRate.equals("0")) {
                SARPrice = xmlPrice * Double.parseDouble(xmlExchangeRate);
            } else {
                tempPrice = ((Double.parseDouble(xmlBufferRate) / 100) * Double.parseDouble(xmlExchangeRate)) + Double.parseDouble(xmlExchangeRate);
                SARPrice = xmlPrice * tempPrice;
            }
            if (selectedCBufferRateValue.equals("0")) {
                finalPrice = SARPrice * (1 / Double.parseDouble(selectedCBufferRateValue));
            } else {
                Double finalROE = ((Double.parseDouble(selectedCBufferRateValue) / 100) * Double.parseDouble(selectedExchangeRate)) + Double.parseDouble(selectedExchangeRate);
                finalPrice = SARPrice / finalROE;
            }
        }
        return finalPrice;
    }

    public static String convertUserSelectionCurrencyWithDecimal(Double xmlPrice, String xmlCCode, Context context) {
        String totalPrice = "";
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyCode = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        if (currencyList == null) {
            String data = pref.getString(Constants.ALL_CURRENCY, "");
            Type listType = new TypeToken<ArrayList<CurrencyBean>>() {
            }.getType();
            currencyList = new Gson().fromJson(data, listType);
        }

        if (currencyList == null) {
            return selectedCurrencyCode + " " + xmlCCode;
        }

        CurrencyBean bean = new CurrencyBean();
        bean.setCurrency(selectedCurrencyCode);
        CurrencyBean model = currencyList.get(currencyList.indexOf(bean));
        String selectedExchangeRate = model.getExchangeRate();
        String selectedCBufferRateValue = model.getBufferRate();
        bean = new CurrencyBean();
        bean.setCurrency(xmlCCode);
        CurrencyBean xmlModel = currencyList.get(currencyList.indexOf(bean));
        String xmlBufferRate = xmlModel.getBufferRate();
        String xmlExchangeRate = xmlModel.getExchangeRate();
        Double finalPrice = 0.0;
        if (xmlCCode.equals(selectedCurrencyCode)) {
            finalPrice = xmlPrice;
        } else if (!selectedCurrencyCode.equals("SAR") && xmlCCode.equals("SAR")) {
            if (xmlBufferRate.equals("0")) {
                finalPrice = xmlPrice * (1 / Double.parseDouble(selectedCBufferRateValue));
            } else {
                Double finalROE = ((Double.parseDouble(selectedCBufferRateValue) / 100) * Double.parseDouble(selectedExchangeRate)) + Double.parseDouble(selectedExchangeRate);
                finalPrice = xmlPrice / finalROE;
            }
        } else if (selectedCurrencyCode.equals("SAR") && !xmlCCode.equals("SAR")) {
            if (xmlBufferRate.equals("0")) {
                finalPrice = xmlPrice * Double.parseDouble(xmlExchangeRate);
            } else {
                Double PRICE = ((Double.parseDouble(xmlBufferRate) / 100) * Double.parseDouble(xmlExchangeRate)) + Double.parseDouble(xmlExchangeRate);
                finalPrice = xmlPrice * PRICE;
            }
        } else if (!selectedCurrencyCode.equals("SAR") && !xmlCCode.equals("SAR")) {
            double tempPrice = 0;
            double SARPrice = 0;
            if (xmlBufferRate.equals("0")) {
                SARPrice = xmlPrice * Double.parseDouble(xmlExchangeRate);
            } else {
                tempPrice = ((Double.parseDouble(xmlBufferRate) / 100) * Double.parseDouble(xmlExchangeRate)) + Double.parseDouble(xmlExchangeRate);
                SARPrice = xmlPrice * tempPrice;
            }
            if (selectedCBufferRateValue.equals("0")) {
                finalPrice = SARPrice * (1 / Double.parseDouble(selectedCBufferRateValue));
            } else {
                Double finalROE = ((Double.parseDouble(selectedCBufferRateValue) / 100) * Double.parseDouble(selectedExchangeRate)) + Double.parseDouble(selectedExchangeRate);
                finalPrice = SARPrice / finalROE;
            }
        }
        if (Utils.isArabicLangSelected(context)) {
            if (selectedCurrencyCode.equalsIgnoreCase("SAR")) {
                totalPrice = String.format(Locale.ENGLISH, "%.2f", finalPrice) + " " + context.getString(R.string.label_SAR_currency_name);
            } else {
                totalPrice = selectedCurrencyCode + " " + String.format(Locale.ENGLISH, "%.2f", finalPrice);
            }
        } else {
            totalPrice = selectedCurrencyCode + " " + String.format(Locale.ENGLISH, "%.2f", finalPrice);
        }
        return totalPrice;
    }

    public static String convertPriceToUserSelectionPrice(Double currentPrice, Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyValue = pref.getString(Constants.SELECTED_CURRENCY_VALUE, "1");
        String selectedCurrencyCode = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        String bufferRateValue = pref.getString(Constants.SELECTED_CURRENCY_BUFFER_RATE, "0");
        Double bufferPrice = Double.parseDouble(selectedCurrencyValue) + ((Double.parseDouble(selectedCurrencyValue) * Double.parseDouble(bufferRateValue)) / 100);
        Double calculatedPrice = currentPrice / bufferPrice;
        return String.format(Locale.ENGLISH, "%.2f", calculatedPrice);
    }

    public static String getSelectedCurrencyCode(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        return pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static boolean isArabicLangSelected(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedLanguage = pref.getString(Constants.USER_SELECTED_LANGUAGE, Constants.LANGUAGE_ENGLISH_CODE);
        if (selectedLanguage.equalsIgnoreCase(Constants.LANGUAGE_ARABIC_CODE)) {
            return true;
        } else {
            return false;
        }
    }

    public static String dateDifference(String startDate, String endDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Date formatted = null;
        Date converted = null;
        try {
            formatted = formatter.parse(startDate);
            converted = formatter.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long different = formatted.getTime() - converted.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        return String.valueOf(elapsedDays);
    }

    public static String flightSearchDateDifference(String startDate, String endDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date formatted = null;
        Date converted = null;
        try {
            formatted = formatter.parse(startDate);
            converted = formatter.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long different = formatted.getTime() - converted.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        return String.valueOf(elapsedDays);
    }

    public static String dayDifference(String startDate, String endDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date formatted = null;
        Date converted = null;
        try {
            formatted = formatter.parse(startDate);
            converted = formatter.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long different = formatted.getTime() - converted.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        return String.valueOf(elapsedDays);
    }

    public static String birthDateToTimeStampFormat(String dateTime) {
        DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return targetFormat.format(date) + "T00:00:00";
    }

    public static String TimeStampFormatToDateFormat(String dateTime) {
        DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return targetFormat.format(date);
    }

    public static String getDifferenceBetweenTwodaysInHrMinsFormat(String currentDate, String previousDate, Activity activity) {
        String resultTime = "";
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        Date dateCurrent = null;
        Date dateBefore = null;
        try {
            dateCurrent = simpleDateFormat.parse(currentDate);
            dateBefore = simpleDateFormat.parse(previousDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long timeDifference = dateCurrent.getTime() - dateBefore.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedHours = timeDifference / hoursInMilli;
        timeDifference = timeDifference % hoursInMilli;
        long elapsedMinutes = timeDifference / minutesInMilli;
        String hrsLbl = "";

        if (elapsedHours == 0 || elapsedHours == 1) {
            hrsLbl = elapsedHours + " " + activity.getResources().getString(R.string.label_one_hr);
        } else if (elapsedHours > 1 && elapsedHours < 11) {
            hrsLbl = elapsedHours + " " + activity.getResources().getString(R.string.label_two_ten_hr);
        } else {
            hrsLbl = elapsedHours + " " + activity.getResources().getString(R.string.label_eleven_hr);
        }
        String minsLbl = "";
        if (elapsedMinutes == 1) {
            minsLbl = elapsedMinutes + " " + activity.getResources().getString(R.string.label_one_min);
        } else if (elapsedMinutes > 1 && elapsedMinutes < 11) {
            minsLbl = elapsedMinutes + " " + activity.getResources().getString(R.string.label_two_ten_min);
        } else {
            minsLbl = elapsedMinutes + " " + activity.getResources().getString(R.string.label_eleven_min);
        }
        resultTime = hrsLbl + " " + minsLbl;
        return resultTime;
    }

    public static String getDifferenceBetweenTimeInHrMinsFormat(String currentDate, String previousDate, Activity activity) {
        String resultTime = "";
        DateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", Locale.ENGLISH);
        Date dateCurrent = null;
        Date dateBefore = null;
        try {
            dateCurrent = simpleDateFormat.parse(currentDate);
            dateBefore = simpleDateFormat.parse(previousDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long timeDifference = dateCurrent.getTime() - dateBefore.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedHours = timeDifference / hoursInMilli;
        timeDifference = timeDifference % hoursInMilli;
        long elapsedMinutes = timeDifference / minutesInMilli;
        String hrsLbl = "";
        if (elapsedHours == 0 || elapsedHours == 1) {
            hrsLbl = elapsedHours + " " + activity.getResources().getString(R.string.label_one_hr);
        } else if (elapsedHours > 1 && elapsedHours < 11) {
            hrsLbl = elapsedHours + " " + activity.getResources().getString(R.string.label_two_ten_hr);
        } else {
            hrsLbl = elapsedHours + " " + activity.getResources().getString(R.string.label_eleven_hr);
        }
        String minsLbl = "";
        if (elapsedMinutes == 1) {
            minsLbl = elapsedMinutes + " " + activity.getResources().getString(R.string.label_one_min);
        } else if (elapsedMinutes > 1 && elapsedMinutes < 11) {
            minsLbl = elapsedMinutes + " " + activity.getResources().getString(R.string.label_two_ten_min);
        } else {
            minsLbl = elapsedMinutes + " " + activity.getResources().getString(R.string.label_eleven_min);
        }
        resultTime = hrsLbl + " " + minsLbl;
        return resultTime;
    }

    public static String convertTocalendardate(String strCurrentDate, Context context) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date newDate = null;
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        format = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        String date = format.format(newDate);
        String str[] = date.split(" ");
        date = str[0] + " " + getYearName(str[1], context) + " " + str[2];
        return date;
    }

    public static String convertToCalendarHeaderFormat(String strCurrentDate, Context context) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date newDate = null;
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        format = new SimpleDateFormat("EEE dd MMM yyyy", Locale.ENGLISH);
        String date = format.format(newDate);
        String str[] = date.split(" ");
        date = getDayName(str[0], context) + " " + str[1] + " " + getYearName(str[2], context) + " " + str[3];
        return date;
    }

    public static String convertToJourneyDate(String journeyDate, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat dayFormat = new SimpleDateFormat("EEE", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(journeyDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getDayName(dayFormat.format(date), activity) + " " + dateFormat.format(date) + " " + getYearName(monthFormat.format(date), activity);
    }

    public static String getYearName(String yearString, Context context) {
        String convertedString = "";
        switch (yearString.toLowerCase()) {
            case "jan":
                convertedString = context.getString(R.string.jan_date_lbl);
                break;
            case "feb":
                convertedString = context.getString(R.string.feb_date_lbl);
                break;
            case "mar":
                convertedString = context.getString(R.string.mar_date_lbl);
                break;
            case "apr":
                convertedString = context.getString(R.string.apr_date_lbl);
                break;
            case "may":
                convertedString = context.getString(R.string.may_date_lbl);
                break;
            case "jun":
                convertedString = context.getString(R.string.jun_date_lbl);
                break;
            case "jul":
                convertedString = context.getString(R.string.jul_date_lbl);
                break;
            case "aug":
                convertedString = context.getString(R.string.aug_date_lbl);
                break;
            case "sep":
                convertedString = context.getString(R.string.sep_date_lbl);
                break;
            case "oct":
                convertedString = context.getString(R.string.oct_date_lbl);
                break;
            case "nov":
                convertedString = context.getString(R.string.nov_date_lbl);
                break;
            case "dec":
                convertedString = context.getString(R.string.dec_date_lbl);
                break;
        }
        return convertedString;
    }

    public static String getMonthName(int month, Context context) {
        String convertedString = "";
        switch (month) {
            case 1:
                convertedString = context.getString(R.string.jan_date_lbl);
                break;
            case 2:
                convertedString = context.getString(R.string.feb_date_lbl);
                break;
            case 3:
                convertedString = context.getString(R.string.mar_date_lbl);
                break;
            case 4:
                convertedString = context.getString(R.string.apr_date_lbl);
                break;
            case 5:
                convertedString = context.getString(R.string.may_date_lbl);
                break;
            case 6:
                convertedString = context.getString(R.string.jun_date_lbl);
                break;
            case 7:
                convertedString = context.getString(R.string.jul_date_lbl);
                break;
            case 8:
                convertedString = context.getString(R.string.aug_date_lbl);
                break;
            case 9:
                convertedString = context.getString(R.string.sep_date_lbl);
                break;
            case 10:
                convertedString = context.getString(R.string.oct_date_lbl);
                break;
            case 11:
                convertedString = context.getString(R.string.nov_date_lbl);
                break;
            case 12:
                convertedString = context.getString(R.string.dec_date_lbl);
                break;
        }
        return convertedString;
    }

    private static String getDayName(String dayString, Context context) {
        String convertedString = "";
        switch (dayString.toLowerCase()) {
            case "sun":
                convertedString = context.getString(R.string.sun_date_lbl);
                break;
            case "mon":
                convertedString = context.getString(R.string.mon_date_lbl);
                break;
            case "tue":
                convertedString = context.getString(R.string.tue_date_lbl);
                break;
            case "wed":
                convertedString = context.getString(R.string.wed_date_lbl);
                break;
            case "thu":
                convertedString = context.getString(R.string.thu_date_lbl);
                break;
            case "fri":
                convertedString = context.getString(R.string.fri_date_lbl);
                break;
            case "sat":
                convertedString = context.getString(R.string.sat_date_lbl);
                break;
        }
        return convertedString;
    }

    // getting the maximum value
    public static int getMaxValue(int[] array) {
        int maxValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > maxValue) {
                maxValue = array[i];
            }
        }
        return maxValue;
    }

    public static String distance(double lat1, double lon1, double lat2, double lon2) {
        Location loc1 = new Location("");
        loc1.setLatitude(lat1);
        loc1.setLongitude(lon1);
        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(lon2);
        double distanceInMeters = loc1.distanceTo(loc2);
        return String.format(Locale.ENGLISH, "%.2f", distanceInMeters / 1000);
    }

    public static String convertToHotelCheckDate(String strCurrentDate, Context context) {
        DateFormat originalFormat = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH);
        DateFormat dayFormat = new SimpleDateFormat("EE", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(strCurrentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getDayName(dayFormat.format(date), context) + " " + dateFormat.format(date) + "-" + getYearName(monthFormat.format(date), context) + "-" + targetFormat.format(date);
    }

    public static String convertToHotelCheckDateAndTime(String strCurrentDate, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH);
        DateFormat dayFormat = new SimpleDateFormat("EEE", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(strCurrentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String formattedDate = getDayName(dayFormat.format(date), activity) + ", " + getYearName(monthFormat.format(date), activity) + " " + dateFormat.format(date) + ", " + targetFormat.format(date);
        return formattedDate;
    }

    public static Object getUserEmail(Context context) {
        Object loggedUser;
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String userMail = preferences.getString(Constants.MEMBER_EMAIL, "");
        if (!userMail.equalsIgnoreCase("")) {
            loggedUser = userMail;
        } else {
            loggedUser = JSONObject.NULL;
        }
        return loggedUser;

    }

    public static String getArabicTextOfDayAndMonth(String givenText) {
        String arabicValue = givenText;
        if (givenText.equalsIgnoreCase("SUN"))
            arabicValue = "الأحد";
        else if (givenText.equalsIgnoreCase("MON"))
            arabicValue = "الأحد";
        else if (givenText.equalsIgnoreCase("TUE"))
            arabicValue = "الأحد";
        else if (givenText.equalsIgnoreCase("WED"))
            arabicValue = "الأحد";
        else if (givenText.equalsIgnoreCase("THU"))
            arabicValue = "الأحد";
        else if (givenText.equalsIgnoreCase("FRI"))
            arabicValue = "الأحد";
        else if (givenText.equalsIgnoreCase("SAT"))
            arabicValue = "الأحد";
        else if (givenText.equalsIgnoreCase("JAN"))
            arabicValue = "يناير";
        else if (givenText.equalsIgnoreCase("FEB"))
            arabicValue = "فبراير";
        else if (givenText.equalsIgnoreCase("MAR"))
            arabicValue = "مارس";
        else if (givenText.equalsIgnoreCase("APR"))
            arabicValue = "أبريل";
        else if (givenText.equalsIgnoreCase("MAY"))
            arabicValue = "مايو";
        else if (givenText.equalsIgnoreCase("JUN"))
            arabicValue = "يونيو";
        else if (givenText.equalsIgnoreCase("JUL"))
            arabicValue = "يوليو";
        else if (givenText.equalsIgnoreCase("AUG"))
            arabicValue = "أغسطس";
        else if (givenText.equalsIgnoreCase("SEP"))
            arabicValue = "سبتمبر";
        else if (givenText.equalsIgnoreCase("OCT"))
            arabicValue = "أكتوبر";
        else if (givenText.equalsIgnoreCase("NOV"))
            arabicValue = "نوفمبر";
        else if (givenText.equalsIgnoreCase("DEC"))
            arabicValue = "ديسمبر";
        return arabicValue;
    }

    public static String getArabicTextOfBoardBasis(String givenText) {
        String arabicValue = givenText;
        if (givenText.equalsIgnoreCase("Bed and breakfast")) {
            arabicValue = "مبيت و افطار";
        } else if (givenText.equalsIgnoreCase("Half board")) {
            arabicValue = "افطار و عشاء";
        } else if (givenText.equalsIgnoreCase("Full board")) {
            arabicValue = "جميع الوجبات";
        } else if (givenText.equalsIgnoreCase("Room Only")) {
            arabicValue = "غرفة فقط";
        } else if (givenText.equalsIgnoreCase("None")) {
            arabicValue = "غرفة فقط";
        }
        return arabicValue;
    }

    public void setCurrencyList(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String data = pref.getString(Constants.ALL_CURRENCY, "");
        Type listType = new TypeToken<ArrayList<CurrencyBean>>() {
        }.getType();
        currencyList = new Gson().fromJson(data, listType);
    }

    public static String formattimeToString(String datestring, Activity activity) {
        String resultStr = "";
        if (datestring != null) {
            String[] separated = datestring.split("T");
            if (separated.length == 2) {
                String[] timeseparated = separated[1].split(":");
                if (timeseparated.length == 3) {
                    resultStr = timeseparated[0] + ":" + timeseparated[1];// this will contain " they taste good"
                }
            }
        }
        return resultStr;
    }

    public static String getPriceCurrencyCode(Context context) {
        String userSelectedCurrency = "";
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyCode = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        if (selectedCurrencyCode.equalsIgnoreCase("SAR")) {
            userSelectedCurrency = context.getString(R.string.label_SAR_currency_name);
        } else {
            userSelectedCurrency = selectedCurrencyCode;
        }
        return userSelectedCurrency;
    }

    public static String getMonthandDateString(String datestring, Context context) {
        String resultstring = "";
        if (datestring != null) {
            String[] separated = datestring.split("-");
            if (separated.length == 3) {
                String monthstring = getMonthName(Integer.parseInt(separated[1]), context);
                resultstring = separated[2] + " " + monthstring;
            }
        }
        return resultstring;
    }

    public static int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static void printMessage(String tag, String message) {
        if (Constants.IS_LOG_ENABLED) {
            Log.e(tag, message);
        }
    }

    public static String hijriCalendarDate(boolean adjust) {
        String hijriDate = "";
        Calendar today = Calendar.getInstance();
        int adj = 0;
        if (adjust) {
            adj = 0;
        } else {
            adj = 1;
        }
        if (adjust) {
            int adjustmili = 1000 * 60 * 60 * 24 * adj;
            long todaymili = today.getTimeInMillis() + adjustmili;
            today.setTimeInMillis(todaymili);
        }
        double day = today.get(Calendar.DAY_OF_MONTH);
        double month = today.get(Calendar.MONTH);
        double year = today.get(Calendar.YEAR);
        double m = month + 1;
        double y = year;
        if (m < 3) {
            y -= 1;
            m += 12;
        }
        double a = Math.floor(y / 100.);
        double b = 2 - a + Math.floor(a / 4.);
        if (y < 1583)
            b = 0;
        if (y == 1582) {
            if (m > 10)
                b = -10;
            if (m == 10) {
                b = 0;
                if (day > 4)
                    b = -10;
            }
        }
        double jd = Math.floor(365.25 * (y + 4716)) + Math.floor(30.6001 * (m + 1)) + day
                + b - 1524;
        b = 0;
        if (jd > 2299160) {
            a = Math.floor((jd - 1867216.25) / 36524.25);
            b = 1 + a - Math.floor(a / 4.);
        }
        double bb = jd + b + 1524;
        double cc = Math.floor((bb - 122.1) / 365.25);
        double dd = Math.floor(365.25 * cc);
        double ee = Math.floor((bb - dd) / 30.6001);
        day = (bb - dd) - Math.floor(30.6001 * ee);
        month = ee - 1;
        if (ee > 13) {
            cc += 1;
            month = ee - 13;
        }
        year = cc - 4716;
        double wd = gmod(jd + 1, 7) + 1;
        double iyear = 10631. / 30.;
        double epochastro = 1948084;
        double epochcivil = 1948085;
        double shift1 = 8.01 / 60.;
        double z = jd - epochastro;
        double cyc = Math.floor(z / 10631.);
        z = z - 10631 * cyc;
        double j = Math.floor((z - shift1) / iyear);
        double iy = 30 * cyc + j;
        z = z - Math.floor(j * iyear + shift1);
        double im = Math.floor((z + 28.5001) / 29.5);
        if (im == 13)
            im = 12;
        double id = z - Math.floor(29.5001 * im - 29);
        double[] myRes = new double[8];
        myRes[0] = day; // calculated day (CE)
        myRes[1] = month - 1; // calculated month (CE)
        myRes[2] = year; // calculated year (CE)
        myRes[3] = jd - 1; // julian day number
        myRes[4] = wd - 1; // weekday number
        myRes[5] = id; // islamic date
        myRes[6] = im - 1; // islamic month
        myRes[7] = iy; // islamic year
        hijriDate = (int) myRes[5] + "-" + (int) myRes[6] + "-" + (int) myRes[7];
        return hijriDate;
    }

    static double gmod(double n, double m) {
        return ((n % m) + m) % m;
    }

    public static String addDaysToCalendar(String checkInDate) {
        String dateInString = checkInDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dateInString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, 10);
        sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date resultDate = new Date(c.getTimeInMillis());
        dateInString = sdf.format(resultDate);
        return dateInString.toString();
    }

    public static String addOneDayToCalendar(String checkInDate) {
        String dateInString = checkInDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dateInString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, 1);
        sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date resultDate = new Date(c.getTimeInMillis());
        dateInString = sdf.format(resultDate);
        return dateInString.toString();
    }

    public static String addReviewDaysToCalendar(String checkInDate, String duration) {
        String dateInString = checkInDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dateInString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, Integer.parseInt(duration));
        sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date resultDate = new Date(c.getTimeInMillis());
        dateInString = sdf.format(resultDate);
        return dateInString.toString();
    }

    public static int getJourneyDifferenceTime(String arrivalDate, String departureDate) {
        int durationInMinutes = 0;
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        Date dateCurrent = null;
        Date dateBefore = null;
        try {
            dateCurrent = simpleDateFormat.parse(departureDate);
            dateBefore = simpleDateFormat.parse(arrivalDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (dateCurrent == null || dateBefore == null) {
            return 0;
        }

        long timeDifference = dateCurrent.getTime() - dateBefore.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedHours = timeDifference / hoursInMilli;
        timeDifference = timeDifference % hoursInMilli;
        long elapsedMinutes = timeDifference / minutesInMilli;
        durationInMinutes = ((int) elapsedHours * 60) + (int) elapsedMinutes;
        return durationInMinutes;
    }

    public static void getFlightStopValues(Activity activity, String tripType, boolean isNonStopChecked, boolean multiCityNonStopChecked,
                                           ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList) {
        ArrayList<FilterStopSelectionItem> stopsListArray = new ArrayList<>();
        if (Singleton.getInstance().resultStopsArrayList.size() == 0) {
            ArrayList<String> stopsTempArray = new ArrayList<>();
            for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    for (int k = 0; k < bean.getFsDataBeanArrayList().size(); k++) {
                        if ((bean.getFsDataBeanArrayList().get(0).getSq()) != null) {
                            if (!stopsTempArray.contains(bean.getFsDataBeanArrayList().get(0).getSq())) {
                                stopsTempArray.add(bean.getFsDataBeanArrayList().get(0).getSq());
                            }
                        }
                    }
                }
            }
            stopsListArray.clear();
            FilterStopSelectionItem stopSelectionItem = new FilterStopSelectionItem();
            for (int i = 0; i < stopsTempArray.size(); i++) {
                String stopVar = stopsTempArray.get(i);
                if (stopVar.equalsIgnoreCase("0")) {
                    stopSelectionItem = new FilterStopSelectionItem();
                    stopSelectionItem.setStopMessage(activity.getResources().getString(R.string.label_non_stop_text));
                    stopSelectionItem.setStopType(0);
                    if (tripType.equalsIgnoreCase("1") || tripType.equalsIgnoreCase("2")) {
                        if (isNonStopChecked) {
                            stopSelectionItem.setIsSelected(true);
                        } else {
                            stopSelectionItem.setIsSelected(false);
                        }
                    } else if (tripType.equalsIgnoreCase("3")) {
                        if (multiCityNonStopChecked) {
                            stopSelectionItem.setIsSelected(true);
                        } else {
                            stopSelectionItem.setIsSelected(false);
                        }
                    }
                    stopsListArray.add(stopSelectionItem);
                } else if (stopVar.equalsIgnoreCase("1")) {
                    stopSelectionItem = new FilterStopSelectionItem();
                    stopSelectionItem.setStopMessage("1 " + activity.getResources().getString(R.string.label_stop_text));
                    stopSelectionItem.setStopType(1);
                    stopSelectionItem.setIsSelected(false);
                    stopsListArray.add(stopSelectionItem);
                } else {
                    stopSelectionItem = new FilterStopSelectionItem();
                    stopSelectionItem.setStopMessage("2+ " + activity.getResources().getString(R.string.label_stops_text));
                    stopSelectionItem.setStopType(2);
                    stopSelectionItem.setIsSelected(false);
                    stopsListArray.add(stopSelectionItem);
                }
            }
            Collections.sort(stopsListArray, new Comparator<FilterStopSelectionItem>() {
                @Override
                public int compare(FilterStopSelectionItem e1, FilterStopSelectionItem e2) {
                    Integer a = e1.getStopType();
                    Integer b = e2.getStopType();
                    return a.compareTo(b);
                }
            });
            Singleton.getInstance().resultStopsArrayList = stopsListArray;
        } else {
            stopsListArray.addAll(Singleton.getInstance().resultStopsArrayList);
        }
    }

    public static void getSelectedAirlineValues(ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList, String selectedMal,
                                                boolean isForwardResults) {
        if (isForwardResults) {
            ArrayList<FilterAirlineSelectionItem> airLineArray = new ArrayList<>();
            if (Singleton.getInstance().resultAirlinesArrayList.size() == 0) {
                airLineArray.clear();
                for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                    for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                        String refNum = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getRefNum();
                        OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                        if (refNum.equalsIgnoreCase("1")) {
                            for (int k = 0; k < bean.getFsDataBeanArrayList().size(); k++) {
                                boolean isAvailable = false;
                                for (FilterAirlineSelectionItem item : airLineArray) {
                                    if (item.getMal().equalsIgnoreCase(bean.getFsDataBeanArrayList().get(k).getMal())) {
                                        isAvailable = true;
                                        break;
                                    }
                                }
                                if (!isAvailable) {
                                    if (selectedMal.isEmpty()) {
                                        airLineArray.add(new FilterAirlineSelectionItem(bean.getFsDataBeanArrayList().get(k).getMal(), false));
                                    } else {
                                        if (selectedMal.equalsIgnoreCase(bean.getFsDataBeanArrayList().get(k).getMal())) {
                                            airLineArray.add(new FilterAirlineSelectionItem(bean.getFsDataBeanArrayList().get(k).getMal(), true));
                                        } else {
                                            airLineArray.add(new FilterAirlineSelectionItem(bean.getFsDataBeanArrayList().get(k).getMal(), false));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                airLineArray.addAll(Singleton.getInstance().resultAirlinesArrayList);
            }
        } else {
            ArrayList<FilterAirlineSelectionItem> airLineArray = new ArrayList<>();
            if (Singleton.getInstance().returnResultAirlinesArrayList.size() == 0) {
                airLineArray.clear();
                for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                    for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                        String refNum = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getRefNum();
                        OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                        if (refNum.equalsIgnoreCase("2")) {
                            for (int k = 0; k < bean.getFsDataBeanArrayList().size(); k++) {
                                boolean isAvailable = false;
                                for (FilterAirlineSelectionItem item : airLineArray) {
                                    if (item.getMal().equalsIgnoreCase(bean.getFsDataBeanArrayList().get(k).getMal())) {
                                        isAvailable = true;
                                        break;
                                    }
                                }
                                if (!isAvailable) {
                                    if (selectedMal.isEmpty()) {
                                        airLineArray.add(new FilterAirlineSelectionItem(bean.getFsDataBeanArrayList().get(k).getMal(), false));
                                    } else {
                                        if (selectedMal.equalsIgnoreCase(bean.getFsDataBeanArrayList().get(k).getMal())) {
                                            airLineArray.add(new FilterAirlineSelectionItem(bean.getFsDataBeanArrayList().get(k).getMal(), true));
                                        } else {
                                            airLineArray.add(new FilterAirlineSelectionItem(bean.getFsDataBeanArrayList().get(k).getMal(), false));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                airLineArray.addAll(Singleton.getInstance().returnResultAirlinesArrayList);
            }
        }
    }

    public static String formatDateToUserDateFormat(String selectedDate, Activity activity) {
        Date date = null;
        DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        DateFormat dayFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("yy", Locale.ENGLISH);
        try {
            date = originalFormat.parse(selectedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String formattedDate = dayFormat.format(date) + " " + getYearName(dateFormat.format(date), activity) + " " + monthFormat.format(date);
        return formattedDate;
    }

    public static String getMealName(Activity activity, String selectedMealName) {
        if (selectedMealName.equalsIgnoreCase("none")) {
            return activity.getString(R.string.label_room_only);
        }
        String mealName = "";
        InputStream inputStream = null;
        try {
            inputStream = activity.getAssets().open("meal_type.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        CSVFile csvFile = new CSVFile(inputStream);
        if (Utils.isArabicLangSelected(activity)) {
            mealName = csvFile.getMealType(selectedMealName, Constants.LANGUAGE_ARABIC_CODE);
        } else {
            mealName = selectedMealName;
        }
        if (mealName.isEmpty()) {
            mealName = selectedMealName;
        }
        return mealName;
    }

    public static String getAircraftType(String aircraftType, Activity activity) {
        String aircraftName = "";
        InputStream inputStream = null;
        try {
            inputStream = activity.getAssets().open("aircraft_master.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        CSVFile csvFile = new CSVFile(inputStream);
        aircraftName = csvFile.getAircraftName(aircraftType);
        if (aircraftName.isEmpty()) {
            aircraftName = aircraftType;
        }
        return aircraftName;
    }

    //Convert timestamp to minutes format
    public static long dateFormatToMinutesFormat(String date) {
        long resultTime = 0;
        DateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", Locale.ENGLISH);
        Date dateCurrent = null;
        try {
            dateCurrent = simpleDateFormat.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long timeDifference = dateCurrent.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedHours = timeDifference / hoursInMilli;
        timeDifference = timeDifference % hoursInMilli;
        long elapsedMinutes = timeDifference / minutesInMilli;
        resultTime = (elapsedHours * 60) + elapsedMinutes;
        return resultTime;
    }

    public static long dateFormatToHotelMinutesFormat(String date) {
        long resultTime = 0;
        DateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH);
        Date dateCurrent = null;
        try {
            dateCurrent = simpleDateFormat.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long timeDifference = dateCurrent.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedHours = timeDifference / hoursInMilli;
        timeDifference = timeDifference % hoursInMilli;
        long elapsedMinutes = timeDifference / minutesInMilli;
        resultTime = (elapsedHours * 60) + elapsedMinutes;
        return resultTime;
    }

    public static String cancellationDateFormat(String dateTime, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
        DateFormat dayFormat = new SimpleDateFormat("E", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMMMMM", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String formattedDate = getDayName(dayFormat.format(date), activity) + ", " + dateFormat.format(date) + " " + getYearName(monthFormat.format(date), activity);
        return formattedDate;
    }

    public static String flightLayoverTimeFormat(String currentDate, String previousDate, Activity activity) {
        String resultStr = "";
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        Date dateCurrent = null;
        Date dateBefore = null;
        try {
            dateCurrent = simpleDateFormat.parse(currentDate);
            dateBefore = simpleDateFormat.parse(previousDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long timeDifference = dateCurrent.getTime() - dateBefore.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedHours = timeDifference / hoursInMilli;
        timeDifference = timeDifference % hoursInMilli;
        long elapsedMinutes = timeDifference / minutesInMilli;
        try {
            if (elapsedHours == 1 && elapsedMinutes == 1) {
                resultStr = elapsedHours + "" + activity.getResources().getString(R.string.SHLbl) + " " + elapsedMinutes + "" + activity.getResources().getString(R.string.SMLbl) + " ";
            } else if ((elapsedHours > 1 && elapsedHours < 11) && elapsedMinutes == 1) {
                resultStr = elapsedHours + "" + activity.getResources().getString(R.string.PHsLbl) + " " + elapsedMinutes + "" + activity.getResources().getString(R.string.SMLbl) + " ";
            } else if (elapsedHours > 11 && elapsedMinutes == 1) {
                resultStr = elapsedHours + "" + activity.getResources().getString(R.string.MoreHsLbl) + " " + elapsedMinutes + "" + activity.getResources().getString(R.string.SMLbl) + " ";
            } else if (elapsedHours == 1 && (elapsedMinutes > 1 && elapsedMinutes < 11)) {
                resultStr = elapsedHours + "" + activity.getResources().getString(R.string.SHLbl) + " " + elapsedMinutes + "" + activity.getResources().getString(R.string.PMsLbl) + " ";
            } else if ((elapsedHours > 1 && elapsedHours < 11) && (elapsedMinutes > 1 && elapsedMinutes < 11)) {
                resultStr = elapsedHours + "" + activity.getResources().getString(R.string.PHsLbl) + " " + elapsedMinutes + "" + activity.getResources().getString(R.string.PMsLbl) + " ";
            } else if (elapsedHours > 11 && (elapsedMinutes > 1 && elapsedMinutes < 11)) {
                resultStr = elapsedHours + "" + activity.getResources().getString(R.string.MoreHsLbl) + " " + elapsedMinutes + "" + activity.getResources().getString(R.string.PMsLbl) + " ";
            } else if (elapsedHours == 1 && elapsedMinutes > 11) {
                resultStr = elapsedHours + "" + activity.getResources().getString(R.string.SHLbl) + " " + elapsedMinutes + "" + activity.getResources().getString(R.string.MoreMsLbl) + " ";
            } else if ((elapsedHours > 1 && elapsedHours < 11) && elapsedMinutes > 11) {
                resultStr = elapsedHours + "" + activity.getResources().getString(R.string.PHsLbl) + " " + elapsedMinutes + "" + activity.getResources().getString(R.string.MoreMsLbl) + " ";
            } else if (elapsedHours > 11 && elapsedMinutes > 11) {
                resultStr = elapsedHours + "" + activity.getResources().getString(R.string.MoreHsLbl) + " " + elapsedMinutes + "" + activity.getResources().getString(R.string.MoreMsLbl) + " ";
            } else {
                resultStr = elapsedHours + "" + activity.getResources().getString(R.string.SHLbl) + " " + elapsedMinutes + "" + activity.getResources().getString(R.string.SMLbl) + " ";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultStr;
    }

    public static String dateFormatToHoursMinutes(String date) {
        String timeInMin = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        try {
            Date varDate = dateFormat.parse(date);
            dateFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            timeInMin = dateFormat.format(varDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeInMin;
    }

    public static String getDaysInHrMinsFormat(String currentDate, String previousDate, Activity activity) {
        if (currentDate.isEmpty() || previousDate.isEmpty()) {
            return "";
        }
        String resultTime = "";
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        Date dateCurrent = null;
        Date dateBefore = null;
        try {
            dateCurrent = simpleDateFormat.parse(currentDate);
            dateBefore = simpleDateFormat.parse(previousDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long timeDifference = dateCurrent.getTime() - dateBefore.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedHours = timeDifference / hoursInMilli;
        timeDifference = timeDifference % hoursInMilli;
        long elapsedMinutes = timeDifference / minutesInMilli;
        String hrsLbl = "";
        if (elapsedHours == 1) {
            hrsLbl = elapsedHours + "" + activity.getResources().getString(R.string.SHLbl);
        } else if (elapsedHours > 1 && elapsedHours < 10) {
            hrsLbl = elapsedHours + "" + activity.getResources().getString(R.string.PHsLbl);
        } else {
            hrsLbl = elapsedHours + "" + activity.getResources().getString(R.string.MoreHsLbl);
        }
        String minsLbl = "";
        if (elapsedMinutes == 1) {
            minsLbl = elapsedMinutes + "" + activity.getResources().getString(R.string.SMLbl);
        } else if (elapsedMinutes > 1 && elapsedMinutes < 10) {
            minsLbl = elapsedMinutes + "" + activity.getResources().getString(R.string.PMsLbl);
        } else {
            minsLbl = elapsedMinutes + "" + activity.getResources().getString(R.string.MoreMsLbl);
        }
        resultTime = hrsLbl + " " + minsLbl;
        return resultTime;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static String convertToHotelCheckDateFormat(String strCurrentDate, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat dayFormat = new SimpleDateFormat("EEE", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(strCurrentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String formattedDate = getDayName(dayFormat.format(date), activity) + ", " + getYearName(monthFormat.format(date), activity) + " " + dateFormat.format(date) + ", " + targetFormat.format(date);
        return formattedDate;
    }

    public static String formatTimeStampToTripAdvisorDate(String dateTime, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat yearFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String formattedDate = getYearName(monthFormat.format(date), activity) + " " + dateFormat.format(date) + ", " + yearFormat.format(date);
        return formattedDate;
    }

    public static String convert(String myString) {
        String resultStr = myString.trim();
        if (resultStr.contains("\\u")) {
            String str = resultStr.split(" ")[0];
            str = str.replace("\\", "");
            String[] arr = str.split("u");
            String text = "";
            for (
                    int i = 1;
                    i < arr.length; i++) {
                int hexVal = Integer.parseInt(arr[i], 16);
                text += (char) hexVal;
            }
            return text;
        } else {
            return resultStr;
        }
    }

    public static String addDayToCalendar(String checkInDate) {
        String dateInString = checkInDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dateInString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, 0);
        sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date resultDate = new Date(c.getTimeInMillis());
        dateInString = sdf.format(resultDate);
        return dateInString.toString();
    }

    public static JSONObject getAppSettingData(JSONObject sourceObj, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        try {
            if (preferences.getString(Constants.ISCOUNTRY_GLOBAL, "GL").equalsIgnoreCase("EG")) {
                sourceObj.put("agentDutyCode", Constants.AGENT_DUTY_CODE_EG);
                sourceObj.put("device", Constants.DEVICE_TYPE);
                sourceObj.put("companyId", Constants.COMPANY_ID_EG_FLIGHTS);
            } else {
                sourceObj.put("agentDutyCode", Constants.AGENT_DUTY_CODE_FLYIN);
                sourceObj.put("device", Constants.DEVICE_TYPE);
                sourceObj.put("companyId", Constants.COMPANY_ID_GL_FLIGHTS);
            }
        } catch (Exception e) {
        }
        return sourceObj;
    }

    public static JSONObject getHotelSearchAppSettingData(JSONObject sourceObj, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        try {
            if (preferences.getString(Constants.ISCOUNTRY_GLOBAL, "GL").equalsIgnoreCase("EG")) {
                sourceObj.put("agentDutyCode", Constants.AGENT_DUTY_CODE_EG_HOTELS);
                sourceObj.put("device", Constants.DEVICE_TYPE);
            } else {
                sourceObj.put("agentDutyCode", Constants.AGENT_DUTY_CODE_GL_HOTELS);
                sourceObj.put("device", Constants.DEVICE_TYPE);
            }
        } catch (Exception e) {
        }
        return sourceObj;
    }

    public static JSONObject getHotelAppSettingData(JSONObject sourceObj, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        try {
            if (preferences.getString(Constants.ISCOUNTRY_GLOBAL, "GL").equalsIgnoreCase("EG")) {
                sourceObj.put("companyId", Constants.COMPANY_ID_EG_HOTELS);
            } else {
                sourceObj.put("companyId", Constants.COMPANY_ID_GL_HOTELS);
            }
        } catch (Exception e) {
        }
        return sourceObj;
    }

    public static String formatDateToMonthDateFormat(String dateTime, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String formattedDate = targetFormat.format(date) + " " + getYearName(monthFormat.format(date), activity);
        return formattedDate;
    }

    public static void getOneWayFlightStopValues(Activity activity, String tripType, boolean isNonStopChecked, boolean multiCityNonStopChecked,
                                                 ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList) {
        ArrayList<FilterStopSelectionItem> stopsListArray = new ArrayList<>();
        if (Singleton.getInstance().resultStopsArrayList.size() == 0) {
            ArrayList<String> stopsTempArray = new ArrayList<>();
            for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    String refNum = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getRefNum();
                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    for (int k = 0; k < bean.getFsDataBeanArrayList().size(); k++) {
                        if ((bean.getFsDataBeanArrayList().get(0).getSq()) != null && refNum.equalsIgnoreCase("1")) {
                            if (!stopsTempArray.contains(bean.getFsDataBeanArrayList().get(0).getSq())) {
                                stopsTempArray.add(bean.getFsDataBeanArrayList().get(0).getSq());
                            }
                        }
                    }
                }
            }
            stopsListArray.clear();
            FilterStopSelectionItem stopSelectionItem = new FilterStopSelectionItem();
            for (int i = 0; i < stopsTempArray.size(); i++) {
                String stopVar = stopsTempArray.get(i);
                if (stopVar.equalsIgnoreCase("0")) {
                    stopSelectionItem = new FilterStopSelectionItem();
                    stopSelectionItem.setStopMessage(activity.getResources().getString(R.string.label_non_stop_text));
                    stopSelectionItem.setStopType(0);
                    if (tripType.equalsIgnoreCase("1") || tripType.equalsIgnoreCase("2")) {
                        if (isNonStopChecked) {
                            stopSelectionItem.setIsSelected(true);
                        } else {
                            stopSelectionItem.setIsSelected(false);
                        }
                    } else if (tripType.equalsIgnoreCase("3")) {
                        if (multiCityNonStopChecked) {
                            stopSelectionItem.setIsSelected(true);
                        } else {
                            stopSelectionItem.setIsSelected(false);
                        }
                    }
                    stopsListArray.add(stopSelectionItem);
                } else if (stopVar.equalsIgnoreCase("1")) {
                    stopSelectionItem = new FilterStopSelectionItem();
                    stopSelectionItem.setStopMessage("1 " + activity.getResources().getString(R.string.label_stop_text));
                    stopSelectionItem.setStopType(1);
                    stopSelectionItem.setIsSelected(false);
                    stopsListArray.add(stopSelectionItem);
                } else {
                    stopSelectionItem = new FilterStopSelectionItem();
                    stopSelectionItem.setStopMessage("2+ " + activity.getResources().getString(R.string.label_stops_text));
                    stopSelectionItem.setStopType(2);
                    stopSelectionItem.setIsSelected(false);
                    stopsListArray.add(stopSelectionItem);
                }
            }
            Collections.sort(stopsListArray, new Comparator<FilterStopSelectionItem>() {
                @Override
                public int compare(FilterStopSelectionItem e1, FilterStopSelectionItem e2) {
                    Integer a = e1.getStopType();
                    Integer b = e2.getStopType();
                    return a.compareTo(b);
                }
            });
            Singleton.getInstance().resultStopsArrayList = stopsListArray;
        } else {
            stopsListArray.addAll(Singleton.getInstance().resultStopsArrayList);
        }
    }


    public static Spannable addFlightGreyField(String inputText, Activity activity) {
        Spannable textField = new SpannableString(" (" + inputText + ")");
        textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.flight_name_colors)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }
//    TextView textView  = (TextView) findViewById(R.id.textview);
//    SpannableString ss = new SpannableString("abc");
//    Drawable d = getResources().getDrawable(R.drawable.icon32);
//            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
//    ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
//            ss.setSpan(span, 0, 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
//            textView.setText(ss);
//var imageSpan = new ImageSpan(this, Resource.Drawable.Icon); //Find your drawable.
//    var spannableString = new SpannableString(textView.Text); //Set text of SpannableString from TextView
//            spannableString.SetSpan(imageSpan, textView.Text.Length -1, textView.Text.Length, 0); //Add image at end of string
//
//    textView.TextFormatted = spannableString; //Assign string to TextView (Use TextFormatted for Spannables)


    public static Spannable addFlydubaiField(String inputText, String baggage, Activity activity) {
        String baggage2 = inputText + " ( " + baggage + " )";
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(baggage2);
        ImageSpan busImageSpan = new ImageSpan(activity, R.drawable.bus_icon);
        ForegroundColorSpan bgSpanColorRed = new ForegroundColorSpan(activity.getResources().getColor(R.color.flight_name_colors));
        ssBuilder.setSpan(
                busImageSpan,
                baggage2.indexOf(")") + String.valueOf(")").length(),
                baggage2.indexOf(")") + String.valueOf(")").length() + 1,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        //  ssBuilder.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.flight_name_colors)), 0, baggage2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        Spannable textField = new SpannableString(" (" + baggage + ")");
//        Drawable d = activity.getResources().getDrawable(R.drawable.bus_icon);
//        ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
//        textField.setSpan(span, baggage.length() - 1, baggage.length(), 0);
        // textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.flight_name_colors)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ssBuilder;
    }


    public static String dayDiff(String startDate, String endDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            Date stDate = sdf.parse(startDate);
            Date enDate = sdf.parse(endDate);
            String formattedStartTime = output.format(stDate);
            String formattedEndTime = output.format(enDate);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            if (stDate.getDay() == enDate.getDay()) {
                return "";
            } else {
                return "+1d";
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String getStopOverTimeFormat(String currentDate, String previousDate, Activity activity) {
        if (currentDate.isEmpty() || previousDate.isEmpty()) {
            return "";
        }
        String resultTime = "";
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        Date dateCurrent = null;
        Date dateBefore = null;
        try {
            dateCurrent = simpleDateFormat.parse(currentDate);
            dateBefore = simpleDateFormat.parse(previousDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long timeDifference = dateCurrent.getTime() - dateBefore.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedHours = timeDifference / hoursInMilli;
        timeDifference = timeDifference % hoursInMilli;
        long elapsedMinutes = timeDifference / minutesInMilli;
        String hrsFormat = String.format(Locale.ENGLISH, "%02d", ((int) elapsedHours));
        String minsFormat = String.format(Locale.ENGLISH, "%02d", ((int) elapsedMinutes));
        resultTime = hrsFormat + ":" + minsFormat;
        return resultTime;
    }

    public static String minutesToTimeFormat(String time, Activity activity) {
        String newTime = "";
        if (time.equalsIgnoreCase("null") || time.isEmpty()) {
            return "";
        }
        try {
            String startTime = "00:00";
            int minutes = Integer.parseInt(time);
            int h = minutes / 60 + Integer.valueOf(startTime.substring(0, 1));
            int m = minutes % 60 + Integer.valueOf(startTime.substring(3, 4));
            if (h == 1 && m == 1) {
                newTime = h + "" + activity.getResources().getString(R.string.label_one_hr) + " " + m + "" + activity.getResources().getString(R.string.label_one_min) + " ";
            } else if ((h > 1 && h < 11) && m == 1) {
                newTime = h + "" + activity.getResources().getString(R.string.label_two_ten_hr) + " " + m + "" + activity.getResources().getString(R.string.label_one_min) + " ";
            } else if (h > 11 && m == 1) {
                newTime = h + "" + activity.getResources().getString(R.string.label_eleven_hr) + " " + m + "" + activity.getResources().getString(R.string.label_one_min) + " ";
            } else if (h == 1 && (m > 1 && m < 11)) {
                newTime = h + "" + activity.getResources().getString(R.string.label_one_hr) + " " + m + "" + activity.getResources().getString(R.string.label_two_ten_min) + " ";
            } else if ((h > 1 && h < 11) && (m > 1 && m < 11)) {
                newTime = h + "" + activity.getResources().getString(R.string.label_two_ten_hr) + " " + m + "" + activity.getResources().getString(R.string.label_two_ten_min) + " ";
            } else if (h > 11 && (m > 1 && m < 11)) {
                newTime = h + "" + activity.getResources().getString(R.string.label_eleven_hr) + " " + m + "" + activity.getResources().getString(R.string.label_two_ten_min) + " ";
            } else if (h == 1 && m > 11) {
                newTime = h + "" + activity.getResources().getString(R.string.label_one_hr) + " " + m + "" + activity.getResources().getString(R.string.label_eleven_min) + " ";
            } else if ((h > 1 && h < 11) && m > 11) {
                newTime = h + "" + activity.getResources().getString(R.string.label_two_ten_hr) + " " + m + "" + activity.getResources().getString(R.string.label_eleven_min) + " ";
            } else if (h > 11 && m > 11) {
                newTime = h + "" + activity.getResources().getString(R.string.label_eleven_hr) + " " + m + "" + activity.getResources().getString(R.string.label_eleven_min) + " ";
            } else {
                newTime = h + "" + activity.getResources().getString(R.string.label_one_hr) + " " + m + "" + activity.getResources().getString(R.string.label_one_min) + " ";
            }
        } catch (Exception e) {
            e.printStackTrace();
            newTime = "";
        }
        return newTime;
    }

    public static String decodeUnicode(String theString) {
        char aChar;
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);
        for (int x = 0; x < len; ) {
            aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if (aChar == 'u') {
                    // Read the xxxx
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = theString.charAt(x++);
                        switch (aChar) {
                            case '0':
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                value = (value << 4) + aChar - '0';
                                break;
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                                value = (value << 4) + 10 + aChar - 'a';
                                break;
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                                value = (value << 4) + 10 + aChar - 'A';
                                break;
                            default:
                                throw new IllegalArgumentException(
                                        "Malformed   \\uxxxx   encoding.");
                        }

                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't')
                        aChar = '\t';
                    else if (aChar == 'r')
                        aChar = '\r';
                    else if (aChar == 'n')
                        aChar = '\n';
                    else if (aChar == 'f')
                        aChar = '\f';
                    outBuffer.append(aChar);
                }
            } else
                outBuffer.append(aChar);
        }
        return outBuffer.toString();
    }

    public static String getFlightMealName(Activity activity, String selectedMealName) {
        String mealName = "";
        InputStream inputStream = null;
        try {
            inputStream = activity.getAssets().open("airmeal.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        CSVFile csvFile = new CSVFile(inputStream);
        if (Utils.isArabicLangSelected(activity)) {
            mealName = csvFile.getFlightAirMeal(selectedMealName, Constants.LANGUAGE_ARABIC_CODE);
        } else {
            mealName = csvFile.getFlightAirMeal(selectedMealName, Constants.LANGUAGE_ENGLISH_CODE);
        }
        if (mealName.isEmpty()) {
            mealName = selectedMealName;
        }
        return mealName;
    }

    public static String getCountryName(String countryCode, Activity activity) {
        String countryName = "";
        InputStream inputStream = null;
        try {
            inputStream = activity.getAssets().open("country_codes.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        CSVFile csvFile = new CSVFile(inputStream);
        countryName = csvFile.getCountryFlagName(countryCode);
        if (countryName.isEmpty()) {
            countryName = countryCode;
        }
        return countryName;
    }

    public static String convertRewardTimetoActualFormat(String strCurrentDate, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        DateFormat dayFormat = new SimpleDateFormat("EEE", Locale.ENGLISH);
        DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(strCurrentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String formattedDate = getDayName(dayFormat.format(date), activity) + ", " + dateFormat.format(date) + " " + getYearName(monthFormat.format(date), activity) + " " + targetFormat.format(date);
        return formattedDate;
    }

    public static String convertLoyaltyMemberDateFormat(String strCurrentDate, Context context) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Date newDate = null;
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        format = new SimpleDateFormat("EEE dd MMM yyyy", Locale.ENGLISH);
        String date = format.format(newDate);
        String str[] = date.split(" ");
        date = getDayName(str[0], context) + ", " + str[1] + " " + getYearName(str[2], context) + " " + str[3];
        return date;
    }

    public static long dateFormatToLoyaltyDurationFormat(String date) {
        long resultTime = 0;
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        Date dateCurrent = null;
        try {
            dateCurrent = simpleDateFormat.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long timeDifference = dateCurrent.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedHours = timeDifference / hoursInMilli;
        timeDifference = timeDifference % hoursInMilli;
        long elapsedMinutes = timeDifference / minutesInMilli;
        resultTime = (elapsedHours * 60) + elapsedMinutes;
        return resultTime;
    }

    public static String formatTripDateToServerDateFormat(String dateTime, Activity activity) {
        DateFormat originalFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return targetFormat.format(date);
    }

    public static String convertGregToHijriDate(String hijriDt) {
        String hijriDate = "";
        String[] hijriActualFormat = hijriDt.split("-");
        String requiredFormat = hijriActualFormat[2] + "-" + hijriActualFormat[1] + "-" + hijriActualFormat[0];
        ISOChronology iso = ISOChronology.getInstanceUTC();
        IslamicChronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(requiredFormat, hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        hijriDate = String.valueOf(dtIso);
        String[] gregorianDate = hijriDate.split("T");
        Utils.printMessage(TAG, "dtHijri :: " + dtHijri + " dtIso : " + dtIso + "  hijriDate : " + gregorianDate[0]);
        return gregorianDate[0];
    }

    public static String convertToConfirmationcCalendardate(String strCurrentDate, Context context) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH);
        Date newDate = null;
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        format = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        String date = format.format(newDate);
        String str[] = date.split(" ");
        date = str[0] + " " + getYearName(str[1], context) + " " + str[2];
        return date;
    }

    public static String convertOtherCurrencytoSarCurrency(Double currentPrice, Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedCurrencyValue = pref.getString(Constants.SELECTED_CURRENCY_VALUE, "1");
        String bufferRateValue = pref.getString(Constants.SELECTED_CURRENCY_BUFFER_RATE, "0");
        Double bufferPrice = Double.parseDouble(selectedCurrencyValue) + ((Double.parseDouble(selectedCurrencyValue) * Double.parseDouble(bufferRateValue)) / 100);
        Double calculatedPrice = currentPrice * bufferPrice;
        return String.format(Locale.ENGLISH, "%.2f", calculatedPrice);
    }

    public static String checkInfoSourceVal(int infoSource) {
        String infoSourceType = "";
        switch (infoSource) {
            case 0:
                infoSourceType = "AMADEUS";
                break;
            case 3:
                infoSourceType = "GDSINVENTORY";
                break;
            case 5:
                infoSourceType = "GALILEO";
                break;
            case 7:
                infoSourceType = "FLYDUBAI";
                break;
            case 9:
                infoSourceType = "JAZEERA";
                break;
            case 10:
                infoSourceType = "FLYNAS";
                break;
            case 13:
                infoSourceType = "PYTON";
                break;
            case 15:
                infoSourceType = "SABRE";
                break;
            case 16:
                infoSourceType = "HITCHHICKER";
                break;
            case 17:
                infoSourceType = "AIRARABIA";
                break;
            default:
                infoSourceType = "";
                break;
        }
        return infoSourceType;
    }

    public static ArrayList<String> checkRefundTypeResult(String refundVal) {
        ArrayList<String> refundTypeArr = new ArrayList<>();
        try {
            String[] refundData = new String[3];
            if (refundVal.contains("/")) {
                refundData = refundVal.split("/");
                if (refundData.length > 2) {
                    refundTypeArr.add(refundData[0].toLowerCase());
                    refundTypeArr.add(refundData[1].toLowerCase());
                    refundTypeArr.add(refundData[2].toLowerCase());
                } else {
                    refundTypeArr.add(refundData[0].toLowerCase());
                    refundTypeArr.add(refundData[1].toLowerCase());
                }
            } else {
                refundTypeArr.add(refundVal.toLowerCase());
            }
        } catch (Exception e) {
        }
        return refundTypeArr;
    }

    public static String getDid(Activity activity) {
        String imeiNo = "";
        try {
            TelephonyManager tm = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm != null)
                imeiNo = tm.getDeviceId();
            if (imeiNo.equalsIgnoreCase("") || imeiNo.length() == 0)
                imeiNo = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
        }
        return imeiNo;
    }

    public static String getMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }
                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:", b));
                }
                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        boolean isIPv4 = sAddr.indexOf(':') < 0;
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%');
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                isInBackground = true;
            }
        }
        return isInBackground;
    }

    public static String encryptInputVal(String inputStr) {
        String encryptedStr = "";
        try {
            String key = Constants.KEY;
            String iv = Constants.IV;
            CryptLib cryptLib = new CryptLib();
            encryptedStr = cryptLib.encryptSimple(inputStr, key, iv);
            encryptedStr = encryptedStr.replaceAll("\\n", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedStr;
    }

    public static void convertedTimeFormat(Context context) {
        try {
            SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
            String timeZone = pref.getString(Constants.SERVER_TIME_MILLI, "");
            if (!timeZone.isEmpty()) {
                Long serverMilliSeconds = Long.parseLong(timeZone);
                Long localMilliSeconds = System.currentTimeMillis();
                Long resultMilli = localMilliSeconds - serverMilliSeconds;
                int diff = resultMilli.intValue();
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt(Constants.SERVER_TIME_DIFF, diff).apply();
            }
        } catch (Exception e) {
        }
    }

    public static String getConvertedTimeFormat(Context context, String currentTime) {
        String timeInMilli = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy h:mm:ss", Locale.ENGLISH);
            Date datetime = null;
            try {
                datetime = sdf.parse(currentTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+00"));
            String convertedTime = sdf.format(datetime);
            Utils.printMessage(TAG, "Converted time : "+convertedTime);
        } catch (Exception e) {
        }
        return timeInMilli;
    }
}