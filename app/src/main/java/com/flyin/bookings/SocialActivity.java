package com.flyin.bookings;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

public class SocialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        TextView socialFacebook = (TextView) findViewById(R.id.social_facebook);
        TextView socialTwitter = (TextView) findViewById(R.id.social_twitter);
        TextView socialGooglePlus = (TextView) findViewById(R.id.social_google_plus);
        TextView socialInstagram = (TextView) findViewById(R.id.social_instagram);
        TextView socialYoutube = (TextView) findViewById(R.id.social_youtube);
        TextView socialBlog = (TextView) findViewById(R.id.social_blog);
        LinearLayout facebookLayout = (LinearLayout) findViewById(R.id.facebook_layout);
        LinearLayout twitterLayout = (LinearLayout) findViewById(R.id.twitter_layout);
        LinearLayout googlePlusLayout = (LinearLayout) findViewById(R.id.google_plus_layout);
        LinearLayout instagramLayout = (LinearLayout) findViewById(R.id.instagram_layout);
        LinearLayout youtubeLayout = (LinearLayout) findViewById(R.id.youtube_layout);
        LinearLayout blogLayout = (LinearLayout) findViewById(R.id.blog_layout);
        if (Utils.isArabicLangSelected(SocialActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            socialFacebook.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            socialTwitter.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            socialGooglePlus.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            socialInstagram.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            socialYoutube.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            socialBlog.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface titleFace = Typeface.createFromAsset(getAssets(), fontText);
        socialFacebook.setTypeface(titleFace);
        socialTwitter.setTypeface(titleFace);
        socialGooglePlus.setTypeface(titleFace);
        socialInstagram.setTypeface(titleFace);
        socialYoutube.setTypeface(titleFace);
        socialBlog.setTypeface(titleFace);
        facebookLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://www.facebook.com/flyin.com"));
                startActivity(intent);
            }
        });
        twitterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://twitter.com/flyincom"));
                startActivity(intent);
            }
        });
        googlePlusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://plus.google.com/+flyin/posts"));
                startActivity(intent);
            }
        });
        instagramLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://www.instagram.com/flyincom"));
                startActivity(intent);
            }
        });
        youtubeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://www.youtube.com/user/FlyinVideos"));
                startActivity(intent);
            }
        });
        blogLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("http://blog.flyin.com/"));
                startActivity(intent);
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_social_titleBar);
        mTitleText.setTypeface(tf);
        backText.setTypeface(titleFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(SocialActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }
}
