package com.flyin.bookings;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.calendarlistview.library.DatePickerController;
import com.calendarlistview.library.DayPickerView;
import com.calendarlistview.library.SimpleMonthAdapter;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.Calendar;

public class CalendarDisplayActivity extends AppCompatActivity implements DatePickerController {
    private TextView checkInDateTxt = null;
    private TextView checkInDateValue = null;
    private TextView checkOutDateTxt = null;
    private TextView checkOutDateValue = null;
    private TextView singleTripDateValue = null;
    private static final String TAG = "CalendarDisplayActivity";
    private String singleTripSelectedDate = "";
    private String roundTripSelectedDepartureDate = "";
    private String roundTripSelectedReturnDate = "";
    private int singleDepartureDay = -1;
    private int singleDepartureMonth = -1;
    private int singleDepartureYear = -1;
    private int departureDay = -1;
    private int departureMonth = -1;
    private int departureYear = -1;
    private boolean isDepartureSelected = false;
    private int tripType;
    private boolean isHotelDateSelected = false;
    private String disabledDates = "";
    private boolean isArabicSelected = false;
    private ImageView departueSelectedIcon = null;
    private ImageView returnSelectedIcon = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_display_view);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        RelativeLayout singleTripLayout = (RelativeLayout) findViewById(R.id.singleTripLayout);
        LinearLayout roundTripLayout = (LinearLayout) findViewById(R.id.roundTripLayout);
        roundTripLayout.setVisibility(View.GONE);
        TextView singleTripDateTxt = (TextView) findViewById(R.id.singleTripDateTxt);
        singleTripDateValue = (TextView) findViewById(R.id.singleTripDateValue);
        checkInDateTxt = (TextView) findViewById(R.id.checkInDateTxt);
        checkInDateValue = (TextView) findViewById(R.id.checkInDateValue);
        checkOutDateTxt = (TextView) findViewById(R.id.checkOutDateTxt);
        checkOutDateValue = (TextView) findViewById(R.id.checkOutDateValue);
        TextView sundayText = (TextView) findViewById(R.id.sunday_text);
        TextView mondayText = (TextView) findViewById(R.id.monday_text);
        TextView tuesdayText = (TextView) findViewById(R.id.tuesday_text);
        TextView wednesdayText = (TextView) findViewById(R.id.wednesday_text);
        TextView thursdayText = (TextView) findViewById(R.id.thursday_text);
        TextView fridayText = (TextView) findViewById(R.id.friday_text);
        TextView saturdayText = (TextView) findViewById(R.id.saturday_text);
        departueSelectedIcon = (ImageView) findViewById(R.id.departueSelectedIcon);
        returnSelectedIcon = (ImageView) findViewById(R.id.returnSelectedIcon);
        departueSelectedIcon.setVisibility(View.VISIBLE);
        returnSelectedIcon.setVisibility(View.GONE);
        isArabicSelected = Utils.isArabicLangSelected(CalendarDisplayActivity.this);
        if (isArabicSelected) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface titleFace = Typeface.createFromAsset(getAssets(), fontTitle);
        singleTripDateTxt.setTypeface(titleFace);
        singleTripDateValue.setTypeface(titleFace);
        checkInDateTxt.setTypeface(titleFace);
        checkInDateValue.setTypeface(titleFace);
        checkOutDateTxt.setTypeface(titleFace);
        checkOutDateValue.setTypeface(titleFace);
        sundayText.setTypeface(titleFace);
        mondayText.setTypeface(titleFace);
        tuesdayText.setTypeface(titleFace);
        wednesdayText.setTypeface(titleFace);
        thursdayText.setTypeface(titleFace);
        fridayText.setTypeface(titleFace);
        saturdayText.setTypeface(titleFace);
        if (isArabicSelected) {
            singleTripDateTxt.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            checkInDateTxt.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            checkOutDateTxt.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            singleTripDateValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            checkInDateValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            checkOutDateValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
        }
        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey(Constants.TRIP_TYPE)) {
                tripType = b.getInt(Constants.TRIP_TYPE, 0);
            }
            if (b.containsKey(Constants.SELECTED_DATE)) {
                singleTripSelectedDate = b.getString(Constants.SELECTED_DATE, "");
            }
            if (b.containsKey(Constants.SELECTED_DEPARTURE_DATE)) {
                roundTripSelectedDepartureDate = b.getString(Constants.SELECTED_DEPARTURE_DATE, "");
            }
            if (b.containsKey(Constants.SELECTED_RETURN_DATE)) {
                roundTripSelectedReturnDate = b.getString(Constants.SELECTED_RETURN_DATE, "");
            }

            if (b.containsKey(Constants.IS_DEPARTURE_SELECTED)) {
                isDepartureSelected = b.getBoolean(Constants.IS_DEPARTURE_SELECTED, false);
            }
            if (b.containsKey(Constants.DISABLED_DATES)) {
                disabledDates = b.getString(Constants.DISABLED_DATES, "");
            }
            if (b.containsKey(Constants.IS_HOTEL_DATE_SELECTED)) {
                isHotelDateSelected = b.getBoolean(Constants.IS_HOTEL_DATE_SELECTED, false);
            }
        }
        DayPickerView dayPickerView = (DayPickerView) findViewById(R.id.pickerView);
        if (tripType == 1) {
            singleTripLayout.setVisibility(View.VISIBLE);
            roundTripLayout.setVisibility(View.GONE);
            dayPickerView.setController(this, singleTripSelectedDate, tripType, "", "", false, disabledDates, isArabicSelected, isHotelDateSelected);
        } else if (tripType == 2) {
            singleTripLayout.setVisibility(View.GONE);
            roundTripLayout.setVisibility(View.VISIBLE);
            if (!roundTripSelectedDepartureDate.isEmpty() || !roundTripSelectedDepartureDate.equalsIgnoreCase("")) {
                String convertedFormat = Utils.convertToCalendarHeaderFormat(roundTripSelectedDepartureDate, CalendarDisplayActivity.this);
                checkInDateValue.setText(convertedFormat);
                try {
                    String str[] = roundTripSelectedDepartureDate.split("-");
                    departureDay = Integer.parseInt(str[2]);
                    departureMonth = Integer.parseInt(str[1]);
                    departureYear = Integer.parseInt(str[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                departueSelectedIcon.setVisibility(View.GONE);
                returnSelectedIcon.setVisibility(View.VISIBLE);
            }
            if (!roundTripSelectedReturnDate.isEmpty() || !roundTripSelectedReturnDate.equalsIgnoreCase("")) {
                String convertedFormat = Utils.convertToCalendarHeaderFormat(roundTripSelectedReturnDate, CalendarDisplayActivity.this);
                checkOutDateValue.setText(convertedFormat);
            }
            dayPickerView.setController(this, singleTripSelectedDate, tripType, roundTripSelectedDepartureDate, roundTripSelectedReturnDate, isDepartureSelected, "", isArabicSelected, isHotelDateSelected);
        }
        if (!singleTripSelectedDate.isEmpty() || !singleTripSelectedDate.equalsIgnoreCase("")) {
            String convertedFormat = Utils.convertToCalendarHeaderFormat(singleTripSelectedDate, CalendarDisplayActivity.this);
            singleTripDateValue.setText(convertedFormat);
        }
        if (isHotelDateSelected) {
            checkInDateTxt.setText(R.string.label_hotel_search_check_in);
            checkOutDateTxt.setText(R.string.label_hotel_search_check_out);
        } else {
            checkInDateTxt.setText(R.string.departure_lbl);
            checkOutDateTxt.setText(R.string.return_lbl);
        }
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.calendar_actionbar).setVisibility(View.VISIBLE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_to_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_header);
        Button doneButton = (Button) mCustomView.findViewById(R.id.select_button);
        backText.setText(R.string.label_select_date);
        backText.setTypeface(textFace);
        doneButton.setTypeface(textFace);
        if (Utils.isArabicLangSelected(CalendarDisplayActivity.this)) {
            backText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            doneButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tripType == 1) {
                    if (singleDepartureDay == -1) {
                        displayErrorMessage(getString(R.string.label_err_dept_date_msg));
                        return;
                    }
                    Intent intent = new Intent();
                    intent.putExtra(Constants.SELECTED_DAY, singleDepartureDay);
                    intent.putExtra(Constants.SELECTED_MONTH, singleDepartureMonth);
                    intent.putExtra(Constants.SELECTED_YEAR, singleDepartureYear);
                    setResult(RESULT_OK, intent);
                    finish();
                } else if (tripType == 2) {
                    if (departureDay == -1) {
                        if (isHotelDateSelected) {
                            displayErrorMessage(getString(R.string.check_in_error));
                        } else {
                            displayErrorMessage(getString(R.string.label_err_dept_date_msg));
                        }
                        return;
                    }
                    if (singleDepartureDay == -1) {
                        if (isHotelDateSelected) {
                            displayErrorMessage(getString(R.string.check_out_error));
                        } else {
                            displayErrorMessage(getString(R.string.label_err_arr_date_msg));
                        }
                        return;
                    }
                    Intent intent = new Intent();
                    intent.putExtra(Constants.DEPARTURE_DAY, departureDay);
                    intent.putExtra(Constants.DEPARTURE_MONTH, departureMonth);
                    intent.putExtra(Constants.DEPARTURE_YEAR, departureYear);
                    intent.putExtra(Constants.RETURN_DAY, singleDepartureDay);
                    intent.putExtra(Constants.RETURN_MONTH, singleDepartureMonth);
                    intent.putExtra(Constants.RETURN_YEAR, singleDepartureYear);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(titleFace);
    }

    @Override
    public int getMaxYear() {
        Calendar calendar = Calendar.getInstance();
        return (calendar.get(Calendar.YEAR) + 1);
    }

    @Override
    public void onDayOfMonthSelected(int year, int month, int day, boolean isDepartureSelected) {
        if (tripType == 1) {
            String convertedFormat = Utils.convertToCalendarHeaderFormat(year + "-" + (month + 1) + "-" + day, CalendarDisplayActivity.this);
            singleTripDateValue.setText(convertedFormat);
            singleDepartureDay = day;
            singleDepartureMonth = month + 1;
            singleDepartureYear = year;
        } else if (tripType == 2) {
            if (isDepartureSelected) {
                String convertedFormat = Utils.convertToCalendarHeaderFormat(year + "-" + (month + 1) + "-" + day, CalendarDisplayActivity.this);
                checkInDateValue.setText(convertedFormat);
                checkInDateValue.setTextColor(Color.parseColor("#194e91"));
                checkInDateTxt.setTextColor(Color.parseColor("#194e91"));
                checkOutDateValue.setTextColor(Color.parseColor("#8e9aa3"));
                checkOutDateTxt.setTextColor(Color.parseColor("#8e9aa3"));
                departureDay = day;
                departureMonth = month + 1;
                departureYear = year;
                checkOutDateValue.setText(getString(R.string.pick_date_lbl));
                departueSelectedIcon.setVisibility(View.GONE);
                returnSelectedIcon.setVisibility(View.VISIBLE);
                singleDepartureDay = -1;
            } else {
                String convertedFormat = Utils.convertToCalendarHeaderFormat(year + "-" + (month + 1) + "-" + day, CalendarDisplayActivity.this);
                checkOutDateValue.setText(convertedFormat);
                checkOutDateValue.setTextColor(Color.parseColor("#194e91"));
                checkOutDateTxt.setTextColor(Color.parseColor("#194e91"));
                checkInDateValue.setTextColor(Color.parseColor("#8e9aa3"));
                checkInDateTxt.setTextColor(Color.parseColor("#8e9aa3"));
                singleDepartureDay = day;
                singleDepartureMonth = month + 1;
                singleDepartureYear = year;
                departueSelectedIcon.setVisibility(View.GONE);
                returnSelectedIcon.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onDateRangeSelected(SimpleMonthAdapter.SelectedDays<SimpleMonthAdapter.CalendarDay> selectedDays) {
        Utils.printMessage(TAG, "Date range selected " + selectedDays.getFirst().toString() + " --> " + selectedDays.getLast().toString());
    }

    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.setBackgroundResource(R.color.error_message_background);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }
}
