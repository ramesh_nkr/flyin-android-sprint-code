package com.flyin.bookings.listeners;

public interface TokenUpdateListener {
    public void dataResponse(String message, String callType);
}
