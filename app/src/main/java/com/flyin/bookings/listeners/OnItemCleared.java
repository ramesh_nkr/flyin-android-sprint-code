package com.flyin.bookings.listeners;

public interface OnItemCleared {

    public void onItemCleared(int position, String value);
}
