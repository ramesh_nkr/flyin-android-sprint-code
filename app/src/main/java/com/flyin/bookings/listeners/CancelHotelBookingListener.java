package com.flyin.bookings.listeners;

public interface CancelHotelBookingListener {

    public void onSelectedItemClick(int position, int type, String value);

    public void onSelectedListItem(int position, String value, String hotelId);
}
