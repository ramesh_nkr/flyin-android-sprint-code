package com.flyin.bookings.listeners;

public interface OnCustomItemSelectListener {

    public void onItemClick(int position);

    public void onSelectedItemClick(int position, int type);
}
