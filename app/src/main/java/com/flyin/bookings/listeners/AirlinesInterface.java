package com.flyin.bookings.listeners;

public interface AirlinesInterface<T> {
    void onSuccess(T response);

    void onFailed(String message);
}
