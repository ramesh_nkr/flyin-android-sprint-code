package com.flyin.bookings.listeners;

public interface TravellerItemNameListener {

    public void onNameItemClick(int position);
}
