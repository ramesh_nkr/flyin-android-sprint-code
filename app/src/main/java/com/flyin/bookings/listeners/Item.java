package com.flyin.bookings.listeners;

public interface Item {

    public boolean isSection();
}