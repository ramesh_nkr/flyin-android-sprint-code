package com.flyin.bookings.listeners;

import com.flyin.bookings.model.RoomCombinationObjectBean;

public interface RoomInformationListener {

    public void onItemClick(int position, int type, boolean isIncrement);

    public void onItemCancelClick(int position);

    public void onChildItemClick(int position, int tag, int value);

    public void onRoomItemClick(int position, int roomPosition);

    public void onShowRoomDetailsClick(int position);

    public void onShowMoreClick(int position, String hotelId, RoomCombinationObjectBean rcObj);
}
