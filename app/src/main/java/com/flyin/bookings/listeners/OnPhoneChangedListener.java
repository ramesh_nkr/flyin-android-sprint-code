package com.flyin.bookings.listeners;

public interface OnPhoneChangedListener {

    public void onPhoneChanged(String phone);
}
