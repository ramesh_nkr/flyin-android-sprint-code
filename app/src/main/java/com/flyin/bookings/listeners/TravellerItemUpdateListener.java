package com.flyin.bookings.listeners;

public interface TravellerItemUpdateListener {

    public void onItemClick(int position, int type, String value, String code, int selectedPosition, boolean isHijriChecked, boolean isIqamaSelected);
}
