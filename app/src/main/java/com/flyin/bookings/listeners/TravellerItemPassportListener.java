package com.flyin.bookings.listeners;

public interface TravellerItemPassportListener {
    public void onPassportItemClick(int position, String value, boolean isIqamaChecked);
}
