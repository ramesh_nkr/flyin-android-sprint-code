package com.flyin.bookings.listeners;

public interface TimeChangeListener {
    public void dataResponse(String message);
}
