package com.flyin.bookings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.model.RoomModel;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

@SuppressWarnings("ALL")
public class NearByMapFragment extends Fragment implements OnMapReadyCallback, AsyncTaskListener {
    private static final int GET_ACCESS_TOKEN = 20, GET_CITY = 21, SEARCH_HOTEL = 22, STATIC_DATA = 23;
    private ArrayList<HotelModel> hotelList = new ArrayList<>();
    private String accessTocken, huId;
    private int night_count, room_count;
    private HotelModel hotelModel;
    private ArrayList<String> adult_count_list;
    private ProgressBar progressbar;
    private GoogleMap googleMap;
    private SharedPreferences preferences;
    private TextView hotel_name, hotel_price, hotel_distance, close;
    private RatingBar hotel_star;
    private ArrayList<Marker> markers;
    private RelativeLayout bottomLayout, topLayout;
    private ImageView nearImage;
    private TextView errorText, errorDescriptionText;
    private LayoutInflater layoutInflater;
    private Typeface regularLight, regularBold;
    private ArrayList<String> huidlist;
    private TextView btn_book_hotel_map;
    private View transparent;
    private RelativeLayout errorView = null, loadingViewLayout;
    private TextView errorMessageText = null;
    private ImageView errorImage;
    private Button searchButton;
    private int adults, childs;
    private static final String TAG = "NearByMapFragment";
    private ArrayList<String> adultsCountList;
    private ArrayList<String> childCountList;
    private HashMap<String, ArrayList<String>> childCount;
    private String headerDate = "";
    private String finalPrice = "";
    private int _id = 1;
    private String randomNumber = "";
    private Activity mActivity;
    private String hotelCheckInDate, hotelCheckOutDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nearby_map, container, false);
        huidlist = new ArrayList<>();
        preferences = getActivity().getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        hotel_name = (TextView) view.findViewById(R.id.txt_hotel_title);
        hotel_distance = (TextView) view.findViewById(R.id.distance_from_location);
        nearImage = (ImageView) view.findViewById(R.id.img_hotel_thumbnail);
        hotel_star = (RatingBar) view.findViewById(R.id.ratingBar_hotel_map);
        hotel_price = (TextView) view.findViewById(R.id.txt_hotel_price);
        close = (TextView) view.findViewById(R.id.close);
        btn_book_hotel_map = (TextView) view.findViewById(R.id.btn_book_hotel_map);
        topLayout = (RelativeLayout) view.findViewById(R.id.map_top_layout);
        transparent = (ImageView) view.findViewById(R.id.transparent_image);
        bottomLayout = (RelativeLayout) view.findViewById(R.id.bottomLayout);
        String fontRegularBold = Constants.FONT_ROBOTO_BOLD;
        String fontRegularLight = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(getActivity())) {
            fontRegularBold = Constants.FONT_DROIDKUFI_BOLD;
            fontRegularLight = Constants.FONT_DROIDKUFI_REGULAR;
        }
        Typeface regularLight = Typeface.createFromAsset(getActivity().getAssets(), fontRegularLight);
        Typeface regularBold = Typeface.createFromAsset(getActivity().getAssets(), fontRegularBold);
        hotel_name.setTypeface(regularBold);
        hotel_distance.setTypeface(regularLight);
        close.setTypeface(regularBold);
        btn_book_hotel_map.setTypeface(regularBold);
        bottomLayout.setVisibility(View.GONE);
        hotel_distance.setVisibility(View.GONE);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        hotelModel = getArguments().getParcelable("model");
        adults = getArguments().getInt("adults", 0);
        childs = getArguments().getInt("childs", 0);
        adultsCountList = getArguments().getStringArrayList("adults_list");
        childCountList = getArguments().getStringArrayList("childs_list");
        headerDate = getArguments().getString("header_date");
        hotelCheckInDate = getArguments().getString("hotelCheckInDate", "");
        hotelCheckOutDate = getArguments().getString("hotelCheckOutDate", "");
        Utils.printMessage(TAG, "In : " + hotelCheckInDate + " out : " + hotelCheckOutDate);
        childCount = (HashMap<String, ArrayList<String>>) getArguments().getSerializable("children_list");
        randomNumber = getArguments().getString(Constants.BOOKING_RANDOM_NUMBER, "");
        night_count = Integer.parseInt(hotelModel.getDur());
        room_count = Integer.parseInt(hotelModel.getRoom_count());
        huId = hotelModel.getHuid();
        adult_count_list = hotelModel.getAdultcountlist();
        errorView = (RelativeLayout) view.findViewById(R.id.errorView);
        loadingViewLayout = (RelativeLayout) view.findViewById(R.id.loading_view_layout);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) view.findViewById(R.id.errorMessageText);
        mapFragment.getMapAsync(NearByMapFragment.this);
        if (Utils.isConnectingToInternet(getActivity())) {
            String json = buildJson();
            if (json != null) {
                showLoading();
                new HTTPAsync(getActivity(), NearByMapFragment.this, Constants.HOTEL_SEARCH_URL, "", json,
                        SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
            }
        } else {
            try {
                layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(regularLight);
                errorDescriptionText.setTypeface(regularLight);
                searchButton.setTypeface(regularLight);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isInternetPresent = Utils.isConnectingToInternet(getActivity());
                        if (isInternetPresent) {
                            loadingViewLayout.removeView(errorView);
                            String json = buildJson();
                            if (json != null) {
                                showLoading();
                                new HTTPAsync(getActivity(), NearByMapFragment.this, Constants.HOTEL_SEARCH_URL, "",
                                        json, SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
                            }
                        }
                    }
                });
                loadErrorType(Constants.NETWORK_ERROR);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        btn_book_hotel_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                ArrayList<RoomModel> tempHotelList = new ArrayList<>();
                for (int i = 0; i < hotelList.get(0).getRoomlist().size(); i++) {
                    String roomPrice = Utils.convertPriceToUserSelectionHotel(Double.parseDouble(hotelList.get(0).getRoomlist().get(i).getPrice()),
                            hotelList.get(0).getRoomlist().get(i).getCurrency(), getActivity());
                    if (finalPrice.equalsIgnoreCase(roomPrice)) {
                        tempHotelList.add(hotelList.get(0).getRoomlist().get(i));
                        break;
                    }
                }
                hotelList.get(0).getRoomlist().clear();
                hotelList.get(0).setRoomlist(tempHotelList);
                String modelString = gson.toJson(hotelList.get(0));
                Fragment fragment = new HotelTravelerDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("model", modelString);
                bundle.putParcelable("modelList", hotelModel);
                bundle.putInt("adult_count", adults);
                bundle.putInt("child_count", childs);
                bundle.putStringArrayList("adults_list", adultsCountList);
                bundle.putStringArrayList("childs_list", childCountList);
                bundle.putSerializable("children_list", childCount);
                bundle.putString("header_date", headerDate);
                bundle.putString("payPrice", finalPrice);
                bundle.putString("gettingDataFrom", "detailsFragment");
                bundle.putString("isMapFragment", "fromMapFrag");
                bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                fragment.setArguments(bundle);
                ((MainSearchActivity) getActivity()).pushFragment(fragment);

            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomLayout.setVisibility(View.GONE);
            }
        });
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        topLayout.getLayoutParams().height = height - height / 3;
        topLayout.getLayoutParams().width = width;
        transparent.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        HotelDetailFragment.scollview.requestDisallowInterceptTouchEvent(true);
                        return false;
                    case MotionEvent.ACTION_UP:
                        HotelDetailFragment.scollview.requestDisallowInterceptTouchEvent(false);
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        HotelDetailFragment.scollview.requestDisallowInterceptTouchEvent(true);
                        return false;
                    default:
                        return true;
                }
            }
        });
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Bundle bundle = getArguments();
        if (bundle == null) {
            Utils.printMessage(TAG, "No lat longs in bundle found");
            return;
        }
        this.googleMap = googleMap;
    }

    public Bitmap addMarker(HotelModel model) {
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap bmp = Bitmap.createBitmap(180, 180, conf);
        Canvas canvas1 = new Canvas(bmp);
        Paint color = new Paint();
        int scaledSize = mActivity.getResources().getDimensionPixelSize(R.dimen.flight_duration_text);
        int priceTextScale = mActivity.getResources().getDimensionPixelSize(R.dimen.flight_moreDetails);
        int imageScaledSize = mActivity.getResources().getDimensionPixelSize(R.dimen.margin_size);
        int firstxPosScaleSize = mActivity.getResources().getDimensionPixelSize(R.dimen.signInPage_view_gap);
        int firstyPosScaleSize = mActivity.getResources().getDimensionPixelSize(R.dimen.contact_email_margin);
        int xPosScaleSize = mActivity.getResources().getDimensionPixelSize(R.dimen.userDetails_margin);
        int yPosScaleSize = mActivity.getResources().getDimensionPixelSize(R.dimen.flag_height);
        int textYScaledSize = mActivity.getResources().getDimensionPixelSize(R.dimen.search_margin);
        color.setTextSize(priceTextScale);
        color.setColor(Color.WHITE);
        Paint color2 = new Paint();
        color2.setTextSize(scaledSize);
        color2.setColor(Color.WHITE);
        if (model.getStar().equalsIgnoreCase("0")) {
            canvas1.drawBitmap(BitmapFactory.decodeResource(mActivity.getResources(), R.drawable.callout2), 0, 0, color);
            canvas1.drawText(getString(R.string.label_star_unrate), xPosScaleSize, textYScaledSize, color2);
            canvas1.drawText(finalPrice, xPosScaleSize, yPosScaleSize, color);
        } else {
            canvas1.drawBitmap(BitmapFactory.decodeResource(mActivity.getResources(), R.drawable.callout2), 0, 0, color);
            canvas1.drawText(model.getStar(), imageScaledSize, imageScaledSize, color2);
            canvas1.drawBitmap(BitmapFactory.decodeResource(mActivity.getResources(), R.drawable.smallstar), firstxPosScaleSize, firstyPosScaleSize, color);
            canvas1.drawText(finalPrice, xPosScaleSize, yPosScaleSize, color);
        }
        return bmp;
    }

    public String buildJson() {
        final SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
        if (tokenTime == -1) {
            getTokenFromServer();
        } else {
            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
            if (diff > Long.parseLong(expireIn)) {
                getTokenFromServer();
            } else {
                accessTocken = preferences.getString(Constants.ACCESS_TOKEN, "0");
            }
        }
        if (huId != null) {
            JSONObject obj = new JSONObject();
            String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
            String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
            String timeStamp = currentDateFormat + "T" + currentTimeFormat;
            try {
                SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                String userSelectedCurr = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
                String groupType = null;
                if (pref.getBoolean(Constants.isFanClub, false)) {
                    groupType = "ALHILAL";
                }
                JSONObject source = new JSONObject();
                source = Utils.getHotelSearchAppSettingData(source, getActivity());
                source.put("clientId", Constants.CLIENT_ID);
                source.put("clientSecret", Constants.CLIENT_SECRET);
                source.put("paxNationality", Utils.getUserCountry(getActivity()));
                source.put("echoToken", randomNumber);
                source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
                source.put("accessToken", Utils.getRequestAccessToken(mActivity));
                source.put("timeStamp", timeStamp);
                source.put("currency", userSelectedCurr);
                source.put("groupType", groupType);
                JSONObject sourceRQ = new JSONObject();
                sourceRQ.put("source", source);
                JSONObject sc = new JSONObject();
                sc.put("ad", Utils.formatDateToServerDateFormat(hotelModel.getCheck_in_date()));
                sc.put("dur", night_count);
                sc.put("uids", huId);
                sc.put("nbh", "NearByHotels");
                sc.put("skm", "false");
                sourceRQ.put("sc", sc);
                JSONObject rms = new JSONObject();
                JSONArray rm = new JSONArray();
                HashMap<String, ArrayList<String>> child_count_list = hotelModel.getChild_count_list();
                int _id = 1;
                int noOfAdults = 0;
                int noOfChildrens = 0;
                if (room_count == 1) {
                    noOfAdults = Integer.parseInt(adult_count_list.get(0));
                    Utils.printMessage(TAG, "adults count::" + noOfAdults);
                    noOfChildrens = 0;
                    if (child_count_list.get("room" + 1) != null) {
                        noOfChildrens = child_count_list.get("room" + 1).size();
                    }
                } else {
                    for (int i = 0; i < room_count; i++) {
                        Utils.printMessage(TAG, "adult_count_list.get(i)::" + adult_count_list.get(i));
                        //Utils.printMessage(TAG, "child_count_list::" + child_count_list.get("room" + (i + 1)));
                        int tempChildCount = 0;
                        if (child_count_list != null) {
                            if (child_count_list.get("room" + (i + 1)) != null) {
                                tempChildCount = child_count_list.get("room" + (i + 1)).size();
                            }
                        }
                        int occupancy = Integer.parseInt(adult_count_list.get(i)) + tempChildCount;
                        if (noOfAdults < occupancy) {
                            noOfAdults = occupancy;
                        }
                    }
                }
                for (int i = 1; i <= room_count; i++) {
                    if (adult_count_list.size() > 0) {
                        JSONObject rooms = new JSONObject();
                        JSONArray pax = new JSONArray();
                        int adult_count = Integer.parseInt(adult_count_list.get(i - 1));
                        for (int j = 0; j < noOfAdults; j++) {
                            JSONObject age_obj = new JSONObject();
                            age_obj.put("age", 45);
                            age_obj.put("id", _id);
                            pax.put(age_obj);
                            _id++;
                        }
                        rooms.put("na", noOfAdults);
                        rooms.put("nc", noOfChildrens);
                        if (child_count_list != null) {
                            ArrayList<String> list = child_count_list.get("room" + i);
                            for (int j = 0; j < noOfChildrens; j++) {
                                JSONObject age_obj = new JSONObject();
                                age_obj.put("age", list.get(j));
                                age_obj.put("id", _id);
                                pax.put(age_obj);
                                _id++;
                            }
                        }
                        rooms.put("pax", pax);
                        rm.put(rooms);
                    }
                }
                rms.put("rm", rm);
                sourceRQ.put("rms", rms);
                obj.put("searchRQ", sourceRQ);
            } catch (Exception e) {
                Utils.printMessage(TAG, "error" + e.getMessage());
            }
            Utils.printMessage(TAG, "json" + obj.toString());
            return obj.toString();
        } else {
        }
        return null;
    }

    private void getTokenFromServer() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(getActivity()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(getActivity(), NearByMapFragment.this, Constants.CLIENT_AUTHENTICATION, "",
                obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            Utils.printMessage(TAG, "getActvity not null");
        }
        if (mActivity != null) {
            Utils.printMessage(TAG, "mActivity not null");
        }
    }

    @Override
    public void onTaskComplete(String data, final int serviceType) {
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            try {
                layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(regularLight);
                errorDescriptionText.setTypeface(regularLight);
                searchButton.setTypeface(regularLight);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isInternetPresent = Utils.isConnectingToInternet(getActivity());
                        if (isInternetPresent) {
                            loadingViewLayout.removeView(errorView);
                            if (serviceType == SEARCH_HOTEL) {
                                String json = buildJson();
                                if (json != null) {
                                    showLoading();
                                    new HTTPAsync(getActivity(), NearByMapFragment.this, Constants.HOTEL_SEARCH_URL,
                                            "", json, SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
                                }
                            } else if (serviceType == GET_ACCESS_TOKEN)
                                getTokenFromServer();
                            else if (serviceType == STATIC_DATA) {
                                String jsn = makeJson();
                                if (jsn != null) {
                                    new HTTPAsync(getActivity(), NearByMapFragment.this, Constants.NEAR_MAP_SEARCH_URL,
                                            Constants.HOTEL_STATIC_CONTENT, jsn, STATIC_DATA, HTTPAsync.METHOD_POST).execute();
                                }
                            }
                        }
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            } catch (Exception e) {
            }
        } else {
            if (serviceType == GET_ACCESS_TOKEN) {
                JSONObject obj;
                try {
                    obj = new JSONObject(data);
                    String accessToken = obj.getString("accessToken");
                    String expireIn = obj.getString("expireIn");
                    String refreshToken = obj.getString("refreshToken");
                    long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                    SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Constants.ACCESS_TOKEN, accessToken);
                    editor.putString(Constants.EXPIRE_IN, expireIn);
                    editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                    editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                    editor.commit();
                    buildJson();
                } catch (JSONException e) {
                }
            } else if (serviceType == SEARCH_HOTEL) {
                parsedaa(data);
                Utils.printMessage(TAG, "DATA" + data);
                String jsn = makeJson();
                if (jsn != null) {
                    new HTTPAsync(getActivity(), NearByMapFragment.this, Constants.HOTEL_INFO_RQ_URL, "", jsn,
                            STATIC_DATA, HTTPAsync.METHOD_POST).execute();
                }
            } else if (serviceType == STATIC_DATA) {
                try {
                    ArrayList<Integer> errorlist = new ArrayList<>();
                    JSONArray arr = new JSONArray(data);
                    for (int i = 0; i < arr.length(); i++) {
                        HotelModel model = hotelList.get(i);
                        JSONObject obj = arr.optJSONObject(i);
                        String error = obj.optString("error");
                        if (error != null && error.equalsIgnoreCase("data not found")) {
                            errorlist.add(i);
                            continue;
                        }
                        model.setCheck_in_date(hotelCheckInDate);
                        model.setCheck_out_date(hotelCheckOutDate);
                        JSONObject hotel = obj.optJSONObject("hotel");
                        JSONObject bacickInfo = hotel.optJSONObject("basicInfo");
                        JSONObject geocode = bacickInfo.optJSONObject("geoCode");
                        model.setLatitude(geocode.optString("latitude"));
                        model.setLongitude(geocode.optString("longitude"));
                        JSONObject distric = bacickInfo.optJSONObject("district");
                        JSONObject city = bacickInfo.optJSONObject("city");
                        model.setDistric(distric.getString("name"));
                        model.setCity(city.optString("name"));
                        model.setCityId(city.optString("id"));
                        JSONObject hotelType = bacickInfo.optJSONObject("hotelType");
                        Iterator<String> hotel_key = hotelType.keys();
                        String hotel_type = "";
                        while (hotel_key.hasNext()) {
                            String key = hotel_key.next();
                            try {
                                if (hotel_type.length() > 0) {
                                    hotel_type = hotel_type + ",";
                                }
                                hotel_type = hotel_type + hotelType.getString(key);
                            } catch (JSONException e) {
                            }
                        }
                        model.setRoom_count("" + room_count);
                        model.setHotel_type(hotel_type);
                        JSONObject country = bacickInfo.optJSONObject("country");
                        model.setCountry(country.optString("name"));
                        model.setImage(bacickInfo.optString("mainImage"));
                        model.setStar(bacickInfo.optString("starRating"));
                        model.setFullAddress(bacickInfo.optString("addressText"));
                        model.setHna(bacickInfo.optString("name"));
                        JSONObject imageObj = hotel.optJSONObject("images");
                        JSONArray imageArr = imageObj.optJSONArray("others");
                        String[] imagelist = new String[imageArr.length()];
                        for (int j = 0; j < imageArr.length(); j++) {
                            imagelist[j] = imageArr.getString(j);
                        }
                        model.setImagelist(imagelist);
                        JSONObject policy = hotel.optJSONObject("policy");
                        String cancel_policy = policy.optString("cancellationPolicy");
                        if (cancel_policy != null) {
                            model.setCancel_policy(cancel_policy);
                        }
                        String child_policy = policy.optString("childPolicy");
                        if (child_policy != null) {
                            model.setChild_policy(child_policy);
                        }
                        String pet_policy = policy.optString("petPolicy");
                        if (pet_policy != null) {
                            model.setPet(pet_policy);
                        }
                        String check_out = policy.optString("check_out");
                        if (check_out != null) {
                            model.setCheck_out(check_out);
                        }
                        String check_in = policy.optString("check_in");
                        if (check_in != null) {
                            model.setCheck_in(check_in);
                        }
                        JSONObject description = hotel.optJSONObject("descriptions");
                        String lobby = description.optString("lobby");
                        if (lobby != null) {
                            model.setLobby(lobby);
                        }
                        String exterior = description.optString("exterior");
                        if (exterior != null) {
                            model.setExterior(exterior);
                        }
                        String rooms = description.optString("aboutHotel");
                        if (rooms != null) {
                            model.setRooms(rooms);
                        }
                        JSONObject facilities = hotel.optJSONObject("facilities");
                        JSONObject activity = facilities.optJSONObject("activities");
                        int k = 0;
                        if (activity != null) {
                            Iterator<String> activity_key = activity.keys();
                            String[] activitylist = new String[activity.length()];
                            while (activity_key.hasNext()) {
                                String key = activity_key.next();
                                try {
                                    String value = activity.getString(key);
                                    activitylist[k] = value;
                                } catch (JSONException e) {
                                }
                                k++;
                            }
                            model.setActivity(activitylist);
                        }
                        JSONObject service = facilities.optJSONObject("services");
                        if (service != null) {
                            Iterator<String> service_key = service.keys();
                            String[] servicelist = new String[service.length()];
                            k = 0;
                            while (service_key.hasNext()) {
                                String key = service_key.next();
                                try {
                                    String value = service.getString(key);
                                    servicelist[k] = value;
                                } catch (JSONException e) {
                                }
                                k++;
                            }
                            model.setService(servicelist);
                        }
                        JSONObject general = facilities.optJSONObject("activities");
                        if (general != null) {
                            Iterator<String> general_key = general.keys();
                            String[] generallist = new String[general.length()];
                            k = 0;
                            while (general_key.hasNext()) {
                                String key = general_key.next();
                                try {
                                    String value = general.getString(key);
                                    generallist[k] = value;
                                } catch (JSONException e) {
                                }
                                k++;
                            }
                            model.setGeneral(generallist);
                        }
                        hotelList.set(i, model);
                    }
                    loadingViewLayout.removeAllViews();
                    Utils.printMessage(TAG, "error size;" + errorlist.size());
                    for (int j = 0; j < errorlist.size(); j++) {
                        Utils.printMessage(TAG, "error position;" + errorlist.get(j));
                        hotelList.remove(errorlist.get(j) - j);
                    }
                    errorlist.clear();
                    if (hotelList.size() > 0) {
                        setNearHotel(hotelList.get(0));
                        callMarker();
                    } else {
                    }
                } catch (JSONException e) {
                    Utils.printMessage(TAG, "error" + e.getMessage());
                } catch (Exception e) {
                    Utils.printMessage(TAG, "error" + e.getMessage());
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
    }

    public void parsedaa(String data) {
        hotelList.clear();
        huidlist.clear();
        try {
            JSONObject obj = new JSONObject(data);
            JSONObject searchRSObj = obj.optJSONObject("searchRS");
            if (searchRSObj.has("err")) {
                layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(regularLight);
                errorDescriptionText.setTypeface(regularLight);
                searchButton.setTypeface(regularLight);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getActivity().onBackPressed();
                    }
                });
                loadErrorType(Constants.SELECTED_HOTEL_NOT_FOUND_ERROR);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
                return;
            }
            JSONArray arr = searchRSObj.getJSONArray("hotel");
            for (int i = 0; i < arr.length(); i++) {
                JSONObject rcObj = arr.optJSONObject(i);
                try {
                    JSONArray hotelRoomArr = rcObj.getJSONArray("rc");
                    for (int k = 0; k < hotelRoomArr.length(); k++) {
                        JSONArray roomJsonArray = hotelRoomArr.getJSONObject(k).getJSONArray("room");
                        _id = 1;
                        for (int j = 0; j < roomJsonArray.length(); j++) {
                            roomJsonArray.getJSONObject(j).put("id", "1");
                            roomJsonArray.getJSONObject(j).put("lp", setLP());
                            roomJsonArray.getJSONObject(j).put("ocp", getPAXOBject(j));
                        }
                    }
                    JSONObject rmsObj = rcObj.getJSONObject("rms");
                    JSONArray hrmArr = rmsObj.getJSONArray("hrm");
                    for (int j = 0; j < hrmArr.length(); j++) {
                        JSONObject roomJsonObj = hrmArr.getJSONObject(j).getJSONObject("room");
                        _id = 1;
                        roomJsonObj.put("id", "1");
                        roomJsonObj.put("lp", setLP());
                        roomJsonObj.put("ocp", getPAXOBject(0));
                    }
                } catch (Exception e) {
                    Utils.printMessage(TAG, "ERROR :: " + e.getMessage());
                }
                JSONArray rcArr = rcObj.getJSONArray("rc");
                HotelModel model = null;
                ArrayList<RoomModel> roomlist = new ArrayList<>();
                for (int j = 0; j < rcArr.length(); j++) {
                    model = new HotelModel();
                    model.setHotelJson(rcObj.toString());
                    JSONObject rObj = rcArr.optJSONObject(j);
                    model.setDur(rObj.optString("dur"));
                    JSONObject pObj = rObj.optJSONObject("p");
                    model.setP(pObj.optString("val"));
                    model.setCur(pObj.optString("cur"));
                    JSONObject wdObj = rObj.optJSONObject("wdp");
                    model.setWdp(wdObj.optString("val"));
                    JSONObject htObj = rObj.optJSONObject("ht");
                    model.setHotelTypeValue(htObj.toString());
                    model.setHuid(htObj.optString("uid"));
                    JSONArray roomArray = rObj.getJSONArray("room");
                    for (int k = 0; k < roomArray.length(); k++) {
                        RoomModel roomModel = new RoomModel();
                        JSONObject roomObj = roomArray.optJSONObject(k);
                        roomModel.setNight_count(roomObj.optString("dur"));
                        JSONObject pRoomObj = roomObj.optJSONObject("p");
                        roomModel.setPrice(pRoomObj.optString("val"));
                        roomModel.setCurrency(pRoomObj.optString("cur"));
                        JSONObject wdpRoomObj = roomObj.optJSONObject("wdp");
                        roomModel.setWdp(wdpRoomObj.optString("val"));
                        roomModel.setBrakfast(roomObj.optString("mn"));
                        roomModel.setRoom_type(roomObj.optString("rn"));
                        JSONObject piRoomObj = roomObj.optJSONObject("pi");
                        roomModel.setFree_cancel_date(piRoomObj.optString("FreeCancellationDate"));
                        roomlist.add(roomModel);
                    }
                    model.setRoomlist(roomlist);
                    hotelList.add(model);
                }
                huidlist.add(model.getHuid());
            }
        } catch (JSONException e) {
        } catch (Exception ex) {
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public String makeJson() {
        String result = null;
        String accessTocken = preferences.getString(Constants.ACCESS_TOKEN, "0");
        String language = preferences.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
        JSONObject source = new JSONObject();
        JSONObject crt = new JSONObject();
        JSONObject hcp = new JSONObject();
        JSONObject finaldata = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        try {
            source.put("device", Constants.DEVICE_TYPE);
            source.put("clientId", Constants.CLIENT_ID);
            source.put("clientSecret", Constants.CLIENT_SECRET);
            source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
            source.put("accessToken", Utils.getRequestAccessToken(mActivity));
            source.put("timeStamp", timeStamp);
            JSONArray mJSONArray = new JSONArray(huidlist);
            Utils.printMessage(TAG, "mJSONArray" + mJSONArray.toString());
            crt.put("language", language);
            crt.put("huid", mJSONArray);
            crt.put("pt", "B|F");
            hcp.put("source", source);
            hcp.put("crt", crt);
            finaldata.put("hcp", hcp);
            return finaldata.toString();
        } catch (JSONException e) {
        }
        return result;
    }

    public void callMarker() {
        if (hotelList != null) {
            markers = new ArrayList<>();
            HotelModel hotelModel = hotelList.get(0);
            Marker marker = googleMap
                    .addMarker(new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(hotelModel.getLatitude()),
                                    Double.parseDouble(hotelModel.getLongitude())))
                            .icon(BitmapDescriptorFactory.fromBitmap(addMarker(hotelModel)))
                            .anchor(0.5f, 1));
            markers.add(marker);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(markers.get(0).getPosition(), 17));
            //zoomTofitMarkersToScreen();
        }
    }

    public void zoomTofitMarkersToScreen() {
        Utils.printMessage(TAG, "zoomTofitMarkersToScreen()");
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int padding = 8;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        googleMap.animateCamera(cu);
    }

    public void setNearHotel(final HotelModel model) {
        bottomLayout.setVisibility(View.VISIBLE);
        ViewTreeObserver vto = nearImage.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                nearImage.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = nearImage.getMeasuredHeight();
                int finalWidth = nearImage.getMeasuredWidth();
                String url = Constants.IMAGE_SCALING_URL + finalWidth + "x" + finalHeight + Constants.IMAGE_QUALITY + "" + model.getImage();
                Glide.with(mActivity).load(url).placeholder(R.drawable.imagethumb).into(nearImage);
                return true;
            }
        });
        hotel_name.setText(model.getHna());
        hotel_star.setRating(Integer.parseInt(model.getStar()));
        String selectedPrice = "";
        try {
            JSONObject mapRoomObj = new JSONObject(model.getHotelJson());
            JSONArray rcRoomArr = mapRoomObj.getJSONArray("rc");
            JSONObject roomObj = rcRoomArr.getJSONObject(0);
            model.setMapRoomJson(roomObj.toString());
            selectedPrice = Utils.convertPriceToUserSelectionHotel(Double.parseDouble(roomObj.getJSONObject("p").getString("val")), roomObj.getJSONObject("p").getString("cur"), getActivity());
            Utils.printMessage(TAG, "Model :: " + roomObj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        hotel_price.setText(selectedPrice);
        finalPrice = selectedPrice;
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(mActivity.getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_network_error_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(mActivity.getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_no_results_found_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_search_text));
                break;
            case Constants.SELECTED_HOTEL_NOT_FOUND_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.selected_hotel_not_available_error));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            default:
                break;
        }
    }

    private void showLoading() {
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView waitText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setTypeface(regularBold);
        waitText.setTypeface(regularLight);
        loadingText.setVisibility(View.GONE);
        waitText.setText(getActivity().getResources().getString(R.string.label_just_a_moment));
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.removeAllViews();
        loadingViewLayout.addView(loadingView);
    }

    public JSONObject setLP() {
        JSONObject object = new JSONObject();
        try {
            object.put("fname", "lalit");
            object.put("lname", "sharma");
            object.put("age", "28");
            object.put("email", "lalitsharma1607@gmail.com");
            object.put("ph", "9015376344");
            return object;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    private JSONObject setOCP(JSONObject hotelObj, int roomPos) {
        try {
            hotelObj.getJSONArray("room").getJSONObject(roomPos).put("ocp", getPAXOBject(roomPos));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hotelObj;
    }

    private JSONObject getPAXOBject(int roomPos) {
        try {
            ArrayList<String> adult_count_list = hotelModel.getAdultcountlist();
            HashMap<String, ArrayList<String>> child_count_list = hotelModel.getChild_count_list();
            JSONObject rooms = new JSONObject();
            JSONArray pax = new JSONArray();
            int adult_count = Integer.parseInt(adult_count_list.get(roomPos));
            for (int j = 0; j < adult_count; j++) {
                JSONObject age_obj = new JSONObject();
                age_obj.put("age", 45);
                age_obj.put("id", _id);
                pax.put(age_obj);
                _id++;
            }
            rooms.put("na", adult_count);
            if (child_count_list != null) {
                ArrayList<String> list = child_count_list.get("room" + (roomPos + 1));
                if (list != null) {
                    for (int j = 0; j < list.size(); j++) {
                        JSONObject age_obj = new JSONObject();
                        age_obj.put("age", list.get(j));
                        age_obj.put("id", _id);
                        pax.put(age_obj);
                        _id++;
                    }
                    rooms.put("nc", list.size());
                }
            }
            rooms.put("pax", pax);
            return rooms;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
