package com.flyin.bookings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.FlightsDataBean;
import com.flyin.bookings.model.LayoverDataBean;
import com.flyin.bookings.model.PassengersBean;
import com.flyin.bookings.model.PriceBean;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class FlightTripDetailsActivity extends AppCompatActivity implements AsyncTaskListener {
    private TextView airlineTicketCost, taxFare, numberOfTickets, airFareTotal, errorText, errorDescriptionText;
    private Typeface titleFace, textFace, regularLight;
    private LinearLayout flightInformationLayout, priceInformationHideLayout, passengerInfoHideLayout, couponLayout;
    private ImageView priceInfoDownImage, passengerDetailsImage;
    private ArrayList<FlightsDataBean> tripFlightsArrayList = null;
    private ArrayList<PassengersBean> passengersBeanArrayList = null;
    private PriceBean priceBean;
    private static final String TAG = "FlightTripDetailsActivity";
    private LayoutInflater inflater;
    private String journeyType = "";
    private String passengerMail = "";
    private boolean isProfileDataExpanded = true;
    private boolean isPriceDetailsExpanded = true;
    private int passengerCount = 0;
    private int numberOfPassengers = 0;
    private String refundStatus = "";
    private int multiTripStopCount2 = 0;
    private int multiTripStopCount3 = 0;
    private String taskType = "";
    private static final int GET_CANCEL_RESPONSE = 1;
    private static final int GET_AMEND_RESPONSE = 2;
    private boolean isInternetPresent = false;
    private View loadingView, couponView;
    private TextView numberOfCoupon;
    private ImageView errorImage;
    private Button searchButton;
    private RelativeLayout loadingViewLayout;
    private String booingIdNumber = "";
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private MyReceiver myReceiver;
    private IntentFilter intentFilter;
    private boolean isArabicLang;
    private String previousTime = "";
    private String departureAirportCode = "";
    private String arrivalAirportCode = "";
    private String firstDepartureCode = "";
    private String firstArrivalCode = "";
    private String secondDepartureCode = "";
    private String secondArrivalCode = "";
    private String thirdDepartureCode = "";
    private String thirdArrivalCode = "";
    private String firstTripDetails = "";
    private String secondTripDetails = "";
    private String thirdTripDetails = "";
    private int firstLegOrder = 0;
    private int secondLegOrder = 0;
    private int thirdLegOrder = 0;
    private int forwardLegOrder = 0;
    private String legOrder = "";
    private String journeyOrder = "";
    private String previousAirport = "";
    private HashMap<String, FlightAirportName> airportNamesMap = new HashMap<>();
    private HashMap<String, String> airLineNamesMap = new HashMap<>();
    private LinearLayout qitafDiscountLayout = null;
    private TextView qitafDiscountValue = null;
    private ArrayList<String> airlinePnrArr = new ArrayList<>();
    private LinearLayout redeemedRewardsLayout = null;
    private TextView redeemedRewardsValue = null;
    private ArrayList<String> fareTypeArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_confirmation);
        isArabicLang = false;
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        String fontMedium = Constants.FONT_ROBOTO_MEDIUM;
        String fontLight = Constants.FONT_ROBOTO_LIGHT;
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        tripFlightsArrayList = new ArrayList<>();
        passengersBeanArrayList = new ArrayList<>();
        TextView bookConfirmationText = (TextView) findViewById(R.id.booking_confirmation_text);
        TextView thankYouText = (TextView) findViewById(R.id.thank_you_text);
        TextView bookingConfirmedText = (TextView) findViewById(R.id.booking_confirmed);
        TextView airlinePnrNumberText = (TextView) findViewById(R.id.airline_pnr_number_text);
        TextView airlinePnrNumber = (TextView) findViewById(R.id.airline_pnr_number);
        TextView tripIdText = (TextView) findViewById(R.id.trip_id_text);
        TextView tripIdNumber = (TextView) findViewById(R.id.trip_id);
        TextView dateOfBookingText = (TextView) findViewById(R.id.date_of_booking_text);
        TextView dateOfBooking = (TextView) findViewById(R.id.date_of_booking);
        TextView passengerDetailsText = (TextView) findViewById(R.id.passenger_details_text);
        TextView passengerInformationText = (TextView) findViewById(R.id.price_information_text);
        TextView airlineTicketCostText = (TextView) findViewById(R.id.airline_ticket_cost_text);
        airlineTicketCost = (TextView) findViewById(R.id.airline_ticket_cost);
        TextView taxFareText = (TextView) findViewById(R.id.taxes_fare_text);
        taxFare = (TextView) findViewById(R.id.taxes_fare_number);
        TextView numberOfTicketsText = (TextView) findViewById(R.id.number_of_tickets_text);
        numberOfTickets = (TextView) findViewById(R.id.number_of_tickets);
        TextView airFareTotalText = (TextView) findViewById(R.id.airfare_subtotal_text);
        airFareTotal = (TextView) findViewById(R.id.airfare_subtotal);
        TextView cancelBookingText = (TextView) findViewById(R.id.cancel_booking_text);
        TextView amendBookingText = (TextView) findViewById(R.id.amend_booking_text);
        flightInformationLayout = (LinearLayout) findViewById(R.id.flights_information);
        LinearLayout priceInformationLayout = (LinearLayout) findViewById(R.id.price_information_layout);
        LinearLayout passengerDetailsLayout = (LinearLayout) findViewById(R.id.passenger_details_layout);
        priceInformationHideLayout = (LinearLayout) findViewById(R.id.price_info_layout);
        passengerInfoHideLayout = (LinearLayout) findViewById(R.id.passenger_data_layout);
        priceInfoDownImage = (ImageView) findViewById(R.id.price_information_image);
        passengerDetailsImage = (ImageView) findViewById(R.id.passenger_details_image);
        ImageView cancelBookingImage = (ImageView) findViewById(R.id.cancel_booking_image);
        ImageView amendBookingImage = (ImageView) findViewById(R.id.amend_booking_image);
        RelativeLayout bookingDetailsLayout = (RelativeLayout) findViewById(R.id.booking_confirmation_details_layout);
        LinearLayout cancelBookingLayout = (LinearLayout) findViewById(R.id.cancel_booking_layout);
        LinearLayout amendBookingLayout = (LinearLayout) findViewById(R.id.amend_booking_layout);
        ImageView homeLogo = (ImageView) findViewById(R.id.home_logo);
        ImageView facebookLogo = (ImageView) findViewById(R.id.facebook_logo);
        ImageView twitterLogo = (ImageView) findViewById(R.id.twitter_logo);
        ImageView googlePlusLogo = (ImageView) findViewById(R.id.google_plus_logo);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        TextView numberOfCouponText = (TextView) findViewById(R.id.coupon_textview);
        numberOfCoupon = (TextView) findViewById(R.id.coupon_amount);
        couponLayout = (LinearLayout) findViewById(R.id.coupon_layout);
        couponView = (View) findViewById(R.id.coupon_view);
        qitafDiscountLayout = (LinearLayout) findViewById(R.id.qitaf_discount_layout);
        TextView qitafDiscountText = (TextView) findViewById(R.id.qitaf_discount_text);
        qitafDiscountValue = (TextView) findViewById(R.id.qitaf_discount);
        redeemedRewardsLayout = (LinearLayout) findViewById(R.id.redeemed_rewards_layout);
        TextView redeemedHeaderText = (TextView) findViewById(R.id.redeemed_rewards_text);
        redeemedRewardsValue = (TextView) findViewById(R.id.redeemed_rewards_value);
        if (Utils.isArabicLangSelected(FlightTripDetailsActivity.this)) {
            isArabicLang = true;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_ROBOTO_BOLD;
            fontMedium = Constants.FONT_DROIDKUFI_REGULAR;
            fontLight = Constants.FONT_ROBOTO_LIGHT;
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
            cancelBookingImage.setRotation(180);
            amendBookingImage.setRotation(180);
            airlinePnrNumber.setGravity(Gravity.END);
            tripIdNumber.setGravity(Gravity.END);
            dateOfBooking.setGravity(Gravity.END);
            airlineTicketCost.setGravity(Gravity.END);
            taxFare.setGravity(Gravity.END);
            numberOfTickets.setGravity(Gravity.END);
            airFareTotal.setGravity(Gravity.END);
            qitafDiscountValue.setGravity(Gravity.END);
            redeemedRewardsValue.setGravity(Gravity.END);
            numberOfCoupon.setGravity(Gravity.END);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                airlinePnrNumber.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                tripIdNumber.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                dateOfBooking.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                airlineTicketCost.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                taxFare.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                numberOfTickets.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                airFareTotal.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                redeemedRewardsValue.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                numberOfCoupon.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                qitafDiscountValue.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            }
            bookConfirmationText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            thankYouText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            bookingConfirmedText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            airlinePnrNumberText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            airlinePnrNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            tripIdText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            tripIdNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            dateOfBookingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            dateOfBooking.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerDetailsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerInformationText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            airlineTicketCostText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            airlineTicketCost.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            taxFareText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            numberOfCouponText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            taxFare.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            numberOfCoupon.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            numberOfTicketsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            numberOfTickets.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            qitafDiscountText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            qitafDiscountValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            airFareTotalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            airFareTotal.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            cancelBookingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            amendBookingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            redeemedHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            redeemedRewardsValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        titleFace = Typeface.createFromAsset(getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(getAssets(), fontText);
        Typeface boldFace = Typeface.createFromAsset(getAssets(), fontBold);
        Typeface regularMedium = Typeface.createFromAsset(getAssets(), fontMedium);
        regularLight = Typeface.createFromAsset(getAssets(), fontLight);
        Typeface textRegular = Typeface.createFromAsset(getAssets(), fontRegular);
        bookConfirmationText.setTypeface(titleFace);
        thankYouText.setTypeface(titleFace);
        bookingConfirmedText.setTypeface(textFace);
        airlinePnrNumberText.setTypeface(textFace);
        airlinePnrNumber.setTypeface(boldFace);
        tripIdText.setTypeface(textFace);
        tripIdNumber.setTypeface(boldFace);
        dateOfBookingText.setTypeface(textFace);
        dateOfBooking.setTypeface(boldFace);
        passengerDetailsText.setTypeface(titleFace);
        passengerInformationText.setTypeface(titleFace);
        airlineTicketCostText.setTypeface(titleFace);
        airlineTicketCost.setTypeface(textRegular);
        taxFareText.setTypeface(titleFace);
        numberOfCouponText.setTypeface(titleFace);
        taxFare.setTypeface(textRegular);
        numberOfCoupon.setTypeface(textRegular);
        numberOfTicketsText.setTypeface(titleFace);
        numberOfTickets.setTypeface(textRegular);
        airFareTotalText.setTypeface(tf);
        airFareTotal.setTypeface(regularMedium);
        cancelBookingText.setTypeface(titleFace);
        amendBookingText.setTypeface(titleFace);
        qitafDiscountText.setTypeface(titleFace);
        qitafDiscountValue.setTypeface(textRegular);
        redeemedHeaderText.setTypeface(titleFace);
        redeemedRewardsValue.setTypeface(textRegular);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            airportNamesMap = (HashMap<String, FlightAirportName>) b.get(Constants.AIRPORT_NAME_HASHMAP);
            airLineNamesMap = (HashMap<String, String>) b.get(Constants.AIRLINE_NAME_HASHMAP);
        }
        cancelBookingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskType = "Cancellation";
                final String requestJSON = bookingDetailsTask();
                isInternetPresent = Utils.isConnectingToInternet(FlightTripDetailsActivity.this);
                if (isInternetPresent) {
                    getCancelRequestFromServer(requestJSON);
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(titleFace);
                    errorDescriptionText.setTypeface(titleFace);
                    searchButton.setTypeface(titleFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(FlightTripDetailsActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                getCancelRequestFromServer(requestJSON);
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        });
        amendBookingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskType = "Amendment";
                final String requestJSON = bookingDetailsTask();
                isInternetPresent = Utils.isConnectingToInternet(FlightTripDetailsActivity.this);
                if (isInternetPresent) {
                    getAmendRequestFromServer(requestJSON);
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(titleFace);
                    errorDescriptionText.setTypeface(titleFace);
                    searchButton.setTypeface(titleFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(FlightTripDetailsActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                getAmendRequestFromServer(requestJSON);
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        });
        try {
            bookingDetailsLayout.setVisibility(View.GONE);
            tripFlightsArrayList = Singleton.getInstance().selectedTripFlightObject.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList();
            passengersBeanArrayList = Singleton.getInstance().selectedTripFlightObject.getPassengerDetailsObjectBean().getPassengersBeanArrayList();
            priceBean = Singleton.getInstance().selectedTripFlightObject.getPriceBean();
            passengerCount = passengersBeanArrayList.size();
            numberOfPassengers = passengersBeanArrayList.size();
            passengerMail = Singleton.getInstance().selectedTripFlightObject.getGeneralDetailsObjectBean().getCustomerId();
            refundStatus = Singleton.getInstance().selectedTripFlightObject.getEntityDetailsObjectBean().getFlightDetailsBean().getCancellationPolicyObjectBean().getNotesArray().get(0);
            fareTypeArray = Utils.checkRefundTypeResult(refundStatus);
            booingIdNumber = Singleton.getInstance().selectedTripFlightObject.getGeneralDetailsObjectBean().getFlyinCode();
            tripIdNumber.setText(booingIdNumber);
            String bookedDate = Utils.formatTripTimeStampToDate(Singleton.getInstance().selectedTripFlightObject.getGeneralDetailsObjectBean().getBookingDate(), FlightTripDetailsActivity.this);
            dateOfBooking.setText(bookedDate);
        } catch (Exception e) {
        }
        passengerDetailsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isProfileDataExpanded) {
                    passengerInfoHideLayout.setVisibility(View.GONE);
                    isProfileDataExpanded = true;
                    passengerDetailsImage.setImageResource(R.drawable.down_arrow);
                } else {
                    passengerInfoHideLayout.setVisibility(View.VISIBLE);
                    isProfileDataExpanded = false;
                    passengerDetailsImage.setImageResource(R.drawable.top_arrow);
                }
            }
        });
        priceInformationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPriceDetailsExpanded) {
                    priceInformationHideLayout.setVisibility(View.GONE);
                    isPriceDetailsExpanded = true;
                    priceInfoDownImage.setImageResource(R.drawable.down_arrow);
                } else {
                    priceInformationHideLayout.setVisibility(View.VISIBLE);
                    isPriceDetailsExpanded = false;
                    priceInfoDownImage.setImageResource(R.drawable.top_arrow);
                }
            }
        });
        FlightsDataBean flightsDataBean = new FlightsDataBean();
        LayoverDataBean layoverDataBean = new LayoverDataBean();
        airlinePnrArr.clear();
        for (int i = 0; i < tripFlightsArrayList.size(); i++) {
            if (!airlinePnrArr.contains(tripFlightsArrayList.get(i).getAirlinePNR())) {
                airlinePnrArr.add(tripFlightsArrayList.get(i).getAirlinePNR());
            }
            Utils.printMessage(TAG, "PNR arr : " + airlinePnrArr);
            flightsDataBean = tripFlightsArrayList.get(i);
            journeyType = tripFlightsArrayList.get(i).getJourneyType();
            legOrder = tripFlightsArrayList.get(i).getLegOrder();
            journeyOrder = tripFlightsArrayList.get(i).getJourneyOrder();
            if (journeyType.equalsIgnoreCase("O")) {
                if (legOrder.equalsIgnoreCase("1")) {
                    firstTripDetails = Utils.formatDateToMonth(tripFlightsArrayList.get(0).getDepartureDate(), FlightTripDetailsActivity.this);
                    departureAirportCode = tripFlightsArrayList.get(0).getDepartureAirport();
                    arrivalAirportCode = tripFlightsArrayList.get(tripFlightsArrayList.size() - 1).getArrivalAirport();
                    firstLegOrder++;
                    layoverDataBean.setJourneyType("O");
                    if (firstLegOrder == 1) {
                        flightsDataBean.setIsLayover(false);
                        flightsDataBean.setHeadingMessage(getHeaderMessage(layoverDataBean));
                    } else {
                        flightsDataBean.setIsLayover(true);
                        flightsDataBean.setHeadingMessage(Utils.getDifferenceBetweenTimeInHrMinsFormat(tripFlightsArrayList.get(i).getDepartureDate(), previousTime, FlightTripDetailsActivity.this));
                    }
                    flightsDataBean.setStopsCount(layoverDataBean.getStopsCount());
                    previousTime = tripFlightsArrayList.get(i).getArrivalDate();
                }
            } else if (journeyType.equalsIgnoreCase("R")) {
                if (legOrder.equalsIgnoreCase("2")) {
                    secondLegOrder++;
                    secondTripDetails = Utils.formatDateToMonth(tripFlightsArrayList.get(secondLegOrder).getDepartureDate(), FlightTripDetailsActivity.this);
                    departureAirportCode = tripFlightsArrayList.get(0).getDepartureAirport();
                    arrivalAirportCode = tripFlightsArrayList.get(secondLegOrder - 1).getArrivalAirport();
                    layoverDataBean.setJourneyType("R");
                    layoverDataBean.setDepartureName("");

                    if (secondLegOrder == 1) {
                        flightsDataBean.setIsLayover(false);
                        flightsDataBean.setHeadingMessage(getHeaderMessage(layoverDataBean));
                    } else {
                        flightsDataBean.setIsLayover(true);
                        flightsDataBean.setHeadingMessage(Utils.getDifferenceBetweenTimeInHrMinsFormat(tripFlightsArrayList.get(i).getDepartureDate(), previousTime, FlightTripDetailsActivity.this));
                    }
                    flightsDataBean.setStopsCount(layoverDataBean.getStopsCount());
                    previousTime = tripFlightsArrayList.get(i).getArrivalDate();
                }
            } else if (journeyType.equalsIgnoreCase("M")) {
                firstLegOrder++;
                secondLegOrder++;
                thirdLegOrder++;
                layoverDataBean.setJourneyType("M");
                layoverDataBean.setDepartureName("");
                if (legOrder.equalsIgnoreCase("1")) {
                    forwardLegOrder++;
                    firstDepartureCode = tripFlightsArrayList.get(0).getDepartureAirport();
                    firstArrivalCode = tripFlightsArrayList.get(forwardLegOrder - 1).getArrivalAirport();
                    firstTripDetails = Utils.formatDateToMonth(tripFlightsArrayList.get(forwardLegOrder - 1).getDepartureDate(), FlightTripDetailsActivity.this);
                    layoverDataBean.setLegOrder("1");
                    if (forwardLegOrder == 1) {
                        flightsDataBean.setIsLayover(false);
                        flightsDataBean.setHeadingMessage(getMultiCityHeaderMessage(layoverDataBean));
                    } else {
                        flightsDataBean.setIsLayover(true);
                        flightsDataBean.setHeadingMessage(Utils.getDifferenceBetweenTimeInHrMinsFormat(tripFlightsArrayList.get(i).getDepartureDate(), previousTime, FlightTripDetailsActivity.this));
                    }
                    flightsDataBean.setStopsCount(layoverDataBean.getStopsCount());
                    previousTime = tripFlightsArrayList.get(i).getArrivalDate();
                }
                if (legOrder.equalsIgnoreCase("2")) {
                    secondDepartureCode = tripFlightsArrayList.get(firstLegOrder - 1).getDepartureAirport();
                    secondArrivalCode = tripFlightsArrayList.get(secondLegOrder - 1).getArrivalAirport();
                    secondTripDetails = Utils.formatDateToMonth(tripFlightsArrayList.get(firstLegOrder - 1).getDepartureDate(), FlightTripDetailsActivity.this);
                    layoverDataBean.setLegOrder("2");
                    multiTripStopCount2++;
                    if (multiTripStopCount2 == 1) {
                        flightsDataBean.setIsLayover(false);
                        flightsDataBean.setHeadingMessage(getMultiCityHeaderMessage(layoverDataBean));
                    } else {
                        flightsDataBean.setIsLayover(true);
                        flightsDataBean.setHeadingMessage(Utils.getDifferenceBetweenTimeInHrMinsFormat(tripFlightsArrayList.get(i).getDepartureDate(), previousTime, FlightTripDetailsActivity.this));
                    }
                    flightsDataBean.setStopsCount(layoverDataBean.getStopsCount());
                    previousTime = tripFlightsArrayList.get(i).getArrivalDate();
                }
                if (legOrder.equalsIgnoreCase("3")) {
                    thirdDepartureCode = tripFlightsArrayList.get(secondLegOrder - 1).getDepartureAirport();
                    thirdArrivalCode = tripFlightsArrayList.get(thirdLegOrder - 1).getArrivalAirport();
                    thirdTripDetails = Utils.formatDateToMonth(tripFlightsArrayList.get(secondLegOrder - 1).getDepartureDate(), FlightTripDetailsActivity.this);
                    layoverDataBean.setLegOrder("3");
                    multiTripStopCount3++;
                    if (multiTripStopCount3 == 1) {
                        flightsDataBean.setIsLayover(false);
                        flightsDataBean.setHeadingMessage(getMultiCityHeaderMessage(layoverDataBean));
                    } else {
                        flightsDataBean.setIsLayover(true);
                        flightsDataBean.setHeadingMessage(Utils.getDifferenceBetweenTimeInHrMinsFormat(tripFlightsArrayList.get(i).getDepartureDate(), previousTime, FlightTripDetailsActivity.this));
                    }
                    flightsDataBean.setStopsCount(layoverDataBean.getStopsCount());
                    previousTime = tripFlightsArrayList.get(i).getArrivalDate();
                }
            }
            setFlightDetailsInfo(flightsDataBean);
        }
        if (airlinePnrArr.size() > 0) {
            if(airlinePnrArr.size() == 1){
                if(airlinePnrArr.get(0) == null || airlinePnrArr.get(0).equalsIgnoreCase("null")){
                    Utils.printMessage(TAG, "GDS : "+Utils.checkStringVal(Singleton.getInstance().selectedTripFlightObject.getEntityDetailsObjectBean().getFlightDetailsBean().getGdsPnr()));
                    airlinePnrNumber.setText(Utils.checkStringVal(Singleton.getInstance().selectedTripFlightObject.getEntityDetailsObjectBean().getFlightDetailsBean().getGdsPnr()));
                }else {
                    airlinePnrNumber.setText(airlinePnrArr.get(0));
                }
            }else {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < airlinePnrArr.size(); i++) {
                    if(airlinePnrArr.get(i) == null || airlinePnrArr.get(i).equalsIgnoreCase("null")){
                        if (Utils.checkStringVal(airlinePnrArr.get(i)).isEmpty()) {
                            String gdsPnr = Singleton.getInstance().selectedTripFlightObject.getEntityDetailsObjectBean().getFlightDetailsBean().getGdsPnr();
                            String[] gdsValues = gdsPnr.split("/");
                            if (i == 0) {
                                builder.append(gdsValues[i]);
                            } else {
                                builder.append("/" + gdsValues[i]);
                            }
                        }
                    }else {
                        if (i == 0) {
                            builder.append(airlinePnrArr.get(i));
                        } else {
                            builder.append("/" + airlinePnrArr.get(i));
                        }
                    }
                }
                airlinePnrNumber.setText(builder.toString());
            }
        } else {
            airlinePnrNumber.setText(Utils.checkStringVal(Singleton.getInstance().selectedTripFlightObject.getEntityDetailsObjectBean().getFlightDetailsBean().getGdsPnr()));
        }
        homeLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHomeClick();
            }
        });
        facebookLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://www.facebook.com/flyin.com"));
                startActivity(intent);
            }
        });
        twitterLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://twitter.com/flyincom"));
                startActivity(intent);
            }
        });
        googlePlusLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://plus.google.com/+flyin/posts"));
                startActivity(intent);
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.home_back_layout);
        ImageView mImageView = (ImageView) mCustomView.findViewById(R.id.first_home_icon);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_home_text);
        ImageView first_home_logo = (ImageView) mCustomView.findViewById(R.id.first_home_logo);
        first_home_logo.setImageResource(R.mipmap.flyin_home);
        first_home_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHomeClick();
            }
        });
        mCustomView.findViewById(R.id.sing_in_button).setVisibility(View.GONE);
        ImageView homeIcon = (ImageView) mCustomView.findViewById(R.id.home_icon);
        homeIcon.setVisibility(View.VISIBLE);
        if (Utils.isArabicLangSelected(FlightTripDetailsActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backText.setTypeface(titleFace);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        mActionBar.setCustomView(mCustomView, layout);
        mActionBar.setDisplayShowCustomEnabled(true);
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        PassengersBean passengersBean = new PassengersBean();
        for (int i = 0; i < passengersBeanArrayList.size(); i++) {
            passengersBean = passengersBeanArrayList.get(i);
            if (i == 0) {
                passengersBean.setIsSecondPassenger(false);
            } else {
                passengersBean.setIsSecondPassenger(true);
            }
            passengerCount++;
            setPassengerInformation(passengersBean);
        }
        setPriceDetails(priceBean);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        myReceiver = new MyReceiver();
        intentFilter = new IntentFilter("com.flyin.bookings.ERROR_MESSAGE");
        registerReceiver(myReceiver, intentFilter);
    }

    private void onHomeClick() {
        Intent intent = new Intent(FlightTripDetailsActivity.this, MainSearchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void setFlightDetailsInfo(FlightsDataBean data) {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_trip_detail_confirmation, null);
        flightInformationLayout.addView(view);
        LinearLayout odoHeaderLayout = (LinearLayout) view.findViewById(R.id.odo_header_layout);
        TextView departHeaderText = (TextView) view.findViewById(R.id.departure_text);
        TextView departText = (TextView) view.findViewById(R.id.departure_journey);
        TextView departStops = (TextView) view.findViewById(R.id.departure_stops);
        ImageView airlineImage = (ImageView) view.findViewById(R.id.airline_image);
        TextView departFrom = (TextView) view.findViewById(R.id.departure_from);
        TextView departCountry = (TextView) view.findViewById(R.id.departure_country);
        TextView departLocation = (TextView) view.findViewById(R.id.departure_location);
        TextView departTerminal = (TextView) view.findViewById(R.id.depart_terminal);
        TextView departTime = (TextView) view.findViewById(R.id.depatingtime);
        TextView departDate = (TextView) view.findViewById(R.id.departure_date);
        TextView journeyAirlineName = (TextView) view.findViewById(R.id.journey_airline_name);
        TextView journeyAirlineDetails = (TextView) view.findViewById(R.id.journey_airline_details);
        TextView arrivalTo = (TextView) view.findViewById(R.id.arrival_to);
        TextView arrivalCountry = (TextView) view.findViewById(R.id.arrival_country);
        TextView arrivalLocation = (TextView) view.findViewById(R.id.arrival_location);
        TextView arrivalTerminal = (TextView) view.findViewById(R.id.arrival_terminal);
        TextView arrivalTime = (TextView) view.findViewById(R.id.arrival_time);
        TextView arrivalDate = (TextView) view.findViewById(R.id.arrival_date);
        TextView flightStops = (TextView) view.findViewById(R.id.flight_stops);
        TextView flightDuration = (TextView) view.findViewById(R.id.flight_duration);
        TextView flightRefundStatus = (TextView) view.findViewById(R.id.flight_refund_status);
        LinearLayout layoverLayout = (LinearLayout) view.findViewById(R.id.layover_layout);
        TextView layoverText = (TextView) view.findViewById(R.id.layover_text);
        TextView secondDepartText = (TextView) view.findViewById(R.id.second_departure_text);
        TextView baggageInformation = (TextView) view.findViewById(R.id.baggage_information);
        if (isArabicLang) {
            departText.setGravity(Gravity.START);
            secondDepartText.setGravity(Gravity.START);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                departText.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                secondDepartText.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
            departHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            departText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            departStops.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            layoverText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            secondDepartText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            departFrom.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            departCountry.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            departLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            departTerminal.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            departTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            departDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            journeyAirlineName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            journeyAirlineDetails.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            arrivalTo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            arrivalCountry.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            arrivalLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            arrivalTerminal.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            arrivalTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            arrivalDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            flightStops.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            flightDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            flightRefundStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            baggageInformation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
        }
        departHeaderText.setTypeface(titleFace);
        departText.setTypeface(titleFace);
        departStops.setTypeface(titleFace);
        departFrom.setTypeface(titleFace);
        departCountry.setTypeface(textFace);
        departLocation.setTypeface(textFace);
        departTerminal.setTypeface(textFace);
        departTime.setTypeface(textFace);
        departDate.setTypeface(textFace);
        journeyAirlineName.setTypeface(textFace);
        journeyAirlineDetails.setTypeface(regularLight);
        arrivalTo.setTypeface(titleFace);
        arrivalCountry.setTypeface(textFace);
        arrivalLocation.setTypeface(textFace);
        arrivalTerminal.setTypeface(textFace);
        arrivalTime.setTypeface(textFace);
        arrivalDate.setTypeface(textFace);
        flightStops.setTypeface(textFace);
        flightDuration.setTypeface(titleFace);
        flightRefundStatus.setTypeface(textFace);
        baggageInformation.setTypeface(textFace);
        layoverText.setTypeface(titleFace);
        secondDepartText.setTypeface(titleFace);
        if (journeyType.equalsIgnoreCase("O")) {
            if (data.getLegOrder().equalsIgnoreCase("1")) {
                departHeaderText.setText(getString(R.string.label_search_flight_departure));
                if (fareTypeArray.get(0).equalsIgnoreCase("refundable") || fareTypeArray.get(0).equalsIgnoreCase("refundable before departure")) {
                    flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                    flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                } else {
                    flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                    flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                }
            }
        } else if (journeyType.equalsIgnoreCase("R")) {
            if (data.getLegOrder().equalsIgnoreCase("2")) {
                departHeaderText.setText(getString(R.string.label_search_flight_return));
                if (fareTypeArray.size() > 1) {
                    if (fareTypeArray.get(1).equalsIgnoreCase("refundable") || fareTypeArray.get(1).equalsIgnoreCase("refundable before departure")) {
                        flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                        flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                    } else {
                        flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                        flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                    }
                } else {
                    if (fareTypeArray.get(0).equalsIgnoreCase("refundable") || fareTypeArray.get(0).equalsIgnoreCase("refundable before departure")) {
                        flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                        flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                    } else {
                        flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                        flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                    }
                }
            }
        } else if (journeyType.equalsIgnoreCase("M")) {
            if (data.getLegOrder().equalsIgnoreCase("1")) {
                departHeaderText.setText(getString(R.string.label_search_flight_departure));
                if (fareTypeArray.get(0).equalsIgnoreCase("refundable") || fareTypeArray.get(0).equalsIgnoreCase("refundable before departure")) {
                    flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                    flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                } else {
                    flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                    flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                }
            } else if (data.getLegOrder().equalsIgnoreCase("2")) {
                departHeaderText.setText(getString(R.string.label_search_flight_departure));
                if (fareTypeArray.size() > 1) {
                    if (fareTypeArray.get(1).equalsIgnoreCase("refundable") || fareTypeArray.get(1).equalsIgnoreCase("refundable before departure")) {
                        flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                        flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                    } else {
                        flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                        flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                    }
                } else {
                    if (fareTypeArray.get(0).equalsIgnoreCase("refundable") || fareTypeArray.get(0).equalsIgnoreCase("refundable before departure")) {
                        flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                        flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                    } else {
                        flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                        flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                    }
                }
            } else if (data.getLegOrder().equalsIgnoreCase("3")) {
                departHeaderText.setText(getString(R.string.label_search_flight_departure));
                if (fareTypeArray.size() > 2) {
                    if (fareTypeArray.get(2).equalsIgnoreCase("refundable") || fareTypeArray.get(2).equalsIgnoreCase("refundable before departure")) {
                        flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                        flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                    } else {
                        flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                        flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                    }
                } else if (fareTypeArray.size() > 1) {
                    if (fareTypeArray.get(1).equalsIgnoreCase("refundable") || fareTypeArray.get(1).equalsIgnoreCase("refundable before departure")) {
                        flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                        flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                    } else {
                        flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                        flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                    }
                } else {
                    if (fareTypeArray.get(0).equalsIgnoreCase("refundable") || fareTypeArray.get(0).equalsIgnoreCase("refundable before departure")) {
                        flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                        flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                    } else {
                        flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                        flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                    }
                }
            }
        }
        if (data.isLayover()) {
            odoHeaderLayout.setVisibility(View.GONE);
            layoverLayout.setVisibility(View.VISIBLE);
        } else {
            odoHeaderLayout.setVisibility(View.VISIBLE);
            layoverLayout.setVisibility(View.GONE);
        }
        if (data.getStopsCount() == 0) {
            departStops.setText(R.string.label_non_stop_text);
        } else if (data.getStopsCount() == 1) {
            departStops.setText(data.getStopsCount() + " " + getString(R.string.label_stop_text));
        } else {
            departStops.setText(data.getStopsCount() + " " + getString(R.string.label_stops_text));
        }
        String departureAirportName = airportNamesMap.get(data.getDepartureAirport()).getAirpotName();
        String arrivalAirportName = airportNamesMap.get(data.getArrivalAirport()).getAirpotName();
        String airlineName = airLineNamesMap.get(data.getOperatingAirline());
        try {
            if (data.getTerminalinfo().equalsIgnoreCase(null) || !data.getTerminalinfo().equalsIgnoreCase("")) {
                if (data.getTerminalinfo().contains("|")) {
                    String[] terminalText = data.getTerminalinfo().split("\\|", -1);
                    if (!terminalText[0].isEmpty()) {
                        departTerminal.setVisibility(View.VISIBLE);
                        departTerminal.setText(getString(R.string.label_terminal) + " " + terminalText[0]);
                    }
                    if (!terminalText[1].isEmpty()) {
                        arrivalTerminal.setVisibility(View.VISIBLE);
                        arrivalTerminal.setText(getString(R.string.label_terminal) + " " + terminalText[1]);
                    }
                } else {
                    departTerminal.setVisibility(View.VISIBLE);
                    departTerminal.setText(getString(R.string.label_terminal) + " " + data.getTerminalinfo());
                }
            } else {
                departTerminal.setVisibility(View.GONE);
                arrivalTerminal.setVisibility(View.GONE);
            }
        } catch (Exception e) {
        }
        departText.setText(data.getHeadingMessage());
        secondDepartText.setText(data.getHeadingMessage());
        String imagePath = Constants.IMAGE_BASE_URL + data.getMarketingAirline() + Constants.IMAGE_FILE_FORMAT;
        Glide.with(FlightTripDetailsActivity.this).load(imagePath).placeholder(R.drawable.imagethumb_search).into(airlineImage);
        journeyAirlineName.setText(airlineName);
        journeyAirlineDetails.setText(data.getOperatingAirline() + " " + data.getFlightNumber());
        String departAirportName = airportNamesMap.get(data.getDepartureAirport()).getCityName();
        departFrom.setText(data.getDepartureAirport() + " " + departAirportName);
        departCountry.setText(airportNamesMap.get(data.getDepartureAirport()).getCityCode());
        departLocation.setText(departureAirportName);
        departTime.setText(Utils.formatTripDateToTimeFormat(data.getDepartureDate(), FlightTripDetailsActivity.this));
        departDate.setText(Utils.formatTripDateToDayFormat(data.getDepartureDate(), FlightTripDetailsActivity.this));
        String ArrivalAirportName = airportNamesMap.get(data.getArrivalAirport()).getCityName();
        arrivalTo.setText(data.getArrivalAirport() + " " + ArrivalAirportName);
        arrivalCountry.setText(airportNamesMap.get(data.getArrivalAirport()).getCityCode());
        arrivalLocation.setText(arrivalAirportName);
        arrivalTime.setText(Utils.formatTripDateToTimeFormat(data.getArrivalDate(), FlightTripDetailsActivity.this));
        arrivalDate.setText(Utils.formatTripDateToDayFormat(data.getArrivalDate(), FlightTripDetailsActivity.this));
        String classType = "";
        if (data.getClassType().equalsIgnoreCase("Economy")) {
            classType = getString(R.string.label_search_flight_economy);
        } else if (data.getClassType().equalsIgnoreCase("Business")) {
            classType = getString(R.string.label_search_flight_business);
        } else if (data.getClassType().equalsIgnoreCase("First")) {
            classType = getString(R.string.label_search_flight_first);
        }
        flightStops.setText(classType);
        String flightDurationValue = Utils.minutesToTimeFormat(data.getDuration(), FlightTripDetailsActivity.this);
        if (!flightDurationValue.isEmpty()) {
            flightDuration.setText(getString(R.string.label_duration_text) + " " + flightDurationValue);
        }
        try {
            String checkInDetails = Utils.checkStringVal(data.getBaggageInfo());
            if (!checkInDetails.equalsIgnoreCase("")) {
                String baggageInfo = "";
                String[] baggageDetails = checkInDetails.split(" ");
                if (baggageDetails[1].equalsIgnoreCase("Kilos")) {
                    baggageInfo = baggageDetails[0] + " " + getString(R.string.label_kilo);
                } else if (baggageDetails[1].equalsIgnoreCase("Pieces")) {
                    baggageInfo = baggageDetails[0] + " " + getString(R.string.label_pieces);
                } else {
                    baggageInfo = checkInDetails;
                }
                if (isArabicLang) {
                    baggageInformation.setText(getString(R.string.label_reviewPage_baggageInfo) + " '" + baggageInfo + "' (" + getString(R.string.label_per_person_text) + ")");
                } else {
                    baggageInformation.setText(getString(R.string.label_reviewPage_baggageInfo) + " " + getString(R.string.label_reviewPage_checkIn) + " '" + baggageInfo + "' (" + getString(R.string.label_per_person_text) + ")");
                }
            } else {
                baggageInformation.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPassengerInformation(PassengersBean passengerInfo) {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View passengerView = inflater.inflate(R.layout.view_passenger_details, null);
        passengerInfoHideLayout.addView(passengerView);
        TextView passengerNameText = (TextView) passengerView.findViewById(R.id.passenger_name_text);
        TextView passengerTicketText = (TextView) passengerView.findViewById(R.id.passenger_ticket_text);
        passengerNameText.setTypeface(titleFace);
        passengerTicketText.setTypeface(titleFace);
        if (isArabicLang) {
            passengerNameText.setGravity(Gravity.END);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                passengerNameText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            }
            passengerNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerTicketText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        if (!passengerInfo.isSecondPassenger()) {
            passengerNameText.setText(Utils.checkStringVal(passengerInfo.getPersonalDetailsObjectBean().getTitle()) + " " + passengerInfo.getPersonalDetailsObjectBean().getFirstName() + " " + passengerInfo.getPersonalDetailsObjectBean().getLastName() + " - " + Utils.checkStringVal(passengerMail)); // + " - " + passengerMail
            passengerTicketText.setText(getString(R.string.label_ticket_number_text) + " – " + Utils.checkStringVal(passengerInfo.getPersonalDetailsObjectBean().getTicketNo()));
        } else {
            passengerNameText.setText(Utils.checkStringVal(passengerInfo.getPersonalDetailsObjectBean().getTitle()) + " " + passengerInfo.getPersonalDetailsObjectBean().getFirstName() + " " + passengerInfo.getPersonalDetailsObjectBean().getLastName());
            passengerTicketText.setText(getString(R.string.label_ticket_number_text) + " – " + Utils.checkStringVal(passengerInfo.getPersonalDetailsObjectBean().getTicketNo()));
        }
    }

    private void setPriceDetails(PriceBean data) {
        try {
            Double ticketCost = 0.0;
            Double taxCost = 0.0;
            Double airFareTotalCost = 0.0;
            Double totalTicketPrice = 0.0;
            int qitafAmount = 0;
            String redeemedValue = "";
            if (!data.getBaseFare().isEmpty()) {
                ticketCost = Double.parseDouble(data.getBaseFare());
            }
            if (!data.getTax().isEmpty()) {
                taxCost = Double.parseDouble(data.getTax());
            }
            if (!data.getTotal().isEmpty()) {
                airFareTotalCost = Double.parseDouble(data.getTotal());
            }
            if (!data.getRdmDiscountusr().isEmpty()) {
                if (Double.parseDouble(data.getRdmDiscountusr()) > 0) {
                    redeemedValue = String.format(Locale.ENGLISH, "%.2f", Double.parseDouble(data.getRdmDiscountusr()));
                    redeemedRewardsLayout.setVisibility(View.VISIBLE);
                } else {
                    redeemedRewardsLayout.setVisibility(View.GONE);
                }
            } else {
                redeemedRewardsLayout.setVisibility(View.GONE);
            }
            if (!data.getQitafAmount().isEmpty()) {
                if (Double.parseDouble(data.getQitafAmount()) > 0) {
                    Double qitafDisc = Double.parseDouble(data.getQitafAmount());
                    qitafAmount = (int) Math.ceil(qitafDisc);
                    qitafDiscountLayout.setVisibility(View.VISIBLE);
                } else {
                    qitafDiscountLayout.setVisibility(View.GONE);
                }
            }
            String defaultCurr = getString(R.string.label_SAR_currency_name); //label_qitaf_currency_tag
            String currencyName = "";
            if (data.getUserCurrency().equalsIgnoreCase("SAR")) {
                currencyName = getString(R.string.label_SAR_currency_name);
            } else {
                currencyName = data.getUserCurrency();
            }
            if (priceBean.getCouponDiscount() != null && !priceBean.getCouponDiscount().isEmpty()) {
                if (Double.parseDouble(priceBean.getCouponDiscount()) == 0.0) {
                    couponLayout.setVisibility(View.GONE);
                    couponView.setVisibility(View.GONE);
                    totalTicketPrice = airFareTotalCost;
                } else {
                    couponLayout.setVisibility(View.VISIBLE);
                    couponView.setVisibility(View.VISIBLE);
                    totalTicketPrice = airFareTotalCost - Double.parseDouble(priceBean.getCouponDiscount());
                    if (data.getUserCurrency().equalsIgnoreCase("SAR")) {
                        if (isArabicLang) {
                            numberOfCoupon.setText(String.format(getApplicationContext().getResources().getString(R.string.label_placeholder), priceBean.getCouponDiscount(), currencyName));
                        } else {
                            numberOfCoupon.setText(String.format(getApplicationContext().getResources().getString(R.string.label_placeholder), currencyName, priceBean.getCouponDiscount()));
                        }
                    } else {
                        numberOfCoupon.setText(String.format(getApplicationContext().getResources().getString(R.string.label_placeholder), currencyName, priceBean.getCouponDiscount()));
                    }
                }
            } else {
                totalTicketPrice = airFareTotalCost;
                couponLayout.setVisibility(View.GONE);
                couponView.setVisibility(View.GONE);
            }
            String finalPaidPrice = String.format(Locale.ENGLISH, "%.2f", totalTicketPrice);
            if (data.getUserCurrency().equalsIgnoreCase("SAR")) {
                if (isArabicLang) {
                    airlineTicketCost.setText(ticketCost + " " + currencyName);
                    taxFare.setText(taxCost + " " + currencyName);
                    airFareTotal.setText(finalPaidPrice + " " + currencyName);
                    redeemedRewardsValue.setText(redeemedValue + " " + currencyName);
                } else {
                    airlineTicketCost.setText(currencyName + " " + ticketCost);
                    taxFare.setText(currencyName + " " + taxCost);
                    airFareTotal.setText(currencyName + " " + finalPaidPrice);
                    redeemedRewardsValue.setText(currencyName + " " + redeemedValue);
                }
            } else {
                airlineTicketCost.setText(currencyName + " " + ticketCost);
                taxFare.setText(currencyName + " " + taxCost);
                airFareTotal.setText(currencyName + " " + finalPaidPrice);
                redeemedRewardsValue.setText(currencyName + " " + redeemedValue);
            }
            if (isArabicLang) {
                qitafDiscountValue.setText(qitafAmount + " " + defaultCurr);
            } else {
                qitafDiscountValue.setText(defaultCurr + " " + qitafAmount);
            }
        } catch (Exception e) {
        }
        numberOfTickets.setText(String.valueOf(numberOfPassengers));
    }

    private String bookingDetailsTask() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(FlightTripDetailsActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(FlightTripDetailsActivity.this));
            String passengerName = (passengersBeanArrayList.get(0).getPersonalDetailsObjectBean().getFirstName() + " " + passengersBeanArrayList.get(0).getPersonalDetailsObjectBean().getLastName());
            mainJSON.put("source", sourceJSON);
            mainJSON.put("bookingType", "Flight");
            mainJSON.put("taskType", taskType);
            mainJSON.put("bookingId", booingIdNumber);
            mainJSON.put("registerCustomerId", pref.getString(Constants.MEMBER_EMAIL, ""));
            mainJSON.put("leadPaxName", passengerName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void getCancelRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(FlightTripDetailsActivity.this, FlightTripDetailsActivity.this,
                Constants.CANCEL_BOOKING, "", requestJSON, GET_CANCEL_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void getAmendRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(FlightTripDetailsActivity.this, FlightTripDetailsActivity.this,
                Constants.CANCEL_BOOKING, "", requestJSON, GET_AMEND_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        closeLoading();
        if (serviceType == GET_CANCEL_RESPONSE) {
            Utils.printMessage(TAG, "Cancel DATA:: " + data);
            if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
                inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = inflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(textFace);
                errorDescriptionText.setTypeface(textFace);
                searchButton.setTypeface(textFace);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isInternetPresent = Utils.isConnectingToInternet(FlightTripDetailsActivity.this);
                        if (isInternetPresent) {
                            loadingViewLayout.removeView(errorView);
                            finish();
                        }
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            } else {
                try {
                    JSONObject obj = new JSONObject();
                    obj = new JSONObject(data);
                    if (obj.has("status")) {
                        String status = obj.getString("status");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            displayErrorMessage(getString(R.string.CancelSuccessLabel));
                            errorView.setBackgroundResource(R.color.success_message_background);
                            return;
                        } else {
                            if (obj.has("msg")) {
                                JSONArray messageArr = obj.getJSONArray("msg");
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < messageArr.length(); i++) {
                                    if (i == messageArr.length() - 1) {
                                        sb.append(messageArr.getString(i) + ".");
                                    } else {
                                        sb.append(messageArr.getString(i) + ", ");
                                    }
                                }
                                Utils.printMessage(TAG, "Fail Msg : " + sb.toString());
                                displayErrorMessage(sb.toString());
                                errorView.setBackgroundResource(R.color.error_message_background);
                                return;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (serviceType == GET_AMEND_RESPONSE) {
            Utils.printMessage(TAG, "Amend DATA:: " + data);
            if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
                inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = inflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(textFace);
                errorDescriptionText.setTypeface(textFace);
                searchButton.setTypeface(textFace);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isInternetPresent = Utils.isConnectingToInternet(FlightTripDetailsActivity.this);
                        if (isInternetPresent) {
                            loadingViewLayout.removeView(errorView);
                            finish();
                        }
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            } else {
                try {
                    JSONObject obj = new JSONObject();
                    obj = new JSONObject(data);
                    if (obj.has("status")) {
                        String status = obj.getString("status");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            displayErrorMessage(getString(R.string.CancelSuccessLabel));
                            errorView.setBackgroundResource(R.color.success_message_background);
                            return;
                        } else {
                            if (obj.has("msg")) {
                                JSONArray messageArr = obj.getJSONArray("msg");
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < messageArr.length(); i++) {
                                    if (i == messageArr.length() - 1) {
                                        sb.append(messageArr.getString(i) + ".");
                                    } else {
                                        sb.append(messageArr.getString(i) + ", ");
                                    }
                                }
                                displayErrorMessage(sb.toString());
                                errorView.setBackgroundResource(R.color.error_message_background);
                                return;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(titleFace);
        descriptionText.setVisibility(View.GONE);
        if (isArabicLang) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    private void loadErrorType(int type) {

        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.ERROR_MESSAGE")) {
                displayErrorMessage(intent.getStringExtra("message"));
            }
        }
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 3000);
        }
    }

    private String getHeaderMessage(LayoverDataBean layoverDataBean) {
        int count = -1;
        for (int i = 0; i < tripFlightsArrayList.size(); i++) {
            if (layoverDataBean.getJourneyType().equalsIgnoreCase(tripFlightsArrayList.get(i).getJourneyType())) {
                count++;
                if (layoverDataBean.getDepartureName().equalsIgnoreCase("")) {
                    layoverDataBean.setDepartureName(tripFlightsArrayList.get(i).getDepartureAirport());
                }
                layoverDataBean.setArrivalName(tripFlightsArrayList.get(i).getArrivalAirport());
                layoverDataBean.setStopsCount(count);
            }
        }
        String fromCity = airportNamesMap.get(layoverDataBean.getDepartureName()).getCityName();
        String toCity = airportNamesMap.get(layoverDataBean.getArrivalName()).getCityName();
        layoverDataBean.setHeaderMessage(fromCity + " - " + toCity);
        return layoverDataBean.getHeaderMessage();
    }

    private String getMultiCityHeaderMessage(LayoverDataBean layoverDataBean) {
        int stopCount = -1;
        for (int i = 0; i < tripFlightsArrayList.size(); i++) {
            if (layoverDataBean.getJourneyType().equalsIgnoreCase(tripFlightsArrayList.get(i).getJourneyType())) {
                if (layoverDataBean.getLegOrder().equalsIgnoreCase(tripFlightsArrayList.get(i).getLegOrder())) {
                    stopCount++;
                    if (layoverDataBean.getDepartureName().equalsIgnoreCase("")) {
                        layoverDataBean.setDepartureName(tripFlightsArrayList.get(i).getDepartureAirport());
                    }
                    layoverDataBean.setArrivalName(tripFlightsArrayList.get(i).getArrivalAirport());
                    layoverDataBean.setStopsCount(stopCount);
                }
            }
        }
        String fromCity = airportNamesMap.get(layoverDataBean.getDepartureName()).getCityName();
        String toCity = airportNamesMap.get(layoverDataBean.getArrivalName()).getCityName();
        layoverDataBean.setHeaderMessage(fromCity + " - " + toCity);
        return layoverDataBean.getHeaderMessage();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver);
    }
}
