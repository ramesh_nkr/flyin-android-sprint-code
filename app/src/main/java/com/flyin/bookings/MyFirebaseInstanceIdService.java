package com.flyin.bookings;

import com.flyin.bookings.util.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.webengage.sdk.android.WebEngage;

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        WebEngage.get().setRegistrationID(FirebaseInstanceId.getInstance().getToken());
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Utils.printMessage("MyFirebaseInstance", "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(refreshedToken);
    }
}
