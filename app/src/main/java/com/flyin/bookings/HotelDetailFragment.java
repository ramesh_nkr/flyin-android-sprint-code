package com.flyin.bookings;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.flyin.bookings.adapter.DemoPagerAdapter;
import com.flyin.bookings.dialog.AddTravellerDialog;
import com.flyin.bookings.model.FhRoomPassengerDetailsBean;
import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.model.RoomModel;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import me.relex.circleindicator.CircleIndicator;

@SuppressWarnings("ALL")
public class HotelDetailFragment extends Fragment implements AsyncTaskListener {
    private static final int ROUND_TRIP = 41;
    private static final int GET_ACCESS_TOKEN = 10, SEARCH_HOTEL = 12, UNIQUE_SEARCH_HOTEL = 13;
    public static ScrollView scollview;
    private RadioButton roomSelect, hotelInfo, viewMap;
    private HotelModel model = null;
    private String[] imageList = null;
    private TextView address, check_in, check_out, edit, hotel_name;
    private RatingBar hotel_rating;
    private RelativeLayout edit_layout;
    private int room_count = 1, night_count = 0;
    private HashMap<String, ArrayList<String>> child_count_list = null;
    private ArrayList<String> adult_count_list = null;
    private ArrayList<String> age_child = null;
    private LinearLayout mLayout;
    private Button update;
    private Calendar calendar;
    boolean isArabicLang;
    private String accessTocken, cityId;
    private String header_date = "";
    private TextView checkInTime, checkOutTime;
    private TextView errorText, errorDescriptionText;
    private LayoutInflater layoutInflater;
    private Typeface regularLight, regularBold;
    private ViewPager viewpager;
    private ArrayList<String> huidlist;
    private ArrayList<HotelModel> hotelList = null;
    private String hotelCheckInDate, hotelCheckOutDate;
    private int childs, tempChild;
    private int adults, tempAdult;
    private RelativeLayout errorView = null, loadingViewLayout;
    private TextView errorMessageText = null;
    private ImageView errorImage;
    private Button searchButton;
    private CircleIndicator indicator;
    private ArrayList<String> adultsCountList = null;
    private ArrayList<String> childCountList = null;
    private HashMap<String, ArrayList<String>> childCount = null;
    private static final String TAG = "HotelDetailFragment";
    private View loadingView = null;
    private Handler handler = null;
    private String hotelInDate = "";
    private String hotelOutDate = "";
    private LinearLayout guestLayout = null;
    private TextView guestCountText = null;
    private AddTravellerDialog addTravellerDialog = null;
    private ArrayList<FhRoomPassengerDetailsBean> roomPassengerArrayList = null;
    private String cabinType = "";
    private boolean isClassTypeNeed = false;
    private int totalAdultCnt = 1;
    private int totalChildCnt = 0;
    private int totalPassengerCount = 1;
    private ArrayList<String> childList = null;
    private ArrayList<String> occupancy_adult_count_list = null;
    private HashMap<String, ArrayList<String>> occupancy_child_count_list = null;
    private LinearLayout backLayout = null;
    private JSONObject roomJsonObj = new JSONObject();
    private LinearLayout bnplPackageLayout = null;
    private TextView bnplHeaderText = null;
    private String currentDate = "";
    private String randomNumber = "";
    private String getDetailsData = "";
    private Activity mActivity;
    private String searchJson = "";
    private String uniqueHotelId = "";
    private int totalRoomCount = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((MainSearchActivity) mActivity).hideToolbar();
        View view = inflater.inflate(R.layout.fragment_hotel_detail, container, false);
        hotelList = new ArrayList<>();
        huidlist = new ArrayList<>();
        model = new HotelModel();
        age_child = new ArrayList<>();
        roomPassengerArrayList = new ArrayList<>();
        childList = new ArrayList<>();
        occupancy_child_count_list = new HashMap<>();
        occupancy_adult_count_list = new ArrayList<>();
        child_count_list = new HashMap<>();
        adult_count_list = new ArrayList<>();
        adultsCountList = new ArrayList<>();
        childCountList = new ArrayList<>();
        childCount = new HashMap<>();
        if (getArguments() != null) {
            model = getArguments().getParcelable("model");
            searchJson = getArguments().getString("search_jsn", "");
            uniqueHotelId = getArguments().getString("hotel_id", "");
            totalRoomCount = getArguments().getInt("total_room_cnt", 1);
            childs = getArguments().getInt("child_count", 0);
            adults = getArguments().getInt("adult_count");
            adultsCountList = getArguments().getStringArrayList("adults_list");
            childCountList = getArguments().getStringArrayList("childs_list");
            childCount = (HashMap<String, ArrayList<String>>) getArguments().getSerializable("children_list");
            randomNumber = getArguments().getString(Constants.BOOKING_RANDOM_NUMBER, "");
            getDetailsData = getArguments().getString("isMapFragment", "");
            hotelCheckInDate = getArguments().getString("hotelCheckInDate", "");
            hotelCheckOutDate = getArguments().getString("hotelCheckOutDate", "");
        }
        findViewId(view);
        if (!searchJson.isEmpty()) {
            uniqueSearch(uniqueHotelId);
        } else {
            uniqueSearch(model.getHuid());
        }
        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        boolean isRoomsInfoUpdated = preferences.getBoolean(Constants.IS_ROOM_INFO_UPDATED, false);
        if (!isRoomsInfoUpdated) {
            for (int i = 0; i < Singleton.getInstance().roomPassengerInfoList.size(); i++) {
                FhRoomPassengerDetailsBean bean1 = Singleton.getInstance().roomPassengerInfoList.get(i);
                FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
                bean.setAdultCnt(bean1.getAdultCnt());
                bean.setChildAge(bean1.getChildAge());
                bean.setAge(bean1.getAge());
                bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
                bean.setChildCnt(bean1.getChildCnt());
                bean.setClassType(bean1.getClassType());
                bean.setInfantCnt(bean1.getInfantCnt());
                roomPassengerArrayList.add(bean);
            }
            room_count = Singleton.getInstance().roomPassengerInfoList.size();
        } else {
            for (int i = 0; i < Singleton.getInstance().updatedRoomPassengerInfoList.size(); i++) {
                FhRoomPassengerDetailsBean bean1 = Singleton.getInstance().updatedRoomPassengerInfoList.get(i);
                FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
                bean.setAdultCnt(bean1.getAdultCnt());
                bean.setChildAge(bean1.getChildAge());
                bean.setAge(bean1.getAge());
                bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
                bean.setChildCnt(bean1.getChildCnt());
                bean.setClassType(bean1.getClassType());
                bean.setInfantCnt(bean1.getInfantCnt());
                roomPassengerArrayList.add(bean);
            }
            room_count = Singleton.getInstance().updatedRoomPassengerInfoList.size();
        }
        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        try {
            header_date = getArguments().getString("header_date");
            if (header_date.contains("-")) {
                String[] hotelCheckDate = header_date.split("-");
                hotelInDate = hotelCheckDate[0];
                hotelOutDate = hotelCheckDate[1];
            }
        } catch (Exception e) {
        }
        bnplHeaderText.setText(getString(R.string.label_book_now_pay_later) + " " + getString(R.string.label_lock_great_price));
        guestLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addTravellerDialog = new AddTravellerDialog(mActivity, roomPassengerArrayList, cabinType, isClassTypeNeed);
                addTravellerDialog.setCancelable(true);
                addTravellerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (addTravellerDialog != null) {
                            if (addTravellerDialog.roomPassengerArrayList.size() != 0) {
                                int adultCnt = 0;
                                int childCnt = 0;
                                int infantCnt = 0;
                                for (int i = 0; i < addTravellerDialog.roomPassengerArrayList.size(); i++) {
                                    adultCnt = adultCnt + addTravellerDialog.roomPassengerArrayList.get(i).getAdultCnt();
                                    childCnt = childCnt + addTravellerDialog.roomPassengerArrayList.get(i).getChildCnt();
                                    infantCnt = infantCnt + addTravellerDialog.roomPassengerArrayList.get(i).getInfantCnt();
                                }
                                totalAdultCnt = adultCnt;
                                totalChildCnt = childCnt;
                                totalPassengerCount = adultCnt + childCnt;
                                cabinType = addTravellerDialog.classTypeCode;
                                String roomsCountText = "";
                                String adtCountText = "";
                                if (totalPassengerCount > 1) {
                                    adtCountText = getString(R.string.label_search_flight_travellers);
                                } else {
                                    adtCountText = getString(R.string.label_add_traveller);
                                }
                                if (addTravellerDialog.roomPassengerArrayList.size() > 1) {
                                    roomsCountText = getString(R.string.label_multiple_rooms);
                                } else {
                                    roomsCountText = getString(R.string.label_search_page_room);
                                }
                                guestCountText.setText(addTravellerDialog.roomPassengerArrayList.size() + " " + roomsCountText + ", " + totalPassengerCount + " " + adtCountText);
                            }
                        }
                    }
                });
                addTravellerDialog.show();
                Window window = addTravellerDialog.getWindow();
                int width = (int) (mActivity.getResources().getDisplayMetrics().widthPixels * 1.0);
                int height = (int) (mActivity.getResources().getDisplayMetrics().heightPixels * 0.85);
                window.setLayout(width, height);
            }
        });
        update.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (hotelCheckInDate != null && hotelCheckOutDate != null) {
                    if (hotelCheckInDate.equalsIgnoreCase(hotelCheckOutDate)) {
                        displayErrorMessage(getString(R.string.hotel_same_date_error));
                        return;
                    }
                    try {
                        Date date1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(hotelCheckInDate);
                        Date date2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(hotelCheckOutDate);
                        night_count = (int) ((date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000));
                        if (night_count > 30) {
                            displayErrorMessage(mActivity.getString(R.string.more_than_30_night_error));
                            return;
                        }
                    } catch (ParseException e1) {
                    } catch (Exception e) {
                    }
                }
                if (Utils.isConnectingToInternet(mActivity)) {
                    String jsn = buildJson();
                    if (jsn != null) {
                        showLoading();
                        new HTTPAsync(mActivity, HotelDetailFragment.this, Constants.HOTEL_SEARCH_URL, "",
                                jsn, SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
                    }
                } else {
                    try {
                        layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                        errorText = (TextView) errorView.findViewById(R.id.error_text);
                        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                        searchButton = (Button) errorView.findViewById(R.id.search_button);
                        errorText.setTypeface(regularLight);
                        errorDescriptionText.setTypeface(regularLight);
                        searchButton.setTypeface(regularLight);
                        searchButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                                if (isInternetPresent) {
                                    loadingViewLayout.removeView(errorView);
                                    loadingViewLayout.setVisibility(View.GONE);
                                    update.performClick();
                                }
                            }
                        });
                        loadErrorType(Constants.NETWORK_ERROR);
                        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        loadingViewLayout.addView(errorView);
                        loadingViewLayout.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        check_in.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent it = new Intent(mActivity, CalendarDisplayActivity.class);
                it.putExtra(Constants.TRIP_TYPE, 2);
                it.putExtra(Constants.SELECTED_DEPARTURE_DATE, hotelCheckInDate);
                it.putExtra(Constants.SELECTED_RETURN_DATE, hotelCheckOutDate);
                it.putExtra(Constants.IS_DEPARTURE_SELECTED, true);
                it.putExtra(Constants.IS_HOTEL_DATE_SELECTED, true);
                it.putExtra(Constants.CALENDAR_TYPE, 0);
                it.putExtra(Constants.HOTEL_SEARCH, true);
                mActivity.startActivityForResult(it, ROUND_TRIP);
            }
        });
        check_out.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(mActivity, CalendarDisplayActivity.class);
                it.putExtra(Constants.TRIP_TYPE, 2);
                it.putExtra(Constants.SELECTED_DEPARTURE_DATE, hotelCheckInDate);
                it.putExtra(Constants.SELECTED_RETURN_DATE, hotelCheckOutDate);
                it.putExtra(Constants.IS_DEPARTURE_SELECTED, false);
                it.putExtra(Constants.CALENDAR_TYPE, 0);
                it.putExtra(Constants.IS_HOTEL_DATE_SELECTED, true);
                it.putExtra(Constants.HOTEL_SEARCH, true);
                mActivity.startActivityForResult(it, ROUND_TRIP);
            }
        });
        roomSelect.setChecked(true);
        hotelInfo.setChecked(false);
        viewMap.setChecked(false);
        roomSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("model", model);
                bundle.putInt("adult_count", adults);
                bundle.putInt("child_count", childs);
                bundle.putStringArrayList("adults_list", adultsCountList);
                bundle.putStringArrayList("childs_list", childCountList);
                bundle.putSerializable("children_list", childCount);
                bundle.putString("header_date", header_date);
                bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                Fragment fragment = new RoomSelectedFragment();
                fragment.setArguments(bundle);
                loadFragment(fragment);
                defaultTabSelection();
                roomSelect.setChecked(true);
                hotelInfo.setChecked(false);
                viewMap.setChecked(false);
            }
        });
        hotelInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                roomSelect.setBackgroundResource(R.drawable.select_room_round_unselected);
                roomSelect.setTextColor(Utils.getColor(mActivity, R.color.accent_color));
                hotelInfo.setBackgroundResource(R.drawable.hotel_info_selected);
                hotelInfo.setTextColor(Utils.getColor(mActivity, R.color.white));
                viewMap.setBackgroundResource(R.drawable.view_map_round_unselected);
                viewMap.setTextColor(Utils.getColor(mActivity, R.color.accent_color));
                Bundle bundle = new Bundle();
                bundle.putParcelable("model", model);
                Fragment fragment = new HotelInfoFragment();
                fragment.setArguments(bundle);
                loadFragment(fragment);
                roomSelect.setChecked(false);
                hotelInfo.setChecked(true);
                viewMap.setChecked(false);
            }
        });
        viewMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new NearByMapFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("model", model);
                bundle.putInt("adults", adults);
                bundle.putInt("childs", childs);
                bundle.putStringArrayList("adults_list", adultsCountList);
                bundle.putStringArrayList("childs_list", childCountList);
                bundle.putSerializable("children_list", childCount);
                bundle.putString("header_date", header_date);
                bundle.putString("hotelCheckInDate", model.getCheck_in_date());
                bundle.putString("hotelCheckOutDate", model.getCheck_out_date());
                bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                fragment.setArguments(bundle);
                loadFragment(fragment);
                roomSelect.setBackgroundResource(R.drawable.select_room_round_unselected);
                roomSelect.setTextColor(Utils.getColor(mActivity, R.color.accent_color));
                hotelInfo.setBackgroundResource(R.drawable.hotel_info_unselected);
                hotelInfo.setTextColor(Utils.getColor(mActivity, R.color.accent_color));
                viewMap.setBackgroundResource(R.drawable.view_map_round_selected);
                viewMap.setTextColor(Utils.getColor(mActivity, R.color.white));
                roomSelect.setChecked(false);
                hotelInfo.setChecked(false);
                viewMap.setChecked(true);
            }
        });
        backLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loadingViewLayout.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (handler != null) {
                    handler.removeCallbacksAndMessages(null);
                    slideHandler.removeCallbacks(SlideImageUpdater);
                }
                mActivity.onBackPressed();
            }
        });
        checkPassengerCount();
        edit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loadingViewLayout.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (edit_layout.getVisibility() == View.GONE) {
                    scollview.scrollTo(0, 0);
                    edit.setText(mActivity.getResources().getString(R.string.label_cancel));
                    edit_layout.setVisibility(View.VISIBLE);
                    tempAdult = 0;
                    tempChild = 0;
                    room_count = 0;
                    calendar = Calendar.getInstance();
                    hotelCheckInDate = model.getCheck_in_date();
                    hotelCheckOutDate = model.getCheck_out_date();
                    DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                    Date date = null;
                    try {
                        date = originalFormat.parse(hotelCheckInDate);
                        check_in.setText(targetFormat.format(date));
                        date = originalFormat.parse(hotelCheckOutDate);
                        check_out.setText(targetFormat.format(date));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    checkPassengerCount();
                } else {
                    //scollview.scrollTo(0, 0);
                    edit_layout.setVisibility(View.GONE);
                    edit.setText(mActivity.getResources().getString(R.string.label_edit_btn));
                    night_count = 0;
                    room_count = 1;
                    adult_count_list.clear();
                    child_count_list.clear();
                    hotelList.clear();
                    huidlist.clear();
                    night_count = 0;
                }
            }
        });
        return view;
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        int animDuration = 400;
        try {
            if (mActivity != null) {
                animDuration = mActivity.getResources().getInteger(android.R.integer.config_mediumAnimTime);
            }
        } catch (Exception e) {
        }
        if (errorView != null) {
            if (errorView.getAlpha() == 0) {
                errorView.setTranslationY(-60);
                errorView.setVisibility(View.VISIBLE);
                errorView.animate().translationY(0).alpha(1).setDuration(animDuration);
                if (mActivity != null) {
                    try {
                        handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (errorView != null && mActivity != null && mActivity.getResources() != null) {
                                        int animDur = 400;
                                        try {
                                            if (mActivity != null) {
                                                animDur = mActivity.getResources().getInteger(android.R.integer.config_mediumAnimTime);
                                            }
                                        } catch (Exception e) {
                                        }
                                        errorView.animate().alpha(0).setDuration(animDur);
                                    }
                                } catch (Exception e) {
                                }
                            }
                        }, 2000);
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (imageList != null) {
            viewpager.setAdapter(new DemoPagerAdapter(imageList, mActivity));
            indicator.setViewPager(viewpager);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (isAdded())
            mActivity.invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void loadFragment(Fragment fragment) {
        if (fragment == null && !isAdded())
            return;
        try {
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.container_body, fragment);
            ft.commit();
        } catch (Exception e) {
        }
    }

    public String buildJson() {
        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
        if (tokenTime == -1) {
            getTokenFromServer();
            return null;
        } else {
            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
            if (diff > Long.parseLong(expireIn)) {
                getTokenFromServer();
                return null;
            } else {
                accessTocken = preferences.getString(Constants.ACCESS_TOKEN, "0");
            }
        }
        if (hotelCheckInDate != null && hotelCheckOutDate != null) {
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(hotelCheckInDate);
                Date date2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(hotelCheckOutDate);
                night_count = (int) ((date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000));
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }
        if (night_count > 0) {
            JSONObject obj = new JSONObject();
            String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
            String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
            String timeStamp = currentDateFormat + "T" + currentTimeFormat;
            SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            String userSelectedCurr = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
            String groupType = null;
            if (pref.getBoolean(Constants.isFanClub, false)) {
                groupType = "ALHILAL";
            }
            try {
                JSONObject source = new JSONObject();
                source = Utils.getHotelSearchAppSettingData(source, mActivity);
                source.put("clientId", Constants.HOTEL_CLIENT_ID);
                source.put("clientSecret", Constants.HOTEL_CLIENT_SECRET);
                source.put("paxNationality", Utils.getUserCountry(mActivity));
                source.put("echoToken", randomNumber);
                source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
                source.put("accessToken", Utils.getRequestAccessToken(mActivity));
                source.put("timeStamp", timeStamp);
                source.put("currency", userSelectedCurr);
                source.put("groupType", groupType);
                JSONObject sourceRQ = new JSONObject();
                sourceRQ.put("source", source);
                JSONObject sc = new JSONObject();
                sc.put("ad", Utils.formatDateToServerDateFormat(hotelCheckInDate));
                sc.put("dur", night_count);
                sc.put("uids", model.getHuid());
                sc.put("skm", "false");
                sourceRQ.put("sc", sc);
                JSONObject rms = new JSONObject();
                rms.put("rm", addRoomsArray(roomPassengerArrayList.size(), roomPassengerArrayList));
                roomJsonObj.put("rm", addRoomsArray(roomPassengerArrayList.size(), roomPassengerArrayList));
                sourceRQ.put("rms", rms);
                obj.put("searchRQ", sourceRQ);
            } catch (Exception e) {
                Utils.printMessage(TAG, "error" + e.getMessage());
                e.printStackTrace();
            }
            Utils.printMessage(TAG, "json" + obj.toString());
            return obj.toString();
        } else {
            Utils.printMessage(TAG, "night count is zero going back");
            mActivity.onBackPressed();
        }
        return null;
    }

    private void getTokenFromServer() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(mActivity));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(mActivity, HotelDetailFragment.this, Constants.CLIENT_AUTHENTICATION,
                "", obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    public String makeJson() {
        String result = null;
        final SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String accessTocken = preferences.getString(Constants.ACCESS_TOKEN, "0");
        String language = preferences.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
        JSONObject source = new JSONObject();
        JSONObject crt = new JSONObject();
        JSONObject hcp = new JSONObject();
        JSONObject finaldata = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        try {
            source.put("device", Constants.DEVICE_TYPE);
            source.put("clientId", Constants.CLIENT_ID);
            source.put("clientSecret", Constants.CLIENT_SECRET);
            source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
            source.put("accessToken", Utils.getRequestAccessToken(mActivity));
            source.put("timeStamp", timeStamp);
            JSONArray mJSONArray = new JSONArray(huidlist);
            crt.put("hcid", cityId);
            crt.put("language", language);
            crt.put("huid", mJSONArray);
            crt.put("pt", "B|F");
            hcp.put("source", source);
            hcp.put("crt", crt);
            finaldata.put("hcp", hcp);
            return finaldata.toString();
        } catch (JSONException e) {
        } catch (Exception e) {
        }
        return result;
    }

    public void parsedaa(String data) {
        hotelList = new ArrayList<>();
        huidlist = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(data);
            JSONObject searchRSObj = obj.optJSONObject("searchRS");
            if (!searchRSObj.has("hotel") && (searchRSObj.optString("er") != null || searchRSObj.optString("err") != null)) {
                if (mActivity != null) {
                    mActivity.onBackPressed();
                    SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    if (searchRSObj.has("err")) {
                        displayErrorMessage(getString(R.string.label_selected_room_error_msg));
                        editor.putString("no_hotel_msg", mActivity.getResources().getString(R.string.selected_hotel_not_available_error).toString());
                    } else {
                        editor.putString("no_hotel_msg", "");
                    }
                    editor.apply();
                }
            }
            if (searchRSObj.has("err")) {
                displayErrorMessage(getString(R.string.label_selected_room_error_msg));
            } else {
                model = new HotelModel();
                SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                boolean isRoomsInfoUpdated = preferences.getBoolean(Constants.IS_ROOM_INFO_UPDATED, false);
                String updatedHotelInDate = preferences.getString("update_in_date", "");
                String updatedHotelOutDate = preferences.getString("update_out_date", "");
                if (!isRoomsInfoUpdated) {
                    model.setAdultcountlist(adultsCountList);
                    model.setChild_count_list(childCount);
                    model.setCheck_in_date(hotelCheckInDate);
                    model.setCheck_out_date(hotelCheckOutDate);
                } else {
                    model.setCheck_in_date(updatedHotelInDate);
                    model.setCheck_out_date(updatedHotelOutDate);
                }
                JSONArray arr = searchRSObj.getJSONArray("hotel");
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject rcObj = arr.optJSONObject(i);
                    JSONArray rcArr = rcObj.getJSONArray("rc");//
                    HashMap<String, ArrayList<RoomModel>> rcModelArrayList = new HashMap<>();
                    double tempRoomPrice = -1;
                    ArrayList<RoomModel> roomlist = new ArrayList<>();
                    for (int j = 0; j < rcArr.length(); j++) {
                        JSONObject rObj = rcArr.optJSONObject(j);
                        model.setHotelJson(rObj.toString());
                        JSONObject pObj = rObj.optJSONObject("p");
                        model.setDur(rObj.optString("dur"));
                        model.setP(pObj.optString("val"));
                        model.setCur(pObj.optString("cur"));
                        JSONObject wdObj = rObj.optJSONObject("wdp");
                        model.setWdp(wdObj.optString("val"));
                        JSONObject htObj = rObj.optJSONObject("ht");
                        model.setHotelTypeValue(htObj.toString());
                        model.setHuid(htObj.optString("uid"));
                        JSONArray roomArray = rObj.getJSONArray("room");
                        for (int k = 0; k < roomArray.length(); k++) {
                            RoomModel roomModel = new RoomModel();
                            roomModel.setHotelrequestJson(rObj.toString());
                            JSONObject roomObj = roomArray.optJSONObject(k);
                            roomModel.setRoom_json(roomObj.toString());
                            roomModel.setNight_count(roomObj.optString("dur"));
                            roomModel.setPrice(model.getP());
                            roomModel.setCurrency(model.getCur());
                            roomModel.setWdp(model.getWdp());
                            roomModel.setBrakfast(roomObj.optString("mn"));
                            roomModel.setRoom_type(roomObj.optString("rn"));
                            JSONObject ocp = roomObj.getJSONObject("ocp");
                            if (ocp.has("na")) {
                                roomModel.setNa(ocp.getInt("na"));
                            }
                            if (ocp.has("nc")) {
                                roomModel.setNc(ocp.getInt("nc"));
                            } else {
                                roomModel.setNc(0);
                            }
                            if (tempRoomPrice == -1) {
                                tempRoomPrice = Double.parseDouble(model.getP());
                                roomModel.setHotelRoomSortPrice(tempRoomPrice);
                            } else {
                                if (tempRoomPrice < Double.parseDouble(model.getP())) {
                                    tempRoomPrice = Double.parseDouble(model.getP());
                                    roomModel.setHotelRoomSortPrice(tempRoomPrice);
                                }
                            }
                            JSONObject piRoomObj = roomObj.optJSONObject("pi");
                            if (piRoomObj != null) {
                                if (piRoomObj.has("FreeCancellationDate")) {
                                    roomModel.setFree_cancel_date(piRoomObj.optString("FreeCancellationDate"));
                                }
                            }
                            roomModel.setRcObject("rc");
                            roomlist.add(roomModel);
                            ArrayList<RoomModel> list = rcModelArrayList.get(roomModel.getRoom_type());
                            if (list == null) {
                                list = new ArrayList<>();
                            }
                            if (!list.contains(roomModel))
                                list.add(roomModel);
                            rcModelArrayList.put(roomModel.getRoom_type(), list);
                        }

                    }
                    model.setRoomlist(roomlist);
                    JSONObject rmsObj = rcObj.optJSONObject("rms");
                    if (rmsObj != null) {
                        JSONArray rmsArr = rmsObj.getJSONArray("hrm");
                        for (int j = 0; j < rmsArr.length(); j++) {
                            RoomModel roomModel = new RoomModel();
                            JSONObject rObj = rmsArr.optJSONObject(j);
                            roomModel.setHotelrequestJson(rObj.toString());
                            JSONObject roomObj = rObj.getJSONObject("room");
                            roomModel.setRoom_json(roomObj.toString());
                            roomModel.setNight_count(roomObj.optString("dur"));
                            JSONObject pRoomObj = roomObj.optJSONObject("p");
                            roomModel.setPrice(pRoomObj.optString("val"));
                            roomModel.setCurrency(pRoomObj.optString("cur"));
                            JSONObject wdpRoomObj = roomObj.optJSONObject("wdp");
                            roomModel.setWdp(wdpRoomObj.optString("val"));
                            roomModel.setBrakfast(roomObj.optString("mn"));
                            roomModel.setRoom_type(roomObj.optString("rn"));
                            JSONObject htObj = rObj.optJSONObject("ht");
                            roomModel.setHotelJson(htObj.toString());
                            JSONObject ocp = roomObj.getJSONObject("ocp");
                            if (ocp.has("na")) {
                                roomModel.setNa(ocp.getInt("na"));
                            }
                            if (ocp.has("nc")) {
                                roomModel.setNc(ocp.getInt("nc"));
                            } else {
                                roomModel.setNc(0);
                            }
                            if (tempRoomPrice == -1) {
                                tempRoomPrice = Double.parseDouble(roomModel.getPrice()) * totalRoomCount;
                                roomModel.setHotelRoomSortPrice(tempRoomPrice);
                            } else {
                                if (tempRoomPrice < (Double.parseDouble(roomModel.getPrice()) * totalRoomCount)) {
                                    tempRoomPrice = Double.parseDouble(roomModel.getPrice()) * totalRoomCount;
                                    roomModel.setHotelRoomSortPrice(tempRoomPrice);
                                }
                            }
                            JSONObject piRoomObj = roomObj.optJSONObject("pi");
                            if (piRoomObj != null) {
                                if (piRoomObj.has("FreeCancellationDate")) {
                                    roomModel.setFree_cancel_date(piRoomObj.optString("FreeCancellationDate"));
                                }
                            }
                            roomModel.setRcObject("rms");
                            roomlist.add(roomModel);
                            ArrayList<RoomModel> list = rcModelArrayList.get(roomModel.getRoom_type());
                            if (list == null) {
                                list = new ArrayList<>();
                            }
                            if (!list.contains(roomModel)) {
                                list.add(roomModel);
                            }
                            rcModelArrayList.put(roomModel.getRoom_type(), list);
                        }
                        model.setRoomlist(roomlist);
                    }
                    model.setRcModelArrayList(rcModelArrayList);
                    huidlist.add(model.getHuid());
                    hotelList.add(model);
                }
                hotelInfo.setBackgroundResource(R.drawable.hotel_info_unselected);
                if (isAdded())
                    hotelInfo.setTextColor(Utils.getColor(mActivity, R.color.accent_color));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mActivity != null) {
            SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            String errorMessageAlert = pref.getString("errorMessage", "");
            if (!errorMessageAlert.isEmpty()) {
                displayErrorMessage(getString(R.string.label_selected_room_error_msg));
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("errorMessage", "");
                editor.apply();
            }
        }
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            try {
                layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(regularLight);
                errorDescriptionText.setTypeface(regularLight);
                searchButton.setTypeface(regularLight);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadingViewLayout.removeView(errorView);
                        loadingViewLayout.setVisibility(View.GONE);
                        Intent intent = new Intent(mActivity, MainSearchActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
                loadingViewLayout.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (serviceType == GET_ACCESS_TOKEN) {
                Utils.printMessage(TAG, "GET_ACCESS_TOKEN" + data);
                JSONObject obj;
                try {
                    obj = new JSONObject(data);
                    String accessToken = obj.getString("accessToken");
                    String expireIn = obj.getString("expireIn");
                    String refreshToken = obj.getString("refreshToken");
                    long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                    SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Constants.ACCESS_TOKEN, accessToken);
                    editor.putString(Constants.EXPIRE_IN, expireIn);
                    editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                    editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                    editor.commit();
                    update.performClick();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (serviceType == SEARCH_HOTEL) {
                Utils.printMessage(TAG, "SEARCH_HOTEL" + data);
                try {
                    JSONObject dataJson = new JSONObject(data);
                    if (dataJson.has("searchRS")) {
                        if (dataJson.getJSONObject("searchRS").has("err")) {
                            loadingViewLayout.removeAllViews();
                            loadingViewLayout.setVisibility(View.GONE);
                            displayErrorMessage(getString(R.string.label_selected_room_error_msg));
                            return;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    header_date = Utils.convertToJourneyDate(hotelCheckInDate, mActivity) + "-" + Utils.convertToJourneyDate(hotelCheckOutDate, mActivity);
                } catch (Exception e) {
                }
                String[] hotelCheckDate = header_date.split("-");
                hotelInDate = hotelCheckDate[0];
                hotelOutDate = hotelCheckDate[1];
                model.getAdultcountlist().clear();
                int noOfAdults = 0;
                int noOfChildrens = 0;
                Singleton.getInstance().updatedRoomPassengerInfoList.clear();
                for (int i = 0; i < roomPassengerArrayList.size(); i++) {
                    FhRoomPassengerDetailsBean bean1 = roomPassengerArrayList.get(i);
                    FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
                    bean.setAdultCnt(bean1.getAdultCnt());
                    bean.setChildAge(bean1.getChildAge());
                    bean.setAge(bean1.getAge());
                    bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
                    bean.setChildCnt(bean1.getChildCnt());
                    bean.setClassType(bean1.getClassType());
                    bean.setInfantCnt(bean1.getInfantCnt());
                    Singleton.getInstance().updatedRoomPassengerInfoList.add(bean);
                }
                if (Singleton.getInstance().updatedRoomPassengerInfoList.size() == 1) {
                    noOfAdults = Singleton.getInstance().updatedRoomPassengerInfoList.get(0).getAdultCnt();
                    noOfChildrens = Singleton.getInstance().updatedRoomPassengerInfoList.get(0).getChildCnt();
                    ArrayList<String> adultcount = occupancy_adult_count_list;
                    model.setAdultcountlist(adultcount);
                    if (noOfChildrens > 0) {
                        model.getChild_count_list().clear();
                        model.setChild_count_list(occupancy_child_count_list);
                    }
                } else {
                    for (int i = 0; i < Singleton.getInstance().updatedRoomPassengerInfoList.size(); i++) {
                        int occupancy = Singleton.getInstance().updatedRoomPassengerInfoList.get(i).getAdultCnt() + Singleton.getInstance().updatedRoomPassengerInfoList.get(i).getChildCnt();
                        if (noOfAdults < occupancy) {
                            noOfAdults = occupancy;
                        }
                    }
                    ArrayList<String> adultcount = occupancy_adult_count_list;
                    model.setAdultcountlist(adultcount);
                }
                if (model.getAdultcountlist().size() == 0) {
                    if (roomPassengerArrayList.size() == 1) {
                        ArrayList<String> adultcount = new ArrayList<>();
                        adultcount.add(String.valueOf(roomPassengerArrayList.get(0).getAdultCnt()));
                        model.setAdultcountlist(adultcount);
                        int noOfChds = roomPassengerArrayList.get(0).getChildCnt();
                        age_child = new ArrayList<>();
                        if (noOfChds > 0) {
                            for (int j = 0; j < noOfChds; j++) {
                                age_child.add(String.valueOf(roomPassengerArrayList.get(0).getChildAgeArray().get(j)));
                            }
                        }
                        occupancy_child_count_list.put("room1", age_child);
                        model.setChild_count_list(occupancy_child_count_list);
                    } else {
                        int noOfAdts = 0;
                        for (int i = 0; i < roomPassengerArrayList.size(); i++) {
                            int occupancy = roomPassengerArrayList.get(i).getAdultCnt() + roomPassengerArrayList.get(i).getChildCnt();
                            if (noOfAdts < occupancy) {
                                noOfAdts = occupancy;
                            }
                        }
                        ArrayList<String> adultcount = new ArrayList<>();
                        for (int i = 0; i < roomPassengerArrayList.size(); i++) {
                            adultcount.add(String.valueOf(noOfAdts));
                        }
                        model.setAdultcountlist(adultcount);
                    }
                }
                SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(Constants.IS_ROOM_INFO_UPDATED, true);
                editor.putString("room_object", roomJsonObj.toString());
                editor.putString("update_in_date", hotelCheckInDate);
                editor.putString("update_out_date", hotelCheckOutDate);
                editor.putString("update_stay_nights", String.valueOf(checkDateDiff(hotelCheckInDate, hotelCheckOutDate)));
                editor.apply();
                parsedaa(data);
                String jsn = makeJson();
                if (jsn != null) {
                    new HTTPAsync(mActivity, HotelDetailFragment.this, Constants.HOTEL_INFO_RQ_URL, "", jsn, 1,
                            HTTPAsync.METHOD_POST).execute();
                }
            } else if (serviceType == UNIQUE_SEARCH_HOTEL) {
                Utils.printMessage(TAG, "unique_data" + data);
                parsedaa(data);
                String jsn = makeJson();
                if (jsn != null) {
                    new HTTPAsync(mActivity, HotelDetailFragment.this, Constants.HOTEL_INFO_RQ_URL, "", jsn, 1,
                            HTTPAsync.METHOD_POST).execute();
                }
            } else {
                Utils.printMessage(TAG, "Static Search :: " + data);
                try {
                    loadingViewLayout.removeAllViews();
                    loadingViewLayout.setVisibility(View.GONE);
                    JSONArray arr = new JSONArray(data);
                    for (int i = 0; i < arr.length(); i++) {
                        HotelModel model = hotelList.get(i);
                        JSONObject obj = arr.optJSONObject(i);
                        String error = obj.optString("error");
                        if (error.equalsIgnoreCase("data not found")) {
                            displayErrorMessage(getString(R.string.label_selected_room_error_msg));
                            return;
                        }
                        JSONObject hotel = obj.optJSONObject("hotel");
                        JSONObject bacickInfo = hotel.optJSONObject("basicInfo");
                        JSONObject geocode = bacickInfo.optJSONObject("geoCode");
                        model.setLatitude(geocode.optString("longitude"));
                        model.setLongitude(geocode.optString("latitude"));
                        JSONObject distric = bacickInfo.optJSONObject("district");
                        JSONObject contact = bacickInfo.optJSONObject("contactInfo");
                        model.setHemail(contact.optString("email"));
                        model.setHfaxCode(contact.optString("faxCode"));
                        model.setHfaxNo(contact.optString("fax"));
                        model.setHphoneCode(contact.optString("phoneCode"));
                        model.setHphoneNo(contact.optString("phone"));
                        JSONObject city = bacickInfo.optJSONObject("city");
                        model.setDistric(distric.getString("name"));
                        model.setCity(city.optString("name"));
                        model.setCityId(city.optString("id"));
                        JSONObject chainName = bacickInfo.optJSONObject("chain");
                        model.setChainName(chainName.getString("name"));
                        JSONObject hotelType = bacickInfo.optJSONObject("hotelType");
                        if (hotelType != null) {
                            Iterator<String> hotel_key = hotelType.keys();
                            String hotel_type = "";
                            while (hotel_key.hasNext()) {
                                String key = hotel_key.next();
                                try {
                                    if (hotel_type.length() > 0) {
                                        hotel_type = hotel_type + ",";
                                    }
                                    hotel_type = hotel_type + hotelType.getString(key);
                                } catch (JSONException e) {
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            model.setHotel_type(hotel_type);
                        }
                        model.setRoom_count("" + this.room_count);
                        JSONObject country = bacickInfo.optJSONObject("country");
                        model.setCountry(country.optString("name"));
                        model.setImage(bacickInfo.optString("mainImage"));
                        model.setStar(bacickInfo.optString("starRating"));
                        model.setFullAddress(bacickInfo.optString("addressText"));
                        model.setHna(bacickInfo.optString("name"));
                        JSONObject imageObj = hotel.optJSONObject("images");
                        JSONArray imageArr = imageObj.optJSONArray("others");
                        String[] imagelist = new String[imageArr.length()];
                        for (int j = 0; j < imageArr.length(); j++) {
                            imagelist[j] = imageArr.getString(j);
                        }
                        model.setImagelist(imagelist);
                        JSONObject policy = hotel.optJSONObject("policy");
                        String cancel_policy = policy.optString("cancellationPolicy");
                        if (cancel_policy != null) {
                            model.setCancel_policy(cancel_policy);
                        }
                        String child_policy = policy.optString("childPolicy");
                        if (child_policy != null) {
                            model.setChild_policy(child_policy);
                        }
                        String pet_policy = policy.optString("petPolicy");
                        if (pet_policy != null) {
                            model.setPet(pet_policy);
                        }
                        String check_out = policy.optString("check_out");
                        if (check_out != null) {
                            model.setCheck_out(check_out);
                        }
                        String check_in = policy.optString("check_in");
                        if (check_in != null) {
                            model.setCheck_in(check_in);
                        }
                        JSONObject description = hotel.optJSONObject("descriptions");
                        String lobby = description.optString("lobby");
                        if (lobby != null) {
                            model.setLobby(lobby);
                        }
                        String exterior = description.optString("exterior");
                        if (exterior != null) {
                            model.setExterior(exterior);
                        }
                        String rooms = description.optString("aboutHotel");
                        if (rooms != null) {
                            model.setRooms(rooms);
                        }
                        JSONObject facilities = hotel.optJSONObject("facilities");
                        JSONObject activity = facilities.optJSONObject("activities");
                        int k = 0;
                        if (activity != null) {
                            Iterator<String> activity_key = activity.keys();
                            String[] activitylist = new String[activity.length()];
                            while (activity_key.hasNext()) {
                                String key = activity_key.next();
                                try {
                                    String value = activity.getString(key);
                                    activitylist[k] = value;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                k++;
                            }
                            model.setActivity(activitylist);
                        }
                        JSONObject service = facilities.optJSONObject("services");
                        if (service != null) {
                            Iterator<String> service_key = service.keys();
                            String[] servicelist = new String[service.length()];
                            k = 0;
                            while (service_key.hasNext()) {
                                String key = service_key.next();
                                try {
                                    String value = service.getString(key);
                                    servicelist[k] = value;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                k++;
                            }
                            model.setService(servicelist);
                        }
                        JSONObject general = facilities.optJSONObject("activities");
                        if (general != null) {
                            Iterator<String> general_key = general.keys();
                            String[] generallist = new String[general.length()];
                            k = 0;
                            while (general_key.hasNext()) {
                                String key = general_key.next();
                                try {
                                    String value = general.getString(key);
                                    generallist[k] = value;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                k++;
                            }
                            model.setGeneral(generallist);
                        }
                        hotelList.set(i, model);
                        loadingViewLayout.removeAllViews();
                        loadingViewLayout.setVisibility(View.GONE);
                        this.model = model;
                        updateHotelDetails();
                        adults = tempAdult;
                        childs = tempChild;
                        refreshFragment();
                        defaultTabSelection();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("model", model);
                        bundle.putInt("adult_count", adults);
                        bundle.putInt("child_count", childs);
                        bundle.putStringArrayList("adults_list", adultsCountList);
                        bundle.putStringArrayList("childs_list", childCountList);
                        bundle.putSerializable("children_list", childCount);
                        bundle.putString("header_date", header_date);
                        bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                        Fragment fragment = new RoomSelectedFragment();
                        fragment.setArguments(bundle);
                        loadFragment(fragment);
                    }
                } catch (JSONException e) {
                    Utils.printMessage(TAG, "error" + e.getMessage());
                    closeLoading();
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        if (loadingViewLayout != null) {
            loadingViewLayout.removeAllViews();
            loadingViewLayout.setVisibility(View.GONE);
        }
    }

    public void refreshFragment() {
        if (model != null) {
            imageList = model.getImagelist();
        }
        hotelCheckInDate = null;
        hotelCheckOutDate = null;
        night_count = 0;
        room_count = 1;
        if (model != null) {
            String hotelAddress = (model.getFullAddress()).replace("\n", " ");
            address.setText(hotelAddress);
            hotel_name.setText(model.getHna());
            float star = Float.parseFloat(model.getStar());
            if (star != 0.0) {
                hotel_rating.setVisibility(View.VISIBLE);
                hotel_rating.setRating(Float.parseFloat(model.getStar()));
            }
            SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            String updatedHotelInDate = preferences.getString("update_in_date", "");
            String updatedHotelDuration = preferences.getString("update_stay_nights", "");
            if (updatedHotelInDate.isEmpty()) {
                if (model.getCheck_in() == null || model.getCheck_in().equalsIgnoreCase("null")) {
                    checkInTime.setText(hotelInDate);
                } else {
                    checkInTime.setText(hotelInDate + " " + model.getCheck_in());
                }
                if (model.getCheck_out() == null || model.getCheck_out().equalsIgnoreCase("null")) {
                    checkOutTime.setText(hotelOutDate);
                } else {
                    checkOutTime.setText(hotelOutDate + "  " + model.getCheck_out());
                }
            } else {
                if (model.getCheck_in() == null || model.getCheck_in().equalsIgnoreCase("null")) {
                    checkInTime.setText(Utils.convertToJourneyDate(updatedHotelInDate, mActivity));
                } else {
                    checkInTime.setText(Utils.convertToJourneyDate(updatedHotelInDate, mActivity) + " " + model.getCheck_in());
                }
                String outDate = Utils.addReviewDaysToCalendar(updatedHotelInDate, updatedHotelDuration);
                if (model.getCheck_out() == null || model.getCheck_out().equalsIgnoreCase("null")) {
                    checkOutTime.setText(Utils.convertToJourneyDate(outDate, mActivity));
                } else {
                    checkOutTime.setText(Utils.convertToJourneyDate(outDate, mActivity) + "  " + model.getCheck_out());
                }
            }
        }
        edit_layout.setVisibility(View.GONE);
        if (isAdded()) {
            edit.setText(mActivity.getString(R.string.label_edit_btn));
            hotelInfo.setBackgroundResource(R.drawable.hotel_info_unselected);
            hotelInfo.setTextColor(Utils.getColor(mActivity, R.color.accent_color));
            if (imageList != null) {
                viewpager.setAdapter(new DemoPagerAdapter(imageList, mActivity));
                indicator.setViewPager(viewpager);
                slideHandler.removeCallbacks(SlideImageUpdater);
                SlideImageUpdater.run();
            }
            if (model != null) {
                for (int i = 0; i < model.getRoomlist().size(); i++) {
                    String cancel_date = model.getRoomlist().get(i).getFree_cancel_date();
                    if (cancel_date != null && !cancel_date.equalsIgnoreCase("null") && cancel_date.length() > 0) {
                        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
                        Date dateBefore = null;
                        try {
                            dateBefore = simpleDateFormat.parse(cancel_date);
                        } catch (Exception e) {
                        }
                        SimpleDateFormat finalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                        String finalDate = finalFormat.format(dateBefore);
                        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
                        String CancellationDateDiff = Utils.flightSearchDateDifference(finalDate, currentDate);
                        if (Integer.parseInt(CancellationDateDiff) > 0) {
                            bnplPackageLayout.setVisibility(View.VISIBLE);
                        } else {
                            bnplPackageLayout.setVisibility(View.GONE);
                        }
                    }
                }
            }
        }
    }

    private int sliderIndex = 0;
    private int userSelectedIndex = 0;

    private void updateSliderImages() {
        userSelectedIndex = viewpager.getCurrentItem();
        if (sliderIndex < userSelectedIndex) {
            sliderIndex = userSelectedIndex;
        }
        if (sliderIndex >= imageList.length) {
            sliderIndex = 0;
        }
        viewpager.setCurrentItem(sliderIndex);
        sliderIndex++;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ROUND_TRIP) {
            if (resultCode == mActivity.RESULT_OK) {
                if (data != null) {
                    int day = data.getExtras().getInt(Constants.DEPARTURE_DAY, 0);
                    int month = data.getExtras().getInt(Constants.DEPARTURE_MONTH, 0);
                    int year = data.getExtras().getInt(Constants.DEPARTURE_YEAR, 0);
                    if (calendar == null) {
                        Utils.printMessage(TAG, "Calendar is getting null");
                        calendar = Calendar.getInstance();
                    }
                    calendar.set(year, month - 1, day);
                    hotelCheckInDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                    check_in.setText(new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(calendar.getTime()));
                    day = data.getExtras().getInt(Constants.RETURN_DAY, 0);
                    month = data.getExtras().getInt(Constants.RETURN_MONTH, 0);
                    year = data.getExtras().getInt(Constants.RETURN_YEAR, 0);
                    calendar.set(year, month - 1, day);
                    hotelCheckOutDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                    check_out.setText(new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(calendar.getTime()));
                }
            }
        }
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(mActivity.getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_network_error_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(mActivity.getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_no_results_found_message_hotels));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_search_text));
                break;
            case Constants.SELECTED_HOTEL_NOT_FOUND_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.selected_hotel_not_available_error));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    public void uniqueSearch(final String hotelId) {
        if (Utils.isConnectingToInternet(mActivity)) {
            String jsn = null;
            jsn = getUniqueRequest(hotelId);
            Utils.printMessage(TAG, "unique search request " + jsn + " Hotel Id :: " + hotelId);
            if (jsn != null) {
                showLoading();
                new HTTPAsync(mActivity, HotelDetailFragment.this, Constants.HOTEL_SEARCH_URL, "",
                        jsn, UNIQUE_SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
            }
        } else {
            try {
                layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(regularLight);
                errorDescriptionText.setTypeface(regularLight);
                searchButton.setTypeface(regularLight);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                        if (isInternetPresent) {
                            loadingViewLayout.removeView(errorView);
                            loadingViewLayout.setVisibility(View.GONE);
                            String jsn = null;
                            jsn = getUniqueRequest(hotelId);
                            Utils.printMessage(TAG, "unique search request " + jsn + " Hotel Id :: " + hotelId);
                            if (jsn != null) {
                                showLoading();
                                new HTTPAsync(mActivity, HotelDetailFragment.this, Constants.HOTEL_SEARCH_URL, "",
                                        jsn, UNIQUE_SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
                            }
                        }
                    }
                });
                loadErrorType(Constants.NETWORK_ERROR);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
                loadingViewLayout.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getUniqueRequest(String uid) {
        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String jsonString = preferences.getString(Constants.REQUEST_JSON, null);
        boolean isRoomsInfoUpdated = preferences.getBoolean(Constants.IS_ROOM_INFO_UPDATED, false);
        String roomObject = preferences.getString("room_object", "");
        String hotelInDate = preferences.getString("update_in_date", "");
        String hotelDuration = preferences.getString("update_stay_nights", "");
        if (jsonString != null) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                JSONObject sourceobj = jsonObject.getJSONObject("searchRQ");
                JSONObject scobj = sourceobj.getJSONObject("sc");
                JSONObject rmsObj = sourceobj.getJSONObject("rms");
                scobj.put("uids", uid);
                if (scobj.has("cid")) {
                    scobj.remove("cid");
                }
                if (isRoomsInfoUpdated) {
                    if (!roomObject.isEmpty()) {
                        JSONObject rmsJSON = new JSONObject(roomObject);
                        sourceobj.remove("rms");
                        sourceobj.put("rms", rmsJSON);
                        if (!hotelInDate.isEmpty()) {
                            scobj.put("ad", Utils.formatDateToServerDateFormat(hotelInDate));
                            scobj.put("dur", hotelDuration);
                        }
                    }
                } else {
                    sourceobj.put("rms", rmsObj);
                }
                sourceobj.put("sc", scobj);
                return jsonObject.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void showLoading() {
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView waitText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setTypeface(regularBold);
        waitText.setTypeface(regularLight);
        loadingText.setVisibility(View.GONE);
        waitText.setTextColor(Utils.getColor(mActivity, R.color.black_color));
        waitText.setText(mActivity.getResources().getString(R.string.label_just_a_moment));
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.removeAllViews();
        loadingViewLayout.addView(loadingView);
        loadingViewLayout.setVisibility(View.VISIBLE);
    }

    private void closeLoading() {
        try {
            loadingViewLayout.removeView(loadingView);
            loadingViewLayout.setVisibility(View.GONE);
            loadingView = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        slideHandler.removeCallbacks(SlideImageUpdater);
        hotelCheckInDate = null;
        hotelCheckOutDate = null;
    }

    private void defaultTabSelection() {
        roomSelect.setBackgroundResource(R.drawable.select_room_round_selected);
        roomSelect.setTextColor(Utils.getColor(mActivity, R.color.white));
        hotelInfo.setBackgroundResource(R.drawable.hotel_info_unselected);
        hotelInfo.setTextColor(Utils.getColor(mActivity, R.color.accent_color));
        viewMap.setBackgroundResource(R.drawable.view_map_round_unselected);
        viewMap.setTextColor(Utils.getColor(mActivity, R.color.accent_color));
    }

    private Handler slideHandler = new Handler();
    public Runnable SlideImageUpdater = new Runnable() {

        @Override
        public void run() {
            updateSliderImages();
            slideHandler.postDelayed(SlideImageUpdater, 5000); // 5 seconds
        }
    };

    /**
     * Adding number of Rooms into Rooms Object
     **/
    private JSONArray addRoomsArray(int roomSize, ArrayList<FhRoomPassengerDetailsBean> passengerList) {
        occupancy_adult_count_list.clear();
        occupancy_child_count_list.clear();
        JSONArray rmArr = new JSONArray();
        int noOfAdults = 0;
        int noOfChildrens = 0;
        if (roomSize == 1) {
            noOfAdults = passengerList.get(0).getAdultCnt();
            noOfChildrens = passengerList.get(0).getChildCnt();
        } else {
            for (int i = 0; i < roomSize; i++) {
                int occupancy = passengerList.get(i).getAdultCnt() + passengerList.get(i).getChildCnt();
                if (noOfAdults < occupancy) {
                    noOfAdults = occupancy;
                }
            }
        }
        int passengerID = 0;
        for (int i = 0; i < roomSize; i++) {
            JSONObject roomDetailsObj = new JSONObject();
            JSONArray paxArr = new JSONArray();
            ArrayList<String> age_child = new ArrayList<>();
            try {
                for (int j = 0; j < noOfAdults; j++) {
                    JSONObject paxDetailObj = new JSONObject();
                    passengerID++;
                    paxDetailObj.put("age", 45);
                    paxDetailObj.put("id", passengerID);
                    paxArr.put(paxDetailObj);
                }
                roomDetailsObj.put("na", noOfAdults);
                roomDetailsObj.put("nc", noOfChildrens);
                occupancy_adult_count_list.add(String.valueOf(noOfAdults));
                if (noOfChildrens > 0) {
                    for (int j = 0; j < noOfChildrens; j++) {
                        JSONObject paxDetailObj = new JSONObject();
                        passengerID++;
                        paxDetailObj.put("age", passengerList.get(i).getChildAgeArray().get(j));
                        paxDetailObj.put("id", passengerID);
                        paxArr.put(paxDetailObj);

                        age_child.add(String.valueOf(passengerList.get(i).getChildAgeArray().get(j)));
                    }
                }
                occupancy_child_count_list.put("room" + String.valueOf(i + 1), age_child);
                roomDetailsObj.put("pax", paxArr);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            rmArr.put(roomDetailsObj);
        }
        room_count = roomSize;
        for (int i = 0; i < roomSize; i++) {
            int childCount = 0;
            ArrayList<String> age_child = new ArrayList<>();
            childCount = passengerList.get(i).getChildCnt();
            adult_count_list.add(String.valueOf(passengerList.get(i).getAdultCnt()));
            if (childCount > 0) {
                childList.add(String.valueOf(passengerList.get(i).getChildCnt()));
            }
            if (childCount > 0) {
                for (int j = 0; j < childCount; j++) {
                    age_child.add(String.valueOf(passengerList.get(i).getChildAgeArray().get(j)));
                }
            }
            child_count_list.put("room" + String.valueOf(i + 1), age_child);
        }
        return rmArr;
    }

    private void checkPassengerCount() {
        int adultCnt = 0;
        int childCnt = 0;
        int infantCnt = 0;
        for (int i = 0; i < roomPassengerArrayList.size(); i++) {
            adultCnt = adultCnt + roomPassengerArrayList.get(i).getAdultCnt();
            childCnt = childCnt + roomPassengerArrayList.get(i).getChildCnt();
            infantCnt = infantCnt + roomPassengerArrayList.get(i).getInfantCnt();
        }
        totalAdultCnt = adultCnt;
        totalChildCnt = childCnt;
        totalPassengerCount = adultCnt + childCnt;
        String roomsCountText = "";
        String adtCountText = "";
        if (totalPassengerCount > 1) {
            adtCountText = getString(R.string.label_search_flight_travellers);
        } else {
            adtCountText = getString(R.string.label_add_traveller);
        }
        if (roomPassengerArrayList.size() > 1) {
            roomsCountText = getString(R.string.label_multiple_rooms);
        } else {
            roomsCountText = getString(R.string.label_search_page_room);
        }
        guestCountText.setText(roomPassengerArrayList.size() + " " + roomsCountText + ", " + totalPassengerCount + " " + adtCountText);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        Utils.printMessage(TAG, "onAttach calling");
    }

    private int checkDateDiff(String inDate, String outDate) {
        int resultDays = 0;
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(inDate);
            Date date2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(outDate);
            resultDays = (int) ((date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000));
        } catch (Exception e) {
        }
        return resultDays;
    }

    private void findViewId(View view) {
        scollview = (ScrollView) view.findViewById(R.id.scroll_view);
        TextView backButton = (TextView) view.findViewById(R.id.img_back);
        address = (TextView) view.findViewById(R.id.address);
        hotel_name = (TextView) view.findViewById(R.id.txt_city);
        hotel_rating = (RatingBar) view.findViewById(R.id.ratingBar);
        check_in = (TextView) view.findViewById(R.id.date1);
        check_out = (TextView) view.findViewById(R.id.date2);
        checkInTime = (TextView) view.findViewById(R.id.check_in_time);
        checkOutTime = (TextView) view.findViewById(R.id.check_out_time);
        roomSelect = (RadioButton) view.findViewById(R.id.tabs_room_select);
        hotelInfo = (RadioButton) view.findViewById(R.id.tabs_hotel_info);
        viewMap = (RadioButton) view.findViewById(R.id.tabs_view_map);
        TextView txt_date = (TextView) view.findViewById(R.id.txt_date);
        edit = (TextView) view.findViewById(R.id.edit);
        update = (Button) view.findViewById(R.id.update);
        edit_layout = (RelativeLayout) view.findViewById(R.id.edit_layout);
        viewpager = (ViewPager) view.findViewById(R.id.pager_introduction);
        indicator = (CircleIndicator) view.findViewById(R.id.indicator);
        backLayout = (LinearLayout) view.findViewById(R.id.back_layout);
        loadingViewLayout = (RelativeLayout) view.findViewById(R.id.loading_view_layout);
        errorView = (RelativeLayout) view.findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) view.findViewById(R.id.errorMessageText);
        TextView parkingText = (TextView) view.findViewById(R.id.parking);
        TextView wifiText = (TextView) view.findViewById(R.id.wifi);
        TextView swimmingPoolText = (TextView) view.findViewById(R.id.swimming_pool);
        TextView gymText = (TextView) view.findViewById(R.id.gym);
        TextView checkInText = (TextView) view.findViewById(R.id.check_in_text);
        TextView checkOutText = (TextView) view.findViewById(R.id.check_out_text);
        TextView chooseRoomHeader = (TextView) view.findViewById(R.id.choose_your_room);
        TextView checkInHeader = (TextView) view.findViewById(R.id.check_in_header);
        TextView checkOutHeader = (TextView) view.findViewById(R.id.check_out_header);
        guestLayout = (LinearLayout) view.findViewById(R.id.guest_layout);
        TextView guestHeader = (TextView) view.findViewById(R.id.guest_header);
        guestCountText = (TextView) view.findViewById(R.id.guest_count_text);
        bnplPackageLayout = (LinearLayout) view.findViewById(R.id.bnpl_package_layout);
        bnplHeaderText = (TextView) view.findViewById(R.id.book_now_header);
        TextView bnplInfoText = (TextView) view.findViewById(R.id.book_now_description);
        String fontRegularBold = Constants.FONT_ROBOTO_BOLD;
        String fontRegularLight = Constants.FONT_ROBOTO_LIGHT;
        String regularText = Constants.FONT_ROBOTO_REGULAR;
        String mediumText = Constants.FONT_ROBOTO_MEDIUM;
        isArabicLang = false;
        if (Utils.isArabicLangSelected(mActivity)) {
            isArabicLang = true;
            fontRegularBold = Constants.FONT_DROIDKUFI_BOLD;
            fontRegularLight = Constants.FONT_DROIDKUFI_REGULAR;
            regularText = Constants.FONT_DROIDKUFI_REGULAR;
            mediumText = Constants.FONT_DROIDKUFI_REGULAR;
            backButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            hotel_name.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            txt_date.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            edit.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            address.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            parkingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            wifiText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            swimmingPoolText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            gymText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            checkInText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            checkInTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            checkOutText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            checkOutTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            roomSelect.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_time_text));
            hotelInfo.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_time_text));
            viewMap.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_time_text));
            chooseRoomHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            checkInHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            checkOutHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            update.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_hotel_name_text));
            guestHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            guestCountText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            bnplHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            bnplInfoText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_time_text));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backButton.setLayoutParams(params);
        }
        regularLight = Typeface.createFromAsset(mActivity.getAssets(), fontRegularLight);
        regularBold = Typeface.createFromAsset(mActivity.getAssets(), fontRegularBold);
        Typeface textFace = Typeface.createFromAsset(mActivity.getAssets(), regularText);
        Typeface titleFace = Typeface.createFromAsset(mActivity.getAssets(), mediumText);
        address.setTypeface(regularLight);
        hotel_name.setTypeface(regularBold);
        check_in.setTypeface(textFace);
        check_out.setTypeface(textFace);
        roomSelect.setTypeface(regularBold);
        hotelInfo.setTypeface(regularBold);
        txt_date.setTypeface(titleFace);
        edit.setTypeface(textFace);
        update.setTypeface(regularBold);
        viewMap.setTypeface(regularBold);
        errorMessageText.setTypeface(regularLight);
        backButton.setTypeface(textFace);
        chooseRoomHeader.setTypeface(textFace);
        parkingText.setTypeface(textFace);
        wifiText.setTypeface(textFace);
        swimmingPoolText.setTypeface(textFace);
        gymText.setTypeface(textFace);
        checkInText.setTypeface(regularLight);
        checkOutText.setTypeface(regularLight);
        chooseRoomHeader.setTypeface(textFace);
        checkInHeader.setTypeface(regularLight);
        checkOutHeader.setTypeface(regularLight);
        guestHeader.setTypeface(textFace);
        guestCountText.setTypeface(textFace);
        checkInTime.setTypeface(textFace);
        checkOutTime.setTypeface(textFace);
        bnplHeaderText.setTypeface(regularBold);
        bnplInfoText.setTypeface(textFace);
    }

    private void updateHotelDetails() {
        if (model.getAdultcountlist() == null || model.getAdultcountlist().size() == 0 ||
                model.getChild_count_list() == null || model.getChild_count_list().size() == 0) {
            if (roomPassengerArrayList.size() == 1) {
                ArrayList<String> adultcount = new ArrayList<>();
                adultcount.add(String.valueOf(roomPassengerArrayList.get(0).getAdultCnt()));
                model.setAdultcountlist(adultcount);
                int noOfChildrens = roomPassengerArrayList.get(0).getChildCnt();
                age_child = new ArrayList<>();
                if (noOfChildrens > 0) {
                    for (int j = 0; j < noOfChildrens; j++) {
                        age_child.add(String.valueOf(roomPassengerArrayList.get(0).getChildAgeArray().get(j)));
                    }
                }
                occupancy_child_count_list.put("room1", age_child);
                model.setChild_count_list(occupancy_child_count_list);
            } else {
                int noOfAdults = 0;
                for (int i = 0; i < roomPassengerArrayList.size(); i++) {
                    int occupancy = roomPassengerArrayList.get(i).getAdultCnt() + roomPassengerArrayList.get(i).getChildCnt();
                    if (noOfAdults < occupancy) {
                        noOfAdults = occupancy;
                    }
                }
                ArrayList<String> adultcount = new ArrayList<>();
                for (int i = 0; i < roomPassengerArrayList.size(); i++) {
                    adultcount.add(String.valueOf(noOfAdults));
                }
                model.setAdultcountlist(adultcount);
            }
        }
        for (int i = 0; i < model.getRoomlist().size(); i++) {
            String cancel_date = model.getRoomlist().get(i).getFree_cancel_date();
            if (cancel_date != null && !cancel_date.equalsIgnoreCase("null") && cancel_date.length() > 0) {
                DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
                Date dateBefore = null;
                try {
                    dateBefore = simpleDateFormat.parse(cancel_date);
                } catch (Exception e) {
                }
                SimpleDateFormat finalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                String finalDate = finalFormat.format(dateBefore);
                String CancellationDateDiff = Utils.flightSearchDateDifference(finalDate, currentDate);
                if (Integer.parseInt(CancellationDateDiff) > 0) {
                    bnplPackageLayout.setVisibility(View.VISIBLE);
                } else {
                    bnplPackageLayout.setVisibility(View.GONE);
                }
            }
        }
    }
}


