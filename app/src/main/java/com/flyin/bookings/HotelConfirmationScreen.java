package com.flyin.bookings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.model.FhRoomPassengerDetailsBean;
import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.GPSTracker;
import com.flyin.bookings.util.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class HotelConfirmationScreen extends AppCompatActivity implements AsyncTaskListener {
    private static final int GET_BOOK_REQ = 15455;
    private static final int GET_ACCESS_TOKEN = 55454;
    private String bookRefId = "", leadPassengerName = "", emailId = "", phoneNumber = "";
    private TextView thankYouText;
    private TextView bookingConfirmationText;
    private TextView emailSendingText;
    private ImageView hotelImage;
    private TextView hotelName;
    private TextView adressText;
    private TextView phoneText;
    private TextView emailText;
    private TextView bookingNoText;
    private TextView flightNoText;
    private TextView checkinText;
    private TextView checkoutText;
    private TextView resDetailsText;
    private TextView totalPriceText;
    private TextView roomDetails;
    private TextView roomDescription;
    private TextView guestText;
    private TextView roomFacilitiesText;
    private TextView childPolicyText;
    private TextView numberOfCoupon;
    private View loadingView;
    private RelativeLayout loadingViewLayout;
    private TextView errorMessageText;
    private String accessTocken = "";
    private RelativeLayout errorView;
    private String hotelInfoJson;
    private String refId;
    private String hotelJson;
    private String hotelDataJson;
    private HotelModel htModel = null;
    private Typeface regularLight;
    private TextView errorText;
    private ImageView errorImage;
    private TextView errorDescriptionText;
    private TextView searchButton;
    private RatingBar hotel_rating;
    private GPSTracker gps;
    private double currentLatitude = 0.0;
    private double currentLongitude = 0.0;
    private static final String TAG = "HotelConfirmationScreen";
    private TextView checkInTime = null;
    private TextView checkOutTime = null;
    private boolean isArabicLang = false;
    private LinearLayout bnplPackageLayout = null, couponLayout;
    private TextView bnplDataText = null;
    private String currentDate = "";
    private String cancellationDate = "";
    private String bnplBooking = "";
    private String bnplPaymentUrl = "";
    private String cua;
    private LayoutInflater inflater;
    private ArrayList<FhRoomPassengerDetailsBean> roomPassengerArrayList = null;
    private String bookingNoteInfo = "";
    private LinearLayout rewardsLayout;
    private TextView rewardsPointsText;
    private static final int GET_LOYALTY_POINTS = 5;
    private static final int GET_CONFIRM_LOYALTY_RESULT = 6;
    private LinearLayout qitafAmountLayout = null;
    private TextView qitafDiscountValue = null;
    private LinearLayout rewardsRedeemedLayout = null;
    private TextView rewardsRedeemedValue = null;
    private String qitafDiscount = "";
    private String rewardsDiscount = "";
    private String hotelNameInEng = "";
    private String userFullName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_hotel_confirmation);
        findViews();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            refId = bundle.getString("BOOKING_ID", "");
            hotelJson = bundle.getString("HOTEL_OBJ", "");
            hotelDataJson = bundle.getString("BOOKING_DATA_OBJ", "");
            hotelInfoJson = bundle.getString("BOOKING_INFO_OBJ", "");
            bnplBooking = bundle.getString("bnplBookingStatus", "");
            bnplPaymentUrl = bundle.getString("BNPL_PAYMENT_URL", "");
            cua = bundle.getString(Constants.COUPON_AMOUNT, "");
            qitafDiscount = bundle.getString("QITAF_DISCOUNT", "");
            rewardsDiscount = bundle.getString("REWARDS_DISCOUNT", "");
            hotelNameInEng = bundle.getString("eng_hotel_name", "");
            userFullName = bundle.getString(Constants.USER_FULL_NAME, "");
            String hotelModelString = Singleton.getInstance().model;
            Gson gson = new Gson();
            htModel = gson.fromJson(hotelModelString, HotelModel.class);
            Singleton.getInstance().model = "";
            Utils.printMessage(TAG, "bnplBooking :: " + bnplBooking);
            if (htModel == null) {
                finish();
            }
            String requestUserDetailsJSON = makeJson();
            if (Utils.isConnectingToInternet(this)) {
                HTTPAsync async = new HTTPAsync(HotelConfirmationScreen.this, HotelConfirmationScreen.this,
                        Constants.HOTEL_BOOK_RQ, "", requestUserDetailsJSON, GET_BOOK_REQ, HTTPAsync.METHOD_POST);
                async.execute();
                showLoading();
            }
        }
        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        ImageView mImageView = (ImageView) mCustomView.findViewById(R.id.home_icon);
        ImageView mHomeIcon = (ImageView) mCustomView.findViewById(R.id.home_logo);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.btn_singin);
        mTitleTextView.setVisibility(View.GONE);
        mImageView.setVisibility(View.INVISIBLE);
        mHomeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Singleton.getInstance().roomPassengerInfoList.clear();
                Intent intent = new Intent(HotelConfirmationScreen.this, MainSearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        mActionBar.setCustomView(mCustomView, layout);
        mActionBar.setDisplayShowCustomEnabled(true);
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        try {
            int qitafAmount = 0;
            int redeemedAmount = 0;
            if (!qitafDiscount.isEmpty()) {
                if (Double.parseDouble(qitafDiscount) > 0) {
                    qitafAmount = (int) Double.parseDouble(qitafDiscount);
                    qitafAmountLayout.setVisibility(View.VISIBLE);
                } else {
                    qitafAmountLayout.setVisibility(View.GONE);
                }
            }
            if (!rewardsDiscount.isEmpty()) {
                if (Double.parseDouble(rewardsDiscount) > 0.0) {
                    rewardsRedeemedLayout.setVisibility(View.VISIBLE);
                } else {
                    rewardsRedeemedLayout.setVisibility(View.GONE);
                }
            }
            String currencyName = "";
            SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            String userSelectedCurr = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
            String defaultCurr = getString(R.string.label_SAR_currency_name);
            String redeemedConvertedAmount = Utils.convertonly_PriceToUserSelectionPrice(Double.parseDouble(rewardsDiscount), HotelConfirmationScreen.this);
            if (userSelectedCurr.equalsIgnoreCase("SAR")) {
                currencyName = getString(R.string.label_SAR_currency_name);
                if (isArabicLang) {
                    rewardsRedeemedValue.setText(redeemedConvertedAmount + " " + currencyName);
                } else {
                    rewardsRedeemedValue.setText(currencyName + " " + redeemedConvertedAmount);
                }
            } else {
                currencyName = userSelectedCurr;
                rewardsRedeemedValue.setText(currencyName + " " + redeemedConvertedAmount);
            }
            if (isArabicLang) {
                qitafDiscountValue.setText(qitafAmount + " " + defaultCurr);
            } else {
                qitafDiscountValue.setText(defaultCurr + " " + qitafAmount);
            }
        } catch (Exception e) {
        }
    }

    public String makeJson() {
        final SharedPreferences preferences = HotelConfirmationScreen.this.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
        if (tokenTime == -1) {
            getTokenFromServer();
        } else {
            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
            if (diff > Long.parseLong(expireIn)) {
                getTokenFromServer();
            } else {
                accessTocken = preferences.getString(Constants.ACCESS_TOKEN, "0");
            }
        }
        JSONObject obj = new JSONObject();
        JSONObject bookingJson = new JSONObject();
        try {
            JSONObject bookingJsonObj = new JSONObject(hotelDataJson);
            bookingJsonObj.put("hImage", htModel.getImage());
            if (bnplBooking.equalsIgnoreCase("true")) {
                bookingJsonObj.put("bnpl", true);
                bookingJsonObj.put("tnyurlpath", Constants.TNY_URL_PATH);
                bookingJsonObj.put("tnypayurl", bnplPaymentUrl + "" + Constants.TNY_PAY_URL + "" + refId);
            } else {
                bookingJsonObj.put("bnpl", false);
            }
            bookingJsonObj.put("usrSlctOcp", getUserSelectedOCP());
            if (bookingJsonObj.has("loyaltydetails")) {
                bookingJsonObj.remove("loyaltydetails");
            }
            if (bookingJsonObj.has("language")) {
                bookingJsonObj.remove("language");
            }
            bookingJson.put("BookingData", bookingJsonObj);
            JSONObject jsonObject = new JSONObject(hotelInfoJson);
            jsonObject.getJSONObject("source").put("accessToken", Utils.getRequestAccessToken(HotelConfirmationScreen.this));
            jsonObject.getJSONObject("source").put("referenceID", refId);
            bookingJson.put("BookingRQ", jsonObject);
            JSONObject hotelObj = new JSONObject(hotelJson);
            hotelObj = setLfcDetails(hotelObj);
            Utils.printMessage(TAG, "hotelObj : " + hotelJson);
            obj.put("Booking", bookingJson);
            Utils.printMessage("booking req", "" + obj);
            return "" + obj;
        } catch (Exception e) {
            Utils.printMessage("Exception2", "..." + e.getMessage());
        }
        return "" + obj;
    }

    private JSONObject getUserSelectedOCP() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("rm", getRoomDetailsArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private JSONArray getRoomDetailsArray() {
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < Integer.parseInt(htModel.getRoom_count()); i++) {
                if (htModel.getAdultcountlist().size() == 0 || htModel.getChild_count_list().size() == 0) {
                    roomPassengerArrayList = new ArrayList<>();
                    SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    boolean isRoomsInfoUpdated = preferences.getBoolean(Constants.IS_ROOM_INFO_UPDATED, false);
                    if (!isRoomsInfoUpdated) {
                        for (int j = 0; j < Singleton.getInstance().roomPassengerInfoList.size(); j++) {
                            FhRoomPassengerDetailsBean bean1 = Singleton.getInstance().roomPassengerInfoList.get(j);
                            FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
                            bean.setAdultCnt(bean1.getAdultCnt());
                            bean.setChildAge(bean1.getChildAge());
                            bean.setAge(bean1.getAge());
                            bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
                            bean.setChildCnt(bean1.getChildCnt());
                            bean.setClassType(bean1.getClassType());
                            bean.setInfantCnt(bean1.getInfantCnt());
                            roomPassengerArrayList.add(bean);
                        }
                    } else {
                        for (int j = 0; j < Singleton.getInstance().updatedRoomPassengerInfoList.size(); j++) {
                            FhRoomPassengerDetailsBean bean1 = Singleton.getInstance().updatedRoomPassengerInfoList.get(j);
                            FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
                            bean.setAdultCnt(bean1.getAdultCnt());
                            bean.setChildAge(bean1.getChildAge());
                            bean.setAge(bean1.getAge());
                            bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
                            bean.setChildCnt(bean1.getChildCnt());
                            bean.setClassType(bean1.getClassType());
                            bean.setInfantCnt(bean1.getInfantCnt());
                            roomPassengerArrayList.add(bean);
                        }
                    }
                    if (roomPassengerArrayList.size() == 1) {
                        ArrayList<String> adultcount = new ArrayList<>();
                        HashMap<String, ArrayList<String>> occupancy_child_count_list = new HashMap<>();
                        adultcount.add(String.valueOf(roomPassengerArrayList.get(0).getAdultCnt()));
                        htModel.setAdultcountlist(adultcount);
                        int noOfChildrens = roomPassengerArrayList.get(0).getChildCnt();
                        ArrayList<String> age_child = new ArrayList<>();
                        if (noOfChildrens > 0) {
                            for (int j = 0; j < noOfChildrens; j++) {
                                age_child.add(String.valueOf(roomPassengerArrayList.get(0).getChildAgeArray().get(j)));
                            }
                        }
                        occupancy_child_count_list.put("room1", age_child);
                        htModel.setChild_count_list(occupancy_child_count_list);
                    } else {
                        int noOfAdults = 0;
                        for (int j = 0; j < roomPassengerArrayList.size(); j++) {
                            int occupancy = roomPassengerArrayList.get(j).getAdultCnt() + roomPassengerArrayList.get(j).getChildCnt();
                            if (noOfAdults < occupancy) {
                                noOfAdults = occupancy;
                            }
                        }
                        ArrayList<String> adultcount = new ArrayList<>();
                        for (int j = 0; j < roomPassengerArrayList.size(); j++) {
                            adultcount.add(String.valueOf(noOfAdults));
                        }
                        htModel.setAdultcountlist(adultcount);
                    }
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id", i + 1);
                jsonObject.put("na", htModel.getAdultcountlist().get(i));
                jsonObject.put("nc", htModel.getChild_count_list().get("room" + (i + 1)).size());
                jsonArray.put(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    private void findViews() {
        thankYouText = (TextView) findViewById(R.id.thank_you_text);
        bookingConfirmationText = (TextView) findViewById(R.id.booking_confirmation_text);
        emailSendingText = (TextView) findViewById(R.id.email_sending_text);
        TextView userEmailId = (TextView) findViewById(R.id.user_email_id);
        hotelImage = (ImageView) findViewById(R.id.hotel_image);
        hotelName = (TextView) findViewById(R.id.hotel_name);
        adressText = (TextView) findViewById(R.id.adress_text);
        TextView phoneTitle = (TextView) findViewById(R.id.phone_title);
        phoneText = (TextView) findViewById(R.id.phone_text);
        TextView emailTitle = (TextView) findViewById(R.id.email_title);
        emailText = (TextView) findViewById(R.id.email_text);
        TextView showDirection = (TextView) findViewById(R.id.show_direction);
        TextView bookingNoTitle = (TextView) findViewById(R.id.booking_no_title);
        bookingNoText = (TextView) findViewById(R.id.booking_no_text);
        TextView flightNoTitle = (TextView) findViewById(R.id.flight_no_title);
        flightNoText = (TextView) findViewById(R.id.flight_no_text);
        TextView checkinTitle = (TextView) findViewById(R.id.checkin_title);
        checkinText = (TextView) findViewById(R.id.checkin_text);
        checkInTime = (TextView) findViewById(R.id.check_in_time);
        TextView checkoutTitle = (TextView) findViewById(R.id.checkout_title);
        checkoutText = (TextView) findViewById(R.id.checkout_text);
        checkOutTime = (TextView) findViewById(R.id.check_out_time);
        TextView reservationDetailTitle = (TextView) findViewById(R.id.reservation_detail_title);
        resDetailsText = (TextView) findViewById(R.id.res_details_text);
        TextView totalpriceTitle = (TextView) findViewById(R.id.totalprice_title);
        totalPriceText = (TextView) findViewById(R.id.total_price_text);
        TextView totalPriceText2 = (TextView) findViewById(R.id.total_price_text2);
        TextView resDetailsTitle2 = (TextView) findViewById(R.id.res_details_title2);
        roomDetails = (TextView) findViewById(R.id.room_details);
        roomDescription = (TextView) findViewById(R.id.room_description);
        TextView guestTitle = (TextView) findViewById(R.id.guest_title);
        guestText = (TextView) findViewById(R.id.guest_text);
        TextView roomFacilitiesTitle = (TextView) findViewById(R.id.room_facilities_title);
        roomFacilitiesText = (TextView) findViewById(R.id.room_facilities_text);
        TextView childPolicyTitle = (TextView) findViewById(R.id.child_policy_title);
        childPolicyText = (TextView) findViewById(R.id.child_policy_text);
        TextView infoTitle = (TextView) findViewById(R.id.info_title);
        TextView infoDetails2 = (TextView) findViewById(R.id.info_details2);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        Button backToHome = (Button) findViewById(R.id.btn_backtohome);
        hotel_rating = (RatingBar) findViewById(R.id.ratingBar);
        bnplPackageLayout = (LinearLayout) findViewById(R.id.bnpl_package_layout);
        bnplDataText = (TextView) findViewById(R.id.bnpl_data_text);
        TextView numberOfCouponText = (TextView) findViewById(R.id.coupon_textview);
        numberOfCoupon = (TextView) findViewById(R.id.coupon_amount);
        couponLayout = (LinearLayout) findViewById(R.id.coupon_layout);
        rewardsLayout = (LinearLayout) findViewById(R.id.reward_points_layout);
        rewardsPointsText = (TextView) findViewById(R.id.rewards_flight_dest_textview);
        errorView.setVisibility(View.GONE);
        qitafAmountLayout = (LinearLayout) findViewById(R.id.qitaf_discount_layout);
        TextView qitafDiscountText = (TextView) findViewById(R.id.qitaf_discount_text);
        qitafDiscountValue = (TextView) findViewById(R.id.qitaf_discount);
        rewardsRedeemedLayout = (LinearLayout) findViewById(R.id.flyin_rewards_redeem_layout);
        TextView rewardsRedeemedHeader = (TextView) findViewById(R.id.flyin_rewards_redeem_text);
        rewardsRedeemedValue = (TextView) findViewById(R.id.flyin_rewards_redeem_value);
        String fontRegularBold = Constants.FONT_ROBOTO_BOLD;
        String fontRegularLight = Constants.FONT_ROBOTO_LIGHT;
        String fontRegularMedium = Constants.FONT_ROBOTO_MEDIUM;
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        isArabicLang = false;
        if (Utils.isArabicLangSelected(this)) {
            isArabicLang = true;
            fontRegularBold = Constants.FONT_DROIDKUFI_BOLD;
            fontRegularLight = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegularMedium = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
            flightNoText.setGravity(Gravity.START);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                flightNoText.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
        }
        regularLight = Typeface.createFromAsset(getAssets(), fontRegularLight);
        Typeface regularMedium = Typeface.createFromAsset(getAssets(), fontRegularMedium);
        Typeface regularBold = Typeface.createFromAsset(getAssets(), fontRegularBold);
        Typeface regularFont = Typeface.createFromAsset(getAssets(), fontRegular);
        thankYouText.setTypeface(regularMedium);
        bookingConfirmationText.setTypeface(regularMedium);
        emailSendingText.setTypeface(regularMedium);
        userEmailId.setTypeface(regularBold);
        hotelName.setTypeface(regularBold);
        adressText.setTypeface(regularMedium);
        phoneTitle.setTypeface(regularMedium);
        phoneText.setTypeface(regularMedium);
        emailTitle.setTypeface(regularMedium);
        emailText.setTypeface(regularMedium);
        showDirection.setTypeface(regularBold);
        bookingNoTitle.setTypeface(regularMedium);
        bookingNoText.setTypeface(regularMedium);
        flightNoTitle.setTypeface(regularMedium);
        checkinTitle.setTypeface(regularMedium);
        checkinText.setTypeface(regularMedium);
        checkInTime.setTypeface(regularMedium);
        checkoutTitle.setTypeface(regularMedium);
        checkoutText.setTypeface(regularMedium);
        checkOutTime.setTypeface(regularMedium);
        reservationDetailTitle.setTypeface(regularMedium);
        numberOfCouponText.setTypeface(regularMedium);
        numberOfCoupon.setTypeface(regularMedium);
        resDetailsText.setTypeface(regularMedium);
        totalpriceTitle.setTypeface(regularBold);
        totalPriceText.setTypeface(regularBold);
        totalPriceText2.setTypeface(regularMedium);
        resDetailsTitle2.setTypeface(regularBold);
        roomDetails.setTypeface(regularMedium);
        roomDescription.setTypeface(regularMedium);
        guestTitle.setTypeface(regularMedium);
        guestText.setTypeface(regularMedium);
        roomFacilitiesTitle.setTypeface(regularMedium);
        roomFacilitiesText.setTypeface(regularMedium);
        childPolicyTitle.setTypeface(regularMedium);
        childPolicyText.setTypeface(regularMedium);
        infoTitle.setTypeface(regularMedium);
        infoDetails2.setTypeface(regularMedium);
        backToHome.setTypeface(regularMedium);
        bnplDataText.setTypeface(regularFont);
        qitafDiscountText.setTypeface(regularMedium);
        qitafDiscountValue.setTypeface(regularMedium);
        rewardsRedeemedHeader.setTypeface(regularMedium);
        rewardsRedeemedValue.setTypeface(regularMedium);
        rewardsPointsText.setTypeface(regularFont);
        if (isArabicLang) {
            bookingConfirmationText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.userName_text));
            bnplDataText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            emailSendingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            hotelName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            adressText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            phoneTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            phoneText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            emailTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            emailText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            showDirection.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            bookingNoTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            bookingNoText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            flightNoTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            flightNoText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            checkinTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            checkinText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            checkInTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            checkoutTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            checkoutText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            checkOutTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            reservationDetailTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            resDetailsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            numberOfCouponText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            numberOfCoupon.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            totalpriceTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            totalPriceText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.button_text_size));
            totalPriceText2.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            resDetailsTitle2.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            roomDetails.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            roomDescription.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            guestTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            guestText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            roomFacilitiesTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            roomFacilitiesText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            childPolicyTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            childPolicyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            infoTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            infoDetails2.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            backToHome.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            qitafDiscountText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            qitafDiscountValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            rewardsRedeemedHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            rewardsRedeemedValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            rewardsPointsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            bookingNoText.setGravity(Gravity.START);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                bookingNoText.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
        }
        showDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadGPSValues();
                if (currentLatitude != 0.0 && currentLongitude != 0.0) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse("geo:" + currentLatitude + "," + currentLongitude + "?q=" +
                            htModel.getLatitude() + "," + htModel.getLongitude() + " (" + htModel.getHna() + ")"));
                    startActivity(intent);
                }
            }
        });
        backToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Singleton.getInstance().roomPassengerInfoList.clear();
                Intent intent = new Intent(HotelConfirmationScreen.this, MainSearchActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                HotelConfirmationScreen.this.finish();
            }
        });
    }

    private void showLoading() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setVisibility(View.GONE);
        descriptionText.setText(getResources().getString(R.string.lable_searching_hotels));
        descriptionText.setVisibility(View.GONE);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
        loadingViewLayout.removeAllViews();
        loadingViewLayout.setVisibility(View.GONE);
    }

    private void getTokenFromServer() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(HotelConfirmationScreen.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(HotelConfirmationScreen.this, HotelConfirmationScreen.this,
                Constants.CLIENT_AUTHENTICATION, "", obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            displayErrorMsg();
        } else {
            if (serviceType == GET_ACCESS_TOKEN) {
                closeLoading();
                JSONObject obj;
                try {
                    obj = new JSONObject(data);
                    String accessToken = obj.getString("accessToken");
                    String expireIn = obj.getString("expireIn");
                    String refreshToken = obj.getString("refreshToken");
                    long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                    SharedPreferences preferences = HotelConfirmationScreen.this.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Constants.ACCESS_TOKEN, accessToken);
                    editor.putString(Constants.EXPIRE_IN, expireIn);
                    editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                    editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                    editor.apply();
                    String json = makeJson();
                    Utils.printMessage("json", "" + json);
                    if (json != null) {
                        HTTPAsync async = new HTTPAsync(HotelConfirmationScreen.this, HotelConfirmationScreen.this,
                                Constants.HOTEL_BOOK_RQ, "", json, GET_BOOK_REQ, HTTPAsync.METHOD_POST);
                        async.execute();
                        showLoading();
                    }
                } catch (JSONException e) {
                } catch (Exception e) {
                }
            } else if (serviceType == GET_BOOK_REQ) {
                parsedaa(data);
            } else if (serviceType == GET_LOYALTY_POINTS) {
                Utils.printMessage(TAG, "Loyalty Points :: " + data);
                JSONObject obj = null;
                closeLoading();
                try {
                    obj = new JSONObject(data);
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        String points = obj.getString("earnPoints");
                        Utils.printMessage(TAG, "earn points::" + points);
                        handleLoyaltyConfirmService(points);
                        rewardsLayout.setVisibility(View.VISIBLE);
                        rewardsPointsText.setText(String.format(getResources().getString(R.string.label_rewards_earned_points), points));
                    } else {
                        Utils.printMessage(TAG, "earn points::" + obj.getString("message"));
                        rewardsLayout.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                }
            } else if (serviceType == GET_CONFIRM_LOYALTY_RESULT) {
                Utils.printMessage(TAG, "Confirm Loyalty :: " + data);
                closeLoading();
            }
        }
    }

    private void parsedaa(String data) {
        Utils.printMessage(TAG, "Response : " + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            //closeLoading();
            displayErrorMsg();
        } else {
            try {
                JSONObject jsonObject = new JSONObject(data);
                if (jsonObject.has("BookingRS")) {
                    JSONObject jsonBooking = jsonObject.getJSONObject("BookingRS");
                    String st = jsonBooking.getString("st");
                    if (st.equalsIgnoreCase("FAIL")) {
                        //closeLoading();
                        displayErrorMsg();
                        return;
                    }
                    if (st.equals("SUCCESS") || st.equalsIgnoreCase("Pending Confirmation")) {
                        JSONObject jsonObject1 = jsonBooking.getJSONObject("source");
                        if (jsonObject1.has("referenceID")) {
                            bookRefId = jsonObject1.getString("referenceID");
                        }
                        JSONObject hotelObject = jsonBooking.getJSONObject("hotel");
                        JSONObject rcObject = hotelObject.getJSONObject("rc");
                        JSONArray roomArray = rcObject.getJSONArray("room");
                        String dur = rcObject.getString("dur");
                        dur = (dur == null || dur.equals("null")) ? "0" : dur;
                        JSONObject lpObject = roomArray.getJSONObject(0).getJSONObject("lp");
                        leadPassengerName = lpObject.optString("fname") + " " + lpObject.optString("lname");
                        emailId = lpObject.optString("email");
                        phoneNumber = lpObject.optString("ph");
                        roomDetails.setText(roomArray.getJSONObject(0).getString("rn"));
                        String nightInArbic = "";
                        if (Utils.isArabicLangSelected(this)) {
                            if (Integer.parseInt(dur) == 1) {
                                nightInArbic = getResources().getString(R.string.label_one_night);
                            } else if (Integer.parseInt(dur) > 1 && Integer.parseInt(dur) < 11) {
                                nightInArbic = getResources().getString(R.string.label_below_ten_night);
                            } else {
                                nightInArbic = getResources().getString(R.string.label_more_nights);
                            }
                        } else {
                            nightInArbic = Integer.parseInt(htModel.getDur()) > 1 ?
                                    getResources().getString(R.string.label_more_nights).toLowerCase() :
                                    getResources().getString(R.string.label_one_night).toLowerCase();
                        }
                        String room;
                        if (htModel.getRoom_count().equals("1")) {
                            room = getString(R.string.room_lebel);
                        } else {
                            room = getString(R.string.rooms_lebel);
                        }
                        resDetailsText.setText(dur + " " + nightInArbic + ", " + htModel.getRoom_count() + " " + room);
                        roomDescription.setText("NA");
                        childPolicyText.setText(getString(R.string.no_cancellation_text));
                        String resultDays = "";
                        if (bnplBooking.equalsIgnoreCase("true")) {
                            if (roomArray.getJSONObject(0).has("ci")) {
                                JSONObject ciObject = roomArray.getJSONObject(0).getJSONObject("ci");
                                if (ciObject.has("lfc")) {
                                    JSONObject lfcObject = ciObject.getJSONObject("lfc");
                                    if (lfcObject.optString("d") != null && !lfcObject.optString("d").equals("null")) {
                                        resultDays = Utils.flightSearchDateDifference(lfcObject.optString("d"), currentDate);
                                        if (Integer.parseInt(resultDays) >= 0) {
                                            if (lfcObject.has("d") && lfcObject.has("hou") && lfcObject.has("min")) {
                                                DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                                DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                                                Date date = null;
                                                try {
                                                    date = originalFormat.parse(lfcObject.optString("d"));
                                                    cancellationDate = targetFormat.format(date) + " " + lfcObject.optString("hou") + ":" + lfcObject.optString("min");
                                                } catch (ParseException e) {
                                                    cancellationDate = lfcObject.optString("d") + " " + lfcObject.optString("hou") + ":" + lfcObject.optString("min");
                                                }
                                                bnplPackageLayout.setVisibility(View.VISIBLE);
                                            }
                                        } else {
                                            bnplPackageLayout.setVisibility(View.GONE);
                                        }
                                        Utils.printMessage(TAG, "cancellationDate : " + cancellationDate);
                                        String firstMsg = getString(R.string.label_info_text);
                                        String secondMsg = " " + getString(R.string.label_confirmation_page_bnpl_info_text) + " " + cancellationDate + ". " + getString(R.string.label_confirmation_pay_bnpl_text);
                                        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(firstMsg + secondMsg);
                                        stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, firstMsg.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                                        stringBuilder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.refund_text_color)),
                                                firstMsg.length() + 1, firstMsg.length() + secondMsg.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                                        bnplDataText.setText(stringBuilder);
                                    }
                                }
                            }
                        }
                        if (roomArray.getJSONObject(0).has("ci")) {
                            JSONObject ciObject = roomArray.getJSONObject(0).getJSONObject("ci");
                            if (ciObject.has("nrf")) {
                                if (ciObject.optString("nrf") != null && !ciObject.optString("nrf").equals("null")) {
                                    childPolicyText.setText(getString(R.string.non_refundable_hotel));
                                } else {
                                    StringBuilder sb = new StringBuilder();
                                    if (ciObject.has("lfc")) {
                                        JSONObject lfcObject = ciObject.getJSONObject("lfc");
                                        if (lfcObject.optString("d") != null && !lfcObject.optString("d").equals("null")) {
                                            sb.append(getString(R.string.cancellation_text) + lfcObject.optString("d") + " " + lfcObject.optString("hou") + ":" + lfcObject.optString("min"));
                                        }
                                    }
                                    if (ciObject.has("cp")) {
                                        JSONArray cpArray = ciObject.getJSONArray("cp");
                                        for (int i = 0; i < cpArray.length(); i++) {
                                            JSONObject cpArrayJSONObject = cpArray.getJSONObject(i);
                                            JSONObject crgObject = cpArrayJSONObject.getJSONObject("crg");
                                            JSONObject psObject = cpArrayJSONObject.getJSONObject("ps");
                                            JSONObject peObject = cpArrayJSONObject.getJSONObject("pe");
                                            String fromDate = psObject.optString("d") + " " + psObject.optString("hou") + ":" + psObject.optString("min");
                                            String toDate = peObject.optString("d") + " " + peObject.optString("hou") + ":" + peObject.optString("min");
                                            String policyData = "";
                                            if (crgObject.getString("ty").equalsIgnoreCase("Nights")) {
                                                policyData = crgObject.optString("val") + " " + getString(R.string.label_more_nights);
                                            } else if (crgObject.getString("ty").equalsIgnoreCase("Percentage")) {
                                                policyData = crgObject.optString("val") + "%";
                                            } else {
                                                policyData = crgObject.optString("val") + " " + crgObject.optString("ty");
                                            }
                                            if (isArabicLang) {
                                                sb.append(" " + policyData + " " + getString(R.string.booking_from_text) + " (" + getString(R.string.label_from_text) + " " + fromDate + " " + getString(R.string.to_text) + " " + toDate + ")").append((i == (cpArray.length() - 1)) ? "" : "\n");
                                            } else {
                                                sb.append(" " + policyData + " " + getString(R.string.booking_from_text) + " (" + getString(R.string.label_from_text) + " " + fromDate + " " + getString(R.string.to_text) + " " + toDate + ")").append((i == (cpArray.length() - 1)) ? "" : "\n");
                                            }
                                        }
                                    }
                                    childPolicyText.setText(sb.toString());
                                }
                            } else {
                                StringBuilder sb = new StringBuilder();
                                if (ciObject.has("lfc")) {
                                    JSONObject lfcObject = ciObject.getJSONObject("lfc");
                                    if (lfcObject.optString("d") != null && !lfcObject.optString("d").equals("null")) {
                                        sb.append(getString(R.string.cancellation_text) + lfcObject.optString("d") + " " + lfcObject.optString("hou") + ":" + lfcObject.optString("min"));
                                    }
                                }
                                if (ciObject.has("cp")) {
                                    JSONArray cpArray = ciObject.getJSONArray("cp");
                                    for (int i = 0; i < cpArray.length(); i++) {
                                        JSONObject cpArrayJSONObject = cpArray.getJSONObject(i);
                                        JSONObject crgObject = cpArrayJSONObject.getJSONObject("crg");
                                        JSONObject psObject = cpArrayJSONObject.getJSONObject("ps");
                                        JSONObject peObject = cpArrayJSONObject.getJSONObject("pe");
                                        String fromDate = psObject.optString("d") + " " + psObject.optString("hou") + ":" + psObject.optString("min");
                                        String toDate = peObject.optString("d") + " " + peObject.optString("hou") + ":" + peObject.optString("min");
                                        String policyData = "";
                                        if (crgObject.getString("ty").equalsIgnoreCase("Nights")) {
                                            policyData = crgObject.optString("val") + " " + getString(R.string.label_more_nights);
                                        } else if (crgObject.getString("ty").equalsIgnoreCase("Percentage")) {
                                            policyData = crgObject.optString("val") + "%";
                                        } else {
                                            policyData = crgObject.optString("val") + " " + crgObject.optString("ty");
                                        }
                                        if (isArabicLang) {
                                            sb.append(" " + policyData + " " + getString(R.string.booking_from_text) + " (" + getString(R.string.label_from_text) + " " + fromDate + " " + getString(R.string.to_text) + " " + toDate + ")").append((i == (cpArray.length() - 1)) ? "" : "\n");
                                        } else {
                                            sb.append(" " + policyData + " " + getString(R.string.booking_from_text) + " (" + getString(R.string.label_from_text) + " " + fromDate + " " + getString(R.string.to_text) + " " + toDate + ")").append((i == (cpArray.length() - 1)) ? "" : "\n");
                                        }
                                    }
                                }
                                childPolicyText.setText(sb.toString());
                            }
                        } else if (!cancellationDate.equalsIgnoreCase("")) {
                            childPolicyText.setText(getString(R.string.cancellation_text) + " " + cancellationDate);
                        }
                        String bookingNp = hotelObject.optJSONObject("acd").getString("bc");
                        String bookingNumber = "";
                        if (bookingNp == null)
                            bookingNp = "";
                        if (bookingNp.length() != 0 && bookingNp != null) {
                            String[] bookingNumArr = bookingNp.split(",");
                            for (String str : bookingNumArr) {
                                if (!bookingNumber.isEmpty()) {
                                    bookingNumber += ",";
                                }
                                bookingNumber += str.substring(str.indexOf("-") + 1);
                            }
                        }
                        if (bnplBooking.equalsIgnoreCase("true")) {
                            bookingNoText.setVisibility(View.GONE);
                        } else {
                            bookingNoText.setVisibility(View.VISIBLE);
                            bookingNoText.setText("" + bookingNumber);
                        }
                        if (hotelObject.optJSONObject("acd").has("note") && (bnplBooking.equalsIgnoreCase("false") || bnplBooking.equalsIgnoreCase(""))) {
                            if (hotelObject.optJSONObject("acd").getString("note") != null ||
                                    !hotelObject.optJSONObject("acd").getString("note").equalsIgnoreCase("null") ||
                                    !hotelObject.optJSONObject("acd").getString("note").isEmpty()) {
                                bookingNoteInfo = hotelObject.optJSONObject("acd").getString("note");
                            }
                        }
                    }
                    displayUI();
                } else {
                    String st = jsonObject.getString("status");
                    if (st.equalsIgnoreCase("FAIL") || st.equalsIgnoreCase("FAILURE")) {
                        //closeLoading();
                        displayErrorMsg();
                    }
                }
            } catch (JSONException e) {
            } catch (Exception e) {
            }
        }
    }

    private void displayErrorMsg() {
        Utils.printMessage(TAG, "Getting Fail");
        loadingViewLayout.setVisibility(View.VISIBLE);
        final View errorView = inflater.inflate(R.layout.view_error_response, null);
        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
        errorText = (TextView) errorView.findViewById(R.id.error_text);
        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
        searchButton = (Button) errorView.findViewById(R.id.search_button);
        errorText.setTypeface(regularLight);
        errorDescriptionText.setTypeface(regularLight);
        searchButton.setTypeface(regularLight);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isInternetPresent = Utils.isConnectingToInternet(HotelConfirmationScreen.this);
                if (isInternetPresent) {
                    Singleton.getInstance().roomPassengerInfoList.clear();
                    Intent intent = new Intent(HotelConfirmationScreen.this, MainSearchActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        });
        loadErrorType(Constants.WRONG_ERROR_PAGE);
        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(errorView);
    }

    private void displayUI() {
        thankYouText.setText(getString(R.string.label_confirmation_thank_you) + " " + leadPassengerName);
        String hotelAddress = (htModel.getFullAddress()).replace("\n", " ");
        adressText.setText(hotelAddress);
        if (bookingNoteInfo.isEmpty()) {
            if (bnplBooking.equalsIgnoreCase("true")) {
                bookingConfirmationText.setText(R.string.book_hotel_reserve_text);
            } else {
                bookingConfirmationText.setText(R.string.Your_booking_is_now_confirmed);
            }
            String firstMsg = getString(R.string.email_sent_confirmation_text);
            emailSendingText.setText(firstMsg + " " + emailId, TextView.BufferType.SPANNABLE);
            Spannable s = (Spannable) emailSendingText.getText();
            int start = firstMsg.length();
            int end = start + emailId.length() + 1;
            s.setSpan(new ForegroundColorSpan(0xFF839abe), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            bnplPackageLayout.setVisibility(View.VISIBLE);
            if (bnplBooking.equalsIgnoreCase("true")) {
                bookingConfirmationText.setText(R.string.book_hotel_reserve_text);
            } else {
                bookingConfirmationText.setText(R.string.label_hotel_pending_header);
                bnplDataText.setText(R.string.label_hotel_pending_info);
            }
            String firstMsg = getString(R.string.email_sent_confirmation_text);
            emailSendingText.setText(firstMsg + " " + emailId, TextView.BufferType.SPANNABLE);
            Spannable s = (Spannable) emailSendingText.getText();
            int start = firstMsg.length();
            int end = start + emailId.length() + 1;
            s.setSpan(new ForegroundColorSpan(0xFF839abe), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        ViewTreeObserver vto = hotelImage.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                hotelImage.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = hotelImage.getMeasuredHeight();
                int finalWidth = hotelImage.getMeasuredWidth();
                String url = Constants.IMAGE_SCALING_URL + finalWidth + "x" + finalHeight + Constants.IMAGE_QUALITY + "" + htModel.getImage();
                Glide.with(HotelConfirmationScreen.this).load(url).placeholder(R.drawable.imagethumb_search).into(hotelImage);
                return true;
            }
        });
        hotelName.setText("" + htModel.getHna());
        float star = Float.parseFloat(htModel.getStar());
        if (star != 0.0) {
            hotel_rating.setVisibility(View.VISIBLE);
            hotel_rating.setRating(Float.parseFloat(htModel.getStar()));
        }
        phoneText.setText("" + htModel.getHphoneNo());
        emailText.setText("" + htModel.getHemail());
        String flyinTripNumber = "";
        if (!TextUtils.isEmpty(bookRefId)) {
            flyinTripNumber = bookRefId.substring(2);
        }
        flightNoText.setText("" + flyinTripNumber);
        if (isArabicLang) {
            checkinText.setText(Utils.convertTocalendardate(htModel.getCheck_in_date(), HotelConfirmationScreen.this));
            checkoutText.setText(Utils.convertTocalendardate(htModel.getCheck_out_date(), HotelConfirmationScreen.this));
        } else {
            checkinText.setText(Utils.convertToHotelCheckDateFormat(htModel.getCheck_in_date(), HotelConfirmationScreen.this));
            checkoutText.setText(Utils.convertToHotelCheckDateFormat(htModel.getCheck_out_date(), HotelConfirmationScreen.this));
        }
        if (htModel.getCheck_in() == null || htModel.getCheck_in().equalsIgnoreCase("null")) {
            checkInTime.setVisibility(View.GONE);
        } else {
            checkInTime.setText("(" + getResources().getString(R.string.label_from_text) + " " + htModel.getCheck_in() + ")");
        }
        if (htModel.getCheck_out() == null || htModel.getCheck_out().equalsIgnoreCase("null")) {
            checkOutTime.setVisibility(View.GONE);
        } else {
            checkOutTime.setText("(" + getResources().getString(R.string.label_until_text) + " " + htModel.getCheck_out() + ")");
        }
        String room;
        if (htModel.getRoom_count().equals("1")) {
            room = getString(R.string.room_lebel);
        } else {
            room = getString(R.string.rooms_lebel);
        }
        resDetailsText.setText("" + htModel.getDur() + " " + (!htModel.getDur().equals("1") ? getString(R.string.label_more_nights) : getString(R.string.label_one_night))
                + ", " + htModel.getRoom_count() + " " + room);
        if (Utils.isArabicLangSelected(this)) {
            String nightInArbic = "";
            if (Integer.parseInt(htModel.getDur()) == 1) {
                nightInArbic = getResources().getString(R.string.label_one_night);
            } else if (Integer.parseInt(htModel.getDur()) > 1 && Integer.parseInt(htModel.getDur()) < 11) {
                nightInArbic = getResources().getString(R.string.label_below_ten_night);
            } else {
                nightInArbic = getResources().getString(R.string.label_more_nights);
            }
            resDetailsText.setText("" + htModel.getDur() + " " + nightInArbic + ", " + htModel.getRoom_count() + " " + room);
        }
        Double userPayPrice = 0.0;
        String currencyName;
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String userSelectedCurr = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        if (userSelectedCurr.equalsIgnoreCase("SAR")) {
            currencyName = getString(R.string.label_SAR_currency_name);
        } else {
            currencyName = userSelectedCurr;
        }
        Double convertedTotalPrice = Double.parseDouble(htModel.getP());
        if (!cua.isEmpty()) {
            if (Double.parseDouble(cua) == 0) {
                couponLayout.setVisibility(View.GONE);
                userPayPrice = convertedTotalPrice;
            } else {
                userPayPrice = convertedTotalPrice - Double.parseDouble(cua);
                couponLayout.setVisibility(View.VISIBLE);
                numberOfCoupon.setText(String.format(getApplicationContext().getResources().getString(R.string.label_placeholder), currencyName, cua));
            }
        } else {
            couponLayout.setVisibility(View.GONE);
            userPayPrice = convertedTotalPrice;
        }
        String finalPaidPrice = String.format(Locale.ENGLISH, "%.2f", userPayPrice);
        if (userSelectedCurr.equalsIgnoreCase("SAR")) {
            if (Utils.isArabicLangSelected(this)) {
                totalPriceText.setText(finalPaidPrice + " " + currencyName);
            } else {
                totalPriceText.setText(currencyName + " " + finalPaidPrice);
            }
        } else {
            totalPriceText.setText(currencyName + " " + finalPaidPrice);
        }

        String adultLabel = "";
        int totalAdultCnt = htModel.getAdultcountlist().size();
        if (totalAdultCnt == 1) {
            adultLabel = getResources().getString(R.string.SAdultLbl);
        } else {
            adultLabel = getResources().getString(R.string.PAdultLbl);
        }
        guestText.setText("" + leadPassengerName + " (" + adultLabel + ") " /*+ htModel.getAdultcountlist().size()+" Peoples"*/);
        roomFacilitiesText.setText(getFacilities(htModel.getService()));
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        if (!preferences.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
            if (preferences.getBoolean(Constants.IS_REWARDS_ENABLED, false)) {
                handleEranLoyaltyRequest();
            } else {
                closeLoading();
            }
        } else {
            closeLoading();
        }
    }

    private String getFacilities(String[] services) {
        StringBuilder sb = new StringBuilder();
        for (String str : services) {
            sb.append(str + ", ");
        }
        return sb.toString();
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(getResources().getString(R.string.label_network_error_message));
                searchButton.setText(getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getResources().getString(R.string.label_no_results_found_message_hotels));
                searchButton.setText(getResources().getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Singleton.getInstance().roomPassengerInfoList.clear();
        Intent intent = new Intent(HotelConfirmationScreen.this, MainSearchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void loadGPSValues() {
        gps = new GPSTracker(HotelConfirmationScreen.this);
        if (gps.canGetLocation()) {
            currentLatitude = gps.getLatitude();
            currentLongitude = gps.getLongitude();
        } else {
            gps.showSettingsAlert();
        }
    }

    private JSONObject setLfcDetails(JSONObject hotel) {
        try {
            if (hotel.has("rc")) {
                if (hotel.getJSONObject("rc").has("room")) {
                    JSONArray roomArr = hotel.getJSONObject("rc").getJSONArray("room");
                    if (roomArr.length() != 0) {
                        for (int i = 0; i < roomArr.length(); i++) {
                            if (roomArr.getJSONObject(i).has("ci")) {
                                if (roomArr.getJSONObject(i).getJSONObject("ci").has("lfc")) {
                                    JSONObject lfcObj = roomArr.getJSONObject(i).getJSONObject("ci").getJSONObject("lfc");
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hotel;
    }

    private void handleEranLoyaltyRequest() {
        String earnPoints = handleEarnLoyaltyPoints();
        showLoading();
        HTTPAsync async = new HTTPAsync(HotelConfirmationScreen.this, HotelConfirmationScreen.this,
                Constants.EARN_LOYALTY_POINTS_RQ, "", earnPoints, GET_LOYALTY_POINTS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String handleEarnLoyaltyPoints() {
        JSONObject sourceObj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, 0);
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        try {
            sourceObj.put("apikey", Constants.MERCHANDISE_API_KEY);
            sourceObj.put("userid", pref.getString(Constants.USER_UNIQUE_ID, ""));
            sourceObj.put("bookingCode", bookRefId);
            sourceObj.put("product", "hotel");
            sourceObj.put("currency", "SAR");
            sourceObj.put("basefareAmount", Double.parseDouble(Utils.convertOtherCurrencytoSarCurrency(Double.parseDouble(htModel.getP()), HotelConfirmationScreen.this)));
            sourceObj.put("totalAmount", Double.parseDouble(Utils.convertOtherCurrencytoSarCurrency(Double.parseDouble(htModel.getP()), HotelConfirmationScreen.this)));
            sourceObj.put("companyId", Constants.LOYALTY_COMPANY_ID);
            JSONObject flightObj = new JSONObject();
            JSONObject routeObj = new JSONObject();
            flightObj.put("route", routeObj);
            sourceObj.put("flight", flightObj);
            JSONObject hotelObj = new JSONObject();
            ArrayList<String> hotelIdList = new ArrayList<>();
            hotelIdList.add(htModel.getHuid());
            JSONArray hotelTypearray = new JSONArray(hotelIdList);
            hotelObj.put("crs", crs(htModel.getHotelTypeValue()));
            hotelObj.put("rating", htModel.getStar());
            hotelObj.put("hname", hotelNameInEng);
            hotelObj.put("cityId", htModel.getCityId());
            hotelObj.put("hotelType", hotelTypearray);
            sourceObj.put("hotel", hotelObj);
            JSONObject fphObject = new JSONObject();
            sourceObj.put("fph", fphObject);
            sourceObj.put("bookingDate", timeStamp + ".000Z");
            sourceObj.put("travelPeriodFrom", Utils.formatDateToServerDateFormat(htModel.getCheck_in_date()) + "T" + currentTimeFormat + ".000Z");
            sourceObj.put("travelPeriodTo", Utils.formatDateToServerDateFormat(htModel.getCheck_out_date()) + "T" + currentTimeFormat + ".000Z");
            sourceObj.put("groupType", groupType);
            if (bnplBooking.equalsIgnoreCase("true")) {
                sourceObj.put("onHold", "1");
            } else {
                sourceObj.put("onHold", "0");
            }
            Utils.printMessage("handleEarnLoyaltyPoints", sourceObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sourceObj.toString();
    }

    private String crs(String htValue) {
        String crs = "";
        try {
            JSONObject hotelCrs = new JSONObject(htValue);
            crs = hotelCrs.get("crs").toString();
            return crs;
        } catch (Exception e) {
        }
        return crs;
    }

    private void handleLoyaltyConfirmService(String earnedPoints) {
        String loyaltyPoints = handleLoyaltyConfirmRQ(earnedPoints);
        Utils.printMessage(TAG, "Conf Loyalty RQ :: " + loyaltyPoints);
        HTTPAsync async = new HTTPAsync(HotelConfirmationScreen.this, HotelConfirmationScreen.this,
                Constants.CONFIRM_LOYALTY_URL, "", loyaltyPoints, GET_CONFIRM_LOYALTY_RESULT, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String handleLoyaltyConfirmRQ(String earnedPoints) {
        JSONObject mainJSON = new JSONObject();
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(HotelConfirmationScreen.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(HotelConfirmationScreen.this));
            mainJSON.put("source", sourceJSON);
            JSONObject commonDetailsJSON = new JSONObject();
            commonDetailsJSON.put("flyincode", refId);
            commonDetailsJSON.put("customerId", pref.getString(Constants.MEMBER_EMAIL, ""));
            commonDetailsJSON.put("leadpax", userFullName);
            commonDetailsJSON.put("ipAddrs", Utils.checkStringValue("null"));
            commonDetailsJSON.put("prodType", "H");
            commonDetailsJSON.put("apptype", "M-App");
            commonDetailsJSON.put("paymtdate", timeStamp);
            commonDetailsJSON.put("serviceType", "Loyalty");
            mainJSON.put("commondetails", commonDetailsJSON);
            JSONObject loyaltyJSON = new JSONObject();
            loyaltyJSON.put("earnpoint", Integer.parseInt(earnedPoints));
            loyaltyJSON.put("redeempoint", Utils.checkStringValue("null"));
            loyaltyJSON.put("rdmdiscountusr", Utils.checkStringValue("null"));
            loyaltyJSON.put("status", "CONFIRMED");
            mainJSON.put("loyaltydetails", loyaltyJSON);
        } catch (Exception e) {
        }
        return mainJSON.toString();
    }
}
