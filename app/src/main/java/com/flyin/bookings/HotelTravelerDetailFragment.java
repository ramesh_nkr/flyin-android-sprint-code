package com.flyin.bookings;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.dialog.SignInDialog;
import com.flyin.bookings.dialog.TripAdvisorDialog;
import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.model.RoomModel;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.model.TripAdvisorBean;
import com.flyin.bookings.model.TripDialogBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

@SuppressWarnings("deprecation")
public class HotelTravelerDetailFragment extends Fragment implements OnClickListener, AsyncTaskListener {
    private static final int GET_ACCESS_TOKEN = 1212;
    private static final int BOOK_SEARCH_HOTEL = 1111;
    private HotelModel hotelModel = null;
    private TextView errorText, errorDescriptionText;
    private LayoutInflater layoutInflater;
    private Typeface regularLight, regularBold, regularFont, regularMedium;
    private ImageView image;
    private TextView hotelName;
    private RatingBar ratingBarHotel;
    private TextView hotelAddress;
    private TextView checkInTime;
    private TextView checkOutTime;
    private TextView roomType;
    private TextView breakfast;
    private TextView cancelPolicyText;
    private TextView hotDeal;
    private TextView offer;
    private TextView discountCurrencyText = null;
    private TextView pricePrevious;
    private TextView discountPriceDecimalText = null;
    private TextView currencyText = null;
    private TextView priceFinal;
    private TextView priceDecimalText = null;
    private TextView noOfNightRoom;
    private Button submit;
    private ArrayList<RoomModel> roomList = null;
    private String accessTocken;
    private int childCount, adultCount;
    private String hotelString;
    private RelativeLayout errorView, loadingViewLayout;
    private TextView errorMessageText = null;
    private ImageView errorImage;
    private Button searchButton;
    private boolean isArabicLang;
    private Button signin;
    private SignInDialog dialog;
    private boolean isSingInClicked;
    private LinearLayout persion_layout;
    private static final String TAG = "HotelTravelerDetailFragment";
    private HotelModel modelList;
    private String headerDate = "";
    private String currentDate = "";
    private LinearLayout backLayout = null;
    private int _id = 1;
    private String mapFragmentData = "";
    private String getDetailsData = "";
    private boolean isPriceChanged = false;
    private LinearLayout bnplPackageLayout = null;
    private String hotelInDate = "";
    private String hotelOutDate = "";
    private LinearLayout cancellationViewLayout = null;
    private TextView cancellationDateText = null;
    private String randomNumber = "";
    private boolean isCancellationDataExpanded = true;
    private ImageView cancellationImage = null;
    private LinearLayout cancellationDetailsLayout = null;
    private RelativeLayout cancellationPolicyLayout = null;
    private boolean isBnplAvailable = false;
    private LinearLayout discountPriceLayout = null;
    private String tripAdvisorCityId = "";
    private ArrayList<String> huidlist = null;
    private int SEARCH_HOTEL_TRIP = 20;
    private ArrayList<TripAdvisorBean> tripAdvisorAry = null;
    private View advisorView = null;
    private LinearLayout tripAdvisorLayout = null;
    private ImageView owlImage = null;
    private TextView reviewText = null;
    private ProgressDialog barProgressDialog = null;
    private static final int GET_TRIP_ADVISOR = 13;
    private ArrayList<TripDialogBean> tripArrayList = null;
    private String userPayPrice = "";
    private Activity mActivity;
    private static final int GET_LOYALTY_POINTS = 5;
    private LinearLayout rewardsLayout;
    private TextView rewardsPointsText;
    private String earnPointsPrice = "";
    private String earnPointsHotelName = "";
    private String userEarnedPoints = "";
    private MyReceiver myReceiver;
    private String mapVisiblePrice = "";

    @SuppressLint("LongLogTag")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hotel_traveler_detail, container, false);
        String fontRegularBold = Constants.FONT_ROBOTO_BOLD;
        String fontRegularLight = Constants.FONT_ROBOTO_LIGHT;
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        String fontMedium = Constants.FONT_ROBOTO_MEDIUM;
        if (Utils.isArabicLangSelected(mActivity)) {
            isArabicLang = true;
            fontRegularBold = Constants.FONT_DROIDKUFI_BOLD;
            fontRegularLight = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
            fontMedium = Constants.FONT_DROIDKUFI_REGULAR;
        }
        regularLight = Typeface.createFromAsset(mActivity.getAssets(), fontRegularLight);
        regularBold = Typeface.createFromAsset(mActivity.getAssets(), fontRegularBold);
        regularFont = Typeface.createFromAsset(mActivity.getAssets(), fontRegular);
        regularMedium = Typeface.createFromAsset(mActivity.getAssets(), fontMedium);
        roomList = new ArrayList<>();
        huidlist = new ArrayList<>();
        tripAdvisorAry = new ArrayList<>();
        tripArrayList = new ArrayList<>();
        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        findViews(view);
        Gson gson = new Gson();
        String hotelModelString = getArguments().getString("model");
        hotelModel = gson.fromJson(hotelModelString, HotelModel.class);
        modelList = getArguments().getParcelable("modelList");
        childCount = getArguments().getInt("child_count", 0);
        adultCount = getArguments().getInt("adult_count", 1);
        headerDate = getArguments().getString("header_date");
        mapVisiblePrice = getArguments().getString("payPrice", "");
        randomNumber = getArguments().getString(Constants.BOOKING_RANDOM_NUMBER, "");
        if (headerDate.contains("-")) {
            String[] hotelCheckDate = headerDate.split("-");
            hotelInDate = hotelCheckDate[0];
            hotelOutDate = hotelCheckDate[1];
        }
        mapFragmentData = getArguments().getString("isMapFragment");
        getDetailsData = getArguments().getString("gettingDataFrom");
        roomList = hotelModel.getRoomlist();
        hotelName.setText(hotelModel.getHna());
        String hotelAdd = (hotelModel.getFullAddress()).replace("\n", " ");
        hotelAddress.setText(hotelAdd);
        float star = Float.parseFloat(hotelModel.getStar());
        if (star != 0.0) {
            ratingBarHotel.setVisibility(View.VISIBLE);
            ratingBarHotel.setRating(Float.parseFloat(hotelModel.getStar()));
        } else {
            ratingBarHotel.setVisibility(View.GONE);
        }
        huidlist.add(hotelModel.getHuid());
        tripAdvisorCityId = hotelModel.getCityId();
        backLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (loadingViewLayout.getVisibility() == View.VISIBLE) {
                    return;
                }
                mActivity.onBackPressed();
            }
        });
        RoomModel model = roomList.get(0);
        roomType.setText("");
        currencyText.setText("");
        priceFinal.setText("");
        priceDecimalText.setText("");
        double cp = Double.parseDouble(model.getPrice());
        double wdp = Double.parseDouble(model.getWdp());
        if (cp < wdp) {
            discountCurrencyText.setText("");
            pricePrevious.setText("");
            discountPriceDecimalText.setText("");
            discountCurrencyText.setPaintFlags(discountCurrencyText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            pricePrevious.setPaintFlags(pricePrevious.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            discountPriceDecimalText.setPaintFlags(discountPriceDecimalText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            hotDeal.setVisibility(View.VISIBLE);
            discountPriceLayout.setVisibility(View.VISIBLE);
            offer.setVisibility(View.VISIBLE);
            double per = (((wdp - cp) * 100) / cp);
            int percent = (int) per;
        } else {
            discountPriceLayout.setVisibility(View.GONE);
            hotDeal.setVisibility(View.GONE);
            offer.setVisibility(View.GONE);
        }
        String room;
        if (hotelModel.getRoom_count().equals("1")) {
            room = " " + mActivity.getString(R.string.room_lebel);
        } else {
            room = mActivity.getString(R.string.rooms_lebel);
        }
        String night;
        if (hotelModel.getDur().equals("1")) {
            night = " " + mActivity.getString(R.string.label_one_night) + ", ";
        } else {
            night = " " + mActivity.getString(R.string.label_more_nights) + ", ";
        }
        noOfNightRoom.setText(mActivity.getString(R.string.label_for_text) + " " + hotelModel.getDur() + night + hotelModel.getRoom_count() + room);
        if (Utils.isArabicLangSelected(mActivity)) {
            String nightInArbic = "";
            if (Integer.parseInt(hotelModel.getDur()) == 1) {
                nightInArbic = mActivity.getResources().getString(R.string.label_one_night);
            } else if (Integer.parseInt(hotelModel.getDur()) > 1 && Integer.parseInt(hotelModel.getDur()) < 11) {
                nightInArbic = mActivity.getResources().getString(R.string.label_below_ten_night);
            } else {
                nightInArbic = mActivity.getResources().getString(R.string.label_more_nights);
            }
            noOfNightRoom.setText(mActivity.getString(R.string.label_for_text) + " " + hotelModel.getDur() + " " + nightInArbic + ", " + hotelModel.getRoom_count() + room);
        }
        tripAdvisorLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handleHotelTripAdvisor(hotelModel.getHuid());
            }
        });
        if (hotelModel.getImage() != null && hotelModel.getImage().length() > 0) {
            image.setVisibility(View.VISIBLE);
            ViewTreeObserver vto = image.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    image.getViewTreeObserver().removeOnPreDrawListener(this);
                    int finalHeight = image.getMeasuredHeight();
                    int finalWidth = image.getMeasuredWidth();
                    String url = Constants.IMAGE_SCALING_URL + finalWidth + "x" + finalHeight + Constants.IMAGE_QUALITY + "" + hotelModel.getImage();
                    Glide.with(mActivity).load(url).placeholder(R.drawable.imagethumb).into(image);
                    return true;
                }
            });
        } else {
            image.setVisibility(View.GONE);
        }
        //Utils.printMessage(TAG, "Review :: "+modelList.getReviewCount() +" Image url :: "+modelList.getReviewImgUrl());
        if (Utils.isConnectingToInternet(mActivity)) {
            String json = makeJson();
//            int maxLogSize = 1000;
//            for (int i = 0; i <= json.length() / maxLogSize; i++) {
//                int start = i * maxLogSize;
//                int end = (i + 1) * maxLogSize;
//                end = end > json.length() ? json.length() : end;
//                Log.v(TAG, json.substring(start, end));
//            }
            Utils.printMessage(TAG, "json1" + json);
            if (json != null) {
                new HTTPAsync(mActivity, HotelTravelerDetailFragment.this, Constants.HOTEL_NEW_BOOKINGINFO, "",
                        json, BOOK_SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
                showLoading();
            }
        } else {
            try {
                SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, mActivity.MODE_PRIVATE);
                String memberAccessToken = pref.getString(Constants.MEMBER_ACCESS_TOKEN, "");
                if (!memberAccessToken.equalsIgnoreCase("")) {
                    searchButton.setText(mActivity.getResources().getString(R.string.continue_booking));
                } else if (memberAccessToken.equalsIgnoreCase("")) {
                    searchButton.setText(mActivity.getResources().getString(R.string.continue_as_guiest));
                }
                layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(regularLight);
                errorDescriptionText.setTypeface(regularLight);
                searchButton.setTypeface(regularLight);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                        if (isInternetPresent) {
                            loadingViewLayout.removeView(errorView);
                            loadingViewLayout.setVisibility(View.GONE);
                            String json = makeJson();
                            Utils.printMessage(TAG, "json" + json);
                            if (json != null) {
                                new HTTPAsync(mActivity, HotelTravelerDetailFragment.this, Constants.HOTEL_NEW_BOOKINGINFO,
                                        "", json, BOOK_SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
                                showLoading();
                            }
                        }
                    }
                });
                loadErrorType(Constants.NETWORK_ERROR);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
                loadingViewLayout.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter("com.flyin.bookings.ERROR_MESSAGE");
        mActivity.registerReceiver(myReceiver, intentFilter);
        return view;
    }

    private void findViews(View view) {
        TextView imgBack = (TextView) view.findViewById(R.id.img_back);
        ImageView headerMiddle = (ImageView) view.findViewById(R.id.header_middle);
        image = (ImageView) view.findViewById(R.id.image);
        hotelName = (TextView) view.findViewById(R.id.hotel_name);
        ratingBarHotel = (RatingBar) view.findViewById(R.id.ratingBar_hotel);
        hotelAddress = (TextView) view.findViewById(R.id.hotel_address);
        checkInTime = (TextView) view.findViewById(R.id.check_in_time);
        checkOutTime = (TextView) view.findViewById(R.id.check_out_time);
        roomType = (TextView) view.findViewById(R.id.room_type);
        breakfast = (TextView) view.findViewById(R.id.breakfast);
        TextView cancalation = (TextView) view.findViewById(R.id.cancalation);
        TextView cancelPolicyTitle = (TextView) view.findViewById(R.id.cancel_policy_title);
        cancelPolicyText = (TextView) view.findViewById(R.id.cancel_policy_text);
        hotDeal = (TextView) view.findViewById(R.id.hot_deal);
        offer = (TextView) view.findViewById(R.id.offer);
        discountCurrencyText = (TextView) view.findViewById(R.id.discount_currency_text);
        pricePrevious = (TextView) view.findViewById(R.id.price_previous);
        discountPriceDecimalText = (TextView) view.findViewById(R.id.discount_price_decimal_text);
        currencyText = (TextView) view.findViewById(R.id.currency_text);
        priceFinal = (TextView) view.findViewById(R.id.price_final);
        priceDecimalText = (TextView) view.findViewById(R.id.price_decimal_text);
        noOfNightRoom = (TextView) view.findViewById(R.id.no_of_night_room);
        TextView hotelTaxesText = (TextView) view.findViewById(R.id.hotel_taxes_text);
        submit = (Button) view.findViewById(R.id.submit);
        signin = (Button) view.findViewById(R.id.signin);
        errorView = (RelativeLayout) view.findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) view.findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(regularLight);
        persion_layout = (LinearLayout) view.findViewById(R.id.persion_image_layout);
        loadingViewLayout = (RelativeLayout) view.findViewById(R.id.loading_view_layout);
        TextView checkInHeader = (TextView) view.findViewById(R.id.check_in_text);
        TextView checkOutHeader = (TextView) view.findViewById(R.id.check_out_text);
        backLayout = (LinearLayout) view.findViewById(R.id.back_layout);
        bnplPackageLayout = (LinearLayout) view.findViewById(R.id.bnpl_package_layout);
        TextView bookNowHeader = (TextView) view.findViewById(R.id.book_now_header);
        TextView bookNowDescription = (TextView) view.findViewById(R.id.book_now_description);
        TextView totalPriceHeaderText = (TextView) view.findViewById(R.id.total_price_header);
        cancellationViewLayout = (LinearLayout) view.findViewById(R.id.cancellation_view_layout);
        cancellationDateText = (TextView) view.findViewById(R.id.cancellation_date);
        TextView roomTypeHeaderText = (TextView) view.findViewById(R.id.room_details_header);
        cancellationImage = (ImageView) view.findViewById(R.id.cancellation_image);
        cancellationDetailsLayout = (LinearLayout) view.findViewById(R.id.cancellation_details_layout);
        cancellationPolicyLayout = (RelativeLayout) view.findViewById(R.id.cancalation_policy_layout);
        discountPriceLayout = (LinearLayout) view.findViewById(R.id.previous_price_layout);
        advisorView = (View) view.findViewById(R.id.advisor_view);
        tripAdvisorLayout = (LinearLayout) view.findViewById(R.id.trip_advisor_layout);
        rewardsLayout = (LinearLayout) view.findViewById(R.id.reward_points_layout);
        rewardsPointsText = (TextView) view.findViewById(R.id.rewards_flight_dest_textview);
        owlImage = (ImageView) view.findViewById(R.id.owl_image);
        reviewText = (TextView) view.findViewById(R.id.review_text);
        submit.setOnClickListener(this);
        signin.setOnClickListener(this);
        hotelName.setTypeface(regularFont);
        hotelAddress.setTypeface(regularFont);
        checkInTime.setTypeface(regularFont);
        checkOutTime.setTypeface(regularFont);
        checkInHeader.setTypeface(regularFont);
        checkOutHeader.setTypeface(regularFont);
        roomTypeHeaderText.setTypeface(regularFont);
        roomType.setTypeface(regularFont);
        breakfast.setTypeface(regularFont);
        cancalation.setTypeface(regularFont);
        cancellationDateText.setTypeface(regularFont);
        cancelPolicyTitle.setTypeface(regularFont);
        cancelPolicyText.setTypeface(regularFont);
        hotDeal.setTypeface(regularFont);
        offer.setTypeface(regularFont);
        discountCurrencyText.setTypeface(regularFont);
        pricePrevious.setTypeface(regularBold);
        discountPriceDecimalText.setTypeface(regularFont);
        currencyText.setTypeface(regularFont);
        priceFinal.setTypeface(regularBold);
        priceDecimalText.setTypeface(regularFont);
        signin.setTypeface(regularBold);
        imgBack.setTypeface(regularFont);
        bookNowHeader.setTypeface(regularFont);
        bookNowDescription.setTypeface(regularFont);
        totalPriceHeaderText.setTypeface(regularMedium);
        noOfNightRoom.setTypeface(regularFont);
        hotelTaxesText.setTypeface(regularFont);
        reviewText.setTypeface(regularFont);
        rewardsPointsText.setTypeface(regularFont);
        if (isArabicLang) {
            headerMiddle.setImageResource(R.drawable.arabic_one);
            imgBack.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            hotelName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.userName_text));
            hotelAddress.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_time_text));
            checkInTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            checkOutTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            checkInHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            checkOutHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            roomTypeHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            roomType.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            breakfast.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            bookNowHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            bookNowDescription.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.flight_duration_text));
            cancalation.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            cancellationDateText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            cancelPolicyTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            cancelPolicyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_time_text));
            hotDeal.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.results_view_arabic));
            offer.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.results_view_arabic));
            totalPriceHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            noOfNightRoom.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            hotelTaxesText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_time_text));
            discountCurrencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            pricePrevious.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            discountPriceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            currencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.contact_text));
            priceFinal.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.button_text_size));
            priceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.contact_text));
            submit.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.contact_text));
            signin.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.contact_text));
            errorMessageText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            reviewText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_time_text));
            rewardsPointsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            roomType.setGravity(Gravity.START);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                roomType.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, -7, 0, 0);
            imgBack.setLayoutParams(params);
        }
        submit.setTypeface(regularBold);
        roomTypeHeaderText.setText(mActivity.getString(R.string.label_room_details) + ":");
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String memberAccessToken = pref.getString(Constants.MEMBER_ACCESS_TOKEN, "");
        if (!memberAccessToken.equalsIgnoreCase("")) {
            signin.setVisibility(View.GONE);
            submit.setText(mActivity.getResources().getString(R.string.continue_booking));
            isSingInClicked = false;
            //handleEranLoyaltyRequest();
        } else if (memberAccessToken.equalsIgnoreCase("")) {
            signin.setVisibility(View.VISIBLE);
            submit.setText(mActivity.getResources().getString(R.string.continue_as_guiest));
            isSingInClicked = true;
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        super.onActivityResult(reqCode, resCode, data);
        if (dialog != null) {
            dialog.passFacebookResult(reqCode, resCode, data);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == submit) {
            Gson gson = new Gson();
            String modelString = gson.toJson(hotelModel);
            Utils.printMessage(TAG, "gson" + modelString);
            Singleton.getInstance().model = modelString;
            Singleton.getInstance().hotelString = hotelString;
            Intent intent = new Intent(mActivity, HotelBookingPassengerActivity.class);
            intent.putExtra("isForHotel", true);
            intent.putExtra("adult_count", adultCount);
            intent.putExtra("child_count", childCount);
            intent.putExtra("pay_price", userPayPrice);
            intent.putExtra(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
            intent.putExtra("eng_hotel_name", earnPointsHotelName);
            intent.putExtra(Constants.USER_EARNED_POINTS, userEarnedPoints);
            mActivity.startActivity(intent);
        } else if (v == signin) {
            final SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            dialog = new SignInDialog(mActivity);
            if (!isSingInClicked) {
                pref.edit().putString(Constants.MEMBER_EMAIL, "").apply();
                pref.edit().putString(Constants.MEMBER_ACCESS_TOKEN, "").apply();
                pref.edit().putString(Constants.MEMBER_TOKEN_TYPE, "").apply();
                pref.edit().putString(Constants.USER_UNIQUE_ID, "").apply();
                pref.edit().putBoolean(Constants.isFanClub, false).apply();
                pref.edit().putBoolean(Constants.IS_REWARDS_ENABLED, false).apply();
                dialog.callFacebookLogout();
                isSingInClicked = true;
            } else {
                dialog.setCancelable(false);
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        String memberAccessToken = pref.getString(Constants.MEMBER_ACCESS_TOKEN, "");
                        if (!memberAccessToken.equalsIgnoreCase("")) {
                            signin.setVisibility(View.GONE);
                            submit.setText(mActivity.getResources().getString(R.string.continue_booking));
                            isSingInClicked = false;
                            if (!pref.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
                                if (pref.getBoolean(Constants.IS_REWARDS_ENABLED, false)) {
                                    handleEranLoyaltyRequest();
                                }
                            }
                        } else {
                            submit.setText(mActivity.getResources().getString(R.string.continue_as_guiest));
                            signin.setVisibility(View.VISIBLE);
                            isSingInClicked = true;
                        }
                    }
                });
                dialog.show();
            }
        }
    }

    private void getTokenFromServer() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(mActivity));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(mActivity, HotelTravelerDetailFragment.this, Constants.CLIENT_AUTHENTICATION,
                "", obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    public String makeJson() {
        final SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
        if (tokenTime == -1) {
            getTokenFromServer();
        } else {
            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
            if (diff > Long.parseLong(expireIn)) {
                getTokenFromServer();
            } else {
                accessTocken = preferences.getString(Constants.ACCESS_TOKEN, "0");
            }
        }
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        JSONObject obj = new JSONObject();
        try {
            SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            String userSelectedCurr = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
            String groupType = null;
            if (pref.getBoolean(Constants.isFanClub, false)) {
                groupType = "ALHILAL";
            }
            JSONObject source = new JSONObject();
            source = Utils.getHotelSearchAppSettingData(source, mActivity);
            source.put("clientId", Constants.HOTEL_CLIENT_ID);
            source.put("clientSecret", Constants.HOTEL_CLIENT_SECRET);
            source.put("paxNationality", Utils.getUserCountry(mActivity));
            source.put("echoToken", randomNumber);
            source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
            source.put("accessToken", Utils.getRequestAccessToken(mActivity));
            source.put("timeStamp", timeStamp);
            source.put("currency", userSelectedCurr);
            source.put("groupType", groupType);
            JSONObject sourceRQ = new JSONObject();
            sourceRQ.put("source", source);
            JSONObject hotel = new JSONObject(hotelModel.getHotelJson());
            if (mapFragmentData != null && !mapFragmentData.equalsIgnoreCase("") && mapFragmentData.equalsIgnoreCase("fromMapFrag")) {
                JSONObject mapHotelRoomObj = new JSONObject();
                try {
                    JSONArray hotelRoomArr = hotel.getJSONArray("rc");
                    for (int k = 0; k < hotelRoomArr.length(); k++) {
                        JSONArray roomJsonArray = hotelRoomArr.getJSONObject(k).getJSONArray("room");
                        _id = 1;
                        int roomId = 1;
                        for (int j = 0; j < roomJsonArray.length(); j++) {
                            roomJsonArray.getJSONObject(j).put("id", String.valueOf(roomId));
                            roomJsonArray.getJSONObject(j).put("lp", setLP());
                            roomJsonArray.getJSONObject(j).put("ocp", getPAXOBject(j));
                            roomId++;
                        }
                    }
                    JSONObject mapRoomObj = new JSONObject(hotelModel.getMapRoomJson());
                    JSONArray rcArray = new JSONArray();
                    rcArray.put(0, mapRoomObj);
                    mapHotelRoomObj.put("rc", rcArray);
                } catch (Exception e) {
                    Utils.printMessage(TAG, "ERROR :: " + e.getMessage());
                }
                sourceRQ.put("hotel", mapHotelRoomObj);
            }else {
                sourceRQ.put("hotel", hotel);
            }
            obj.put("BookingInfoRQ", sourceRQ);
            return obj.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject setLP() {
        JSONObject object = new JSONObject();
        try {
            object.put("fname", "lalit");
            object.put("lname", "sharma");
            object.put("age", "28");
            object.put("email", "lalitsharma1607@gmail.com");
            object.put("ph", "9015376344");
            return object;
        } catch (JSONException e) {
            Utils.printMessage(TAG, "Exception4" + e.getMessage());
        }
        return null;
    }

    private JSONObject getPAXOBject(int roomPos) {
        try {
            ArrayList<String> adult_count_list = modelList.getAdultcountlist();
            HashMap<String, ArrayList<String>> child_count_list = modelList.getChild_count_list();
            JSONObject rooms = new JSONObject();
            JSONArray pax = new JSONArray();
            int adult_count = Integer.parseInt(adult_count_list.get(roomPos));
            for (int j = 0; j < adult_count; j++) {
                JSONObject age_obj = new JSONObject();
                age_obj.put("age", 45);
                age_obj.put("id", _id);
                pax.put(age_obj);
                _id++;
            }
            rooms.put("na", adult_count);
            ArrayList<String> list;
            list = child_count_list.get("room" + (roomPos + 1));
            if (list != null) {
                for (int j = 0; j < list.size(); j++) {
                    JSONObject age_obj = new JSONObject();
                    age_obj.put("age", list.get(j));
                    age_obj.put("id", _id);
                    pax.put(age_obj);
                    _id++;
                }
                rooms.put("nc", list.size());
            }
            rooms.put("pax", pax);
            return rooms;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onTaskComplete(String data, final int serviceType) {
        Utils.printMessage(TAG, "data" + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            loadingViewLayout.removeAllViews();
            loadingViewLayout.setVisibility(View.GONE);
            closeProgress();
            try {
                if (isAdded())
                    layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(regularLight);
                errorDescriptionText.setTypeface(regularLight);
                searchButton.setTypeface(regularLight);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                        if (isInternetPresent) {
                            if (isAdded()) {
                                mActivity.onBackPressed();
                            }
                        }
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
                loadingViewLayout.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (serviceType == GET_ACCESS_TOKEN) {
                loadingViewLayout.removeAllViews();
                loadingViewLayout.setVisibility(View.GONE);
                Utils.printMessage(TAG, "GET_ACCESS_TOKEN" + data);
                JSONObject obj;
                try {
                    obj = new JSONObject(data);
                    String accessToken = obj.getString("accessToken");
                    String expireIn = obj.getString("expireIn");
                    String refreshToken = obj.getString("refreshToken");
                    long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                    SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Constants.ACCESS_TOKEN, accessToken);
                    editor.putString(Constants.EXPIRE_IN, expireIn);
                    editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                    editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                    editor.apply();
                    String json = makeJson();
                    Utils.printMessage(TAG, "json" + json);
                    if (json != null) {
                        new HTTPAsync(mActivity, HotelTravelerDetailFragment.this, Constants.HOTEL_NEW_BOOKINGINFO, "",
                                json, BOOK_SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
                        showLoading();
                    }
                } catch (JSONException e) {
                } catch (Exception e) {
                }
            } else if (serviceType == BOOK_SEARCH_HOTEL) {
                parsedaa(data);
                if (isPriceChanged) {
                    showPriceChangeAlert();
                }
                if (isBnplAvailable) {
                    isCancellationDataExpanded = false;
                    cancellationDetailsLayout.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!isCancellationDataExpanded) {
                                cancellationPolicyLayout.setVisibility(View.GONE);
                                isCancellationDataExpanded = true;
                                cancellationImage.setImageResource(R.drawable.date_top_arrow);
                            } else {
                                cancellationPolicyLayout.setVisibility(View.VISIBLE);
                                isCancellationDataExpanded = false;
                                cancellationImage.setImageResource(R.drawable.date_down_arrow);
                            }
                            Utils.printMessage(TAG, "boolean value : " + isCancellationDataExpanded);
                        }
                    });
                }
            } else if (serviceType == SEARCH_HOTEL_TRIP) {
                try {
                    JSONArray arr = new JSONArray(data);
                    for (int j = 0; j < arr.length(); j++) {
                        if (arr.getJSONObject(j).has("ratingObj")) {
                            TripAdvisorBean tripAdvisorBean = new TripAdvisorBean();
                            tripAdvisorBean.setHuid(arr.getJSONObject(j).get("huid").toString());
                            if (arr.getJSONObject(j).getJSONObject("ratingObj").has("num_reviews"))
                                tripAdvisorBean.setNumReview(arr.getJSONObject(j).getJSONObject("ratingObj").getString("num_reviews"));
                            if (arr.getJSONObject(j).getJSONObject("ratingObj").has("rating_image_url"))
                                tripAdvisorBean.setRatingImageUrl(arr.getJSONObject(j).getJSONObject("ratingObj").getString("rating_image_url"));
                            tripAdvisorAry.add(tripAdvisorBean);
                        }
                    }
                    if (tripAdvisorAry.size() > 0) {
                        try {
                            float star = Float.parseFloat(hotelModel.getStar());
                            if (tripAdvisorAry.get(0).getNumReview() == null || tripAdvisorAry.get(0).getNumReview().isEmpty()) {
                                tripAdvisorLayout.setVisibility(View.GONE);
                                advisorView.setVisibility(View.GONE);
                            } else {
                                if (Integer.parseInt(tripAdvisorAry.get(0).getNumReview()) == 0) {
                                    tripAdvisorLayout.setVisibility(View.GONE);
                                    advisorView.setVisibility(View.GONE);
                                } else {
                                    tripAdvisorLayout.setVisibility(View.VISIBLE);
                                    Resources res = mActivity.getResources();
                                    String text = String.format(res.getString(R.string.label_hotel_review), tripAdvisorAry.get(0).getNumReview());
                                    reviewText.setText(text);
                                    reviewText.setVisibility(View.VISIBLE);
                                    advisorView.setVisibility(View.VISIBLE);
                                    if (tripAdvisorAry.get(0).getRatingImageUrl() == null || tripAdvisorAry.get(0).getRatingImageUrl().isEmpty()) {
                                        owlImage.setVisibility(View.GONE);
                                    } else {
                                        Glide.with(mActivity).load(tripAdvisorAry.get(0).getRatingImageUrl()).placeholder(R.drawable.imagethumb_search).into(owlImage);
                                        owlImage.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                            if (star != 0.0) {
                                ratingBarHotel.setVisibility(View.VISIBLE);
                                ratingBarHotel.setRating(Float.parseFloat(hotelModel.getStar()));
                            } else {
                                ratingBarHotel.setVisibility(View.GONE);
                                advisorView.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            tripAdvisorLayout.setVisibility(View.GONE);
                            advisorView.setVisibility(View.GONE);
                            Utils.printMessage(TAG, "error Count : " + e.getMessage());
                        }
                    }
                    SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    if (!pref.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
                        if (pref.getBoolean(Constants.IS_REWARDS_ENABLED, false)) {
                            handleEranLoyaltyRequest();
                        } else {
                            loadingViewLayout.removeAllViews();
                            loadingViewLayout.setVisibility(View.GONE);
                        }
                    } else {
                        loadingViewLayout.removeAllViews();
                        loadingViewLayout.setVisibility(View.GONE);
                    }
                    Utils.printMessage(TAG, " tripAdvisorAry ::" + tripAdvisorAry.size());
                } catch (Exception e) {
                }
            } else if (serviceType == GET_TRIP_ADVISOR) {
                Utils.printMessage(TAG, "SEARCH_HOTEL :: " + data);
                tripArrayList.clear();
                try {
                    JSONObject obj = new JSONObject(data);
                    if (obj.has("status")) {
                        if (!obj.getString("status").equalsIgnoreCase("SUCCESS")) {
                            return;
                        }
                        TripDialogBean bean = new TripDialogBean();
                        bean.setItemType(TripDialogBean.RATING_HEADER);
                        bean.setExpanded(true);
                        tripArrayList.add(bean);
                        if (obj.has("data")) {
                            if (obj.getJSONObject("data").has("staticInfo")) {
                                JSONObject staticInfo = obj.getJSONObject("data").getJSONObject("staticInfo");
                                if (staticInfo.has("rating_image_url")) {
                                    bean = new TripDialogBean();
                                    bean.setItemType(TripDialogBean.RATING_ITEM);
                                    bean.setRatingImage(staticInfo.getString("rating_image_url"));
                                    bean.setReviewsCount(staticInfo.getString("num_reviews"));
                                    bean.setExpanded(true);
                                    tripArrayList.add(bean);
                                }
                                if (staticInfo.has("subratings")) {
                                    for (int i = 0; i < staticInfo.getJSONArray("subratings").length(); i++) {
                                        JSONObject tripObj = staticInfo.getJSONArray("subratings").getJSONObject(i);
                                        bean = new TripDialogBean();
                                        bean.setItemType(TripDialogBean.RATING_ITEM);
                                        bean.setRatingImage(tripObj.getString("rating_image_url"));
                                        bean.setReviewsCount("0");
                                        bean.setTitle(tripObj.getString("localized_name"));
                                        bean.setExpanded(true);
                                        tripArrayList.add(bean);
                                    }
                                }
                            }
                            bean = new TripDialogBean();
                            bean.setItemType(TripDialogBean.REVIEW_HEADER);
                            bean.setExpanded(true);
                            tripArrayList.add(bean);
                            if (obj.getJSONObject("data").has("review")) {
                                for (int i = 0; i < obj.getJSONObject("data").getJSONArray("review").length(); i++) {
                                    JSONObject tripObj = obj.getJSONObject("data").getJSONArray("review").getJSONObject(i);
                                    bean = new TripDialogBean();
                                    bean.setItemType(TripDialogBean.REVIEW_ITEM);
                                    bean.setRatingImage(tripObj.getString("rating_image_url"));
                                    bean.setTitle(tripObj.getString("title"));
                                    bean.setPublishedDate(tripObj.getString("published_date"));
                                    bean.setUser(tripObj.getString("user"));
                                    bean.setReview(tripObj.getString("review"));
                                    bean.setExpanded(true);
                                    tripArrayList.add(bean);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                }
                closeProgress();
                TripAdvisorDialog tripAdvisorDialog = new TripAdvisorDialog(mActivity, hotelModel.getHna(), tripArrayList);
                tripAdvisorDialog.show();
                tripAdvisorDialog.setCancelable(true);
                Window window = tripAdvisorDialog.getWindow();
                int width = (int) (mActivity.getResources().getDisplayMetrics().widthPixels * 1.0);
                int height = (int) (mActivity.getResources().getDisplayMetrics().heightPixels * 0.85);
                window.setLayout(width, height);
            } else if (serviceType == GET_LOYALTY_POINTS) {
                loadingViewLayout.removeAllViews();
                loadingViewLayout.setVisibility(View.GONE);
                Utils.printMessage(TAG, "Loyalty Points :: " + data);
                JSONObject obj = null;
                try {
                    obj = new JSONObject(data);
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        String points = obj.getString("earnPoints");
                        Utils.printMessage(TAG, "earn points::" + points);
                        userEarnedPoints = points;
                        rewardsLayout.setVisibility(View.VISIBLE);
                        rewardsPointsText.setText(String.format(mActivity.getResources().getString(R.string.label_rewards_earned_points), points));

                    } else {
                        Utils.printMessage(TAG, "earn points::" + obj.getString("message"));
                        rewardsLayout.setVisibility(View.GONE);
                    }
                } catch (Exception e) {

                }
            }
        }


    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        if (loadingViewLayout != null) {
            loadingViewLayout.removeAllViews();
            loadingViewLayout.setVisibility(View.GONE);
        }
    }

    public void parsedaa(String data) {
        Utils.printMessage(TAG, "response" + data);
        try {
            JSONObject obj = new JSONObject(data);
            JSONObject searchRSObj = obj.optJSONObject("BookingInfoRS");
            if (searchRSObj.has("st")) {
                if (searchRSObj.getString("st").equalsIgnoreCase("FAIL") ||
                        searchRSObj.getString("st").equalsIgnoreCase("null") ||
                        searchRSObj.getString("st") == null) {
                    gotoPreviousScreen();
                } else {
                    if (searchRSObj.has("err")) {
                        if (searchRSObj.getString("err") != null && !searchRSObj.getString("err").equalsIgnoreCase("null")) {
                            if (searchRSObj.getJSONObject("err").has("mes")) {
                                if (searchRSObj.getJSONObject("err").getString("mes").equalsIgnoreCase("PRICE_CHANGED")) {
                                    isPriceChanged = true;
                                }
                            }
                        }
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            String jsonTrip = hotelJsonTripAdvisor();
                            if (jsonTrip != null) {
                                boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                                if (isInternetPresent) {
                                    new HTTPAsync(mActivity, HotelTravelerDetailFragment.this, Constants.TRIP_ADVISOR_URL, "",
                                            jsonTrip, SEARCH_HOTEL_TRIP, HTTPAsync.METHOD_POST).execute();
                                }
                            }
                        }
                    }, 200);
                    JSONObject arr = searchRSObj.getJSONObject("hotel");
                    JSONObject rcObj = arr.optJSONObject("rc");
                    hotelString = rcObj.toString();
                    Singleton.getInstance().hotelString = "";
                    Singleton.getInstance().hotelString = hotelString;
                    Utils.printMessage(TAG, "inDate : " + rcObj.getString("ad"));
                    String hotelInDate = rcObj.getString("ad");
                    JSONObject htObject = rcObj.getJSONObject("ht");

                    try {
                        earnPointsHotelName = htObject.getString("hna");
                    } catch (Exception e) {
                    }
                    if (hotelModel.getCheck_in() == null || hotelModel.getCheck_in().equalsIgnoreCase("null")) {
                        checkInTime.setText(Utils.convertToJourneyDate(hotelInDate, mActivity));
                    } else {
                        checkInTime.setText(Utils.convertToJourneyDate(hotelInDate, mActivity) + " " + hotelModel.getCheck_in());
                    }
                    String hotelOutDate = Utils.addReviewDaysToCalendar(hotelInDate, rcObj.getString("dur"));
                    if (hotelModel.getCheck_out() == null || hotelModel.getCheck_out().equalsIgnoreCase("null")) {
                        checkOutTime.setText(Utils.convertToJourneyDate(hotelOutDate, mActivity));
                    } else {
                        checkOutTime.setText(Utils.convertToJourneyDate(hotelOutDate, mActivity) + " " + hotelModel.getCheck_out());
                    }
                    JSONObject pObj = rcObj.optJSONObject("p");
                    JSONObject wdObj = rcObj.optJSONObject("wdp");
                    double cp = Double.parseDouble(pObj.optString("val"));
                    double wdp = Double.parseDouble(wdObj.optString("val"));
                    if (isAdded()) {
                        Double price = Utils.convertPriceToUserSelectionWithDecimal(Double.parseDouble(pObj.optString("val")), pObj.optString("cur"), mActivity);
                        String actualPriceResult = String.format(Locale.ENGLISH, "%.2f", price);
                        Utils.printMessage(TAG, "price : " + actualPriceResult);
                        String priceResult[] = actualPriceResult.split("\\.");
                        earnPointsPrice = actualPriceResult;
                        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                        String currencyResult = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
                        if (isArabicLang) {
                            currencyText.setTypeface(regularFont);
                            priceFinal.setTypeface(regularBold);
                            priceDecimalText.setTypeface(regularFont);
                            currencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
                            priceFinal.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.text_size));
                            priceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
                            priceFinal.setText("." + priceResult[0] + " ");
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);
                            params.setMargins(0, 0, 0, 0);
                            currencyText.setLayoutParams(params);
                            currencyText.setText(priceResult[1]);
                            if (currencyResult.equalsIgnoreCase("SAR")) {
                                priceDecimalText.setText(mActivity.getString(R.string.label_SAR_currency_name));
                            } else {
                                priceDecimalText.setText(currencyResult);
                            }
                        } else {
                            currencyText.setText(currencyResult);
                            priceFinal.setText(priceResult[0] + ".");
                            priceDecimalText.setText(priceResult[1]);
                        }
                        userPayPrice = actualPriceResult;
                        if (hotelModel != null) {
                            hotelModel.setP(pObj.optString("val"));
                            hotelModel.setWdp(wdObj.optString("val"));
                        }
                    }
                    if (cp < wdp) {
                        discountPriceLayout.setVisibility(View.VISIBLE);
                        discountCurrencyText.setPaintFlags(discountCurrencyText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        pricePrevious.setPaintFlags(pricePrevious.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        discountPriceDecimalText.setPaintFlags(discountPriceDecimalText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        hotDeal.setVisibility(View.VISIBLE);
                        if (isAdded()) {
                            Double price = Utils.convertPriceToUserSelectionWithDecimal(Double.parseDouble(wdObj.optString("val")), wdObj.optString("cur"), mActivity);
                            String actualPriceResult = String.valueOf(price);
                            String priceResult[] = actualPriceResult.split("\\.");
                            SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                            String currencyResult = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
                            if (isArabicLang) {
                                discountCurrencyText.setTypeface(regularFont);
                                pricePrevious.setTypeface(regularBold);
                                discountPriceDecimalText.setTypeface(regularFont);
                                discountCurrencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
                                pricePrevious.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.text_size));
                                discountPriceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
                                pricePrevious.setText("." + priceResult[0] + " ");
                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);
                                params.setMargins(0, 0, 0, 0);
                                discountCurrencyText.setLayoutParams(params);
                                discountCurrencyText.setText(priceResult[1]);
                                if (currencyResult.equalsIgnoreCase("SAR")) {
                                    discountPriceDecimalText.setText(mActivity.getString(R.string.label_SAR_currency_name));
                                } else {
                                    discountPriceDecimalText.setText(currencyResult);
                                }
                            } else {
                                discountCurrencyText.setText(currencyResult);
                                pricePrevious.setText(priceResult[0] + ".");
                                discountPriceDecimalText.setText(priceResult[1]);
                            }
                        }
                        offer.setVisibility(View.VISIBLE);
                        double per = (((wdp - cp) * 100) / cp);
                        int percent = (int) per;
                        offer.setText(percent + mActivity.getString(R.string.price_saving_text) + Utils.convertPriceToUserSelectionHotel(Double.parseDouble(String.valueOf(wdp - cp)), wdObj.optString("cur"), mActivity));
                    } else {
                        discountPriceLayout.setVisibility(View.GONE);
                        hotDeal.setVisibility(View.GONE);
                        offer.setVisibility(View.GONE);
                    }
                    JSONArray jsonArrayRooms = rcObj.getJSONArray("room");
                    String roomName = jsonArrayRooms.getJSONObject(0).getString("rn");
                    roomType.setText(roomName);
                    breakfast.setText(Utils.getMealName(mActivity, jsonArrayRooms.getJSONObject(0).getString("mn")));
                    ImageView persion_image;
                    if (jsonArrayRooms.length() != 0) {
                        int na = 0;
                        int nc = 0;
                        int totalPassengers = 0;
                        for (int i = 0; i < jsonArrayRooms.length(); i++) {
                            if (jsonArrayRooms.getJSONObject(i).getJSONObject("ocp").has("na")) {
                                na = Integer.parseInt(jsonArrayRooms.getJSONObject(i).getJSONObject("ocp").getString("na"));
                            }
                            if (jsonArrayRooms.getJSONObject(i).getJSONObject("ocp").has("nc")) {
                                if (!Utils.checkStringVal(jsonArrayRooms.getJSONObject(i).getJSONObject("ocp").getString("nc")).equalsIgnoreCase("")) {
                                    nc = Integer.parseInt(jsonArrayRooms.getJSONObject(i).getJSONObject("ocp").getString("nc"));
                                }
                            }
                            int occupancy = na + nc;
                            if (totalPassengers < occupancy) {
                                totalPassengers = occupancy;
                            }
                        }
                        if (totalPassengers > 0)
                            for (int i = 0; i < totalPassengers; i++) {
                                if (isAdded()) {
                                    persion_image = new ImageView(mActivity);
                                    persion_image.setImageResource(R.drawable.hotel_review_passenger);
                                    persion_layout.addView(persion_image);
                                }
                            }
                    }
                    cancelPolicyText.setText(getString(R.string.no_cancellation_text));
                    if (jsonArrayRooms.getJSONObject(0).has("ci")) {
                        JSONObject ciObject = jsonArrayRooms.getJSONObject(0).getJSONObject("ci");
                        if (ciObject.has("nrf")) {
                            if (ciObject.optString("nrf") != null && !ciObject.optString("nrf").equals("null")) {
                                cancelPolicyText.setText(getString(R.string.non_refundable_hotel));
                            } else {
                                StringBuilder sb = new StringBuilder();
                                String resultDays = "";
                                if (ciObject.has("lfc")) {
                                    JSONObject lfcObject = ciObject.getJSONObject("lfc");
                                    if (lfcObject.has("d")) {
                                        if (lfcObject.optString("d") != null && !lfcObject.optString("d").equals("null")) {
                                            resultDays = Utils.flightSearchDateDifference(lfcObject.optString("d"), currentDate);
                                            if (Integer.parseInt(resultDays) >= 0) {
                                                sb.append(mActivity.getString(R.string.cancellation_text) + lfcObject.optString("d") + " " + lfcObject.optString("hou") + ":" + lfcObject.optString("min") + "\n");
                                                try {
                                                    if (lfcObject.has("d") && lfcObject.has("hou") && lfcObject.has("min")) {
                                                        String cancel_date = lfcObject.getString("d") + "T" + lfcObject.getString("hou") + ":" + lfcObject.getString("min");
                                                        cancellationDetailsLayout.setVisibility(View.VISIBLE);
                                                        cancellationDateText.setText(getString(R.string.label_room_selected_until_text) + " " + Utils.cancellationDateFormat(cancel_date, mActivity));
                                                        isBnplAvailable = true;
                                                    }
                                                } catch (Exception e) {
                                                }
                                                cancellationViewLayout.setVisibility(View.VISIBLE);
                                                bnplPackageLayout.setVisibility(View.VISIBLE);
                                            } else {
                                                cancellationViewLayout.setVisibility(View.GONE);
                                                bnplPackageLayout.setVisibility(View.GONE);
                                                isBnplAvailable = false;
                                            }
                                        }
                                    }
                                }
                                if (ciObject.has("cp")) {
                                    JSONArray cpArray = ciObject.getJSONArray("cp");
                                    for (int i = 0; i < cpArray.length(); i++) {
                                        JSONObject cpArrayJSONObject = cpArray.getJSONObject(i);
                                        JSONObject crgObject = cpArrayJSONObject.getJSONObject("crg");
                                        JSONObject psObject = cpArrayJSONObject.getJSONObject("ps");
                                        JSONObject peObject = cpArrayJSONObject.getJSONObject("pe");
                                        String fromDate = psObject.optString("d") + " " + psObject.optString("hou") + ":" + psObject.optString("min");
                                        String toDate = peObject.optString("d") + " " + peObject.optString("hou") + ":" + peObject.optString("min");
                                        String policyData = "";
                                        if (crgObject.getString("ty").equalsIgnoreCase("Nights")) {
                                            policyData = crgObject.optString("val") + " " + mActivity.getString(R.string.label_more_nights);
                                        } else if (crgObject.getString("ty").equalsIgnoreCase("Percentage")) {
                                            policyData = crgObject.optString("val") + "%";
                                        } else {
                                            policyData = crgObject.optString("val") + " " + crgObject.optString("ty");
                                        }
                                        if (isArabicLang) {
                                            sb.append(" " + policyData + " " + getString(R.string.booking_from_text) + " (" + getString(R.string.label_from_text) + " " + fromDate + " " + getString(R.string.to_text) + " " + toDate + ")").append((i == (cpArray.length() - 1)) ? "" : "\n");
                                        } else {
                                            sb.append(" " + policyData + " " + getString(R.string.booking_from_text) + " (" + getString(R.string.label_from_text) + " " + fromDate + " " + getString(R.string.to_text) + " " + toDate + ")").append((i == (cpArray.length() - 1)) ? "" : "\n");
                                        }
                                    }
                                }
                                cancelPolicyText.setText(sb.toString());
                            }
                        } else {
                            StringBuilder sb = new StringBuilder();
                            String resultDays = "";
                            if (ciObject.has("lfc")) {
                                JSONObject lfcObject = ciObject.getJSONObject("lfc");
                                if (lfcObject.has("d")) {
                                    if (lfcObject.optString("d") != null && !lfcObject.optString("d").equals("null")) {
                                        resultDays = Utils.flightSearchDateDifference(lfcObject.optString("d"), currentDate);
                                        if (Integer.parseInt(resultDays) >= 0) {
                                            sb.append(mActivity.getString(R.string.cancellation_text) + lfcObject.optString("d") + " " + lfcObject.optString("hou") + ":" + lfcObject.optString("min") + "\n");
                                            try {
                                                if (lfcObject.has("d") && lfcObject.has("hou") && lfcObject.has("min")) {
                                                    String cancel_date = lfcObject.getString("d") + "T" + lfcObject.getString("hou") + ":" + lfcObject.getString("min");
                                                    cancellationDetailsLayout.setVisibility(View.VISIBLE);
                                                    cancellationDateText.setText(getString(R.string.label_room_selected_until_text) + " " + Utils.cancellationDateFormat(cancel_date, mActivity));
                                                    isBnplAvailable = true;
                                                }
                                            } catch (Exception e) {
                                            }
                                            cancellationViewLayout.setVisibility(View.VISIBLE);
                                            bnplPackageLayout.setVisibility(View.VISIBLE);
                                        } else {
                                            cancellationViewLayout.setVisibility(View.GONE);
                                            bnplPackageLayout.setVisibility(View.GONE);
                                            isBnplAvailable = false;
                                        }
                                    }
                                }
                            }
                            if (ciObject.has("cp")) {
                                JSONArray cpArray = ciObject.getJSONArray("cp");
                                for (int i = 0; i < cpArray.length(); i++) {
                                    JSONObject cpArrayJSONObject = cpArray.getJSONObject(i);
                                    JSONObject crgObject = cpArrayJSONObject.getJSONObject("crg");
                                    JSONObject psObject = cpArrayJSONObject.getJSONObject("ps");
                                    JSONObject peObject = cpArrayJSONObject.getJSONObject("pe");
                                    String fromDate = psObject.optString("d") + " " + psObject.optString("hou") + ":" + psObject.optString("min");
                                    String toDate = peObject.optString("d") + " " + peObject.optString("hou") + ":" + peObject.optString("min");
                                    String policyData = "";
                                    if (crgObject.getString("ty").equalsIgnoreCase("Nights")) {
                                        policyData = crgObject.optString("val") + " " + mActivity.getString(R.string.label_more_nights);
                                    } else if (crgObject.getString("ty").equalsIgnoreCase("Percentage")) {
                                        policyData = crgObject.optString("val") + "%";
                                    } else {
                                        policyData = crgObject.optString("val") + " " + crgObject.optString("ty");
                                    }
                                    if (isArabicLang) {
                                        sb.append(" " + policyData + " " + getString(R.string.booking_from_text) + " (" + getString(R.string.label_from_text) + " " + fromDate + " " + getString(R.string.to_text) + " " + toDate + ")").append((i == (cpArray.length() - 1)) ? "" : "\n");
                                    } else {
                                        sb.append(" " + policyData + " " + getString(R.string.booking_from_text) + " (" + getString(R.string.label_from_text) + " " + fromDate + " " + getString(R.string.to_text) + " " + toDate + ")").append((i == (cpArray.length() - 1)) ? "" : "\n");
                                    }
                                }
                            }
                            cancelPolicyText.setText(sb.toString());
                        }
                    }
                }
            } else {
                gotoPreviousScreen();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception er) {
            er.printStackTrace();
        }
    }

    private void showPriceChangeAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
        alertDialog.setCancelable(false);
        alertDialog.setTitle(mActivity.getString(R.string.flyin));
        alertDialog.setMessage(mActivity.getString(R.string.label_price_changed));
        alertDialog.setPositiveButton(mActivity.getResources().getString(R.string.label_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setNegativeButton(mActivity.getResources().getString(R.string.label_no_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mActivity.onBackPressed();
            }
        });
        alertDialog.show();
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(mActivity.getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_network_error_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(mActivity.getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_no_results_found_message_hotels));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_search_text));
                break;
            case Constants.SELECTED_HOTEL_NOT_FOUND_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.selected_hotel_not_available_error));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            default:
                break;
        }
    }

    private void showLoading() {
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView waitText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setTypeface(regularBold);
        waitText.setTypeface(regularLight);
        loadingText.setVisibility(View.GONE);
        waitText.setText(mActivity.getResources().getString(R.string.label_just_a_moment));
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.removeAllViews();
        loadingViewLayout.addView(loadingView);
        loadingViewLayout.setVisibility(View.VISIBLE);
    }

    private void gotoPreviousScreen() {
        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        if (editor != null) {
            editor.putString("errorMessage", "No_Hotel");
            editor.apply();
        }
        mActivity.onBackPressed();
    }

    private String hotelJsonTripAdvisor() {
        String result = null;
        JSONObject source = new JSONObject();
        JSONObject crt = new JSONObject();
        JSONObject hcp = new JSONObject();
        JSONObject finaldata = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        try {
            source.put("device", Constants.DEVICE_TYPE);
            source.put("clientId", Constants.HOTEL_CLIENT_ID);
            source.put("clientSecret", Constants.HOTEL_CLIENT_SECRET);
            source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
            source.put("accessToken", Utils.getRequestAccessToken(mActivity));
            source.put("timeStamp", timeStamp);
            JSONArray mJSONArray = new JSONArray(huidlist);
            crt.put("hcid", tripAdvisorCityId);
            crt.put("huid", mJSONArray);
            hcp.put("source", source);
            hcp.put("crt", crt);
            finaldata.put("hcp", hcp);
            return finaldata.toString();
        } catch (JSONException e) {
        } catch (Exception e) {
        }
        return result;
    }

    private void handleHotelTripAdvisor(String hUid) {
        showProgress();
        String json = makeReviewsJson(hUid);
        new HTTPAsync(mActivity, HotelTravelerDetailFragment.this, Constants.TRIP_ADVISOR_DETAIL_URL, "", json,
                GET_TRIP_ADVISOR, HTTPAsync.METHOD_POST).execute();
    }

    private void handleEranLoyaltyRequest() {
        String earnPoints = handleEarnLoyaltyPoints();
        showLoading();
        HTTPAsync async = new HTTPAsync(mActivity, HotelTravelerDetailFragment.this, Constants.CALCULATE_EARN_POINTS,
                "", earnPoints, GET_LOYALTY_POINTS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String makeReviewsJson(String hUid) {
        String result = "";
        final SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String language = preferences.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
        if (language.equalsIgnoreCase("en_GB")) {
            language = "en_US";
        }
        JSONObject source = new JSONObject();
        try {
            source.put("apikey", Constants.MERCHANDISE_API_KEY);
            source.put("HUID", hUid);
            source.put("lang", language);
            return source.toString();
        } catch (Exception e) {
        }
        return result;
    }

    private void showProgress() {
        if (barProgressDialog == null) {
            barProgressDialog = new ProgressDialog(mActivity);
            barProgressDialog.setMessage(mActivity.getString(R.string.label_just_a_moment));
            barProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            barProgressDialog.show();
        }
    }

    private void closeProgress() {
        if (barProgressDialog != null) {
            barProgressDialog.dismiss();
            barProgressDialog = null;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    private String handleEarnLoyaltyPoints() {
        JSONObject sourceObj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, 0);
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        try {
            String ddt = null, ardt = null, mal = null;

            JSONObject hotelObj = new JSONObject();
            JSONArray hotelTypearray = new JSONArray(huidlist);
            sourceObj.put("apikey", Constants.MERCHANDISE_API_KEY);
            sourceObj.put("userid", pref.getString(Constants.USER_UNIQUE_ID, ""));
            sourceObj.put("product", "hotel");
            sourceObj.put("currency", "SAR");
            sourceObj.put("basefareAmount", Double.parseDouble(Utils.convertOtherCurrencytoSarCurrency(Double.parseDouble(earnPointsPrice), mActivity)));
            sourceObj.put("totalAmount", Double.parseDouble(Utils.convertOtherCurrencytoSarCurrency(Double.parseDouble(earnPointsPrice), mActivity)));
            sourceObj.put("companyId", Constants.LOYALTY_COMPANY_ID);
            hotelObj.put("crs", crs(hotelModel.getHotelTypeValue()));
            hotelObj.put("rating", hotelModel.getStar());
            hotelObj.put("hname", earnPointsHotelName);
            hotelObj.put("cityId", hotelModel.getCityId());
            hotelObj.put("hotelType", hotelTypearray);
            sourceObj.put("hotel", hotelObj);
            sourceObj.put("bookingDate", timeStamp + ".000Z");
            sourceObj.put("travelPeriodFrom", Utils.formatDateToServerDateFormat(hotelModel.getCheck_in_date()) + "T" + currentTimeFormat + ".000Z");
            sourceObj.put("travelPeriodTo", Utils.formatDateToServerDateFormat(hotelModel.getCheck_out_date()) + "T" + currentTimeFormat + ".000Z");
            sourceObj.put("groupType", groupType);
            Utils.printMessage("handleEarnLoyaltyPoints", sourceObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sourceObj.toString();
    }

    private String crs(String htValue) {
        String crs = "";
        try {
            JSONObject hotelCrs = new JSONObject(hotelModel.getHotelTypeValue());
            crs = hotelCrs.get("crs").toString();
            return crs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return crs;
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.ERROR_MESSAGE")) {
                String type = "";
                if (intent.hasExtra("type")) {
                    type = intent.getStringExtra("type");
                }
                if (type.equalsIgnoreCase("success")) {
                    displaySuccessMessage(intent.getStringExtra("message"), true);
                } else {
                    displaySuccessMessage(intent.getStringExtra("message"), false);
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.unregisterReceiver(myReceiver);
    }

    private void displaySuccessMessage(String message, boolean isSucessMessage) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            if (isSucessMessage) {
                errorView.setBackgroundResource(R.color.success_message_background);
            } else {
                errorView.setBackgroundResource(R.color.error_message_background);
            }
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(mActivity.getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(mActivity.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }
}
