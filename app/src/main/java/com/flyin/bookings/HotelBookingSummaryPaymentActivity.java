package com.flyin.bookings;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;
import com.google.gson.Gson;

import org.json.JSONObject;

@SuppressWarnings("deprecation")
public class HotelBookingSummaryPaymentActivity extends AppCompatActivity implements com.flyin.bookings.services.AsyncTaskListener {
    private static final String TAG = "HotelBookingSummaryPaymentActivity";
    private static final int GET_AFP_RESPONSE = 2;
    private String tokenId = "";
    private String pnrNumber = "";
    private String currency = "";
    private String flyinCode = "";
    private String userFullName = "";
    private String emailId = "";
    private String ticketRQJSONObject = "";
    private String currencyValue = "";
    private boolean isInternetPresent = false;
    private ImageView errorImage;
    private Button searchButton;
    private LayoutInflater inflater;
    private TextView errorText, errorDescriptionText;
    private Typeface textFace;
    private RelativeLayout loadingViewLayout;
    private View loadingView;
    private String iqu = "";
    private String pcc = "";
    private String bnplbkng = "";
    private String statusCode = "";
    private String status = "";
    private boolean isPaymentFinished = false;
    private ProgressBar pBar;
    private HotelModel hotelModel;
    private String refId;
    private String cryptJson;
    private String hotelInfoJson;
    private String hotelDataJson;
    private String hotelJson;
    private String bnplPaymentUrl = "";
    private String cua = "";
    private String upi;
    private String randomNumber = "";
    private String qitafDiscount = "";
    private String rewardsDiscount = "";
    private String hotelNameInEng = "";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tripsummary_payment);
        isInternetPresent = Utils.isConnectingToInternet(HotelBookingSummaryPaymentActivity.this);
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(HotelBookingSummaryPaymentActivity.this)) {
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        textFace = Typeface.createFromAsset(getAssets(), fontText);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        WebView webview = (WebView) findViewById(R.id.web_view);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String paymentUrl = bundle.getString("PAYMENT_URL", "");
            bnplPaymentUrl = bundle.getString("BNPL_PAYMENT_URL", "");
            tokenId = bundle.getString("TOKENID", "");
            cryptJson = bundle.getString("CRYPT_JSON", "");
            refId = bundle.getString("BOOKING_ID", "");
            hotelJson = bundle.getString("HOTEL_OBJ", "");
            hotelDataJson = bundle.getString("BOOKING_DATA_OBJ", "");
            hotelInfoJson = bundle.getString("BOOKING_INFO_OBJ", "");
            randomNumber = bundle.getString(Constants.BOOKING_RANDOM_NUMBER, "");
            userFullName = bundle.getString(Constants.USER_FULL_NAME, "");
            emailId = bundle.getString(Constants.USER_EMAIL, "");
            hotelNameInEng = bundle.getString("eng_hotel_name", "");
            Utils.printMessage(TAG, "paymentUrl :: " + paymentUrl);
            webview.loadUrl(paymentUrl);
        }
        String hotelModelString = Singleton.getInstance().model;
        Gson gson = new Gson();
        hotelModel = gson.fromJson(hotelModelString, HotelModel.class);
        if (!isInternetPresent) {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(HotelBookingSummaryPaymentActivity.this);
                    if (isInternetPresent) {
                        loadingViewLayout.removeView(errorView);
                        Singleton.getInstance().roomPassengerInfoList.clear();
                        Intent intent = new Intent(HotelBookingSummaryPaymentActivity.this, MainSearchActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
        pBar = (ProgressBar) findViewById(R.id.progress);
        pBar.setMax(100);
        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Utils.printMessage(TAG, "Processing Web View url click...");
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                pBar.setVisibility(View.VISIBLE);
                pBar.setProgress(0);
                super.onPageStarted(view, url, favicon);
            }

            public void onPageFinished(WebView view, final String url) {
                Utils.printMessage(TAG, "URL::" + url);
                pBar.setVisibility(View.GONE);
                pBar.setProgress(100);
                try {
                    String[] outerStr = url.split("\\?");
                    if (outerStr.length == 2) {
                        String[] str = outerStr[1].split("&");
                        String message = "";
                        for (String item : str) {
                            String[] innerStr = item.split("=");
                            if (innerStr.length == 2) {
                                if (innerStr[0].equalsIgnoreCase("status")) {
                                    status = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("message")) {
                                    message = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("statusCode")) {
                                    statusCode = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("iqu")) {
                                    iqu = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("pcc")) {
                                    pcc = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("bnplbkng")) {
                                    bnplbkng = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("cua")) {
                                    cua = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("qra")) {
                                    qitafDiscount = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("lra")) {
                                    rewardsDiscount = innerStr[1];
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                }
                onThreadExecutionFinished();
            }
        });
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && pBar.getVisibility() == ProgressBar.GONE) {
                    pBar.setVisibility(ProgressBar.VISIBLE);
                }
                pBar.setProgress(progress);
                if (progress == 100) {
                    pBar.setVisibility(ProgressBar.GONE);
                }
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.VISIBLE);
        ImageView mImageView = (ImageView) mCustomView.findViewById(R.id.first_home_icon);
        ImageView mHeaderImage = (ImageView) mCustomView.findViewById(R.id.first_home_logo);
        TextView mSignInText = (TextView) mCustomView.findViewById(R.id.sing_in_button);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.home_back_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_home_text);
        mSignInText.setVisibility(View.GONE);
        mHeaderImage.setImageResource(R.drawable.screen_three);
        backText.setTypeface(textFace);
        if (Utils.isArabicLangSelected(HotelBookingSummaryPaymentActivity.this)) {
            mImageView.setScaleType(ImageView.ScaleType.FIT_START);
            mHeaderImage.setImageResource(R.drawable.arabic_three);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = getIntent();
                setResult(RESULT_OK, it);
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    private String HandleAfterPaymentRequest() {
        try {
            JSONObject mainJSON = new JSONObject(cryptJson);
            mainJSON.remove("upi");
            //mainJSON.put("brn", refId);
//            mainJSON.put("tid", tokenId);
//            mainJSON.put("usrc", Utils.getSelectedCurrencyCode(this));
//            mainJSON.put("usrba", hotelModel.getP());
//            mainJSON.put("rurl", "");
//            mainJSON.put("prdct", "H");
            mainJSON.put("stus", status);
            mainJSON.put("stusc", statusCode);
//            mainJSON.put("lpax", userFullName);
//            mainJSON.put("lpaxemail", emailId);
            mainJSON.put("iqu", iqu);
            mainJSON.put("dc", pcc);
            mainJSON.put("stype", "AFP");
            Utils.printMessage("after payment crypt json", "" + mainJSON);
            return mainJSON.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void getAfterPaymentRequestFromServer(String requestJSON) {
        com.flyin.bookings.services.HTTPAsync async = new com.flyin.bookings.services.HTTPAsync(HotelBookingSummaryPaymentActivity.this, HotelBookingSummaryPaymentActivity.this, Constants.SECURE_PAYMENT_URL, "", requestJSON,
                GET_AFP_RESPONSE, com.flyin.bookings.services.HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "DATA::" + data);
        JSONObject obj = null;
        try {
            if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
                inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = inflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(textFace);
                errorDescriptionText.setTypeface(textFace);
                searchButton.setTypeface(textFace);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(HotelBookingSummaryPaymentActivity.this, MainSearchActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            } else {
                obj = new JSONObject(data);
                if (obj.has("accessToken")) {
                    String accessToken = obj.getString("accessToken");
                    if (serviceType == GET_AFP_RESPONSE) {
                        String afpStatus = obj.getString("status");
                        if (afpStatus.equalsIgnoreCase("SUCCESS")) {
                            Intent intent = new Intent(HotelBookingSummaryPaymentActivity.this, HotelConfirmationScreen.class);
                            //intent.putExtra("HOTEL", hotelModelGson);
                            intent.putExtra("HOTEL_OBJ", "" + hotelJson);
                            intent.putExtra("BOOKING_DATA_OBJ", "" + hotelDataJson);
                            intent.putExtra("BOOKING_INFO_OBJ", "" + hotelInfoJson);
                            intent.putExtra("BOOKING_ID", refId);
                            intent.putExtra("bnplBookingStatus", bnplbkng);
                            intent.putExtra("BNPL_PAYMENT_URL", bnplPaymentUrl);
                            intent.putExtra(Constants.COUPON_AMOUNT, cua);
                            intent.putExtra(Constants.USER_FULL_NAME, userFullName);
                            intent.putExtra(Constants.USER_EMAIL, emailId);
                            intent.putExtra("QITAF_DISCOUNT", qitafDiscount);
                            intent.putExtra("REWARDS_DISCOUNT", rewardsDiscount);
                            intent.putExtra("eng_hotel_name", hotelNameInEng);
                            startActivity(intent);
                            this.finish();
                            //closeLoading();
                        } else {
                            closeLoading();
                            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View errorView = inflater.inflate(R.layout.view_error_response, null);
                            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                            errorText = (TextView) errorView.findViewById(R.id.error_text);
                            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                            searchButton = (Button) errorView.findViewById(R.id.search_button);
                            errorText.setTypeface(textFace);
                            errorDescriptionText.setTypeface(textFace);
                            searchButton.setTypeface(textFace);
                            searchButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Singleton.getInstance().roomPassengerInfoList.clear();
                                    Intent intent = new Intent(HotelBookingSummaryPaymentActivity.this, MainSearchActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });
                            loadErrorType(Constants.WRONG_ERROR_PAGE);
                            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            loadingViewLayout.addView(errorView);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {

    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_payment_loading_text);
        loadingText.setTypeface(textFace);
        descriptionText.setVisibility(View.GONE);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    private void loadErrorType(int type) {

        switch (type) {
            case Constants.ERROR_PAGE: //"404errorpage":
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE: //"errorpage":
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR: //"networkerror":
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(getResources().getString(R.string.label_network_error_message));
                searchButton.setText(getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED: // "paymentfailed":
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR: //"resulterror":
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getResources().getString(R.string.label_no_results_found_message_hotels));
                searchButton.setText(getResources().getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent it = getIntent();
        setResult(RESULT_OK, it);
        finish();
    }

    private void onThreadExecutionFinished() {
        if (statusCode.isEmpty()) {
            //Log.e(TAG, "url not correct");
        } else if (statusCode.equalsIgnoreCase("200")) {
            if (!isPaymentFinished) {
                isPaymentFinished = true;
                isInternetPresent = Utils.isConnectingToInternet(HotelBookingSummaryPaymentActivity.this);
                final String requestJson = HandleAfterPaymentRequest();
                if (isInternetPresent) {
                    getAfterPaymentRequestFromServer(requestJson);
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(textFace);
                    errorDescriptionText.setTypeface(textFace);
                    searchButton.setTypeface(textFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(HotelBookingSummaryPaymentActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                getAfterPaymentRequestFromServer(requestJson);
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        } else if (statusCode.equalsIgnoreCase("108")) {
            Utils.printMessage(TAG, "Payment Failure");
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HotelBookingSummaryPaymentActivity.this, MainSearchActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            loadErrorType(Constants.PAYMENT_FAILED);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
    }
}
