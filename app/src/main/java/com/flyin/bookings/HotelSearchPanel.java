package com.flyin.bookings;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyin.bookings.adapter.HotelSearchAdapter;
import com.flyin.bookings.listeners.AirlinesInterface;
import com.flyin.bookings.model.CityModel;
import com.flyin.bookings.model.PredectiveSearchModel;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.DbHandler;
import com.flyin.bookings.util.GPSTracker;
import com.flyin.bookings.util.LocationSearchData;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

public class HotelSearchPanel extends AppCompatActivity implements AsyncTaskListener {
    private static final String TAG = "SearchAirportActivity";
    private static final int GET_CITY = 1;
    private static final int GET_LOCATION = 2;
    TextView errorText, errorDescriptionText;
    LayoutInflater layoutInflater;
    ArrayList<String> listdata;
    ArrayList<CityModel> modelListdata;
    private TextView recentlySearchedText, cancelAirport;
    private LinearLayout searchAirportScreenLayout, searchAirportTextView, recentSearchText;
    private Typeface lightFace;
    private ListView searchListView;
    private HotelSearchAdapter airportAdapter;
    private EditText inputSearch;
    private GPSTracker gps;
    private String selectedLang = "";
    private ProgressDialog barProgressDialog = null;
    private RelativeLayout errorView = null, loadingViewLayout;
    private ImageView errorImage;
    private Button searchButton;

    private static void requestPermission(final Context context) {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION)) {
            new AlertDialog.Builder(context)
                    .setMessage(context.getResources().getString(R.string.permission_storage))
                    .setPositiveButton(R.string.label_selected_flight_message, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                    2);
                        }
                    }).show();
        } else {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    2);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hotel_search_panel);
        modelListdata = new ArrayList<>();
        listdata = new ArrayList<>();
        searchListView = (ListView) findViewById(R.id.search_airports_listView);
        if (Utils.isArabicLangSelected(HotelSearchPanel.this)) {
            selectedLang = Constants.LANGUAGE_ARABIC_CODE;
        } else {
            selectedLang = Constants.LANGUAGE_ENGLISH_CODE;
        }
        DbHandler dbHandler = new DbHandler(HotelSearchPanel.this);
        modelListdata = dbHandler.getRecentHotelsList(selectedLang);
        airportAdapter = new HotelSearchAdapter(HotelSearchPanel.this, modelListdata);
        searchListView.setAdapter(airportAdapter);
        airportAdapter.notifyDataSetChanged();
        selectedLang = Constants.LANGUAGE_ENGLISH_CODE;
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        TextView searchAirportText = (TextView) findViewById(R.id.search_airport_title);
        TextView currentLocationText = (TextView) findViewById(R.id.current_location_title);
        recentlySearchedText = (TextView) findViewById(R.id.recently_searched_title);
        cancelAirport = (TextView) findViewById(R.id.cancel_airport);
        searchAirportScreenLayout = (LinearLayout) findViewById(R.id.search_airport_screen_layout);
        inputSearch = (EditText) findViewById(R.id.search_airport_view);
        searchAirportTextView = (LinearLayout) findViewById(R.id.search_airport_textView);
        recentSearchText = (LinearLayout) findViewById(R.id.recent_search_text);
        LinearLayout currentLocationLayout = (LinearLayout) findViewById(R.id.current_location_layout);
        ImageView clearTextImage = (ImageView) findViewById(R.id.clear_text_image);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        errorView.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(HotelSearchPanel.this)) {
            selectedLang = Constants.LANGUAGE_ARABIC_CODE;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_DROIDKUFI_BOLD;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                searchAirportText.setTextDirection(View.TEXT_DIRECTION_RTL);
                inputSearch.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
            currentLocationText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            recentlySearchedText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            searchAirportText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            inputSearch.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            cancelAirport.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontTitle);
        lightFace = Typeface.createFromAsset(getAssets(), fontText);
        Typeface boldFace = Typeface.createFromAsset(getAssets(), fontBold);
        searchAirportText.setTypeface(textFace);
        currentLocationText.setTypeface(textFace);
        recentlySearchedText.setTypeface(boldFace);
        cancelAirport.setTypeface(textFace);
        inputSearch.setTypeface(textFace);
        recentlySearchedText.setVisibility(modelListdata.size() == 0 ? View.GONE : View.VISIBLE);
        searchAirportText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchAirportTextView.setVisibility(View.GONE);
                searchAirportScreenLayout.setVisibility(View.VISIBLE);
                recentSearchText.setVisibility(View.GONE);
                inputSearch.requestFocus();
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                modelListdata = new ArrayList<>();
                airportAdapter = new HotelSearchAdapter(HotelSearchPanel.this, modelListdata);
                searchListView.setAdapter(airportAdapter);
                recentlySearchedText.setVisibility(modelListdata.size() == 0 ? View.GONE : View.VISIBLE);
            }
        });
        cancelAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchAirportTextView.setVisibility(View.VISIBLE);
                searchAirportScreenLayout.setVisibility(View.GONE);
                recentSearchText.setVisibility(View.VISIBLE);
                final InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.hideSoftInputFromWindow(cancelAirport.getWindowToken(), 0);
                inputSearch.setText("");
                onStop();
                modelListdata.clear();
                DbHandler dbHandler = new DbHandler(HotelSearchPanel.this);
                modelListdata = dbHandler.getRecentHotelsList(selectedLang);
                airportAdapter = new HotelSearchAdapter(HotelSearchPanel.this, modelListdata);
                searchListView.setAdapter(airportAdapter);
                airportAdapter.notifyDataSetChanged();
                recentlySearchedText.setVisibility(modelListdata.size() == 0 ? View.GONE : View.VISIBLE);
            }
        });
        clearTextImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputSearch.setText("");
                modelListdata.clear();
                airportAdapter.notifyDataSetChanged();
            }
        });
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(final CharSequence cs, int arg1, int arg2, int arg3) {
                Utils.printMessage(TAG, "input char " + cs);
                if (cs.toString().trim().length() >= 3) {
                    boolean isInternetPresent = Utils.isConnectingToInternet(HotelSearchPanel.this);
                    if (isInternetPresent) {
                        try {
                            new HTTPAsync(HotelSearchPanel.this, HotelSearchPanel.this,
                                    Constants.HOTEL_PREDICTIVE_SEARCH, "" + URLEncoder.encode("" + cs.toString().trim(), "UTF-8"), GET_CITY, 1)
                                    .execute();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    } else {
                        layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                        errorText = (TextView) errorView.findViewById(R.id.error_text);
                        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                        searchButton = (Button) errorView.findViewById(R.id.search_button);
                        errorText.setTypeface(lightFace);
                        errorDescriptionText.setTypeface(lightFace);
                        searchButton.setTypeface(lightFace);
                        searchButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadingViewLayout.removeView(errorView);
                                try {
                                    new HTTPAsync(HotelSearchPanel.this, HotelSearchPanel.this,
                                            Constants.HOTEL_PREDICTIVE_SEARCH, "" + URLEncoder.encode("" + cs, "UTF-8"), GET_CITY, 1)
                                            .execute();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        loadErrorType(Constants.NETWORK_ERROR);
                        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        loadingViewLayout.addView(errorView);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
        searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
                String localLang = Locale.getDefault().getLanguage();
                String keyboardLang = "";
                if (localLang.contains("en")) {
                    keyboardLang = Constants.LANGUAGE_ENGLISH_CODE;
                } else if (localLang.contains("ar")) {
                    keyboardLang = Constants.LANGUAGE_ARABIC_CODE;
                }
                CityModel model = modelListdata.get(position);
                Intent it = getIntent();
                it.putExtra(Constants.SELECTED_CITY_CODE, model.getCityId());
                it.putExtra(Constants.SELECTED_CITY, model.getCityName() + ", " + model.getCountryName());
                it.putExtra(Constants.SERACH_TYPE, model.getType());
                it.putExtra(Constants.SEARCH_LANG, keyboardLang);
                it.putExtra(Constants.SELECTED_HOTEL_ID, model.getHotelId());
                it.putExtra(Constants.SELECTED_HOTEL_NAME, model.getHotelName());
                setResult(RESULT_OK, it);
                DbHandler dbHandler = new DbHandler(HotelSearchPanel.this);
                dbHandler.addRecentHotel(model, keyboardLang);
                finish();
            }
        });
        currentLocationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recentSearchText.setVisibility(View.GONE);
                loadGPSValues();
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_filter_airport_destination);
        mTitleText.setTypeface(tf);
        backText.setTypeface(textFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(HotelSearchPanel.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    private void loadGPSValues() {
        if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(HotelSearchPanel.this, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(HotelSearchPanel.this, Manifest.permission.ACCESS_FINE_LOCATION)
                ) {
            requestPermission(HotelSearchPanel.this);
            return;
        }
        gps = new GPSTracker(HotelSearchPanel.this);
        if (gps.canGetLocation()) {
            LocationSearchData searchData = new LocationSearchData();
            showLoading();
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            if (latitude == 0.0 && longitude == 0.0) {
                if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(HotelSearchPanel.this, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                        PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(HotelSearchPanel.this, Manifest.permission.ACCESS_FINE_LOCATION)
                        ) {
                    requestPermission(HotelSearchPanel.this);
                    return;
                } else {
                    closeLoading();
                }
            } else {
                String json = makeJson(HotelSearchPanel.this, latitude, longitude);
                searchData.resultData(HotelSearchPanel.this, json, new AirlinesInterface<ArrayList<PredectiveSearchModel>>() {
                    @Override
                    public void onSuccess(ArrayList<PredectiveSearchModel> response) {
                        if (response.size() > 0) {
                            modelListdata.clear();
                            CityModel cityModel = new CityModel();
                            cityModel.setCityName(response.get(0).getCityName());
                            cityModel.setCityId(String.valueOf(response.get(0).getCityId()));
                            cityModel.setCountryName(response.get(0).getCountryName());
                            cityModel.setType("CITY");
                            modelListdata.add(cityModel);
                            airportAdapter.notifyDataSetChanged();
                            closeLoading();
                        }
                    }

                    @Override
                    public void onFailed(String message) {
                        Utils.printMessage(TAG, message);
                        closeLoading();
                        Toast.makeText(HotelSearchPanel.this, getString(R.string.label_error_hotels), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            gps.showSettingsAlert();
        }
    }

    private String makeJson(Context contex, double lat, double lng) {
        final SharedPreferences preferences = contex.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String language = preferences.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
            mainJSON.put("lang", language);
            mainJSON.put("lat", lat);
            mainJSON.put("lng", lng);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void showLoading() {
        if (barProgressDialog == null) {
            barProgressDialog = new ProgressDialog(HotelSearchPanel.this);
            barProgressDialog.setMessage(getResources().getString(R.string.label_wait_please_message));
            barProgressDialog.setProgressStyle(barProgressDialog.STYLE_SPINNER);
            barProgressDialog.setCancelable(false);
            barProgressDialog.show();
        }
    }

    private void closeLoading() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if (barProgressDialog != null) {
                    barProgressDialog.dismiss();
                    barProgressDialog = null;
                }
            }
        }, 500);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (barProgressDialog != null) {
            barProgressDialog.dismiss();
            barProgressDialog = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Utils.printMessage(TAG, "onRequestPermissionsResult::" + grantResults.length);
        switch (requestCode) {
            case 2: {
                if (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    loadGPSValues();
                    Utils.printMessage(TAG, "permission granted");
                } else {
                    Utils.printMessage(TAG, "permission granted failure");
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                return;
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "DATA ::  " + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
            View keyboardView = this.getCurrentFocus();
            if (keyboardView != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(keyboardView.getWindowToken(), 0);
            }
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(lightFace);
            errorDescriptionText.setTypeface(lightFace);
            searchButton.setTypeface(lightFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    loadingViewLayout.removeView(errorView);
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        } else {
            if (serviceType == GET_LOCATION) {
                try {
                    JSONObject jsonObj = new JSONObject(data);
                    String Status = jsonObj.getString("status");
                    if (Status.equalsIgnoreCase("OK")) {
                        JSONArray Results = jsonObj.getJSONArray("results");
                        JSONObject zero = Results.getJSONObject(0);
                        JSONArray address_components = zero.getJSONArray("address_components");
                        for (int i = 0; i < address_components.length(); i++) {
                            JSONObject zero2 = address_components.getJSONObject(i);
                            String long_name = zero2.getString("long_name");
                            JSONArray mtypes = zero2.getJSONArray("types");
                            String Type = mtypes.getString(0);
                            if (TextUtils.isEmpty(long_name) == false || !long_name.equals(null) || long_name.length() > 0 || long_name != "") {
                                if (Type.equalsIgnoreCase("locality")) {
                                    Utils.printMessage(TAG, "city name : " + long_name);
                                    handleCurrentLocation(long_name);
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (serviceType == GET_CITY) {
                closeLoading();
                listdata.clear();
                modelListdata.clear();
                try {
                    JSONObject object = new JSONObject(data);
                    if (object.has("cities")) {
                        JSONArray cityObject = object.optJSONArray("cities");
                        for (int i = 0; i < cityObject.length(); i++) {
                            CityModel model = new CityModel();
                            JSONObject payloadObject = cityObject.optJSONObject(i);
                            JSONObject payload = payloadObject.optJSONObject("payload");
                            String city_name = payload.optString("cityName");
                            String city_id = payload.optString("cityId");
                            String country = payload.optString("countryName");
                            listdata.add(city_name + ", " + country);
                            model.setCityId(city_id);
                            model.setCityName(city_name);
                            model.setCountryName(country);
                            model.setHotelId("");
                            model.setHotelName(city_name + ", " + country);
                            model.setType("CITY");
                            modelListdata.add(model);
                        }
                    }
                    if (object.has("hotels")) {
                        JSONArray hotelObject = object.optJSONArray("hotels");
                        for (int i = 0; i < hotelObject.length(); i++) {
                            CityModel model = new CityModel();
                            JSONObject payloadObject = hotelObject.optJSONObject(i);
                            JSONObject payload = payloadObject.optJSONObject("payload");
                            String hotel_name = payload.optString("cityName");
                            String city_name = payload.optString("hotelName");
                            String city_id = payload.optString("cityId");
                            String country = payload.optString("countryName");
                            String hotel_id = payload.optString("uniqueId");
                            listdata.add(city_name + ", " + country);
                            model.setCityId(city_id);
                            model.setCityName(city_name);
                            model.setCountryName(hotel_name);
                            model.setHotelId(hotel_id);
                            model.setHotelName(hotel_name + ", " + country);
                            model.setType("HOTEL");
                            modelListdata.add(model);
                        }
                    }
                    if (object.has("airports")) {
                        JSONArray airportObject = object.optJSONArray("airports");
                        for (int i = 0; i < airportObject.length(); i++) {
                            CityModel model = new CityModel();
                            JSONObject payloadObject = airportObject.optJSONObject(i);
                            String aName = payloadObject.getString("text");
                            JSONObject payload = payloadObject.optJSONObject("payload");
                            String airportCode = payload.optString("airportCode");
                            String cityId = payload.optString("cityId");
                            String airportName = payload.optString("airportName");
                            String cityName = payload.optString("cityName");
                            listdata.add(aName);
                            model.setCityId(cityId);
                            model.setCityName(airportName);
                            model.setCountryName(cityName);
                            model.setHotelId("");
                            model.setHotelName(aName);
                            model.setType("AIRPORT");
                            modelListdata.add(model);
                        }
                    }
                    Utils.printMessage("list size", "" + modelListdata.size());
                    airportAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message_hotels));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private void handleCurrentLocation(final String long_name) {
        boolean isInternetPresent = Utils.isConnectingToInternet(HotelSearchPanel.this);
        if (isInternetPresent) {
            View keyboardView = this.getCurrentFocus();
            if (keyboardView != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(keyboardView.getWindowToken(), 0);
            }
            try {
                new HTTPAsync(HotelSearchPanel.this, HotelSearchPanel.this,
                        Constants.HOTEL_PREDICTIVE_SEARCH, "" + URLEncoder.encode("" + long_name, "UTF-8"), GET_CITY, 1)
                        .execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            View keyboardView = this.getCurrentFocus();
            if (keyboardView != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(keyboardView.getWindowToken(), 0);
            }
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(lightFace);
            errorDescriptionText.setTypeface(lightFace);
            searchButton.setTypeface(lightFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadingViewLayout.removeView(errorView);
                    try {
                        new HTTPAsync(HotelSearchPanel.this, HotelSearchPanel.this,
                                Constants.HOTEL_PREDICTIVE_SEARCH, "" + URLEncoder.encode("" + long_name, "UTF-8"), GET_CITY, 1)
                                .execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
