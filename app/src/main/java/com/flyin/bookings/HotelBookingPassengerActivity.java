package com.flyin.bookings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.adapter.PassengerInfoAdapter;
import com.flyin.bookings.dialog.SignInDialog;
import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.model.TravellerDetailsBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.PaymentKeyGeneration;
import com.flyin.bookings.util.Utils;
import com.google.gson.Gson;
import com.webengage.sdk.android.User;
import com.webengage.sdk.android.WebEngage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class HotelBookingPassengerActivity extends AppCompatActivity implements AsyncTaskListener {
    private Typeface titleFace;
    private HotelModel hotelModel = null;
    private String hotelString = "";
    private SharedPreferences preferences = null;
    private boolean isSingInClicked = true;
    private boolean isUserSignIn = false;
    private SignInDialog signInDialog;
    private ArrayList<TravellerDetailsBean> travellersList = null;
    private boolean isInternetPresent = false;
    private static final int GET_FETCH_RESULT = 1;
    private static final int PREBOOKING_REQ = 2;
    private static final int BOOK_FLIGHT = 3;
    private static final int SECURE_CRYPT_REQUEST = 555;
    private static final int GET_ACCESS_TOKEN = 10;
    private static final int UPDATE_PASSENGER_INFO = 9;
    private LayoutInflater inflater;
    private View loadingView;
    private ImageView errorImage = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private Button searchButton = null;
    private RelativeLayout loadingViewLayout = null;
    private static final String TAG = "HotelBookingPassengerActivity";
    private String requestJsonData = "";
    private String bookingRefNo = "";
    private String tokenId;
    private JSONObject hotelJsonAfterPreBook;
    private JSONObject bookingDataJsonBeforePreBook;
    private JSONObject bookingInfoJsonBeforePreBook;
    private JSONObject cryptReqJson;
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private String accessTocken = "";
    private TextView personName = null;
    private PassengerInfoAdapter passengerInfoAdapter = null;
    private int adultCount = 0;
    private int childCount = 0;
    private String totalPrice = "";
    private LinearLayout signInLayout = null;
    private ProgressDialog barProgressDialog = null;
    private ArrayList<TravellerDetailsBean> accountTravellerArrayList = null;
    private String lastFreeCancelDate = "";
    private String upi;
    private String randomNumber = "";
    private String hotelNameInEng = "";
    private String userEarnedPoints = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_passenger);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            randomNumber = bundle.getString(Constants.BOOKING_RANDOM_NUMBER, "");
            boolean isForHotel = bundle.getBoolean("isForHotel", false);
            if (isForHotel) {
                childCount = bundle.getInt("child_count");
                adultCount = bundle.getInt("adult_count");
                totalPrice = bundle.getString("pay_price");
                hotelNameInEng = bundle.getString("eng_hotel_name");
                userEarnedPoints = bundle.getString(Constants.USER_EARNED_POINTS, "");
            }
        }
        preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        isInternetPresent = Utils.isConnectingToInternet(HotelBookingPassengerActivity.this);
        travellersList = new ArrayList<>();
        accountTravellerArrayList = new ArrayList<>();
        personName = (TextView) findViewById(R.id.passenger_header);
        TextView hotelName = (TextView) findViewById(R.id.selected_hotel_name);
        TextView imgBack = (TextView) findViewById(R.id.img_back);
        TextView nightCountText = (TextView) findViewById(R.id.total_night_text);
        TextView nightCountValue = (TextView) findViewById(R.id.total_night_count);
        TextView roomCountText = (TextView) findViewById(R.id.total_room_text);
        TextView roomCountValue = (TextView) findViewById(R.id.total_room_count);
        TextView totalPriceText = (TextView) findViewById(R.id.total_price_text);
        TextView totalPriceValue = (TextView) findViewById(R.id.total_pay_price);
        TextView orLabelText = (TextView) findViewById(R.id.enter_details_text);
        TextView termsConditionText = (TextView) findViewById(R.id.terms_conditions_text);
        ImageView backImage = (ImageView) findViewById(R.id.back_home);
        ImageView middleHeader = (ImageView) findViewById(R.id.header_middle);
        final ImageView hotelFullImage = (ImageView) findViewById(R.id.hotel_full_image);
        LinearLayout backLayout = (LinearLayout) findViewById(R.id.back_layout);
        signInLayout = (LinearLayout) findViewById(R.id.sign_in_layout);
        Button signInButton = (Button) findViewById(R.id.sign_in_button);
        Button continueButton = (Button) findViewById(R.id.continue_button);
        RatingBar hotelRating = (RatingBar) findViewById(R.id.ratingBar);
        ListView passengerListView = (ListView) findViewById(R.id.passengers_list);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        handleUserFetchResults();
        if (Utils.isArabicLangSelected(HotelBookingPassengerActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            middleHeader.setImageResource(R.drawable.arabic_two);
            imgBack.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            hotelName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            nightCountText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            nightCountValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            roomCountText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            roomCountValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            totalPriceText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            totalPriceValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            signInButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            orLabelText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            termsConditionText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            continueButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            imgBack.setLayoutParams(params);
            personName.setGravity(Gravity.START);
            totalPriceValue.setGravity(Gravity.END);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                personName.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                totalPriceValue.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            }
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        titleFace = Typeface.createFromAsset(getAssets(), fontTitle);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontText);
        personName.setTypeface(titleFace);
        imgBack.setTypeface(titleFace);
        hotelName.setTypeface(tf);
        nightCountText.setTypeface(titleFace);
        nightCountValue.setTypeface(titleFace);
        roomCountText.setTypeface(titleFace);
        roomCountValue.setTypeface(titleFace);
        totalPriceText.setTypeface(tf);
        totalPriceValue.setTypeface(tf);
        signInButton.setTypeface(tf);
        orLabelText.setTypeface(titleFace);
        termsConditionText.setTypeface(textFace);
        continueButton.setTypeface(tf);
        errorMessageText.setTypeface(titleFace);
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String userMail = pref.getString(Constants.MEMBER_EMAIL, "");
        if (!userMail.equalsIgnoreCase("")) {
            upi = userMail;
        } else {
            upi = "null";
        }
        try {
            String hotelModelString = Singleton.getInstance().model;
            Gson gson = new Gson();
            hotelModel = gson.fromJson(hotelModelString, HotelModel.class);
            hotelString = Singleton.getInstance().hotelString;
            Singleton.getInstance().hotelString = "";
        } catch (Exception e) {
        }
        personName.setText(getString(R.string.label_guest_details_title));
        if (hotelModel.getImage() != null && hotelModel.getImage().length() > 0) {
            ViewTreeObserver vto = hotelFullImage.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    hotelFullImage.getViewTreeObserver().removeOnPreDrawListener(this);
                    int finalHeight = hotelFullImage.getMeasuredHeight();
                    int finalWidth = hotelFullImage.getMeasuredWidth();
                    String url = Constants.IMAGE_SCALING_URL + finalWidth + "x" + finalHeight + Constants.IMAGE_QUALITY + "" + hotelModel.getImage();
                    Glide.with(HotelBookingPassengerActivity.this).load(url).placeholder(R.drawable.imagethumb)
                            .into(hotelFullImage);
                    return true;
                }
            });
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.hide();
        String[] titleArray = getResources().getStringArray(R.array.title_selection);
        for (int i = 0; i < Integer.parseInt(hotelModel.getRoom_count()); i++) {
            TravellerDetailsBean travellerDetails = new TravellerDetailsBean();
            travellerDetails.setTravellerHeaderName(getString(R.string.label_room_count) + " " + (i + 1));
            travellerDetails.setTravellerHeader(getString(R.string.SAdultLbl) + " " + (i + 1));
            travellerDetails.setTravellerEmail(getString(R.string.label_enter_your_details));
            travellerDetails.setIsExpanded(false);
            travellerDetails.setPassengerType("ADT");
            travellerDetails.setTitle(titleArray[0]);
            travellersList.add(travellerDetails);
        }
        hotelName.setText(hotelModel.getHna());
        hotelRating.setRating(Float.parseFloat(hotelModel.getStar()));
        roomCountValue.setText(hotelModel.getRoom_count());
        nightCountValue.setText(hotelModel.getDur());
        String actualPriceResult = "";
        String currencyResult = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        if (Utils.isArabicLangSelected(HotelBookingPassengerActivity.this)) {
            if (currencyResult.equalsIgnoreCase("SAR")) {
                actualPriceResult = totalPrice + " " + getString(R.string.label_SAR_currency_name);
            } else {
                actualPriceResult = currencyResult + " " + totalPrice;
            }
        } else {
            actualPriceResult = currencyResult + " " + totalPrice;
        }
        totalPriceValue.setText(actualPriceResult);
        passengerInfoAdapter = new PassengerInfoAdapter(HotelBookingPassengerActivity.this, travellersList);
        passengerListView.setAdapter(passengerInfoAdapter);
        passengerListView.setFocusable(false);
        Utils.setListViewHeightBasedOnChildren(passengerListView);
        passengerInfoAdapter.notifyDataSetChanged();
        passengerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Singleton.getInstance().travellersInfoBean = travellersList.get(position);
                Singleton.getInstance().fetchTravellerArrayList = accountTravellerArrayList;
                Intent intent = new Intent(HotelBookingPassengerActivity.this, HotelBookingPassengerInfoActivity.class);
                intent.putExtra(Constants.IS_USER_LOGGED, isUserSignIn);
                intent.putExtra(Constants.SELECTED_USER_POSITION, String.valueOf(position));
                startActivityForResult(intent, UPDATE_PASSENGER_INFO);
            }
        });
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInDialog = new SignInDialog(HotelBookingPassengerActivity.this);
                if (!isSingInClicked) {
                    preferences.edit().putString(Constants.MEMBER_EMAIL, "").apply();
                    preferences.edit().putString(Constants.MEMBER_ACCESS_TOKEN, "").apply();
                    preferences.edit().putString(Constants.MEMBER_TOKEN_TYPE, "").apply();
                    preferences.edit().putString(Constants.USER_UNIQUE_ID, "").apply();
                    preferences.edit().putBoolean(Constants.isFanClub, false).apply();
                    preferences.edit().putBoolean(Constants.IS_REWARDS_ENABLED, false).apply();
                    signInDialog.callFacebookLogout();
                    User weUser = WebEngage.get().user();
                    weUser.logout();
                    isSingInClicked = true;
                    isUserSignIn = false;
                } else {
                    signInDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handleUserFetchResults();
                        }
                    });
                    signInDialog.show();
                }
            }
        });
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < travellersList.size(); i++) {
                    if (travellersList.get(i).getFirstName().isEmpty()) {
                        displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getResources().getString(R.string.label_err_fname_msg));
                        return;
                    }
                    if (!Utils.isValidName(Utils.getNameWithoutChar(travellersList.get(i).getFirstName()))) {
                        displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " +
                                getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_firstName));
                        return;
                    }
                    for (int j = 0; j < travellersList.size(); j++) {
                        if (i < j) {
                            if (travellersList.get(i).getFirstName().equalsIgnoreCase(travellersList.get(j).getFirstName())) {
                                displayErrorMessage(travellersList.get(j).getTravellerHeader() + " - " +
                                        getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_firstName));
                                return;
                            }
                        }
                    }
                    if (travellersList.get(i).getLastName().isEmpty()) {
                        displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getResources().getString(R.string.label_err_lname_msg));
                        return;
                    }
                    if (!Utils.isValidName(Utils.getNameWithoutChar(travellersList.get(i).getLastName()))) {
                        displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.ErrInvalidNameMsg)
                                + " " + getString(R.string.label_traveller_lastName));
                        return;
                    }
                    if (Utils.getNameWithoutChar(travellersList.get(i).getFirstName()).equalsIgnoreCase(Utils.getNameWithoutChar(travellersList.get(i).getLastName()))) {
                        displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " +
                                getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_lastName));
                        return;
                    }
                    travellersList.get(i).setTravellerHeaderName(travellersList.get(i).getFirstName() + " " + travellersList.get(i).getLastName());
                    if (i == 0) {
                        if (travellersList.get(i).getEmail().isEmpty()) {
                            displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_email_message));
                            return;
                        }
                        if (!Utils.isValidEmail(travellersList.get(i).getEmail())) {
                            displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_email_valid_message));
                            return;
                        }
                        if (travellersList.get(i).getPhoneNo().isEmpty() || !Utils.isValidMobile(travellersList.get(i).getPhoneNo())) {
                            displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_err_phone_msg));
                            return;
                        }
                        travellersList.get(i).setTravellerEmail(travellersList.get(i).getEmail());
                    } else {
                        travellersList.get(i).setTravellerEmail("");
                    }
                }

                if (isInternetPresent) {
                    showLoading();
                    requestJsonData = makeJson();
                    HTTPAsync async = new HTTPAsync(HotelBookingPassengerActivity.this, HotelBookingPassengerActivity.this,
                            Constants.HOTEL_PRE_BOOKING, "", requestJsonData, PREBOOKING_REQ, HTTPAsync.METHOD_POST);
                    async.execute();
//                    int maxLogSize = 1000;
//                    for(int i = 0; i <= requestJsonData.length() / maxLogSize; i++) {
//                        int start = i * maxLogSize;
//                        int end = (i+1) * maxLogSize;
//                        end = end > requestJsonData.length() ? requestJsonData.length() : end;
//                        Log.v(TAG, requestJsonData.substring(start, end));
//                    }
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(titleFace);
                    errorDescriptionText.setTypeface(titleFace);
                    searchButton.setTypeface(titleFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(HotelBookingPassengerActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                showLoading();
                                HTTPAsync async = new HTTPAsync(HotelBookingPassengerActivity.this, HotelBookingPassengerActivity.this, Constants.HOTEL_PRE_BOOKING, "", requestJsonData,
                                        PREBOOKING_REQ, HTTPAsync.METHOD_POST);
                                async.execute();
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        });
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        JSONObject obj = null;
        Utils.printMessage(TAG, "RESPONSE::" + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            someThingWrongError();
        } else {
            try {
                obj = new JSONObject(data);
                if (serviceType == PREBOOKING_REQ) {
                    closeLoading();
                    if (obj.has("BookingInfoRS")) {
                        String bookRSValue = obj.getString("BookingInfoRS");
                        if (bookRSValue.equalsIgnoreCase("null")) {
                            someThingWrongError();
                        } else if (obj.getJSONObject("BookingInfoRS").getString("st").equalsIgnoreCase("FAILURE") || obj.getJSONObject("BookingInfoRS").getString("st").equalsIgnoreCase("FAIL")) {
                            someThingWrongError();
                        } else if (obj.getJSONObject("BookingInfoRS").getString("st").equalsIgnoreCase("SUCCESS")) {
                            JSONObject jsonObject = obj.getJSONObject("BookingInfoRS");
                            JSONObject sourceJsonObject = jsonObject.getJSONObject("source");
                            bookingRefNo = sourceJsonObject.getString("referenceID");
                            Utils.printMessage(TAG, "booking ref no: " + bookingRefNo);
                            hotelJsonAfterPreBook = jsonObject.getJSONObject("hotel");
                            PaymentKeyGeneration paymentKeyGen = new PaymentKeyGeneration();
                            tokenId = paymentKeyGen.generateRandomString();
                            cryptReqJson = new JSONObject();
                            cryptReqJson.put("brn", bookingRefNo);
                            cryptReqJson.put("tid", tokenId);
                            cryptReqJson.put("usrc", Utils.getSelectedCurrencyCode(HotelBookingPassengerActivity.this));
                            Utils.printMessage(TAG, "payment amt :: " + hotelModel.getP());
                            cryptReqJson.put("usrba", hotelModel.getP());
                            cryptReqJson.put("rurl", "");
                            cryptReqJson.put("prdct", "H");
                            cryptReqJson.put("stus", "");
                            cryptReqJson.put("stusc", "");
                            cryptReqJson.put("lpax", travellersList.get(0).getFirstName() + "" + travellersList.get(0).getLastName());
                            cryptReqJson.put("lpaxemail", getLeadPaxEmail());
                            cryptReqJson.put("iqu", "");
                            cryptReqJson.put("dc", "");
                            if (!upi.equalsIgnoreCase("null")) {
                                cryptReqJson.put("upi", upi);
                            }
                            cryptReqJson.put("stype", "BFP");
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                showLoading();
                                HTTPAsync async = new HTTPAsync(HotelBookingPassengerActivity.this,
                                        HotelBookingPassengerActivity.this, Constants.SECURE_PAYMENT_URL, "",
                                        cryptReqJson.toString(),
                                        SECURE_CRYPT_REQUEST, HTTPAsync.METHOD_POST);
                                async.execute();
                            }
                        }
                    } else {
                        someThingWrongError();
                    }
                } else if (serviceType == SECURE_CRYPT_REQUEST) {
                    if (obj.has("status") && obj.getString("status").equalsIgnoreCase("SUCCESS")) {
                        String name = URLEncoder.encode(travellersList.get(0).getFirstName() + travellersList.get(0).getLastName(), "utf-8");
                        String paxName = travellersList.get(0).getFirstName() + " " + travellersList.get(0).getLastName();
                        String email = URLEncoder.encode(getLeadPaxEmail(), "utf-8");
                        String locale = "";
                        Utils.printMessage(TAG, "bfp payment amt :: " + hotelModel.getP());
                        String payment = hotelModel.getP();
                        if (Utils.isArabicLangSelected(HotelBookingPassengerActivity.this)) {
                            locale = "ar_SA";
                        } else {
                            locale = "en_GB";
                        }
                        String url = "";
                        if (upi.equalsIgnoreCase("null")) {
                            url = Constants.PAYMENT_URL + "tokenId=" + tokenId + "&amount=" +
                                    payment + "&currency=" + Utils.getSelectedCurrencyCode(HotelBookingPassengerActivity.this)
                                    + "&product=H&" + "flyinCode=" + bookingRefNo + "&leadPaxName=" + name + "&emailId=" + email +
                                    "&locale=" + locale + "&fsc=ma&pcc=" + obj.getString("accessToken");
                        } else {
                            String upis = URLEncoder.encode(upi, "utf-8");
                            SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                            String uniqueId = pref.getString(Constants.USER_UNIQUE_ID, "");
                            url = Constants.PAYMENT_URL + "tokenId=" + tokenId + "&amount=" +
                                    payment + "&currency=" + Utils.getSelectedCurrencyCode(HotelBookingPassengerActivity.this)
                                    + "&product=H&" + "flyinCode=" + bookingRefNo + "&leadPaxName=" + name + "&emailId=" + email +
                                    "&locale=" + locale + "&fsc=ma&pcc=" + obj.getString("accessToken") + "&upi=" + upis + "&uid=" + uniqueId;
                        }
                        String bnplUrl = Constants.PAYMENT_URL + "tokenId=" + tokenId + "&amount=" +
                                payment + "&currency=" + Utils.getSelectedCurrencyCode(HotelBookingPassengerActivity.this)
                                + "&product=H&" + "flyinCode=" + bookingRefNo + "&leadPaxName=" + name + "&emailId=" + email +
                                "&locale=" + locale + "&fsc=ma";
                        Utils.printMessage(TAG, "Url :: " + url);
                        Utils.printMessage(TAG, "Bnpl Url : " + bnplUrl);
                        Intent intent = new Intent(HotelBookingPassengerActivity.this, HotelBookingSummaryPaymentActivity.class);
                        intent.putExtra("PAYMENT_URL", url);
                        intent.putExtra("BNPL_PAYMENT_URL", bnplUrl);
                        intent.putExtra("TOKENID", tokenId);
                        intent.putExtra("CRYPT_JSON", "" + cryptReqJson);
                        intent.putExtra("HOTEL_OBJ", "" + hotelJsonAfterPreBook);
                        intent.putExtra("BOOKING_DATA_OBJ", "" + bookingDataJsonBeforePreBook);
                        intent.putExtra("BOOKING_INFO_OBJ", "" + bookingInfoJsonBeforePreBook);
                        intent.putExtra("BOOKING_ID", bookingRefNo);
                        intent.putExtra(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                        intent.putExtra(Constants.USER_FULL_NAME, paxName);
                        intent.putExtra(Constants.USER_EMAIL, getLeadPaxEmail());
                        intent.putExtra("eng_hotel_name", hotelNameInEng);
                        startActivityForResult(intent, BOOK_FLIGHT);
                        closeLoading();
                    } else {
                        closeLoading();
                        someThingWrongError();
                    }
                } else if (serviceType == GET_FETCH_RESULT) {
                    closeFetchResultLoading();
                    Singleton.getInstance().fetchTravellerArrayList.clear();
                    try {
                        obj = new JSONObject(data);
                        if (obj.has("customerDetails")) {
                            JSONArray customerDetailsArray = obj.getJSONArray("customerDetails");
                            for (int i = 0; i < customerDetailsArray.length(); i++) {
                                TravellerDetailsBean travellerBean = new TravellerDetailsBean();
                                if (customerDetailsArray.getJSONObject(i).has("basicDetail")) {
                                    JSONObject basicDetailsObj = customerDetailsArray.getJSONObject(i).getJSONObject("basicDetail");
                                    travellerBean.setTitle(basicDetailsObj.getString("title"));
                                    travellerBean.setFirstName(basicDetailsObj.getString("firstName"));
                                    travellerBean.setLastName(basicDetailsObj.getString("lastName"));
                                    travellerBean.setPhoneNo(basicDetailsObj.getString("mobileNumber"));
                                    travellerBean.setMobileAreaCode(basicDetailsObj.getString("mobileAreaCode"));
                                    travellerBean.setAddress(basicDetailsObj.getString("address"));
                                    travellerBean.setEmail(basicDetailsObj.getString("email"));
                                    travellerBean.setProfileId(basicDetailsObj.getString("profileID"));
                                }
                                if (customerDetailsArray.getJSONObject(i).has("passportDetails") && !customerDetailsArray.getJSONObject(i).getJSONArray("passportDetails").isNull(0)) {
                                    JSONArray passportDetailsArray = customerDetailsArray.getJSONObject(i).getJSONArray("passportDetails");
                                    for (int j = 0; j < passportDetailsArray.length(); j++) {
                                        JSONObject passportDetailsObj = passportDetailsArray.getJSONObject(j);
                                        if (passportDetailsObj.has("docType")) {
                                            String documentType = passportDetailsObj.getString("docType");
                                            if (documentType.equalsIgnoreCase("2")) {
                                                travellerBean.setPassportIssueDate(Utils.checkStringVal(passportDetailsObj.getString("issuedate")));
                                                travellerBean.setPassportNumber(Utils.checkStringVal(passportDetailsObj.getString("passportNo")));
                                                travellerBean.setPassportExpiry(Utils.checkStringVal(passportDetailsObj.getString("expiryDate")));
                                                travellerBean.setDateOfBirth(Utils.checkStringVal(passportDetailsObj.getString("dateofBirth")));
                                                travellerBean.setNationality(Utils.checkStringVal(passportDetailsObj.getString("nationality")));
                                                travellerBean.setPassportIssuingCountry(Utils.checkStringVal(passportDetailsObj.getString("countryIssued")));
                                                travellerBean.setDocumentType(passportDetailsObj.getString("docType"));
                                            }
                                        }
                                    }
                                }
                                if (Utils.checkStringVal(travellerBean.getFirstName()).isEmpty() && Utils.checkStringVal(travellerBean.getLastName()).isEmpty()) {
                                    continue;
                                }
                                accountTravellerArrayList.add(travellerBean);
                            }
                        }
                    } catch (Exception e) {
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.order_being_processed_message);
        loadingText.setTypeface(titleFace);
        descriptionText.setVisibility(View.GONE);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    public String makeJson() {
        final SharedPreferences preferences = HotelBookingPassengerActivity.this
                .getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
        if (tokenTime == -1) {
            getTokenFromServer();
        } else {
            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
            if (diff > Long.parseLong(expireIn)) {
                getTokenFromServer();
            } else {
                accessTocken = Utils.getRequestAccessToken(HotelBookingPassengerActivity.this);
            }
        }
        JSONObject obj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        try {
            SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            String userSelectedCurr = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
            String groupType = null;
            if (pref.getBoolean(Constants.isFanClub, false)) {
                groupType = "ALHILAL";
            }
            String userSelectedLang = "EN";
            if (Utils.isArabicLangSelected(HotelBookingPassengerActivity.this)) {
                userSelectedLang = "AR";
            } else {
                userSelectedLang = "EN";
            }
            JSONObject preBooking = new JSONObject();
            JSONObject source = new JSONObject();
            source = Utils.getHotelSearchAppSettingData(source, HotelBookingPassengerActivity.this);
            source.put("clientId", Constants.CLIENT_ID);
            source.put("clientSecret", Constants.CLIENT_SECRET);
            source.put("paxNationality", Utils.getUserCountry(HotelBookingPassengerActivity.this));
            source.put("echoToken", randomNumber);
            source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
            source.put("accessToken", Utils.getRequestAccessToken(HotelBookingPassengerActivity.this));
            source.put("timeStamp", timeStamp);
            source.put("referenceID", null);
            source.put("currency", userSelectedCurr);
            source.put("groupType", groupType);
            source.put("language", userSelectedLang);
            JSONObject sourceRQ = new JSONObject();
            sourceRQ.put("source", source);
            Utils.printMessage(TAG, "hotelString" + hotelString);
            JSONObject hotel = new JSONObject(hotelString);
            hotel = setLeadPaxDetails(hotel);
            JSONObject rcObj = new JSONObject();
            if (hotel.getJSONArray("room").getJSONObject(0).has("ci")) {
                JSONObject ciObject = hotel.getJSONArray("room").getJSONObject(0).getJSONObject("ci");
                if (ciObject.has("lfc")) {
                    JSONObject lfcObject = ciObject.getJSONObject("lfc");
                    if (lfcObject.has("d") && lfcObject.has("hou") && lfcObject.has("min")) {
                        lastFreeCancelDate = lfcObject.optString("d") + "T" + lfcObject.optString("hou") + ":" + lfcObject.optString("min");
                    }
                }
            }
            rcObj.put("rc", hotel);
            sourceRQ.put("hotel", rcObj);
            preBooking.put("BookingInfoRQ", sourceRQ);
            JSONObject bookingData = new JSONObject();
            bookingData.put("selectedCurrency", Utils.getSelectedCurrencyCode(this));
            bookingData.put("hotelName", hotelNameInEng);
            bookingData.put("starRating", hotelModel.getStar() + " star");
            bookingData.put("cityId", hotelModel.getCityId());
            bookingData.put("hPhoneCode", hotelModel.getHphoneCode());
            bookingData.put("hPhoneNo", hotelModel.getHphoneNo());
            bookingData.put("hFaxCode", hotelModel.getHfaxCode());
            bookingData.put("hFaxNo", hotelModel.getHfaxNo());
            bookingData.put("hmailId", hotelModel.getHemail());
            bookingData.put("hAddress", hotelModel.getFullAddress());
            bookingData.put("loginId", JSONObject.NULL);
            bookingData.put("lpmailid", getLeadPaxEmail());
            bookingData.put("totalPrice", hotelModel.getP());
            bookingData.put("loginId", Utils.getUserEmail(this));
            bookingData.put("srvrcncldate", Utils.checkStringValue("null"));
            bookingData.put("lastFreeCnclDate", lastFreeCancelDate);
            bookingData.put("checkInPolicy", hotelModel.getCheck_in());
            bookingData.put("checkOutPolicy", hotelModel.getCheck_out());
            bookingData.put("affName", Utils.checkStringValue("null"));
            bookingData.put("affUnqId", Utils.checkStringValue("null"));
            bookingData.put("domainType", Utils.checkStringValue("null"));
            bookingData = Utils.getHotelAppSettingData(bookingData, HotelBookingPassengerActivity.this);
            bookingData.put("dmCurrency", "SAR");
            JSONObject loyaltyObj = new JSONObject();
            if (!pref.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
                if (pref.getBoolean(Constants.IS_REWARDS_ENABLED, false)) {
                    if (!userEarnedPoints.isEmpty()) {
                        loyaltyObj.put("earnpoint", Integer.parseInt(userEarnedPoints));
                    } else {
                        loyaltyObj.put("earnpoint", Utils.checkStringValue("null"));
                    }
                } else {
                    loyaltyObj.put("earnpoint", Utils.checkStringValue("null"));
                }
            } else {
                loyaltyObj.put("earnpoint", Utils.checkStringValue("null"));
            }
            loyaltyObj.put("redeempoint", Utils.checkStringValue("null"));
            loyaltyObj.put("rdmdiscountusr", Utils.checkStringValue("null"));
            loyaltyObj.put("status", "PENDING");
            bookingData.put("loyaltydetails", loyaltyObj);
            preBooking.put("BookingData", bookingData);
            bookingDataJsonBeforePreBook = bookingData;
            bookingInfoJsonBeforePreBook = sourceRQ;
            obj.put("PreBooking", preBooking);
            Utils.printMessage(TAG, "finalhotelString" + obj.toString());
            return obj.toString();
        } catch (Exception e) {
            e.printStackTrace();
            Utils.printMessage(TAG, "Exception2" + e.getMessage());
        }
        return null;
    }

    private JSONObject setLeadPaxDetails(JSONObject hotel) {
        try {
            if (hotel.getJSONArray("room").length() != 0) {
                for (int i = 0; i < hotel.getJSONArray("room").length(); i++) {
                    hotel.getJSONArray("room").getJSONObject(i).put("lp", setLP(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hotel;
    }


    public JSONObject setLP(int i) {
        JSONObject object = new JSONObject();
        try {
            object.put("fname", travellersList.get(i).getFirstName());
            object.put("lname", travellersList.get(i).getLastName());
            object.put("age", "99");
            if (i == 0) {
                String mobileNumber = travellersList.get(0).getPhoneNo();
                String mobilePcc = travellersList.get(0).getPhoneCode();
                String email = travellersList.get(0).getEmail();
                object.put("email", email);
                object.put("ph", (mobilePcc + "-" + mobileNumber));
            }
            return object;
        } catch (JSONException e) {
            Utils.printMessage(TAG, "Exception4" + e.getMessage());
        }
        return null;
    }

    private String getLeadPaxEmail() {
        if (travellersList != null && travellersList.size() != 0) {
            return travellersList.get(0).getEmail();
        }
        return null;
    }

    private void getTokenFromServer() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(HotelBookingPassengerActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(HotelBookingPassengerActivity.this, HotelBookingPassengerActivity.this,
                Constants.CLIENT_AUTHENTICATION, "", obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == UPDATE_PASSENGER_INFO) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    passengerInfoAdapter.notifyDataSetChanged();
                }
            }
        } else if (requestCode == BOOK_FLIGHT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    loadingViewLayout.removeView(loadingView);
                    loadingViewLayout.removeAllViews();
                }
            }
        } else {
            signInDialog.passFacebookResult(requestCode, resultCode, data);
        }
    }

    private void handleUserFetchResults() {
        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String memberAccessToken = preferences.getString(Constants.MEMBER_ACCESS_TOKEN, "");
        if (!memberAccessToken.equalsIgnoreCase("")) {
            isUserSignIn = true;
            isSingInClicked = false;
            signInLayout.setVisibility(View.GONE);
            final String requestUserDetailsJSON = travellerDetailsTask();
            if (isInternetPresent) {
                showFetchResultLoading();
                HTTPAsync async = new HTTPAsync(HotelBookingPassengerActivity.this, HotelBookingPassengerActivity.this, Constants.FETCH_TRAVELLER, "", requestUserDetailsJSON,
                        GET_FETCH_RESULT, HTTPAsync.METHOD_POST);
                async.execute();
            } else {
                inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = inflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(titleFace);
                errorDescriptionText.setTypeface(titleFace);
                searchButton.setTypeface(titleFace);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isInternetPresent = Utils.isConnectingToInternet(HotelBookingPassengerActivity.this);
                        if (isInternetPresent) {
                            loadingViewLayout.removeView(errorView);
                            showFetchResultLoading();
                            HTTPAsync async = new HTTPAsync(HotelBookingPassengerActivity.this, HotelBookingPassengerActivity.this, Constants.FETCH_TRAVELLER, "", requestUserDetailsJSON,
                                    GET_FETCH_RESULT, HTTPAsync.METHOD_POST);
                            async.execute();
                        }
                    }
                });
                loadErrorType(Constants.NETWORK_ERROR);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            }
        } else if (memberAccessToken.equalsIgnoreCase("")) {
            isUserSignIn = false;
            isSingInClicked = true;
            personName.setText(getString(R.string.label_guest_details_title));
            signInLayout.setVisibility(View.VISIBLE);
        }
    }

    private String travellerDetailsTask() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(HotelBookingPassengerActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(HotelBookingPassengerActivity.this));
            mainJSON.put("source", sourceJSON);
            mainJSON.put("userUniqueId", pref.getString(Constants.USER_UNIQUE_ID, ""));
            personName.setText(getString(R.string.label_welcome_title) + ", " + pref.getString(Constants.MEMBER_EMAIL, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void showFetchResultLoading() {
        if (barProgressDialog == null) {
            barProgressDialog = new ProgressDialog(HotelBookingPassengerActivity.this);
            barProgressDialog.setMessage(getString(R.string.label_wait_please_message));
            barProgressDialog.setProgressStyle(barProgressDialog.STYLE_SPINNER);
            barProgressDialog.setCancelable(false);
            barProgressDialog.show();
        }
    }

    private void closeFetchResultLoading() {
        if (barProgressDialog != null) {
            barProgressDialog.dismiss();
            barProgressDialog = null;
        }
    }

    private void someThingWrongError() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View errorView = inflater.inflate(R.layout.view_error_response, null);
        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
        errorText = (TextView) errorView.findViewById(R.id.error_text);
        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
        searchButton = (Button) errorView.findViewById(R.id.search_button);
        errorText.setTypeface(titleFace);
        errorDescriptionText.setTypeface(titleFace);
        searchButton.setTypeface(titleFace);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetPresent = Utils.isConnectingToInternet(HotelBookingPassengerActivity.this);
                if (isInternetPresent) {
                    loadingViewLayout.removeView(errorView);
                    Singleton.getInstance().roomPassengerInfoList.clear();
                    Intent intent = new Intent(HotelBookingPassengerActivity.this, MainSearchActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        });
        loadErrorType(Constants.WRONG_ERROR_PAGE);
        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(errorView);
    }
}
