package com.flyin.bookings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.model.DummyRoomSelectedBean;
import com.flyin.bookings.model.FhRoomPassengerDetailsBean;
import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.model.RoomModel;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("deprecation")
public class RoomSelectedFragment extends Fragment implements AsyncTaskListener {
    private HotelModel hotelModel = null;
    private LinearLayout topLayout;
    private HashMap<String, ArrayList<RoomModel>> rcModelArrayList = null;
    private int _id = 1;
    private int adults;
    private int childs;
    private static final String TAG = "RoomSelectedFragment";
    private ArrayList<RoomModel> tempArrayList = null;
    private ArrayList<RoomModel> roomArrayList = null;
    private ArrayList<String> adultsCountList;
    private ArrayList<String> childCountList;
    private HashMap<String, ArrayList<String>> childCount;
    private String headerDate = "";
    private Typeface regularBold, textFace;
    private boolean isArabicLang = false;
    private ArrayList<FhRoomPassengerDetailsBean> roomPassengerArrayList = null;
    private String currentDate = "";
    private String randomNumber = "";
    private Activity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_room, container, false);
        topLayout = (LinearLayout) view.findViewById(R.id.top_layout);
        _id = 1;
        if (getArguments() != null) {
            hotelModel = getArguments().getParcelable("model");
            childs = getArguments().getInt("child_count");
            adults = getArguments().getInt("adult_count");
            adultsCountList = getArguments().getStringArrayList("adults_list");
            childCountList = getArguments().getStringArrayList("childs_list");
            childCount = (HashMap<String, ArrayList<String>>) getArguments().getSerializable("children_list");
            headerDate = getArguments().getString("header_date");
            randomNumber = getArguments().getString(Constants.BOOKING_RANDOM_NUMBER, "");
        }
        roomPassengerArrayList = new ArrayList<>();
        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        boolean isRoomsInfoUpdated = preferences.getBoolean(Constants.IS_ROOM_INFO_UPDATED, false);
        if (!isRoomsInfoUpdated) {
            for (int i = 0; i < Singleton.getInstance().roomPassengerInfoList.size(); i++) {
                FhRoomPassengerDetailsBean bean1 = Singleton.getInstance().roomPassengerInfoList.get(i);
                FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
                bean.setAdultCnt(bean1.getAdultCnt());
                bean.setChildAge(bean1.getChildAge());
                bean.setAge(bean1.getAge());
                bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
                bean.setChildCnt(bean1.getChildCnt());
                bean.setClassType(bean1.getClassType());
                bean.setInfantCnt(bean1.getInfantCnt());
                roomPassengerArrayList.add(bean);
            }
        } else {
            for (int i = 0; i < Singleton.getInstance().updatedRoomPassengerInfoList.size(); i++) {
                FhRoomPassengerDetailsBean bean1 = Singleton.getInstance().updatedRoomPassengerInfoList.get(i);
                FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
                bean.setAdultCnt(bean1.getAdultCnt());
                bean.setChildAge(bean1.getChildAge());
                bean.setAge(bean1.getAge());
                bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
                bean.setChildCnt(bean1.getChildCnt());
                bean.setClassType(bean1.getClassType());
                bean.setInfantCnt(bean1.getInfantCnt());
                roomPassengerArrayList.add(bean);
            }
        }
        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        rcModelArrayList = new HashMap<>();
        rcModelArrayList.clear();
        String fontRegularBold = Constants.FONT_ROBOTO_BOLD;
        String regularText = Constants.FONT_ROBOTO_REGULAR;
        isArabicLang = false;
        if (Utils.isArabicLangSelected(mActivity)) {
            isArabicLang = true;
            fontRegularBold = Constants.FONT_DROIDKUFI_BOLD;
            regularText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        regularBold = Typeface.createFromAsset(mActivity.getAssets(), fontRegularBold);
        textFace = Typeface.createFromAsset(mActivity.getAssets(), regularText);
        rcModelArrayList.putAll(hotelModel.getRcModelArrayList());
        ArrayList<String> keylist = new ArrayList<>();
        keylist.addAll(rcModelArrayList.keySet());
        Map<String, RoomModel> tempRcModelArr = new HashMap<String, RoomModel>();
        for (int j = 0; j < keylist.size(); j++) {
            ArrayList<RoomModel> rcModelArr = rcModelArrayList.get(keylist.get(j));
            Double roomPrice = rcModelArr.get(0).getHotelRoomSortPrice();
            tempRcModelArr.put(keylist.get(j), rcModelArr.get(0));
        }
        List<RoomModel> roomByPrice = new ArrayList<RoomModel>(tempRcModelArr.values());
        Collections.sort(roomByPrice, new Comparator<RoomModel>() {
            public int compare(RoomModel e1, RoomModel e2) {
                Double price1 = e1.getHotelRoomSortPrice();
                Double price2 = e2.getHotelRoomSortPrice();
                return price1.compareTo(price2);
            }
        });
        ArrayList<DummyRoomSelectedBean> roomSelectedBeanArrayList = new ArrayList<>();
        for (int i = 0; i < roomByPrice.size(); i++) {
            for (String roomName : keylist) {
                if (roomByPrice.get(i).getRoom_type().equalsIgnoreCase(roomName)) {
                    ArrayList<RoomModel> rcmodel = rcModelArrayList.get(roomName);
                    Set<RoomModel> set = new HashSet<>(rcmodel);
                    rcmodel.clear();
                    rcmodel.addAll(set);
                    DummyRoomSelectedBean roomSelectedBean = new DummyRoomSelectedBean();
                    roomSelectedBean.setPrice(rcmodel.get(0).getHotelRoomSortPrice());
                    roomSelectedBean.setName(roomName);
                    roomSelectedBean.setRcmodel(rcmodel);
                    roomSelectedBeanArrayList.add(roomSelectedBean);
                }
            }
        }
        for (int k = 0; k < roomSelectedBeanArrayList.size(); k++) {
            for (int i = 0; i < roomSelectedBeanArrayList.get(k).getRcmodel().size(); i++) {
                RoomModel roomModel = roomSelectedBeanArrayList.get(k).getRcmodel().get(i);
                double tempArrayPrice = 0.0;
                if (!roomModel.getIsRcObject().equalsIgnoreCase("rc")) {
                    tempArrayPrice = Integer.parseInt(hotelModel.getRoom_count()) * Double.parseDouble(roomModel.getPrice());
                } else {
                    tempArrayPrice = Double.parseDouble(roomModel.getPrice());
                }
                roomSelectedBeanArrayList.get(k).getRcmodel().get(i).setTempPrice(tempArrayPrice);
            }
        }
        for (int k = 0; k < roomSelectedBeanArrayList.size(); k++) {
            Collections.sort(roomSelectedBeanArrayList.get(k).getRcmodel(), new Comparator<RoomModel>() {
                public int compare(RoomModel e1, RoomModel e2) {
                    Double price1 = e1.getTempPrice();
                    Double price2 = e2.getTempPrice();
                    return price1.compareTo(price2);
                }
            });
        }
        Collections.sort(roomSelectedBeanArrayList, new Comparator<DummyRoomSelectedBean>() {
            public int compare(DummyRoomSelectedBean e1, DummyRoomSelectedBean e2) {
                Double price1 = e1.getRcmodel().get(0).getTempPrice();
                Double price2 = e2.getRcmodel().get(0).getTempPrice();
                return price1.compareTo(price2);
            }
        });
        ArrayList<String> roomNameList = new ArrayList<>();
        for (int i = 0; i < roomSelectedBeanArrayList.size(); i++) {
            DummyRoomSelectedBean dummyRoom = roomSelectedBeanArrayList.get(i);
            if(!roomNameList.contains(dummyRoom.getName())){
                setRoom(dummyRoom.getRcmodel(), dummyRoom.getName());
            }
            roomNameList.add(dummyRoom.getName());
        }
        String image_url = hotelModel.getImage();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {

    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "data: " + data);
    }

    public void setRoom(final ArrayList<RoomModel> room_list, final String key) {
        tempArrayList = new ArrayList<>();
        roomArrayList = new ArrayList<>();
        ImageView hotel_image, persion_image;
        TextView room_type;
        final LinearLayout header_layout, persion_layout;
        View view_header = LayoutInflater.from(mActivity).inflate(R.layout.hotel_info_item_main, topLayout, false);
        header_layout = (LinearLayout) view_header.findViewById(R.id.hotel_info_item);
        persion_layout = (LinearLayout) view_header.findViewById(R.id.persion_image_layout);
        room_type = (TextView) view_header.findViewById(R.id.room_type);
        hotel_image = (ImageView) view_header.findViewById(R.id.hotel_image);
        room_type.setText(key);
        int totalPassengers = room_list.get(0).getNa() + room_list.get(0).getNc();
        for (int i = 0; i < totalPassengers; i++) {
            if (isAdded()) {
                persion_image = new ImageView(mActivity);
                persion_image.setImageResource(R.drawable.peopleicon);
                persion_layout.addView(persion_image);
            }
        }
        if (isArabicLang) {
            room_type.setGravity(Gravity.START);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                room_type.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
        }
        tempArrayList.clear();
        for (int i = 0; i < room_list.size(); i++) {
            boolean isAvailable = false;
            RoomModel roomModel = room_list.get(i);
            for (RoomModel item : tempArrayList) {
                double tempArrayPrice = 0.0;
                double itemArrayPrice = 0.0;
                if (!roomModel.getIsRcObject().equalsIgnoreCase("rc")) {
                    tempArrayPrice = Integer.parseInt(hotelModel.getRoom_count()) * Double.parseDouble(roomModel.getPrice());
                } else {
                    tempArrayPrice = Double.parseDouble(roomModel.getPrice());
                }
                if (!item.getIsRcObject().equalsIgnoreCase("rc")) {
                    itemArrayPrice = Integer.parseInt(hotelModel.getRoom_count()) * Double.parseDouble(item.getPrice());
                } else {
                    itemArrayPrice = Double.parseDouble(item.getPrice());
                }
                int tempRoomPrice = (int) Math.ceil(tempArrayPrice);
                int itemRoomPrice = (int) Math.ceil(itemArrayPrice);
                if (item.getBrakfast().equalsIgnoreCase(roomModel.getBrakfast()) && (tempRoomPrice == itemRoomPrice)) {
                    isAvailable = true;
                    break;
                }
            }
            if (!isAvailable) {
                tempArrayList.add(roomModel);
            }
        }
        //sort the room by price
        Collections.sort(tempArrayList, new Comparator<RoomModel>() {
            public int compare(RoomModel e1, RoomModel e2) {
                Double firstRoomPrice = 0.0;
                Double secondRoomPrice = 0.0;
                if (!e1.getIsRcObject().equalsIgnoreCase("rc")) {
                    firstRoomPrice = Integer.parseInt(hotelModel.getRoom_count()) * Double.parseDouble(e1.getPrice());
                } else {
                    firstRoomPrice = Double.parseDouble(e1.getPrice());
                }
                if (!e2.getIsRcObject().equalsIgnoreCase("rc")) {
                    secondRoomPrice = Integer.parseInt(hotelModel.getRoom_count()) * Double.parseDouble(e2.getPrice());
                } else {
                    secondRoomPrice = Double.parseDouble(e2.getPrice());
                }
                return firstRoomPrice.compareTo(secondRoomPrice);
            }
        });
        roomArrayList.clear();
        for (int i = 0; i < tempArrayList.size(); i++) {
            boolean isAvailable = false;
            RoomModel roomModel = tempArrayList.get(i);
            for (RoomModel item : roomArrayList) {
                if (item.getBrakfast().equalsIgnoreCase(roomModel.getBrakfast())) {
                    isAvailable = true;
                    break;
                }
            }
            if (!isAvailable) {
                roomArrayList.add(roomModel);
            }
        }
        for (int i = 0; i < roomArrayList.size(); i++) {
            final RoomModel model = roomArrayList.get(i);
            final TextView breakfast, cancellation_date, cancel_policy_text, previos_price, final_price, room_count, hotDeal,
                    offer, cancellation_title, cancellation_header, bookNowHeader, bookNowDescription, discPriceCurrencyText,
                    discPriceDecimalText, priceCurrencyText, priceDecimalText;
            final RelativeLayout cancellation_layout, cancalation_policy_layout;
            LinearLayout bnplPackageLayout, discountPriceLayout;
            final View view = LayoutInflater.from(mActivity).inflate(R.layout.hotel_info_item, topLayout, false);
            breakfast = (TextView) view.findViewById(R.id.breakfast);
            cancellation_date = (TextView) view.findViewById(R.id.cancalation_date);
            cancel_policy_text = (TextView) view.findViewById(R.id.cancel_policy_text);
            previos_price = (TextView) view.findViewById(R.id.price_previous);
            final_price = (TextView) view.findViewById(R.id.price_final);
            discPriceCurrencyText = (TextView) view.findViewById(R.id.currency_text_disc);
            discPriceDecimalText = (TextView) view.findViewById(R.id.price_decimal_text_disc);
            priceCurrencyText = (TextView) view.findViewById(R.id.currency_text_final);
            priceDecimalText = (TextView) view.findViewById(R.id.price_decimal_text__final);
            discountPriceLayout = (LinearLayout) view.findViewById(R.id.discount_price_layout);
            room_count = (TextView) view.findViewById(R.id.no_of_night_room);
            offer = (TextView) view.findViewById(R.id.offer);
            hotDeal = (TextView) view.findViewById(R.id.hot_deal);
            cancellation_layout = (RelativeLayout) view.findViewById(R.id.cancellation_layout);
            cancalation_policy_layout = (RelativeLayout) view.findViewById(R.id.cancalation_policy_layout);
            cancellation_title = (TextView) view.findViewById(R.id.cancel_policy_title);
            cancellation_header = (TextView) view.findViewById(R.id.cancalation);
            TextView book = (TextView) view.findViewById(R.id.book1);
            bnplPackageLayout = (LinearLayout) view.findViewById(R.id.bnpl_package_layout);
            bookNowHeader = (TextView) view.findViewById(R.id.book_now_header);
            bookNowDescription = (TextView) view.findViewById(R.id.book_now_description);
            cancellation_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //show the complete cancellation details
                }
            });
            if (isArabicLang) {
                breakfast.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
                cancellation_header.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
                cancellation_date.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
                cancellation_title.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
                cancel_policy_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
                hotDeal.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
                offer.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
                previos_price.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
                final_price.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.userName_text));
                room_count.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
                book.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
                bookNowHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
                bookNowDescription.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
                discPriceCurrencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.flight_moreDetails));
                discPriceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.flight_moreDetails));
                priceCurrencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
                priceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            }
            breakfast.setTypeface(regularBold);
            cancellation_header.setTypeface(regularBold);
            cancellation_date.setTypeface(textFace);
            cancellation_title.setTypeface(regularBold);
            cancel_policy_text.setTypeface(textFace);
            hotDeal.setTypeface(textFace);
            offer.setTypeface(textFace);
            previos_price.setTypeface(textFace);
            final_price.setTypeface(textFace);
            discPriceCurrencyText.setTypeface(textFace);
            discPriceDecimalText.setTypeface(textFace);
            priceCurrencyText.setTypeface(textFace);
            priceDecimalText.setTypeface(textFace);
            room_count.setTypeface(textFace);
            book.setTypeface(textFace);
            bookNowHeader.setTypeface(regularBold);
            bookNowDescription.setTypeface(textFace);
            breakfast.setText(Utils.getMealName(mActivity, model.getBrakfast()));
            double price = Double.parseDouble(model.getPrice());
            double wdp = Double.parseDouble(model.getWdp());
            if (!model.getIsRcObject().equals("rc")) {
                price = Integer.parseInt(hotelModel.getRoom_count()) * price;
                wdp = Integer.parseInt(hotelModel.getRoom_count()) * wdp;
            }
            Double origprice = Utils.convertPriceToUserSelectionWithDecimal(price, model.getCurrency(), mActivity);
            String actualPriceResult = String.format(Locale.ENGLISH, "%.2f", origprice);
            String priceResult[] = actualPriceResult.split("\\.");
            String currencyResult = model.getCurrency();
            if (isArabicLang) {
                priceCurrencyText.setTypeface(textFace);
                final_price.setTypeface(regularBold);
                priceDecimalText.setTypeface(textFace);
                priceCurrencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
                final_price.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.text_size));
                priceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
                final_price.setText("." + priceResult[0] + " ");
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new RecyclerView.LayoutParams(
                        RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));
                params.setMargins(0, 0, 0, 0);
                priceCurrencyText.setLayoutParams(params);
                priceCurrencyText.setText(priceResult[1]);
                if (currencyResult.equalsIgnoreCase("SAR")) {
                    priceDecimalText.setText(mActivity.getString(R.string.label_SAR_currency_name));
                } else {
                    priceDecimalText.setText(currencyResult);
                }
            } else {
                priceCurrencyText.setText(currencyResult);
                final_price.setText(priceResult[0] + ".");
                priceDecimalText.setText(priceResult[1]);
            }
            if (price < wdp) {
                Double discPrigprice = Utils.convertPriceToUserSelectionWithDecimal(wdp, model.getCurrency(), mActivity);
                ;
                String discActualPriceResult = String.format(Locale.ENGLISH, "%.2f", discPrigprice);
                String discPpriceResult[] = discActualPriceResult.split("\\.");
                if (isArabicLang) {
                    discPriceCurrencyText.setTypeface(textFace);
                    previos_price.setTypeface(regularBold);
                    discPriceDecimalText.setTypeface(textFace);
                    discPriceCurrencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.flight_duration_text));
                    previos_price.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.homeScreen_header_text));
                    discPriceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.flight_duration_text));
                    previos_price.setText("." + discPpriceResult[0] + " ");
                    LinearLayout.LayoutParams discparams = new LinearLayout.LayoutParams(new RecyclerView.LayoutParams(
                            RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));
                    discparams.setMargins(0, 0, 0, 0);
                    discPriceCurrencyText.setLayoutParams(discparams);
                    discPriceCurrencyText.setText(discPpriceResult[1]);
                    if (currencyResult.equalsIgnoreCase("SAR")) {
                        discPriceDecimalText.setText(mActivity.getString(R.string.label_SAR_currency_name));
                    } else {
                        discPriceDecimalText.setText(currencyResult);
                    }
                } else {
                    discPriceCurrencyText.setText(currencyResult);
                    previos_price.setText(discPpriceResult[0] + ".");
                    discPriceDecimalText.setText(discPpriceResult[1]);
                }
                discountPriceLayout.setVisibility(View.VISIBLE);
                hotDeal.setVisibility(View.VISIBLE);
                previos_price.setVisibility(View.VISIBLE);
                previos_price.setPaintFlags(previos_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                discPriceCurrencyText.setPaintFlags(discPriceCurrencyText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                discPriceDecimalText.setPaintFlags(discPriceDecimalText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                offer.setVisibility(View.VISIBLE);
                double per = (((wdp - price) * 100) / price);
                int percent = (int) per;
                Double priceToShow = (wdp - price);
                offer.setText(percent + mActivity.getString(R.string.price_saving_text) + Utils.convertPriceToUserSelectionHotel(priceToShow, model.getCurrency(), mActivity));
            } else {
                discountPriceLayout.setVisibility(View.GONE);
                previos_price.setVisibility(View.GONE);
                hotDeal.setVisibility(View.GONE);
                offer.setVisibility(View.GONE);
            }
            String room;
            if (hotelModel.getRoom_count().equalsIgnoreCase("1")) {
                room = " " + mActivity.getString(R.string.room_lebel);
            } else {
                room = mActivity.getString(R.string.rooms_lebel);
            }
            String night;
            if (hotelModel.getDur().equals("1")) {
                night = " " + mActivity.getString(R.string.label_one_night) + ", ";
            } else {
                night = " " + mActivity.getString(R.string.label_more_nights) + ", ";
            }
            room_count.setText(hotelModel.getDur() + night + hotelModel.getRoom_count() + room);
            if (Utils.isArabicLangSelected(mActivity)) {
                String nightInArbic = "";
                if (Integer.parseInt(hotelModel.getDur()) == 1) {
                    nightInArbic = mActivity.getResources().getString(R.string.label_one_night);
                } else if (Integer.parseInt(hotelModel.getDur()) > 1 && Integer.parseInt(hotelModel.getDur()) < 11) {
                    nightInArbic = mActivity.getResources().getString(R.string.label_below_ten_night);
                } else {
                    nightInArbic = mActivity.getResources().getString(R.string.label_more_nights);
                }
                room_count.setText(hotelModel.getDur() + nightInArbic + hotelModel.getRoom_count() + room);
            }
            hotel_image.setVisibility(View.GONE);
            String cancel_date = model.getFree_cancel_date();
            if (cancel_date != null && !cancel_date.equalsIgnoreCase("null") && cancel_date.length() > 0) {
                DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
                Date dateBefore = null;
                try {
                    dateBefore = simpleDateFormat.parse(cancel_date);
                } catch (Exception e) {
                }
                SimpleDateFormat finalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                String finalDate = finalFormat.format(dateBefore);
                String CancellationDateDiff = Utils.flightSearchDateDifference(finalDate, currentDate);
                if (Integer.parseInt(CancellationDateDiff) > 0) {
                    bnplPackageLayout.setVisibility(View.VISIBLE);
                } else {
                    bnplPackageLayout.setVisibility(View.GONE);
                }
                cancellation_layout.setVisibility(View.VISIBLE);
                try {
                    cancellation_date.setText(getString(R.string.label_room_selected_until_text) + " " + Utils.cancellationDateFormat(cancel_date, mActivity));
                } catch (Exception e) {
                }
                try {
                    cancel_policy_text.setText(cancel_policy_text.getText() + cancel_date.replace("T", " "));
                } catch (Exception e) {
                }
            } else {
                cancellation_layout.setVisibility(View.GONE);
                cancalation_policy_layout.setVisibility(View.GONE);
            }
            cancellation_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cancalation_policy_layout.getVisibility() == View.VISIBLE) {
                        cancalation_policy_layout.setVisibility(View.GONE);
                        cancellation_title.setVisibility(View.GONE);
                    } else {
                        cancalation_policy_layout.setVisibility(View.VISIBLE);
                        cancellation_title.setVisibility(View.VISIBLE);
                    }
                }
            });
            book.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (model.getIsRcObject().equalsIgnoreCase("rc")) {
                            JSONObject objectrc = new JSONObject();
                            JSONObject hotelObj = new JSONObject(model.getHotelrequestJson());
                            JSONArray roomJsonArray = hotelObj.getJSONArray("room");
                            Utils.printMessage(TAG, "roomJsonArray :: " + roomJsonArray);
                            _id = 1;
                            int roomId = 1;
                            for (int i = 0; i < roomJsonArray.length(); i++) {
                                roomJsonArray.getJSONObject(i).put("id", String.valueOf(roomId));
                                roomJsonArray.getJSONObject(i).put("lp", setLP());
                                setOCP(hotelObj, i);
                                roomId++;
                            }
                            objectrc.put("rc", hotelObj);
                            hotelModel.setHotelJson(objectrc.toString());
                        } else {
                            String requestjsondata = hotelModel.getHotelJson();
                            JSONObject requestjsonobject = new JSONObject(requestjsondata);
                            JSONObject roomJsonObj = new JSONObject(model.getRoom_json());
                            requestjsonobject.getJSONObject("p").put("val", roomJsonObj.getJSONObject("p").getString("val"));
                            requestjsonobject.getJSONObject("p").put("cur", roomJsonObj.getJSONObject("p").getString("cur"));
                            requestjsonobject.getJSONObject("wdp").put("val", roomJsonObj.getJSONObject("wdp").getString("val"));
                            requestjsonobject.getJSONObject("wdp").put("cur", roomJsonObj.getJSONObject("wdp").getString("cur"));
                            requestjsonobject.getJSONObject("np").put("val", roomJsonObj.getJSONObject("np").getString("val"));
                            requestjsonobject.getJSONObject("np").put("cur", roomJsonObj.getJSONObject("np").getString("cur"));
                            if (roomJsonObj.has("mkp")) {
                                JSONObject mkp = roomJsonObj.optJSONObject("mkp");
                                if (mkp != null) {
                                    requestjsonobject.getJSONObject("mkp").put("val", mkp.optString("val"));
                                    requestjsonobject.getJSONObject("mkp").put("cur", mkp.optString("cur"));
                                }
                            }
                            JSONArray array = new JSONArray();
                            _id = 1;
                            int roomId = 1;
                            for (int k = 0; k < Integer.parseInt(hotelModel.getRoom_count()); k++) {
                                JSONObject hotelObj = new JSONObject(model.getHotelrequestJson());
                                JSONObject jsonObj = hotelObj.getJSONObject("room");
                                jsonObj.put("id", String.valueOf(roomId));
                                jsonObj.put("lp", setLP());
                                jsonObj.put("ocp", getPAXOBject(k));
                                array.put(k, jsonObj);
                                roomId++;
                            }
                            requestjsonobject.put("room", array);
                            try {
                                JSONObject htJSON = new JSONObject(model.getHotelJson());
                                requestjsonobject.remove("ht");
                                requestjsonobject.put("ht", htJSON);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            JSONObject objectrc = new JSONObject();
                            objectrc.put("rc", requestjsonobject);
                            hotelModel.setHotelJson(objectrc.toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    hotelModel.setCur(model.getCurrency());
                    Gson gson = new Gson();
                    String modelString = gson.toJson(hotelModel);
                    Fragment fragment = new HotelTravelerDetailFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("modelList", hotelModel);
                    bundle.putString("model", modelString);
                    bundle.putInt("adult_count", adults);
                    bundle.putInt("child_count", childs);
                    bundle.putStringArrayList("adults_list", adultsCountList);
                    bundle.putStringArrayList("childs_list", childCountList);
                    bundle.putSerializable("children_list", childCount);
                    bundle.putString("header_date", headerDate);
                    bundle.putString("payPrice", final_price.getText().toString());
                    bundle.putSerializable("room_count_list", roomPassengerArrayList);
                    bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                    bundle.putString("gettingDataFrom", "detailsFragment");
                    fragment.setArguments(bundle);
                    ((MainSearchActivity) mActivity).pushFragment(fragment);
                }
            });
            header_layout.addView(view);
        }
        topLayout.addView(view_header);
    }

    private JSONObject setOCP(JSONObject hotelObj, int roomPos) {
        try {
            hotelObj.getJSONArray("room").getJSONObject(roomPos).put("ocp", getPAXOBject(roomPos));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hotelObj;
    }

    private JSONObject getPAXOBject(int roomPos) {
        try {
            ArrayList<String> adult_count_list = hotelModel.getAdultcountlist();
            HashMap<String, ArrayList<String>> child_count_list = hotelModel.getChild_count_list();
            JSONObject rooms = new JSONObject();
            JSONArray pax = new JSONArray();
            int adult_count = Integer.parseInt(adult_count_list.get(roomPos));
            Utils.printMessage(TAG, "adult_count::" + adult_count);
            for (int j = 0; j < adult_count; j++) {
                JSONObject age_obj = new JSONObject();
                age_obj.put("age", 45);
                age_obj.put("id", _id);
                pax.put(age_obj);
                _id++;
            }
            rooms.put("na", adult_count);
            ArrayList<String> list;
            if (child_count_list != null) {
                list = child_count_list.get("room" + (roomPos + 1));
                if (list != null) {
                    for (int j = 0; j < list.size(); j++) {
                        JSONObject age_obj = new JSONObject();
                        age_obj.put("age", list.get(j));
                        age_obj.put("id", _id);
                        pax.put(age_obj);
                        _id++;
                    }
                    rooms.put("nc", list.size());
                }
                rooms.put("pax", pax);
            }
            return rooms;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject setLP() {
        JSONObject object = new JSONObject();
        try {
            object.put("fname", "lalit");
            object.put("lname", "sharma");
            object.put("age", "28");
            object.put("email", "lalitsharma1607@gmail.com");
            object.put("ph", "9015376344");
            return object;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }
}
