package com.flyin.bookings;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.adapter.SavedCardsAdapter;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.SavedCardsBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SavedCardsActivity extends AppCompatActivity implements OnCustomItemSelectListener, AsyncTaskListener {
    private Typeface titleFace;
    private SavedCardsAdapter savedCardsAdapter;
    private ListView listView;
    private ArrayList<SavedCardsBean> cardsArrayList = new ArrayList();
    private int GET_SEARCH_RESULTS = 1;
    private int GET_DEL_RESULTS = 2;
    private LayoutInflater inflater;
    private ImageView errorImage = null;
    private Button searchButton = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private RelativeLayout headerViewLayout = null;
    private View loadingView = null;
    private static final String TAG = "MultiCityResultsActivity";
    private String cardUID = "";
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null, noCardsText;
    private MyReceiver myReceiver;
    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_cards);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(SavedCardsActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        titleFace = Typeface.createFromAsset(getAssets(), fontText);
        listView = (ListView) findViewById(R.id.listview_cards);
        noCardsText = (TextView) findViewById(R.id.no_cards_textview);
        headerViewLayout = (RelativeLayout) findViewById(R.id.header_view_layout);
        getRequestFromServer();
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(titleFace);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter("com.flyin.bookings.ERROR_MESSAGE");
        registerReceiver(myReceiver, intentFilter);
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_saved_cards);
        mTitleText.setTypeface(tf);
        backText.setTypeface(titleFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(SavedCardsActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.ERROR_MESSAGE")) {
                displayErrorMessage(intent.getStringExtra("message"));
            }
        }
    }

    private void getRequestFromServer() {
        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
            mainJSON.put("uid", preferences.getString(Constants.MEMBER_EMAIL, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        showLoading();
        HTTPAsync async = new HTTPAsync(SavedCardsActivity.this, SavedCardsActivity.this, Constants.SAVEDCARD_SEARCH_URL,
                "", mainJSON.toString(), GET_SEARCH_RESULTS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void deletCardDetails(String cardUID) {
        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
            mainJSON.put("uid", preferences.getString(Constants.MEMBER_EMAIL, ""));
            mainJSON.put("cardUID", cardUID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        showLoading();
        HTTPAsync async = new HTTPAsync(SavedCardsActivity.this, SavedCardsActivity.this, Constants.SAVEDCARD_DEL_SEARCH_URL,
                "", mainJSON.toString(), GET_DEL_RESULTS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView waitText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setTypeface(titleFace);
        waitText.setTypeface(titleFace);
        waitText.setVisibility(View.GONE);
        loadingText.setText(getString(R.string.label_just_a_moment));
        if (Utils.isArabicLangSelected(SavedCardsActivity.this)) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            waitText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        headerViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        try {
            headerViewLayout.removeView(loadingView);
            loadingView = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {

        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            closeLoading();
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(titleFace);
            errorDescriptionText.setTypeface(titleFace);
            searchButton.setTypeface(titleFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    headerViewLayout.removeView(errorView);
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            headerViewLayout.addView(errorView);
        } else {
            if (serviceType == GET_SEARCH_RESULTS) {
                closeLoading();
                JSONObject resultDataObj = null;
                String tdrResult = "";
                try {
                    resultDataObj = new JSONObject(data);
                    if (resultDataObj.get("status").equals("SUCCESS")) {
                        JSONArray piArray = resultDataObj.getJSONArray("cards");
                        if (piArray.length() != 0) {
                            noCardsText.setVisibility(View.GONE);
                            for (int i = 0; i < piArray.length(); i++) {
                                SavedCardsBean savedCardsBean = new SavedCardsBean();
                                savedCardsBean.setCardHolderName(piArray.getJSONObject(i).getString("customerName"));
                                savedCardsBean.setExpiryDate(piArray.getJSONObject(i).getString("expiryDate"));
                                savedCardsBean.setCardNumber(piArray.getJSONObject(i).getString("cardNumber"));
                                savedCardsBean.setCardType(piArray.getJSONObject(i).getString("paymentOption"));
                                savedCardsBean.setCardUID(piArray.getJSONObject(i).getString("cardUID"));
                                cardsArrayList.add(savedCardsBean);
                            }
                        } else {
                            noCardsText.setText(getString(R.string.label_nocards_Available));
                            noCardsText.setVisibility(View.VISIBLE);
                            displayErrorMessage(getString(R.string.label_nocards_Available));
                        }
                    } else {
                        displayErrorMessage(getString(R.string.label_nocards_Available));
                        noCardsText.setVisibility(View.VISIBLE);
                        noCardsText.setText(getString(R.string.label_nocards_Available));
                    }
                    savedCardsAdapter = new SavedCardsAdapter(SavedCardsActivity.this, cardsArrayList, SavedCardsActivity.this);
                    listView.setAdapter(savedCardsAdapter);
                    savedCardsAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Utils.printMessage(TAG, "Exception occured::" + e.getMessage());
                }
            } else if (serviceType == GET_DEL_RESULTS) {
                closeLoading();
                JSONObject resultDataObj = null;
                try {
                    resultDataObj = new JSONObject(data);
                    if (resultDataObj.get("status").equals("SUCCESS")) {
                        cardsArrayList.remove(pos);
                        savedCardsAdapter.notifyDataSetChanged();
                        displayErrorMessage(getString(R.string.label_removed_card));
                    } else {
                        //Handle Err
                    }
                } catch (Exception e) {
                    Utils.printMessage(TAG, "Exception occured::" + e.getMessage());
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(final int position) {
        cardUID = cardsArrayList.get(position).getCardUID();
        pos = position;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(SavedCardsActivity.this);
        alertDialog.setCancelable(false);
        alertDialog.setTitle(SavedCardsActivity.this.getString(R.string.label_remove_confirm));
        alertDialog.setPositiveButton(SavedCardsActivity.this.getResources().getString(R.string.label_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                deletCardDetails(cardsArrayList.get(position).getCardUID());
            }
        });
        alertDialog.setNegativeButton(SavedCardsActivity.this.getResources().getString(R.string.label_no_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onSelectedItemClick(int position, int type) {
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (message.equalsIgnoreCase(getString(R.string.label_removed_card))) {
            errorView.setBackgroundResource(R.color.success_message_background);
        } else {
            errorView.setBackgroundResource(R.color.error_message_background);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }
}
