package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.HotelDetailFragment;
import com.flyin.bookings.HotelListFragment;
import com.flyin.bookings.MainSearchActivity;
import com.flyin.bookings.R;
import com.flyin.bookings.listeners.CancelHotelBookingListener;
import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.model.RoundImageTransform;
import com.flyin.bookings.model.TripAdvisorBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

@SuppressWarnings("deprecation")
public class HotelListsAdapter extends RecyclerView.Adapter<HotelListsAdapter.MyViewHolder> {
    private final HashMap<String, ArrayList<String>> chidlrenCount;
    private Activity activity;
    private final int childCount;
    private final int adultCount;
    private String header_date;
    private ArrayList<HotelModel> finalList = new ArrayList<>();
    private ArrayList<HotelModel> moviesList;
    private HotelListFragment fragment;
    private ArrayList<String> adultList;
    private ArrayList<String> childList;
    private ArrayList<HotelModel> hotelList = new ArrayList<>();
    private static final String TAG = "HotelListAdapter";
    public static int sCorner = 0;
    public static int sMargin = 0;
    private String currentDate;
    private ArrayList<TripAdvisorBean> tripAdvisorArrayList = new ArrayList<>();
    private CancelHotelBookingListener listener;
    private String randomNumber = "";
    private Typeface regularLight, regularFace, mediumFace, textFace;
    private String cityName = "";
    private String hotelCheckInDate = "";
    private String hotelCheckOutDate = "";
    private ProgressBar progressBar;

    public HotelListsAdapter(ArrayList<HotelModel> moviesList, HotelListFragment fragment, Activity activity, String date,
                             int adultCount, int childCount, ArrayList<String> adultList, HashMap<String, ArrayList<String>> count,
                             ArrayList<String> childList, ArrayList<HotelModel> hotelList, String currentDate,
                             ArrayList<TripAdvisorBean> tripAdvisorArray, CancelHotelBookingListener listener, String randomNumber,
                             String cityName, String hotelCheckInDate, String hotelCheckOutDate) {
        this.finalList = new ArrayList<>(hotelList);
        this.moviesList = moviesList;
        this.activity = activity;
        header_date = date;
        this.childCount = childCount;
        this.adultCount = adultCount;
        this.fragment = fragment;
        this.adultList = adultList;
        this.chidlrenCount = count;
        this.childList = childList;
        this.hotelList = hotelList;
        this.currentDate = currentDate;
        this.tripAdvisorArrayList = tripAdvisorArray;
        this.listener = listener;
        this.randomNumber = randomNumber;
        this.cityName = cityName;
        this.hotelCheckInDate = hotelCheckInDate;
        this.hotelCheckOutDate = hotelCheckOutDate;
        progressBar = new ProgressBar(activity);
        String fontRegularLight = Constants.FONT_ROBOTO_LIGHT;
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        String fontMedium = Constants.FONT_ROBOTO_MEDIUM;
        String fontText = Constants.FONT_ROBOTO_BOLD;
        if (Utils.isArabicLangSelected(activity)) {
            fontRegularLight = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
            fontMedium = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        regularLight = Typeface.createFromAsset(activity.getAssets(), fontRegularLight);
        regularFace = Typeface.createFromAsset(activity.getAssets(), fontRegular);
        mediumFace = Typeface.createFromAsset(activity.getAssets(), fontMedium);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_hotel_view_item, parent, false);
        return new MyViewHolder(itemView, activity);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_hotel_name, price, address, night, discountedPrice, hotDeal, offer_text, bnplPackageText, reviewText,
                bookHotelText, discPriceCurrencyText, discPriceDecimalText, priceCurrencyText, priceDecimalText, hotelResultsText;
        private ImageView image, imageOwl;
        private RatingBar rate;
        private ProgressBar progressBar;
        private RelativeLayout imageLayout, priceLoadingLayout, offerTextLayout;
        private LinearLayout tripAdvisorLayout, discountpriceLayout, headerViewLayout, priceViewLayout;

        public MyViewHolder(View view, Context context) {
            super(view);
            txt_hotel_name = (TextView) view.findViewById(R.id.txt_hotel_name);
            hotDeal = (TextView) view.findViewById(R.id.hot_deal);
            price = (TextView) view.findViewById(R.id.price);
            discountedPrice = (TextView) view.findViewById(R.id.price_previous);
            address = (TextView) view.findViewById(R.id.address);
            offer_text = (TextView) view.findViewById(R.id.offer_percent);
            offerTextLayout = (RelativeLayout) view.findViewById(R.id.offer_text_layout);
            bnplPackageText = (TextView) view.findViewById(R.id.bnpl_package_text);
            imageLayout = (RelativeLayout) view.findViewById(R.id.image_layout);
            night = (TextView) view.findViewById(R.id.night_count);
            image = (ImageView) view.findViewById(R.id.image);
            rate = (RatingBar) view.findViewById(R.id.ratingBar);
            reviewText = (TextView) view.findViewById(R.id.review_text);
            imageOwl = (ImageView) view.findViewById(R.id.owl);
            tripAdvisorLayout = (LinearLayout) view.findViewById(R.id.trip_advisor_layout);
            discountpriceLayout = (LinearLayout) view.findViewById(R.id.discount_price_layout);
            bookHotelText = (TextView) view.findViewById(R.id.book_hotel_button);
            discPriceCurrencyText = (TextView) view.findViewById(R.id.currency_text);
            discPriceDecimalText = (TextView) view.findViewById(R.id.price_decimal_text);
            priceCurrencyText = (TextView) view.findViewById(R.id.currency_text_);
            priceDecimalText = (TextView) view.findViewById(R.id.price_decimal_text_);
            headerViewLayout = (LinearLayout) view.findViewById(R.id.header_view_layout);
            hotelResultsText = (TextView) view.findViewById(R.id.txt_result_count_lebel);
            priceViewLayout = (LinearLayout) view.findViewById(R.id.price_view_layout);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            String fontRegularLight = Constants.FONT_ROBOTO_LIGHT;
            String fontRegular = Constants.FONT_ROBOTO_REGULAR;
            String fontMedium = Constants.FONT_ROBOTO_MEDIUM;
            if (Utils.isArabicLangSelected(context)) {
                fontRegularLight = Constants.FONT_DROIDKUFI_REGULAR;
                fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
                fontMedium = Constants.FONT_DROIDKUFI_REGULAR;
            }
            Typeface regularLight = Typeface.createFromAsset(context.getAssets(), fontRegularLight);
            Typeface regularFace = Typeface.createFromAsset(context.getAssets(), fontRegular);
            Typeface mediumFace = Typeface.createFromAsset(context.getAssets(), fontMedium);
            txt_hotel_name.setTypeface(mediumFace);
            address.setTypeface(regularLight);
            offer_text.setTypeface(regularFace);
            night.setTypeface(regularFace);
            reviewText.setTypeface(regularLight);
            hotDeal.setTypeface(regularFace);
            price.setTypeface(mediumFace);
            bnplPackageText.setTypeface(regularFace);
            bookHotelText.setTypeface(regularFace);
            discountedPrice.setTypeface(regularFace);
            hotelResultsText.setTypeface(mediumFace);
            if (Utils.isArabicLangSelected(context)) {
                txt_hotel_name.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_hotel_header_text));
                address.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_text_size));
                discountedPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_time_text));
                hotDeal.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_airline_name_text));
                offer_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_time_text));
                bnplPackageText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                reviewText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_time_text));
                night.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.flight_moreDetails));
                bookHotelText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.hotel_city_text_size));
                price.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                hotelResultsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_text_size));
                offerTextLayout.setBackgroundResource(R.drawable.arabic_trangle);
            } else {
                offerTextLayout.setBackgroundResource(R.drawable.trangle);
            }
        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {
            if (position == 0) {
                holder.headerViewLayout.setVisibility(View.VISIBLE);
                if (moviesList != null && moviesList.size() != 0) {
                    holder.hotelResultsText.setVisibility(View.VISIBLE);
                    String resultText = "";
                    String count = String.valueOf(moviesList.size());
                    if (moviesList.size() == 1) {
                        resultText = activity.getString(R.string.label_one_hotel_text) + " " + activity.getString(R.string.hotel_available_text) + " " + cityName;
                    } else if (moviesList.size() > 1 && moviesList.size() < 11) {
                        resultText = activity.getString(R.string.label_two_ten_hotels_text) + " " + activity.getString(R.string.hotel_available_text) + " " + cityName;
                    } else {
                        resultText = activity.getString(R.string.label_eleven_hotels_text) + " " + activity.getString(R.string.hotel_available_text) + " " + cityName;
                    }
                    holder.hotelResultsText.setText(count + " " + resultText, TextView.BufferType.SPANNABLE);
                    Spannable s = (Spannable) holder.hotelResultsText.getText();
                    int start = count.length();
                    s.setSpan(new RelativeSizeSpan(1.5f), 0, start, 0);
                    s.setSpan(new ForegroundColorSpan(0xFFFCC524), 0, start, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else {
                    holder.hotelResultsText.setVisibility(View.GONE);
                }
            } else {
                holder.headerViewLayout.setVisibility(View.GONE);
            }
            final HotelModel hotel = moviesList.get(position);
            String hotelDis = hotel.getDistric();
            hotelDis = (hotelDis == null || hotelDis.equalsIgnoreCase("null")) ? "" : hotel.getDistric();
            String adr = hotelDis.equals("") ? hotel.getCity() : hotelDis + ", " + hotel.getCity();
            holder.address.setText(adr);
            final ImageView iv = holder.image;
            ViewTreeObserver vto = iv.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    iv.getViewTreeObserver().removeOnPreDrawListener(this);
                    int finalHeight = iv.getMeasuredHeight();
                    int finalWidth = iv.getMeasuredWidth();
                    String url = Constants.IMAGE_SCALING_URL + finalWidth + "x" + finalHeight + Constants.IMAGE_QUALITY + "" + hotel.getImage();
                    Glide.with(activity).load(url)
                            .placeholder(R.drawable.imagethumb_search)
                            .bitmapTransform(new RoundImageTransform(activity, sCorner, sMargin)).into(holder.image);
                    return true;
                }
            });
            holder.txt_hotel_name.setText(hotel.getHna());
            String nightInArbic = "";
            if (Utils.isArabicLangSelected(activity)) {
                holder.offer_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.flight_moreDetails));
                if (Integer.parseInt(hotel.getDur()) == 1) {
                    nightInArbic = activity.getResources().getString(R.string.label_one_night);
                } else if (Integer.parseInt(hotel.getDur()) > 1 && Integer.parseInt(hotel.getDur()) < 11) {
                    nightInArbic = activity.getResources().getString(R.string.label_below_ten_night);
                } else {
                    nightInArbic = activity.getResources().getString(R.string.label_more_nights);
                }
            } else {
                nightInArbic = Integer.parseInt(hotel.getDur()) > 1 ? activity.getResources().getString(R.string.label_more_nights).toLowerCase() : activity.getResources().getString(R.string.label_one_night).toLowerCase();
            }
            holder.night.setText(activity.getResources().getString(R.string.price_for) + " " + hotel.getDur() + " " + nightInArbic);
            float star = Float.parseFloat(hotel.getStar());
            if (star != 0.0) {
                holder.rate.setVisibility(View.VISIBLE);
                holder.rate.setRating(Float.parseFloat(hotel.getStar()));
            } else {
                holder.rate.setVisibility(View.GONE);
            }
            holder.tripAdvisorLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onSelectedListItem(position, hotel.getHna(), hotel.getHuid());
                    }
                }
            });
            double price = Double.parseDouble(hotel.getP());
            double wdp = Double.parseDouble(hotel.getWdp());
            Double actualResult = Utils.convertPriceToUserSelectionWithDecimal(price, hotel.getCur(), activity);
            String actualPriceResult = String.format(Locale.ENGLISH, "%.2f", actualResult);
            String priceResult[] = actualPriceResult.split("\\.");
            Double discPryPrice = Utils.convertPriceToUserSelectionWithDecimal(wdp, hotel.getCur(), activity);
            String discActualPriceResult = String.format(Locale.ENGLISH, "%.2f", discPryPrice);
            String discPpriceResult[] = discActualPriceResult.split("\\.");
            String currencyResult = hotel.getCur();
            if (Utils.isArabicLangSelected(activity)) {
                holder.priceCurrencyText.setTypeface(regularFace);
                holder.price.setTypeface(textFace);
                holder.priceDecimalText.setTypeface(regularFace);
                holder.priceCurrencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                holder.price.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.text_size));
                holder.priceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                holder.price.setText("." + priceResult[0] + " ");
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new RecyclerView.LayoutParams(
                        RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));
                params.setMargins(0, 0, 0, 0);
                holder.priceCurrencyText.setLayoutParams(params);
                holder.priceCurrencyText.setText(priceResult[1]);
                if (currencyResult.equalsIgnoreCase("SAR")) {
                    holder.priceDecimalText.setText(activity.getString(R.string.label_SAR_currency_name));
                } else {
                    holder.priceDecimalText.setText(currencyResult);
                }
                holder.discPriceCurrencyText.setTypeface(regularFace);
                holder.discountedPrice.setTypeface(textFace);
                holder.discPriceDecimalText.setTypeface(regularFace);
                holder.discPriceCurrencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.flight_duration_text));
                holder.discountedPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.homeScreen_header_text));
                holder.discPriceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.flight_duration_text));
                holder.discountedPrice.setText("." + discPpriceResult[0] + " ");
                LinearLayout.LayoutParams discparams = new LinearLayout.LayoutParams(new RecyclerView.LayoutParams(
                        RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));
                discparams.setMargins(0, 0, 0, 0);
                holder.discPriceCurrencyText.setLayoutParams(discparams);
                holder.discPriceCurrencyText.setText(discPpriceResult[1]);
                if (currencyResult.equalsIgnoreCase("SAR")) {
                    holder.discPriceDecimalText.setText(activity.getString(R.string.label_SAR_currency_name));
                } else {
                    holder.discPriceDecimalText.setText(currencyResult);
                }
            } else {
                holder.priceCurrencyText.setText(currencyResult);
                holder.price.setText(priceResult[0] + ".");
                holder.priceDecimalText.setText(priceResult[1]);
                holder.discPriceCurrencyText.setText(currencyResult);
                holder.discountedPrice.setText(discPpriceResult[0] + ".");
                holder.discPriceDecimalText.setText(discPpriceResult[1]);
            }
            if (price < wdp) {
                holder.hotDeal.setVisibility(View.VISIBLE);
                holder.discountedPrice.setVisibility(View.VISIBLE);
                holder.discountpriceLayout.setVisibility(View.VISIBLE);
                holder.offerTextLayout.setVisibility(View.VISIBLE);
                holder.discountedPrice.setPaintFlags(holder.discountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.discPriceCurrencyText.setPaintFlags(holder.discPriceCurrencyText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.discPriceDecimalText.setPaintFlags(holder.discPriceDecimalText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                double per = (((wdp - price) * 100) / wdp);
                int percent = (int) Math.round(per);
                holder.offer_text.setText(percent + "%\n" + activity.getString(R.string.label_percent_off));
            } else {
                holder.discountedPrice.setVisibility(View.GONE);
                holder.discountpriceLayout.setVisibility(View.GONE);
                holder.hotDeal.setVisibility(View.GONE);
                holder.offerTextLayout.setVisibility(View.GONE);
            }
            if (hotel.getBnplDateDiff() > 0) {
                holder.bnplPackageText.setVisibility(View.VISIBLE);
            } else {
                holder.bnplPackageText.setVisibility(View.GONE);
            }
            if (holder.price.getText().toString().isEmpty()) {
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.priceViewLayout.setVisibility(View.GONE);
            } else {
                holder.progressBar.setVisibility(View.GONE);
                holder.priceViewLayout.setVisibility(View.VISIBLE);
            }
            holder.bookHotelText.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    SharedPreferences pref = activity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    if (editor != null) {
                        editor.putBoolean(Constants.IS_ROOM_INFO_UPDATED, false);
                        editor.putString("room_object", "");
                        editor.putString("update_in_date", "");
                        editor.putString("update_stay_nights", "");
                        editor.apply();
                    }
                    Bundle bundle = new Bundle();
                    try {
                        bundle.putParcelable("model", (Parcelable) moviesList.get(position).clone());
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                    bundle.putStringArrayList("adults_list", adultList);
                    bundle.putStringArrayList("childs_list", childList);
                    bundle.putSerializable("children_list", chidlrenCount);
                    bundle.putString("header_date", header_date);
                    bundle.putInt("adult_count", adultCount);
                    bundle.putInt("child_count", childCount);
                    bundle.putString("hotelCheckInDate", hotelCheckInDate);
                    bundle.putString("hotelCheckOutDate", hotelCheckOutDate);
                    bundle.putBoolean("isListFragment", true);
                    bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                    Fragment fragment = new HotelDetailFragment();
                    fragment.setArguments(bundle);
                    ((MainSearchActivity) activity).pushFragment(fragment);
                }
            });
            try {
                if (hotel.getReviewCount() == null || hotel.getReviewCount().isEmpty()) {
                    holder.reviewText.setVisibility(View.GONE);
                    holder.imageOwl.setVisibility(View.GONE);
                } else {
                    if (Integer.parseInt(hotel.getReviewCount()) == 0) {
                        holder.reviewText.setVisibility(View.GONE);
                        holder.imageOwl.setVisibility(View.GONE);
                    } else {
                        Resources res = activity.getResources();
                        String text = String.format(res.getString(R.string.label_hotel_review), hotel.getReviewCount());
                        holder.reviewText.setText(text);
                        holder.reviewText.setVisibility(View.VISIBLE);
                        holder.reviewText.setPaintFlags(holder.reviewText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                        if (hotel.getReviewImgUrl() == null || hotel.getReviewImgUrl().isEmpty()) {
                            holder.imageOwl.setVisibility(View.GONE);
                        } else {
                            Glide.with(activity).load(hotel.getReviewImgUrl()).placeholder(R.drawable.imagethumb_search).into(holder.imageOwl);
                            holder.imageOwl.setVisibility(View.VISIBLE);
                        }
                    }
                }
            } catch (Exception e) {
                holder.reviewText.setVisibility(View.GONE);
                holder.imageOwl.setVisibility(View.GONE);
                Utils.printMessage(TAG, "error Count : " + e.getMessage());
            }
        } catch (Exception e) {
            Utils.printMessage(TAG, "error" + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public void filter(String charText) {
        moviesList.clear();
        if (charText.length() == 0 || charText.isEmpty()) {
            moviesList.addAll(finalList);
        } else {
            for (HotelModel nameModel : finalList) {
                String str = nameModel.getHna();
                if (!str.equalsIgnoreCase("null") || !str.equalsIgnoreCase("") && str != null) {
                    if (str.toLowerCase(Locale.getDefault()).contains(charText.toLowerCase(Locale.getDefault()))) {
                        moviesList.add(nameModel);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }
}
