package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.model.FilterRefundSelectionItem;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class FlightRefundStatusAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<FilterRefundSelectionItem> refundList = null;
    private Typeface textFace;

    public FlightRefundStatusAdapter(Activity activity, ArrayList<FilterRefundSelectionItem> refundList) {
        this.activity = activity;
        this.refundList = refundList;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(activity)) {
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return refundList.size();
    }

    @Override
    public Object getItem(int position) {
        return refundList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.view_airline_filter, null);
        FilterRefundSelectionItem refundSelectionItem = refundList.get(position);
        TextView priceRefundStatus = (TextView) convertView.findViewById(R.id.airline_text);
        ImageView selectIcon = (ImageView) convertView.findViewById(R.id.selectIcon);
        priceRefundStatus.setTypeface(textFace);
        if (refundList.get(position).isSelected()) {
            selectIcon.setVisibility(View.VISIBLE);
        } else {
            selectIcon.setVisibility(View.GONE);
        }
        priceRefundStatus.setText(refundSelectionItem.getRefundMessage());
        return convertView;
    }
}
