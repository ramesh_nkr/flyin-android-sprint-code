package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Typeface;
import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.R;
import com.flyin.bookings.dialog.CancelBookingDialog;
import com.flyin.bookings.listeners.CancelHotelBookingListener;
import com.flyin.bookings.model.BookingsObject;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class HotelTripsAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<BookingsObject> tripArrayList;
    private LayoutInflater inflater;
    private Typeface titleFace, textFace, boldFace, lightFace;
    private CancelHotelBookingListener listener;

    public HotelTripsAdapter(Activity activity, ArrayList<BookingsObject> tripArrayList, CancelHotelBookingListener listener) {
        this.activity = activity;
        this.tripArrayList = tripArrayList;
        this.listener = listener;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_MEDIUM;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        String fontLight = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_DROIDKUFI_BOLD;
            fontLight = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        boldFace = Typeface.createFromAsset(activity.getAssets(), fontBold);
        lightFace = Typeface.createFromAsset(activity.getAssets(), fontLight);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return tripArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return tripArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.view_hotel_trips, parent, false);
        LinearLayout bookingStatusLayout = (LinearLayout) convertView.findViewById(R.id.booking_status_layout);
        ImageView hotelImage = (ImageView) convertView.findViewById(R.id.hotel_image);
        TextView hotelName = (TextView) convertView.findViewById(R.id.hotel_name);
        TextView hotelCityName = (TextView) convertView.findViewById(R.id.hotel_city_name);
        TextView checkInText = (TextView) convertView.findViewById(R.id.hotel_check_in);
        TextView checkOutText = (TextView) convertView.findViewById(R.id.hotel_check_out);
        TextView checkInTime = (TextView) convertView.findViewById(R.id.hotel_check_in_time);
        TextView checkOutTime = (TextView) convertView.findViewById(R.id.hotel_check_out_time);
        TextView checkInDate = (TextView) convertView.findViewById(R.id.hotel_check_in_date);
        TextView checkOutDate = (TextView) convertView.findViewById(R.id.hotel_check_out_date);
        TextView roomNameText = (TextView) convertView.findViewById(R.id.room_name_text);
        TextView bookedStatusText = (TextView) convertView.findViewById(R.id.booked_status_text);
        TextView referenceNumberText = (TextView) convertView.findViewById(R.id.reference_number_text);
        TextView paymentHeading = (TextView) convertView.findViewById(R.id.payment_heading);
        TextView paymentFirstDataText = (TextView) convertView.findViewById(R.id.payment_last_date_first_text);
        TextView paymentDescriptionText = (TextView) convertView.findViewById(R.id.payment_last_date_text);
        Button payNowButton = (Button) convertView.findViewById(R.id.pay_now_button);
        Button cancelBookingButton = (Button) convertView.findViewById(R.id.cancel_booking_button);
        hotelName.setTypeface(textFace);
        hotelCityName.setTypeface(titleFace);
        checkInText.setTypeface(titleFace);
        checkOutText.setTypeface(titleFace);
        checkInTime.setTypeface(titleFace);
        checkOutTime.setTypeface(titleFace);
        checkInDate.setTypeface(titleFace);
        checkOutDate.setTypeface(titleFace);
        roomNameText.setTypeface(titleFace);
        bookedStatusText.setTypeface(titleFace);
        referenceNumberText.setTypeface(lightFace);
        paymentHeading.setTypeface(titleFace);
        paymentFirstDataText.setTypeface(titleFace);
        paymentDescriptionText.setTypeface(titleFace);
        payNowButton.setTypeface(titleFace);
        cancelBookingButton.setTypeface(titleFace);
        if (Utils.isArabicLangSelected(activity)) {
            hotelName.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            hotelCityName.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            checkInText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            checkOutText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            checkInTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            checkOutTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            checkInDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            checkOutDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            roomNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            bookedStatusText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            referenceNumberText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            paymentHeading.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            paymentFirstDataText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            paymentDescriptionText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            payNowButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            cancelBookingButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                checkInTime.setTextDirection(View.TEXT_DIRECTION_RTL);
                checkOutTime.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
        }
        final BookingsObject bookingsObject = tripArrayList.get(position);
        if (bookingsObject.getEntityDetailsObjectBean().getHotelMoreInfo() != null) {
            String imagePath = bookingsObject.getEntityDetailsObjectBean().getHotelMoreInfo().getImagePath();
            if (!imagePath.equalsIgnoreCase("")) {
                String hotelImagePath = imagePath.replaceAll(" ", "%20");
                Glide.with(activity).load(hotelImagePath).placeholder(R.drawable.imagethumb_search).into(hotelImage);
                hotelImage.setScaleType(ImageView.ScaleType.FIT_XY);
            } else {
                hotelImage.setImageResource(R.drawable.imagethumb);
                hotelImage.setScaleType(ImageView.ScaleType.FIT_XY);
            }
            String checkInDateValue = Utils.convertToHotelCheckDate(bookingsObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getCheckInDate(), activity);
            String checkOutDateValue = Utils.convertToHotelCheckDate(bookingsObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getCheckOutDate(), activity);
            hotelName.setText(bookingsObject.getEntityDetailsObjectBean().getHotelMoreInfo().getName());
            hotelCityName.setText(bookingsObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getCity());
            checkInTime.setText(bookingsObject.getEntityDetailsObjectBean().getHotelMoreInfo().getCheckInTime());
            checkOutTime.setText(bookingsObject.getEntityDetailsObjectBean().getHotelMoreInfo().getCheckOutTime());
            checkInDate.setText(checkInDateValue);
            checkOutDate.setText(checkOutDateValue);
        }
        referenceNumberText.setText(activity.getResources().getString(R.string.label_hotel_ref_number));
        String tripNumber = bookingsObject.getGeneralDetailsObjectBean().getFlyinCode().substring(2);
        referenceNumberText.append(Utils.addBookingReferenceField(tripNumber, activity));
        bookedStatusText.setText(bookingsObject.getGeneralDetailsObjectBean().getBookingStatus());
        String bookingStatus = bookingsObject.getGeneralDetailsObjectBean().getBookingStatus();
        if (bookingStatus.equalsIgnoreCase("Confirmed")) {
            bookedStatusText.setTextColor(Utils.getColor(activity, R.color.confirmed_trip_color));
        } else if (bookingStatus.equalsIgnoreCase("Cancelled")) {
            bookedStatusText.setTextColor(Utils.getColor(activity, R.color.cancelled_trip_color));
        } else if (bookingStatus.equalsIgnoreCase("Booked")) {
            bookedStatusText.setTextColor(Utils.getColor(activity, R.color.hotel_status_color));
        } else {
            bookedStatusText.setTextColor(Utils.getColor(activity, R.color.other_trip_color));
        }
        if (bookingStatus.equalsIgnoreCase("Booked")) {
            bookingStatusLayout.setVisibility(View.VISIBLE);
        } else {
            bookingStatusLayout.setVisibility(View.GONE);
        }
        String roomName = bookingsObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getRoomType();
        roomNameText.setText(Utils.addRoomNameField(roomName, activity));
        if (bookingsObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getFreeCancellationDate() != null || !bookingsObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getFreeCancellationDate().equalsIgnoreCase("null"))
            paymentDescriptionText.setText(bookingsObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getFreeCancellationDate());
        cancelBookingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CancelBookingDialog cancelBookingDialog = new CancelBookingDialog(activity, bookingsObject);
                cancelBookingDialog.setCancelable(true);
                cancelBookingDialog.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (cancelBookingDialog.isCanceled) {
                            if (listener != null) {
                                listener.onSelectedItemClick(position, 0, cancelBookingDialog.selectedCancelRemark);
                            }
                        }
                    }
                });
                cancelBookingDialog.show();
                Window window = cancelBookingDialog.getWindow();
                int width = (int) (activity.getResources().getDisplayMetrics().widthPixels * 1.0);
                int height = WindowManager.LayoutParams.WRAP_CONTENT;
                if (window != null) {
                    window.setLayout(width, height);
                }
            }
        });
        payNowButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onSelectedItemClick(position, 1, "");
                }
            }
        });
        return convertView;
    }
}
