package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.model.CityModel;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class HotelSearchAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<CityModel> airportList;
    private LayoutInflater inflater;
    private Typeface titleFace, textFace;

    public HotelSearchAdapter(Activity activity, ArrayList<CityModel> airportList) {
        this.activity = activity;
        this.airportList = airportList;
        String fontTitle = Constants.FONT_ROBOTO_BOLD;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_BOLD;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return airportList.size();
    }

    @Override
    public Object getItem(int position) {
        return airportList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.view_search_hotels, null);
        TextView headerName = (TextView) convertView.findViewById(R.id.header_name);
        TextView airportCity = (TextView) convertView.findViewById(R.id.airport_name);
        if (Utils.isArabicLangSelected(activity)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                airportCity.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
            headerName.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.results_view_arabic));
            airportCity.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.results_view_arabic));
        }
        if (airportList.get(position).getType().equals("CITY")) {
            headerName.setText(R.string.label_city);
            airportCity.setCompoundDrawablesWithIntrinsicBounds(R.drawable.city, 0, 0, 0);
        } else if (airportList.get(position).getType().equals("HOTEL")) {
            headerName.setText(R.string.label_hotel);
            airportCity.setCompoundDrawablesWithIntrinsicBounds(R.drawable.bed, 0, 0, 0);
        } else {
            headerName.setText(R.string.label_airport);
            airportCity.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hotel_search_plane, 0, 0, 0);
        }
        airportCity.setCompoundDrawablePadding(20);
        if (position != 0) {
            if (airportList.get(position).getType().equals(airportList.get(position - 1).getType())) {
                headerName.setVisibility(View.GONE);
            } else
                headerName.setVisibility(View.VISIBLE);
        } else
            headerName.setVisibility(View.VISIBLE);
        airportCity.setText(airportList.get(position).getCityName() + ", " + airportList.get(position).getCountryName());
        airportCity.setTypeface(textFace);
        headerName.setTypeface(titleFace);
        return convertView;
    }
}
