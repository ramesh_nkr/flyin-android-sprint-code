package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.model.FilterAirportSelectionItem;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public class FlightAirportFilterAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<FilterAirportSelectionItem> airportList = null;
    private HashMap<String, FlightAirportName> airportFilter = null;
    private Typeface textFace, titleFace;

    public FlightAirportFilterAdapter(Activity activity, ArrayList<FilterAirportSelectionItem> airportList,
                                      HashMap<String, FlightAirportName> airportFIltermap) {
        this.activity = activity;
        this.airportList = airportList;
        this.airportFilter = airportFIltermap;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        String fontTitle = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(activity)) {
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return airportList.size();
    }

    @Override
    public Object getItem(int position) {
        return airportList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        FilterAirportSelectionItem airportSelectionItem = airportList.get(position);
        holder = new ViewHolder();
        if (!airportSelectionItem.isHeader()) {
            convertView = inflater.inflate(R.layout.view_airline_filter, null);
            holder.flightAirportName = (TextView) convertView.findViewById(R.id.airline_text);
            holder.flightSelectionIcon = (ImageView) convertView.findViewById(R.id.selectIcon);
            holder.flightAirportName.setTypeface(titleFace);
            if (airportList.get(position).isSelected()) {
                holder.flightSelectionIcon.setVisibility(View.VISIBLE);
            } else {
                holder.flightSelectionIcon.setVisibility(View.GONE);
            }
            try {
                holder.flightAirportName.setText(airportFilter.get(airportSelectionItem.getAirportCode()).getAirpotName());
            } catch (Exception e) {
            }
        }
        if (airportSelectionItem.isHeader()) {
            convertView = inflater.inflate(R.layout.view_airport_filter_header, null);
            holder.headerText = (TextView) convertView.findViewById(R.id.airport_text);
            holder.headerText.setText(airportSelectionItem.getHeaderText());
            holder.headerText.setTypeface(textFace);
        }
        return convertView;
    }

    public static class ViewHolder {
        public TextView flightAirportName, headerText;
        public ImageView flightSelectionIcon;
    }
}
