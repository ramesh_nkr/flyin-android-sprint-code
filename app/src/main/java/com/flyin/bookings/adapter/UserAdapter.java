package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.TravellerDetailsBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class UserAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<TravellerDetailsBean> listPersons;
    private LayoutInflater inflater;
    private OnCustomItemSelectListener listener;
    private Typeface titleFace, textFace;

    public UserAdapter(Activity activity, ArrayList<TravellerDetailsBean> listPersons,
                       OnCustomItemSelectListener listener) {
        this.activity = activity;
        this.listPersons = listPersons;
        this.listener = listener;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listPersons.size();
    }

    @Override
    public Object getItem(int position) {
        return listPersons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View childView, ViewGroup Parent) {
        childView = inflater.inflate(R.layout.view_useraccount, null);
        TextView personName = (TextView) childView.findViewById(R.id.user_name);
        ImageView rightArrow = (ImageView) childView.findViewById(R.id.section_btn);
        TextView personEmail = (TextView) childView.findViewById(R.id.user_email);
        Button userEditButton = (Button) childView.findViewById(R.id.user_edit_btn);
        Button userDeleteButton = (Button) childView.findViewById(R.id.user_delete_btn);
        final LinearLayout emailLayout = (LinearLayout) childView.findViewById(R.id.emailLayout);
        LinearLayout nameLayout = (LinearLayout) childView.findViewById(R.id.nameLayout);
        ImageView email_icon = (ImageView) childView.findViewById(R.id.email_icon);
        personName.setTypeface(titleFace);
        personEmail.setTypeface(titleFace);
        userEditButton.setTypeface(textFace);
        userDeleteButton.setTypeface(textFace);
        if (Utils.isArabicLangSelected(activity)) {
            rightArrow.setRotation(180);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                personName.setGravity(Gravity.START);
                personName.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
            userEditButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            userDeleteButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        personName.setText(Utils.checkStringVal(listPersons.get(position).getTitle()) + " " + Utils.checkStringVal(listPersons.get(position).getFirstName()) + " " + Utils.checkStringVal(listPersons.get(position).getLastName()));
        if (Utils.checkStringVal(listPersons.get(position).getEmail()).isEmpty()) {
            email_icon.setVisibility(View.GONE);
            personEmail.setText("");
        } else {
            email_icon.setVisibility(View.VISIBLE);
            personEmail.setText(Utils.checkStringVal(listPersons.get(position).getEmail()));
        }
        rightArrow.setImageResource(R.drawable.right_arrow);
        if (listPersons.get(position).getIsExpanded()) {
            emailLayout.setVisibility(View.VISIBLE);
            rightArrow.setImageResource(R.drawable.down_arrow);
        } else {
            emailLayout.setVisibility(View.GONE);
            rightArrow.setImageResource(R.drawable.right_arrow);
        }
        userEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSelectedItemClick(position, Constants.EDIT_BUTTON_SELECTION);
            }
        });
        userDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSelectedItemClick(position, Constants.DELETE_BUTTON_SELECTION);
            }
        });
        nameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(position);
                }
            }
        });
        return childView;
    }
}
