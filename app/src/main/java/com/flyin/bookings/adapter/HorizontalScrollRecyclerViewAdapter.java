package com.flyin.bookings.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.FlightForwardResultsActivity;
import com.flyin.bookings.R;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.FlightBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("deprecation")
public class HorizontalScrollRecyclerViewAdapter extends RecyclerView.Adapter<HorizontalScrollRecyclerViewAdapter.MyViewHolder> {
    private ArrayList<FlightBean> flightsArrayList = new ArrayList();
    private Context context;
    private LayoutInflater layoutInflater;
    private OnCustomItemSelectListener customItemListner;
    private Typeface regularFace, boldFace;
    private HashMap<String, String> airLineNamesMap = new HashMap<>();
    private Activity activity;
    private String tripType = "";

    public HorizontalScrollRecyclerViewAdapter(Context context, ArrayList<FlightBean> flightsArrayList,
                                               HashMap<String, String> airLineNamesMap, OnCustomItemSelectListener listener,
                                               String tripType) {
        this.context = context;
        this.airLineNamesMap = airLineNamesMap;
        this.flightsArrayList = flightsArrayList;
        this.customItemListner = listener;
        this.tripType = tripType;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        if (Utils.isArabicLangSelected(context)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_DROIDKUFI_BOLD;
        }
        regularFace = Typeface.createFromAsset(context.getAssets(), fontTitle);
        boldFace = Typeface.createFromAsset(context.getAssets(), fontBold);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public void onViewDetachedFromWindow(MyViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        //    holder.itemView.clearAnimation();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_oneway_trip_flights, parent, false);
        return new MyViewHolder(itemView, context);
    }

    @Override
    public void onBindViewHolder(MyViewHolder mHolder, final int position) {
        try {
            String imagePath = Constants.IMAGE_BASE_URL + flightsArrayList.get(position).getImage() + Constants.IMAGE_FILE_FORMAT;
            Glide.with(context).load(imagePath).placeholder(R.drawable.imagethumb_search).into(mHolder.airlineImage);
            SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            String currencyResult = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
            String displayPayPrice = "";
            if (tripType.equalsIgnoreCase("1")) {
                if (Utils.isArabicLangSelected(context)) {
                    String userSelectedCur = "";
                    if (currencyResult.equalsIgnoreCase("SAR")) {
                        userSelectedCur = context.getString(R.string.label_SAR_currency_name);
                    } else {
                        userSelectedCur = currencyResult;
                    }
                    displayPayPrice = (int) Math.ceil(Double.parseDouble(Utils.convertDecimalPriceToUserPrice(Double.parseDouble(flightsArrayList.get(position).getPrice()), context))) + " " + userSelectedCur;
                } else {
                    displayPayPrice = currencyResult + " " + (int) Math.ceil(Double.parseDouble(Utils.convertDecimalPriceToUserPrice(Double.parseDouble(flightsArrayList.get(position).getPrice()), context)));
                }
            } else if (tripType.equalsIgnoreCase("2")) {
                Double resultPrice = Double.parseDouble(flightsArrayList.get(position).getPrice());
                if (Utils.isArabicLangSelected(context)) {
                    String userSelectedCur = "";
                    if (currencyResult.equalsIgnoreCase("SAR")) {
                        userSelectedCur = context.getString(R.string.label_SAR_currency_name);
                    } else {
                        userSelectedCur = currencyResult;
                    }
                    displayPayPrice = (int) Math.ceil(Math.abs(resultPrice)) + " " + userSelectedCur;
                } else {
                    displayPayPrice = currencyResult + " " + (int) Math.ceil(Math.abs(resultPrice));
                }
            }
            try {
                String airlineName = airLineNamesMap.get(flightsArrayList.get(position).getImage());
                mHolder.airlinePrice.setText(displayPayPrice);
                mHolder.airlineName.setText(airlineName);
            } catch (Exception e) {
            }
            if (!flightsArrayList.get(position).isDataCleared()) {
                if (flightsArrayList.get(position).isClicked()) {
                    mHolder.crossImage.setVisibility(View.VISIBLE);
                    mHolder.airwaysLayout.setBackground(context.getResources().getDrawable(R.drawable.selected_flight_border));
                    mHolder.airlinePrice.setTextColor(Utils.getColor(context, R.color.result_compo_duration));
                } else {
                    mHolder.crossImage.setVisibility(View.GONE);
                    mHolder.airwaysLayout.setBackground(context.getResources().getDrawable(R.drawable.multi_list_view_border));
                    mHolder.airlinePrice.setTextColor(Utils.getColor(context, R.color.black_color));
                }
            } else {
                mHolder.crossImage.setVisibility(View.GONE);
                mHolder.airwaysLayout.setBackground(context.getResources().getDrawable(R.drawable.multi_list_view_border));
            }
            if (mHolder.airwaysLayout != null) {
                mHolder.airwaysLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (FlightForwardResultsActivity.isSecondServiceCalled) {
                            if (customItemListner != null) {
                                customItemListner.onItemClick(position);
                            }
                        }
                    }
                });
            }
        } catch (Exception e) {
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return flightsArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView airlineImage, crossImage;
        LinearLayout airwaysLayout;
        TextView airlinePrice, airlineName;

        @SuppressLint("NewApi")
        public MyViewHolder(View view, Context context) {
            super(view);
            airwaysLayout = (LinearLayout) view.findViewById(R.id.layout_airways_flight);
            crossImage = (ImageView) view.findViewById(R.id.cross_image);
            airlinePrice = (TextView) view.findViewById(R.id.airways_price);
            airlineName = (TextView) view.findViewById(R.id.airways_name);
            airlineImage = (ImageView) view.findViewById(R.id.airways_imageview);
            airlinePrice.setTypeface(boldFace);
            airlineName.setTypeface(regularFace);
            airlineName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            airlinePrice.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            if (Utils.isArabicLangSelected(context)) {
                airlinePrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.flight_moreDetails));
                airlineName.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.flight_moreDetails));
            }
        }
    }
}
