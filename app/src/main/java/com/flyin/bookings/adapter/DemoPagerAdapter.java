package com.flyin.bookings.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.bumptech.glide.Glide;
import com.flyin.bookings.R;
import com.flyin.bookings.util.Constants;

public class DemoPagerAdapter extends PagerAdapter {
    private final SparseArray<ImageView> mHolderArray = new SparseArray<>();
    private int mSize;
    String imageArray[];
    Context mContext;
    private ImageView image = null;
    private int imageWidth = 0;

    public DemoPagerAdapter(String imageArray[], Context mContext) {
        this.imageArray = imageArray;
        mSize = imageArray.length;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mSize;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView(mHolderArray.get(position));
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        image = new ImageView(view.getContext());
        measureImageDimension();
        String imageScaling = "";
        if (imageWidth != 0) {
            imageScaling = Constants.IMAGE_SCALING_URL + String.valueOf(imageWidth) + "x450" + Constants.IMAGE_QUALITY + "" + imageArray[position];
        } else {
            imageScaling = imageArray[position];
        }
        Glide.with(mContext).load(imageScaling).placeholder(R.drawable.imagethumb).into(image);
        image.setImageResource(R.drawable.imagethumb);
        image.setScaleType(ScaleType.CENTER_CROP);
        view.addView(image, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mHolderArray.put(position, image);
        return image;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    private void measureImageDimension() {
        ViewTreeObserver obs = image.getViewTreeObserver();
        obs.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                image.getViewTreeObserver().removeOnPreDrawListener(this);
                imageWidth = image.getMeasuredWidth();
                return true;
            }
        });
    }
}