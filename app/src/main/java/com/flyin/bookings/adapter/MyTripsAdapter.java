package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.R;
import com.flyin.bookings.model.BookingsObject;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class MyTripsAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<BookingsObject> tripArrayList;
    private LayoutInflater inflater;
    private Typeface titleFace, textFace;

    public MyTripsAdapter(Activity activity, ArrayList<BookingsObject> tripArrayList) {
        this.activity = activity;
        this.tripArrayList = tripArrayList;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return tripArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return tripArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.view_my_trips, null);
        ImageView airwayImage = (ImageView) convertView.findViewById(R.id.flight_image);
        TextView flightSource = (TextView) convertView.findViewById(R.id.source_journey_city);
        TextView flightSourceDetails = (TextView) convertView.findViewById(R.id.source_journey_details);
        TextView flightBookingStatus = (TextView) convertView.findViewById(R.id.booking_status);
        ImageView directionImage = (ImageView) convertView.findViewById(R.id.round_trip_image);
        TextView flightDestination = (TextView) convertView.findViewById(R.id.destination_journey_city);
        TextView flightDestinationDetails = (TextView) convertView.findViewById(R.id.destination_journey_details);
        ImageView flightDetailsArrow = (ImageView) convertView.findViewById(R.id.flight_details_arrow);
        flightSource.setTypeface(titleFace);
        flightSourceDetails.setTypeface(titleFace);
        flightDestination.setTypeface(titleFace);
        flightDestinationDetails.setTypeface(titleFace);
        flightBookingStatus.setTypeface(textFace);
        if (Utils.isArabicLangSelected(activity)) {
            directionImage.setRotation(180);
            flightDetailsArrow.setRotation(180);
            flightBookingStatus.setGravity(Gravity.END);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                flightBookingStatus.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            }
            flightSource.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            flightSourceDetails.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            flightBookingStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            flightDestination.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            flightDestinationDetails.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        BookingsObject bookingsObject = tripArrayList.get(position);
        int arrSize = bookingsObject.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().size();
        String imagePath = Constants.IMAGE_BASE_URL + bookingsObject.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(0).getMarketingAirline() + Constants.IMAGE_FILE_FORMAT;
        Glide.with(activity).load(imagePath).placeholder(R.drawable.imagethumb_search).into(airwayImage);
        String tempOrder = "";
        String resultOrder = "";
        String departureCityName = "";
        String arrivalCityName = "";
        String departureAirportCode = "";
        String arrivalAirportCode = "";
        int legOrder = 0;
        for (int i = 0; i < arrSize; i++) {
            tempOrder = bookingsObject.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getJourneyType();
            if (tempOrder.equalsIgnoreCase("R")) {
                resultOrder = "R";
                break;
            } else {
                resultOrder = "O";
                legOrder++;
            }
        }
        if (legOrder != 0) {
            departureCityName = bookingsObject.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(0).getDepartureAirportName();
            arrivalCityName = bookingsObject.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(legOrder - 1).getArrivalAirportName();
            departureAirportCode = bookingsObject.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(0).getDepartureAirport();
            arrivalAirportCode = bookingsObject.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(legOrder - 1).getArrivalAirport();
        }
        if (resultOrder.equalsIgnoreCase("R")) {
            directionImage.setImageResource(R.drawable.fromtoarrows);
        } else {
            directionImage.setImageResource(R.drawable.fromarrow);
        }
        flightSource.setText(departureCityName + " (" + departureAirportCode + ")");
        flightDestination.setText(arrivalCityName + " (" + arrivalAirportCode + ")");
        String bookingStatus = bookingsObject.getGeneralDetailsObjectBean().getBookingStatus();
        flightBookingStatus.setText(bookingStatus);
        if (bookingStatus.equalsIgnoreCase("Confirmed")) {
            flightBookingStatus.setTextColor(Utils.getColor(activity, R.color.confirmed_trip_color));
        } else if (bookingStatus.equalsIgnoreCase("Cancelled")) {
            flightBookingStatus.setTextColor(Utils.getColor(activity, R.color.cancelled_trip_color));
        } else {
            flightBookingStatus.setTextColor(Utils.getColor(activity, R.color.other_trip_color));
        }
        String departureTime = Utils.formatTripDateToTime(bookingsObject.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(0).getDepartureDate(), activity);
        String arrivalTime = Utils.formatTripDateToTime(bookingsObject.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(bookingsObject.getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().size() - 1).getArrivalDate(), activity);
        flightSourceDetails.setText(departureTime);
        flightDestinationDetails.setText(arrivalTime);
        return convertView;
    }
}
