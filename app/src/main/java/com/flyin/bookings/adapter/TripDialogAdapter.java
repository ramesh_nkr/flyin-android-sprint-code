package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.R;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.TripDialogBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class TripDialogAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<TripDialogBean> tripArrayList;
    private Typeface titleFace, textFace, tf, textBold;
    private OnCustomItemSelectListener listener;

    public TripDialogAdapter(Activity activity, ArrayList<TripDialogBean> tripAdvisorArray,
                             OnCustomItemSelectListener listener) {
        this.activity = activity;
        this.tripArrayList = tripAdvisorArray;
        this.listener = listener;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_MEDIUM;
        String fontStyle = Constants.FONT_ROBOTO_BOLD;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontStyle = Constants.FONT_DROIDKUFI_BOLD;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        textBold = Typeface.createFromAsset(activity.getAssets(), fontStyle);
    }

    @Override
    public int getCount() {
        return tripArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return tripArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater;
        TripDialogBean bean = tripArrayList.get(position);
        if (bean.getItemType() == TripDialogBean.RATING_HEADER) { // header layout
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.trip_traveller_header, null);
            LinearLayout hotelRatingHeaderLayout = (LinearLayout) convertView.findViewById(R.id.traveller_header_layout);
            ImageView ratingImage = (ImageView) convertView.findViewById(R.id.header_drop_image);
            TextView tripText = (TextView) convertView.findViewById(R.id.trip_header_text);
            tripText.setTypeface(titleFace);
            if (Utils.isArabicLangSelected(activity)) {
                tripText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            }
            tripText.setText(activity.getResources().getString(R.string.label_traveller_ratings));
            if (bean.isExpanded()) {
                ratingImage.setImageResource(R.drawable.top_arrow);
            } else {
                ratingImage.setImageResource(R.drawable.down_arrow);
            }
            if (hotelRatingHeaderLayout != null) {
                hotelRatingHeaderLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onItemClick(position);
                        }
                    }
                });
            }
        } else if (bean.getItemType() == TripDialogBean.RATING_ITEM) { // rating items
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.tripad_traveller_rating, null);
            ImageView ratingImages = (ImageView) convertView.findViewById(R.id.rating_image);
            TextView ratingText = (TextView) convertView.findViewById(R.id.rating_text);
            TextView overallratingText = (TextView) convertView.findViewById(R.id.overalrating_text);
            overallratingText.setTypeface(textBold);
            ratingText.setTypeface(titleFace);
            if (Utils.isArabicLangSelected(activity)) {
                overallratingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                ratingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            }
            if (Double.parseDouble(bean.getReviewsCount()) != 0) {
                overallratingText.setVisibility(View.VISIBLE);
                ratingText.setText(Utils.addTripDetailsField(bean.getReviewsCount(), activity));
                Glide.with(activity).load(bean.getRatingImage()).placeholder(R.drawable.imagethumb_search).into(ratingImages);
            } else {
                int imgWdt = activity.getResources().getDimensionPixelSize(R.dimen.search_screen_margin);
                int imgHgt = activity.getResources().getDimensionPixelSize(R.dimen.multi_trip_height);
                int imgPadding = activity.getResources().getDimensionPixelSize(R.dimen.userDetails_margin);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(imgWdt, imgHgt);
                layoutParams.setMargins(0, imgPadding, 0, 0);
                ratingImages.setLayoutParams(layoutParams);
                overallratingText.setVisibility(View.GONE);
                ratingText.setText(bean.getTitle());
                Glide.with(activity).load(bean.getRatingImage()).placeholder(R.drawable.imagethumb_search).into(ratingImages);
            }
        } else if (bean.getItemType() == TripDialogBean.REVIEW_HEADER) { // header layout
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.trip_traveller_header, null);
            LinearLayout hotelReviewHeaderLayout = (LinearLayout) convertView.findViewById(R.id.traveller_header_layout);
            ImageView reviewImage = (ImageView) convertView.findViewById(R.id.header_drop_image);
            TextView tripText = (TextView) convertView.findViewById(R.id.trip_header_text);
            tripText.setTypeface(titleFace);
            if (Utils.isArabicLangSelected(activity)) {
                tripText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            }
            tripText.setText(activity.getResources().getString(R.string.label_traveller_reviews));
            if (bean.isExpanded()) {
                reviewImage.setImageResource(R.drawable.top_arrow);
            } else {
                reviewImage.setImageResource(R.drawable.down_arrow);
            }
            if (hotelReviewHeaderLayout != null) {
                hotelReviewHeaderLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onItemClick(position);
                        }
                    }
                });
            }
        } else if (bean.getItemType() == TripDialogBean.REVIEW_ITEM) { // review items
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.trip_traveller_review, null);
            RelativeLayout hotelReviewLayout = (RelativeLayout) convertView.findViewById(R.id.hotel_review_layout);
            ImageView reviewImages = (ImageView) convertView.findViewById(R.id.review_images);
            TextView reviewHeader = (TextView) convertView.findViewById(R.id.review_text_header);
            TextView byText = (TextView) convertView.findViewById(R.id.by_text);
            TextView reviewUser = (TextView) convertView.findViewById(R.id.user_text);
            TextView reviewUserDate = (TextView) convertView.findViewById(R.id.user_text_date);
            TextView reviewText = (TextView) convertView.findViewById(R.id.review_text);
            reviewHeader.setTypeface(textBold);
            reviewUser.setTypeface(textBold);
            reviewText.setTypeface(titleFace);
            byText.setTypeface(titleFace);
            reviewUserDate.setTypeface(titleFace);
            reviewHeader.setText(bean.getTitle());
            if (Utils.isArabicLangSelected(activity)) {
                reviewHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                byText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                reviewUser.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                reviewUserDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                reviewText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            }
            if (bean.getUser() != null) {
                reviewUser.setText(Utils.adduserTripAdField(bean.getUser(), activity));
                if (Utils.isArabicLangSelected(activity)) {
                    String val = Utils.formatTimeStampToTripAdvisorDate(bean.getPublishedDate(), activity);
                    reviewUserDate.setText(activity.getString(R.string.label_on) + " " + val);
                } else {
                    reviewUserDate.setText(activity.getString(R.string.label_on) + " " + bean.getPublishedDate());
                }
            }
            reviewText.setText(bean.getReview());
            Glide.with(activity).load(bean.getRatingImage()).placeholder(R.drawable.imagethumb_search).into(reviewImages);
        }
        return convertView;
    }
}