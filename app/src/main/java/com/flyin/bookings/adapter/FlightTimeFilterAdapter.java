package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.model.FilterTimeSelectionItem;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class FlightTimeFilterAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<FilterTimeSelectionItem> timeSelectionList = null;
    private Typeface textFace, titleFace;

    public FlightTimeFilterAdapter(Activity activity, ArrayList<FilterTimeSelectionItem> timeSelectionList) {
        this.activity = activity;
        this.timeSelectionList = timeSelectionList;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        String fontTitle = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(activity)) {
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return timeSelectionList.size();
    }

    @Override
    public Object getItem(int position) {
        return timeSelectionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        FilterTimeSelectionItem timeSelectionItem = timeSelectionList.get(position);
        holder = new ViewHolder();
        if (!timeSelectionItem.isHeader()) {
            convertView = inflater.inflate(R.layout.view_time_filter_text, null);
            holder.flightDepartureText = (TextView) convertView.findViewById(R.id.time_departure_text);
            holder.flightDepartureTime = (TextView) convertView.findViewById(R.id.time_departure_time);
            holder.flightSelectionIcon = (ImageView) convertView.findViewById(R.id.departure_selection);
            if (timeSelectionList.get(position).isSelected()) {
                holder.flightSelectionIcon.setVisibility(View.VISIBLE);
            } else {
                holder.flightSelectionIcon.setVisibility(View.GONE);
            }
            holder.flightDepartureText.setText(timeSelectionItem.getDepartureText());
            holder.flightDepartureTime.setText(timeSelectionItem.getDepartureTime());
            holder.flightDepartureText.setTypeface(titleFace);
            holder.flightDepartureTime.setTypeface(titleFace);
        }
        if (timeSelectionItem.isHeader()) {
            convertView = inflater.inflate(R.layout.view_airport_filter_header, null);
            holder.headerText = (TextView) convertView.findViewById(R.id.airport_text);
            holder.headerText.setText(timeSelectionItem.getHeaderText());
            holder.headerText.setTypeface(textFace);
        }
        return convertView;
    }

    public static class ViewHolder {
        public TextView flightDepartureText, flightDepartureTime, headerText;
        public ImageView flightSelectionIcon;
    }
}
