package com.flyin.bookings.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.R;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.LoyaltyBookingInfoBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class RewardsStatementAdapter extends RecyclerView.Adapter<RewardsStatementAdapter.MyViewHolder> {
    private ArrayList<LoyaltyBookingInfoBean> rewardsStatementBeanArrayList = new ArrayList();
    private Activity activity;
    private LayoutInflater layoutInflater;
    private OnCustomItemSelectListener customItemListner;
    private Typeface regularFace, boldFace, mediumFace;
    private static final String TAG = "RewardsStatementAdapter";

    public RewardsStatementAdapter(Activity activity, ArrayList<LoyaltyBookingInfoBean> rewardsStatementBeanArrayList,
                                   OnCustomItemSelectListener listener) {
        this.activity = activity;
        this.rewardsStatementBeanArrayList = rewardsStatementBeanArrayList;
        this.customItemListner = listener;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        String fontMedium = Constants.FONT_ROBOTO_MEDIUM;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_DROIDKUFI_BOLD;
            fontMedium = Constants.FONT_DROIDKUFI_REGULAR;
        }
        regularFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        boldFace = Typeface.createFromAsset(activity.getAssets(), fontBold);
        mediumFace = Typeface.createFromAsset(activity.getAssets(), fontMedium);
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rewards_mystatement_fragment, parent, false);
        return new MyViewHolder(itemView, activity);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {
            final LoyaltyBookingInfoBean loyaltyBookingInfoBean = rewardsStatementBeanArrayList.get(position);
            if (loyaltyBookingInfoBean.getProduct().equalsIgnoreCase("flight")) {
                holder.headerText.setText(activity.getString(R.string.label_flight_booking));
                holder.headerImage.setImageResource(R.drawable.flightnormal);
                if (Utils.isArabicLangSelected(activity)) {
                    holder.headerImage.setRotation(270);
                }
            } else if (loyaltyBookingInfoBean.getProduct().equalsIgnoreCase("hotel")) {
                holder.headerText.setText(activity.getString(R.string.label_hotel_booking));
                holder.headerImage.setImageResource(R.drawable.loyalty_hotelicon);
                if (Utils.isArabicLangSelected(activity)) {
                    holder.headerImage.setRotation(0);
                }
            } else if (loyaltyBookingInfoBean.getProduct().equalsIgnoreCase("other")) {
                holder.headerText.setText(activity.getString(R.string.label_gift_points));
                holder.headerImage.setImageResource(R.drawable.reward_white);
                if (Utils.isArabicLangSelected(activity)) {
                    holder.headerImage.setRotation(0);
                }
            } else if (loyaltyBookingInfoBean.getProduct().equalsIgnoreCase("fph")) {
                holder.headerText.setText(activity.getString(R.string.label_package_booking));
                holder.headerImage.setImageResource(R.drawable.package_icon);
                if (Utils.isArabicLangSelected(activity)) {
                    holder.headerImage.setRotation(0);
                }
            } else {
                holder.parentViewLayout.setVisibility(View.GONE);
            }
            if (loyaltyBookingInfoBean.getProductCode().equalsIgnoreCase("1")) {
                if (Utils.isArabicLangSelected(activity)) {
                    holder.flightJourneyIcon.setRotation(180);
                }
                String imagePath = Constants.IMAGE_BASE_URL + loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getAirline() + Constants.IMAGE_FILE_FORMAT;
                Glide.with(activity).load(imagePath).placeholder(R.drawable.imagethumb_search).into(holder.flightImage);
                holder.flightSourceTextView.setText(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getDepartureAirportName() + "(" + loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getDepartureAirport() + ")");
                holder.flightDestinationTextView.setText(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getArrivalAirportName() + "(" + loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getArrivalAirport() + ")");
                holder.flightSourceDatetimeText.setText(Utils.formatDateToUserDateFormat(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getDepartureDate(), activity));
                holder.flightDestinationDateTimeText.setText(Utils.formatDateToUserDateFormat(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getArrivalDate(), activity));
                holder.flightIdText.setText(loyaltyBookingInfoBean.getBookingCode().substring(2));
            } else if (loyaltyBookingInfoBean.getProductCode().equalsIgnoreCase("2")) {
                if (loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getImage() != null) {
                    String imagePath = loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getImage();
                    Glide.with(activity).load(imagePath).placeholder(R.drawable.imagethumb_search).into(holder.hotelImage);
                }
                if (loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getHotelName() != null ||
                        !loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getHotelName().isEmpty()) {
                    holder.hotelNameTextView.setText(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getHotelName());
                }
                holder.hotelFlyinIdText.setText(loyaltyBookingInfoBean.getBookingCode().substring(2));
                DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                Date date = null;
                try {
                    date = originalFormat.parse(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCheckIn());
                    holder.hotelCheckInDateTime.setText(" : " + targetFormat.format(date));
                    date = originalFormat.parse(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCheckOut());
                    holder.hotelCheckOutDateTime.setText(" : " + targetFormat.format(date));
                } catch (ParseException e) {
                }
                if (!loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCityName().isEmpty() ||
                        loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCityName() != null) {
                    holder.hotelAddressTextview.setText(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCityName() + ", " + loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCountryName());
                }
            } else if (loyaltyBookingInfoBean.getProductCode().equalsIgnoreCase("3")) {
                if (!loyaltyBookingInfoBean.getDescription().isEmpty()) {
                    holder.giftRewardText.setText(loyaltyBookingInfoBean.getDescription());
                }
            } else if (loyaltyBookingInfoBean.getProductCode().equalsIgnoreCase("4")) {
                if (loyaltyBookingInfoBean.getLoyaltyBookingDataBean() != null) {
                    if (!loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getUniqueId().isEmpty()) {
                        if (Utils.isArabicLangSelected(activity)) {
                            holder.flightJourneyIcon.setRotation(180);
                        }
                        String imagePath = Constants.IMAGE_BASE_URL + loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getAirline() + Constants.IMAGE_FILE_FORMAT;
                        Glide.with(activity).load(imagePath).placeholder(R.drawable.imagethumb_search).into(holder.flightImage);
                        holder.flightSourceTextView.setText(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getDepartureAirportName() + "(" + loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getDepartureAirport() + ")");
                        holder.flightDestinationTextView.setText(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getArrivalAirportName() + "(" + loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getArrivalAirport() + ")");
                        holder.flightSourceDatetimeText.setText(Utils.formatDateToUserDateFormat(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getDepartureDate(), activity));
                        holder.flightDestinationDateTimeText.setText(Utils.formatDateToUserDateFormat(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getArrivalDate(), activity));
                        holder.flightIdText.setText(loyaltyBookingInfoBean.getBookingCode().substring(2));
                        if (loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getImage() != null) {
                            String hotelImagePath = loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getImage();
                            Glide.with(activity).load(hotelImagePath).placeholder(R.drawable.imagethumb_search).into(holder.hotelImage);
                        }
                        if (loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getHotelName() != null ||
                                !loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getHotelName().isEmpty()) {
                            holder.hotelNameTextView.setText(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getHotelName());
                        }
                        holder.hotelFlyinIdText.setText(loyaltyBookingInfoBean.getBookingCode().substring(2));
                        DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                        DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                        Date date = null;
                        try {
                            date = originalFormat.parse(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCheckIn());
                            holder.hotelCheckInDateTime.setText(" : " + targetFormat.format(date));
                            date = originalFormat.parse(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCheckOut());
                            holder.hotelCheckOutDateTime.setText(" : " + targetFormat.format(date));
                        } catch (ParseException e) {
                        }
                        if (!loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCityName().isEmpty() ||
                                loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCityName() != null) {
                            holder.hotelAddressTextview.setText(loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCityName() + ", " + loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getCountryName());
                        }
                    }
                }
            }
            holder.headerDateTime.setText(Utils.convertRewardTimetoActualFormat(loyaltyBookingInfoBean.getDate(), activity));
            if (loyaltyBookingInfoBean.getTransactionType().equalsIgnoreCase("burn")) {
                holder.headerPointsText.setText("-" + String.valueOf(loyaltyBookingInfoBean.getPoints()));
                holder.headerPointsText.setTextColor(Utils.getColor(activity, R.color.terms_text));
            } else {
                holder.headerPointsText.setText("+" + String.valueOf(loyaltyBookingInfoBean.getPoints()));
                holder.headerPointsText.setTextColor(Utils.getColor(activity, R.color.rewards_Point));
            }
            if (loyaltyBookingInfoBean.isExpanded()) {
                if (loyaltyBookingInfoBean.getProductCode().equalsIgnoreCase("1")) {
                    holder.flightLayout.setVisibility(View.VISIBLE);
                    holder.hotelLayout.setVisibility(View.GONE);
                    holder.giftLayout.setVisibility(View.GONE);
                    if (holder.flightLayout.getAlpha() == 0) {
                        holder.flightLayout.setTranslationY(-60);
                        holder.flightLayout.animate().translationY(0).alpha(1)
                                .setDuration(activity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                    }
                } else if (loyaltyBookingInfoBean.getProductCode().equalsIgnoreCase("2")) {
                    holder.hotelLayout.setVisibility(View.VISIBLE);
                    holder.flightLayout.setVisibility(View.GONE);
                    holder.giftLayout.setVisibility(View.GONE);
                    if (holder.hotelLayout.getAlpha() == 0) {
                        holder.hotelLayout.setTranslationY(-60);
                        holder.hotelLayout.animate().translationY(0).alpha(1)
                                .setDuration(activity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                    }
                } else if (loyaltyBookingInfoBean.getProductCode().equalsIgnoreCase("3")) {
                    if (!loyaltyBookingInfoBean.getDescription().isEmpty()) {
                        holder.giftLayout.setVisibility(View.VISIBLE);
                        holder.flightLayout.setVisibility(View.GONE);
                        holder.hotelLayout.setVisibility(View.GONE);
                        if (holder.giftLayout.getAlpha() == 0) {
                            holder.giftLayout.setTranslationY(-60);
                            holder.giftLayout.animate().translationY(0).alpha(1)
                                    .setDuration(activity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                        }
                    }
                } else if (loyaltyBookingInfoBean.getProductCode().equalsIgnoreCase("4")) {
                    if (!loyaltyBookingInfoBean.getLoyaltyBookingDataBean().getUniqueId().isEmpty()) {
                        holder.flightLayout.setVisibility(View.VISIBLE);
                        holder.hotelLayout.setVisibility(View.VISIBLE);
                        if (holder.flightLayout.getAlpha() == 0) {
                            holder.flightLayout.setTranslationY(-60);
                            holder.flightLayout.animate().translationY(0).alpha(1)
                                    .setDuration(activity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                        }
                        if (holder.hotelLayout.getAlpha() == 0) {
                            holder.hotelLayout.setTranslationY(-60);
                            holder.hotelLayout.animate().translationY(0).alpha(1)
                                    .setDuration(activity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                        }
                    }
                    holder.giftLayout.setVisibility(View.GONE);
                } else {
                    holder.flightLayout.setVisibility(View.GONE);
                    holder.hotelLayout.setVisibility(View.GONE);
                    holder.giftLayout.setVisibility(View.GONE);
                    holder.flightLayout.animate().alpha(0)
                            .setDuration(activity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                    holder.hotelLayout.animate().alpha(0)
                            .setDuration(activity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                    holder.giftLayout.animate().alpha(0)
                            .setDuration(activity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                }
            } else {
                holder.flightLayout.setVisibility(View.GONE);
                holder.hotelLayout.setVisibility(View.GONE);
                holder.giftLayout.setVisibility(View.GONE);
                holder.flightLayout.animate().alpha(0)
                        .setDuration(activity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                holder.hotelLayout.animate().alpha(0)
                        .setDuration(activity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                holder.giftLayout.animate().alpha(0)
                        .setDuration(activity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
            }
            holder.headerLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (customItemListner != null) {
                        customItemListner.onItemClick(position);
                    }
                }
            });
        } catch (Exception e) {
            Utils.printMessage(TAG, "Err Msg : " + e.getMessage());
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return rewardsStatementBeanArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView headerImage, flightImage, hotelImage, flightJourneyIcon;
        LinearLayout flightLayout, hotelLayout, headerLayout, parentViewLayout, giftLayout;
        TextView headerText, headerDateTime, headerPointsTextView, headerPointsText, flightSourceTextView,
                flightDestinationTextView, flightIdTextView, flightSourceDatetimeText, flightDestinationDateTimeText,
                flightIdText, hotelNameTextView, hotelAddressTextview, hotelFlyinIdTextView, hotelFlyinIdText,
                hotelCheckInDateTimeTextView, hotelCheckOutDateTimeTextView, hotelCheckInDateTime,
                hotelCheckOutDateTime, giftRewardText;

        public MyViewHolder(View view, Activity activity) {
            super(view);
            headerImage = (ImageView) view.findViewById(R.id.rewards_header_image);
            flightImage = (ImageView) view.findViewById(R.id.rewards_flight_imageview);
            hotelImage = (ImageView) view.findViewById(R.id.rewards_hotel_imageview);
            headerLayout = (LinearLayout) view.findViewById(R.id.header_layout);
            flightLayout = (LinearLayout) view.findViewById(R.id.rewards_flight_layout);
            hotelLayout = (LinearLayout) view.findViewById(R.id.rewards_hotel_layout);
            giftLayout = (LinearLayout) view.findViewById(R.id.rewards_gift_layout);
            headerText = (TextView) view.findViewById(R.id.rewards_header_textview);
            headerDateTime = (TextView) view.findViewById(R.id.rewards_header_datetime_textview);
            headerPointsTextView = (TextView) view.findViewById(R.id.rewards_points_textview);
            headerPointsText = (TextView) view.findViewById(R.id.rewards_points_text);
            flightSourceTextView = (TextView) view.findViewById(R.id.rewards_flight_src_textview);
            flightSourceDatetimeText = (TextView) view.findViewById(R.id.rewards_flight_src_datetime_textview);
            flightDestinationTextView = (TextView) view.findViewById(R.id.rewards_flight_dest_textview);
            flightDestinationDateTimeText = (TextView) view.findViewById(R.id.rewards_flight_dest_datetime_textview);
            flightIdTextView = (TextView) view.findViewById(R.id.rewards_flight_id_textview);
            flightIdText = (TextView) view.findViewById(R.id.rewards_flight_id_code_textview);
            hotelNameTextView = (TextView) view.findViewById(R.id.rewards_hotel_textview);
            hotelAddressTextview = (TextView) view.findViewById(R.id.rewards_hotel_address_textview);
            hotelFlyinIdTextView = (TextView) view.findViewById(R.id.rewards_hotel_id_textview);
            hotelFlyinIdText = (TextView) view.findViewById(R.id.rewards_hotel_id_code_textview);
            hotelCheckInDateTimeTextView = (TextView) view.findViewById(R.id.rewards_hotel_check_in_textview);
            hotelCheckOutDateTimeTextView = (TextView) view.findViewById(R.id.rewards_hotel_check_out_textview);
            hotelCheckInDateTime = (TextView) view.findViewById(R.id.rewards_hotel_check_in_text);
            hotelCheckOutDateTime = (TextView) view.findViewById(R.id.rewards_hotel_check_out_text);
            flightJourneyIcon = (ImageView) view.findViewById(R.id.flight_journey_icon);
            parentViewLayout = (LinearLayout) view.findViewById(R.id.parent_view_layout);
            giftRewardText = (TextView) view.findViewById(R.id.gift_reward_text);
            if (Utils.isArabicLangSelected(activity)) {
                headerText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                headerDateTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                headerPointsTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
                headerPointsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.contact_text));
                flightSourceTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.results_view_arabic));
                flightDestinationTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.results_view_arabic));
                flightSourceDatetimeText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                flightDestinationDateTimeText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                flightIdTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.results_view_arabic));
                flightIdText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                hotelNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
                hotelAddressTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                hotelFlyinIdTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
                hotelFlyinIdText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                hotelCheckInDateTimeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                hotelCheckOutDateTimeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                hotelCheckInDateTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                hotelCheckOutDateTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
                giftRewardText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            }
            headerText.setTypeface(boldFace);
            headerDateTime.setTypeface(regularFace);
            headerPointsTextView.setTypeface(regularFace);
            headerPointsText.setTypeface(mediumFace);
            flightSourceTextView.setTypeface(regularFace);
            flightDestinationTextView.setTypeface(regularFace);
            flightSourceDatetimeText.setTypeface(regularFace);
            flightDestinationDateTimeText.setTypeface(regularFace);
            flightIdTextView.setTypeface(regularFace);
            flightIdText.setTypeface(regularFace);
            hotelNameTextView.setTypeface(regularFace);
            hotelAddressTextview.setTypeface(regularFace);
            hotelFlyinIdTextView.setTypeface(regularFace);
            hotelFlyinIdText.setTypeface(regularFace);
            hotelCheckInDateTimeTextView.setTypeface(regularFace);
            hotelCheckOutDateTimeTextView.setTypeface(regularFace);
            hotelCheckInDateTime.setTypeface(regularFace);
            hotelCheckOutDateTime.setTypeface(regularFace);
            giftRewardText.setTypeface(regularFace);
        }
    }
}
