package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.R;
import com.flyin.bookings.dialog.ReturnFlightReviewDialog;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.FSDataBean;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.OriginDestinationOptionBean;
import com.flyin.bookings.model.PricedItineraryObject;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

@SuppressWarnings("deprecation")
public class FlightRoundTripAdapter extends BaseAdapter {
    private Activity activity;
    private Typeface titleFace, textFace;
    private ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList;
    private HashMap<String, FlightAirportName> airportMap;
    private HashMap<String, String> airlineMap;
    private Context context;
    private String travellerCount;
    private String sourceName, returnName;
    private OnCustomItemSelectListener listener;
    private String tripType;
    private LayoutInflater inflater = null;
    private String selectedClassType = "";
    private String totalPackagePrice = "";
    private String returnFlightPrice = "";
    public static final String TAG = "FlightRoundTripAdapter";

    public FlightRoundTripAdapter(Context context, Activity activity, ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList,
                                  HashMap<String, FlightAirportName> airportNamesMap, HashMap<String, String> airLineNamesMap,
                                  String traveller, String sourceName, String returnName, OnCustomItemSelectListener listener, String tripType,
                                  String selectedClassType, String totalPackagePrice, String returnFlightPrice) {
        this.activity = activity;
        this.context = context;
        this.pricedItineraryObjectArrayList = pricedItineraryObjectArrayList;
        this.airportMap = airportNamesMap;
        this.airlineMap = airLineNamesMap;
        this.travellerCount = traveller;
        this.sourceName = sourceName;
        this.returnName = returnName;
        this.listener = listener;
        this.tripType = tripType;
        this.selectedClassType = selectedClassType;
        this.totalPackagePrice = totalPackagePrice;
        this.returnFlightPrice = returnFlightPrice;
        String fontText = Constants.FONT_ROBOTO_BOLD;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(activity)) {
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pricedItineraryObjectArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return pricedItineraryObjectArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.oneway_flights_row, parent, false);
            holder = new ViewHolder();
            holder.airwaysTextView = (TextView) view.findViewById(R.id.airways_text);
            holder.airwaysCodeTextview = (TextView) view.findViewById(R.id.airways_code);
            holder.flydubaiText = (TextView) view.findViewById(R.id.flydubai_text);
            holder.flydubaitextbrace = (TextView) view.findViewById(R.id.flydubai_text_brace);
            holder.stopTextview = (TextView) view.findViewById(R.id.text_stops);
            holder.travellerCountTextview = (TextView) view.findViewById(R.id.traveller_count_text);
            holder.sourceCodeTextview = (TextView) view.findViewById(R.id.source_code);
            holder.sourceNameTextview = (TextView) view.findViewById(R.id.source_from);
            holder.destinationCodeTextview = (TextView) view.findViewById(R.id.destination_code);
            holder.destinationNameTextview = (TextView) view.findViewById(R.id.destination_name);
            holder.currencyTextview = (TextView) view.findViewById(R.id.currency_text);
            holder.priceTextview = (TextView) view.findViewById(R.id.price_text);
            holder.sourceTimeTextView = (TextView) view.findViewById(R.id.source_time);
            holder.destinationTimeTextview = (TextView) view.findViewById(R.id.destination_time);
            holder.refundTextview = (TextView) view.findViewById(R.id.refund_type_text);
            holder.layoverTimeTextview = (TextView) view.findViewById(R.id.layover_time_text);
            holder.priceDecimalText = (TextView) view.findViewById(R.id.price_decimal_text);
            holder.image = (ImageView) view.findViewById(R.id.flight_image);
            holder.stopsLayout = (RelativeLayout) view.findViewById(R.id.stops_layout);
            holder.stopsViewLayout = (RelativeLayout) view.findViewById(R.id.stops_view_layout);
            holder.stopNamesLayout = (LinearLayout) view.findViewById(R.id.stops_names_layout);
            holder.stopNameViewLayout = (RelativeLayout) view.findViewById(R.id.stops_name_view_layout);
            holder.stopTimeLayout = (RelativeLayout) view.findViewById(R.id.time_view_layout);
            holder.stopTimeViewLayout = (RelativeLayout) view.findViewById(R.id.stops_time_view_layout);
            holder.conatainer = (LinearLayout) view.findViewById(R.id.conatainer);
            holder.amenitiesLayout = (LinearLayout) view.findViewById(R.id.default_amenities_layout);
            holder.amenitiesView = (View) view.findViewById(R.id.amenities_view);
            holder.firstImage = (ImageView) view.findViewById(R.id.first_icon);
            holder.layoutText = (TextView) view.findViewById(R.id.layout_text);
            holder.freshMealText = (TextView) view.findViewById(R.id.fresh_meal_text);
            holder.wifiText = (TextView) view.findViewById(R.id.wifi_text);
            holder.avodText = (TextView) view.findViewById(R.id.avod_text);
            holder.layoutIcon = (ImageView) view.findViewById(R.id.layout_icon);
            holder.freshMealIcon = (ImageView) view.findViewById(R.id.fresh_meal_icon);
            holder.wifiIcon = (ImageView) view.findViewById(R.id.wifi_icon);
            holder.avodIcon = (ImageView) view.findViewById(R.id.avod_icon);
            holder.tripTypeTextView = (TextView) view.findViewById(R.id.trip_type_text);
            holder.classTypeTextView = (TextView) view.findViewById(R.id.class_type);
            holder.stopsEmptyView = (View) view.findViewById(R.id.stops_name_view);
            holder.forwardBusIcon = (ImageView) view.findViewById(R.id.forward_bus_image);
            holder.priceDetailsLayout = (LinearLayout) view.findViewById(R.id.layout_price_detail);
            holder.flightInfoLayout = (RelativeLayout) view.findViewById(R.id.flight_info_layout);
            holder.flightTripLayout = (LinearLayout) view.findViewById(R.id.flight_trip_layout);
            holder.flightImageLayout = (RelativeLayout) view.findViewById(R.id.circle_layout);
            String fontText = Constants.FONT_ROBOTO_LIGHT;
            String fontTitle = Constants.FONT_ROBOTO_REGULAR;
            String fontPath = Constants.FONT_ROBOTO_MEDIUM;
            String fontMedium = Constants.FONT_ROBOTO_BOLD;
            if (Utils.isArabicLangSelected(context)) {
                fontText = Constants.FONT_DROIDKUFI_REGULAR;
                fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
                fontPath = Constants.FONT_DROIDKUFI_REGULAR;
                fontMedium = Constants.FONT_DROIDKUFI_BOLD;
                holder.airwaysTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                holder.airwaysCodeTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                holder.stopTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                holder.flydubaiText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                holder.flydubaitextbrace.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));

                holder.travellerCountTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                holder.layoverTimeTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                holder.sourceNameTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                holder.sourceCodeTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                holder.destinationNameTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                holder.destinationCodeTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                holder.currencyTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                holder.priceTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
                holder.priceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                holder.sourceTimeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                holder.destinationTimeTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                holder.refundTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                holder.layoutText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                holder.freshMealText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                holder.wifiText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                holder.avodText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                holder.classTypeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                holder.tripTypeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                holder.sourceCodeTextview.setGravity(Gravity.START);
                holder.destinationCodeTextview.setGravity(Gravity.END);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    holder.sourceCodeTextview.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                    holder.destinationCodeTextview.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                }
            }
            Typeface lightFace = Typeface.createFromAsset(context.getAssets(), fontText);
            Typeface regularFace = Typeface.createFromAsset(context.getAssets(), fontTitle);
            Typeface mediumFace = Typeface.createFromAsset(context.getAssets(), fontPath);
            Typeface boldFace = Typeface.createFromAsset(context.getAssets(), fontMedium);
            holder.airwaysTextView.setTypeface(mediumFace);
            holder.flydubaiText.setTypeface(mediumFace);
            holder.flydubaitextbrace.setTypeface(mediumFace);
            holder.airwaysCodeTextview.setTypeface(regularFace);
            holder.stopTextview.setTypeface(regularFace);
            holder.travellerCountTextview.setTypeface(regularFace);
            holder.sourceCodeTextview.setTypeface(regularFace);
            holder.sourceNameTextview.setTypeface(regularFace);
            holder.destinationCodeTextview.setTypeface(regularFace);
            holder.destinationNameTextview.setTypeface(regularFace);
            holder.currencyTextview.setTypeface(regularFace);
            holder.priceTextview.setTypeface(boldFace);
            holder.priceDecimalText.setTypeface(regularFace);
            holder.sourceTimeTextView.setTypeface(regularFace);
            holder.destinationTimeTextview.setTypeface(regularFace);
            holder.refundTextview.setTypeface(regularFace);
            holder.layoverTimeTextview.setTypeface(regularFace);
            holder.layoutText.setTypeface(regularFace);
            holder.freshMealText.setTypeface(regularFace);
            holder.wifiText.setTypeface(regularFace);
            holder.avodText.setTypeface(regularFace);
            holder.classTypeTextView.setTypeface(regularFace);
            holder.tripTypeTextView.setTypeface(regularFace);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        try {
            final PricedItineraryObject pricedItineraryObject = pricedItineraryObjectArrayList.get(position);
            final OriginDestinationOptionBean odoObject = pricedItineraryObject.getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
            if (odoObject.getRefNum().equalsIgnoreCase("2")) {
                holder.airwaysTextView.setText(airlineMap.get(odoObject.getFsDataBeanArrayList().get(0).getMal()));
                try {
                    if (!odoObject.getCb().isEmpty() && odoObject.getFsDataBeanArrayList().get(0).getMal().equalsIgnoreCase("FZ")) {
                        String baggage = "";
                        if (odoObject.getCb().toLowerCase().contains("kgs")) {
                            baggage = odoObject.getCb().toLowerCase().replaceAll("kgs", activity.getString(R.string.label_kilo));
                        } else if (odoObject.getCb().toLowerCase().contains("kilos")) {
                            baggage = odoObject.getCb().toLowerCase().replaceAll("kilos", activity.getString(R.string.label_kilo));
                        } else if (odoObject.getCb().toLowerCase().contains("piece")) {
                            baggage = odoObject.getCb().toLowerCase().replaceAll("piece", activity.getString(R.string.label_pieces));
                        }
                        holder.flydubaiText.setVisibility(View.VISIBLE);
                        holder.flydubaiText.setText("(" + baggage);
                        holder.flydubaitextbrace.setText(")");
                        holder.flydubaitextbrace.setVisibility(View.VISIBLE);
                    } else {
                        holder.flydubaiText.setVisibility(View.GONE);
                        holder.flydubaitextbrace.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String airwaysCode = odoObject.getFsDataBeanArrayList().get(0).getMal() + " " + odoObject.getFsDataBeanArrayList().get(0).getFn();
                holder.airwaysCodeTextview.setText(String.format(activity.getResources().getString(R.string.label_airways_code_view), airwaysCode));
                holder.sourceCodeTextview.setText(odoObject.getFsDataBeanArrayList().get(0).getDap());
                String dateDiff = "";
                try {
                    dateDiff = Utils.dayDiff(odoObject.getFsDataBeanArrayList().get(0).getDdt(), odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getArdt());
                } catch (Exception e) {
                }
                if (odoObject.getFsDataBeanArrayList().get(0).getEq().equalsIgnoreCase("BUS")) {
                    holder.forwardBusIcon.setVisibility(View.VISIBLE);
                } else {
                    holder.forwardBusIcon.setVisibility(View.GONE);
                }
                holder.sourceNameTextview.setText(airportMap.get(odoObject.getFsDataBeanArrayList().get(0).getDap()).getCityName());
                holder.destinationCodeTextview.setText(odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getAap());
                holder.destinationNameTextview.setText(airportMap.get(odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getAap()).getCityName());
                holder.sourceTimeTextView.setText(Utils.formatDateToTime(odoObject.getFsDataBeanArrayList().get(0).getDdt(), activity));
                if (dateDiff.isEmpty()) {
                    holder.destinationTimeTextview.setText(Utils.formatDateToTime(odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getArdt(), activity));
                } else {
                    holder.destinationTimeTextview.setText(Utils.formatDateToTime(odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getArdt(), activity));
                    holder.destinationTimeTextview.append(Utils.addFlightGreyField(dateDiff, activity));
                }
                if (pricedItineraryObject.getRefundablestate()) {
                    holder.refundTextview.setTextColor(Utils.getColor(context, R.color.refund_text_color));
                    holder.refundTextview.setText(context.getString(R.string.RefLbl));
                } else {
                    holder.refundTextview.setTextColor(Utils.getColor(context, R.color.non_refund_color));
                    holder.refundTextview.setText(context.getString(R.string.NonRefLbl));
                }
                String imagePath = Constants.IMAGE_BASE_URL + odoObject.getFsDataBeanArrayList().get(0).getMal() + Constants.IMAGE_FILE_FORMAT;
                Glide.with(activity).load(imagePath).placeholder(R.drawable.imagethumb_search).into(holder.image);
                if (tripType.equalsIgnoreCase("1")) {
                    holder.tripTypeTextView.setVisibility(View.GONE);
                } else if (tripType.equalsIgnoreCase("2")) {
                    holder.tripTypeTextView.setText(activity.getResources().getString(R.string.label_search_roundtrip));
                    holder.tripTypeTextView.setVisibility(View.VISIBLE);
                }
                if (selectedClassType.equalsIgnoreCase("Economy")) {
                    holder.classTypeTextView.setText(activity.getResources().getString(R.string.label_search_flight_economy));
                } else if (selectedClassType.equalsIgnoreCase("Business")) {
                    holder.classTypeTextView.setText(activity.getResources().getString(R.string.label_search_flight_business));
                } else if (selectedClassType.equalsIgnoreCase("First")) {
                    holder.classTypeTextView.setText(activity.getResources().getString(R.string.label_search_flight_first));
                } else {
                    holder.classTypeTextView.setText(activity.getResources().getString(R.string.label_search_flight_economy));
                }
                if (Integer.parseInt(travellerCount) == 1) {
                    holder.travellerCountTextview.setText(travellerCount + " " + activity.getResources().getString(R.string.label_add_traveller));
                } else {
                    holder.travellerCountTextview.setText(travellerCount + " " + activity.getResources().getString(R.string.label_search_flight_travellers));
                }
                holder.layoverTimeTextview.setText(Utils.formatDurationToString(odoObject.getFsDataBeanArrayList().get(0).getDur(), activity));
                Double resultPrice = Double.parseDouble(pricedItineraryObject.getAipiBean().getItfObject().getPriceDiff());
                String returnPayPrice = String.format(Locale.ENGLISH, "%.2f", Math.abs(resultPrice));
                String priceResult[] = returnPayPrice.split("\\.");
                SharedPreferences pref = activity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                String currencyResult = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
                if (Utils.isArabicLangSelected(activity)) {
                    holder.currencyTextview.setTypeface(titleFace);
                    holder.priceTextview.setTypeface(textFace);
                    holder.priceDecimalText.setTypeface(titleFace);
                    holder.currencyTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                    holder.priceTextview.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.text_size));
                    holder.priceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                    holder.priceTextview.setText("." + priceResult[0] + " ");
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new RecyclerView.LayoutParams(
                            RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));
                    params.setMargins(0, 0, 0, 0);
                    holder.currencyTextview.setLayoutParams(params);
                    holder.currencyTextview.setText(priceResult[1]);
                    if (currencyResult.equalsIgnoreCase("SAR")) {
                        holder.priceDecimalText.setText(activity.getString(R.string.label_SAR_currency_name));
                    } else {
                        holder.priceDecimalText.setText(currencyResult);
                    }
                } else {
                    holder.currencyTextview.setText(currencyResult);
                    holder.priceTextview.setText(priceResult[0] + ".");
                    holder.priceDecimalText.setText(priceResult[1]);
                }
                int stopCount = 0;
                try {
                    stopCount = Integer.parseInt(odoObject.getFsDataBeanArrayList().get(0).getSq());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (stopCount == 0) {
                    holder.stopTextview.setText(activity.getString(R.string.label_non_stop_text));
                } else if (stopCount == 1) {
                    holder.stopTextview.setText(stopCount + " " + activity.getString(R.string.label_stop_text));
                } else {
                    holder.stopTextview.setText(stopCount + " " + activity.getString(R.string.label_stops_text));
                }
                Display device = ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
                int deviceWidth = device.getWidth();
                int priceViewWidth = 0;
                if (holder.currencyTextview.getWidth() != 0 && holder.priceTextview.getWidth() != 0 && holder.priceDecimalText.getWidth() != 0) {
                    priceViewWidth = holder.currencyTextview.getWidth() + holder.priceTextview.getWidth() +
                            holder.priceDecimalText.getWidth() + activity.getResources().getDimensionPixelSize(R.dimen.padding_size);
                } else {
                    priceViewWidth = FlightOneWayTripAdapter.priceViewWidthVal;
                }
                int flightImageViewWidth = activity.getResources().getDimensionPixelSize(R.dimen.result_combo_flag);
                int flightInfoViewWidth = deviceWidth - (flightImageViewWidth + priceViewWidth + activity.getResources().getDimensionPixelSize(R.dimen.margin_size));
                RelativeLayout.LayoutParams priceParams = new RelativeLayout.LayoutParams(priceViewWidth, RelativeLayout.LayoutParams.WRAP_CONTENT);
                int layoutRightMgn = activity.getResources().getDimensionPixelSize(R.dimen.userDetails_margin);
                priceParams.setMargins(layoutRightMgn, 0, layoutRightMgn, 0);
                if (Utils.isArabicLangSelected(activity)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        priceParams.addRule(RelativeLayout.ALIGN_PARENT_END);
                    }
                } else {
                    priceParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                }
                holder.priceDetailsLayout.setLayoutParams(priceParams);
                RelativeLayout.LayoutParams infoParams = new RelativeLayout.LayoutParams(flightInfoViewWidth, RelativeLayout.LayoutParams.WRAP_CONTENT);
                if (Utils.isArabicLangSelected(activity)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        infoParams.addRule(RelativeLayout.END_OF, R.id.circle_layout);
                        infoParams.addRule(RelativeLayout.START_OF, R.id.layout_price_detail);
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        infoParams.addRule(RelativeLayout.RIGHT_OF, R.id.circle_layout);
                        infoParams.addRule(RelativeLayout.LEFT_OF, R.id.layout_price_detail);
                    }
                }
                holder.flightInfoLayout.setLayoutParams(infoParams);
                int stopQuantity = Integer.parseInt(odoObject.getFsDataBeanArrayList().get(0).getSq());
                if (stopQuantity > 0) {
                    holder.stopsViewLayout.removeAllViews();
                    holder.stopNameViewLayout.removeAllViews();
                    holder.stopTimeViewLayout.removeAllViews();
                    holder.stopsEmptyView.setVisibility(View.VISIBLE);
                } else {
                    holder.stopsViewLayout.setVisibility(View.GONE);
                    holder.stopNameViewLayout.setVisibility(View.GONE);
                    holder.stopTimeViewLayout.setVisibility(View.GONE);
                    android.widget.LinearLayout.LayoutParams stopsParam = new android.widget.LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
                    holder.sourceNameTextview.setLayoutParams(stopsParam);
                    holder.destinationNameTextview.setLayoutParams(stopsParam);
                    holder.stopsEmptyView.setVisibility(View.GONE);
                }
                ArrayList<FSDataBean> fsBeanArrayList = new ArrayList<>();
                fsBeanArrayList.addAll(odoObject.getFsDataBeanArrayList());
                addStops((stopQuantity + 1), fsBeanArrayList, holder.stopsViewLayout, holder.stopNameViewLayout, holder.stopTimeViewLayout,
                        holder.sourceNameTextview, holder.stopsEmptyView, holder.destinationNameTextview, flightInfoViewWidth);
                if (stopQuantity > 0) {
                    if (odoObject.getFsDataBeanArrayList().get(0).getIconNameArrayList() != null) {
                        amenties(holder, odoObject, 0);
                    } else if (odoObject.getFsDataBeanArrayList().get(1).getIconNameArrayList() != null) {
                        amenties(holder, odoObject, 1);
                    } else {
                        holder.amenitiesLayout.setVisibility(View.GONE);
                        holder.amenitiesView.setVisibility(View.GONE);
                    }
                } else {
                    if (odoObject.getFsDataBeanArrayList().get(0).getIconNameArrayList() != null) {
                        amenties(holder, odoObject, 0);
                    } else {
                        holder.amenitiesLayout.setVisibility(View.GONE);
                        holder.amenitiesView.setVisibility(View.GONE);
                    }
                }
            }
            // setAnimation(holder.conatainer, position);
        } catch (Exception e) {
            Utils.printMessage("adapter", "Err " + e.getMessage());
        }
        holder.conatainer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final ReturnFlightReviewDialog flightReviewDialog = new ReturnFlightReviewDialog(activity,
                        pricedItineraryObjectArrayList.get(position), airportMap, airlineMap, travellerCount, sourceName, returnName,
                        listener, position, tripType, totalPackagePrice, returnFlightPrice, pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().gettFare());
                flightReviewDialog.setCancelable(true);
                flightReviewDialog.show();
                Window window = flightReviewDialog.getWindow();
                int width = (int) (activity.getResources().getDisplayMetrics().widthPixels * 1.0);
                int height = (int) (activity.getResources().getDisplayMetrics().heightPixels * 0.85);
                window.setLayout(width, height);
                holder.conatainer.setClickable(false);
                flightReviewDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        holder.conatainer.setClickable(true);
                    }
                });
            }
        });
        return view;
    }

    private static class ViewHolder {
        public TextView airwaysTextView, airwaysCodeTextview, stopTextview, travellerCountTextview, sourceCodeTextview, sourceNameTextview, destinationCodeTextview, destinationNameTextview, currencyTextview,
                priceTextview, sourceTimeTextView, destinationTimeTextview, refundTextview, layoverTimeTextview, priceDecimalText;
        private ImageView image, firstImage, layoutIcon, freshMealIcon, wifiIcon, avodIcon, forwardBusIcon;
        public View amenitiesView, stopsEmptyView;
        private RelativeLayout stopsLayout, stopTimeLayout, stopsViewLayout, stopNameViewLayout, stopTimeViewLayout, flightInfoLayout, flightImageLayout;
        private LinearLayout conatainer, amenitiesLayout, stopNamesLayout, priceDetailsLayout, flightTripLayout;
        private TextView layoutText, freshMealText, wifiText, avodText, tripTypeTextView, classTypeTextView, flydubaiText, flydubaitextbrace;
    }

    private void amenties(ViewHolder holder, OriginDestinationOptionBean odoObject, int pos) {
        holder.amenitiesLayout.setVisibility(View.VISIBLE);
        holder.amenitiesView.setVisibility(View.VISIBLE);
        try {
            if (odoObject.getFsDataBeanArrayList().get(pos).getFlightAmenitiesBean().getSeatLayout() != null) {
                if (!odoObject.getFsDataBeanArrayList().get(pos).getFlightAmenitiesBean().getSeatLayout().isEmpty()) {
                    holder.layoutIcon.setVisibility(View.VISIBLE);
                    holder.layoutText.setVisibility(View.VISIBLE);
                    holder.layoutIcon.setImageResource(R.drawable.seat_layout);
                    holder.layoutText.setTextColor(Utils.getColor(context, R.color.button_bg_blue));
                    holder.layoutText.setText(odoObject.getFsDataBeanArrayList().get(pos).getFlightAmenitiesBean().getSeatLayout());
                } else {
                    holder.layoutIcon.setVisibility(View.GONE);
                    holder.layoutText.setVisibility(View.GONE);
                }
            } else {
                holder.layoutIcon.setVisibility(View.GONE);
                holder.layoutText.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            holder.layoutIcon.setVisibility(View.GONE);
            holder.layoutText.setVisibility(View.GONE);
        }
        if (!odoObject.getFsDataBeanArrayList().get(pos).getFlightAmenitiesBean().getMeals().isEmpty()) {
            holder.freshMealIcon.setImageResource(R.drawable.fresh_meal);
            holder.freshMealText.setTextColor(Utils.getColor(context, R.color.button_bg_blue));
        } else {
            holder.freshMealIcon.setImageResource(R.drawable.fresh_meal_hover);
            holder.freshMealText.setTextColor(Utils.getColor(context, R.color.room_details_border_color));
        }
        if (!odoObject.getFsDataBeanArrayList().get(pos).getFlightAmenitiesBean().getWifi().isEmpty()) {
            holder.wifiIcon.setImageResource(R.drawable.wifi);
            holder.wifiText.setTextColor(Utils.getColor(context, R.color.button_bg_blue));
        } else {
            holder.wifiIcon.setImageResource(R.drawable.wifi_hover);
            holder.wifiText.setTextColor(Utils.getColor(context, R.color.room_details_border_color));
        }
        if (!odoObject.getFsDataBeanArrayList().get(pos).getFlightAmenitiesBean().getAvod().isEmpty()
                && !odoObject.getFsDataBeanArrayList().get(pos).getFlightAmenitiesBean().getAvod().equalsIgnoreCase("NA")) {
            holder.avodIcon.setImageResource(R.drawable.avod);
            holder.avodText.setTextColor(Utils.getColor(context, R.color.button_bg_blue));
        } else {
            holder.avodIcon.setImageResource(R.drawable.avod_hover);
            holder.avodText.setTextColor(Utils.getColor(context, R.color.room_details_border_color));
        }
    }

    private void addStops(int count, ArrayList<FSDataBean> fsBeanArrayList, RelativeLayout stopsLayout, RelativeLayout stopNamesLayout,
                          RelativeLayout stopTimeLayout, TextView sourceNameTextview, View stopsEmptyView, TextView destinationNameTextview, int flightViewWidth) {
        int cellSpaceWidth = (flightViewWidth) / count;
        String previousStop = "";
        String previousTime = "";
        if (cellSpaceWidth != 0) {
            if (count > 1) {
                if (count == 2) {
                    android.widget.LinearLayout.LayoutParams stopsParam = new android.widget.LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.28f);
                    sourceNameTextview.setLayoutParams(stopsParam);
                    android.widget.LinearLayout.LayoutParams stopsViewParam = new android.widget.LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.44f);
                    stopsEmptyView.setLayoutParams(stopsViewParam);
                    android.widget.LinearLayout.LayoutParams returnStopsParam = new android.widget.LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.28f);
                    destinationNameTextview.setLayoutParams(returnStopsParam);
                } else {
                    android.widget.LinearLayout.LayoutParams stopsParam = new android.widget.LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.39f);
                    sourceNameTextview.setLayoutParams(stopsParam);
                    android.widget.LinearLayout.LayoutParams stopsViewParam = new android.widget.LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.22f);
                    stopsEmptyView.setLayoutParams(stopsViewParam);
                    android.widget.LinearLayout.LayoutParams returnStopsParam = new android.widget.LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.39f);
                    destinationNameTextview.setLayoutParams(returnStopsParam);
                }
                int layoutHeight = activity.getResources().getDimensionPixelSize(R.dimen.padding_size);
                int timeLeftPadding = activity.getResources().getDimensionPixelSize(R.dimen.flag_height);
                int imageWdtHt = activity.getResources().getDimensionPixelSize(R.dimen.margin_size);
                int circleImgMar = activity.getResources().getDimensionPixelSize(R.dimen.signInPage_view_gap);
                int stopCount = 0;
                for (int i = 1; i < count; i++) {
                    stopsLayout.setVisibility(View.VISIBLE);
                    stopNamesLayout.setVisibility(View.VISIBLE);
                    stopTimeLayout.setVisibility(View.VISIBLE);
                    previousStop = fsBeanArrayList.get(stopCount).getAap();
                    int xVal = (cellSpaceWidth * i) - circleImgMar;
                    int yVal = (layoutHeight / 2) - (imageWdtHt / 2);
                    int stopXVal = (cellSpaceWidth * i) - circleImgMar;
                    int timeXVal = (cellSpaceWidth * i) - timeLeftPadding;
                    ImageView iv = new ImageView(activity);
                    iv.setImageResource(R.drawable.yellow_radio);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(imageWdtHt, imageWdtHt);
                    if (Utils.isArabicLangSelected(activity)) {
                        params.rightMargin = xVal;
                    } else {
                        params.leftMargin = xVal;
                    }
                    params.topMargin = yVal;
                    iv.setTag("1");
                    stopsLayout.addView(iv, params);
                    RelativeLayout.LayoutParams stopParams = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    TextView tv = new TextView(activity);
                    tv.setText(previousStop);
                    tv.setTypeface(titleFace);
                    tv.setTextColor(Utils.getColor(activity, R.color.flight_combo_depart_color));
                    if (Utils.isArabicLangSelected(activity)) {
                        stopParams.rightMargin = stopXVal;
                        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                    } else {
                        stopParams.leftMargin = stopXVal;
                        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.flight_more_options_text));
                    }
                    stopParams.topMargin = activity.getResources().getDimensionPixelSize(R.dimen.contact_email_margin);
                    stopNamesLayout.addView(tv, stopParams);
                    RelativeLayout.LayoutParams timeParams = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    TextView timeFormatText = new TextView(activity);
                    timeFormatText.setTypeface(titleFace);
                    timeFormatText.setTextColor(Utils.getColor(activity, R.color.non_refund_color));
                    if (Utils.isArabicLangSelected(activity)) {
                        timeParams.rightMargin = timeXVal;
                        timeFormatText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                    } else {
                        timeParams.leftMargin = timeXVal;
                        timeFormatText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.flight_more_options_text));
                    }
                    timeParams.topMargin = activity.getResources().getDimensionPixelSize(R.dimen.stop_over_text_margin);
                    String currentTime = "";
                    if (fsBeanArrayList.size() > stopCount + 1) {
                        currentTime = fsBeanArrayList.get(stopCount + 1).getDdt();
                    }
                    previousTime = fsBeanArrayList.get(stopCount).getArdt();
                    timeFormatText.setText(Utils.getStopOverTimeFormat(currentTime, previousTime, activity));
                    stopTimeLayout.addView(timeFormatText, timeParams);
                    stopCount++;
                }
            }
        }
    }
}
