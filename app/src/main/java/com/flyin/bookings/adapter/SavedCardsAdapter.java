package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.SavedCardsBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;


public class SavedCardsAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private Typeface titleFace, textFace;
    private ArrayList<SavedCardsBean> cardsListArray;
    private OnCustomItemSelectListener listener;

    public SavedCardsAdapter(Activity activity, ArrayList<SavedCardsBean> cardsListArray,
                             OnCustomItemSelectListener listener) {
        this.activity = activity;
        this.cardsListArray = cardsListArray;
        this.listener = listener;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return cardsListArray.size();
    }

    @Override
    public Object getItem(int position) {
        return cardsListArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.saved_cards_row, null);
        ImageView visaCardTypeImage = (ImageView) convertView.findViewById(R.id.visacardtype);
        ImageView visaCardHoverImage = (ImageView) convertView.findViewById(R.id.visacardtype_hover);
        TextView visaCardNumberFirst = (TextView) convertView.findViewById(R.id.visa_card_number_one);
        TextView visaCardNumberSecond = (TextView) convertView.findViewById(R.id.visa_card_number_two);
        TextView visaCardNumberthree = (TextView) convertView.findViewById(R.id.visa_card_number_three);
        TextView visaCardNumberEnd = (TextView) convertView.findViewById(R.id.visa_card_number_end);
        TextView cardHolderName = (TextView) convertView.findViewById(R.id.card_holder_name);
        TextView cardHolderExpiry = (TextView) convertView.findViewById(R.id.card_holder_expiry);
        ImageView closeImage = (ImageView) convertView.findViewById(R.id.close_imageView);
        cardHolderName.setTypeface(textFace);
        cardHolderName.setTypeface(titleFace);
        if (Utils.isArabicLangSelected(activity)) {
            cardHolderName.setGravity(Gravity.END);
            cardHolderName.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            cardHolderExpiry.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            visaCardNumberFirst.setGravity(Gravity.START);
            visaCardNumberFirst.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            visaCardNumberSecond.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            visaCardNumberthree.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            visaCardNumberEnd.setGravity(Gravity.END);
            visaCardNumberEnd.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
        cardHolderName.setText(cardsListArray.get(position).getCardHolderName());
        try {
            if (!cardsListArray.get(position).getCardType().equalsIgnoreCase("VISA")) {
                visaCardTypeImage.setImageResource(R.drawable.mastercard_logo);
                visaCardHoverImage.setImageResource(R.drawable.mastercard_hover);
            } else {
                visaCardTypeImage.setImageResource(R.drawable.visa_logo);
                visaCardHoverImage.setImageResource(R.drawable.visa_hover);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        if (!cardsListArray.get(position).getExpiryDate().isEmpty()) {
            String[] split = cardsListArray.get(position).getExpiryDate().split("(?<=\\G..)");
            cardHolderExpiry.setText(String.format(activity.getResources().getString(R.string.label_expiry), split[0], split[1]));
        } else {
            cardHolderExpiry.setText(String.format(activity.getResources().getString(R.string.label_expiry), 00, 00));
        }
        try {
            if (!cardsListArray.get(position).getCardNumber().isEmpty()) {
                visaCardNumberFirst.setText(cardsListArray.get(position).getCardNumber().substring(0, 4));
                visaCardNumberSecond.setText(cardsListArray.get(position).getCardNumber().substring(4, 8));
                visaCardNumberthree.setText(cardsListArray.get(position).getCardNumber().substring(8, 12));
                visaCardNumberEnd.setText(cardsListArray.get(position).getCardNumber().substring(12, 16));
            }
        } catch (Exception e) {
            e.getMessage();
            visaCardNumberFirst.setText("****");
            visaCardNumberSecond.setText("****");
            visaCardNumberthree.setText("****");
            visaCardNumberEnd.setText("****");
        }
        return convertView;
    }
}
