package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.model.FilterAirlineSelectionItem;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public class FlightAirlineFilterAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<FilterAirlineSelectionItem> airlineList = null;
    private HashMap<String, String> airLineNamesMap = null;
    private Typeface textFace;

    public FlightAirlineFilterAdapter(Activity activity, ArrayList<FilterAirlineSelectionItem> airlineList,
                                      HashMap<String, String> airLineNamesMap) {
        this.activity = activity;
        this.airlineList = airlineList;
        this.airLineNamesMap = airLineNamesMap;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(activity)) {
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return airlineList.size();
    }

    @Override
    public Object getItem(int position) {
        return airlineList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.view_airline_filter, null);
        TextView flightAirlineName = (TextView) convertView.findViewById(R.id.airline_text);
        ImageView selectIcon = (ImageView) convertView.findViewById(R.id.selectIcon);
        flightAirlineName.setTypeface(textFace);
        flightAirlineName.setText(airLineNamesMap.get(airlineList.get(position).getMal()));
        if (airlineList.get(position).isSelected()) {
            selectIcon.setVisibility(View.VISIBLE);
        } else {
            selectIcon.setVisibility(View.GONE);
        }
        return convertView;
    }
}
