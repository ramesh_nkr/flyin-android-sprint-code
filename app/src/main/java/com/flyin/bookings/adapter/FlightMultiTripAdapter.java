package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.R;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.FSDataBean;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.OriginDestinationOptionBean;
import com.flyin.bookings.model.PricedItineraryObject;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public class FlightMultiTripAdapter extends RecyclerView.Adapter<FlightMultiTripAdapter.MyViewHolder> {
    private ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList;
    private HashMap<String, FlightAirportName> airportMap;
    private HashMap<String, String> airlineMap;
    private Activity activity;
    private static final String TAG = "FlightMultiTripAdapter";
    private String firstAirlineName = "";
    private String secondAirlineName = "";
    private String thirdAirlineName = "";
    private int totalPassengersCount;
    private Typeface titleFace, textFace;
    private OnCustomItemSelectListener listener;

    public FlightMultiTripAdapter(Activity activity, ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList,
                                  HashMap<String, FlightAirportName> airportNamesMap, HashMap<String, String> airLineNamesMap,
                                  int totalPassengersCount, OnCustomItemSelectListener listener) {
        this.activity = activity;
        this.pricedItineraryObjectArrayList = pricedItineraryObjectArrayList;
        this.airportMap = airportNamesMap;
        this.airlineMap = airLineNamesMap;
        this.totalPassengersCount = totalPassengersCount;
        this.listener = listener;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_BOLD;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_BOLD;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_multi_trip_list_view, parent, false);
        return new MyViewHolder(itemView, activity);
    }

    @Override
    public int getItemCount() {
        return pricedItineraryObjectArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView airlineNamesText, refundTypeText, currencyText, priceText, priceDecimalText, travellersText, firstDateText, firstDepartTime,
                firstDepartCity, firstArrivalTime, firstArrivalCity, firstFlightDuration, secondDateText, secondDepartTime,
                secondDepartCity, secondArrivalTime, secondArrivalCity, secondFlightDuration, thirdDateText, thirdDepartTime,
                thirdDepartCity, thirdArrivalTime, thirdArrivalCity, thirdFlightDuration;
        private ImageView firstFlightImage, secondFlightImage, thirdFlightImage;
        private RelativeLayout firstImageLayout, secondImageLayout, thirdImageLayout, firstStopViewLayout, secondStopViewLayout,
                thirdStopViewLayout, firstStopNamesLayout, secondStopNamesLayout, thirdStopNamesLayout;
        private LinearLayout secondFlightLayout, thirdFlightLayout;
        private Typeface regularFace = null;
        private Typeface boldFace = null;

        public MyViewHolder(View view, Context context) {
            super(view);
            airlineNamesText = (TextView) view.findViewById(R.id.airline_name_text);
            refundTypeText = (TextView) view.findViewById(R.id.refund_type_text);
            currencyText = (TextView) view.findViewById(R.id.price_currency_text);
            priceText = (TextView) view.findViewById(R.id.price_text);
            priceDecimalText = (TextView) view.findViewById(R.id.price_decimal_text);
            travellersText = (TextView) view.findViewById(R.id.traveller_text);
            firstDateText = (TextView) view.findViewById(R.id.first_date_text);
            firstDepartTime = (TextView) view.findViewById(R.id.first_departure_time);
            firstDepartCity = (TextView) view.findViewById(R.id.first_depart_city);
            firstArrivalTime = (TextView) view.findViewById(R.id.first_arrival_time);
            firstArrivalCity = (TextView) view.findViewById(R.id.first_arrival_city);
            firstFlightDuration = (TextView) view.findViewById(R.id.first_flight_duration);
            secondDateText = (TextView) view.findViewById(R.id.second_date_text);
            secondDepartTime = (TextView) view.findViewById(R.id.second_departure_time);
            secondDepartCity = (TextView) view.findViewById(R.id.second_depart_city);
            secondArrivalTime = (TextView) view.findViewById(R.id.second_arrival_time);
            secondArrivalCity = (TextView) view.findViewById(R.id.second_arrival_city);
            secondFlightDuration = (TextView) view.findViewById(R.id.second_flight_duration);
            thirdDateText = (TextView) view.findViewById(R.id.third_date_text);
            thirdDepartTime = (TextView) view.findViewById(R.id.third_departure_time);
            thirdDepartCity = (TextView) view.findViewById(R.id.third_depart_city);
            thirdArrivalTime = (TextView) view.findViewById(R.id.third_arrival_time);
            thirdArrivalCity = (TextView) view.findViewById(R.id.third_arrival_city);
            thirdFlightDuration = (TextView) view.findViewById(R.id.third_flight_duration);
            firstFlightImage = (ImageView) view.findViewById(R.id.first_flight_image);
            secondFlightImage = (ImageView) view.findViewById(R.id.second_flight_image);
            thirdFlightImage = (ImageView) view.findViewById(R.id.third_flight_image);
            firstImageLayout = (RelativeLayout) view.findViewById(R.id.first_image_layout);
            secondImageLayout = (RelativeLayout) view.findViewById(R.id.second_image_layout);
            thirdImageLayout = (RelativeLayout) view.findViewById(R.id.third_image_layout);
            secondFlightLayout = (LinearLayout) view.findViewById(R.id.second_flight_layout);
            thirdFlightLayout = (LinearLayout) view.findViewById(R.id.third_flight_layout);
            firstStopViewLayout = (RelativeLayout) view.findViewById(R.id.first_stop_view_layout);
            secondStopViewLayout = (RelativeLayout) view.findViewById(R.id.second_stop_view_layout);
            thirdStopViewLayout = (RelativeLayout) view.findViewById(R.id.third_stop_view_layout);
            firstStopNamesLayout = (RelativeLayout) view.findViewById(R.id.stops_names_layout);
            secondStopNamesLayout = (RelativeLayout) view.findViewById(R.id.second_stops_names_layout);
            thirdStopNamesLayout = (RelativeLayout) view.findViewById(R.id.third_stops_names_layout);
            String fontText = Constants.FONT_ROBOTO_LIGHT;
            String fontTitle = Constants.FONT_ROBOTO_REGULAR;
            String fontPath = Constants.FONT_ROBOTO_MEDIUM;
            String fontMedium = Constants.FONT_ROBOTO_BOLD;
            if (Utils.isArabicLangSelected(context)) {
                fontText = Constants.FONT_DROIDKUFI_REGULAR;
                fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
                fontPath = Constants.FONT_DROIDKUFI_REGULAR;
                fontMedium = Constants.FONT_DROIDKUFI_BOLD;
            }
            Typeface lightFace = Typeface.createFromAsset(context.getAssets(), fontText);
            regularFace = Typeface.createFromAsset(context.getAssets(), fontTitle);
            Typeface mediumFace = Typeface.createFromAsset(context.getAssets(), fontPath);
            boldFace = Typeface.createFromAsset(context.getAssets(), fontMedium);
            airlineNamesText.setTypeface(boldFace);
            refundTypeText.setTypeface(regularFace);
            currencyText.setTypeface(regularFace);
            priceText.setTypeface(boldFace);
            priceDecimalText.setTypeface(regularFace);
            travellersText.setTypeface(regularFace);
            firstDateText.setTypeface(regularFace);
            firstDepartTime.setTypeface(boldFace);
            firstDepartCity.setTypeface(regularFace);
            firstArrivalTime.setTypeface(boldFace);
            firstArrivalCity.setTypeface(regularFace);
            firstFlightDuration.setTypeface(regularFace);
            secondDateText.setTypeface(regularFace);
            secondDepartTime.setTypeface(boldFace);
            secondDepartCity.setTypeface(regularFace);
            secondArrivalTime.setTypeface(boldFace);
            secondArrivalCity.setTypeface(regularFace);
            secondFlightDuration.setTypeface(regularFace);
            thirdDateText.setTypeface(regularFace);
            thirdDepartTime.setTypeface(boldFace);
            thirdDepartCity.setTypeface(regularFace);
            thirdArrivalTime.setTypeface(boldFace);
            thirdArrivalCity.setTypeface(regularFace);
            thirdFlightDuration.setTypeface(regularFace);
            if (Utils.isArabicLangSelected(context)) {
                airlineNamesText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                refundTypeText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_time_text));
                currencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                priceText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_size));
                priceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                travellersText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_time_text));
                firstDateText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                firstDepartTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                firstDepartCity.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                firstArrivalTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                firstArrivalCity.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                firstFlightDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                secondDateText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                secondDepartTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                secondDepartCity.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                secondArrivalTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                secondArrivalCity.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                secondFlightDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                thirdDateText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                thirdDepartTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                thirdDepartCity.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                thirdArrivalTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                thirdArrivalCity.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
                thirdFlightDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            }
        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        try {
            PricedItineraryObject pricedItineraryObject = pricedItineraryObjectArrayList.get(position);
            for (int i = 0; i < pricedItineraryObject.getAiBean().getOriginDestinationOptionBeanArrayList().size(); i++) {
                OriginDestinationOptionBean odoObject = pricedItineraryObject.getAiBean().getOriginDestinationOptionBeanArrayList().get(i);
                for (int j = 0; j < odoObject.getFsDataBeanArrayList().size(); j++) {
                    if (i == 0) {
                        String imagePath = Constants.IMAGE_BASE_URL + odoObject.getFsDataBeanArrayList().get(0).getMal() + Constants.IMAGE_FILE_FORMAT;
                        Glide.with(activity).load(imagePath).placeholder(R.drawable.imagethumb_search).into(holder.firstFlightImage);
                        firstAirlineName = airlineMap.get(odoObject.getFsDataBeanArrayList().get(0).getMal());
                        holder.firstDepartTime.setText(Utils.formatDateToTime(odoObject.getFsDataBeanArrayList().get(0).getDdt(), activity));
                        holder.firstArrivalTime.setText(Utils.formatDateToTime(odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getArdt(), activity));
                        holder.firstDepartCity.setText(airportMap.get(odoObject.getFsDataBeanArrayList().get(0).getDap()).getCityName());
                        holder.firstArrivalCity.setText(airportMap.get(odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getAap()).getCityName());
                        holder.firstFlightDuration.setText(Utils.formatDurationToNewTimeFormat(odoObject.getFsDataBeanArrayList().get(0).getDur(), activity));
                        holder.firstDateText.setText(Utils.formatDateToMonthDateFormat(odoObject.getFsDataBeanArrayList().get(0).getDdt(), activity));
                        int departStopQuantity = Integer.parseInt(odoObject.getFsDataBeanArrayList().get(0).getSq());
                        if (holder.firstStopViewLayout.getChildCount() > 0) {
                            for (int k = 0; k < holder.firstStopViewLayout.getChildCount(); k++) {
                                if (holder.firstStopViewLayout.getChildAt(k).getTag() != null) {
                                    if (holder.firstStopViewLayout.getChildAt(k).getTag().equals("1")) {
                                        holder.firstStopViewLayout.removeViewAt(k);
                                    }
                                }
                            }
                        }
                        if (departStopQuantity > 0) {
                            holder.firstStopNamesLayout.removeAllViews();
                        } else {
                            holder.firstStopNamesLayout.setVisibility(View.GONE);
                        }
                        ArrayList<FSDataBean> fsBeanArrayList = new ArrayList<>();
                        fsBeanArrayList.addAll(odoObject.getFsDataBeanArrayList());
                        addStops((departStopQuantity + 1), fsBeanArrayList, holder.firstStopNamesLayout, holder.firstStopViewLayout);
                    } else if (i == 1) {
                        holder.secondImageLayout.setVisibility(View.VISIBLE);
                        String imagePath = Constants.IMAGE_BASE_URL + odoObject.getFsDataBeanArrayList().get(0).getMal() + Constants.IMAGE_FILE_FORMAT;
                        Glide.with(activity).load(imagePath).placeholder(R.drawable.imagethumb_search).into(holder.secondFlightImage);
                        secondAirlineName = " / " + airlineMap.get(odoObject.getFsDataBeanArrayList().get(0).getMal());
                        holder.secondFlightLayout.setVisibility(View.VISIBLE);
                        holder.secondDepartTime.setText(Utils.formatDateToTime(odoObject.getFsDataBeanArrayList().get(0).getDdt(), activity));
                        holder.secondArrivalTime.setText(Utils.formatDateToTime(odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getArdt(), activity));
                        holder.secondDepartCity.setText(airportMap.get(odoObject.getFsDataBeanArrayList().get(0).getDap()).getCityName());
                        holder.secondArrivalCity.setText(airportMap.get(odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getAap()).getCityName());
                        holder.secondFlightDuration.setText(Utils.formatDurationToNewTimeFormat(odoObject.getFsDataBeanArrayList().get(0).getDur(), activity));
                        holder.secondDateText.setText(Utils.formatDateToMonthDateFormat(odoObject.getFsDataBeanArrayList().get(0).getDdt(), activity));
                        int departStopQuantity = Integer.parseInt(odoObject.getFsDataBeanArrayList().get(0).getSq());
                        if (holder.secondStopViewLayout.getChildCount() > 0) {
                            for (int k = 0; k < holder.secondStopViewLayout.getChildCount(); k++) {
                                if (holder.secondStopViewLayout.getChildAt(k).getTag() != null) {
                                    if (holder.secondStopViewLayout.getChildAt(k).getTag().equals("1")) {
                                        holder.secondStopViewLayout.removeViewAt(k);
                                    }
                                }
                            }
                        }
                        if (departStopQuantity > 0) {
                            holder.secondStopNamesLayout.removeAllViews();
                        } else {
                            holder.secondStopNamesLayout.setVisibility(View.GONE);
                        }
                        ArrayList<FSDataBean> fsBeanArrayList = new ArrayList<>();
                        fsBeanArrayList.addAll(odoObject.getFsDataBeanArrayList());
                        addStops((departStopQuantity + 1), fsBeanArrayList, holder.secondStopNamesLayout, holder.secondStopViewLayout);
                    } else if (i == 2) {
                        holder.thirdImageLayout.setVisibility(View.VISIBLE);
                        String imagePath = Constants.IMAGE_BASE_URL + odoObject.getFsDataBeanArrayList().get(0).getMal() + Constants.IMAGE_FILE_FORMAT;
                        Glide.with(activity).load(imagePath).placeholder(R.drawable.imagethumb_search).into(holder.thirdFlightImage);
                        thirdAirlineName = " / " + airlineMap.get(odoObject.getFsDataBeanArrayList().get(0).getMal());
                        holder.thirdFlightLayout.setVisibility(View.VISIBLE);
                        holder.thirdDepartTime.setText(Utils.formatDateToTime(odoObject.getFsDataBeanArrayList().get(0).getDdt(), activity));
                        holder.thirdArrivalTime.setText(Utils.formatDateToTime(odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getArdt(), activity));
                        holder.thirdDepartCity.setText(airportMap.get(odoObject.getFsDataBeanArrayList().get(0).getDap()).getCityName());
                        holder.thirdArrivalCity.setText(airportMap.get(odoObject.getFsDataBeanArrayList().get(odoObject.getFsDataBeanArrayList().size() - 1).getAap()).getCityName());
                        holder.thirdFlightDuration.setText(Utils.formatDurationToNewTimeFormat(odoObject.getFsDataBeanArrayList().get(0).getDur(), activity));
                        holder.thirdDateText.setText(Utils.formatDateToMonthDateFormat(odoObject.getFsDataBeanArrayList().get(0).getDdt(), activity));
                        int departStopQuantity = Integer.parseInt(odoObject.getFsDataBeanArrayList().get(0).getSq());
                        if (holder.thirdStopViewLayout.getChildCount() > 0) {
                            for (int k = 0; k < holder.thirdStopViewLayout.getChildCount(); k++) {
                                if (holder.thirdStopViewLayout.getChildAt(k).getTag() != null) {
                                    if (holder.thirdStopViewLayout.getChildAt(k).getTag().equals("1")) {
                                        holder.thirdStopViewLayout.removeViewAt(k);
                                    }
                                }
                            }
                        }
                        if (departStopQuantity > 0) {
                            holder.thirdStopNamesLayout.removeAllViews();
                        } else {
                            holder.thirdStopNamesLayout.setVisibility(View.GONE);
                        }
                        ArrayList<FSDataBean> fsBeanArrayList = new ArrayList<>();
                        fsBeanArrayList.addAll(odoObject.getFsDataBeanArrayList());
                        addStops((departStopQuantity + 1), fsBeanArrayList, holder.thirdStopNamesLayout, holder.thirdStopViewLayout);
                    }
                    holder.airlineNamesText.setText(firstAirlineName + "" + secondAirlineName + "" + thirdAirlineName);
                }
                if (pricedItineraryObject.getRefundablestate()) {
                    holder.refundTypeText.setTextColor(Utils.getColor(activity, R.color.refund_text_color));
                    holder.refundTypeText.setText(activity.getString(R.string.RefLbl));
                } else {
                    holder.refundTypeText.setTextColor(Utils.getColor(activity, R.color.non_refund_color));
                    holder.refundTypeText.setText(activity.getString(R.string.NonRefLbl));
                }
                if (totalPassengersCount > 1) {
                    holder.travellersText.setText(totalPassengersCount + " " + activity.getString(R.string.label_search_flight_travellers));
                } else {
                    holder.travellersText.setText(totalPassengersCount + " " + activity.getString(R.string.label_add_traveller));
                }
                Double price = Double.valueOf(pricedItineraryObject.getAipiBean().getItfObject().gettFare());
                String actualPriceResult = Utils.convertonly_PriceToUserSelectionPrice(price, activity.getApplicationContext());
                String priceResult[] = actualPriceResult.split("\\.");
                SharedPreferences pref = activity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                String currencyResult = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
                if (Utils.isArabicLangSelected(activity)) {
                    holder.currencyText.setTypeface(titleFace);
                    holder.priceText.setTypeface(textFace);
                    holder.priceDecimalText.setTypeface(titleFace);
                    holder.currencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                    holder.priceText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.text_size));
                    holder.priceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                    holder.priceText.setText("." + priceResult[0] + " ");
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new RecyclerView.LayoutParams(
                            RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));
                    params.setMargins(0, 0, 0, 0);
                    holder.currencyText.setLayoutParams(params);
                    holder.currencyText.setText(priceResult[1]);
                    if (currencyResult.equalsIgnoreCase("SAR")) {
                        holder.priceDecimalText.setText(activity.getString(R.string.label_SAR_currency_name));
                    } else {
                        holder.priceDecimalText.setText(currencyResult);
                    }
                } else {
                    holder.currencyText.setText(currencyResult);
                    holder.priceText.setText(priceResult[0] + ".");
                    holder.priceDecimalText.setText(priceResult[1]);
                }
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(position);
                    }
                }
            });
        } catch (Exception e) {
            Utils.printMessage(TAG, "Error : " + e.getMessage());
        }
    }

    private void addStops(int count, ArrayList<FSDataBean> fsBeanArrayList, RelativeLayout stopNamesLayout, RelativeLayout stopsLayout) {
        int cellSpaceWidth = activity.getResources().getDimensionPixelSize(R.dimen.flag_margin) / count;
        int nameSpaceWidth = activity.getResources().getDimensionPixelSize(R.dimen.signInPage_view_gap) / count;
        String previousStop = "";
        if (cellSpaceWidth != 0 && nameSpaceWidth != 0) {
            if (count > 1) {
                int imageScaledSize = activity.getResources().getDimensionPixelSize(R.dimen.circle_image_height);
                int imgPaddingSize = activity.getResources().getDimensionPixelSize(R.dimen.multi_trip_height);
                int textLeftPadding = activity.getResources().getDimensionPixelSize(R.dimen.margin_size);
                int stopCount = 0;
                for (int i = 1; i < count; i++) {
                    stopNamesLayout.setVisibility(View.VISIBLE);
                    previousStop = fsBeanArrayList.get(stopCount).getAap();
                    int xVal = ((cellSpaceWidth * i) + imageScaledSize / 2) - imgPaddingSize;
                    int yVal = activity.getResources().getDimensionPixelSize(R.dimen.menu_view_size);
                    int stopXVal = 0;
                    if (count == 2) {
                        stopXVal = (cellSpaceWidth * i) - (nameSpaceWidth * i);
                    } else if (count == 3) {
                        if (i == 1) {
                            stopXVal = (cellSpaceWidth * i) - (nameSpaceWidth * i) - imgPaddingSize;
                        } else {
                            stopXVal = (cellSpaceWidth * i) - (nameSpaceWidth * i) + textLeftPadding;
                        }
                    } else {
                        stopNamesLayout.setVisibility(View.GONE);
                    }
                    ImageView iv = new ImageView(activity);
                    iv.setImageResource(R.drawable.yellow_radio);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(imageScaledSize, imageScaledSize);
                    if (Utils.isArabicLangSelected(activity)) {
                        params.rightMargin = xVal;
                    } else {
                        params.leftMargin = xVal;
                    }
                    params.topMargin = yVal;
                    iv.setTag("1");
                    stopsLayout.addView(iv, params);
                    RelativeLayout.LayoutParams stopParams = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    TextView tv = new TextView(activity);
                    tv.setText(previousStop);
                    tv.setTextColor(Utils.getColor(activity, R.color.flight_number_text_color));
                    if (Utils.isArabicLangSelected(activity)) {
                        stopParams.rightMargin = stopXVal;
                        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.flight_duration_text));
                    } else {
                        stopParams.leftMargin = stopXVal;
                        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.flight_moreDetails));
                    }
                    stopParams.topMargin = activity.getResources().getDimensionPixelSize(R.dimen.stop_over_text_margin);
                    stopNamesLayout.addView(tv, stopParams);
                    stopCount++;
                }
            }
        }
    }
}
