package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.dialog.TravellerListDialog;
import com.flyin.bookings.listeners.RoomInformationListener;
import com.flyin.bookings.model.FhRoomPassengerDetailsBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;
import java.util.Locale;

public class RoomInformationAdapter extends BaseAdapter {
    private Activity activity = null;
    private ArrayList<FhRoomPassengerDetailsBean> roomPassengerArrayList = null;
    private LayoutInflater inflater = null;
    private RoomInformationListener listener = null;
    private Typeface titleFace, textFace, tf;
    private LinearLayout firstChildLayout = null;
    private LinearLayout secondChildLayout = null;
    private LinearLayout thirdChildLayout = null;
    private LinearLayout fourthChildLayout = null;
    private LinearLayout fifthChildLayout = null;

    public RoomInformationAdapter(Activity activity, ArrayList<FhRoomPassengerDetailsBean> roomPassengerArrayList,
                                  RoomInformationListener listener) {
        this.activity = activity;
        this.roomPassengerArrayList = roomPassengerArrayList;
        this.listener = listener;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_BOLD;
        String fontStyle = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_BOLD;
            fontStyle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        tf = Typeface.createFromAsset(activity.getAssets(), fontStyle);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return roomPassengerArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return roomPassengerArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.view_add_room, null);
        TextView roomHeaderText = (TextView) convertView.findViewById(R.id.room_header_text);
        TextView adultHeader = (TextView) convertView.findViewById(R.id.adult_header);
        final TextView adultCountText = (TextView) convertView.findViewById(R.id.adult_count);
        TextView childHeader = (TextView) convertView.findViewById(R.id.child_header);
        final TextView childCountText = (TextView) convertView.findViewById(R.id.child_count);
        TextView infantHeader = (TextView) convertView.findViewById(R.id.infant_header);
        final TextView infantCountText = (TextView) convertView.findViewById(R.id.infant_count);
        ImageView closeRoomIcon = (ImageView) convertView.findViewById(R.id.close_icon);
        ImageView adultDecrementIcon = (ImageView) convertView.findViewById(R.id.adult_decrement_icon);
        ImageView adultIncrementIcon = (ImageView) convertView.findViewById(R.id.adult_increment_icon);
        ImageView childDecrementIcon = (ImageView) convertView.findViewById(R.id.child_decrement_icon);
        ImageView childIncrementIcon = (ImageView) convertView.findViewById(R.id.child_increment_icon);
        ImageView infantDecrementIcon = (ImageView) convertView.findViewById(R.id.infant_decrement_icon);
        ImageView infantIncrementIcon = (ImageView) convertView.findViewById(R.id.infant_increment_icon);
        LinearLayout adultDecrementLayout = (LinearLayout) convertView.findViewById(R.id.adult_decrement_layout);
        LinearLayout adultIncrementLayout = (LinearLayout) convertView.findViewById(R.id.adult_increment_layout);
        LinearLayout childDecrementLayout = (LinearLayout) convertView.findViewById(R.id.child_decrement_layout);
        LinearLayout childIncrementLayout = (LinearLayout) convertView.findViewById(R.id.child_increment_layout);
        LinearLayout infantDecrementLayout = (LinearLayout) convertView.findViewById(R.id.infant_decrement_layout);
        LinearLayout infantIncrementLayout = (LinearLayout) convertView.findViewById(R.id.infant_increment_layout);
        final RelativeLayout childAgeLayout = (RelativeLayout) convertView.findViewById(R.id.child_age_layout);
        firstChildLayout = (LinearLayout) convertView.findViewById(R.id.first_child_layout);
        secondChildLayout = (LinearLayout) convertView.findViewById(R.id.second_child_layout);
        thirdChildLayout = (LinearLayout) convertView.findViewById(R.id.third_child_layout);
        fourthChildLayout = (LinearLayout) convertView.findViewById(R.id.fourth_child_layout);
        fifthChildLayout = (LinearLayout) convertView.findViewById(R.id.fifth_child_layout);
        final TextView firstChildAge = (TextView) convertView.findViewById(R.id.first_child_age);
        final TextView secondChildAge = (TextView) convertView.findViewById(R.id.second_child_age);
        final TextView thirdChildAge = (TextView) convertView.findViewById(R.id.third_child_age);
        final TextView fourthChildAge = (TextView) convertView.findViewById(R.id.fourth_child_age);
        final TextView fifthChildAge = (TextView) convertView.findViewById(R.id.fifth_child_age);
        TextView childHeaderText = (TextView) convertView.findViewById(R.id.child_header_text);
        LinearLayout adultViewLayout = (LinearLayout) convertView.findViewById(R.id.adult_view_layout);
        LinearLayout childViewLayout = (LinearLayout) convertView.findViewById(R.id.child_view_layout);
        LinearLayout infantViewLayout = (LinearLayout) convertView.findViewById(R.id.infant_view_layout);
        LinearLayout.LayoutParams childParam1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        childParam1.weight = 0.5f;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            childParam1.setMargins(25, 0, 25, 0);
        } else {
            childParam1.setMargins(15, 0, 15, 0);
        }
        adultViewLayout.setLayoutParams(childParam1);
        LinearLayout.LayoutParams childParam2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        childParam2.weight = 0.5f;
        if (Utils.isArabicLangSelected(activity)) {
            childParam2.setMargins(25, 0, 0, 0);
        } else {
            childParam2.setMargins(0, 0, 25, 0);
        }
        childViewLayout.setLayoutParams(childParam2);
        infantViewLayout.setVisibility(View.GONE);
        adultHeader.setText(activity.getString(R.string.SAdultLbl));
        adultHeader.append(Utils.addTitleField(activity.getString(R.string.label_hotel_adult_age)));
        childHeader.setText(activity.getString(R.string.SChildLbl));
        childHeader.append(Utils.addTitleField(activity.getString(R.string.label_hotel_child_age)));
        infantHeader.setText(activity.getString(R.string.SInfantLbl));
        infantHeader.append(Utils.addTitleField(activity.getString(R.string.label_hotel_infant_age)));
        if (Utils.isArabicLangSelected(activity)) {
            roomHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            adultHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            adultCountText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            childHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            childCountText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            infantHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            infantCountText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            childHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            firstChildAge.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            secondChildAge.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            thirdChildAge.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            fourthChildAge.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            fifthChildAge.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                adultDecrementLayout.setBackgroundResource(R.drawable.right_traveller_count);
                adultIncrementLayout.setBackgroundResource(R.drawable.left_traveller_count);
                childDecrementLayout.setBackgroundResource(R.drawable.right_traveller_count);
                childIncrementLayout.setBackgroundResource(R.drawable.left_traveller_count);
                infantDecrementLayout.setBackgroundResource(R.drawable.right_traveller_count);
                infantIncrementLayout.setBackgroundResource(R.drawable.left_traveller_count);
            }
        }
        roomHeaderText.setTypeface(textFace);
        adultHeader.setTypeface(titleFace);
        adultCountText.setTypeface(titleFace);
        childHeader.setTypeface(titleFace);
        childCountText.setTypeface(titleFace);
        infantHeader.setTypeface(titleFace);
        infantCountText.setTypeface(titleFace);
        childHeaderText.setTypeface(titleFace);
        firstChildAge.setTypeface(titleFace);
        secondChildAge.setTypeface(titleFace);
        thirdChildAge.setTypeface(titleFace);
        fourthChildAge.setTypeface(titleFace);
        fifthChildAge.setTypeface(titleFace);
        if (position == 0) {
            closeRoomIcon.setVisibility(View.GONE);
        } else {
            closeRoomIcon.setVisibility(View.VISIBLE);
        }
        if (roomPassengerArrayList.get(position).isExpanded()) {
            childAgeLayout.setVisibility(View.VISIBLE);
        } else {
            childAgeLayout.setVisibility(View.GONE);
        }
        roomHeaderText.setText(activity.getString(R.string.label_single_room) + " " + (position + 1));
        closeRoomIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemCancelClick(position);
                }
            }
        });
        int adultCount = roomPassengerArrayList.get(position).getAdultCnt();
        int childCount = roomPassengerArrayList.get(position).getChildCnt();
        int infantCount = roomPassengerArrayList.get(position).getInfantCnt();
        String formattedAdultCnt = String.format(Locale.ENGLISH, "%02d", adultCount);
        adultCountText.setText(formattedAdultCnt);
        String formattedChildCnt = String.format(Locale.ENGLISH, "%02d", childCount);
        childCountText.setText(formattedChildCnt);
        String formattedInfantCnt = String.format(Locale.ENGLISH, "%02d", infantCount);
        infantCountText.setText(formattedInfantCnt);
        if (adultCount == 6) {
            adultIncrementIcon.setImageResource(R.drawable.inc_psgr);
        } else {
            adultIncrementIcon.setImageResource(R.drawable.add_psgr);
        }
        if (adultCount == 1) {
            adultDecrementIcon.setImageResource(R.drawable.sub_psgr);
        } else {
            adultDecrementIcon.setImageResource(R.drawable.less_psgr);
        }
        if (childCount == 5) {
            childIncrementIcon.setImageResource(R.drawable.inc_psgr);
        } else {
            childIncrementIcon.setImageResource(R.drawable.add_psgr);
        }
        if (childCount == 0) {
            childDecrementIcon.setImageResource(R.drawable.sub_psgr);
        } else {
            childDecrementIcon.setImageResource(R.drawable.less_psgr);
        }
        if (childCount >= 1) {
            switch (childCount) {
                case 1:
                    firstChildLayout.setVisibility(View.VISIBLE);
                    childAgeLayout.setVisibility(View.VISIBLE);
                    break;
                case 2:
                    firstChildLayout.setVisibility(View.VISIBLE);
                    secondChildLayout.setVisibility(View.VISIBLE);
                    break;
                case 3:
                    firstChildLayout.setVisibility(View.VISIBLE);
                    secondChildLayout.setVisibility(View.VISIBLE);
                    thirdChildLayout.setVisibility(View.VISIBLE);
                    break;
                case 4:
                    firstChildLayout.setVisibility(View.VISIBLE);
                    secondChildLayout.setVisibility(View.VISIBLE);
                    thirdChildLayout.setVisibility(View.VISIBLE);
                    fourthChildLayout.setVisibility(View.VISIBLE);
                    break;
                case 5:
                    firstChildLayout.setVisibility(View.VISIBLE);
                    secondChildLayout.setVisibility(View.VISIBLE);
                    thirdChildLayout.setVisibility(View.VISIBLE);
                    fourthChildLayout.setVisibility(View.VISIBLE);
                    fifthChildLayout.setVisibility(View.VISIBLE);
                    break;
            }
        }
        adultDecrementLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(position, 1, false);
                }
            }
        });
        adultIncrementLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(position, 1, true);
                }
            }
        });
        childDecrementLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(position, 2, false);
                }
            }
        });
        childIncrementLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(position, 2, true);
                }
            }
        });
        infantDecrementLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(position, 3, false);
                }
            }
        });
        infantIncrementLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(position, 3, true);
                }
            }
        });
        int firstChdAge = 2;
        if (roomPassengerArrayList.get(position).getChildAgeArray().get(0) != 0) {
            firstChdAge = roomPassengerArrayList.get(position).getChildAgeArray().get(0);
        }
        firstChildAge.setText(setChildAgeWithTitle(firstChdAge));
        firstChildLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog childAgeDialog = new TravellerListDialog(activity, Constants.CHILD_AGE_SELECTION, null, "", null, false);
                childAgeDialog.setCancelable(true);
                childAgeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (childAgeDialog != null) {
                            if (childAgeDialog.selectedChildAge != 0) {
                                firstChildAge.setText(setChildAgeWithTitle(childAgeDialog.selectedChildAge));
                                if (listener != null) {
                                    listener.onChildItemClick(position, 0, childAgeDialog.selectedChildAge);
                                }
                            }
                        }
                    }
                });
                childAgeDialog.show();
            }
        });
        int secondChdAge = 2;
        if (roomPassengerArrayList.get(position).getChildAgeArray().get(1) != 0) {
            secondChdAge = roomPassengerArrayList.get(position).getChildAgeArray().get(1);
        }
        secondChildAge.setText(setChildAgeWithTitle(secondChdAge));
        secondChildLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog childAgeDialog = new TravellerListDialog(activity, Constants.CHILD_AGE_SELECTION, null, "", null, false);
                childAgeDialog.setCancelable(true);
                childAgeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (childAgeDialog != null) {
                            if (childAgeDialog.selectedChildAge != 0) {
                                secondChildAge.setText(setChildAgeWithTitle(childAgeDialog.selectedChildAge));
                                if (listener != null) {
                                    listener.onChildItemClick(position, 1, childAgeDialog.selectedChildAge);
                                }
                            }
                        }
                    }
                });
                childAgeDialog.show();
            }
        });
        int thirdChdAge = 2;
        if (roomPassengerArrayList.get(position).getChildAgeArray().get(2) != 0) {
            thirdChdAge = roomPassengerArrayList.get(position).getChildAgeArray().get(2);
        }
        thirdChildAge.setText(setChildAgeWithTitle(thirdChdAge));
        thirdChildLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog childAgeDialog = new TravellerListDialog(activity, Constants.CHILD_AGE_SELECTION, null, "", null, false);
                childAgeDialog.setCancelable(true);
                childAgeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (childAgeDialog != null) {
                            if (childAgeDialog.selectedChildAge != 0) {
                                thirdChildAge.setText(setChildAgeWithTitle(childAgeDialog.selectedChildAge));
                                if (listener != null) {
                                    listener.onChildItemClick(position, 2, childAgeDialog.selectedChildAge);
                                }
                            }
                        }
                    }
                });
                childAgeDialog.show();
            }
        });
        int fourthChdAge = 2;
        if (roomPassengerArrayList.get(position).getChildAgeArray().get(3) != 0) {
            fourthChdAge = roomPassengerArrayList.get(position).getChildAgeArray().get(3);
        }
        fourthChildAge.setText(setChildAgeWithTitle(fourthChdAge));
        fourthChildLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog childAgeDialog = new TravellerListDialog(activity, Constants.CHILD_AGE_SELECTION, null, "", null, false);
                childAgeDialog.setCancelable(true);
                childAgeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (childAgeDialog != null) {
                            if (childAgeDialog.selectedChildAge != 0) {
                                fourthChildAge.setText(setChildAgeWithTitle(childAgeDialog.selectedChildAge));
                                if (listener != null) {
                                    listener.onChildItemClick(position, 3, childAgeDialog.selectedChildAge);
                                }
                            }
                        }
                    }
                });
                childAgeDialog.show();
            }
        });
        int fifthChdAge = 2;
        if (roomPassengerArrayList.get(position).getChildAgeArray().get(4) != 0) {
            fifthChdAge = roomPassengerArrayList.get(position).getChildAgeArray().get(4);
        }
        fifthChildAge.setText(setChildAgeWithTitle(fifthChdAge));
        fifthChildLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog childAgeDialog = new TravellerListDialog(activity, Constants.CHILD_AGE_SELECTION, null, "", null, false);
                childAgeDialog.setCancelable(true);
                childAgeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (childAgeDialog != null) {
                            if (childAgeDialog.selectedChildAge != 0) {
                                fifthChildAge.setText(setChildAgeWithTitle(childAgeDialog.selectedChildAge));
                                if (listener != null) {
                                    listener.onChildItemClick(position, 4, childAgeDialog.selectedChildAge);
                                }
                            }
                        }
                    }
                });
                childAgeDialog.show();
            }
        });
        return convertView;
    }

    private String setChildAgeWithTitle(int age) {
        String ageTitle = "";
        if (age == 2) {
            ageTitle = age + " " + activity.getResources().getString(R.string.label_child_age_two_years);
        } else if (age > 2 && age < 11) {
            ageTitle = age + " " + activity.getResources().getString(R.string.label_child_age_three_ten);
        } else {
            ageTitle = age + " " + activity.getResources().getString(R.string.label_child_age_eleven_twelve);
        }
        return ageTitle;
    }
}
