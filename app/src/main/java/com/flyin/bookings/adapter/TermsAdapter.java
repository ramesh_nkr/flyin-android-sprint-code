package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.TermsDetailsBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class TermsAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<TermsDetailsBean> listTermsConditions;
    private LayoutInflater inflater;
    private OnCustomItemSelectListener listener;
    private Typeface titleFace, textFace;

    public TermsAdapter(Activity activity, ArrayList<TermsDetailsBean> listTermsConditions,
                        OnCustomItemSelectListener listener) {
        this.activity = activity;
        this.listTermsConditions = listTermsConditions;
        this.listener = listener;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_MEDIUM;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listTermsConditions.size();
    }

    @Override
    public Object getItem(int position) {
        return listTermsConditions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View childView, ViewGroup Parent) {
        if (childView == null)
            childView = inflater.inflate(R.layout.view_terms, null);
        TextView termsText = (TextView) childView.findViewById(R.id.terms_header_name);
        ImageView downArrow = (ImageView) childView.findViewById(R.id.down_btn);
        TextView termsDescription = (TextView) childView.findViewById(R.id.description_text);
        termsText.setTypeface(textFace);
        termsDescription.setTypeface(titleFace);
        LinearLayout textHeaderLayout = (LinearLayout) childView.findViewById(R.id.text_layout);
        termsText.setText(listTermsConditions.get(position).getTermsHeaderName());
        termsDescription.setText(listTermsConditions.get(position).getText());
        downArrow.setImageResource(R.drawable.down_arrow);
        if (listTermsConditions.get(position).isExpanded()) {
            termsDescription.setVisibility(View.VISIBLE);
            downArrow.setImageResource(R.drawable.top_arrow);
        } else {
            termsDescription.setVisibility(View.GONE);
            downArrow.setImageResource(R.drawable.down_arrow);
        }
        if (Utils.isArabicLangSelected(activity)) {
            termsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            termsDescription.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        textHeaderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(position);
                }
            }
        });
        return childView;
    }
}
