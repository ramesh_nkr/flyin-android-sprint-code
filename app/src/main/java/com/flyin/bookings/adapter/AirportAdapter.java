package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.model.PredectiveSearchModel;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class AirportAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<PredectiveSearchModel> airportList;
    private LayoutInflater inflater;
    private Typeface titleFace, textFace;

    public AirportAdapter(Activity activity, ArrayList<PredectiveSearchModel> airportList) {
        this.activity = activity;
        this.airportList = airportList;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return airportList.size();
    }

    @Override
    public Object getItem(int position) {
        return airportList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.view_search_flight, null);
        TextView airportCode = (TextView) convertView.findViewById(R.id.airport_code);
        TextView airportCity = (TextView) convertView.findViewById(R.id.airport_name);
        if (Utils.isArabicLangSelected(activity)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                airportCity.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
            airportCode.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.results_view_arabic));
            airportCity.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.results_view_arabic));
        }
        airportCity.setTypeface(titleFace);
        airportCode.setTypeface(textFace);
        airportCode.setText(airportList.get(position).getAirportCode());
        airportCity.setText(airportList.get(position).getAirportName() + ", " + airportList.get(position).getCityName() + ", " + airportList.get(position).getCountryName());
        return convertView;
    }
}
