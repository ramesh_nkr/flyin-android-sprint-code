package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class TravellerListAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<String> itemsList;
    private Typeface titleFace;
    private LayoutInflater inflater;

    public TravellerListAdapter(Activity activity, ArrayList<String> itemsList) {
        this.activity = activity;
        this.itemsList = itemsList;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View childView, ViewGroup Parent) {
        childView = inflater.inflate(R.layout.view_traveller_dialog_text, null);
        TextView listData = (TextView) childView.findViewById(R.id.list_data_text);
        ImageView rightArrow = (ImageView) childView.findViewById(R.id.right_arrow);
        listData.setTypeface(titleFace);
        if (Utils.isArabicLangSelected(activity)) {
            rightArrow.setRotation(180);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                listData.setGravity(Gravity.START);
                listData.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
        }
        listData.setText(itemsList.get(position).toString());
        return childView;
    }
}
