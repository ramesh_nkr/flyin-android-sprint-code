package com.flyin.bookings.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Country;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class SearchCountryAdapter extends BaseAdapter {
    private Activity activity = null;
    private LayoutInflater inflater = null;
    private ArrayList<Country> countryList = null;
    private boolean isPhoneNumberClicked = false;
    private Typeface textFace;

    public SearchCountryAdapter(Activity activity, ArrayList<Country> countryList, boolean isPhoneNumberClicked) {
        this.activity = activity;
        this.countryList = countryList;
        this.isPhoneNumberClicked = isPhoneNumberClicked;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        textFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        inflater = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return countryList.size();
    }

    @Override
    public Object getItem(int position) {
        return countryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.view_search_country_list, null);
        ImageView countryImage = (ImageView) convertView.findViewById(R.id.country_image);
        TextView countryName = (TextView) convertView.findViewById(R.id.country_name);
        TextView countryCode = (TextView) convertView.findViewById(R.id.country_code);
        ImageView selectIcon = (ImageView) convertView.findViewById(R.id.selectIcon);
        Country country = countryList.get(position);
        countryName.setTypeface(textFace);
        countryCode.setTypeface(textFace);
        if (country != null) {
            countryName.setText(country.getName());
            countryCode.setText(country.getCountryCodeStr());
            countryImage.setImageResource(country.getResId());
            if (country.isSelected()) {
                selectIcon.setVisibility(View.VISIBLE);
            } else {
                selectIcon.setVisibility(View.GONE);
            }
        }
        if (isPhoneNumberClicked) {
            countryCode.setVisibility(View.VISIBLE);
        } else {
            countryCode.setVisibility(View.GONE);
        }
        if (Utils.isArabicLangSelected(activity)) {
            countryName.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            countryCode.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        return convertView;
    }
}
