package com.flyin.bookings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.model.TravellerDetailsBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class PassengerInfoAdapter extends BaseAdapter {
    private Activity activity = null;
    private ArrayList<TravellerDetailsBean> travellersList = null;
    private LayoutInflater inflater = null;
    private Typeface textFace, titleFace;

    public PassengerInfoAdapter(Activity activity, ArrayList<TravellerDetailsBean> travellersList) {
        this.activity = activity;
        this.travellersList = travellersList;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(activity)) {
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return travellersList.size();
    }

    @Override
    public Object getItem(int position) {
        return travellersList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.view_passenger_info, null);
        RelativeLayout passengerImageLayout = (RelativeLayout) convertView.findViewById(R.id.passenger_image_layout);
        TextView passengerShortNameText = (TextView) convertView.findViewById(R.id.passenger_short_name);
        ImageView passengerImg = (ImageView) convertView.findViewById(R.id.passenger_image);
        TextView passengerNameText = (TextView) convertView.findViewById(R.id.passenger_name);
        TextView passengerEmailText = (TextView) convertView.findViewById(R.id.passenger_mail);
        ImageView nextPageImage = (ImageView) convertView.findViewById(R.id.next_page_arrow);
        if (Utils.isArabicLangSelected(activity)) {
            passengerShortNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerEmailText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            nextPageImage.setRotation(180);
        }
        passengerShortNameText.setTypeface(textFace);
        passengerNameText.setTypeface(titleFace);
        passengerEmailText.setTypeface(textFace);
        passengerNameText.setText(travellersList.get(position).getTravellerHeaderName());
        passengerEmailText.setText(travellersList.get(position).getTravellerEmail());
        if (travellersList.get(position).getTravellerEmail().equalsIgnoreCase(activity.getResources().getString(R.string.label_enter_your_details))) {
            passengerNameText.setTextColor(Utils.getColor(activity, R.color.trip_unselected_color));
            passengerImg.setVisibility(View.VISIBLE);
            passengerShortNameText.setVisibility(View.GONE);
            nextPageImage.setImageResource(R.drawable.right_arrow);
        } else {
            if (position != 0) {
                passengerEmailText.setVisibility(View.GONE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    passengerNameText.setGravity(View.TEXT_ALIGNMENT_GRAVITY);
                }
            }
            passengerNameText.setTextColor(Utils.getColor(activity, R.color.result_compo_duration));
            String firstName = travellersList.get(position).getFirstName();
            String lastName = travellersList.get(position).getLastName();
            char first = Character.toUpperCase(firstName.charAt(0));
            char last = Character.toUpperCase(lastName.charAt(0));
            passengerShortNameText.setText(first + "" + last);
            passengerShortNameText.setVisibility(View.VISIBLE);
            passengerImg.setVisibility(View.GONE);
            passengerImageLayout.setBackgroundResource(R.drawable.border_selected_view_circle);
            nextPageImage.setImageResource(R.drawable.person_tick);
            nextPageImage.setRotation(360);
        }
        return convertView;
    }
}
