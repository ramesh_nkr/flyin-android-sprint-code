package com.flyin.bookings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.model.BookingsObject;
import com.flyin.bookings.model.CancellationPolicyBean;
import com.flyin.bookings.model.EntityDetailsObjectBean;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.FlightDetailsObjectBean;
import com.flyin.bookings.model.FlightsDataBean;
import com.flyin.bookings.model.GeneralDetailsObjectBean;
import com.flyin.bookings.model.LayoverDataBean;
import com.flyin.bookings.model.PassengerDetailsObjectBean;
import com.flyin.bookings.model.PassengersBean;
import com.flyin.bookings.model.PersonalObjectBean;
import com.flyin.bookings.model.PriceBean;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class TripConfirmationActivity extends AppCompatActivity implements AsyncTaskListener {
    private TextView airlinePnrNumber, tripIdNumber, dateOfBooking, airlineTicketCost, taxFare, numberOfTickets, airFareTotal,
            errorText, errorDescriptionText;
    private Typeface titleFace, textFace;
    private RelativeLayout loadingViewLayout;
    private LinearLayout flightInformationLayout, priceInformationHideLayout, passengerInfoHideLayout;
    private ImageView priceInfoDownImage, passengerDetailsImage;
    private String requestJSON = "";
    private static final int GET_BOOKED_ID_RESULTS = 1;
    private static final int GET_LOYALTY_POINTS = 5;
    private static final int GET_CONFIRM_LOYALTY_RESULT = 6;
    private LayoutInflater inflater;
    private View loadingView, couponView;
    private boolean isProfileDataExpanded = true;
    private boolean isPriceDetailsExpanded = true;
    private static final String TAG = "TripConfirmationActivity";
    private boolean isInternetPresent = false;
    private ImageView errorImage;
    private Button searchButton;
    private String userFullName = "";
    private boolean isArabicLang;
    private String previousTime = "";
    private TextView numberOfCoupon;
    private LinearLayout couponLayout;
    private String emailId = "";
    private HashMap<String, FlightAirportName> airportNamesMap = new HashMap<>();
    private HashMap<String, String> airLineNamesMap = new HashMap<>();
    private String cua;
    private ArrayList<BookingsObject> bookingMainArray = new ArrayList<>();
    private String journeyType = "";
    private String legOrder = "";
    private int firstLegOrder = 0;
    private int secondLegOrder = 0;
    private int forwardLegOrder = 0;
    private int multiTripStopCount2 = 0;
    private int multiTripStopCount3 = 0;
    private int numberOfPassengers = 0;
    private String passengerMail = "";
    private RelativeLayout refundPolicyLayout = null;
    private ArrayList<String> fareTypeArray = new ArrayList<>();
    private String bookingId = "";
    private LinearLayout rewardsLayout = null;
    private TextView rewardsPointsText = null;
    private LinearLayout qitafDiscountLayout = null;
    private TextView qitafDiscountValue = null;
    private LinearLayout redeemedRewardsLayout = null;
    private TextView redeemedRewardsValue = null;
    private ArrayList<String> airlinePnrArr = new ArrayList<>();
    private String leadPaxName = "";
    private int infoSource = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_confirmation);
        isInternetPresent = Utils.isConnectingToInternet(TripConfirmationActivity.this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Constants.JSON_REQUEST)) {
                requestJSON = bundle.getString(Constants.JSON_REQUEST, "");
            }
            userFullName = bundle.getString(Constants.USER_FULL_NAME, "");
            emailId = bundle.getString(Constants.USER_EMAIL, "");
            cua = bundle.getString(Constants.COUPON_AMOUNT, "");
            airportNamesMap = (HashMap<String, FlightAirportName>) bundle.get(Constants.AIRPORT_NAME_HASHMAP);
            airLineNamesMap = (HashMap<String, String>) bundle.get(Constants.AIRLINE_NAME_HASHMAP);
            bookingId = bundle.getString(Constants.BOOKING_ID, "");
            leadPaxName = bundle.getString(Constants.USER_NAME, "");
            infoSource = Integer.parseInt(bundle.getString(Constants.INFO_SOURCE, ""));
        }
        isArabicLang = false;
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        String fontDateBold = Constants.FONT_ROBOTO_BOLD;
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        String fontMedium = Constants.FONT_ROBOTO_MEDIUM;
        TextView bookConfirmationText = (TextView) findViewById(R.id.booking_confirmation_text);
        TextView thankYouText = (TextView) findViewById(R.id.thank_you_text);
        TextView bookingConfirmedText = (TextView) findViewById(R.id.booking_confirmed);
        TextView emailSendingText = (TextView) findViewById(R.id.email_sending_text);
        TextView userEmailId = (TextView) findViewById(R.id.user_email_id);
        TextView airlinePnrNumberText = (TextView) findViewById(R.id.airline_pnr_number_text);
        airlinePnrNumber = (TextView) findViewById(R.id.airline_pnr_number);
        TextView tripIdText = (TextView) findViewById(R.id.trip_id_text);
        tripIdNumber = (TextView) findViewById(R.id.trip_id);
        TextView dateOfBookingText = (TextView) findViewById(R.id.date_of_booking_text);
        dateOfBooking = (TextView) findViewById(R.id.date_of_booking);
        TextView passengerDetailsText = (TextView) findViewById(R.id.passenger_details_text);
        TextView passengerInformationText = (TextView) findViewById(R.id.price_information_text);
        TextView airlineTicketCostText = (TextView) findViewById(R.id.airline_ticket_cost_text);
        airlineTicketCost = (TextView) findViewById(R.id.airline_ticket_cost);
        TextView taxFareText = (TextView) findViewById(R.id.taxes_fare_text);
        taxFare = (TextView) findViewById(R.id.taxes_fare_number);
        TextView numberOfTicketsText = (TextView) findViewById(R.id.number_of_tickets_text);
        numberOfTickets = (TextView) findViewById(R.id.number_of_tickets);
        TextView airFareTotalText = (TextView) findViewById(R.id.airfare_subtotal_text);
        airFareTotal = (TextView) findViewById(R.id.airfare_subtotal);
        flightInformationLayout = (LinearLayout) findViewById(R.id.flights_information);
        LinearLayout priceInformationLayout = (LinearLayout) findViewById(R.id.price_information_layout);
        LinearLayout passengerDetailsLayout = (LinearLayout) findViewById(R.id.passenger_details_layout);
        priceInformationHideLayout = (LinearLayout) findViewById(R.id.price_info_layout);
        passengerInfoHideLayout = (LinearLayout) findViewById(R.id.passenger_data_layout);
        priceInfoDownImage = (ImageView) findViewById(R.id.price_information_image);
        passengerDetailsImage = (ImageView) findViewById(R.id.passenger_details_image);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        LinearLayout bookingsLayout = (LinearLayout) findViewById(R.id.bookings_layout);
        ImageView homeLogo = (ImageView) findViewById(R.id.home_logo);
        ImageView facebookLogo = (ImageView) findViewById(R.id.facebook_logo);
        ImageView twitterLogo = (ImageView) findViewById(R.id.twitter_logo);
        ImageView googlePlusLogo = (ImageView) findViewById(R.id.google_plus_logo);
        LinearLayout emailIdLayout = (LinearLayout) findViewById(R.id.email_id_layout);
        emailIdLayout.setVisibility(View.VISIBLE);
        RelativeLayout assistanceLayout = (RelativeLayout) findViewById(R.id.rl_main);
        assistanceLayout.setVisibility(View.VISIBLE);
        TextView infoTitle = (TextView) findViewById(R.id.info_title);
        TextView infoDetails2 = (TextView) findViewById(R.id.info_details2);
        couponLayout = (LinearLayout) findViewById(R.id.coupon_layout);
        couponView = findViewById(R.id.coupon_view);
        TextView numberOfCouponText = (TextView) findViewById(R.id.coupon_textview);
        numberOfCoupon = (TextView) findViewById(R.id.coupon_amount);
        refundPolicyLayout = (RelativeLayout) findViewById(R.id.refund_policy_layout);
        TextView refundTypeHeader = (TextView) findViewById(R.id.refund_type_header);
        TextView refundTypeDescription = (TextView) findViewById(R.id.refund_type_description);
        rewardsLayout = (LinearLayout) findViewById(R.id.reward_points_layout);
        rewardsPointsText = (TextView) findViewById(R.id.rewards_flight_dest_textview);
        qitafDiscountLayout = (LinearLayout) findViewById(R.id.qitaf_discount_layout);
        TextView qitafDiscountText = (TextView) findViewById(R.id.qitaf_discount_text);
        qitafDiscountValue = (TextView) findViewById(R.id.qitaf_discount);
        redeemedRewardsLayout = (LinearLayout) findViewById(R.id.redeemed_rewards_layout);
        TextView redeemedHeaderText = (TextView) findViewById(R.id.redeemed_rewards_text);
        redeemedRewardsValue = (TextView) findViewById(R.id.redeemed_rewards_value);
        if (Utils.isArabicLangSelected(TripConfirmationActivity.this)) {
            isArabicLang = true;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_ROBOTO_BOLD;
            fontDateBold = Constants.FONT_DROIDKUFI_BOLD;
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
            fontMedium = Constants.FONT_DROIDKUFI_REGULAR;
            airlinePnrNumber.setGravity(Gravity.END);
            tripIdNumber.setGravity(Gravity.END);
            dateOfBooking.setGravity(Gravity.END);
            airlineTicketCost.setGravity(Gravity.END);
            taxFare.setGravity(Gravity.END);
            numberOfTickets.setGravity(Gravity.END);
            airFareTotal.setGravity(Gravity.END);
            numberOfCoupon.setGravity(Gravity.END);
            qitafDiscountValue.setGravity(Gravity.END);
            redeemedRewardsValue.setGravity(Gravity.END);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                airlinePnrNumber.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                tripIdNumber.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                dateOfBooking.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                airlineTicketCost.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                taxFare.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                numberOfTickets.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                airFareTotal.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                redeemedRewardsValue.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                numberOfCoupon.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            }
            bookConfirmationText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            thankYouText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            bookingConfirmedText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            emailSendingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            userEmailId.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            airlinePnrNumberText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            airlinePnrNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            tripIdText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            tripIdNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            dateOfBookingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            dateOfBooking.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerDetailsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerInformationText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            airlineTicketCostText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            airlineTicketCost.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            taxFareText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            taxFare.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            numberOfTicketsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            numberOfTickets.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            airFareTotalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            airFareTotal.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            infoTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            infoDetails2.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            numberOfCoupon.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            numberOfCouponText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            qitafDiscountText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            qitafDiscountValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            refundTypeHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            refundTypeDescription.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            rewardsPointsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            redeemedHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            redeemedRewardsValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        titleFace = Typeface.createFromAsset(getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(getAssets(), fontText);
        Typeface boldFace = Typeface.createFromAsset(getAssets(), fontBold);
        Typeface dateBoldFace = Typeface.createFromAsset(getAssets(), fontDateBold);
        Typeface textRegular = Typeface.createFromAsset(getAssets(), fontRegular);
        Typeface textMedium = Typeface.createFromAsset(getAssets(), fontMedium);
        bookConfirmationText.setTypeface(titleFace);
        thankYouText.setTypeface(titleFace);
        bookingConfirmedText.setTypeface(textFace);
        emailSendingText.setTypeface(titleFace);
        userEmailId.setTypeface(titleFace);
        airlinePnrNumberText.setTypeface(textFace);
        airlinePnrNumber.setTypeface(boldFace);
        tripIdText.setTypeface(textFace);
        tripIdNumber.setTypeface(boldFace);
        dateOfBookingText.setTypeface(textFace);
        dateOfBooking.setTypeface(dateBoldFace);
        passengerDetailsText.setTypeface(titleFace);
        passengerInformationText.setTypeface(titleFace);
        airlineTicketCostText.setTypeface(titleFace);
        airlineTicketCost.setTypeface(textRegular);
        taxFareText.setTypeface(titleFace);
        taxFare.setTypeface(textRegular);
        numberOfTicketsText.setTypeface(titleFace);
        numberOfTickets.setTypeface(textRegular);
        numberOfCouponText.setTypeface(titleFace);
        numberOfCoupon.setTypeface(textRegular);
        qitafDiscountText.setTypeface(titleFace);
        qitafDiscountValue.setTypeface(textRegular);
        airFareTotalText.setTypeface(tf);
        airFareTotal.setTypeface(textMedium);
        infoTitle.setTypeface(textMedium);
        infoDetails2.setTypeface(textMedium);
        refundTypeHeader.setTypeface(textRegular);
        refundTypeDescription.setTypeface(textRegular);
        rewardsPointsText.setTypeface(titleFace);
        redeemedHeaderText.setTypeface(titleFace);
        redeemedRewardsValue.setTypeface(textRegular);
        bookingsLayout.setVisibility(View.GONE);
        thankYouText.setText(getString(R.string.label_confirmation_thank_you) + " " + userFullName);
        String firstMsg = getString(R.string.email_sent_confirmation_text);
        emailSendingText.setText(firstMsg + " " + emailId, TextView.BufferType.SPANNABLE);
        Spannable s = (Spannable) emailSendingText.getText();
        int start = firstMsg.length();
        int end = start + emailId.length() + 1;
        s.setSpan(new ForegroundColorSpan(0xFF839abe), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        passengerDetailsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isProfileDataExpanded) {
                    passengerInfoHideLayout.setVisibility(View.GONE);
                    isProfileDataExpanded = true;
                    passengerDetailsImage.setImageResource(R.drawable.down_arrow);
                } else {
                    passengerInfoHideLayout.setVisibility(View.VISIBLE);
                    isProfileDataExpanded = false;
                    passengerDetailsImage.setImageResource(R.drawable.top_arrow);
                }
            }
        });
        priceInformationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPriceDetailsExpanded) {
                    priceInformationHideLayout.setVisibility(View.GONE);
                    isPriceDetailsExpanded = true;
                    priceInfoDownImage.setImageResource(R.drawable.down_arrow);
                } else {
                    priceInformationHideLayout.setVisibility(View.VISIBLE);
                    isPriceDetailsExpanded = false;
                    priceInfoDownImage.setImageResource(R.drawable.top_arrow);
                }
            }
        });
        homeLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TripConfirmationActivity.this, MainSearchActivity.class);
                intent.putExtra("isNew", false);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        facebookLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://www.facebook.com/flyin.com"));
                startActivity(intent);
            }
        });
        twitterLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://twitter.com/flyincom"));
                startActivity(intent);
            }
        });
        googlePlusLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://plus.google.com/+flyin/posts"));
                startActivity(intent);
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        ImageView mImageView = (ImageView) mCustomView.findViewById(R.id.home_icon);
        ImageView mHomeIcon = (ImageView) mCustomView.findViewById(R.id.home_logo);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.btn_singin);
        mTitleTextView.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(TripConfirmationActivity.this)) {
            mImageView.setScaleType(ImageView.ScaleType.FIT_END);
        }
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TripConfirmationActivity.this, MenuActivity.class);
                intent.putExtra("isNew", false);
                startActivity(intent);
            }
        });
        mHomeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnToHomePage();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        mActionBar.setCustomView(mCustomView, layout);
        mActionBar.setDisplayShowCustomEnabled(true);
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        if (isInternetPresent) {
            getRequestFromServer(requestJSON);
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(titleFace);
            errorDescriptionText.setTypeface(titleFace);
            searchButton.setTypeface(titleFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(TripConfirmationActivity.this);
                    if (isInternetPresent) {
                        loadingViewLayout.removeView(errorView);
                        getRequestFromServer(requestJSON);
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
    }

    private void getRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(TripConfirmationActivity.this, TripConfirmationActivity.this, Constants.MY_TRIPS_URL,
                "", requestJSON, GET_BOOKED_ID_RESULTS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "DATA::" + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            closeLoading();
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(titleFace);
            errorDescriptionText.setTypeface(titleFace);
            searchButton.setTypeface(titleFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    returnToHomePage();
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        } else {
            try {
                if (serviceType == GET_BOOKED_ID_RESULTS) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(data);
                        if (obj.has("bookingDetails")) {
                            ParseJSONTask task = new ParseJSONTask();
                            task.execute(data);
                        } else {
                            closeLoading();
                            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View errorView = inflater.inflate(R.layout.view_error_response, null);
                            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                            errorText = (TextView) errorView.findViewById(R.id.error_text);
                            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                            searchButton = (Button) errorView.findViewById(R.id.search_button);
                            errorText.setTypeface(titleFace);
                            errorDescriptionText.setTypeface(titleFace);
                            searchButton.setTypeface(titleFace);
                            searchButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    returnToHomePage();
                                }
                            });
                            loadErrorType(Constants.WRONG_ERROR_PAGE);
                            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            loadingViewLayout.addView(errorView);
                        }
                    } catch (Exception e) {
                    }
                } else if (serviceType == GET_LOYALTY_POINTS) {
                    closeLoading();
                    try {
                        JSONObject obj = new JSONObject(data);
                        String status = obj.getString("status");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            String points = obj.getString("earnPoints");
                            Utils.printMessage(TAG, "earn points::" + points);
                            handleLoyaltyConfirmService(points);
                            rewardsLayout.setVisibility(View.VISIBLE);
                            rewardsPointsText.setText(String.format(getResources().getString(R.string.label_rewards_earned_points), points));
                        } else {
                            Utils.printMessage(TAG, "earn points::" + obj.getString("message"));
                            rewardsLayout.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                    }
                } else if (serviceType == GET_CONFIRM_LOYALTY_RESULT) {
                    closeLoading();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(titleFace);
        descriptionText.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(TripConfirmationActivity.this)) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    private void setPassengerInfo(PassengersBean passengerInfo) {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View passengerView = inflater.inflate(R.layout.view_passenger_details, null);
        passengerInfoHideLayout.addView(passengerView);
        TextView passengerNameText = (TextView) passengerView.findViewById(R.id.passenger_name_text);
        TextView passengerTicketText = (TextView) passengerView.findViewById(R.id.passenger_ticket_text);
        passengerNameText.setTypeface(titleFace);
        passengerTicketText.setTypeface(titleFace);
        if (Utils.isArabicLangSelected(TripConfirmationActivity.this)) {
            passengerNameText.setGravity(Gravity.END);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                passengerNameText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            }
            passengerNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerTicketText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        if (!passengerInfo.isSecondPassenger()) {
            passengerNameText.setText(Utils.checkStringVal(passengerInfo.getPersonalDetailsObjectBean().getTitle()) + " " + passengerInfo.getPersonalDetailsObjectBean().getFirstName() + " " + passengerInfo.getPersonalDetailsObjectBean().getLastName() + " - " + Utils.checkStringVal(passengerMail)); // + " - " + passengerMail
            passengerTicketText.setText(getString(R.string.label_ticket_number_text) + " – " + Utils.checkStringVal(passengerInfo.getPersonalDetailsObjectBean().getTicketNo()));
        } else {
            passengerNameText.setText(Utils.checkStringVal(passengerInfo.getPersonalDetailsObjectBean().getTitle()) + " " + passengerInfo.getPersonalDetailsObjectBean().getFirstName() + " " + passengerInfo.getPersonalDetailsObjectBean().getLastName());
            passengerTicketText.setText(getString(R.string.label_ticket_number_text) + " – " + Utils.checkStringVal(passengerInfo.getPersonalDetailsObjectBean().getTicketNo()));
        }
    }

    private void setPriceDetails(PriceBean data) {
        Double ticketCost = Double.parseDouble(data.getBaseFare());
        Double taxCost = Double.parseDouble(data.getTax());
        Double airFareTotalCost = Double.parseDouble(data.getTotal());
        Double totalTicketPrice = 0.0;
        int qitafAmount = 0;
        if (Double.parseDouble(data.getQitafAmount()) > 0) {
            Double qitafDisc = Double.parseDouble(data.getQitafAmount());
            qitafAmount = (int) Math.ceil(qitafDisc);
            qitafDiscountLayout.setVisibility(View.VISIBLE);
        } else {
            qitafDiscountLayout.setVisibility(View.GONE);
        }
        String currencyName = "";
        if (data.getUserCurrency().equalsIgnoreCase("SAR")) {
            currencyName = getString(R.string.label_SAR_currency_name);
        } else {
            currencyName = data.getUserCurrency();
        }
        String defaultCurr = getString(R.string.label_SAR_currency_name); //label_qitaf_currency_tag
        if (!cua.isEmpty()) {
            if (Double.parseDouble(cua) == 0.0) {
                couponLayout.setVisibility(View.GONE);
                couponView.setVisibility(View.GONE);
                totalTicketPrice = airFareTotalCost;
            } else {
                couponLayout.setVisibility(View.VISIBLE);
                couponView.setVisibility(View.VISIBLE);
                totalTicketPrice = airFareTotalCost - Double.parseDouble(cua);
                if (data.getUserCurrency().equalsIgnoreCase("SAR")) {
                    if (Utils.isArabicLangSelected(TripConfirmationActivity.this)) {
                        numberOfCoupon.setText(String.format(getApplicationContext().getResources().getString(R.string.label_placeholder), cua, currencyName));
                    } else {
                        numberOfCoupon.setText(String.format(getApplicationContext().getResources().getString(R.string.label_placeholder), currencyName, cua));
                    }
                } else {
                    numberOfCoupon.setText(String.format(getApplicationContext().getResources().getString(R.string.label_placeholder), currencyName, cua));
                }
            }
        } else {
            totalTicketPrice = airFareTotalCost;
            couponLayout.setVisibility(View.GONE);
            couponView.setVisibility(View.GONE);
        }
        String finalPaidPrice = String.format(Locale.ENGLISH, "%.2f", totalTicketPrice);
        if (data.getUserCurrency().equalsIgnoreCase("SAR")) {
            if (Utils.isArabicLangSelected(TripConfirmationActivity.this)) {
                airlineTicketCost.setText(ticketCost + " " + currencyName);
                taxFare.setText(taxCost + " " + currencyName);
                airFareTotal.setText(finalPaidPrice + " " + currencyName);
            } else {
                airlineTicketCost.setText(currencyName + " " + ticketCost);
                taxFare.setText(currencyName + " " + taxCost);
                airFareTotal.setText(currencyName + " " + finalPaidPrice);
            }
        } else {
            airlineTicketCost.setText(currencyName + " " + ticketCost);
            taxFare.setText(currencyName + " " + taxCost);
            airFareTotal.setText(currencyName + " " + finalPaidPrice);
        }
        if (isArabicLang) {
            qitafDiscountValue.setText(qitafAmount + " " + defaultCurr);
        } else {
            qitafDiscountValue.setText(defaultCurr + " " + qitafAmount);
        }
        numberOfTickets.setText(String.valueOf(numberOfPassengers));
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(getResources().getString(R.string.label_network_error_message));
                searchButton.setText(getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getResources().getString(R.string.label_no_results_found_message));
                searchButton.setText(getResources().getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        returnToHomePage();
    }

    private void returnToHomePage() {
        Intent intent = new Intent(TripConfirmationActivity.this, MainSearchActivity.class);
        intent.putExtra("isNew", false);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private class ParseJSONTask extends AsyncTask<String, Void, String> {
        JSONObject obj = null;

        @Override
        protected String doInBackground(String... urls) {
            try {
                obj = new JSONObject(urls[0]);
                if (obj.has("bookingDetails")) {
                    if (obj.getJSONObject("bookingDetails").has("bookings")) {
                        JSONArray bookingsArr = obj.getJSONObject("bookingDetails").getJSONArray("bookings");
                        bookingMainArray.clear();
                        for (int i = 0; i < bookingsArr.length(); i++) {
                            BookingsObject bookingsObject = new BookingsObject();
                            if (bookingsArr.getJSONObject(i).has("entityDetails")) {
                                EntityDetailsObjectBean entityDetailsObjectBean = new EntityDetailsObjectBean();
                                if (bookingsArr.getJSONObject(i).getJSONObject("entityDetails").has("flightDetails")) {
                                    airlinePnrArr.clear();
                                    FlightDetailsObjectBean flightDetailsObjectBean = new FlightDetailsObjectBean();
                                    JSONArray flightsArray = bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("flightDetails").getJSONArray("flights");
                                    ArrayList<FlightsDataBean> flightsDataBeanArrayList = new ArrayList<>();
                                    for (int j = 0; j < flightsArray.length(); j++) {
                                        JSONObject flightsObj = flightsArray.getJSONObject(j);
                                        FlightsDataBean flightsDataBean = new FlightsDataBean();
                                        flightsDataBean.setDepartureAirport(flightsObj.getString("departureAirport"));
                                        flightsDataBean.setArrivalAirport(flightsObj.getString("arrivalAirport"));
                                        flightsDataBean.setOperatingAirline(flightsObj.getString("operatingAirline"));
                                        flightsDataBean.setMarketingAirline(flightsObj.getString("marketingAirline"));
                                        flightsDataBean.setDuration(flightsObj.getString("duration"));
                                        flightsDataBean.setDepartureDate(flightsObj.getString("departureDate"));
                                        flightsDataBean.setArrivalDate(flightsObj.getString("arrivalDate"));
                                        flightsDataBean.setEquipmentType(flightsObj.getString("equipmentType"));
                                        flightsDataBean.setClassType(flightsObj.getString("classType"));
                                        flightsDataBean.setTerminalinfo(Utils.checkStringVal(flightsObj.getString("terminalinfo")));
                                        flightsDataBean.setBaggageInfo(flightsObj.getString("baggageInfo"));
                                        flightsDataBean.setAirlinePNR(flightsObj.getString("airlinePNR"));
                                        flightsDataBean.setJourneyType(flightsObj.getString("journeyType"));
                                        flightsDataBean.setJourneyOrder(flightsObj.getString("journeyOrder"));
                                        flightsDataBean.setLegOrder(flightsObj.getString("legOrder"));
                                        flightsDataBean.setFlightNumber(flightsObj.getString("flightNumber"));
                                        if (!airlinePnrArr.contains(flightsObj.getString("airlinePNR"))) {
                                            airlinePnrArr.add(flightsObj.getString("airlinePNR"));
                                        }
                                        String orderType = "";
                                        ArrayList<String> orderTypeArr = new ArrayList<String>();
                                        for (int k = 0; k < flightsArray.length(); k++) {
                                            orderType = flightsArray.getJSONObject(k).getString("journeyType");
                                            if (orderType.equalsIgnoreCase("M")) {
                                                if (flightsDataBean.getLegOrder().equalsIgnoreCase(flightsArray.getJSONObject(k).getString("journeyOrder"))) {
                                                    orderTypeArr.add(orderType);
                                                }
                                            } else {
                                                orderTypeArr.add(orderType);
                                            }
                                        }
                                        flightsDataBean.setStopsCount((checkOrderCount(flightsObj.getString("journeyType"), orderTypeArr)) - 1);
                                        flightsDataBeanArrayList.add(flightsDataBean);
                                    }
                                    flightDetailsObjectBean.setFlightsDataBeanArrayList(flightsDataBeanArrayList);
                                    bookingsObject.setFlightDepartDate(Utils.dateFormatToMinutesFormat(flightsDataBeanArrayList.get(0).getDepartureDate()));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("flightDetails").has("cancellationPolicy")) {
                                        CancellationPolicyBean cancellationPolicyBean = new CancellationPolicyBean();
                                        ArrayList<String> notesArray = new ArrayList<>();
                                        for (int j = 0; j < bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("flightDetails").getJSONObject("cancellationPolicy").getJSONArray("notes").length(); j++) {
                                            String refundTypeValue = bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("flightDetails").getJSONObject("cancellationPolicy").getJSONArray("notes").getString(j);
                                            fareTypeArray = Utils.checkRefundTypeResult(refundTypeValue);
                                            notesArray.add(refundTypeValue);
                                        }
                                        cancellationPolicyBean.setNotesArray(notesArray);
                                        flightDetailsObjectBean.setCancellationPolicyObjectBean(cancellationPolicyBean);
                                    }
                                    flightDetailsObjectBean.setGdsPnr(bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("flightDetails").getString("gdsPnr"));
                                    entityDetailsObjectBean.setFlightDetailsBean(flightDetailsObjectBean);
                                }
                                entityDetailsObjectBean.setHotelDetails(bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getString("hotelDetails"));
                                bookingsObject.setEntityDetailsObjectBean(entityDetailsObjectBean);
                            }
                            if (bookingsArr.getJSONObject(i).has("generalDetails")) {
                                GeneralDetailsObjectBean generalDetailsObjectBean = new GeneralDetailsObjectBean();
                                generalDetailsObjectBean.setBookingStatus(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("bookingStatus"));
                                generalDetailsObjectBean.setFlyinCode(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("flyinCode"));
                                generalDetailsObjectBean.setBookingDate(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("bookingDate"));
                                generalDetailsObjectBean.setCustomerId(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("customerId"));
                                bookingsObject.setGeneralDetailsObjectBean(generalDetailsObjectBean);
                            }
                            if (bookingsArr.getJSONObject(i).has("passengerDetails")) {
                                PassengerDetailsObjectBean passengerDetailsObjectBean = new PassengerDetailsObjectBean();
                                if (bookingsArr.getJSONObject(i).getJSONObject("passengerDetails").has("passengers")) {
                                    JSONArray passengerArray = bookingsArr.getJSONObject(i).getJSONObject("passengerDetails").getJSONArray("passengers");
                                    ArrayList<PassengersBean> passengersBeanArrayList = new ArrayList<>();
                                    for (int j = 0; j < passengerArray.length(); j++) {
                                        PassengersBean passengersBean = new PassengersBean();
                                        passengersBean.setMealPreference(passengerArray.getJSONObject(j).getString("mealPreference"));
                                        passengersBean.setSeatPreference(passengerArray.getJSONObject(j).getString("seatPreference"));
                                        passengersBean.setSpecialPreference(passengerArray.getJSONObject(j).getString("specialPreference"));
                                        if (passengerArray.getJSONObject(j).has("personalDetails")) {
                                            PersonalObjectBean personalDetailsObjectBeans = new PersonalObjectBean();
                                            personalDetailsObjectBeans.setTitle(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("title"));
                                            personalDetailsObjectBeans.setFirstName(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("firstName"));
                                            personalDetailsObjectBeans.setMiddleName(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("middleName"));
                                            personalDetailsObjectBeans.setLastName(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("lastName"));
                                            personalDetailsObjectBeans.setTicketNo(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("ticketNo"));
                                            passengersBean.setPersonalDetailsObjectBean(personalDetailsObjectBeans);
                                        }
                                        passengersBeanArrayList.add(passengersBean);
                                        passengerDetailsObjectBean.setPassengersBeanArrayList(passengersBeanArrayList);
                                    }
                                    bookingsObject.setPassengerDetailsObjectBean(passengerDetailsObjectBean);
                                }
                            }
                            if (bookingsArr.getJSONObject(i).has("priceDetails")) {
                                PriceBean priceBean = new PriceBean();
                                if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").has("price"))
                                    priceBean.setTotal(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("total"));
                                priceBean.setTax(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("tax"));
                                priceBean.setBaseFare(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("baseFare"));
                                priceBean.setUserCurrency(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("userCurrency"));
                                try {
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("cancellationCharge"))
                                        priceBean.setCancellationCharge(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("cancellationCharge"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("reIssueCharge"))
                                        priceBean.setReIssueCharge(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("reIssueCharge"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("extraBaggageAmount"))
                                        priceBean.setExtraBaggageAmount(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("extraBaggageAmount"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("couponDiscount"))
                                        priceBean.setCouponDiscount(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("couponDiscount"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("disountPrice"))
                                        priceBean.setDisountPrice(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("disountPrice"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("qitafAmount"))
                                        priceBean.setQitafAmount(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("qitafAmount"));
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("loyaltydetails")) {
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").has("earnpoint")) {
                                            priceBean.setEarnPoints(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").getString("earnpoint"));
                                        }
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").has("redeempoint")) {
                                            priceBean.setRedeemPoint(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").getString("redeempoint"));
                                        }
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").has("rdmdiscountusr")) {
                                            priceBean.setRdmDiscountusr(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").getString("rdmdiscountusr"));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.getMessage();
                                }
                                bookingsObject.setPriceBean(priceBean);
                            }
                            bookingMainArray.add(bookingsObject);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            handleDataUpdate();
        }
    }

    private int checkOrderCount(String value, ArrayList<String> orderType) {
        int stopsCount = 0;
        for (int i = 0; i < orderType.size(); i++) {
            if (value.equalsIgnoreCase(orderType.get(i))) {
                stopsCount++;
            }
        }
        return stopsCount;
    }

    private void setFlightDetailsInfo(FlightsDataBean data) {
        try {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_trip_detail_confirmation, null);
            flightInformationLayout.addView(view);
            LinearLayout odoHeaderLayout = (LinearLayout) view.findViewById(R.id.odo_header_layout);
            TextView departHeaderText = (TextView) view.findViewById(R.id.departure_text);
            TextView departText = (TextView) view.findViewById(R.id.departure_journey);
            TextView departStops = (TextView) view.findViewById(R.id.departure_stops);
            ImageView airlineImage = (ImageView) view.findViewById(R.id.airline_image);
            TextView departFrom = (TextView) view.findViewById(R.id.departure_from);
            TextView departCountry = (TextView) view.findViewById(R.id.departure_country);
            TextView departLocation = (TextView) view.findViewById(R.id.departure_location);
            TextView departTerminal = (TextView) view.findViewById(R.id.depart_terminal);
            TextView departTime = (TextView) view.findViewById(R.id.depatingtime);
            TextView departDate = (TextView) view.findViewById(R.id.departure_date);
            TextView journeyAirlineName = (TextView) view.findViewById(R.id.journey_airline_name);
            TextView journeyAirlineDetails = (TextView) view.findViewById(R.id.journey_airline_details);
            TextView arrivalTo = (TextView) view.findViewById(R.id.arrival_to);
            TextView arrivalCountry = (TextView) view.findViewById(R.id.arrival_country);
            TextView arrivalLocation = (TextView) view.findViewById(R.id.arrival_location);
            TextView arrivalTerminal = (TextView) view.findViewById(R.id.arrival_terminal);
            TextView arrivalTime = (TextView) view.findViewById(R.id.arrival_time);
            TextView arrivalDate = (TextView) view.findViewById(R.id.arrival_date);
            TextView flightStops = (TextView) view.findViewById(R.id.flight_stops);
            TextView flightDuration = (TextView) view.findViewById(R.id.flight_duration);
            TextView flightRefundStatus = (TextView) view.findViewById(R.id.flight_refund_status);
            LinearLayout layoverLayout = (LinearLayout) view.findViewById(R.id.layover_layout);
            TextView layoverText = (TextView) view.findViewById(R.id.layover_text);
            TextView secondDepartText = (TextView) view.findViewById(R.id.second_departure_text);
            TextView baggageInformation = (TextView) view.findViewById(R.id.baggage_information);
            ImageView busIcon = (ImageView) view.findViewById(R.id.depart_bus_image);
            if (Utils.isArabicLangSelected(TripConfirmationActivity.this)) {
                departText.setGravity(Gravity.START);
                secondDepartText.setGravity(Gravity.START);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    departText.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                    secondDepartText.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                }
                departHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                departText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                departStops.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                layoverText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                secondDepartText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                departFrom.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                departCountry.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                departLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                departTerminal.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                departTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                departDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                journeyAirlineName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                journeyAirlineDetails.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                arrivalTo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                arrivalCountry.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                arrivalLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                arrivalTerminal.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                arrivalTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                arrivalDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                flightStops.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                flightDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                flightRefundStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                baggageInformation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            }
            departHeaderText.setTypeface(titleFace);
            departText.setTypeface(titleFace);
            departStops.setTypeface(titleFace);
            departFrom.setTypeface(titleFace);
            departCountry.setTypeface(textFace);
            departLocation.setTypeface(textFace);
            departTerminal.setTypeface(textFace);
            departTime.setTypeface(textFace);
            departDate.setTypeface(textFace);
            journeyAirlineName.setTypeface(textFace);
            journeyAirlineDetails.setTypeface(textFace);
            arrivalTo.setTypeface(titleFace);
            arrivalCountry.setTypeface(textFace);
            arrivalLocation.setTypeface(textFace);
            arrivalTerminal.setTypeface(textFace);
            arrivalTime.setTypeface(textFace);
            arrivalDate.setTypeface(textFace);
            flightStops.setTypeface(textFace);
            flightDuration.setTypeface(titleFace);
            flightRefundStatus.setTypeface(textFace);
            layoverText.setTypeface(titleFace);
            secondDepartText.setTypeface(titleFace);
            baggageInformation.setTypeface(textFace);
            if (data.isLayover()) {
                odoHeaderLayout.setVisibility(View.GONE);
                layoverLayout.setVisibility(View.VISIBLE);
            } else {
                odoHeaderLayout.setVisibility(View.VISIBLE);
                layoverLayout.setVisibility(View.GONE);
                if (data.getEquipmentType().equalsIgnoreCase("BUS")) {
                    busIcon.setVisibility(View.VISIBLE);
                }
            }
            if (journeyType.equalsIgnoreCase("O")) {
                if (data.getLegOrder().equalsIgnoreCase("1")) {
                    departHeaderText.setText(getString(R.string.label_search_flight_departure));
                    if (fareTypeArray.get(0).equalsIgnoreCase("refundable") || fareTypeArray.get(0).equalsIgnoreCase("refundable before departure")) {
                        flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                        flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                    } else {
                        flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                        flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                    }
                }
            } else if (journeyType.equalsIgnoreCase("R")) {
                if (data.getLegOrder().equalsIgnoreCase("2")) {
                    departHeaderText.setText(getString(R.string.label_search_flight_return));
                    if (fareTypeArray.size() > 1) {
                        if (fareTypeArray.get(1).equalsIgnoreCase("refundable") || fareTypeArray.get(1).equalsIgnoreCase("refundable before departure")) {
                            flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                            flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                        } else {
                            flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                            flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                        }
                    } else {
                        if (fareTypeArray.get(0).equalsIgnoreCase("refundable") || fareTypeArray.get(0).equalsIgnoreCase("refundable before departure")) {
                            flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                            flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                        } else {
                            flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                            flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                        }
                    }
                }
            } else if (journeyType.equalsIgnoreCase("M")) {
                if (data.getLegOrder().equalsIgnoreCase("1")) {
                    departHeaderText.setText(getString(R.string.label_search_flight_departure));
                    if (fareTypeArray.get(0).equalsIgnoreCase("refundable") || fareTypeArray.get(0).equalsIgnoreCase("refundable before departure")) {
                        flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                        flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                    } else {
                        flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                        flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                    }
                } else if (data.getLegOrder().equalsIgnoreCase("2")) {
                    departHeaderText.setText(getString(R.string.label_search_flight_departure));
                    if (fareTypeArray.size() > 1) {
                        if (fareTypeArray.get(1).equalsIgnoreCase("refundable") || fareTypeArray.get(1).equalsIgnoreCase("refundable before departure")) {
                            flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                            flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                        } else {
                            flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                            flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                        }
                    } else {
                        if (fareTypeArray.get(0).equalsIgnoreCase("refundable") || fareTypeArray.get(0).equalsIgnoreCase("refundable before departure")) {
                            flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                            flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                        } else {
                            flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                            flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                        }
                    }
                } else if (data.getLegOrder().equalsIgnoreCase("3")) {
                    departHeaderText.setText(getString(R.string.label_search_flight_departure));
                    if (fareTypeArray.size() > 2) {
                        if (fareTypeArray.get(2).equalsIgnoreCase("refundable") || fareTypeArray.get(2).equalsIgnoreCase("refundable before departure")) {
                            flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                            flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                        } else {
                            flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                            flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                        }
                    } else if (fareTypeArray.size() > 1) {
                        if (fareTypeArray.get(1).equalsIgnoreCase("refundable") || fareTypeArray.get(1).equalsIgnoreCase("refundable before departure")) {
                            flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                            flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                        } else {
                            flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                            flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                        }
                    } else {
                        if (fareTypeArray.get(0).equalsIgnoreCase("refundable") || fareTypeArray.get(0).equalsIgnoreCase("refundable before departure")) {
                            flightRefundStatus.setTextColor(Color.parseColor("#4BB43F"));
                            flightRefundStatus.setText(" " + getString(R.string.RefLbl));
                        } else {
                            flightRefundStatus.setTextColor(Color.parseColor("#FF0000"));
                            flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
                        }
                    }
                }
            }
            if (data.getStopsCount() == 0) {
                departStops.setText(R.string.label_non_stop_text);
            } else if (data.getStopsCount() == 1) {
                departStops.setText(data.getStopsCount() + " " + getString(R.string.label_stop_text));
            } else {
                departStops.setText(data.getStopsCount() + " " + getString(R.string.label_stops_text));
            }
            try {
                if (data.getTerminalinfo().equalsIgnoreCase(null) || !data.getTerminalinfo().equalsIgnoreCase("")) {
                    //Utils.printMessage(TAG, "Terminal is :: "+data.getTerminalinfo());
                    if (data.getTerminalinfo().contains("|")) {
                        String[] terminalText = data.getTerminalinfo().split("\\|", -1);
                        if (!terminalText[0].isEmpty()) {
                            departTerminal.setVisibility(View.VISIBLE);
                            departTerminal.setText(getString(R.string.label_terminal) + " " + terminalText[0]);
                        }
                        if (!terminalText[1].isEmpty()) {
                            arrivalTerminal.setVisibility(View.VISIBLE);
                            arrivalTerminal.setText(getString(R.string.label_terminal) + " " + terminalText[1]);
                        }
                    } else {
                        departTerminal.setVisibility(View.VISIBLE);
                        departTerminal.setText(getString(R.string.label_terminal) + " " + data.getTerminalinfo());
                    }
                } else {
                    departTerminal.setVisibility(View.GONE);
                    arrivalTerminal.setVisibility(View.GONE);
                }
            } catch (Exception e) {
            }
            String departureAirportName = airportNamesMap.get(data.getDepartureAirport()).getAirpotName();
            String arrivalAirportName = airportNamesMap.get(data.getArrivalAirport()).getAirpotName();
            departText.setText(data.getHeadingMessage());
            secondDepartText.setText(data.getHeadingMessage());
            String imagePath = Constants.IMAGE_BASE_URL + data.getMarketingAirline() + Constants.IMAGE_FILE_FORMAT;
            Glide.with(TripConfirmationActivity.this).load(imagePath).placeholder(R.drawable.imagethumb_search).into(airlineImage);
            journeyAirlineName.setText(airLineNamesMap.get(data.getOperatingAirline()));
            journeyAirlineDetails.setText(data.getOperatingAirline() + " " + data.getFlightNumber());
//            departFrom.setText(data.getDepartureAirport() + " " + "Hyd");
//            departCountry.setText("IN");
            departFrom.setText(data.getDepartureAirport() + " " + airportNamesMap.get(data.getDepartureAirport()).getCityName());
            departCountry.setText(airportNamesMap.get(data.getDepartureAirport()).getCityCode());
            departLocation.setText(departureAirportName);
            departTime.setText(Utils.formatTripDateToTimeFormat(data.getDepartureDate(), TripConfirmationActivity.this));
            departDate.setText(Utils.formatTripDateToDayFormat(data.getDepartureDate(), TripConfirmationActivity.this));
//            arrivalTo.setText(data.getArrivalAirport() + " " + "DXB");
//            arrivalCountry.setText("AE");
            arrivalTo.setText(data.getArrivalAirport() + " " + airportNamesMap.get(data.getArrivalAirport()).getCityName());
            arrivalCountry.setText(airportNamesMap.get(data.getArrivalAirport()).getCityCode());
            arrivalLocation.setText(arrivalAirportName);
            arrivalTime.setText(Utils.formatTripDateToTimeFormat(data.getArrivalDate(), TripConfirmationActivity.this));
            String arrivalCityDate = Utils.formatTripDateToDayFormat(data.getArrivalDate(), TripConfirmationActivity.this);
            arrivalDate.setText(arrivalCityDate);
            String classType = "";
            if (data.getClassType().equalsIgnoreCase("Economy")) {
                classType = getString(R.string.label_search_flight_economy);
            } else if (data.getClassType().equalsIgnoreCase("Business")) {
                classType = getString(R.string.label_search_flight_business);
            } else if (data.getClassType().equalsIgnoreCase("First")) {
                classType = getString(R.string.label_search_flight_first);
            }
            flightStops.setText(classType);
            String flightDurationValue = Utils.minutesToTimeFormat(data.getDuration(), TripConfirmationActivity.this);
            if (!flightDurationValue.isEmpty()) {
                flightDuration.setText(getString(R.string.label_duration_text) + " " + flightDurationValue);
            }
            try {
                String checkInDetails = Utils.checkStringVal(data.getBaggageInfo());
                if (!checkInDetails.equalsIgnoreCase("")) {
                    String baggageInfo = "";
                    String[] baggageDetails = checkInDetails.split(" ");
                    if (baggageDetails[1].equalsIgnoreCase("Kilos")) {
                        baggageInfo = baggageDetails[0] + " " + getString(R.string.label_kilo);
                    } else if (baggageDetails[1].equalsIgnoreCase("Pieces")) {
                        baggageInfo = baggageDetails[0] + " " + getString(R.string.label_pieces);
                    } else {
                        baggageInfo = checkInDetails;
                    }
                    if (Utils.isArabicLangSelected(TripConfirmationActivity.this)) {
                        baggageInformation.setText(getString(R.string.label_reviewPage_baggageInfo) + " '" + baggageInfo + "' (" + getString(R.string.label_per_person_text) + ")");
                    } else {
                        baggageInformation.setText(getString(R.string.label_reviewPage_baggageInfo) + " " + getString(R.string.label_reviewPage_checkIn) + " '" + baggageInfo + "' (" + getString(R.string.label_per_person_text) + ")");
                    }
                } else {
                    baggageInformation.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
    }

    private String getHeaderMessage(LayoverDataBean layoverDataBean) {
        int count = -1;
        for (int i = 0; i < bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().size(); i++) {
            if (layoverDataBean.getJourneyType().equalsIgnoreCase(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getJourneyType())) {
                count++;
                if (layoverDataBean.getDepartureName().equalsIgnoreCase("")) {
                    layoverDataBean.setDepartureName(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getDepartureAirport());
                }
                layoverDataBean.setArrivalName(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getArrivalAirport());
                layoverDataBean.setStopsCount(count);
            }
        }
        String fromCity = airportNamesMap.get(layoverDataBean.getDepartureName()).getCityName();
        String toCity = airportNamesMap.get(layoverDataBean.getArrivalName()).getCityName();
        layoverDataBean.setHeaderMessage(fromCity + " - " + toCity);
        return layoverDataBean.getHeaderMessage();
    }

    private String getMultiCityHeaderMessage(LayoverDataBean layoverDataBean) {
        int stopCount = -1;
        for (int i = 0; i < bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().size(); i++) {
            if (layoverDataBean.getJourneyType().equalsIgnoreCase(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getJourneyType())) {
                if (layoverDataBean.getLegOrder().equalsIgnoreCase(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getLegOrder())) {
                    stopCount++;
                    if (layoverDataBean.getDepartureName().equalsIgnoreCase("")) {
                        layoverDataBean.setDepartureName(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getDepartureAirport());
                    }
                    layoverDataBean.setArrivalName(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getArrivalAirport());
                    layoverDataBean.setStopsCount(stopCount);
                }
            }
        }
        String fromCity = airportNamesMap.get(layoverDataBean.getDepartureName()).getCityName();
        String toCity = airportNamesMap.get(layoverDataBean.getArrivalName()).getCityName();
        layoverDataBean.setHeaderMessage(fromCity + " - " + toCity);
        return layoverDataBean.getHeaderMessage();
    }

    private void handleDataUpdate() {
        LayoverDataBean layoverDataBean = new LayoverDataBean();
        for (int j = 0; j < bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().size(); j++) {
            FlightsDataBean flightsDataBean = bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j);
            journeyType = bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getJourneyType();
            legOrder = bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getLegOrder();
            if (journeyType.equalsIgnoreCase("O")) {
                if (legOrder.equalsIgnoreCase("1")) {
                    firstLegOrder++;
                    layoverDataBean.setJourneyType("O");
                    if (firstLegOrder == 1) {
                        flightsDataBean.setIsLayover(false);
                        flightsDataBean.setHeadingMessage(getHeaderMessage(layoverDataBean));
                    } else {
                        flightsDataBean.setIsLayover(true);
                        flightsDataBean.setHeadingMessage(Utils.getDifferenceBetweenTimeInHrMinsFormat(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getDepartureDate(), previousTime, TripConfirmationActivity.this));
                    }
                    flightsDataBean.setStopsCount(layoverDataBean.getStopsCount());
                    previousTime = bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getArrivalDate();
                }
            } else if (journeyType.equalsIgnoreCase("R")) {
                if (legOrder.equalsIgnoreCase("2")) {
                    secondLegOrder++;
                    layoverDataBean.setJourneyType("R");
                    layoverDataBean.setDepartureName("");
                    if (secondLegOrder == 1) {
                        flightsDataBean.setIsLayover(false);
                        flightsDataBean.setHeadingMessage(getHeaderMessage(layoverDataBean));
                    } else {
                        flightsDataBean.setIsLayover(true);
                        flightsDataBean.setHeadingMessage(Utils.getDifferenceBetweenTimeInHrMinsFormat(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getDepartureDate(), previousTime, TripConfirmationActivity.this));
                    }
                    flightsDataBean.setStopsCount(layoverDataBean.getStopsCount());
                    previousTime = bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getArrivalDate();
                }
            } else if (journeyType.equalsIgnoreCase("M")) {
                firstLegOrder++;
                secondLegOrder++;
                layoverDataBean.setJourneyType("M");
                layoverDataBean.setDepartureName("");
                if (legOrder.equalsIgnoreCase("1")) {
                    forwardLegOrder++;
                    layoverDataBean.setLegOrder("1");
                    if (forwardLegOrder == 1) {
                        flightsDataBean.setIsLayover(false);
                        flightsDataBean.setHeadingMessage(getMultiCityHeaderMessage(layoverDataBean));
                    } else {
                        flightsDataBean.setIsLayover(true);
                        flightsDataBean.setHeadingMessage(Utils.getDifferenceBetweenTimeInHrMinsFormat(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getDepartureDate(), previousTime, TripConfirmationActivity.this));
                    }
                    flightsDataBean.setStopsCount(layoverDataBean.getStopsCount());
                    previousTime = bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getArrivalDate();
                }
                if (legOrder.equalsIgnoreCase("2")) {
                    layoverDataBean.setLegOrder("2");
                    multiTripStopCount2++;
                    if (multiTripStopCount2 == 1) {
                        flightsDataBean.setIsLayover(false);
                        flightsDataBean.setHeadingMessage(getMultiCityHeaderMessage(layoverDataBean));
                    } else {
                        flightsDataBean.setIsLayover(true);
                        flightsDataBean.setHeadingMessage(Utils.getDifferenceBetweenTimeInHrMinsFormat(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getDepartureDate(), previousTime, TripConfirmationActivity.this));
                    }
                    flightsDataBean.setStopsCount(layoverDataBean.getStopsCount());
                    previousTime = bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getArrivalDate();
                }
                if (legOrder.equalsIgnoreCase("3")) {
                    layoverDataBean.setLegOrder("3");
                    multiTripStopCount3++;
                    if (multiTripStopCount3 == 1) {
                        flightsDataBean.setIsLayover(false);
                        flightsDataBean.setHeadingMessage(getMultiCityHeaderMessage(layoverDataBean));
                    } else {
                        flightsDataBean.setIsLayover(true);
                        flightsDataBean.setHeadingMessage(Utils.getDifferenceBetweenTimeInHrMinsFormat(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getDepartureDate(), previousTime, TripConfirmationActivity.this));
                    }
                    flightsDataBean.setStopsCount(layoverDataBean.getStopsCount());
                    previousTime = bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j).getArrivalDate();
                }
            }
            setFlightDetailsInfo(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(j));
        }
        if (airlinePnrArr.size() > 0) {
            Utils.printMessage(TAG, "PNR arr :: " + airlinePnrArr);
            if(airlinePnrArr.size() == 1){
                if(airlinePnrArr.get(0) == null || airlinePnrArr.get(0).equalsIgnoreCase("null")){
                    airlinePnrNumber.setText(Utils.checkStringVal(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getGdsPnr()));
                }else {
                    airlinePnrNumber.setText(airlinePnrArr.get(0));
                }
            }else {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < airlinePnrArr.size(); i++) {
                    if(airlinePnrArr.get(i) == null || airlinePnrArr.get(i).equalsIgnoreCase("null")){
                        if (Utils.checkStringVal(airlinePnrArr.get(i)).isEmpty()) {
                            String gdsPnr = bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getGdsPnr();
                            String[] gdsValues = gdsPnr.split("/");
                            if (i == 0) {
                                builder.append(gdsValues[i]);
                            } else {
                                builder.append("/" + gdsValues[i]);
                            }
                        }
                    }else {
                        if (i == 0) {
                            builder.append(airlinePnrArr.get(i));
                        } else {
                            builder.append("/" + airlinePnrArr.get(i));
                        }
                    }
                }
                airlinePnrNumber.setText(builder.toString());
            }
        } else {
            airlinePnrNumber.setText(Utils.checkStringVal(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getGdsPnr()));
        }
        String flyinTripNumber = bookingMainArray.get(0).getGeneralDetailsObjectBean().getFlyinCode().substring(2);
        tripIdNumber.setText(flyinTripNumber);
        String bookedDate = Utils.formatTripTimeStampToDate(bookingMainArray.get(0).getGeneralDetailsObjectBean().getBookingDate(), TripConfirmationActivity.this);
        dateOfBooking.setText(bookedDate);
        numberOfPassengers = bookingMainArray.get(0).getPassengerDetailsObjectBean().getPassengersBeanArrayList().size();
        passengerMail = bookingMainArray.get(0).getGeneralDetailsObjectBean().getCustomerId();
        for (int i = 0; i < bookingMainArray.get(0).getPassengerDetailsObjectBean().getPassengersBeanArrayList().size(); i++) {
            PassengersBean passengersBean = new PassengersBean();
            passengersBean = bookingMainArray.get(0).getPassengerDetailsObjectBean().getPassengersBeanArrayList().get(i);
            if (i == 0) {
                passengersBean.setIsSecondPassenger(false);
            } else {
                passengersBean.setIsSecondPassenger(true);
            }
            setPassengerInfo(passengersBean);
        }
        setPriceDetails(bookingMainArray.get(0).getPriceBean());
        if (fareTypeArray.contains("refundable") || fareTypeArray.contains("refundable before departure")) {
            refundPolicyLayout.setVisibility(View.VISIBLE);
        } else {
            refundPolicyLayout.setVisibility(View.GONE);
        }
        try {
            if (!bookingMainArray.get(0).getPriceBean().getRdmDiscountusr().isEmpty()) {
                if (Double.parseDouble(bookingMainArray.get(0).getPriceBean().getRdmDiscountusr()) > 0) {
                    String finalRedeemDiscPrice = String.format(Locale.ENGLISH, "%.2f", Double.parseDouble(bookingMainArray.get(0).getPriceBean().getRdmDiscountusr()));
                    String currencyName = "";
                    redeemedRewardsLayout.setVisibility(View.VISIBLE);
                    if (bookingMainArray.get(0).getPriceBean().getUserCurrency().equalsIgnoreCase("SAR")) {
                        currencyName = getString(R.string.label_SAR_currency_name);
                        if (Utils.isArabicLangSelected(TripConfirmationActivity.this)) {
                            redeemedRewardsValue.setText(finalRedeemDiscPrice + " " + currencyName);
                        } else {
                            redeemedRewardsValue.setText(currencyName + " " + finalRedeemDiscPrice);
                        }
                    } else {
                        currencyName = bookingMainArray.get(0).getPriceBean().getUserCurrency();
                        redeemedRewardsValue.setText(currencyName + " " + finalRedeemDiscPrice);
                    }
                }
            } else {
                redeemedRewardsLayout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
        }
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        if (!pref.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
            if (pref.getBoolean(Constants.IS_REWARDS_ENABLED, false)) {
                handleEranLoyaltyRequest();
            } else {
                closeLoading();
            }
        } else {
            closeLoading();
        }
    }

    private void handleEranLoyaltyRequest() {
        String earnPoints = handleEarnLoyaltyPoints();
        Utils.printMessage(TAG, "Final Resp :: " + earnPoints);
        HTTPAsync async = new HTTPAsync(TripConfirmationActivity.this, TripConfirmationActivity.this,
                Constants.EARN_LOYALTY_POINTS_RQ, "", earnPoints, GET_LOYALTY_POINTS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String handleEarnLoyaltyPoints() {
        JSONObject sourceObj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        try {
            String ddt = "";
            String ardt = "";
            String sourceSector = "";
            String sourceCountry = "";
            String destinationSector = "";
            String destingationCountry = "";
            ArrayList<FlightsDataBean> tempFlightsDataBeanList = new ArrayList<>();
            tempFlightsDataBeanList.clear();
            for (int i = 0; i < bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().size(); i++) {
                String journeyType = bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getJourneyType();
                if (journeyType.equalsIgnoreCase("O") || journeyType.equalsIgnoreCase("M")) {
                    if (bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getLegOrder().equalsIgnoreCase("1")) {
                        tempFlightsDataBeanList.add(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i));
                    }
                }
            }
            for (int i = 0; i < tempFlightsDataBeanList.size(); i++) {
                sourceSector = tempFlightsDataBeanList.get(0).getDepartureAirport();
                sourceCountry = airportNamesMap.get(tempFlightsDataBeanList.get(0).getDepartureAirport()).getCityCode();
                destinationSector = tempFlightsDataBeanList.get(tempFlightsDataBeanList.size() - 1).getArrivalAirport();
                destingationCountry = airportNamesMap.get(tempFlightsDataBeanList.get(tempFlightsDataBeanList.size() - 1).getArrivalAirport()).getCityCode();
            }
            ddt = Utils.formatTripDateToServerDateFormat(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(0).getDepartureDate(), TripConfirmationActivity.this);
            for (int i = 0; i < bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().size(); i++) {
                String journeyType = bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getJourneyType();
                if (journeyType.equalsIgnoreCase("O")) {
                    ardt = Utils.formatTripDateToServerDateFormat(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getDepartureDate(), TripConfirmationActivity.this);
                }
                if (journeyType.equalsIgnoreCase("R")) {
                    ardt = Utils.formatTripDateToServerDateFormat(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getDepartureDate(), TripConfirmationActivity.this);
                    break;
                }
                if (journeyType.equalsIgnoreCase("M")) {
                    ardt = Utils.formatTripDateToServerDateFormat(bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(i).getArrivalDate(), TripConfirmationActivity.this);
                }
            }
            Utils.printMessage(TAG, "ddt :: " + ddt + " ardt :: " + ardt);
            JSONObject flightObj = new JSONObject();
            JSONObject routeObj = new JSONObject();
            sourceObj.put("apikey", Constants.MERCHANDISE_API_KEY);
            sourceObj.put("userid", pref.getString(Constants.USER_UNIQUE_ID, ""));
            sourceObj.put("bookingCode", bookingMainArray.get(0).getGeneralDetailsObjectBean().getFlyinCode());
            sourceObj.put("product", "flight");
            sourceObj.put("currency", "SAR");
            sourceObj.put("basefareAmount", Double.parseDouble(Utils.convertOtherCurrencytoSarCurrency(Double.parseDouble(bookingMainArray.get(0).getPriceBean().getBaseFare()), TripConfirmationActivity.this)));
            sourceObj.put("totalAmount", Double.parseDouble(Utils.convertOtherCurrencytoSarCurrency(Double.parseDouble(bookingMainArray.get(0).getPriceBean().getTotal()), TripConfirmationActivity.this)));
            sourceObj.put("companyId", Constants.LOYALTY_COMPANY_ID);
            flightObj.put("gds", Utils.checkInfoSourceVal(infoSource));
            flightObj.put("airline", bookingMainArray.get(0).getEntityDetailsObjectBean().getFlightDetailsBean().getFlightsDataBeanArrayList().get(0).getMarketingAirline());
            flightObj.put("nop", bookingMainArray.get(0).getPassengerDetailsObjectBean().getPassengersBeanArrayList().size());
            flightObj.put("cartype", "Full cost Carrier");
            routeObj.put("destinationSector", destinationSector);
            routeObj.put("destinationCountry", destingationCountry);
            routeObj.put("sourceCountry", sourceCountry);
            routeObj.put("sourceSector", sourceSector);
            flightObj.put("route", routeObj);
            sourceObj.put("flight", flightObj);
            sourceObj.put("bookingDate", timeStamp + ".000Z");
            sourceObj.put("travelPeriodFrom", ddt + ".000Z");
            sourceObj.put("travelPeriodTo", ardt + ".000Z");
            sourceObj.put("groupType", groupType);
            sourceObj.put("onHold", "0");
            Utils.printMessage("handleEarnLoyaltyPoints", sourceObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sourceObj.toString();
    }

    private void handleLoyaltyConfirmService(String earnedPoints) {
        String loyaltyPoints = handleLoyaltyConfirmRQ(earnedPoints);
        Utils.printMessage(TAG, "Conf Loyalty RQ :: " + loyaltyPoints);
        HTTPAsync async = new HTTPAsync(TripConfirmationActivity.this, TripConfirmationActivity.this,
                Constants.CONFIRM_LOYALTY_URL, "", loyaltyPoints, GET_CONFIRM_LOYALTY_RESULT, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String handleLoyaltyConfirmRQ(String earnedPoints) {
        JSONObject mainJSON = new JSONObject();
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(TripConfirmationActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(TripConfirmationActivity.this));
            mainJSON.put("source", sourceJSON);
            JSONObject commonDetailsJSON = new JSONObject();
            commonDetailsJSON.put("flyincode", bookingMainArray.get(0).getGeneralDetailsObjectBean().getFlyinCode());
            commonDetailsJSON.put("customerId", pref.getString(Constants.MEMBER_EMAIL, ""));
            commonDetailsJSON.put("leadpax", leadPaxName);
            commonDetailsJSON.put("ipAddrs", Utils.checkStringValue("null"));
            commonDetailsJSON.put("prodType", "F");
            commonDetailsJSON.put("apptype", "M-App");
            commonDetailsJSON.put("paymtdate", timeStamp);
            commonDetailsJSON.put("serviceType", "Loyalty");
            mainJSON.put("commondetails", commonDetailsJSON);
            JSONObject loyaltyJSON = new JSONObject();
            loyaltyJSON.put("earnpoint", Integer.parseInt(earnedPoints));
            loyaltyJSON.put("redeempoint", Utils.checkStringValue("null"));
            loyaltyJSON.put("rdmdiscountusr", Utils.checkStringValue("null"));
            loyaltyJSON.put("status", "CONFIRMED");
            mainJSON.put("loyaltydetails", loyaltyJSON);
        } catch (Exception e) {
        }
        return mainJSON.toString();
    }
}
