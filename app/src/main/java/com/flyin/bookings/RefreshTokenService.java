package com.flyin.bookings;

import android.content.Context;
import android.content.SharedPreferences;

import com.flyin.bookings.listeners.TokenUpdateListener;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Calendar;

public class RefreshTokenService implements AsyncTaskListener {
    private static final int GET_ACCESS_TOKEN = 1;
    private static final int GET_REFRESH_TOKEN = 2;
    private TokenUpdateListener listener;
    private String callType = "";
    private SharedPreferences preferences = null;

    public void checkServiceCall(Context context, TokenUpdateListener listener, String callType) {
        this.listener = listener;
        this.callType = callType;
        preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        if (!preferences.getString(Constants.ACCESS_TOKEN, "").isEmpty()) {
            updateRefreshToken(context);
        } else {
            getTokenFromServer(context);
        }
//        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
//        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
//        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
//        if (tokenTime == -1) {
//            updateRefreshToken();
//        } else {
//            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
//            if (diff > Long.parseLong(expireIn)) {
//                updateRefreshToken();
//            }
//        }
    }

    private void getTokenFromServer(Context context) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(context));
            obj.put("resetAll", "Yes");
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(context, RefreshTokenService.this, Constants.CLIENT_AUTHENTICATION, "",
                obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void updateRefreshToken(Context context) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.REFRESH_GRANT_TYPE);
            obj.put("refreshToken", preferences.getString(Constants.REFRESH_TOKEN, ""));
            if (!preferences.getString(Constants.MEMBER_ACCESS_TOKEN, "").equalsIgnoreCase("")) {
                obj.put("mtoken", preferences.getString(Constants.MEMBER_ACCESS_TOKEN, ""));
            }
        } catch (Exception e) {
        }
        HTTPAsync async = new HTTPAsync(context, RefreshTokenService.this, Constants.CLIENT_REFRESH_TOKEN,
                "", obj.toString(), GET_REFRESH_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage("RefreshTokenService", " === DATA RESPONSE ==== " + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            listener.dataResponse("Failure", callType);
        } else {
            if (serviceType == GET_ACCESS_TOKEN) {
                JSONObject obj = null;
                try {
                    obj = new JSONObject(data);
                    if (obj.has("accessToken")) {
                        String accessToken = Utils.checkStringVal(obj.getString("accessToken"));
                        if (!accessToken.equalsIgnoreCase("null") && !accessToken.equalsIgnoreCase("")) {
                            String expireIn = obj.getString("expireIn");
                            String refreshToken = obj.getString("refreshToken");
                            String timeZone = "";
                            if (obj.has("tzone")) {
                                timeZone = obj.getString("tzone");
                            }
                            long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString(Constants.ACCESS_TOKEN, accessToken);
                            editor.putString(Constants.EXPIRE_IN, expireIn);
                            editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                            editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                            editor.putString(Constants.SERVER_TIME_MILLI, timeZone);
                            editor.apply();
                            listener.dataResponse("Success", callType);
                        } else {
                            listener.dataResponse("Failure", callType);
                        }
                    } else {
                        listener.dataResponse("Failure", callType);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (serviceType == GET_REFRESH_TOKEN) {
                JSONObject obj = null;
                try {
                    Object json = new JSONTokener(data).nextValue();
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Constants.ACCESS_TOKEN, "").apply();
                    editor.putString(Constants.EXPIRE_IN, "").apply();
                    editor.putString(Constants.REFRESH_TOKEN, "").apply();
                    editor.remove(Constants.TOKEN_GENERATED_TIME).apply();
                    editor.putString(Constants.SERVER_TIME_MILLI, "").apply();
                    if (json instanceof JSONObject) {
                        obj = new JSONObject(data);
                        if (obj.has("accessToken")) {
                            String accessToken = Utils.checkStringVal(obj.getString("accessToken"));
                            if (!accessToken.equalsIgnoreCase("null") && !accessToken.equalsIgnoreCase("")) {
                                String expireIn = obj.getString("expireIn");
                                String refreshToken = obj.getString("refreshToken");
                                String timeZone = "";
                                if (obj.has("tzone")) {
                                    timeZone = obj.getString("tzone");
                                }
                                long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                                editor.putString(Constants.ACCESS_TOKEN, accessToken);
                                editor.putString(Constants.EXPIRE_IN, expireIn);
                                editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                                editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                                editor.putString(Constants.SERVER_TIME_MILLI, timeZone);
                                editor.apply();
                                listener.dataResponse("Success", callType);
                            } else {
                                listener.dataResponse("Failure", callType);
                            }
                        } else {
                            listener.dataResponse("Failure", callType);
                        }
                    } else {
                        listener.dataResponse("Failure", callType);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {

    }
}
