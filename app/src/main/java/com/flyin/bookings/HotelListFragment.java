package com.flyin.bookings;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.adapter.HotelListsAdapter;
import com.flyin.bookings.dialog.TripAdvisorDialog;
import com.flyin.bookings.listeners.CancelHotelBookingListener;
import com.flyin.bookings.model.FhRoomPassengerDetailsBean;
import com.flyin.bookings.model.HotelModel;
import com.flyin.bookings.model.RoomModel;
import com.flyin.bookings.model.TripAdvisorBean;
import com.flyin.bookings.model.TripDialogBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.GPSTracker;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

@SuppressWarnings("deprecation")
public class HotelListFragment extends Fragment implements AsyncTaskListener, CancelHotelBookingListener {
    private ImageView img_filter;
    private EditText search;
    private ImageView starArrow, priceArrow;
    private boolean star_check = false, price_check = false, check_accending = true;
    private String header_date = "", people, nights;
    private int adults, childs;
    private String city, country;
    private ArrayList<HotelModel> hotelList = new ArrayList<>();
    private ArrayList<HotelModel> finalhotelList = new ArrayList<>();
    private ArrayList<HotelModel> pricelList = new ArrayList<>();
    private RecyclerView recyclerView;
    private HotelListsAdapter mAdapter;
    private Typeface regularFont;
    private TextView txt_count;
    private ArrayList<String> adultList;
    private ArrayList<String> childList;
    private HashMap<String, ArrayList<String>> childCount;
    private static final String TAG = "HotelListFragment";
    private String currentDate = "";
    private ArrayList<TripAdvisorBean> tripAdvisorArrayList = new ArrayList<>();
    private ProgressDialog barProgressDialog = null;
    private static final int GET_TRIP_ADVISOR = 13;
    private ArrayList<TripDialogBean> tripArrayList = new ArrayList<>();
    private String hotelNameVal = "";
    private String cityName = "";
    private String randomNumber = "";
    private Activity mActivity;
    private String jsn, searchType, searchResponse;
    private Button searchButton = null;
    private ImageView errorImage = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private LayoutInflater layoutInflater = null;
    private int SEARCH_HOTEL_TRIP = 20;
    private RelativeLayout loadingViewLayout = null;
    private int SEARCH_HOTEL = 12;
    private int SEARCH_HOTEL_DETAILS = 2;
    private boolean isArabicLang = false;
    private ArrayList<String> huidlist = new ArrayList<>();
    private int searchCount = 0;
    private boolean dealsNearMeClicked = false;
    private String dealsNearCityId = "";
    private String hotelCheckInDate = "", cityId, hotelId;
    private String hotelCheckOutDate = "";
    private ArrayList<String> occupancy_adult_count_list = new ArrayList<>();
    private HashMap<String, ArrayList<String>> occupancy_child_count_list = null;
    private int night_count = 0;
    private GPSTracker gps = null;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private int room_count = 1;
    private ArrayList<FhRoomPassengerDetailsBean> roomPassengerArrayList = null;
    private ArrayList<Integer> errorlist = null;
    private String hotelCityName = "";
    private String from_date = "";
    private String to_date = "";
    private boolean iSstar_check = false, iSprice_check = false, iScheck_upArrow = true;
    private String arrowStaus = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hotel_listing, container, false);
        ((MainSearchActivity) mActivity).hideToolbar();
        Calendar calendar = Calendar.getInstance();
        huidlist = new ArrayList<>();
        childCount = new HashMap<>();
        // adult_count_list = new ArrayList<>();
        childList = new ArrayList<>();
        adultList = new ArrayList<>();
        occupancy_adult_count_list = new ArrayList<>();
        occupancy_child_count_list = new HashMap<>();
        errorlist = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.list_hotel_listing);
        ImageView img_map_view = (ImageView) view.findViewById(R.id.img_map_view);
        TextView img_back = (TextView) view.findViewById(R.id.img_back);
        img_filter = (ImageView) view.findViewById(R.id.img_filter);
        TextView text_city = (TextView) view.findViewById(R.id.txt_city);
        TextView text_country = (TextView) view.findViewById(R.id.txt_country);
        TextView price_text = (TextView) view.findViewById(R.id.price_text);
        TextView star_text = (TextView) view.findViewById(R.id.star_text);
        TextView recommend_text = (TextView) view.findViewById(R.id.recommend_text);
        TextView checkInOut = (TextView) view.findViewById(R.id.txt_date);
        TextView txt_people = (TextView) view.findViewById(R.id.txt_people);
        TextView txt_night = (TextView) view.findViewById(R.id.txt_nights);
        TextView txt_count = (TextView) view.findViewById(R.id.txt_result_count_lebel);
        search = (EditText) view.findViewById(R.id.txt_search_hotel_name);
        starArrow = (ImageView) view.findViewById(R.id.star_arrow);
        priceArrow = (ImageView) view.findViewById(R.id.price_arrow);
        LinearLayout backLayout = (LinearLayout) view.findViewById(R.id.back_layout);
        TextView taxesInfoText = (TextView) view.findViewById(R.id.taxes_info_text);
        String fontRegularBold = Constants.FONT_ROBOTO_BOLD;
        String fontRegularLight = Constants.FONT_ROBOTO_LIGHT;
        String fontRegularMedium = Constants.FONT_ROBOTO_MEDIUM;
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(mActivity)) {
            isArabicLang = true;
            fontRegularBold = Constants.FONT_DROIDKUFI_BOLD;
            fontRegularLight = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegularMedium = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
            checkInOut.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            txt_people.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            txt_night.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_city_text));
            img_back.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            search.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            price_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            star_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            recommend_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            txt_count.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            text_city.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.textView_size));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            img_back.setLayoutParams(params);
        }
        isArabicLang = false;
        Typeface regularLight = Typeface.createFromAsset(mActivity.getAssets(), fontRegularLight);
        Typeface regularBold = Typeface.createFromAsset(mActivity.getAssets(), fontRegularBold);
        Typeface regularMedium = Typeface.createFromAsset(mActivity.getAssets(), fontRegularMedium);
        regularFont = Typeface.createFromAsset(mActivity.getAssets(), fontRegular);
        text_city.setTypeface(regularBold);
        text_country.setTypeface(regularBold);
        price_text.setTypeface(regularMedium);
        star_text.setTypeface(regularMedium);
        recommend_text.setTypeface(regularMedium);
        checkInOut.setTypeface(regularLight);
        txt_people.setTypeface(regularLight);
        txt_night.setTypeface(regularLight);
        search.setTypeface(regularLight);
        txt_count.setTypeface(regularMedium);
        img_back.setTypeface(regularFont);
        taxesInfoText.setTypeface(regularFont);
        errorView = (RelativeLayout) view.findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) view.findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(regularLight);
        loadingViewLayout = (RelativeLayout) view.findViewById(R.id.loading_view_layout);
        layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity.getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        header_date = getArguments().getString("checkin");
        try {
            if (header_date.contains(",")) {
                header_date.replace(",", "");
            }
        } catch (Exception e) {
        }
        Utils.printMessage(TAG, "header_date " + header_date);
        checkInOut.setText(header_date);
        jsn = getArguments().getString("jsn");
        searchResponse = getArguments().getString(Constants.SEARCH_RESPONSE_JSON);
        cityId = getArguments().getString("cityId");
        hotelId = getArguments().getString("hotelId");
        hotelCheckInDate = getArguments().getString("hotelCheckInDate");
        hotelCheckOutDate = getArguments().getString("hotelCheckOutDate");
        randomNumber = getArguments().getString(Constants.BOOKING_RANDOM_NUMBER, "");
        childs = getArguments().getInt("child_count");
        adults = getArguments().getInt("adult_count");
        nights = getArguments().getString("night_count");
        try {
            city = getArguments().getString("city");
            String[] city_data = new String[2];
            if (city.contains(",")) {
                city_data = city.split(",");
                text_city.setText(city);
                if (city_data.length == 1) {
                    cityName = city_data[0];
                    country = city_data[0];
                    text_country.setVisibility(View.GONE);
                } else {
                    cityName = city_data[0];
                    country = city_data[1];
                    text_country.setVisibility(View.GONE);
                }
            } else {
                cityName = city;
                country = city;
                text_city.setText(city);
                text_country.setVisibility(View.GONE);
            }
        } catch (Exception e) {
        }
        people = "" + (childs + adults);
        txt_people.setText(people);
        txt_night.setText(nights);
        mAdapter = new HotelListsAdapter(pricelList, HotelListFragment.this, mActivity, header_date, adults,
                childs, adultList, childCount, childList, hotelList, currentDate, tripAdvisorArrayList,
                HotelListFragment.this, randomNumber, city, hotelCheckInDate, hotelCheckOutDate);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                handleHotelSearch(search.getText().toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
        img_filter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new FilterFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("list", finalhotelList);
                bundle.putString("night_count", "" + nights);
                bundle.putString("checkin", header_date);
                bundle.putInt("adult_count", adults);
                bundle.putInt("child_count", childs);
                bundle.putInt("check_class", 1);
                bundle.putSerializable("children_list", childCount);
                bundle.putStringArrayList("adults_list", new ArrayList<>(adultList));
                bundle.putStringArrayList("childs_list", new ArrayList<>(childList));
                bundle.putBoolean("servicecall", true);
                bundle.putBoolean("price_check", price_check);
                bundle.putBoolean("star_check", star_check);
                bundle.putBoolean("iSprice_check", iSprice_check);
                bundle.putBoolean("iSstar_check", iSstar_check);
                bundle.putBoolean("check_accending", check_accending);
                bundle.putString("arrowStaus", arrowStaus);
                bundle.putString("hotelCheckInDate", hotelCheckInDate);
                bundle.putString("hotelCheckOutDate", hotelCheckOutDate);
                SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                Editor editor = preferences.edit();
                editor.putBoolean("serviceCall", false).apply();
                fragment.setArguments(bundle);
                ((MainSearchActivity) mActivity).pushFragment(fragment);
            }
        });
        img_map_view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new MapFragment();
                Bundle bundle = new Bundle();
                bundle.putString("night_count", "" + nights);
                bundle.putString("checkin", header_date);
                bundle.putParcelableArrayList("list", pricelList);
                bundle.putParcelableArrayList("final_list", finalhotelList);
                bundle.putStringArrayList("adults_list", new ArrayList<>(adultList));
                bundle.putStringArrayList("childs_list", new ArrayList<>(childList));
                bundle.putSerializable("children_list", childCount);
                bundle.putInt("adult_count", adults);
                bundle.putInt("child_count", childs);
                bundle.putString("city", city);
                bundle.putBoolean("servicecall", true);
                bundle.putBoolean("price_check", price_check);
                bundle.putBoolean("star_check", star_check);
                bundle.putBoolean("iSprice_check", iSprice_check);
                bundle.putBoolean("iSstar_check", iSstar_check);
                bundle.putBoolean("check_accending", check_accending);
                bundle.putString("arrowStaus", arrowStaus);
                bundle.putString("hotelCheckInDate", hotelCheckInDate);
                bundle.putString("hotelCheckOutDate", hotelCheckOutDate);
                SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                Editor editor = preferences.edit();
                editor.putBoolean("serviceCall", false).apply();
                bundle.putString(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                fragment.setArguments(bundle);
                ((MainSearchActivity) mActivity).pushFragment(fragment);
            }
        });
        backLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        LinearLayout price_layout = (LinearLayout) view.findViewById(R.id.price_layout);
        price_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                priceArrow.setVisibility(View.VISIBLE);
                starArrow.setVisibility(View.INVISIBLE);
                iSstar_check = false;
                iSprice_check = true;
                if (price_check) {
                    if (check_accending) {
                        priceArrow.setImageResource(R.drawable.arrowdown);
                        arrowStaus = "downarrow";
                        check_accending = false;
                    } else {
                        priceArrow.setImageResource(R.drawable.arrowup);
                        arrowStaus = "upArrow";
                        check_accending = true;
                    }
                    Collections.reverse(pricelList);
                    price_check = true;
                    star_check = false;
                } else {
                    check_accending = false;
                    priceArrow.setImageResource(R.drawable.arrowdown);
                    arrowStaus = "downarrow";
                    Collections.sort(pricelList, new Comparator<HotelModel>() {
                        public int compare(HotelModel one, HotelModel other) {
                            if (Double.parseDouble(one.getP()) < Double.parseDouble(other.getP()))
                                return 1;
                            else if (Double.parseDouble(one.getP()) == Double.parseDouble(other.getP()))
                                return 0;
                            else
                                return -1;
                        }
                    });
                    price_check = true;
                    star_check = false;
                }
                mAdapter = new HotelListsAdapter(pricelList, HotelListFragment.this, mActivity, header_date, adults,
                        childs, adultList, childCount, childList, hotelList, currentDate, tripAdvisorArrayList,
                        HotelListFragment.this, randomNumber, city, hotelCheckInDate, hotelCheckOutDate);
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        });
        TextView recommended_layout = (TextView) view.findViewById(R.id.recommend_text);
        recommended_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                priceArrow.setVisibility(View.INVISIBLE);
                starArrow.setVisibility(View.INVISIBLE);
                price_check = false;
                iSstar_check = false;
                iSprice_check = false;
                ArrayList<HotelModel> temp = new ArrayList<HotelModel>();
                temp.addAll(pricelList);
                pricelList.clear();
                if (hotelList.size() != 0) {
                    pricelList.addAll(hotelList);
                } else {
                    pricelList.addAll(temp);
                }
                arrowStaus = "";
                sortByHotelRank();
                mAdapter = new HotelListsAdapter(pricelList, HotelListFragment.this, mActivity, header_date, adults,
                        childs, adultList, childCount, childList, hotelList, currentDate, tripAdvisorArrayList,
                        HotelListFragment.this, randomNumber, city, hotelCheckInDate, hotelCheckOutDate);
                recyclerView.setAdapter(mAdapter);
            }
        });
        LinearLayout star_layout = (LinearLayout) view.findViewById(R.id.star_layout);
        star_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                starArrow.setVisibility(View.VISIBLE);
                priceArrow.setVisibility(View.INVISIBLE);
                iSstar_check = true;
                iSprice_check = false;
                if (star_check) {
                    if (check_accending) {
                        starArrow.setImageResource(R.drawable.arrowup);
                        check_accending = false;
                        arrowStaus = "upArrow";
                    } else {
                        starArrow.setImageResource(R.drawable.arrowdown);
                        check_accending = true;
                        arrowStaus = "downarrow";
                    }
                    Collections.reverse(pricelList);
                } else {
                    check_accending = false;
                    starArrow.setImageResource(R.drawable.arrowup);
                    arrowStaus = "upArrow";
                    Collections.sort(pricelList, new Comparator<HotelModel>() {
                        public int compare(HotelModel one, HotelModel other) {
                            return one.getStar().compareTo(other.getStar());
                        }
                    });
                }
                price_check = false;
                star_check = true;
                mAdapter = new HotelListsAdapter(pricelList, HotelListFragment.this, mActivity, header_date, adults,
                        childs, adultList, childCount, childList, hotelList, currentDate, tripAdvisorArrayList,
                        HotelListFragment.this, randomNumber, city, hotelCheckInDate, hotelCheckOutDate);
                recyclerView.setAdapter(mAdapter);
            }
        });
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        if (!pref.getString("no_hotel_msg", "").isEmpty()) {
            displayErrorMessage(pref.getString("no_hotel_msg", ""));
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("no_hotel_msg", "");
            editor.apply();
        }
        if (pref.getBoolean("serviceCall", false)) {
            if (getArguments().containsKey("list")) {
                if (getArguments().getParcelableArrayList("list").size() != 0) {
                    img_filter.setClickable(false);
                    SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    Editor editor = preferences.edit();
                    editor.putBoolean("serviceCall", false).apply();
                    hotelList = getArguments().getParcelableArrayList("list");
                    finalhotelList = getArguments().getParcelableArrayList("final_list");
                    adultList = getArguments().getStringArrayList("adults_list");
                    childList = getArguments().getStringArrayList("childs_list");
                    childCount = (HashMap<String, ArrayList<String>>) getArguments().getSerializable("children_list");
                    sortByHotelRank();
                    ArrayList<HotelModel> temp = new ArrayList<HotelModel>();
                    for (int i = 0; i < 20; i++) {
                        HotelModel model = new HotelModel();
                        model = hotelList.get(i);
                        temp.add(model);
                    }
                    mAdapter = new HotelListsAdapter(temp, this, mActivity, header_date, adults, childs, adultList, childCount,
                            childList, temp, currentDate, tripAdvisorArrayList, HotelListFragment.this, randomNumber, city,
                            hotelCheckInDate, hotelCheckOutDate);
                    recyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                    handleSearchRequest(jsn);
                }
            } else {
                img_filter.setClickable(false);
                SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                Editor editor = preferences.edit();
                editor.putBoolean("serviceCall", false).apply();
                mAdapter.notifyDataSetChanged();
                handleSearchRequest(jsn);
            }
        } else {
            if (getArguments().getParcelableArrayList("list") != null) {
                pricelList = getArguments().getParcelableArrayList("list");
                finalhotelList = getArguments().getParcelableArrayList("final_list");
                price_check = getArguments().getBoolean("price_check", false);
                star_check = getArguments().getBoolean("star_check", false);
                iSprice_check = getArguments().getBoolean("iSprice_check", false);
                iSstar_check = getArguments().getBoolean("iSstar_check", false);
                check_accending = getArguments().getBoolean("check_accending", false);
                arrowStaus = getArguments().getString("arrowStaus");
                ArrayList<HotelModel> temp = new ArrayList<>(pricelList);
                hotelList.clear();
                for (int i = 0; i < temp.size(); i++) {
                    HotelModel model = new HotelModel();
                    model = temp.get(i);
                    hotelList.add(model);
                }
            }
            if (iSprice_check) {
                priceArrow.setVisibility(View.VISIBLE);
                starArrow.setVisibility(View.INVISIBLE);
                if (arrowStaus.equalsIgnoreCase("upArrow")) {
                    priceArrow.setImageResource(R.drawable.arrowup);
                    Collections.sort(pricelList, new Comparator<HotelModel>() {
                        public int compare(HotelModel one, HotelModel other) {
                            if (Double.parseDouble(one.getP()) < Double.parseDouble(other.getP()))
                                return 1;
                            else if (Double.parseDouble(one.getP()) == Double.parseDouble(other.getP()))
                                return 0;
                            else
                                return -1;
                        }
                    });
                    Collections.reverse(pricelList);
                    price_check = true;
                    star_check = false;
                }
                if (arrowStaus.equalsIgnoreCase("downarrow")) {
                    priceArrow.setImageResource(R.drawable.arrowdown);
                    Collections.sort(pricelList, new Comparator<HotelModel>() {
                        public int compare(HotelModel one, HotelModel other) {
                            if (Double.parseDouble(one.getP()) < Double.parseDouble(other.getP()))
                                return 1;
                            else if (Double.parseDouble(one.getP()) == Double.parseDouble(other.getP()))
                                return 0;
                            else
                                return -1;
                        }
                    });
                    price_check = true;
                    star_check = false;
                }
                mAdapter = new HotelListsAdapter(pricelList, HotelListFragment.this, mActivity, header_date, adults,
                        childs, adultList, childCount, childList, hotelList, currentDate, tripAdvisorArrayList,
                        HotelListFragment.this, randomNumber, city, hotelCheckInDate, hotelCheckOutDate);
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            } else if (iSstar_check) {
                starArrow.setVisibility(View.VISIBLE);
                priceArrow.setVisibility(View.INVISIBLE);
                if (arrowStaus.equalsIgnoreCase("downarrow")) {
                    starArrow.setImageResource(R.drawable.arrowup);

                    Collections.sort(pricelList, new Comparator<HotelModel>() {
                        public int compare(HotelModel one, HotelModel other) {
                            return one.getStar().compareTo(other.getStar());
                        }
                    });
                    Collections.reverse(pricelList);
                } else if (arrowStaus.equalsIgnoreCase("upArrow")) {
                    check_accending = false;
                    starArrow.setImageResource(R.drawable.arrowup);
                    Collections.sort(pricelList, new Comparator<HotelModel>() {
                        public int compare(HotelModel one, HotelModel other) {
                            return one.getStar().compareTo(other.getStar());
                        }
                    });
                }
                price_check = false;
                star_check = true;
                mAdapter = new HotelListsAdapter(pricelList, HotelListFragment.this, mActivity, header_date, adults,
                        childs, adultList, childCount, childList, hotelList, currentDate, tripAdvisorArrayList,
                        HotelListFragment.this, randomNumber, city, hotelCheckInDate, hotelCheckOutDate);
                recyclerView.setAdapter(mAdapter);
            } else {
                priceArrow.setVisibility(View.INVISIBLE);
                starArrow.setVisibility(View.INVISIBLE);
                price_check = false;
                star_check = false;
                sortByHotelRank();
                mAdapter = new HotelListsAdapter(pricelList, HotelListFragment.this, mActivity, header_date, adults,
                        childs, adultList, childCount, childList, hotelList, currentDate, tripAdvisorArrayList,
                        HotelListFragment.this, randomNumber, city, hotelCheckInDate, hotelCheckOutDate);
                recyclerView.setAdapter(mAdapter);
            }
            mAdapter.notifyDataSetChanged();
        }
        return view;
    }

    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView != null) {
            if (errorView.getAlpha() == 0) {
                errorView.setTranslationY(-60);
                errorView.setVisibility(View.VISIBLE);
                errorView.animate().translationY(0).alpha(1)
                        .setDuration(mActivity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                if (mActivity != null) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (errorView != null && mActivity != null && mActivity.getResources() != null) {
                                errorView.animate().alpha(0)
                                        .setDuration(mActivity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                            }
                        }
                    }, 2000);
                }
            }
        }
    }

    private void sortByHotelRank() {
        try{
            //price sort
            Collections.sort(pricelList, new Comparator<HotelModel>() {
                public int compare(HotelModel one, HotelModel other) {
                    if (Double.parseDouble(one.getP()) > Double.parseDouble(other.getP())) {
                        return 1;
                    } else if (Double.parseDouble(one.getP()) < Double.parseDouble(other.getP())) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            });
            //Rank sort
            Collections.sort(pricelList, new Comparator<HotelModel>() {
                public int compare(HotelModel one, HotelModel other) {
                    if (one.getRank() > other.getRank()) {
                        return 1;
                    } else if (one.getRank() < other.getRank()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            });
            mAdapter = new HotelListsAdapter(pricelList, this, mActivity, header_date, adults, childs, adultList, childCount,
                    childList, hotelList, currentDate, tripAdvisorArrayList, HotelListFragment.this, randomNumber, city,
                    hotelCheckInDate, hotelCheckOutDate);
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }catch (Exception e){
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (mActivity != null)
            mActivity.invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        img_filter.setClickable(true);
        mAdapter.notifyDataSetChanged();
        Utils.printMessage(TAG, "Listing Data :: " + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
            if (errorView != null) {
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(regularFont);
                errorDescriptionText.setTypeface(regularFont);
                searchButton.setTypeface(regularFont);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mActivity.onBackPressed();
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            }
        } else if (serviceType == SEARCH_HOTEL) {
            Utils.printMessage(TAG, "SEARCH_HOTEL :: " + data);
            searchCount++;
            parseSerachData(data);
            String resultData = "success";
            mAdapter.notifyDataSetChanged();
            Utils.printMessage(TAG, "check :: " + resultData);
            if (!resultData.equalsIgnoreCase("fail") && !resultData.equalsIgnoreCase("success") && !resultData.equalsIgnoreCase("")) {
                if (resultData != null) {
                    handleSearchRequest(resultData);
                }
            } else if (resultData.equalsIgnoreCase("success")) {
                final String jsn = makeJson();
                if (jsn != null) {
                    boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                    if (isInternetPresent) {
                        final String jsonTrip = hotelJsonTripAdvisor();
                        if (jsonTrip != null) {
                            new HTTPAsync(mActivity, HotelListFragment.this, Constants.TRIP_ADVISOR_URL, "", jsonTrip,
                                    SEARCH_HOTEL_TRIP, HTTPAsync.METHOD_POST).execute();
                        }
                    }
                } else {
                    final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(regularFont);
                    errorDescriptionText.setTypeface(regularFont);
                    searchButton.setTypeface(regularFont);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                            if (isInternetPresent) {
                                loadingViewLayout.removeAllViews();
                                handleSearchRequest(jsn);
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        } else if (serviceType == SEARCH_HOTEL_TRIP) {
            Utils.printMessage(TAG, "SEARCH_HOTEL_TRIP :: " + data);
            try {
                JSONArray arr = new JSONArray(data);
                for (int j = 0; j < arr.length(); j++) {
                    if (arr.getJSONObject(j).has("ratingObj")) {
                        TripAdvisorBean tripAdvisorBean = new TripAdvisorBean();
                        tripAdvisorBean.setHuid(arr.getJSONObject(j).get("huid").toString());
                        if (arr.getJSONObject(j).getJSONObject("ratingObj").has("num_reviews"))
                            tripAdvisorBean.setNumReview(arr.getJSONObject(j).getJSONObject("ratingObj").getString("num_reviews"));
                        if (arr.getJSONObject(j).getJSONObject("ratingObj").has("rating_image_url"))
                            tripAdvisorBean.setRatingImageUrl(arr.getJSONObject(j).getJSONObject("ratingObj").getString("rating_image_url"));
                        tripAdvisorArrayList.add(tripAdvisorBean);
                    }
                }
                if (hotelList.size() > 0) {
                    for (int j = 0; j < hotelList.size(); j++) {
                        HotelModel model = hotelList.get(j);
                        for (int z = 0; z < tripAdvisorArrayList.size(); z++) {
                            if (model.getHuid().equalsIgnoreCase(tripAdvisorArrayList.get(z).getHuid())) {
                                model.setReviewCount(tripAdvisorArrayList.get(z).getNumReview());
                                model.setReviewImgUrl(tripAdvisorArrayList.get(z).getRatingImageUrl());
                                break;
                            }
                        }
                    }
                }
                mAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (serviceType == SEARCH_HOTEL_DETAILS) {
            pricelList.clear();
            Utils.printMessage(TAG, "SEARCH_HOTEL_DETAILS :: " + data);
            try {
                if (hotelList.size() > 0) {
                    JSONArray arr = new JSONArray(data);
                    for (int i = 0; i < arr.length(); i++) {
                        try {
                            HotelModel model = hotelList.get(i);
                            JSONObject obj = arr.optJSONObject(i);
                            String error = obj.optString("error");
                            if (error.equalsIgnoreCase("data not found")) {
                                errorlist.add(i);
                                continue;
                            }
                            JSONObject hotel = obj.optJSONObject("hotel");
                            JSONObject bacickInfo = hotel.optJSONObject("basicInfo");
                            JSONObject geocode = bacickInfo.optJSONObject("geoCode");
                            model.setLatitude(geocode.optString("latitude"));
                            model.setLongitude(geocode.optString("longitude"));
                            JSONObject distric = bacickInfo.optJSONObject("district");
                            JSONObject contact = bacickInfo.optJSONObject("contactInfo");
                            model.setHemail(contact.optString("email"));
                            model.setHfaxCode(contact.optString("faxCode"));
                            model.setHfaxNo(contact.optString("fax"));
                            model.setHphoneCode(contact.optString("phoneCode"));
                            model.setHphoneNo(contact.optString("phone"));
                            JSONObject city = bacickInfo.optJSONObject("city");
                            model.setDistric(distric.getString("name"));
                            model.setCity(city.optString("name"));
                            model.setCityId(city.optString("id"));
                            JSONObject chainName = bacickInfo.optJSONObject("chain");
                            model.setChainName(chainName.getString("name"));
                            JSONObject hotelType = bacickInfo.optJSONObject("hotelType");
                            if (hotelType != null) {
                                Iterator<String> hotel_key = hotelType.keys();
                                String hotel_type = "";
                                while (hotel_key.hasNext()) {
                                    String key = hotel_key.next();
                                    try {
                                        if (hotel_type.length() > 0) {
                                            hotel_type = hotel_type + ",";
                                        }
                                        hotel_type = hotel_type + hotelType.getString(key);
                                    } catch (JSONException e) {
                                    } catch (Exception e) {
                                    }
                                }
                                model.setHotel_type(hotel_type);
                            }
                            model.setRoom_count("" + room_count);
                            JSONObject country = bacickInfo.optJSONObject("country");
                            model.setCountry(country.optString("name"));
                            model.setImage(bacickInfo.optString("mainImage"));
                            model.setStar(bacickInfo.optString("starRating"));
                            if (bacickInfo.optString("rank") == null || bacickInfo.optString("rank").equals("null"))
                                model.setRank(999999);
                            else
                                model.setRank(Integer.parseInt(bacickInfo.optString("rank")));
                            model.setFullAddress(bacickInfo.optString("addressText"));
                            model.setHna(bacickInfo.optString("name"));
                            JSONObject imageObj = hotel.optJSONObject("images");
                            JSONArray imageArr = imageObj.optJSONArray("others");
                            String[] imagelist = new String[imageArr.length()];
                            for (int j = 0; j < imageArr.length(); j++) {
                                imagelist[j] = imageArr.getString(j);
                            }
                            model.setImagelist(imagelist);
                            JSONObject policy = hotel.optJSONObject("policy");
                            String cancel_policy = policy.optString("cancellationPolicy");
                            if (cancel_policy != null) {
                                model.setCancel_policy(cancel_policy);
                            }
                            String child_policy = policy.optString("childPolicy");
                            if (child_policy != null) {
                                model.setChild_policy(child_policy);
                            }
                            String pet_policy = policy.optString("petPolicy");
                            if (pet_policy != null) {
                                model.setPet(pet_policy);
                            }
                            String check_out = policy.optString("check_out");
                            if (check_out != null) {
                                model.setCheck_out(check_out);
                            }
                            String check_in = policy.optString("check_in");
                            if (check_in != null) {
                                model.setCheck_in(check_in);
                            }
                            JSONObject description = hotel.optJSONObject("descriptions");
                            String lobby = description.optString("lobby");
                            if (lobby != null) {
                                model.setLobby(lobby);
                            }
                            String exterior = description.optString("exterior");
                            if (exterior != null) {
                                model.setExterior(exterior);
                            }
                            String rooms = description.optString("aboutHotel");
                            if (rooms != null) {
                                model.setRooms(rooms);
                            }
                            JSONObject facilities = hotel.optJSONObject("facilities");
                            JSONObject activity = facilities.optJSONObject("activities");
                            int k = 0;
                            if (activity != null) {
                                Iterator<String> activity_key = activity.keys();
                                String[] activitylist = new String[activity.length()];
                                while (activity_key.hasNext()) {
                                    String key = activity_key.next();
                                    try {
                                        String value = activity.getString(key);
                                        activitylist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setActivity(activitylist);
                            }
                            JSONObject general = facilities.optJSONObject("general");
                            int l = 0;
                            if (general != null) {
                                Iterator<String> activity_key = general.keys();
                                String[] generalList = new String[general.length()];
                                while (activity_key.hasNext()) {
                                    String key = activity_key.next();
                                    try {
                                        String value = general.getString(key);
                                        generalList[l] = value;
                                    } catch (JSONException e) {
                                    }
                                    l++;
                                }
                                model.setTopFeatures(generalList);
                            }
                            JSONObject service = facilities.optJSONObject("services");
                            if (service != null) {
                                Iterator<String> service_key = service.keys();
                                String[] servicelist = new String[service.length()];
                                k = 0;
                                while (service_key.hasNext()) {
                                    String key = service_key.next();
                                    try {
                                        String value = service.getString(key);
                                        servicelist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setService(servicelist);
                            }
                            JSONObject activities = facilities.optJSONObject("activities");
                            if (activities != null) {
                                Iterator<String> general_key = activities.keys();
                                String[] generallist = new String[activities.length()];
                                k = 0;
                                while (general_key.hasNext()) {
                                    String key = general_key.next();
                                    try {
                                        String value = activities.getString(key);
                                        generallist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setGeneral(generallist);
                            }
                            //trip advisor
                            if (!model.getRoomlist().get(0).getFree_cancel_date().isEmpty()) {
                                DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
                                Date dateBefore = null;
                                try {
                                    dateBefore = simpleDateFormat.parse(model.getRoomlist().get(0).getFree_cancel_date());
                                } catch (Exception e) {
                                }
                                SimpleDateFormat finalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                String finalDate = finalFormat.format(dateBefore);
                                String CancellationDateDiff = Utils.flightSearchDateDifference(finalDate, currentDate);
                                model.setBnplDateDiff(Integer.parseInt(CancellationDateDiff));
                            } else {
                                model.setBnplDateDiff(0);
                            }
                            for (int z = 0; z < tripAdvisorArrayList.size(); z++) {
                                if (model.getHuid().equalsIgnoreCase(tripAdvisorArrayList.get(z).getHuid())) {
                                    model.setReviewCount(tripAdvisorArrayList.get(z).getNumReview());
                                    model.setReviewImgUrl(tripAdvisorArrayList.get(z).getRatingImageUrl());
                                    break;
                                }
                            }
                            hotelList.set(i, model);
                        } catch (Exception e) {
                            Utils.printMessage(TAG, " error ::" + e.getMessage());
                        }
                    }
                    for (int j = 0; j < errorlist.size(); j++) {
                        hotelList.remove(errorlist.get(j) - j);
                    }
                    pricelList.addAll(hotelList);
                    sortByHotelRank();
                    mAdapter.notifyDataSetChanged();
                    errorlist.clear();
                    finalhotelList = new ArrayList<>(hotelList);
                    hotelCityName = hotelList.get(0).getCity() + ", " + hotelList.get(0).getCountry();
                    Utils.printMessage(TAG, "hotel city name ::" + hotelCityName);
                } else {
                    JSONArray arr = new JSONArray(data);
                    for (int i = 0; i < arr.length(); i++) {
                        try {
                            HotelModel model = new HotelModel();
                            JSONObject obj = arr.optJSONObject(i);
                            String error = obj.optString("error");
                            if (error.equalsIgnoreCase("data not found")) {
                                errorlist.add(i);
                                continue;
                            }
                            model.setHuid(obj.optString("huid"));
                            Utils.printMessage(TAG, " huid ::" + obj.optString("huid"));
                            JSONObject hotel = obj.optJSONObject("hotel");
                            JSONObject bacickInfo = hotel.optJSONObject("basicInfo");
                            JSONObject geocode = bacickInfo.optJSONObject("geoCode");
                            model.setLatitude(geocode.optString("latitude"));
                            model.setLongitude(geocode.optString("longitude"));
                            JSONObject distric = bacickInfo.optJSONObject("district");
                            JSONObject contact = bacickInfo.optJSONObject("contactInfo");
                            model.setHemail(contact.optString("email"));
                            model.setHfaxCode(contact.optString("faxCode"));
                            model.setHfaxNo(contact.optString("fax"));
                            model.setHphoneCode(contact.optString("phoneCode"));
                            model.setHphoneNo(contact.optString("phone"));
                            JSONObject city = bacickInfo.optJSONObject("city");
                            model.setDistric(distric.getString("name"));
                            model.setCity(city.optString("name"));
                            model.setCityId(city.optString("id"));
                            JSONObject chainName = bacickInfo.optJSONObject("chain");
                            model.setChainName(chainName.getString("name"));
                            JSONObject hotelType = bacickInfo.optJSONObject("hotelType");
                            if (hotelType != null) {
                                Iterator<String> hotel_key = hotelType.keys();
                                String hotel_type = "";
                                while (hotel_key.hasNext()) {
                                    String key = hotel_key.next();
                                    try {
                                        if (hotel_type.length() > 0) {
                                            hotel_type = hotel_type + ",";
                                        }
                                        hotel_type = hotel_type + hotelType.getString(key);
                                    } catch (JSONException e) {
                                    } catch (Exception e) {
                                    }
                                }
                                model.setHotel_type(hotel_type);
                            }
                            model.setRoom_count("" + room_count);
                            JSONObject country = bacickInfo.optJSONObject("country");
                            model.setCountry(country.optString("name"));
                            model.setImage(bacickInfo.optString("mainImage"));
                            model.setStar(bacickInfo.optString("starRating"));
                            if (bacickInfo.optString("rank") == null || bacickInfo.optString("rank").equals("null"))
                                model.setRank(999999);
                            else
                                model.setRank(Integer.parseInt(bacickInfo.optString("rank")));
                            model.setFullAddress(bacickInfo.optString("addressText"));
                            model.setHna(bacickInfo.optString("name"));
                            JSONObject imageObj = hotel.optJSONObject("images");
                            JSONArray imageArr = imageObj.optJSONArray("others");
                            String[] imagelist = new String[imageArr.length()];
                            for (int j = 0; j < imageArr.length(); j++) {
                                imagelist[j] = imageArr.getString(j);
                            }
                            model.setImagelist(imagelist);
                            JSONObject policy = hotel.optJSONObject("policy");
                            String cancel_policy = policy.optString("cancellationPolicy");
                            if (cancel_policy != null) {
                                model.setCancel_policy(cancel_policy);
                            }
                            String child_policy = policy.optString("childPolicy");
                            if (child_policy != null) {
                                model.setChild_policy(child_policy);
                            }
                            String pet_policy = policy.optString("petPolicy");
                            if (pet_policy != null) {
                                model.setPet(pet_policy);
                            }
                            String check_out = policy.optString("check_out");
                            if (check_out != null) {
                                model.setCheck_out(check_out);
                            }
                            String check_in = policy.optString("check_in");
                            if (check_in != null) {
                                model.setCheck_in(check_in);
                            }
                            JSONObject description = hotel.optJSONObject("descriptions");
                            String lobby = description.optString("lobby");
                            if (lobby != null) {
                                model.setLobby(lobby);
                            }
                            String exterior = description.optString("exterior");
                            if (exterior != null) {
                                model.setExterior(exterior);
                            }
                            String rooms = description.optString("aboutHotel");
                            if (rooms != null) {
                                model.setRooms(rooms);
                            }
                            JSONObject facilities = hotel.optJSONObject("facilities");
                            JSONObject activity = facilities.optJSONObject("activities");
                            int k = 0;
                            if (activity != null) {
                                Iterator<String> activity_key = activity.keys();
                                String[] activitylist = new String[activity.length()];
                                while (activity_key.hasNext()) {
                                    String key = activity_key.next();
                                    try {
                                        String value = activity.getString(key);
                                        activitylist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setActivity(activitylist);
                            }
                            JSONObject general = facilities.optJSONObject("general");
                            int l = 0;
                            if (general != null) {
                                Iterator<String> activity_key = general.keys();
                                String[] generalList = new String[general.length()];
                                while (activity_key.hasNext()) {
                                    String key = activity_key.next();
                                    try {
                                        String value = general.getString(key);
                                        generalList[l] = value;
                                    } catch (JSONException e) {
                                    }
                                    l++;
                                }
                                model.setTopFeatures(generalList);
                            }
                            JSONObject service = facilities.optJSONObject("services");
                            if (service != null) {
                                Iterator<String> service_key = service.keys();
                                String[] servicelist = new String[service.length()];
                                k = 0;
                                while (service_key.hasNext()) {
                                    String key = service_key.next();
                                    try {
                                        String value = service.getString(key);
                                        servicelist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setService(servicelist);
                            }
                            JSONObject activities = facilities.optJSONObject("activities");
                            if (activities != null) {
                                Iterator<String> general_key = activities.keys();
                                String[] generallist = new String[activities.length()];
                                k = 0;
                                while (general_key.hasNext()) {
                                    String key = general_key.next();
                                    try {
                                        String value = activities.getString(key);
                                        generallist[k] = value;
                                    } catch (JSONException e) {
                                    }
                                    k++;
                                }
                                model.setGeneral(generallist);
                            }
                            try {
                                for (int z = 0; z < tripAdvisorArrayList.size(); z++) {
                                    if (model.getHuid().equalsIgnoreCase(tripAdvisorArrayList.get(z).getHuid())) {
                                        model.setReviewCount(tripAdvisorArrayList.get(z).getNumReview());
                                        model.setReviewImgUrl(tripAdvisorArrayList.get(z).getRatingImageUrl());
                                        break;
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            hotelList.add(model);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    pricelList.addAll(hotelList);
                    sortByHotelRank();
                    mAdapter.notifyDataSetChanged();
                    errorlist.clear();
                    finalhotelList = new ArrayList<>(hotelList);
                    hotelCityName = hotelList.get(0).getCity() + ", " + hotelList.get(0).getCountry();
                    Utils.printMessage(TAG, "hotel city name ::" + hotelCityName);
                }
            } catch (JSONException e) {
            } catch (Exception e) {
            }
        } else if (serviceType == GET_TRIP_ADVISOR) {
            Utils.printMessage(TAG, "SEARCH_HOTEL :: " + data);
            tripArrayList.clear();
            try {
                JSONObject obj = new JSONObject(data);
                if (obj.has("status")) {
                    if (!obj.getString("status").equalsIgnoreCase("SUCCESS")) {
                        return;
                    }
                    TripDialogBean bean = new TripDialogBean();
                    bean.setItemType(TripDialogBean.RATING_HEADER);
                    bean.setExpanded(true);
                    tripArrayList.add(bean);
                    if (obj.has("data")) {
                        if (obj.getJSONObject("data").has("staticInfo")) {
                            JSONObject staticInfo = obj.getJSONObject("data").getJSONObject("staticInfo");
                            if (staticInfo.has("rating_image_url")) {
                                bean = new TripDialogBean();
                                bean.setItemType(TripDialogBean.RATING_ITEM);
                                bean.setRatingImage(staticInfo.getString("rating_image_url"));
                                bean.setReviewsCount(staticInfo.getString("num_reviews"));
                                bean.setExpanded(true);
                                tripArrayList.add(bean);
                            }
                            if (staticInfo.has("subratings")) {
                                for (int i = 0; i < staticInfo.getJSONArray("subratings").length(); i++) {
                                    JSONObject tripObj = staticInfo.getJSONArray("subratings").getJSONObject(i);
                                    bean = new TripDialogBean();
                                    bean.setItemType(TripDialogBean.RATING_ITEM);
                                    bean.setRatingImage(tripObj.getString("rating_image_url"));
                                    bean.setReviewsCount("0");
                                    bean.setTitle(tripObj.getString("localized_name"));
                                    bean.setExpanded(true);
                                    tripArrayList.add(bean);
                                }
                            }
                        }
                        bean = new TripDialogBean();
                        bean.setItemType(TripDialogBean.REVIEW_HEADER);
                        bean.setExpanded(true);
                        tripArrayList.add(bean);
                        if (obj.getJSONObject("data").has("review")) {
                            for (int i = 0; i < obj.getJSONObject("data").getJSONArray("review").length(); i++) {
                                JSONObject tripObj = obj.getJSONObject("data").getJSONArray("review").getJSONObject(i);
                                bean = new TripDialogBean();
                                bean.setItemType(TripDialogBean.REVIEW_ITEM);
                                bean.setRatingImage(tripObj.getString("rating_image_url"));
                                bean.setTitle(tripObj.getString("title"));
                                bean.setPublishedDate(tripObj.getString("published_date"));
                                bean.setUser(tripObj.getString("user"));
                                bean.setReview(tripObj.getString("review"));
                                bean.setExpanded(true);
                                tripArrayList.add(bean);
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
            closeProgress();
            if (tripArrayList.size() > 0) {
                TripAdvisorDialog tripAdvisorDialog = new TripAdvisorDialog(mActivity, hotelNameVal, tripArrayList);
                tripAdvisorDialog.show();
                tripAdvisorDialog.setCancelable(true);
                Window window = tripAdvisorDialog.getWindow();
                int width = (int) (mActivity.getResources().getDisplayMetrics().widthPixels * 1.0);
                int height = (int) (mActivity.getResources().getDisplayMetrics().heightPixels * 0.85);
                window.setLayout(width, height);
            }
        }
    }


    @Override
    public void onTaskCancelled(String data, int serviceType) {

    }

    private void handleHotelSearch(String hotelName) {
        price_check = false;
        star_check = false;
        mAdapter.filter(hotelName);
        sortByHotelRank();
    }

    @Override
    public void onSelectedItemClick(int position, int type, String value) {

    }

    @Override
    public void onSelectedListItem(int position, String value, String hotelId) {
        handleHotelTripAdvisor(hotelId);
        hotelNameVal = value;
    }

    private void handleHotelTripAdvisor(String hUid) {
        String json = makeReviewsJson(hUid);
        new HTTPAsync(mActivity, HotelListFragment.this, Constants.TRIP_ADVISOR_DETAIL_URL, "", json,
                GET_TRIP_ADVISOR, HTTPAsync.METHOD_POST).execute();
    }

    private String makeReviewsJson(String hUid) {
        String result = "";
        final SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String language = preferences.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
        if (language.equalsIgnoreCase("en_GB")) {
            language = "en_US";
        }
        JSONObject source = new JSONObject();
        try {
            source.put("apikey", Constants.MERCHANDISE_API_KEY);
            source.put("HUID", hUid);
            source.put("lang", language);
            return source.toString();
        } catch (Exception e) {
        }
        return result;
    }

    private void showProgress() {
        if (barProgressDialog == null) {
            barProgressDialog = new ProgressDialog(mActivity);
            barProgressDialog.setMessage(mActivity.getString(R.string.label_just_a_moment));
            barProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            barProgressDialog.show();
        }
    }

    private void closeProgress() {
        if (barProgressDialog != null) {
            barProgressDialog.dismiss();
            barProgressDialog = null;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    private void handleSearchRequest(final String jsn) {
        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.REQUEST_JSON, jsn);
        editor.putBoolean(Constants.IS_ROOM_INFO_UPDATED, false);
        editor.apply();
        Utils.printMessage(TAG, "jsn::" + jsn);
        boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
        if (isInternetPresent) {
            new HTTPAsync(mActivity, HotelListFragment.this, Constants.HOTEL_SEARCH_URL, "", jsn,
                    SEARCH_HOTEL, HTTPAsync.METHOD_POST).execute();
        } else {
            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(regularFont);
            errorDescriptionText.setTypeface(regularFont);
            searchButton.setTypeface(regularFont);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isInternetPresent = Utils.isConnectingToInternet(mActivity);
                    if (isInternetPresent) {
                        loadingViewLayout.removeAllViews();
                        handleSearchRequest(jsn);
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(mActivity.getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_network_error_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(mActivity.getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_no_results_found_message_hotels));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private void parseSerachData(String data) {

        if (hotelList.size() > 0) {
            ArrayList<HotelModel> hotelLists = new ArrayList<>();
            HotelModel model;
            try {
                JSONObject obj = new JSONObject(data);
                JSONObject searchRSObj = obj.optJSONObject("searchRS");
                if (searchRSObj.has("err")) {
                    final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(regularFont);
                    errorDescriptionText.setTypeface(regularFont);
                    searchButton.setTypeface(regularFont);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mActivity.onBackPressed();
                        }
                    });
                    loadErrorType(Constants.RESULT_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
                {
                    JSONArray arr = searchRSObj.getJSONArray("hotel");
                    for (int i = 0; i < arr.length(); i++) {
                        model = new HotelModel();
                        JSONObject rcObj1 = arr.optJSONObject(i);
                        JSONArray rcArr1 = rcObj1.getJSONArray("rc");
                        JSONObject rObj1 = rcArr1.optJSONObject(0);
                        JSONObject htObj1 = rObj1.optJSONObject("ht");
                        boolean isHotelContains = false;
                        for (int k = 0; k < hotelList.size(); k++) {
                            if (htObj1.optString("uid").equalsIgnoreCase(hotelList.get(k).getHuid())) {
                                model = hotelList.get(k);
                                isHotelContains = true;
                                break;
                            }
                        }
                        boolean isContains = false;
                        for (int j = 0; j < hotelLists.size(); j++) {
                            if (hotelLists.get(j).getHuid().equalsIgnoreCase(model.getHuid())) {
                                isContains = true;
                                break;
                            }
                        }
                        if (!isContains && isHotelContains) {
                            JSONObject rcObj = arr.optJSONObject(i);
                            model.setCheck_in_date(hotelCheckInDate);
                            model.setCheck_out_date(hotelCheckOutDate);
                            model.setHotelJson(rcObj.toString());
                            JSONArray rcArr = rcObj.getJSONArray("rc");
                            for (int j = 0; j < rcArr.length(); j++) {
                                ArrayList<RoomModel> roomlist = new ArrayList<>();
                                JSONObject rObj = rcArr.optJSONObject(j);
                                model.setDur(rObj.optString("dur"));
                                JSONObject pObj = rObj.optJSONObject("p");
                                model.setP(pObj.optString("val"));
                                model.setCur(pObj.optString("cur"));
                                JSONObject wdObj = rObj.optJSONObject("wdp");
                                model.setWdp(wdObj.optString("val"));
                                JSONObject htObj = rObj.optJSONObject("ht");
                                model.setHuid(htObj.optString("uid"));
                                dealsNearCityId = htObj.optString("cid");
                                ArrayList<String> adultcount = occupancy_adult_count_list;
                                model.setAdultcountlist(adultcount);
                                model.setChild_count_list(occupancy_child_count_list);
                                JSONArray roomArray = rObj.getJSONArray("room");
                                for (int k = 0; k < roomArray.length(); k++) {
                                    RoomModel roomModel = new RoomModel();
                                    JSONObject roomObj = roomArray.optJSONObject(k);
                                    roomModel.setNight_count(roomObj.optString("dur"));
                                    JSONObject pRoomObj = roomObj.optJSONObject("p");
                                    roomModel.setPrice(pRoomObj.optString("val"));
                                    roomModel.setCurrency(pRoomObj.optString("cur"));
                                    JSONObject wdpRoomObj = roomObj.optJSONObject("wdp");
                                    roomModel.setWdp(wdpRoomObj.optString("val"));
                                    roomModel.setBrakfast(roomObj.optString("mn"));
                                    roomModel.setRoom_type(roomObj.optString("rn"));
                                    JSONObject piRoomObj = roomObj.optJSONObject("pi");
                                    try {
                                        if (piRoomObj.has("FreeCancellationDate")) {
                                            roomModel.setFree_cancel_date(piRoomObj.optString("FreeCancellationDate"));
                                        } else {
                                            roomModel.setFree_cancel_date("");
                                        }
                                    } catch (Exception e) {
                                        Utils.printMessage(TAG, "Free canclDate Err :: " + e.getMessage());
                                    }
                                    roomModel.setRoom_count("" + roomArray.length());
                                    roomlist.add(roomModel);
                                }
                                model.setRoomlist(roomlist);
                                model.setDate(hotelCheckInDate);
                                if (!model.getRoomlist().get(0).getFree_cancel_date().isEmpty()) {
                                    DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
                                    Date dateBefore = null;
                                    try {
                                        dateBefore = simpleDateFormat.parse(model.getRoomlist().get(0).getFree_cancel_date());
                                    } catch (Exception e) {
                                    }
                                    SimpleDateFormat finalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                    String finalDate = finalFormat.format(dateBefore);
                                    String CancellationDateDiff = Utils.flightSearchDateDifference(finalDate, currentDate);
                                    model.setBnplDateDiff(Integer.parseInt(CancellationDateDiff));
                                } else {
                                    model.setBnplDateDiff(0);
                                }
                            }
                            huidlist.add(model.getHuid());
                            hotelLists.add(model);
                        }
                    }
                    hotelList.clear();
                    pricelList.clear();
                    finalhotelList.clear();
                    hotelList.addAll(hotelLists);
                    pricelList.addAll(hotelLists);
                    sortByHotelRank();
                    finalhotelList = new ArrayList<>(hotelList);
                    mAdapter = new HotelListsAdapter(pricelList, this, mActivity, header_date, adults, childs, adultList, childCount,
                            childList, hotelList, currentDate, tripAdvisorArrayList, HotelListFragment.this, randomNumber, city,
                            hotelCheckInDate, hotelCheckOutDate);
                    recyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                    Utils.printMessage(TAG, "hotelLists :: " + hotelLists.size());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            hotelList.clear();
            huidlist.clear();
            pricelList.clear();
            String jsn = "";
            try {
                JSONObject obj = new JSONObject(data);
                JSONObject searchRSObj = obj.optJSONObject("searchRS");
                if (searchRSObj.has("err")) {
                    try {
                        Utils.printMessage(TAG, "search count :: " + searchCount);
                        if (searchCount == 1) {
                            searchType = "CITY";
                        } else {
                            final View errorView = layoutInflater.inflate(R.layout.view_error_response, null);
                            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                            errorText = (TextView) errorView.findViewById(R.id.error_text);
                            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                            searchButton = (Button) errorView.findViewById(R.id.search_button);
                            errorText.setTypeface(regularFont);
                            errorDescriptionText.setTypeface(regularFont);
                            searchButton.setTypeface(regularFont);
                            searchButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mActivity.onBackPressed();
                                    loadingViewLayout.removeAllViews();
                                }
                            });
                            loadErrorType(Constants.RESULT_ERROR);
                            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            loadingViewLayout.addView(errorView);
                        }
                    } catch (Exception e) {
                    }
                } else {
                    JSONArray arr = searchRSObj.getJSONArray("hotel");
                    for (int i = 0; i < arr.length(); i++) {
                        HotelModel model = new HotelModel();
                        JSONObject rcObj = arr.optJSONObject(i);
                        model.setCheck_in_date(hotelCheckInDate);
                        model.setCheck_out_date(hotelCheckOutDate);
                        model.setHotelJson(rcObj.toString());
                        JSONArray rcArr = rcObj.getJSONArray("rc");
                        for (int j = 0; j < rcArr.length(); j++) {
                            ArrayList<RoomModel> roomlist = new ArrayList<>();
                            JSONObject rObj = rcArr.optJSONObject(j);
                            model.setDur(rObj.optString("dur"));
                            JSONObject pObj = rObj.optJSONObject("p");
                            model.setP(pObj.optString("val"));
                            model.setCur(pObj.optString("cur"));
                            JSONObject wdObj = rObj.optJSONObject("wdp");
                            model.setWdp(wdObj.optString("val"));
                            JSONObject htObj = rObj.optJSONObject("ht");
                            model.setHuid(htObj.optString("uid"));
                            dealsNearCityId = htObj.optString("cid");
                            ArrayList<String> adultcount = occupancy_adult_count_list;
                            model.setAdultcountlist(adultcount);
                            model.setChild_count_list(occupancy_child_count_list);
                            JSONArray roomArray = rObj.getJSONArray("room");
                            for (int k = 0; k < roomArray.length(); k++) {
                                RoomModel roomModel = new RoomModel();
                                JSONObject roomObj = roomArray.optJSONObject(k);
                                roomModel.setNight_count(roomObj.optString("dur"));
                                JSONObject pRoomObj = roomObj.optJSONObject("p");
                                roomModel.setPrice(pRoomObj.optString("val"));
                                roomModel.setCurrency(pRoomObj.optString("cur"));
                                JSONObject wdpRoomObj = roomObj.optJSONObject("wdp");
                                roomModel.setWdp(wdpRoomObj.optString("val"));
                                roomModel.setBrakfast(roomObj.optString("mn"));
                                roomModel.setRoom_type(roomObj.optString("rn"));
                                JSONObject piRoomObj = roomObj.optJSONObject("pi");
                                try {
                                    if (piRoomObj.has("FreeCancellationDate")) {
                                        roomModel.setFree_cancel_date(piRoomObj.optString("FreeCancellationDate"));
                                    } else {
                                        roomModel.setFree_cancel_date("");
                                    }
                                } catch (Exception e) {
                                    Utils.printMessage(TAG, "Free canclDate Err :: " + e.getMessage());
                                }
                                roomModel.setRoom_count("" + roomArray.length());
                                roomlist.add(roomModel);
                            }
                            if (rObj.has("ht")) {
                                if (rObj.getJSONObject("ht").has("hna")) {
                                    model.setHna(rObj.getJSONObject("ht").getString("hna"));
                                }
                            }
                            model.setRoomlist(roomlist);
                            model.setDate(hotelCheckInDate);
                        }
                        huidlist.add(model.getHuid());
                        hotelList.add(model);
                        pricelList.add(model);
                    }
                }
            } catch (JSONException e) {
            }
        }
    }

    private String makeJson() {
        String result = null;
        if (mActivity != null) {
            final SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            String language = preferences.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
            JSONObject source = new JSONObject();
            JSONObject crt = new JSONObject();
            JSONObject hcp = new JSONObject();
            JSONObject finaldata = new JSONObject();
            String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
            String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
            String timeStamp = currentDateFormat + "T" + currentTimeFormat;
            try {
                source.put("device", Constants.DEVICE_TYPE);
                source.put("clientId", Constants.HOTEL_CLIENT_ID);
                source.put("clientSecret", Constants.HOTEL_CLIENT_SECRET);
                source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
                source.put("accessToken", Utils.getRequestAccessToken(mActivity));
                source.put("timeStamp", timeStamp);
                JSONArray mJSONArray = new JSONArray(huidlist);
                if (dealsNearMeClicked) {
                    crt.put("hcid", dealsNearCityId);
                } else {
                    crt.put("hcid", cityId);
                }
                crt.put("language", language);
                crt.put("huid", mJSONArray);
                crt.put("pt", "B|F");
                hcp.put("source", source);
                hcp.put("crt", crt);
                finaldata.put("hcp", hcp);
                return finaldata.toString();
            } catch (JSONException e) {
            } catch (Exception e) {
            }
        }
        return result;
    }

    private String Json(boolean cityIdRequired) {
        JSONObject obj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String userSelectedCurr = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        try {
            JSONObject source = new JSONObject();
            source = Utils.getHotelSearchAppSettingData(source, mActivity);
            source.put("clientId", Constants.HOTEL_CLIENT_ID);
            source.put("clientSecret", Constants.HOTEL_CLIENT_SECRET);
            source.put("paxNationality", Utils.getUserCountry(mActivity));
            source.put("echoToken", randomNumber);
            source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
            source.put("accessToken", Utils.getRequestAccessToken(mActivity));
            source.put("timeStamp", timeStamp);
            source.put("currency", userSelectedCurr);
            source.put("groupType", groupType);
            JSONObject sourceRQ = new JSONObject();
            sourceRQ.put("source", source);
            if (cityIdRequired) {
                JSONObject sc = new JSONObject();
                sc.put("ad", Utils.formatDateToServerDateFormat(hotelCheckInDate));
                sc.put("dur", nights);
                sc.put("cid", cityId);
                sc.put("skm", "false");
                sourceRQ.put("sc", sc);
            } else {
                if (latitude == 0.0 && longitude == 0.0) {
                    checkLatLongValues();
                    return null;
                }
                JSONObject sc = new JSONObject();
                String inDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
                hotelCheckInDate = Utils.addDaysToCalendar(inDate);
                hotelCheckOutDate = Utils.addOneDayToCalendar(hotelCheckInDate);
                from_date = Utils.convertToJourneyDate(hotelCheckInDate, mActivity);
                to_date = Utils.convertToJourneyDate(hotelCheckOutDate, mActivity);
                sc.put("ad", hotelCheckInDate);
                checkDateDiff();
                sc.put("dur", nights);
                sc.put("skm", "false");
                sourceRQ.put("sc", sc);
                JSONObject gc = new JSONObject();
                gc.put("lat", String.valueOf(latitude));
                gc.put("lon", String.valueOf(longitude));
                gc.put("rds", Constants.HOTEL_GPS_RDS);
                sourceRQ.put("gc", gc);
            }
            JSONObject rms = new JSONObject();
            room_count = roomPassengerArrayList.size();
            rms.put("rm", addRoomsArray(roomPassengerArrayList.size(), roomPassengerArrayList));
            sourceRQ.put("rms", rms);
            obj.put("searchRQ", sourceRQ);
        } catch (Exception e) {
        }
        return obj.toString();
    }

    private int checkDateDiff() {
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(hotelCheckInDate);
            Date date2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(hotelCheckOutDate);
            night_count = (int) ((date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000));
            return night_count;
        } catch (Exception e) {
        }
        return 0;
    }

    private void checkLatLongValues() {
        if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                ) {
            requestPermission(mActivity);
            return;
        }
        gps = new GPSTracker(mActivity);
        if (gps.canGetLocation()) {
            showProgress();
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            String jsn = Json(false);
            if (jsn != null) {
                handleSearchRequest(jsn);
            }
        } else {
            gps.showSettingsAlert();
        }
    }

    private static void requestPermission(final Context context) {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION)) {
            new AlertDialog.Builder(context)
                    .setMessage(context.getResources().getString(R.string.permission_storage))
                    .setPositiveButton(R.string.label_selected_flight_message, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                    2);
                        }
                    }).show();
        } else {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    2);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Utils.printMessage(TAG, "onRequestPermissionsResult::" + grantResults.length);
        switch (requestCode) {
            case 2: {
                if (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    Utils.printMessage(TAG, "permission granted");
                } else {
                    Utils.printMessage(TAG, "permission granted failure");
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                return;
            }
        }
    }

    private JSONArray addRoomsArray(int roomSize, ArrayList<FhRoomPassengerDetailsBean> passengerList) {
        occupancy_adult_count_list.clear();
        occupancy_child_count_list.clear();
        JSONArray rmArr = new JSONArray();
        int noOfAdults = 0;
        int noOfChildrens = 0;
        if (roomSize == 1) {
            noOfAdults = passengerList.get(0).getAdultCnt();
            noOfChildrens = passengerList.get(0).getChildCnt();
        } else {
            for (int i = 0; i < roomSize; i++) {
                int occupancy = passengerList.get(i).getAdultCnt() + passengerList.get(i).getChildCnt();
                if (noOfAdults < occupancy) {
                    noOfAdults = occupancy;
                }
            }
        }
        int passengerID = 0;
        for (int i = 0; i < roomSize; i++) {
            JSONObject roomDetailsObj = new JSONObject();
            JSONArray paxArr = new JSONArray();
            ArrayList<String> age_child = new ArrayList<>();
            try {
                roomDetailsObj.put("na", noOfAdults);
                roomDetailsObj.put("nc", noOfChildrens);
                for (int j = 0; j < noOfAdults; j++) {
                    JSONObject paxDetailObj = new JSONObject();
                    passengerID++;
                    paxDetailObj.put("age", 45);
                    paxDetailObj.put("id", passengerID);
                    paxArr.put(paxDetailObj);
                }
                occupancy_adult_count_list.add(String.valueOf(noOfAdults));
                if (noOfChildrens > 0) {
                    for (int j = 0; j < noOfChildrens; j++) {
                        JSONObject paxDetailObj = new JSONObject();
                        passengerID++;
                        paxDetailObj.put("age", passengerList.get(i).getChildAgeArray().get(j));
                        paxDetailObj.put("id", passengerID);
                        paxArr.put(paxDetailObj);

                        age_child.add(String.valueOf(passengerList.get(i).getChildAgeArray().get(j)));
                    }
                }
                occupancy_child_count_list.put("room" + String.valueOf(i + 1), age_child);
                roomDetailsObj.put("pax", paxArr);
            } catch (JSONException e) {
            }
            rmArr.put(roomDetailsObj);
        }
        for (int i = 0; i < roomSize; i++) {
            int childCounts = 0;
            ArrayList<String> age_child = new ArrayList<>();
            childCounts = passengerList.get(i).getChildCnt();
            adultList.add(String.valueOf(passengerList.get(i).getAdultCnt()));
            if (childCounts > 0) {
                childList.add(String.valueOf(passengerList.get(i).getChildCnt()));
            }
            if (childCounts > 0) {
                for (int j = 0; j < childCounts; j++) {
                    age_child.add(String.valueOf(passengerList.get(i).getChildAgeArray().get(j)));
                }
            }
            childCount.put("room" + String.valueOf(i + 1), age_child);
        }
        return rmArr;
    }

    private String hotelJsonTripAdvisor() {
        String result = null;
        if (mActivity != null) {
            JSONObject source = new JSONObject();
            JSONObject crt = new JSONObject();
            JSONObject hcp = new JSONObject();
            JSONObject finaldata = new JSONObject();
            String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
            String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
            String timeStamp = currentDateFormat + "T" + currentTimeFormat;
            try {
                source.put("device", Constants.DEVICE_TYPE);
                source.put("clientId", Constants.HOTEL_CLIENT_ID);
                source.put("clientSecret", Constants.HOTEL_CLIENT_SECRET);
                source.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
                source.put("accessToken", Utils.getRequestAccessToken(mActivity));
                source.put("timeStamp", timeStamp);
                JSONArray mJSONArray = new JSONArray(huidlist);
                if (dealsNearMeClicked) {
                    crt.put("hcid", dealsNearCityId);
                } else {
                    crt.put("hcid", cityId);
                }
                crt.put("huid", mJSONArray);
                hcp.put("source", source);
                hcp.put("crt", crt);
                finaldata.put("hcp", hcp);
                return finaldata.toString();
            } catch (JSONException e) {
            } catch (Exception e) {
            }
        }
        return result;
    }
}
