package com.flyin.bookings;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.flyin.bookings.adapter.RewardsStatementAdapter;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.LoyaltyBookingInfoBean;

import java.util.ArrayList;

@SuppressWarnings("ALL")
public class RecentTransactionsFragment extends Fragment implements OnCustomItemSelectListener {
    private RewardsStatementAdapter rewardsStatementAdapter;
    private ArrayList<LoyaltyBookingInfoBean> rewardsStatementBeanArrayList = new ArrayList<>();
    private ArrayList<LoyaltyBookingInfoBean> recentTrasactionList = null;
    private RecyclerView recyclerView;
    private RelativeLayout loading_view_layout;
    private Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rewards_statement_frgament, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.rewards_recyclelistview);
        loading_view_layout = (RelativeLayout) view.findViewById(R.id.loading_view_layout);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        if (RewardsActivity.loyaltyDataBean.getLoyaltyBookingInfoArrList() != null) {
            rewardsStatementBeanArrayList = RewardsActivity.loyaltyDataBean.getLoyaltyBookingInfoArrList();
            recentTrasactionList = new ArrayList<>();
            if (rewardsStatementBeanArrayList.size() > 10) {
                for (int i = 0; i < 10; i++) {
                    recentTrasactionList.add(rewardsStatementBeanArrayList.get(i));
                }
            } else {
                for (int i = 0; i < rewardsStatementBeanArrayList.size(); i++) {
                    recentTrasactionList.add(rewardsStatementBeanArrayList.get(i));
                }
            }
            if (recentTrasactionList.size() > 0) {
                rewardsStatementAdapter = new RewardsStatementAdapter(mActivity, recentTrasactionList, RecentTransactionsFragment.this);
                recyclerView.setAdapter(rewardsStatementAdapter);
                rewardsStatementAdapter.notifyDataSetChanged();
            }
        }
        return view;
    }

    @Override
    public void onItemClick(int position) {
        boolean expandStatus = recentTrasactionList.get(position).isExpanded();
        if (!expandStatus) {
            for (LoyaltyBookingInfoBean loyaltyBookingInfoBean : recentTrasactionList) {
                loyaltyBookingInfoBean.setExpanded(false);
            }
            recentTrasactionList.get(position).setExpanded(!expandStatus);
        } else {
            for (LoyaltyBookingInfoBean loyaltyBookingInfoBean : recentTrasactionList) {
                loyaltyBookingInfoBean.setExpanded(false);
            }
        }
        rewardsStatementAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSelectedItemClick(int position, int type) {

    }
}
