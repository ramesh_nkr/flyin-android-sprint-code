package com.flyin.bookings;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.adapter.FlightAirlineFilterAdapter;
import com.flyin.bookings.adapter.FlightAirportFilterAdapter;
import com.flyin.bookings.adapter.FlightRefundStatusAdapter;
import com.flyin.bookings.adapter.FlightStopsFilterAdapter;
import com.flyin.bookings.adapter.FlightTimeFilterAdapter;
import com.flyin.bookings.listeners.AirlinesInterface;
import com.flyin.bookings.model.FSDataBean;
import com.flyin.bookings.model.FilterAirlineSelectionItem;
import com.flyin.bookings.model.FilterAirportSelectionItem;
import com.flyin.bookings.model.FilterRefundSelectionItem;
import com.flyin.bookings.model.FilterStopSelectionItem;
import com.flyin.bookings.model.FilterTimeSelectionItem;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.OriginDestinationOptionBean;
import com.flyin.bookings.model.PricedItineraryObject;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.util.AirlinesData;
import com.flyin.bookings.util.AirportData;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class FlightReturnFilterActivity extends AppCompatActivity implements Serializable {
    private RelativeLayout priceSideLayout = null;
    private RelativeLayout stopsSideLayout = null;
    private RelativeLayout airlinesSideLayout = null;
    private RelativeLayout airportsSideLayout = null;
    private RelativeLayout timeSideLayout = null;
    private TextView filterPrice = null;
    private TextView filterStops = null;
    private TextView filterAirline = null;
    private TextView filterAirports = null;
    private TextView filterTime = null;
    private TextView minPriceValue = null;
    private TextView maxPriceValue = null;
    private ImageView priceFilterImage = null;
    private ImageView stopsFilterImage = null;
    private ImageView airlineFilterImage = null;
    private ImageView airportFilterImage = null;
    private ImageView timeFilterImage = null;
    private Button mApplyButton = null;
    private Button mResetButton = null;
    private LinearLayout refundStatusLayout = null;
    private Typeface textFace = null;
    private Typeface titleFace = null;
    private FlightRefundStatusAdapter refundStatusAdapter;
    private FlightAirlineFilterAdapter airlineFilterAdapter;
    private FlightAirportFilterAdapter airportFilterAdapter;
    private FlightStopsFilterAdapter stopsFilterAdapter;
    private FlightTimeFilterAdapter timeFilterAdapter;
    private Double priceMinValue = 0.0;
    private Double priceMaxValue = 0.0;
    private RangeSeekBar priceRangeBar;
    private ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList = new ArrayList<>();
    private ArrayList priceArrayList = new ArrayList();
    private ArrayList<FilterAirlineSelectionItem> airLineArray = null;
    private ArrayList<FilterAirportSelectionItem> tempArray1 = null;
    private ArrayList<FilterAirportSelectionItem> tempArray2 = null;
    private ArrayList<FilterAirportSelectionItem> tempArray3 = null;
    private ArrayList<FilterAirportSelectionItem> airportArray = null;
    private ArrayList<FilterStopSelectionItem> stopsArray = null;
    private ArrayList<FilterTimeSelectionItem> timeArray = null;
    private ArrayList<FilterTimeSelectionItem> dapTimeListArray = null;
    private ArrayList<FilterTimeSelectionItem> aapTimeListArray = null;
    private ArrayList<FilterRefundSelectionItem> refundStatusArray = null;
    private ArrayList<String> rsArray = null;
    private String minSelectedValue = "";
    private String maxSelectedValue = "";
    private static final String TAG = "FlightForwardFilterActivity";
    private String tripType = "";
    private String sourceName = "";
    private String destinationName = "";
    private String selectedLang = "";
    private boolean isdetailfilter;
    private String secondsourceName = "";
    private String secondreturnName = "";
    private String thirdsourceName = "";
    private String thirdreturnName = "";
    private boolean isNonStopChecked = false;
    private boolean multiCityNonStopChecked = false;
    private ArrayList<String> airportArrayList = new ArrayList<>();
    private ArrayList<String> airlineArrayList = new ArrayList<>();
    private HashMap<String, FlightAirportName> airportNamesMap = new HashMap<>();
    private HashMap<String, String> airLineNamesMap = new HashMap<>();
    private AirportData airportData = new AirportData();
    private AirlinesData airlinesData = new AirlinesData();
    private String selectedMal = "";
    private String forwardFlightPrice = "";
    private String totalPackagePrice = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flightfilter);
        selectedLang = Constants.LANGUAGE_ENGLISH_CODE;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontLight = Constants.FONT_ROBOTO_LIGHT;
        airLineArray = new ArrayList<>();
        airportArray = new ArrayList<>();
        tempArray1 = new ArrayList<>();
        tempArray2 = new ArrayList<>();
        tempArray3 = new ArrayList<>();
        stopsArray = new ArrayList<>();
        timeArray = new ArrayList<>();
        dapTimeListArray = new ArrayList<>();
        aapTimeListArray = new ArrayList<>();
        rsArray = new ArrayList<>();
        refundStatusArray = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Constants.TRIP_TYPE)) {
                tripType = bundle.getString(Constants.TRIP_TYPE, "");
                airLineNamesMap = (HashMap<String, String>) bundle.get(Constants.AIRLINE_NAME_HASHMAP);
                airportNamesMap = (HashMap<String, FlightAirportName>) bundle.get(Constants.AIRPORT_NAME_HASHMAP);
            }
            sourceName = bundle.getString(Constants.FORWARD_SOURCE, "");
            destinationName = bundle.getString(Constants.FORWARD_DESTINATION, "");
            secondsourceName = bundle.getString(Constants.SECOND_FORWARD_SOURCE, "");
            secondreturnName = bundle.getString(Constants.SECOND_FORWARD_DESTINATION, "");
            thirdsourceName = bundle.getString(Constants.THIRD_FORWARD_SOURCE, "");
            thirdreturnName = bundle.getString(Constants.THIRD_FORWARD_DESTINATION, "");
            minSelectedValue = bundle.getString(Constants.PRICE_SELECTED_MINIMUM_VALUE);
            maxSelectedValue = bundle.getString(Constants.PRICE_SELECTED_MAXIMUM_VALUE);
            isNonStopChecked = bundle.getBoolean(Constants.ONE_WAY_NONSTOP_CHECKED, false);
            multiCityNonStopChecked = bundle.getBoolean(Constants.MULTI_TRIP_NONSTOP_CHECKED, false);
            isdetailfilter = bundle.getBoolean(Constants.IS_DETAIL_FILTER, false);
            selectedMal = bundle.getString(Constants.AIRLINE_NAME, "");
            forwardFlightPrice = bundle.getString(Constants.FORWARD_FLIGHT_PRICE, "");
            totalPackagePrice = bundle.getString(Constants.PACKAGE_FLIGHT_PRICE, "");
        }
        filterPrice = (TextView) findViewById(R.id.text_price_filter);
        filterStops = (TextView) findViewById(R.id.text_stops_filter);
        filterAirline = (TextView) findViewById(R.id.text_airline_filter);
        filterAirports = (TextView) findViewById(R.id.text_airports_filter);
        filterTime = (TextView) findViewById(R.id.text_time_filter);
        TextView forwardFlightDetails = (TextView) findViewById(R.id.forward_flight_details);
        TextView airlineSelection = (TextView) findViewById(R.id.airline_selection);
        minPriceValue = (TextView) findViewById(R.id.price_min_value);
        maxPriceValue = (TextView) findViewById(R.id.price_max_value);
        if (Utils.isArabicLangSelected(FlightReturnFilterActivity.this)) {
            selectedLang = Constants.LANGUAGE_ARABIC_CODE;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontLight = Constants.FONT_DROIDKUFI_REGULAR;
            filterPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            filterStops.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            filterAirline.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            filterAirports.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            filterTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            minPriceValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            maxPriceValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        priceFilterImage = (ImageView) findViewById(R.id.image_price_filter);
        stopsFilterImage = (ImageView) findViewById(R.id.image_stops_filter);
        airlineFilterImage = (ImageView) findViewById(R.id.image_airline_filter);
        airportFilterImage = (ImageView) findViewById(R.id.image_airports_filter);
        timeFilterImage = (ImageView) findViewById(R.id.image_time_filter);
        RelativeLayout priceLayout = (RelativeLayout) findViewById(R.id.filter_price_layout);
        RelativeLayout stopsLayout = (RelativeLayout) findViewById(R.id.filter_stops_layout);
        RelativeLayout airlinesLayout = (RelativeLayout) findViewById(R.id.filter_airline_layout);
        RelativeLayout airportsLayout = (RelativeLayout) findViewById(R.id.filter_airports_layout);
        RelativeLayout timeLayout = (RelativeLayout) findViewById(R.id.filter_time_layout);
        priceSideLayout = (RelativeLayout) findViewById(R.id.filter_price_sideLayout);
        stopsSideLayout = (RelativeLayout) findViewById(R.id.filter_stops_sideLayout);
        airlinesSideLayout = (RelativeLayout) findViewById(R.id.filter_airlines_sideLayout);
        airportsSideLayout = (RelativeLayout) findViewById(R.id.filter_airports_sideLayout);
        timeSideLayout = (RelativeLayout) findViewById(R.id.filter_time_sideLayout);
        refundStatusLayout = (LinearLayout) findViewById(R.id.refundable_status_layout);
        ListView refundStatusListView = (ListView) findViewById(R.id.filter_price_listView);
        ListView airlineListView = (ListView) findViewById(R.id.filter_airlines_listView);
        ListView airportListView = (ListView) findViewById(R.id.filter_airports_listView);
        ListView stopsListView = (ListView) findViewById(R.id.filter_stops_listView);
        ListView timeListView = (ListView) findViewById(R.id.filter_time_listView);
        priceRangeBar = (RangeSeekBar) findViewById(R.id.price_slider);
        if (!sourceName.equalsIgnoreCase("") && secondsourceName.equalsIgnoreCase("") && thirdsourceName.equalsIgnoreCase("")) {
            forwardFlightDetails.setText(sourceName + " - " + destinationName);
        } else if (!sourceName.equalsIgnoreCase("") && !secondsourceName.equalsIgnoreCase("") && thirdsourceName.equalsIgnoreCase("")) {
            forwardFlightDetails.setText(sourceName + " - " + destinationName + ", " + secondsourceName + " - " + secondreturnName);
        } else if (!sourceName.equalsIgnoreCase("") && !secondsourceName.equalsIgnoreCase("") && !thirdsourceName.equalsIgnoreCase("")) {
            forwardFlightDetails.setText(sourceName + " - " + destinationName + ", " + secondsourceName + " - " + secondreturnName + ", " + thirdsourceName + " - " + thirdreturnName);
        }
        textFace = Typeface.createFromAsset(getAssets(), fontTitle);
        titleFace = Typeface.createFromAsset(getAssets(), fontLight);
        filterPrice.setTypeface(textFace);
        filterStops.setTypeface(titleFace);
        filterAirline.setTypeface(titleFace);
        filterAirports.setTypeface(titleFace);
        filterTime.setTypeface(titleFace);
        minPriceValue.setTypeface(textFace);
        maxPriceValue.setTypeface(textFace);
        forwardFlightDetails.setTypeface(textFace);
        airlineSelection.setTypeface(textFace);
        priceLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                priceSideLayout.setVisibility(View.VISIBLE);
                stopsSideLayout.setVisibility(View.GONE);
                airlinesSideLayout.setVisibility(View.GONE);
                airportsSideLayout.setVisibility(View.GONE);
                timeSideLayout.setVisibility(View.GONE);
                filterPrice.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.user_profile_email_color));
                priceFilterImage.setImageResource(R.drawable.price_selected);
                filterPrice.setTypeface(textFace);
                filterStops.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                stopsFilterImage.setImageResource(R.drawable.stops);
                filterStops.setTypeface(titleFace);
                filterAirline.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                airlineFilterImage.setImageResource(R.drawable.airline);
                filterAirline.setTypeface(titleFace);
                filterAirports.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                airportFilterImage.setImageResource(R.drawable.airports);
                filterAirports.setTypeface(titleFace);
                filterTime.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                timeFilterImage.setImageResource(R.drawable.time);
                filterTime.setTypeface(titleFace);
            }
        });
        stopsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                priceSideLayout.setVisibility(View.GONE);
                stopsSideLayout.setVisibility(View.VISIBLE);
                airlinesSideLayout.setVisibility(View.GONE);
                airportsSideLayout.setVisibility(View.GONE);
                timeSideLayout.setVisibility(View.GONE);
                filterPrice.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                priceFilterImage.setImageResource(R.drawable.price);
                filterPrice.setTypeface(titleFace);
                filterStops.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.user_profile_email_color));
                stopsFilterImage.setImageResource(R.drawable.stops_selected);
                filterStops.setTypeface(textFace);
                filterAirline.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                airlineFilterImage.setImageResource(R.drawable.airline);
                filterAirline.setTypeface(titleFace);
                filterAirports.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                airportFilterImage.setImageResource(R.drawable.airports);
                filterAirports.setTypeface(titleFace);
                filterTime.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                timeFilterImage.setImageResource(R.drawable.time);
                filterTime.setTypeface(titleFace);
            }
        });
        airlinesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                priceSideLayout.setVisibility(View.GONE);
                stopsSideLayout.setVisibility(View.GONE);
                airlinesSideLayout.setVisibility(View.VISIBLE);
                airportsSideLayout.setVisibility(View.GONE);
                timeSideLayout.setVisibility(View.GONE);
                filterPrice.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                priceFilterImage.setImageResource(R.drawable.price);
                filterPrice.setTypeface(titleFace);
                filterStops.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                stopsFilterImage.setImageResource(R.drawable.stops);
                filterStops.setTypeface(titleFace);
                filterAirline.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.user_profile_email_color));
                airlineFilterImage.setImageResource(R.drawable.airline_selected);
                filterAirline.setTypeface(textFace);
                filterAirports.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                airportFilterImage.setImageResource(R.drawable.airports);
                filterAirports.setTypeface(titleFace);
                filterTime.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                timeFilterImage.setImageResource(R.drawable.time);
                filterTime.setTypeface(titleFace);
            }
        });
        airportsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                priceSideLayout.setVisibility(View.GONE);
                stopsSideLayout.setVisibility(View.GONE);
                airlinesSideLayout.setVisibility(View.GONE);
                airportsSideLayout.setVisibility(View.VISIBLE);
                timeSideLayout.setVisibility(View.GONE);
                filterPrice.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                priceFilterImage.setImageResource(R.drawable.price);
                filterPrice.setTypeface(titleFace);
                filterStops.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                stopsFilterImage.setImageResource(R.drawable.stops);
                filterStops.setTypeface(titleFace);
                filterAirline.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                airlineFilterImage.setImageResource(R.drawable.airline);
                filterAirline.setTypeface(titleFace);
                filterAirports.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.user_profile_email_color));
                airportFilterImage.setImageResource(R.drawable.airports_selected);
                filterAirports.setTypeface(textFace);
                filterTime.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                timeFilterImage.setImageResource(R.drawable.time);
                filterTime.setTypeface(titleFace);
            }
        });
        timeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                priceSideLayout.setVisibility(View.GONE);
                stopsSideLayout.setVisibility(View.GONE);
                airlinesSideLayout.setVisibility(View.GONE);
                airportsSideLayout.setVisibility(View.GONE);
                timeSideLayout.setVisibility(View.VISIBLE);
                filterPrice.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                priceFilterImage.setImageResource(R.drawable.price);
                filterPrice.setTypeface(titleFace);
                filterStops.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                stopsFilterImage.setImageResource(R.drawable.stops);
                filterStops.setTypeface(titleFace);
                filterAirline.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                airlineFilterImage.setImageResource(R.drawable.airline);
                filterAirline.setTypeface(titleFace);
                filterAirports.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.filter_side_text));
                airportFilterImage.setImageResource(R.drawable.airports);
                filterAirports.setTypeface(titleFace);
                filterTime.setTextColor(Utils.getColor(FlightReturnFilterActivity.this, R.color.user_profile_email_color));
                timeFilterImage.setImageResource(R.drawable.time_selected);
                filterTime.setTypeface(textFace);
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_flight, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        ImageView mImageView = (ImageView) mCustomView.findViewById(R.id.back_image);
        mResetButton = (Button) mCustomView.findViewById(R.id.filter_reset_button);
        mApplyButton = (Button) mCustomView.findViewById(R.id.filter_apply_button);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.home_layout);
        TextView backtext = (TextView) mCustomView.findViewById(R.id.back_to_home);
        mResetButton.setTypeface(textFace);
        mApplyButton.setTypeface(textFace);
        backtext.setTypeface(textFace);
        if (Utils.isArabicLangSelected(FlightReturnFilterActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backtext.setLayoutParams(params);
            mResetButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            mApplyButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            backtext.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        refundStatusAdapter = new FlightRefundStatusAdapter(FlightReturnFilterActivity.this, refundStatusArray);
        refundStatusListView.setAdapter(refundStatusAdapter);
        refundStatusListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                refundStatusArray.get(position).setIsSelected(!refundStatusArray.get(position).isSelected());
                refundStatusAdapter.notifyDataSetChanged();
            }
        });
        airlineFilterAdapter = new FlightAirlineFilterAdapter(FlightReturnFilterActivity.this, airLineArray, airLineNamesMap);
        airlineListView.setAdapter(airlineFilterAdapter);
        airlineListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                airLineArray.get(position).setIsSelected(!airLineArray.get(position).isSelected());
                airlineFilterAdapter.notifyDataSetChanged();
            }
        });
        airportFilterAdapter = new FlightAirportFilterAdapter(FlightReturnFilterActivity.this, airportArray, airportNamesMap);
        airportListView.setAdapter(airportFilterAdapter);
        airportListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                airportArray.get(position).setIsSelected(!airportArray.get(position).isSelected());
                airportFilterAdapter.notifyDataSetChanged();
            }
        });
        stopsFilterAdapter = new FlightStopsFilterAdapter(FlightReturnFilterActivity.this, stopsArray);
        stopsListView.setAdapter(stopsFilterAdapter);
        stopsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                stopsArray.get(position).setIsSelected(!stopsArray.get(position).isSelected());
                stopsFilterAdapter.notifyDataSetChanged();
            }
        });
        timeFilterAdapter = new FlightTimeFilterAdapter(FlightReturnFilterActivity.this, timeArray);
        timeListView.setAdapter(timeFilterAdapter);
        timeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                timeArray.get(position).setIsSelected(!timeArray.get(position).isSelected());
                timeFilterAdapter.notifyDataSetChanged();
            }
        });
        if (!isdetailfilter) {
            updateValues();
        } else {
            updatevalues_detailscreen();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void updateValues() {
        pricedItineraryObjectArrayList.addAll(Singleton.getInstance().returnFlightPIArrayList);
        for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
            Double resultPrice = Double.valueOf(pricedItineraryObjectArrayList.get(i).getAipiBean().getItfObject().getPriceDiff());
            priceArrayList.add(resultPrice);
        }
        Collections.sort(priceArrayList);
        priceMinValue = (Double) priceArrayList.get(0);
        priceMaxValue = (Double) priceArrayList.get(priceArrayList.size() - 1);
        priceRangeBar.setRangeValues(priceMinValue.intValue(), priceMaxValue.intValue());
        String priceResulMin = Utils.convertPriceToUserSelectedPrice(priceMinValue, FlightReturnFilterActivity.this);
        String priceResulMax = Utils.convertPriceToUserSelectedPrice(priceMaxValue, FlightReturnFilterActivity.this);
        if (minSelectedValue != null && maxSelectedValue != null) {
            priceRangeBar.setSelectedMinValue(Double.parseDouble(minSelectedValue));
            priceRangeBar.setSelectedMaxValue(Double.parseDouble(maxSelectedValue));
            priceResulMin = Utils.convertPriceToUserSelectedPrice(Double.parseDouble(minSelectedValue), FlightReturnFilterActivity.this);
            priceResulMax = Utils.convertPriceToUserSelectedPrice(Double.parseDouble(maxSelectedValue), FlightReturnFilterActivity.this);
        }
        if (Utils.isArabicLangSelected(FlightReturnFilterActivity.this)) {
            minPriceValue.setText(priceResulMax);
            maxPriceValue.setText(priceResulMin);
        } else {
            minPriceValue.setText(priceResulMin);
            maxPriceValue.setText(priceResulMax);
        }
        priceRangeBar.setOnRangeSeekBarChangeListener(new com.flyin.bookings.RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(com.flyin.bookings.RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                String priceResulMin = Utils.convertPriceToUserSelectedPrice((double) minValue, FlightReturnFilterActivity.this);
                String priceResulMax = Utils.convertPriceToUserSelectedPrice((double) maxValue, FlightReturnFilterActivity.this);
                if (Utils.isArabicLangSelected(FlightReturnFilterActivity.this)) {
                    minPriceValue.setText(priceResulMax);
                    maxPriceValue.setText(priceResulMin);
                } else {
                    minPriceValue.setText(priceResulMin);
                    maxPriceValue.setText(priceResulMax);
                }
            }
        });
        if (Singleton.getInstance().refundStatusArrayList.size() == 0) {
            rsArray.clear();
            for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    String refNum = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getRefNum();
                    String rsStatus = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getRs();
                    if (refNum.equalsIgnoreCase("2")) {
                        if (!rsArray.contains(rsStatus)) {
                            rsArray.add(rsStatus);
                        }
                    }
                }
            }
            refundStatusArray.clear();
            if (rsArray.size() > 1) {
                refundStatusLayout.setVisibility(View.VISIBLE);
            } else {
                refundStatusLayout.setVisibility(View.GONE);
            }
            FilterRefundSelectionItem refundSelectionItem = new FilterRefundSelectionItem();
            ArrayList<String> duplicateArr = new ArrayList<>();
            Utils.printMessage(TAG, "rsArray.size() :: " + rsArray.size());
            for (int i = 0; i < rsArray.size(); i++) {
                String rsStatusVar = rsArray.get(i);
                if (rsStatusVar.equalsIgnoreCase("Refundable") || rsStatusVar.equalsIgnoreCase("Refundable Before Departure")) {
                    if (!duplicateArr.contains("Refundable")) {
                        refundSelectionItem = new FilterRefundSelectionItem();
                        refundSelectionItem.setRefundMessage(getString(R.string.RefLbl));
                        refundSelectionItem.setRefundType(0);
                        refundSelectionItem.setIsSelected(false);
                        refundStatusArray.add(refundSelectionItem);
                        duplicateArr.add("Refundable");
                    }
                } else {
                    if (!duplicateArr.contains("Non Refundable")) {
                        refundSelectionItem = new FilterRefundSelectionItem();
                        refundSelectionItem.setRefundMessage(getString(R.string.NonRefLbl));
                        refundSelectionItem.setRefundType(1);
                        refundSelectionItem.setIsSelected(false);
                        refundStatusArray.add(refundSelectionItem);
                        duplicateArr.add("Non Refundable");
                    }
                }
            }
            Collections.sort(refundStatusArray, new Comparator<FilterRefundSelectionItem>() {
                @Override
                public int compare(FilterRefundSelectionItem e1, FilterRefundSelectionItem e2) {
                    Integer a = e1.getRefundType();
                    Integer b = e2.getRefundType();
                    return a.compareTo(b);
                }
            });
        } else {
            refundStatusArray.addAll(Singleton.getInstance().refundStatusArrayList);
        }
        refundStatusAdapter.notifyDataSetChanged();
        if (Singleton.getInstance().returnResultAirlinesArrayList.size() == 0) {
            for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                        for (int k = 0; k < bean.getFsDataBeanArrayList().size(); k++) {
                            boolean isAvailable = false;
                            for (FilterAirlineSelectionItem item : airLineArray) {
                                if (item.getMal().equalsIgnoreCase(bean.getFsDataBeanArrayList().get(k).getMal())) {
                                    isAvailable = true;
                                    break;
                                }
                            }
                            if (!isAvailable) {
                                if (selectedMal.isEmpty()) {
                                    airLineArray.add(new FilterAirlineSelectionItem(bean.getFsDataBeanArrayList().get(k).getMal(), false));
                                } else {
                                    if (selectedMal.equalsIgnoreCase(bean.getFsDataBeanArrayList().get(k).getMal())) {
                                        airLineArray.add(new FilterAirlineSelectionItem(bean.getFsDataBeanArrayList().get(k).getMal(), true));
                                    } else {
                                        airLineArray.add(new FilterAirlineSelectionItem(bean.getFsDataBeanArrayList().get(k).getMal(), false));
                                    }
                                }
                                airlineArrayList.add(bean.getFsDataBeanArrayList().get(k).getMal());
                            }
                        }
                    }
                }
            }
        } else {
            airLineArray.addAll(Singleton.getInstance().returnResultAirlinesArrayList);
        }
        if (airLineNamesMap.size() == 0) {
            airlinesData.resultData(FlightReturnFilterActivity.this, airlineArrayList, new AirlinesInterface<HashMap<String, String>>() {
                @Override
                public void onSuccess(HashMap<String, String> response) {
                    if (response.size() > 0) {
                        airLineNamesMap.clear();
                        Utils.printMessage(TAG, "airLineNamesMap :: " + response.size());
                        airLineNamesMap.putAll(response);
                        airlineFilterAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailed(String message) {

                }
            });
        }
        airlineFilterAdapter.notifyDataSetChanged();
        if (Singleton.getInstance().resultAirportsArrayList.size() == 0 || (Singleton.getInstance().firstAirportsArrayList.size() == 0 && Singleton.getInstance().secondAirportsArrayList.size() == 0 && Singleton.getInstance().thirdAirportsArrayList.size() == 0)) {
            ArrayList<String> sourceArray = new ArrayList<>();
            ArrayList<String> destinArray = new ArrayList<>();
            ArrayList<String> stopOverArray = new ArrayList<>();
            for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                        for (int k = 0; k < bean.getFsDataBeanArrayList().size(); k++) {
                            FSDataBean fsValue = bean.getFsDataBeanArrayList().get(k);
                            if (k == 0) {
                                if (!sourceArray.contains(fsValue.getDap())) {
                                    sourceArray.add(fsValue.getDap());
                                }
                            }
                            if (k == bean.getFsDataBeanArrayList().size() - 1) {
                                if (!destinArray.contains(fsValue.getAap())) {
                                    destinArray.add(fsValue.getAap());
                                }
                            }
                            if (k != 0 && bean.getFsDataBeanArrayList().size() != 1 && k != 2) {
                                if (!stopOverArray.contains(fsValue.getDap())) {
                                    stopOverArray.add(fsValue.getDap());
                                }
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < sourceArray.size(); i++) {
                FilterAirportSelectionItem airportSelectionItem = new FilterAirportSelectionItem();
                airportSelectionItem.setIsHeader(false);
                airportSelectionItem.setAirportType(1);
                airportSelectionItem.setAirportCode(sourceArray.get(i));
                airportSelectionItem.setAirportName("");
                airportSelectionItem.setIsSelected(false);
                tempArray1.add(airportSelectionItem);
            }
            for (int i = 0; i < destinArray.size(); i++) {
                FilterAirportSelectionItem airportSelectionItem = new FilterAirportSelectionItem();
                airportSelectionItem.setIsHeader(false);
                airportSelectionItem.setAirportType(2);
                airportSelectionItem.setAirportCode(destinArray.get(i));
                airportSelectionItem.setAirportName("");
                airportSelectionItem.setIsSelected(false);
                tempArray2.add(airportSelectionItem);
            }
            for (int i = 0; i < stopOverArray.size(); i++) {
                FilterAirportSelectionItem airportSelectionItem = new FilterAirportSelectionItem();
                airportSelectionItem.setIsHeader(false);
                airportSelectionItem.setAirportType(3);
                airportSelectionItem.setAirportCode(stopOverArray.get(i));
                airportSelectionItem.setAirportName("");
                airportSelectionItem.setIsSelected(false);
                tempArray3.add(airportSelectionItem);
            }
            airportArray.clear();
            FilterAirportSelectionItem airportSelectionItem = new FilterAirportSelectionItem();
            airportSelectionItem.setIsHeader(true);
            airportSelectionItem.setAirportType(1);
            airportSelectionItem.setAirportName("");
            airportSelectionItem.setIsSelected(false);
            airportSelectionItem.setHeaderText(getString(R.string.label_filter_airport_source));
            airportArray.add(airportSelectionItem);
            airportArray.addAll(tempArray1);
            airportSelectionItem = new FilterAirportSelectionItem();
            airportSelectionItem.setAirportType(2);
            airportSelectionItem.setIsHeader(true);
            airportSelectionItem.setHeaderText(getString(R.string.label_filter_airport_destination));
            airportSelectionItem.setAirportName("");
            airportArray.add(airportSelectionItem);
            airportArray.addAll(tempArray2);
            if (tempArray3.size() > 0) {
                airportSelectionItem = new FilterAirportSelectionItem();
                airportSelectionItem.setIsHeader(true);
                airportSelectionItem.setAirportType(3);
                airportSelectionItem.setHeaderText(getString(R.string.label_filter_airport_stopover));
                airportArray.add(airportSelectionItem);
                airportArray.addAll(tempArray3);
            }
        } else {
            airportArray.addAll(Singleton.getInstance().resultAirportsArrayList);
            tempArray1.addAll(Singleton.getInstance().firstAirportsArrayList);
            tempArray2.addAll(Singleton.getInstance().secondAirportsArrayList);
            tempArray3.addAll(Singleton.getInstance().thirdAirportsArrayList);
        }
        if (airportNamesMap.size() == 0) {
            try {
                if (airportArray.size() != 0) {
                    for (int i = 0; i < airportArray.size(); i++) {
                        if (airportArray.get(i).getAirportCode() != null)
                            airportArrayList.add(airportArray.get(i).getAirportCode());
                    }
                }
            } catch (Exception e) {
            }
            airportData.resultData(FlightReturnFilterActivity.this, airportArrayList, new AirlinesInterface<HashMap<String, FlightAirportName>>() {
                @Override
                public void onSuccess(HashMap<String, FlightAirportName> response) {
                    if (response.size() > 0) {
                        airportNamesMap.clear();
                        Utils.printMessage(TAG, "airportNamesMap :: " + response.size());
                        airportNamesMap.putAll(response);
                        airportFilterAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailed(String message) {
                }
            });
        }
        airportFilterAdapter.notifyDataSetChanged();
        if (Singleton.getInstance().resultStopsArrayList.size() == 0) {
            ArrayList<String> stopsTempArray = new ArrayList<>();
            for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                        for (int k = 0; k < bean.getFsDataBeanArrayList().size(); k++) {
                            if ((bean.getFsDataBeanArrayList().get(0).getSq()) != null) {
                                if (!stopsTempArray.contains(bean.getFsDataBeanArrayList().get(0).getSq())) {
                                    stopsTempArray.add(bean.getFsDataBeanArrayList().get(0).getSq());
                                }
                            }
                        }
                    }
                }
            }
            stopsArray.clear();
            FilterStopSelectionItem stopSelectionItem = new FilterStopSelectionItem();
            for (int i = 0; i < stopsTempArray.size(); i++) {
                String stopVar = stopsTempArray.get(i);
                if (!stopVar.equalsIgnoreCase("") || !stopVar.isEmpty()) {
                    if (stopVar.equalsIgnoreCase("0")) {
                        stopSelectionItem = new FilterStopSelectionItem();
                        stopSelectionItem.setStopMessage(getString(R.string.label_non_stop_text));
                        stopSelectionItem.setStopType(0);
                        if (tripType.equalsIgnoreCase("1") || tripType.equalsIgnoreCase("2")) {
                            if (isNonStopChecked) {
                                stopSelectionItem.setIsSelected(true);
                            } else {
                                stopSelectionItem.setIsSelected(false);
                            }
                        }
                        stopsArray.add(stopSelectionItem);
                    } else if (stopVar.equalsIgnoreCase("1")) {
                        stopSelectionItem = new FilterStopSelectionItem();
                        stopSelectionItem.setStopMessage("1 " + getString(R.string.label_stop_text));
                        stopSelectionItem.setStopType(1);
                        stopSelectionItem.setIsSelected(false);
                        stopsArray.add(stopSelectionItem);
                    } else {
                        stopSelectionItem = new FilterStopSelectionItem();
                        stopSelectionItem.setStopMessage("2+ " + getString(R.string.label_stops_text));
                        stopSelectionItem.setStopType(2);
                        stopSelectionItem.setIsSelected(false);
                        stopsArray.add(stopSelectionItem);
                    }
                }
            }
            Collections.sort(stopsArray, new Comparator<FilterStopSelectionItem>() {
                @Override
                public int compare(FilterStopSelectionItem e1, FilterStopSelectionItem e2) {
                    Integer a = e1.getStopType();
                    Integer b = e2.getStopType();
                    return a.compareTo(b);
                }
            });
        } else {
            stopsArray.addAll(Singleton.getInstance().resultStopsArrayList);
        }
        stopsFilterAdapter.notifyDataSetChanged();
        if (Singleton.getInstance().resultTimeArrayList.size() == 0 || (Singleton.getInstance().departureTimeArrayList.size() == 0 && Singleton.getInstance().arrivalTimeArrayList.size() == 0)) {
            ArrayList<Integer> departureTimeArr = new ArrayList<>();
            ArrayList<Integer> arrivalTimeArr = new ArrayList<>();
            int departureTimeVal = 0;
            int arrivalTimeVal = 0;
            for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                        for (int k = 0; k < bean.getFsDataBeanArrayList().size(); k++) {
                            departureTimeVal = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                            if (!departureTimeArr.contains(departureTimeVal)) {
                                departureTimeArr.add(departureTimeVal);
                            }
                            arrivalTimeVal = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(bean.getFsDataBeanArrayList().size() - 1).getArdt());
                            if (!arrivalTimeArr.contains(arrivalTimeVal)) {
                                arrivalTimeArr.add(arrivalTimeVal);
                            }
                        }
                    }
                }
            }
            timeArray.clear();
            FilterTimeSelectionItem timeSelectionItem = new FilterTimeSelectionItem();
            timeSelectionItem.setIsHeader(true);
            timeSelectionItem.setIsSelected(false);
            timeSelectionItem.setHeaderText(getString(R.string.label_filter_time_departure));
            timeArray.add(timeSelectionItem);
            for (int i = 0; i < departureTimeArr.size(); i++) {
                Integer dapTimeVar = departureTimeArr.get(i);
                if (dapTimeVar == 1) {
                    timeSelectionItem = new FilterTimeSelectionItem();
                    timeSelectionItem.setDepartureText(getString(R.string.label_filter_time_early_morning));
                    timeSelectionItem.setIsSelected(false);
                    timeSelectionItem.setDepartureTime(getString(R.string.label_filter_time_early_morning_time));
                    timeSelectionItem.setTimeZone(1);
                    timeSelectionItem.setIsHeader(false);
                    dapTimeListArray.add(timeSelectionItem);
                } else if (dapTimeVar == 2) {
                    timeSelectionItem = new FilterTimeSelectionItem();
                    timeSelectionItem.setDepartureText(getString(R.string.label_filter_time_morning));
                    timeSelectionItem.setIsSelected(false);
                    timeSelectionItem.setDepartureTime(getString(R.string.label_filter_time_morning_time));
                    timeSelectionItem.setTimeZone(2);
                    timeSelectionItem.setIsHeader(false);
                    dapTimeListArray.add(timeSelectionItem);
                } else if (dapTimeVar == 3) {
                    timeSelectionItem = new FilterTimeSelectionItem();
                    timeSelectionItem.setDepartureText(getString(R.string.label_filter_time_afternoon));
                    timeSelectionItem.setIsSelected(false);
                    timeSelectionItem.setDepartureTime(getString(R.string.label_filter_time_afternoon_time));
                    timeSelectionItem.setTimeZone(3);
                    timeSelectionItem.setIsHeader(false);
                    dapTimeListArray.add(timeSelectionItem);
                } else {
                    timeSelectionItem = new FilterTimeSelectionItem();
                    timeSelectionItem.setDepartureText(getString(R.string.label_filter_time_evening));
                    timeSelectionItem.setIsSelected(false);
                    timeSelectionItem.setDepartureTime(getString(R.string.label_filter_time_evening_time));
                    timeSelectionItem.setTimeZone(4);
                    timeSelectionItem.setIsHeader(false);
                    dapTimeListArray.add(timeSelectionItem);
                }
            }
            Collections.sort(dapTimeListArray, new Comparator<FilterTimeSelectionItem>() {
                @Override
                public int compare(FilterTimeSelectionItem e1, FilterTimeSelectionItem e2) {
                    Integer a = e1.getTimeZone();
                    Integer b = e2.getTimeZone();
                    return a.compareTo(b);
                }
            });
            timeArray.addAll(dapTimeListArray);
            timeSelectionItem = new FilterTimeSelectionItem();
            timeSelectionItem.setIsHeader(true);
            timeSelectionItem.setIsSelected(false);
            timeSelectionItem.setHeaderText(getString(R.string.label_filter_time_arrival));
            timeArray.add(timeSelectionItem);
            for (int j = 0; j < arrivalTimeArr.size(); j++) {
                Integer arrTimeVar = arrivalTimeArr.get(j);
                if (arrTimeVar == 1) {
                    timeSelectionItem = new FilterTimeSelectionItem();
                    timeSelectionItem.setDepartureText(getString(R.string.label_filter_time_early_morning));
                    timeSelectionItem.setIsSelected(false);
                    timeSelectionItem.setDepartureTime(getString(R.string.label_filter_time_early_morning_time));
                    timeSelectionItem.setTimeZone(1);
                    timeSelectionItem.setIsHeader(false);
                    aapTimeListArray.add(timeSelectionItem);
                } else if (arrTimeVar == 2) {
                    timeSelectionItem = new FilterTimeSelectionItem();
                    timeSelectionItem.setDepartureText(getString(R.string.label_filter_time_morning));
                    timeSelectionItem.setIsSelected(false);
                    timeSelectionItem.setDepartureTime(getString(R.string.label_filter_time_morning_time));
                    timeSelectionItem.setTimeZone(2);
                    timeSelectionItem.setIsHeader(false);
                    aapTimeListArray.add(timeSelectionItem);
                } else if (arrTimeVar == 3) {
                    timeSelectionItem = new FilterTimeSelectionItem();
                    timeSelectionItem.setDepartureText(getString(R.string.label_filter_time_afternoon));
                    timeSelectionItem.setIsSelected(false);
                    timeSelectionItem.setDepartureTime(getString(R.string.label_filter_time_afternoon_time));
                    timeSelectionItem.setTimeZone(3);
                    timeSelectionItem.setIsHeader(false);
                    aapTimeListArray.add(timeSelectionItem);
                } else {
                    timeSelectionItem = new FilterTimeSelectionItem();
                    timeSelectionItem.setDepartureText(getString(R.string.label_filter_time_evening));
                    timeSelectionItem.setIsSelected(false);
                    timeSelectionItem.setDepartureTime(getString(R.string.label_filter_time_evening_time));
                    timeSelectionItem.setTimeZone(4);
                    timeSelectionItem.setIsHeader(false);
                    aapTimeListArray.add(timeSelectionItem);
                }
            }
            Collections.sort(aapTimeListArray, new Comparator<FilterTimeSelectionItem>() {
                @Override
                public int compare(FilterTimeSelectionItem e1, FilterTimeSelectionItem e2) {
                    Integer a = e1.getTimeZone();
                    Integer b = e2.getTimeZone();
                    return a.compareTo(b);
                }
            });
            timeArray.addAll(aapTimeListArray);
        } else {
            timeArray.addAll(Singleton.getInstance().resultTimeArrayList);
            dapTimeListArray.addAll(Singleton.getInstance().departureTimeArrayList);
            aapTimeListArray.addAll(Singleton.getInstance().arrivalTimeArrayList);
        }
        timeFilterAdapter.notifyDataSetChanged();
        mApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = getIntent();
                it.putExtra(Constants.PRICE_MINIMUM_VALUE, String.valueOf(priceRangeBar.getSelectedMinValue()));
                it.putExtra(Constants.PRICE_MAXIMUM_VALUE, String.valueOf(priceRangeBar.getSelectedMaxValue()));
                it.putExtra(Constants.ONE_WAY_NONSTOP_CHECKED, isNonStopChecked);
                it.putExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, multiCityNonStopChecked);
                Singleton.getInstance().refundStatusArrayList = refundStatusArray;
                Singleton.getInstance().resultStopsArrayList = stopsArray;
                Singleton.getInstance().returnResultAirlinesArrayList = airLineArray;
                Singleton.getInstance().resultAirportsArrayList = airportArray;
                Singleton.getInstance().firstAirportsArrayList = tempArray1;
                Singleton.getInstance().secondAirportsArrayList = tempArray2;
                Singleton.getInstance().thirdAirportsArrayList = tempArray3;
                Singleton.getInstance().resultTimeArrayList = timeArray;
                Singleton.getInstance().departureTimeArrayList = dapTimeListArray;
                Singleton.getInstance().arrivalTimeArrayList = aapTimeListArray;
                setResult(RESULT_OK, it);
                finish();
            }
        });
        mResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = getIntent();
                pricedItineraryObjectArrayList.addAll(Singleton.getInstance().returnFlightPIArrayList);
                for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                    Double resultPrice = Double.valueOf(pricedItineraryObjectArrayList.get(i).getAipiBean().getItfObject().getPriceDiff());
                    ;
                    priceArrayList.add(resultPrice);
                }
                Collections.sort(priceArrayList);
                priceMinValue = (Double) priceArrayList.get(0);
                priceMaxValue = (Double) priceArrayList.get(priceArrayList.size() - 1);
                priceRangeBar.setRangeValues(priceMinValue.intValue(), priceMaxValue.intValue());
                if (minSelectedValue != null && maxSelectedValue != null) {
                    priceRangeBar.setSelectedMinValue(priceMinValue.intValue());
                    priceRangeBar.setSelectedMaxValue(priceMaxValue.intValue());
                }
                it.putExtra(Constants.PRICE_MINIMUM_VALUE, String.valueOf(priceRangeBar.getSelectedMinValue()));
                it.putExtra(Constants.PRICE_MAXIMUM_VALUE, String.valueOf(priceRangeBar.getSelectedMaxValue()));
                it.putExtra(Constants.ONE_WAY_NONSTOP_CHECKED, false);
                it.putExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, false);
                Singleton.getInstance().refundStatusArrayList.clear();
                Singleton.getInstance().resultStopsArrayList.clear();
                Singleton.getInstance().returnResultAirlinesArrayList.clear();
                Singleton.getInstance().resultAirportsArrayList.clear();
                Singleton.getInstance().firstAirportsArrayList.clear();
                Singleton.getInstance().secondAirportsArrayList.clear();
                Singleton.getInstance().thirdAirportsArrayList.clear();
                Singleton.getInstance().resultTimeArrayList.clear();
                Singleton.getInstance().departureTimeArrayList.clear();
                Singleton.getInstance().arrivalTimeArrayList.clear();
                setResult(RESULT_CANCELED, it);
                finish();
            }
        });
    }

    private void updatevalues_detailscreen() {
        if (Singleton.getInstance().refundStatusArrayList.size() == 0) {
            for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    String refNum = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getRefNum();
                    String rsStatus = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getRs();
                    if (refNum.equalsIgnoreCase("2")) {
                        if (!rsArray.contains(rsStatus)) {
                            rsArray.add(rsStatus);
                        }
                    }
                }
            }
            refundStatusArray.clear();
            if (rsArray.size() > 1) {
                refundStatusLayout.setVisibility(View.VISIBLE);
            } else {
                refundStatusLayout.setVisibility(View.GONE);
            }
            FilterRefundSelectionItem refundSelectionItem = new FilterRefundSelectionItem();
            for (int i = 0; i < rsArray.size(); i++) {
                String rsStatusVar = rsArray.get(i);
                if (rsStatusVar.equalsIgnoreCase("Refundable") || rsStatusVar.equalsIgnoreCase("Refundable Before Departure")) {
                    refundSelectionItem = new FilterRefundSelectionItem();
                    refundSelectionItem.setRefundMessage(getString(R.string.RefLbl));
                    refundSelectionItem.setRefundType(0);
                    refundSelectionItem.setIsSelected(false);
                    refundStatusArray.add(refundSelectionItem);
                } else {
                    refundSelectionItem = new FilterRefundSelectionItem();
                    refundSelectionItem.setRefundMessage(getString(R.string.NonRefLbl));
                    refundSelectionItem.setRefundType(1);
                    refundSelectionItem.setIsSelected(false);
                    refundStatusArray.add(refundSelectionItem);
                }
            }
            Collections.sort(refundStatusArray, new Comparator<FilterRefundSelectionItem>() {
                @Override
                public int compare(FilterRefundSelectionItem e1, FilterRefundSelectionItem e2) {
                    Integer a = e1.getRefundType();
                    Integer b = e2.getRefundType();
                    return a.compareTo(b);
                }
            });
        } else {
            refundStatusArray.addAll(Singleton.getInstance().refundStatusArrayList);
        }
        refundStatusAdapter.notifyDataSetChanged();
        mApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = getIntent();
                it.putExtra(Constants.Detail_SCREEN_REFUND, getrefundfilteritem());
                setResult(RESULT_OK, it);
                finish();
            }
        });
    }

    private int getrefundfilteritem() {
        if (refundStatusArray.get(0).isSelected()) {
            if (refundStatusArray.get(1).isSelected()) {
                return 3;
            } else {
                return 1;
            }
        } else {
            if (refundStatusArray.get(1).isSelected()) {
                return 2;
            } else {
                return 0;
            }
        }
    }
}
