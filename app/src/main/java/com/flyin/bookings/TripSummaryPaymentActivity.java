package com.flyin.bookings;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.ItinTotalFareObjectBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.PaymentKeyGeneration;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

public class TripSummaryPaymentActivity extends AppCompatActivity implements AsyncTaskListener {
    private String setPaymentURL = "";
    private String tokenId = "";
    private String amount = "";
    private String pnrNumber = "";
    private String currency = "";
    private String product = "";
    private String flyinCode = "";
    private String leadPaxName = "";
    private String userFullName = "";
    private String emailId = "";
    private String mobile = "";
    private String locale = "";
    private String fsc = "";
    private String randomNumber = "";
    private String aiJSONObject = "";
    private String aipiJSONObject = "";
    private String tinfJSONObject = "";
    private String resultData = "";
    private String tdr = "";
    private String hold = "";
    private String usctp = "";
    private String currencyValue = "";
    private WebView webview;
    private static final String TAG = "TripSummaryPaymentActivity";
    private boolean isInternetPresent = false;
    private ImageView errorImage;
    private Button searchButton;
    private LayoutInflater inflater;
    private TextView errorText, errorDescriptionText;
    private Typeface textFace;
    private RelativeLayout loadingViewLayout;
    private View loadingView;
    private String accessToken = "";
    private String loggedUser = "";
    private String iqu = "";
    private String pcc = "";
    private String statusCode = "";
    private String status = "";
    private static final int GET_BFP_RESPONSE = 1;
    private static final int GET_AFP_RESPONSE = 2;
    private boolean isPaymentFinished = false;
    private ProgressBar pBar;
    private HashMap<String, FlightAirportName> airportNamesMap = new HashMap<>();
    private HashMap<String, String> airLineNamesMap = new HashMap<>();
    private String cua;
    private String upi;
    private String pId = "";
    private String code = "";
    private boolean isFareCombinationSelected = false;
    private int infoSource = -1;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tripsummary_payment);
        isInternetPresent = Utils.isConnectingToInternet(TripSummaryPaymentActivity.this);
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(TripSummaryPaymentActivity.this)) {
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        textFace = Typeface.createFromAsset(getAssets(), fontText);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        webview = (WebView) findViewById(R.id.web_view);
        PaymentKeyGeneration paymentKeyGen = new PaymentKeyGeneration();
        tokenId = paymentKeyGen.generateRandomString();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            resultData = bundle.getString(Constants.JSON_RESULT, "");
            randomNumber = bundle.getString(Constants.BOOKING_RANDOM_NUMBER, "");
            aiJSONObject = bundle.getString(Constants.AI_JSON_OBJECT, "");
            aipiJSONObject = bundle.getString(Constants.AIPI_JSON_OBJECT, "");
            tinfJSONObject = bundle.getString(Constants.TINF_JSON_OBJECT, "");
            leadPaxName = bundle.getString(Constants.USER_NAME, "");
            userFullName = bundle.getString(Constants.USER_FULL_NAME, "");
            loggedUser = bundle.getString(Constants.LOGGED_USER, "");
            emailId = bundle.getString(Constants.USER_EMAIL, "");
            mobile = bundle.getString(Constants.USER_MOBILE, "");
            tdr = bundle.getString(Constants.TDR_VALUE, "");
            hold = bundle.getString(Constants.HOLD_VALUE, "");
            usctp = bundle.getString(Constants.USCTP_VALUE, "");
            airportNamesMap = (HashMap<String, FlightAirportName>) bundle.get(Constants.AIRPORT_NAME_HASHMAP);
            airLineNamesMap = (HashMap<String, String>) bundle.get(Constants.AIRLINE_NAME_HASHMAP);
            isFareCombinationSelected = bundle.getBoolean(Constants.IS_FARE_COMBINATION, false);
            infoSource = Integer.parseInt(bundle.getString(Constants.INFO_SOURCE, ""));
        }
        parseJSON(resultData);
        parseInputJSON(aipiJSONObject);
        final SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        currency = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        String userMail = pref.getString(Constants.MEMBER_EMAIL, "");
        if (!userMail.equalsIgnoreCase("")) {
            upi = userMail;
        } else {
            upi = "null";
        }
        product = "F";
        locale = Utils.getUserCountry(TripSummaryPaymentActivity.this);
        fsc = "ma";
        final String requestJsonData = HandleBeforePaymentRequest();
        if (isInternetPresent) {
            getBeforePaymentRequestFromServer(requestJsonData);
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(TripSummaryPaymentActivity.this);
                    if (isInternetPresent) {
                        loadingViewLayout.removeView(errorView);
                        getBeforePaymentRequestFromServer(requestJsonData);
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
        pBar = (ProgressBar) findViewById(R.id.progress);
        pBar.setMax(100);
        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                pBar.setVisibility(View.VISIBLE);
                pBar.setProgress(0);
                super.onPageStarted(view, url, favicon);
            }

            public void onPageFinished(WebView view, final String url) {
                Utils.printMessage(TAG, "URL::" + url);
                pBar.setVisibility(View.GONE);
                pBar.setProgress(100);
                try {
                    String[] outerStr = url.split("\\?");
                    if (outerStr.length == 2) {
                        String[] str = outerStr[1].split("&");
                        String message = "";
                        for (String item : str) {
                            String[] innerStr = item.split("=");
                            if (innerStr.length == 2) {
                                if (innerStr[0].equalsIgnoreCase("status")) {
                                    status = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("message")) {
                                    message = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("statusCode")) {
                                    statusCode = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("iqu")) {
                                    iqu = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("pcc")) {
                                    pcc = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("cua")) {
                                    cua = innerStr[1];
                                }
                                if (innerStr[0].equalsIgnoreCase("code")) {
                                    code = innerStr[1];
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                }
                onThreadExecutionFinished();
            }
        });
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && pBar.getVisibility() == ProgressBar.GONE) {
                    pBar.setVisibility(ProgressBar.VISIBLE);
                }
                pBar.setProgress(progress);
                if (progress == 100) {
                    pBar.setVisibility(ProgressBar.GONE);
                }
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_flight, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.VISIBLE);
        ImageView mImageView = (ImageView) mCustomView.findViewById(R.id.first_home_icon);
        ImageView mHeaderImage = (ImageView) mCustomView.findViewById(R.id.first_home_logo);
        TextView mSignInText = (TextView) mCustomView.findViewById(R.id.sing_in_button);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.home_back_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_home_text);
        mSignInText.setVisibility(View.GONE);
        mHeaderImage.setImageResource(R.drawable.screen_three);
        backText.setTypeface(textFace);
        if (Utils.isArabicLangSelected(TripSummaryPaymentActivity.this)) {
            mImageView.setScaleType(ImageView.ScaleType.FIT_START);
            mHeaderImage.setImageResource(R.drawable.arabic_three);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            backText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            params.setMargins(0, -5, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = getIntent();
                setResult(RESULT_OK, it);
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    private void onThreadExecutionFinished() {
        if (statusCode.isEmpty()) {
            //Log.e(TAG, "url not correct");
        } else if (statusCode.equalsIgnoreCase("200")) {
            if (!isPaymentFinished) {
                isPaymentFinished = true;
                isInternetPresent = Utils.isConnectingToInternet(TripSummaryPaymentActivity.this);
                final String requestJson = HandleAfterPaymentRequest();
                if (isInternetPresent) {
                    handleAFPServiceCall(requestJson);
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(textFace);
                    errorDescriptionText.setTypeface(textFace);
                    searchButton.setTypeface(textFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(TripSummaryPaymentActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                handleAFPServiceCall(requestJson);
                                //getAfterPaymentRequestFromServer(requestJson);
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        } else if (statusCode.equalsIgnoreCase("108")) {
            Utils.printMessage(TAG, "Payment Failure");
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TripSummaryPaymentActivity.this, MainSearchActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            loadErrorType(Constants.PAYMENT_FAILED);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
    }

    private void parseJSON(String data) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(data);
            if (obj.has("bookRS")) {
                if (obj.getJSONObject("bookRS").has("ar")) {
                    JSONObject arObject = obj.getJSONObject("bookRS").getJSONObject("ar");
                    if (arObject.has("fbi")) {
                        flyinCode = arObject.getString("fbi");
                    }
                    if (arObject.has("pnr")) {
                        pnrNumber = arObject.getString("pnr");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseInputJSON(String aipiObject) {
        JSONObject aipi = null;
        try {
            aipi = new JSONObject(aipiObject);
            if (aipi.has("itf")) {
                String itfData = aipi.getString("itf");
                Object json = new JSONTokener(itfData).nextValue();
                if (json instanceof JSONObject) {
                    if (aipi.getJSONObject("itf").has("tFare")) {
                        amount = aipi.getJSONObject("itf").getString("tFare");
                        Double bookingValue = Double.parseDouble(amount);
                        String resultAmount = Utils.convertPriceToUserSelection(bookingValue, TripSummaryPaymentActivity.this);
                        String[] totalAmount = resultAmount.split(" ");
                        String currencyCode = totalAmount[0];
                        currencyValue = Utils.convertPriceToUserSelectionPrice(bookingValue, TripSummaryPaymentActivity.this);
                        Utils.printMessage(TAG, "amount is " + currencyValue);
                    }
                } else if (json instanceof JSONArray) {
                    ArrayList<ItinTotalFareObjectBean> itfArray = new ArrayList<>();
                    for (int j = 0; j < aipi.getJSONArray("itf").length(); j++) {
                        JSONArray itfArr = aipi.getJSONArray("itf");
                        ItinTotalFareObjectBean itfBean = new ItinTotalFareObjectBean();
                        itfBean.setBf(itfArr.getJSONObject(j).getString("bf"));
                        itfBean.settTx(itfArr.getJSONObject(j).getString("tTx"));
                        itfBean.settSF(itfArr.getJSONObject(j).getString("tSF"));
                        itfBean.setDisc(itfArr.getJSONObject(j).getString("disc"));
                        itfBean.settFare(itfArr.getJSONObject(j).getString("tFare"));
                        itfBean.setIc(itfArr.getJSONObject(j).getString("ic"));
                        itfBean.setUsfc(itfArr.getJSONObject(j).getString("usfc"));
                        itfArray.add(itfBean);
                    }
                    Double price = 0.0;
                    if (itfArray.size() > 1) {
                        price = Double.parseDouble(itfArray.get(0).gettFare()) + Double.parseDouble(itfArray.get(itfArray.size() - 1).gettFare());
                    } else {
                        price = Double.parseDouble(itfArray.get(0).gettFare());
                    }
                    currencyValue = Utils.convertPriceToUserSelectionPrice(price, TripSummaryPaymentActivity.this);
                    Utils.printMessage(TAG, "Price Value : " + currencyValue);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String HandleBeforePaymentRequest() {
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("brn", flyinCode);
            mainJSON.put("tid", tokenId);
            mainJSON.put("usrc", currency);
            mainJSON.put("usrba", currencyValue);
            mainJSON.put("rurl", Utils.checkStringValue("null"));
            mainJSON.put("prdct", "F");
            mainJSON.put("stus", Utils.checkStringValue("null"));
            mainJSON.put("stusc", Utils.checkStringValue("null"));
            mainJSON.put("lpax", userFullName);
            mainJSON.put("lpaxemail", emailId);
            mainJSON.put("iqu", Utils.checkStringValue("null"));
            mainJSON.put("dc", Utils.checkStringValue("null"));
            if (!upi.equalsIgnoreCase("null")) {
                mainJSON.put("upi", upi);
            }
            mainJSON.put("stype", "BFP");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void getBeforePaymentRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(TripSummaryPaymentActivity.this, TripSummaryPaymentActivity.this, Constants.SECURE_PAYMENT_URL, "", requestJSON,
                GET_BFP_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String HandleAfterPaymentRequest() {
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("brn", flyinCode);
            mainJSON.put("tid", tokenId);
            mainJSON.put("usrc", currency);
            mainJSON.put("usrba", currencyValue);
            mainJSON.put("rurl", Utils.checkStringValue("null"));
            mainJSON.put("prdct", "F");
            mainJSON.put("stus", status);
            mainJSON.put("stusc", statusCode);
            mainJSON.put("lpax", userFullName);
            mainJSON.put("lpaxemail", emailId);
            mainJSON.put("iqu", iqu);
            mainJSON.put("dc", pcc);
            mainJSON.put("stype", "AFP");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void getAfterPaymentRequestFromServer(String requestJSON) {
        HTTPAsync async = new HTTPAsync(TripSummaryPaymentActivity.this, TripSummaryPaymentActivity.this, Constants.SECURE_PAYMENT_URL, "", requestJSON,
                GET_AFP_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String handleBookingRQRequest() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(TripSummaryPaymentActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(TripSummaryPaymentActivity.this));
            mainJSON.put("source", sourceJSON);
            String registeredUser = "";
            if (!pref.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
                registeredUser = pref.getString(Constants.MEMBER_EMAIL, "");
                mainJSON.put("userUniqueId", pref.getString(Constants.USER_UNIQUE_ID, ""));
            }
//            else {
//                registeredUser = emailId;
//                mainJSON.put("customerId", emailId);
//            }
//            mainJSON.put("customerId", registeredUser);
//            mainJSON.put("fromDate", "");
//            mainJSON.put("toDate", "");
            mainJSON.put("referenceNo", flyinCode);
            mainJSON.put("searchType", "by_reference");
            mainJSON.put("referenceType", "Flight");
            mainJSON.put("recordType", "Only Flight");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "DATA::" + data);
        closeLoading();
        JSONObject obj = null;
        try {
            if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
                inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = inflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(textFace);
                errorDescriptionText.setTypeface(textFace);
                searchButton.setTypeface(textFace);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(TripSummaryPaymentActivity.this, MainSearchActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            } else {
                obj = new JSONObject(data);
                if (obj.has("accessToken")) {
                    accessToken = obj.getString("accessToken");
                    if (!accessToken.equalsIgnoreCase("null") && serviceType == GET_BFP_RESPONSE) {
                        String status = obj.getString("status");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            String leadPaxName = URLEncoder.encode(userFullName, "utf-8");
                            String leadPaxEmail = URLEncoder.encode(emailId, "utf-8");
                            locale = "en_GB";
                            if (Utils.isArabicLangSelected(TripSummaryPaymentActivity.this)) {
                                locale = "ar_SA";
                            }
                            if (upi.equalsIgnoreCase("null")) {
                                setPaymentURL = Constants.PAYMENT_URL + "tokenId=" + tokenId + "&amount=" + currencyValue +
                                        "&currency=" + currency + "&product=" + product + "&flyinCode=" + flyinCode +
                                        "&leadPaxName=" + leadPaxName + "&emailId=" + leadPaxEmail + "&locale=" + locale +
                                        "&fsc=" + fsc + "&pcc=" + accessToken + "&version=1";
                            } else {
                                String upis = URLEncoder.encode(upi, "utf-8");
                                SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                                String uniqueId = pref.getString(Constants.USER_UNIQUE_ID, "");
                                setPaymentURL = Constants.PAYMENT_URL + "tokenId=" + tokenId + "&amount=" + currencyValue +
                                        "&currency=" + currency + "&product=" + product + "&flyinCode=" + flyinCode +
                                        "&leadPaxName=" + leadPaxName + "&emailId=" + leadPaxEmail + "&locale=" + locale +
                                        "&fsc=" + fsc + "&pcc=" + accessToken + "&upi=" + upis + "&version=1" + "&uid=" + uniqueId;
                            }
                            webview.loadUrl(setPaymentURL);
                            Utils.printMessage(TAG, "Payment URL::" + setPaymentURL);
                        } else {
                            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View errorView = inflater.inflate(R.layout.view_error_response, null);
                            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                            errorText = (TextView) errorView.findViewById(R.id.error_text);
                            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                            searchButton = (Button) errorView.findViewById(R.id.search_button);
                            errorText.setTypeface(textFace);
                            errorDescriptionText.setTypeface(textFace);
                            searchButton.setTypeface(textFace);
                            searchButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    isInternetPresent = Utils.isConnectingToInternet(TripSummaryPaymentActivity.this);
                                    if (isInternetPresent) {
                                        loadingViewLayout.removeView(errorView);
                                        webview.loadUrl(setPaymentURL);
                                    }
                                }
                            });
                            loadErrorType(Constants.WRONG_ERROR_PAGE);
                            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            loadingViewLayout.addView(errorView);
                        }
                    }
                    if (serviceType == GET_AFP_RESPONSE) {
                        String afpStatus = obj.getString("status");
                        if (afpStatus.equalsIgnoreCase("SUCCESS")) {
                            String bookingIdRqJson = handleBookingRQRequest();
                            Utils.printMessage(TAG, "handleTicketRQRequest :: " + bookingIdRqJson);
                            Intent intent = new Intent(TripSummaryPaymentActivity.this, TripConfirmationActivity.class);
                            intent.putExtra(Constants.JSON_REQUEST, bookingIdRqJson);
                            intent.putExtra(Constants.USER_FULL_NAME, userFullName);
                            intent.putExtra(Constants.USER_EMAIL, emailId);
                            intent.putExtra(Constants.AIRPORT_NAME_HASHMAP, airportNamesMap);
                            intent.putExtra(Constants.AIRLINE_NAME_HASHMAP, airLineNamesMap);
                            intent.putExtra(Constants.COUPON_AMOUNT, cua);
                            intent.putExtra(Constants.BOOKING_ID, flyinCode);
                            intent.putExtra(Constants.USER_NAME, leadPaxName);
                            intent.putExtra(Constants.INFO_SOURCE, String.valueOf(infoSource));
                            startActivity(intent);
                        } else {
                            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View errorView = inflater.inflate(R.layout.view_error_response, null);
                            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                            errorText = (TextView) errorView.findViewById(R.id.error_text);
                            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                            searchButton = (Button) errorView.findViewById(R.id.search_button);
                            errorText.setTypeface(textFace);
                            errorDescriptionText.setTypeface(textFace);
                            searchButton.setTypeface(textFace);
                            searchButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(TripSummaryPaymentActivity.this, MainSearchActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });
                            loadErrorType(Constants.WRONG_ERROR_PAGE);
                            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            loadingViewLayout.addView(errorView);
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_payment_loading_text);
        loadingText.setTypeface(textFace);
        descriptionText.setVisibility(View.GONE);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent it = getIntent();
        setResult(RESULT_OK, it);
        finish();
    }

    private void handleAFPServiceCall(String requestJson) {
        if (code.equalsIgnoreCase("502")) {
            getAfterPaymentRequestFromServer(requestJson);
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TripSummaryPaymentActivity.this, MainSearchActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
    }
}
