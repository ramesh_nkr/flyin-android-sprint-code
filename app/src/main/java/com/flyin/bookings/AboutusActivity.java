package com.flyin.bookings;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

@SuppressWarnings("deprecation")
public class AboutusActivity extends AppCompatActivity {
    private WebView webView;
    private String viewInformation = "";
    private String actionBarTitle = "";
    private String gettingDataFrom = "";
    private boolean isFirstTimeLooping = false;
    public static final String TAG = "AboutusActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            viewInformation = bundle.getString(Constants.HTML_WEB_VIEW, "");
            actionBarTitle = bundle.getString(Constants.ACTIONBAR_HEADER, "");
            gettingDataFrom = bundle.getString("Calling_From", "");
        }
        Utils.printMessage(TAG, "WebView Url :: " + viewInformation);
        webView = (WebView) findViewById(R.id.about_flyin_webView);
        if (gettingDataFrom.equalsIgnoreCase("Dialog")) {
            if (!Utils.isConnectingToInternet(AboutusActivity.this)) {
                finish();
            }
        }
        startWebView(viewInformation);
        if (Utils.isArabicLangSelected(AboutusActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            startWebView(viewInformation);
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontTitle);
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(actionBarTitle);
        mTitleText.setTypeface(tf);
        backText.setTypeface(textFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(AboutusActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void startWebView(String url) {
        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onLoadResource(WebView view, String url) {
                if (!isFirstTimeLooping) {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(AboutusActivity.this);
                        progressDialog.setMessage(getResources().getString(R.string.LoadingLabel));
                        progressDialog.show();
                    }
                }
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    if (progressDialog != null) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                            progressDialog = null;
                            isFirstTimeLooping = true;
                        }
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
}
