package com.flyin.bookings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appsee.Appsee;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SplashScreenActivity extends AppCompatActivity implements AsyncTaskListener {
    private boolean isNew = true;
    private static final int GET_ACCESS_TOKEN = 1;
    private static final int GET_REFRESH_TOKEN = 2;
    private static final String TAG = "SplashScreenActivity";
    private Typeface textFace;
    private RelativeLayout loadingViewLayout = null;
    private ImageView errorImage = null;
    private Button searchButton = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private ProgressBar spinner;
    private boolean isAppReLaunched = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(SplashScreenActivity.this)) {
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
        }
        textFace = Typeface.createFromAsset(getAssets(), fontRegular);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        spinner = (ProgressBar) findViewById(R.id.progressBar);
        Appsee.start(Constants.APPSEE_ID);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey("isNew")) {
                isNew = b.getBoolean("isNew");
            }
        }
        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(3 * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String currentDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(new Date());
                            String currentTimeFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH).format(new Date());
                            String timeStamp = currentDateFormat + " " + currentTimeFormat;
                            Utils.printMessage(TAG, "Time is : "+Utils.getConvertedTimeFormat(SplashScreenActivity.this, timeStamp));
                            boolean isInternetPresent = Utils.isConnectingToInternet(SplashScreenActivity.this);
                            if (isInternetPresent) {
                                handleAccessTokenService();
                            } else {
                                handleInternetConnErr();
                            }
                        }
                    });
                }
            }
        };
        timerThread.start();
    }

    private void handleAccessTokenService() {
        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        if (!preferences.getString(Constants.ACCESS_TOKEN, "").isEmpty()) {
            if (preferences.getBoolean(Constants.IS_REFRESH_TOKEN_FAILED, false)) {
                getTokenFromServer();
            } else {
                handleRefreshToken();
            }
        } else {
            getTokenFromServer();
        }
    }

    private void getTokenFromServer() {
        JSONObject obj = new JSONObject();
        try {
            SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(SplashScreenActivity.this));
            if (preferences.getBoolean(Constants.IS_REFRESH_TOKEN_FAILED, false)) {
                obj.put("resetAll", "Yes");
                preferences.edit().putBoolean(Constants.IS_REFRESH_TOKEN_FAILED, false).apply();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(SplashScreenActivity.this, SplashScreenActivity.this,
                Constants.CLIENT_AUTHENTICATION, "", obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void handleRefreshToken() {
        JSONObject obj = new JSONObject();
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.REFRESH_GRANT_TYPE);
            obj.put("refreshToken", pref.getString(Constants.REFRESH_TOKEN, ""));
            if (!pref.getString(Constants.MEMBER_ACCESS_TOKEN, "").equalsIgnoreCase("")) {
                obj.put("mtoken", pref.getString(Constants.MEMBER_ACCESS_TOKEN, ""));
            }
        } catch (Exception e) {
        }
        HTTPAsync async = new HTTPAsync(SplashScreenActivity.this, SplashScreenActivity.this,
                Constants.CLIENT_REFRESH_TOKEN, "", obj.toString(), GET_REFRESH_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, " === DATA RESPONSE ==== " + data);
        if (isAppReLaunched) {
            if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("") && serviceType == GET_ACCESS_TOKEN) {
                handleSomeThingWrongErr();
            } else if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("") && serviceType == GET_REFRESH_TOKEN) {
                handleRefreshErrorService();
            } else {
                SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                if (serviceType == GET_ACCESS_TOKEN) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(data);
                        if (obj.has("accessToken")) {
                            String accessToken = Utils.checkStringVal(obj.getString("accessToken"));
                            if (!accessToken.equalsIgnoreCase("null") && !accessToken.equalsIgnoreCase("")) {
                                String expireIn = obj.getString("expireIn");
                                String refreshToken = obj.getString("refreshToken");
                                String timeZone = "";
                                if (obj.has("tzone")) {
                                    timeZone = obj.getString("tzone");
                                }
                                long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString(Constants.ACCESS_TOKEN, accessToken);
                                editor.putString(Constants.EXPIRE_IN, expireIn);
                                editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                                editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                                editor.putString(Constants.SERVER_TIME_MILLI, timeZone);
                                editor.apply();
                                if (!timeZone.isEmpty()) {
                                    Long localMilliSeconds = System.currentTimeMillis();
                                    Long resultMilli = Long.parseLong(timeZone) - localMilliSeconds;
                                    int diff = resultMilli.intValue();
                                    Utils.printMessage(TAG, "Diff : "+diff);
                                    editor.putInt(Constants.SERVER_TIME_DIFF, diff).apply();
                                }
                                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                                intent.putExtra("isNew", isNew);
                                startActivity(intent);
                                finish();
                            } else {
                                handleSomeThingWrongErr();
                            }
                        } else {
                            handleSomeThingWrongErr();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (serviceType == GET_REFRESH_TOKEN) {
                    JSONObject obj = null;
                    try {
                        preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(Constants.ACCESS_TOKEN, "").apply();
                        editor.putString(Constants.EXPIRE_IN, "").apply();
                        editor.putString(Constants.REFRESH_TOKEN, "").apply();
                        editor.remove(Constants.TOKEN_GENERATED_TIME).apply();
                        editor.putString(Constants.SERVER_TIME_MILLI, "").apply();
                        editor.putString(Constants.MEMBER_ACCESS_TOKEN, "").apply();
                        editor.putInt(Constants.SERVER_TIME_DIFF, 0).apply();
                        obj = new JSONObject(data);
                        if (obj.has("accessToken")) {
                            String accessToken = Utils.checkStringVal(obj.getString("accessToken"));
                            if (!accessToken.equalsIgnoreCase("null") && !accessToken.equalsIgnoreCase("")) {
                                if (obj.has("status")) {
                                    String statusVal = obj.getString("status");
                                    if (statusVal.equalsIgnoreCase("FAILURE")) {
                                        if (obj.getString("errorCode").equalsIgnoreCase("1024")) {
                                            handleRefreshErrorService();
                                        }
                                    }
                                } else {
                                    String expireIn = obj.getString("expireIn");
                                    String refreshToken = obj.getString("refreshToken");
                                    String timeZone = "";
                                    if (obj.has("tzone")) {
                                        timeZone = obj.getString("tzone");
                                    }
                                    String memberToken = "";
                                    if (obj.has("otoken")) {
                                        memberToken = obj.getString("otoken");
                                    }
                                    long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                                    editor.putString(Constants.ACCESS_TOKEN, accessToken).apply();
                                    editor.putString(Constants.EXPIRE_IN, expireIn).apply();
                                    editor.putString(Constants.REFRESH_TOKEN, refreshToken).apply();
                                    editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs).apply();
                                    editor.putString(Constants.SERVER_TIME_MILLI, timeZone).apply();
                                    editor.putString(Constants.MEMBER_ACCESS_TOKEN, memberToken).apply();
                                    if (!timeZone.isEmpty()) {
                                        Long localMilliSeconds = System.currentTimeMillis();
                                        Long resultMilli = Long.parseLong(timeZone) - localMilliSeconds;
                                        int diff = resultMilli.intValue();
                                        Utils.printMessage(TAG, "Diff : "+diff);
                                        editor.putInt(Constants.SERVER_TIME_DIFF, diff).apply();
                                    }
                                    Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                                    intent.putExtra("isNew", isNew);
                                    startActivity(intent);
                                    finish();
                                }
                            } else {
                                handleSomeThingWrongErr();
                            }
                        } else {
                            handleSomeThingWrongErr();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {

    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private void handleInternetConnErr() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View errorView = inflater.inflate(R.layout.view_error_response, null);
        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
        errorText = (TextView) errorView.findViewById(R.id.error_text);
        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
        searchButton = (Button) errorView.findViewById(R.id.search_button);
        errorText.setTypeface(textFace);
        errorDescriptionText.setTypeface(textFace);
        searchButton.setTypeface(textFace);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isInternetPresent = Utils.isConnectingToInternet(SplashScreenActivity.this);
                if (isInternetPresent) {
                    loadingViewLayout.removeView(errorView);
                    handleAccessTokenService();
                }
            }
        });
        loadErrorType(Constants.NETWORK_ERROR);
        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(errorView);
    }

    private void handleSomeThingWrongErr() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View errorView = inflater.inflate(R.layout.view_error_response, null);
        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
        errorText = (TextView) errorView.findViewById(R.id.error_text);
        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
        searchButton = (Button) errorView.findViewById(R.id.search_button);
        errorText.setTypeface(textFace);
        errorDescriptionText.setTypeface(textFace);
        searchButton.setTypeface(textFace);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleAccessTokenService();
            }
        });
        loadErrorType(Constants.WRONG_ERROR_PAGE);
        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(errorView);
    }

    private void handleRefreshErrorService() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        pref.edit().putString(Constants.MEMBER_EMAIL, "").apply();
        pref.edit().putString(Constants.MEMBER_ACCESS_TOKEN, "").apply();
        pref.edit().putString(Constants.MEMBER_TOKEN_TYPE, "").apply();
        pref.edit().putString(Constants.USER_UNIQUE_ID, "").apply();
        pref.edit().putBoolean(Constants.isFanClub, false).apply();
        pref.edit().putBoolean(Constants.IS_REWARDS_ENABLED, false).apply();
        pref.edit().putBoolean(Constants.IS_REFRESH_TOKEN_FAILED, true).apply();
        getTokenFromServer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Utils.isAppIsInBackground(SplashScreenActivity.this)) {
            isAppReLaunched = false;
        }
    }
}
