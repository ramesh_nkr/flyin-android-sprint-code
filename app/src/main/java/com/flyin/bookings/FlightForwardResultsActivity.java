package com.flyin.bookings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.adapter.FlightOneWayTripAdapter;
import com.flyin.bookings.adapter.HorizontalScrollRecyclerViewAdapter;
import com.flyin.bookings.listeners.AirlinesInterface;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.listeners.TokenUpdateListener;
import com.flyin.bookings.model.AirItineraryObjectBean;
import com.flyin.bookings.model.AirItineraryPricingInfoBean;
import com.flyin.bookings.model.AirportSectorBean;
import com.flyin.bookings.model.AmenitiesBean;
import com.flyin.bookings.model.FSDataBean;
import com.flyin.bookings.model.FilterAirlineSelectionItem;
import com.flyin.bookings.model.FilterAirportSelectionItem;
import com.flyin.bookings.model.FilterRefundSelectionItem;
import com.flyin.bookings.model.FilterStopSelectionItem;
import com.flyin.bookings.model.FilterTimeSelectionItem;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.FlightAmenitiesBean;
import com.flyin.bookings.model.FlightBean;
import com.flyin.bookings.model.ItinTotalFareObjectBean;
import com.flyin.bookings.model.OriginDestinationOptionBean;
import com.flyin.bookings.model.PFBDObjectBean;
import com.flyin.bookings.model.PassengerFareObjectBean;
import com.flyin.bookings.model.PricedItineraryObject;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.model.TaxObjectBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.AirlinesData;
import com.flyin.bookings.util.AirportData;
import com.flyin.bookings.util.AmenitiesConstants;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;
import com.webengage.sdk.android.Analytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@SuppressWarnings("deprecation")
public class FlightForwardResultsActivity extends AppCompatActivity implements
        AsyncTaskListener, OnCustomItemSelectListener, TokenUpdateListener {
    private Typeface regularFace = null;
    private String tripType = "";
    private String source = "";
    private String destination = "";
    private String secondSource = "";
    private boolean isInternetPresent = false;
    private boolean isArabicLang = false;
    private String travellerCount = "";
    private String selectedLang = "";
    private String requestJSON = "";
    private ArrayList<PricedItineraryObject> mainPricedItineraryArray = null;
    private ArrayList<PricedItineraryObject> tempPricedItineraryArray = null;
    private ArrayList<OriginDestinationOptionBean> tempForwardPricedItineraryArray = null;
    private ArrayList<OriginDestinationOptionBean> tempReturnPricedItineraryArray = null;
    private ArrayList<FilterStopSelectionItem> stopsArray = null;
    private ArrayList<FilterAirlineSelectionItem> airlineArray = null;
    private ArrayList<FilterAirportSelectionItem> airportListArray = null;
    private ArrayList<FilterAirportSelectionItem> tempArray1 = null;
    private ArrayList<FilterAirportSelectionItem> tempArray2 = null;
    private ArrayList<FilterAirportSelectionItem> tempArray3 = null;
    private ArrayList<FilterTimeSelectionItem> timeArray = null;
    private ArrayList<FilterTimeSelectionItem> dapTimeListArray = null;
    private ArrayList<FilterTimeSelectionItem> aapTimeListArray = null;
    private ArrayList<FilterRefundSelectionItem> refundStatusArray = null;
    private ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList = null;
    private ArrayList<PricedItineraryObject> tempAirlinePIArrayList = null;
    private ArrayList<FlightBean> flightsArrayList = new ArrayList();
    private ArrayList<PricedItineraryObject> mainFCPricedItineraryArray = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private TextView totalFlightCountTextView = null;
    private TextView flightCountEarlierTextView = null;
    private TextView flightCountFastestTextView = null;
    private LayoutInflater inflater = null;
    private ImageView errorImage = null;
    private Button searchButton = null;
    private View loadingView = null;
    private RelativeLayout headerViewLayout = null;
    private static final int GET_SEARCH_RESULTS = 1;
    private static final int GET_FARE_RULE_RESULTS = 2;
    private static final int FLIGHT_PRICE_FILTER = 3;
    private static final int GET_FARE_COMBINATION_RESULTS = 9;
    private ArrayList<String> airportArrayList = new ArrayList<>();
    private ArrayList<String> airlineArrayList = new ArrayList<>();
    private HashMap<String, FlightAirportName> airportNamesMap = new HashMap<>();
    private HashMap<String, String> airLineNamesMap = new HashMap<>();
    private AirportData airportData = new AirportData();
    private AirlinesData airlinesData = new AirlinesData();
    private boolean isTextUpdated = false;
    private static final String TAG = "FlightForwardResultsActivity";
    private boolean fastestFlights = false;
    private boolean earliestFlights = false;
    private boolean totalFlights = false;
    private String secondDestination = "";
    private String thirdSource = "";
    private String thirdDestination = "";
    private String journeyDetails = "";
    private String forwardFlightDate = "";
    private String returnFlightDate = "";
    private String sourceName = "";
    private String returnName = "";
    private String secondsourceName = "";
    private String secondreturnName = "";
    private String thirdsourceName = "";
    private String thirdreturnName = "";
    private boolean isNonStopChecked = false;
    private boolean multiCityNonStopChecked = false;
    private String minValue, maxValue;
    private String selectedClassType = "";
    private String selectedClass = "";
    private ArrayList<AirportSectorBean> flightNamesArray = null;
    private static final int GET_MERCHANDISE_DATA = 5;
    private static final int GET_MERCHANDISE_AIRPORT_DATA = 4;
    private static final int SEARCH_FLIGHT_RESULT = 6;
    private static final int RETURN_FLIGHT_RESULT = 7;
    private HashMap<String, String> routesMap = new HashMap<>();
    private ArrayList<OriginDestinationOptionBean> showOdoArrayList = null;
    private ArrayList<FlightAmenitiesBean> mainMarchArrayList = null;
    private HorizontalScrollRecyclerViewAdapter mAdapter;
    private View noResultView = null;
    private RelativeLayout flightListLayout = null;
    private int earliestCount = 0;
    private int fastestCount = 0;
    private int totalCount = 0;
    private LinearLayout totalCountLayout = null;
    private LinearLayout earliestCountLayout = null;
    private LinearLayout fastestCountLayout = null;
    private String aiJSONObject = "";
    private String aipiJSONObject = "";
    private TextView errorMessageText = null;
    private RelativeLayout errorView = null;
    private boolean isFilterApplied = false;
    private FlightOneWayTripAdapter adapterList;
    private String randomNumber = "";
    private static final int GET_ACCESS_TOKEN = 8;
    private String selectedMal = "";
    private int loopCount = 0;
    public static boolean isSecondServiceCalled = false;
    public static boolean isReturnFlightClicked = false;
    private boolean isAirlineDataParsed = false;
    private boolean isFcDataParsing = false;
    private Analytics analytics;
    private RefreshTokenService refreshTokenData = new RefreshTokenService();
    private boolean isAppReLaunched = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_results_page);
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.hide();
        }
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        //weUser.loggedIn("Raghavendra");
//        analytics = WebEngage.get().analytics();
//        analytics.track("FlightForwardResultsActivity");
        isInternetPresent = Utils.isConnectingToInternet(FlightForwardResultsActivity.this);
        mainPricedItineraryArray = new ArrayList<>();
        tempPricedItineraryArray = new ArrayList<>();
        tempForwardPricedItineraryArray = new ArrayList<>();
        tempReturnPricedItineraryArray = new ArrayList<>();
        showOdoArrayList = new ArrayList<>();
        flightNamesArray = new ArrayList<>();
        mainMarchArrayList = new ArrayList<>();
        pricedItineraryObjectArrayList = new ArrayList<>();
        tempAirlinePIArrayList = new ArrayList<>();
        airlineArray = new ArrayList<>();
        mainFCPricedItineraryArray = new ArrayList<>();
        airportListArray = new ArrayList<>();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontNormal = Constants.FONT_ROBOTO_MEDIUM;
        isArabicLang = false;
        if (Utils.isArabicLangSelected(FlightForwardResultsActivity.this)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontNormal = Constants.FONT_DROIDKUFI_REGULAR;
            isArabicLang = true;
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Constants.JSON_REQUEST)) {
                requestJSON = bundle.getString(Constants.JSON_REQUEST, "");
            }
            if (bundle.containsKey(Constants.TRIP_TYPE)) {
                tripType = bundle.getString(Constants.TRIP_TYPE, "");
            }
            selectedClassType = bundle.getString(Constants.SELECTED_CLASS_TYPE, "");
            source = bundle.getString(Constants.FORWARD_SOURCE, "");
            destination = bundle.getString(Constants.FORWARD_DESTINATION, "");
            secondSource = bundle.getString(Constants.SECOND_FORWARD_SOURCE, "");
            secondDestination = bundle.getString(Constants.SECOND_FORWARD_DESTINATION, "");
            journeyDetails = bundle.getString(Constants.DATE_ONEWAY, "");
            travellerCount = bundle.getString(Constants.TOTAL_PASSENGERS_COUNT, "1");
            sourceName = bundle.getString(Constants.FORWARD_SOURCE_NAME, "");
            returnName = bundle.getString(Constants.RETURN_SOURCE_NAME, "");
            thirdSource = bundle.getString(Constants.THIRD_FORWARD_SOURCE, "");
            thirdDestination = bundle.getString(Constants.THIRD_FORWARD_DESTINATION, "");
            forwardFlightDate = bundle.getString(Constants.FORWARD_FLIGHT_DATE, "");
            returnFlightDate = bundle.getString(Constants.RETURN_FLIGHT_DATE, "");
            secondsourceName = bundle.getString(Constants.SECOND_FORWARD_SOURCE_NAME, "");
            secondreturnName = bundle.getString(Constants.SECOND_FORWARD_DESTINATION_NAME, "");
            thirdsourceName = bundle.getString(Constants.THIRD_FORWARD_SOURCE_NAME, "");
            thirdreturnName = bundle.getString(Constants.THIRD_FORWARD_DESTINATION_NAME, "");
            isNonStopChecked = bundle.getBoolean(Constants.ONE_WAY_NONSTOP_CHECKED, false);
            multiCityNonStopChecked = bundle.getBoolean(Constants.MULTI_TRIP_NONSTOP_CHECKED, false);
            randomNumber = bundle.getString(Constants.BOOKING_RANDOM_NUMBER, "");
            Singleton.getInstance().refundStatusArrayList.clear();
            Singleton.getInstance().resultStopsArrayList.clear();
            Singleton.getInstance().resultAirlinesArrayList.clear();
            Singleton.getInstance().resultAirportsArrayList.clear();
            Singleton.getInstance().firstAirportsArrayList.clear();
            Singleton.getInstance().secondAirportsArrayList.clear();
            Singleton.getInstance().thirdAirportsArrayList.clear();
            Singleton.getInstance().resultTimeArrayList.clear();
            Singleton.getInstance().departureTimeArrayList.clear();
            Singleton.getInstance().arrivalTimeArrayList.clear();
        }
//        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
//        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
//        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
//        if (tokenTime == -1) {
//            getTokenFromServer();
//        } else {
//            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
//            if (diff > Long.parseLong(expireIn)) {
//                getTokenFromServer();
//            }
//        }
        TextView backTextView = (TextView) findViewById(R.id.back_text);
        TextView sourceNameTextView = (TextView) findViewById(R.id.source_name);
        TextView destinationNameTextView = (TextView) findViewById(R.id.destination_name);
        TextView journeyDateTextView = (TextView) findViewById(R.id.journey_datetext);
        TextView travellerCountTextView = (TextView) findViewById(R.id.traveller_count);
        TextView filterText = (TextView) findViewById(R.id.filter_text);
        TextView classTypeTextView = (TextView) findViewById(R.id.class_type);
        LinearLayout filterLayout = (LinearLayout) findViewById(R.id.filter_layout);
        LinearLayout returnBackLayout = (LinearLayout) findViewById(R.id.layout_back);
        ListView forwardListView = (ListView) findViewById(R.id.flights_listview);
        flightListLayout = (RelativeLayout) findViewById(R.id.rec_view_layout);
        headerViewLayout = (RelativeLayout) findViewById(R.id.header_view_layout);
        ImageView flightFlowImage = (ImageView) findViewById(R.id.flight_flow_image);
        View headerView = getLayoutInflater().inflate(R.layout.flight_info_header_layout, null);
        LinearLayout flightInfoHeaderLayout = (LinearLayout) headerView.findViewById(R.id.flight_info_header_layout);
        ImageView flightHeaderImage = (ImageView) headerView.findViewById(R.id.flight_image);
        TextView flightHeaderText = (TextView) headerView.findViewById(R.id.flight_journey_text);
        View headerDividerView = (View) headerView.findViewById(R.id.header_divider_view);
        totalCountLayout = (LinearLayout) headerView.findViewById(R.id.total_count_layout);
        earliestCountLayout = (LinearLayout) headerView.findViewById(R.id.earliest_count_layout);
        fastestCountLayout = (LinearLayout) headerView.findViewById(R.id.fastest_count_layout);
        totalFlightCountTextView = (TextView) headerView.findViewById(R.id.total_flightcount);
        flightCountEarlierTextView = (TextView) headerView.findViewById(R.id.flight_count_earliest);
        flightCountFastestTextView = (TextView) headerView.findViewById(R.id.flight_count_fastest);
        RecyclerView recyclerView = (RecyclerView) headerView.findViewById(R.id.flights_recyclelistview);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        if (isArabicLang) {
            errorMessageText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        mAdapter = new HorizontalScrollRecyclerViewAdapter(FlightForwardResultsActivity.this, flightsArrayList, airLineNamesMap,
                FlightForwardResultsActivity.this, "1");
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        regularFace = Typeface.createFromAsset(getAssets(), fontTitle);
        Typeface mediumFace = Typeface.createFromAsset(getAssets(), fontNormal);
        backTextView.setTypeface(regularFace);
        sourceNameTextView.setTypeface(regularFace);
        destinationNameTextView.setTypeface(regularFace);
        journeyDateTextView.setTypeface(regularFace);
        travellerCountTextView.setTypeface(regularFace);
        classTypeTextView.setTypeface(regularFace);
        totalFlightCountTextView.setTypeface(regularFace);
        flightCountEarlierTextView.setTypeface(regularFace);
        flightCountFastestTextView.setTypeface(regularFace);
        errorMessageText.setTypeface(regularFace);
        filterText.setTypeface(regularFace);
        flightHeaderText.setTypeface(mediumFace);
        if (isArabicLang) {
            backTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            sourceNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            destinationNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            journeyDateTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            travellerCountTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            classTypeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            totalFlightCountTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            flightCountEarlierTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            flightCountFastestTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            flightHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            filterText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            flightFlowImage.setRotation(180);
            flightHeaderImage.setRotation(180);
        }
        String journeyCabinType = "";
        if (selectedClassType.equalsIgnoreCase("Economy")) {
            journeyCabinType = getString(R.string.label_search_flight_economy);
            selectedClass = "Y";
        } else if (selectedClassType.equalsIgnoreCase("Business")) {
            journeyCabinType = getString(R.string.label_search_flight_business);
            selectedClass = "B";
        } else if (selectedClassType.equalsIgnoreCase("First")) {
            journeyCabinType = getString(R.string.label_search_flight_first);
            selectedClass = "F";
        }
        classTypeTextView.setText(journeyCabinType);
        if (isInternetPresent) {
            //refreshTokenData.checkServiceCall(FlightForwardResultsActivity.this, FlightForwardResultsActivity.this);
            getRequestFromServer(requestJSON);
            if (tripType.equalsIgnoreCase("2"))
                getFareRequestFromServer(requestJSON);
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(regularFace);
            errorDescriptionText.setTypeface(regularFace);
            searchButton.setTypeface(regularFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(FlightForwardResultsActivity.this);
                    if (isInternetPresent) {
                        headerViewLayout.removeView(errorView);
                        getRequestFromServer(requestJSON);
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            headerViewLayout.addView(errorView);
        }
        totalFlights = true;
        sourceNameTextView.setText(sourceName);
        destinationNameTextView.setText(returnName);
        flightHeaderText.setText(String.format(getResources().getString(R.string.label_select_your_onward_flight), sourceName, returnName));
        if (tripType.equalsIgnoreCase("1")) {
            flightInfoHeaderLayout.setVisibility(View.GONE);
            headerDividerView.setVisibility(View.GONE);
            flightFlowImage.setImageResource(R.drawable.rightarrow);
            journeyDateTextView.setText(Utils.formatDateToString(forwardFlightDate, FlightForwardResultsActivity.this));
        } else {
            flightInfoHeaderLayout.setVisibility(View.VISIBLE);
            headerDividerView.setVisibility(View.VISIBLE);
            flightFlowImage.setImageResource(R.drawable.vice_versa);
            journeyDateTextView.setText(String.format(getResources().getString(R.string.label_placeholder_to), Utils.formatDateToString(forwardFlightDate, FlightForwardResultsActivity.this), Utils.formatDateToString(returnFlightDate, FlightForwardResultsActivity.this)));
        }
        travellerCountTextView.setText(travellerCount);
        returnBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        earliestCountLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                earliestCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.white_color));
                fastestCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_view_bg));
                totalCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_view_bg));
                fastestFlights = false;
                totalFlights = false;
                earliestFlights = true;
                handleEarliestFlights();
                setFlightsCountsUI();
            }
        });
        totalCountLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                earliestCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_view_bg));
                fastestCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_view_bg));
                totalCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.white_color));
                fastestFlights = false;
                totalFlights = true;
                earliestFlights = false;
                handleFlightsCount();
                setFlightsCountsUI();
                handleFilterResults();
            }
        });
        fastestCountLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                earliestCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_view_bg));
                fastestCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.white_color));
                totalCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_view_bg));
                fastestFlights = true;
                totalFlights = false;
                earliestFlights = false;
                handleNonStopFlights();
                setFlightsCountsUI();
            }
        });
        forwardListView.addHeaderView(headerView);
        adapterList = new FlightOneWayTripAdapter(FlightForwardResultsActivity.this, FlightForwardResultsActivity.this, pricedItineraryObjectArrayList,
                airportNamesMap, airLineNamesMap, travellerCount, sourceName, returnName, FlightForwardResultsActivity.this, tripType, selectedClassType);
        forwardListView.setAdapter(adapterList);
        adapterList.notifyDataSetChanged();
        adapterList.notifyDataSetChanged();
        filterLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handlefilter();
            }
        });
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView waitText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setTypeface(regularFace);
        waitText.setTypeface(regularFace);
        if (isArabicLang) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            waitText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        headerViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        try {
            headerViewLayout.removeView(loadingView);
            loadingView = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(FlightForwardResultsActivity.this, FlightForwardResultsActivity.this,
                Constants.SEARCH_RQ_URL, "", requestJSON, GET_SEARCH_RESULTS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void getFareRequestFromServer(String requestJSON) {
        HTTPAsync async = new HTTPAsync(FlightForwardResultsActivity.this, FlightForwardResultsActivity.this,
                Constants.FARE_COMBINATION_SEARCH_RQ, "", requestJSON, GET_FARE_COMBINATION_RESULTS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String receivedMerchandiseData = "";

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "DATA::" + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            closeLoading();
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(regularFace);
            errorDescriptionText.setTypeface(regularFace);
            searchButton.setTypeface(regularFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            headerViewLayout.addView(errorView);
        } else {
            if (serviceType == GET_MERCHANDISE_DATA) {
                JSONObject obj = null;
                receivedMerchandiseData = data;
                try {
                    obj = new JSONObject(data);
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        if (obj.has("data")) {
                            boolean isRoutesAvailable = false;
                            for (int i = 0; i < flightNamesArray.size(); i++) {
                                String key = flightNamesArray.get(i).getKey();
                                if (obj.getJSONObject("data").has(key)) {
                                    JSONArray flightNameArray = obj.getJSONObject("data").getJSONArray(key);
                                    for (int j = 0; j < flightNameArray.length(); j++) {
                                        if (flightNameArray.getJSONObject(j).has("route")) {
                                            JSONArray routeArr = flightNameArray.getJSONObject(j).getJSONArray("route");
                                            if (routeArr.length() > 0) {
                                                isRoutesAvailable = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (isRoutesAvailable) {
                                handleAirportCountry();
                            } else {
                                parseMerchantData();
                            }
                            Utils.printMessage(TAG, "GET_MERCHANDISE_DATA  completed");
                        }
                    }
                } catch (Exception e) {
                }
            } else if (serviceType == GET_MERCHANDISE_AIRPORT_DATA) {
                JSONObject obj = null;
                try {
                    obj = new JSONObject(data);
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        routesMap.clear();
                        if (obj.has("data")) {
                            JSONArray dataArr = obj.getJSONArray("data");
                            for (int i = 0; i < dataArr.length(); i++) {
                                routesMap.put(dataArr.getJSONObject(i).getString("iata"), dataArr.getJSONObject(i).getString("cc"));
                            }
                            parseMerchantData();
                        }
                    }
                } catch (Exception e) {
                }
                Utils.printMessage(TAG, "GET_MERCHANDISE_AIRPORT_DATA  completed");
            } else if (serviceType == GET_SEARCH_RESULTS) {
                boolean isConversionDone = parseJSON(data);
                if (isConversionDone) {
                    Utils.printMessage(TAG, "main Array SIZE: " + mainPricedItineraryArray.size() + " temp Arr Size : " + tempPricedItineraryArray.size());
                    pricedItineraryObjectArrayList.clear();
                    for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                        for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                            OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                            if (odoBean.getRefNum().equalsIgnoreCase("1")) {
                                pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            }
                        }
                    }
                    updateStopsFlights(stopsArray, isNonStopChecked);
                    sortByLeastDuration();
                }
            } else if (serviceType == GET_FARE_RULE_RESULTS) {
                JSONObject resultDataObj = null;
                String tdrResult = "";
                try {
                    resultDataObj = new JSONObject(data);
                    tdrResult = resultDataObj.getJSONObject("fareruleRS").getString("tdr");
                } catch (Exception e) {
                    Utils.printMessage(TAG, "Exception occured::" + e.getMessage());
                }
                if (tdrResult == null || tdrResult.equalsIgnoreCase("")) {
                    closeLoading();
                    displayErrorMessage(getResources().getString(R.string.label_selected_flight_error_message));
                    return;
                } else {
                    Intent intent = new Intent(FlightForwardResultsActivity.this, TripSummaryActivity.class);
                    intent.putExtra(Constants.JSON_RESULT, data);
                    intent.putExtra(Constants.TRIP_TYPE, String.valueOf(tripType));
                    intent.putExtra(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                    intent.putExtra(Constants.AIPI_JSON_OBJECT, aipiJSONObject);
                    intent.putExtra(Constants.AI_JSON_OBJECT, aiJSONObject);
                    intent.putExtra(Constants.FORWARD_SOURCE, source);
                    intent.putExtra(Constants.FORWARD_DESTINATION, destination);
                    intent.putExtra(Constants.SECOND_FORWARD_SOURCE, secondSource);
                    intent.putExtra(Constants.SECOND_FORWARD_DESTINATION, secondDestination);
                    intent.putExtra(Constants.THIRD_FORWARD_SOURCE, thirdSource);
                    intent.putExtra(Constants.THIRD_FORWARD_DESTINATION, thirdDestination);
                    intent.putExtra(Constants.FORWARD_FLIGHT_DATE, forwardFlightDate);
                    intent.putExtra(Constants.RETURN_FLIGHT_DATE, returnFlightDate);
                    intent.putExtra(Constants.AIRLINE_NAME_HASHMAP, airLineNamesMap);
                    intent.putExtra(Constants.AIRPORT_NAME_HASHMAP, airportNamesMap);
                    startActivityForResult(intent, SEARCH_FLIGHT_RESULT);
                }
            } else if (serviceType == GET_ACCESS_TOKEN) {
                JSONObject obj = null;
                try {
                    obj = new JSONObject(data);
                    String accessToken = obj.getString("accessToken");
                    String expireIn = obj.getString("expireIn");
                    String refreshToken = obj.getString("refreshToken");
                    long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                    SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Constants.ACCESS_TOKEN, accessToken);
                    editor.putString(Constants.EXPIRE_IN, expireIn);
                    editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                    editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                    editor.apply();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (serviceType == GET_FARE_COMBINATION_RESULTS) {
                closeLoading();
                handleMerchandiseRequest();
                Utils.printMessage(TAG, "GET_FARE_COMBINATION_RESULTS::");
                parseFareCombinationJSON(data);
                getAirportDetails();
                getAirLineDetails();
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void handleAirportCountry() {
        ArrayList<String> list = new ArrayList<String>();
        for (OriginDestinationOptionBean odo : showOdoArrayList) {
            for (FSDataBean fs : odo.getFsDataBeanArrayList()) {
                if (!list.contains(fs.getDap())) {
                    list.add(fs.getDap());
                }
                if (!list.contains(fs.getAap())) {
                    list.add(fs.getAap());
                }
            }
        }
        JSONArray array = new JSONArray();
        for (String str : list) {
            array.put(str);
        }
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
            mainJSON.put("lang", selectedLang);
            mainJSON.put("ac", array);
        } catch (Exception e) {
        }
        Utils.printMessage(TAG, "mainJSON::" + mainJSON.toString());
        HTTPAsync async = new HTTPAsync(FlightForwardResultsActivity.this, FlightForwardResultsActivity.this, Constants.MERCHANDISE_BASE_URL,
                Constants.MERCHANDISE_AIRPORT_URI, mainJSON.toString(), GET_MERCHANDISE_AIRPORT_DATA, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void parseMerchantData() {
        try {
            mainMarchArrayList.clear();
            JSONObject obj = new JSONObject(receivedMerchandiseData);
            String status = obj.getString("status");
            if (status.equalsIgnoreCase("SUCCESS")) {
                if (obj.has("data")) {
                    for (int i = 0; i < flightNamesArray.size(); i++) {
                        String key = flightNamesArray.get(i).getKey();
                        if (obj.getJSONObject("data").has(key)) {
                            JSONArray flightNameArray = obj.getJSONObject("data").getJSONArray(key);
                            for (int j = 0; j < flightNameArray.length(); j++) {
                                boolean isAvailableSector = false;
                                if (flightNameArray.getJSONObject(j).has("route")) {
                                    JSONArray routeArr = flightNameArray.getJSONObject(j).getJSONArray("route");
                                    if (routeArr.length() > 0) {
                                        for (int k = 0; k < routeArr.length(); k++) {
                                            JSONObject routeObj = routeArr.getJSONObject(k);
                                            String dap = flightNamesArray.get(i).getDap();
                                            String aap = flightNamesArray.get(i).getAap();
                                            if (routeObj.getString("sourceCountry").equalsIgnoreCase(routesMap.get(dap)) &&
                                                    routeObj.getString("destCountry").equalsIgnoreCase(routesMap.get(aap))) {
                                                for (int z = 0; z < routeObj.getJSONArray("sectors").length(); z++) {
                                                    JSONObject sectorObj = routeObj.getJSONArray("sectors").getJSONObject(z);
                                                    if ((sectorObj.getString("originSec").trim().equalsIgnoreCase(dap) || sectorObj.getString("originSec").trim().equalsIgnoreCase("all")) &&
                                                            (sectorObj.getString("destSec").trim().equalsIgnoreCase(aap) || sectorObj.getString("destSec").trim().equalsIgnoreCase("all"))) {
                                                        isAvailableSector = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        isAvailableSector = true;
                                    }
                                } else {
                                    isAvailableSector = true;
                                }
                                if (!isAvailableSector) {
                                    continue;
                                }
                                if (flightNameArray.getJSONObject(j).has("amenities")) {
                                    FlightAmenitiesBean amenitiesBean = new FlightAmenitiesBean();
                                    JSONObject amenitiesObj = flightNameArray.getJSONObject(j).getJSONObject("amenities");
                                    amenitiesBean.setFlightName(key);
                                    amenitiesBean.setAap(flightNamesArray.get(i).getAap());
                                    amenitiesBean.setDap(flightNamesArray.get(i).getDap());
                                    if (amenitiesObj.has("classType"))
                                        amenitiesBean.setClassType(amenitiesObj.getString("classType"));
                                    if (amenitiesObj.has("seatPitch")) {
                                        amenitiesBean.setSeatPitch(amenitiesObj.getString("seatPitch"));
                                    } else {
                                        amenitiesBean.setSeatPitch("");
                                    }
                                    if (amenitiesObj.has("avod")) {
                                        String avodType = amenitiesObj.getString("avod");
                                        if (avodType.equalsIgnoreCase("Yes")) {
                                            if (amenitiesObj.has("videoType")) {
                                                amenitiesBean.setVideoType(checkAmenitiesValue(AmenitiesConstants.avodVideoHashMap, amenitiesObj.getString("videoType")));
                                            } else {
                                                amenitiesBean.setVideoType("");
                                            }
                                        } else if (avodType.equalsIgnoreCase("Charge")) {
                                            if (amenitiesObj.has("videoType")) {
                                                amenitiesBean.setVideoType(checkAmenitiesValue(AmenitiesConstants.avodChargeVideoHashMap, amenitiesObj.getString("videoType")));
                                            } else {
                                                amenitiesBean.setVideoType("");
                                            }
                                        }
                                        amenitiesBean.setAvod(avodType);
                                    } else {
                                        amenitiesBean.setAvod("");
                                    }
                                    if (amenitiesObj.has("seatWidth")) {
                                        amenitiesBean.setSeatWidth(amenitiesObj.getString("seatWidth"));
                                    } else {
                                        amenitiesBean.setSeatWidth("");
                                    }
                                    if (amenitiesObj.has("seatLength")) {
                                        amenitiesBean.setSeatLength(amenitiesObj.getString("seatLength"));
                                    } else {
                                        amenitiesBean.setSeatLength("");
                                    }
                                    if (amenitiesObj.has("laptopPower")) {
                                        amenitiesBean.setLaptopPower(amenitiesObj.getString("laptopPower"));
                                    } else {
                                        amenitiesBean.setLaptopPower("");
                                    }
                                    if (amenitiesObj.has("seatType")) {
                                        amenitiesBean.setSeatType(checkAmenitiesValue(AmenitiesConstants.seatTypeHashMap, amenitiesObj.getString("seatType")));
                                    } else {
                                        amenitiesBean.setSeatType("");
                                    }
                                    if (amenitiesObj.has("powerType")) {
                                        amenitiesBean.setPowerType(checkAmenitiesValue(AmenitiesConstants.powerTypeHashMap, amenitiesObj.getString("powerType")));
                                    } else {
                                        amenitiesBean.setPowerType("");
                                    }
                                    if (amenitiesObj.has("mobile")) {
                                        amenitiesBean.setMobile(amenitiesObj.getString("mobile"));
                                    } else {
                                        amenitiesBean.setMobile("");
                                    }
                                    if (amenitiesObj.has("prayerArea")) {
                                        amenitiesBean.setPrayerArea(amenitiesObj.getString("prayerArea"));
                                    } else {
                                        amenitiesBean.setPrayerArea("");
                                    }
                                    if (amenitiesObj.has("noOfSeats"))
                                        amenitiesBean.setNoOfSeats(amenitiesObj.getString("noOfSeats"));
                                    if (amenitiesObj.has("meals")) {
                                        amenitiesBean.setMeals(checkAmenitiesValue(AmenitiesConstants.mealsHashMap, amenitiesObj.getString("meals")));
                                    } else {
                                        amenitiesBean.setMeals("");
                                    }
                                    if (amenitiesObj.has("wifi")) {
                                        amenitiesBean.setWifi(checkAmenitiesValue(AmenitiesConstants.wifiHashMap, amenitiesObj.getString("wifi")));
                                    } else {
                                        amenitiesBean.setWifi("");
                                    }
                                    if (amenitiesObj.has("infantCare")) {
                                        amenitiesBean.setInfantCare(checkAmenitiesValue(AmenitiesConstants.infantCareHashMap, amenitiesObj.getString("infantCare")));
                                    } else {
                                        amenitiesBean.setInfantCare("");
                                    }
                                    if (amenitiesObj.has("alocohol")) {
                                        amenitiesBean.setAlocohol(checkAmenitiesValue(AmenitiesConstants.alocoholHashMap, amenitiesObj.getString("alocohol")));
                                    } else {
                                        amenitiesBean.setAlocohol("");
                                    }
                                    if (amenitiesObj.has("avodScreenSize")) {
                                        amenitiesBean.setAvodScreenSize(amenitiesObj.getString("avodScreenSize"));
                                    } else {
                                        amenitiesBean.setAvodScreenSize("");
                                    }
                                    if (amenitiesObj.has("remarks")) {
                                        amenitiesBean.setRemarks(amenitiesObj.getString("remarks"));
                                    } else {
                                        amenitiesBean.setRemarks("");
                                    }
                                    if (amenitiesObj.has("provideTransportation")) {
                                        amenitiesBean.setProvideTransportation(amenitiesObj.getString("provideTransportation"));
                                    } else {
                                        amenitiesBean.setProvideTransportation("");
                                    }
                                    if (amenitiesObj.has("seatLayout")) {
                                        amenitiesBean.setSeatLayout(amenitiesObj.getString("seatLayout"));
                                    } else {
                                        amenitiesBean.setSeatLayout("");
                                    }
                                    if (amenitiesObj.has("provideOtherInfo")) {
                                        amenitiesBean.setProvideOtherInfo(amenitiesObj.getString("provideOtherInfo"));
                                    } else {
                                        amenitiesBean.setProvideOtherInfo("");
                                    }
                                    if (amenitiesObj.has("aircraftImage")) {
                                        amenitiesBean.setAircraftImage(amenitiesObj.getString("aircraftImage"));
                                    } else {
                                        amenitiesBean.setAircraftImage("");
                                    }
                                    if (amenitiesObj.has("classOverviewImage")) {
                                        amenitiesBean.setClassOverviewImage(amenitiesObj.getString("classOverviewImage"));
                                    } else {
                                        amenitiesBean.setClassOverviewImage("");
                                    }
                                    if (amenitiesObj.has("seatImage")) {
                                        amenitiesBean.setSeatImage(amenitiesObj.getString("seatImage"));
                                    } else {
                                        amenitiesBean.setSeatImage("");
                                    }
                                    if (amenitiesObj.has("facilityImage")) {
                                        amenitiesBean.setFacilityImage(amenitiesObj.getString("facilityImage"));
                                    } else {
                                        amenitiesBean.setFacilityImage("");
                                    }
                                    if (amenitiesObj.has("showerArea")) {
                                        amenitiesBean.setShowerArea(amenitiesObj.getString("showerArea"));
                                    } else {
                                        amenitiesBean.setShowerArea("");
                                    }
                                    if (amenitiesObj.has("bodyType")) {
                                        int bodyTypeVal = Integer.parseInt(amenitiesObj.getString("bodyType"));
                                        amenitiesBean.setBodyType(checkBodyTypeAmenitiesValue(AmenitiesConstants.bodyTypeHashMap, bodyTypeVal));
                                    } else {
                                        amenitiesBean.setBodyType("");
                                    }
                                    mainMarchArrayList.add(amenitiesBean);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        AsyncTaskJson task = new AsyncTaskJson();
        task.execute();
    }

    private boolean parseJSON(String data) {
        boolean isConversionDone = false;
        isFcDataParsing = false;
        airLineNamesMap.clear();
        airportNamesMap.clear();
        mainPricedItineraryArray.clear();
        tempPricedItineraryArray.clear();
        tempForwardPricedItineraryArray.clear();
        tempReturnPricedItineraryArray.clear();
        Singleton.getInstance().resultFlightsArrayList.clear();
        pricedItineraryObjectArrayList.clear();
        JSONObject obj = null;
        try {
            Object json = new JSONTokener(data).nextValue();
            if (json instanceof JSONObject) {
                obj = new JSONObject(data);
                if (obj.has("searchRS")) {
                    if (obj.getJSONObject("searchRS").has("pi")) {
                        JSONArray piArray = obj.getJSONObject("searchRS").getJSONArray("pi");
                        airlineArrayList.clear();
                        airportArrayList.clear();
                        for (int i = 0; i < piArray.length(); i++) {
                            PricedItineraryObject piObject = new PricedItineraryObject();
                            if (piArray.getJSONObject(i).has("ai")) {
                                if (piArray.getJSONObject(i).getJSONObject("ai").has("odos")) {
                                    if (piArray.getJSONObject(i).getJSONObject("ai").getJSONObject("odos").has("odo")) {
                                        JSONArray odoArray = piArray.getJSONObject(i).getJSONObject("ai").getJSONObject("odos").getJSONArray("odo");
                                        AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                                        aiBean.setDirId(Utils.checkStringVal(piArray.getJSONObject(i).getJSONObject("ai").getString("dirId")));
                                        long segmentFlightDuration = -1;
                                        Integer segmentDepartureTime = -1;
                                        ArrayList<OriginDestinationOptionBean> odoArrayList = new ArrayList<>();
                                        showOdoArrayList = new ArrayList<>();
                                        for (int j = 0; j < odoArray.length(); j++) {
                                            OriginDestinationOptionBean odoBean = new OriginDestinationOptionBean();
                                            odoBean.setRefNum(odoArray.getJSONObject(j).getString("refNum"));
                                            odoBean.setRph(odoArray.getJSONObject(j).getString("rph"));
                                            if (odoArray.getJSONObject(j).has("mOfcId")) {
                                                odoBean.setmOfcId(odoArray.getJSONObject(j).getString("mOfcId"));
                                            } else {
                                                odoBean.setmOfcId("");
                                            }
                                            odoBean.setTt(odoArray.getJSONObject(j).getString("tt"));
                                            odoBean.setRs(odoArray.getJSONObject(j).getString("rs"));
                                            odoBean.setCb(odoArray.getJSONObject(j).getString("cb"));
                                            odoBean.setPt(odoArray.getJSONObject(j).getString("pt"));
                                            ArrayList<FSDataBean> fsDataBeanArrayList = new ArrayList<>();
                                            String flightDuration = "";
                                            for (int k = 0; k < odoArray.getJSONObject(j).getJSONArray("fs").length(); k++) {
                                                JSONObject fsObj = odoArray.getJSONObject(j).getJSONArray("fs").getJSONObject(k);
                                                FSDataBean fsDataBean = new FSDataBean();
                                                fsDataBean.setDap(fsObj.getString("dap"));
                                                fsDataBean.setDt(fsObj.getString("dt"));
                                                fsDataBean.setAap(fsObj.getString("aap"));
                                                fsDataBean.setAt(fsObj.getString("at"));
                                                fsDataBean.setOal(fsObj.getString("oal"));
                                                fsDataBean.setMal(fsObj.getString("mal"));
                                                fsDataBean.setEq(fsObj.getString("eq"));
                                                fsDataBean.setCt(fsObj.getString("ct"));
                                                fsDataBean.setDur(fsObj.getString("dur"));
                                                fsDataBean.setFdur(fsObj.getString("fdur"));
                                                fsDataBean.setSt(fsObj.getString("st"));
                                                fsDataBean.setSi(fsObj.getString("si"));
                                                fsDataBean.setTi(fsObj.getString("ti"));
                                                fsDataBean.setFi(fsObj.getString("fi"));
                                                fsDataBean.setComment(fsObj.getString("comment"));
                                                fsDataBean.setAs(fsObj.getString("as"));
                                                fsDataBean.setFc(fsObj.getString("fc"));
                                                fsDataBean.setTofi(fsObj.getString("tofi"));
                                                fsDataBean.setFn(fsObj.getString("fn"));
                                                fsDataBean.setIs(fsObj.getString("is"));
                                                fsDataBean.setSq(fsObj.getString("sq"));
                                                fsDataBean.setArdt(fsObj.getString("ardt"));
                                                fsDataBean.setDdt(fsObj.getString("ddt"));
                                                fsDataBeanArrayList.add(fsDataBean);
                                                flightDuration = fsObj.getString("dur");
                                                AirportSectorBean bean = new AirportSectorBean();
                                                bean.setAap(fsObj.getString("aap"));
                                                bean.setDap(fsObj.getString("dap"));
                                                bean.setKey(fsObj.getString("mal") + "-" + fsObj.getString("eq"));
                                                flightNamesArray.add(bean);
                                                airlineArrayList.add(fsObj.getString("mal"));
                                                airportArrayList.add(fsObj.getString("aap"));
                                                airportArrayList.add(fsObj.getString("dap"));
                                            }
                                            long durationInMinutes = -1;
                                            String[] durArr = flightDuration.split(":");
                                            if (durArr.length > 0) {
                                                durationInMinutes = (Integer.parseInt(durArr[0]) * 60) + Integer.parseInt(durArr[1]);
                                            }
                                            odoBean.setFlightDuration(durationInMinutes);
                                            odoBean.setSegmentDepartureTime(Utils.dateFormaterToMinutes(fsDataBeanArrayList.get(0).getDdt()));
                                            odoBean.setFsDataBeanArrayList(fsDataBeanArrayList);
                                            if (odoBean.getRs().equalsIgnoreCase("Refundable")) {
                                                odoBean.setRefundType(1.0);
                                            } else {
                                                odoBean.setRefundType(2.0);
                                            }
                                            odoArrayList.add(odoBean);
                                            if (odoBean.getRefNum().equalsIgnoreCase("1")) {
                                                if (segmentFlightDuration == -1) {
                                                    segmentFlightDuration = durationInMinutes;
                                                    segmentDepartureTime = Utils.dateFormaterToMinutes(fsDataBeanArrayList.get(0).getDdt());
                                                } else {
                                                    if (segmentFlightDuration > durationInMinutes) {
                                                        segmentFlightDuration = durationInMinutes;
                                                        segmentDepartureTime = Utils.dateFormaterToMinutes(fsDataBeanArrayList.get(0).getDdt());
                                                    }
                                                }
                                            }
                                        }
                                        aiBean.setSegmentDepartureTime(segmentDepartureTime);
                                        aiBean.setSegmentFlightDuration(segmentFlightDuration);
                                        aiBean.setOriginDestinationOptionBeanArrayList(odoArrayList);
                                        showOdoArrayList.addAll(odoArrayList);
                                        piObject.setAiBean(aiBean);
                                    }
                                }
                            }
                            if (piArray.getJSONObject(i).has("aipi")) {
                                AirItineraryPricingInfoBean aipiBean = new AirItineraryPricingInfoBean();
                                if (piArray.getJSONObject(i).getJSONObject("aipi").has("itf")) {
                                    ItinTotalFareObjectBean itfBean = new ItinTotalFareObjectBean();
                                    itfBean.setBf(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("bf"));
                                    itfBean.settTx(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("tTx"));
                                    itfBean.settSF(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("tSF"));
                                    itfBean.setDisc(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("disc"));
                                    itfBean.settFare(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("tFare"));
                                    itfBean.setIc(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("ic"));
                                    itfBean.setUsfc(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("usfc"));
                                    itfBean.setPriceDiff(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("tFare"));
                                    aipiBean.setItfObject(itfBean);
                                }
                                aipiBean.setFq(piArray.getJSONObject(i).getJSONObject("aipi").getString("fq"));
                                JSONArray pfbdArray = piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("pfbds").getJSONArray("pfbd");
                                ArrayList<PFBDObjectBean> pfbdObjectBeanArrayList = new ArrayList<>();
                                for (int j = 0; j < pfbdArray.length(); j++) {
                                    PFBDObjectBean pfbdObjectBean = new PFBDObjectBean();
                                    pfbdObjectBean.setpType(pfbdArray.getJSONObject(j).getString("pType"));
                                    pfbdObjectBean.setQty(pfbdArray.getJSONObject(j).getString("qty"));
                                    pfbdObjectBean.setUsfcp(pfbdArray.getJSONObject(j).getString("usfcp"));
                                    ArrayList<String> fbcArray = new ArrayList<>();
                                    if (pfbdArray.getJSONObject(j).has("fbc")) {
                                        try {
                                            if (!pfbdArray.getJSONObject(j).isNull("fbc")) {
                                                for (int k = 0; k < pfbdArray.getJSONObject(j).getJSONArray("fbc").length(); k++) {
                                                    fbcArray.add(pfbdArray.getJSONObject(j).getJSONArray("fbc").getString(k));
                                                }
                                                pfbdObjectBean.setFbcArray(fbcArray);
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                    PassengerFareObjectBean passengerFareObjectBean = new PassengerFareObjectBean();
                                    passengerFareObjectBean.setPbf(pfbdArray.getJSONObject(j).getJSONObject("pf").getString("pbf"));
                                    passengerFareObjectBean.setPtFare(pfbdArray.getJSONObject(j).getJSONObject("pf").getString("ptFare"));
                                    ArrayList<TaxObjectBean> taxObjectBeanArrayList = new ArrayList<>();
                                    for (int k = 0; k < pfbdArray.getJSONObject(j).getJSONObject("pf").getJSONArray("txs").length(); k++) {
                                        TaxObjectBean bean = new TaxObjectBean();
                                        bean.setAmt(pfbdArray.getJSONObject(j).getJSONObject("pf").getJSONArray("txs").getJSONObject(k).getString("amt"));
                                        bean.setTc(pfbdArray.getJSONObject(j).getJSONObject("pf").getJSONArray("txs").getJSONObject(k).getString("tc"));
                                        taxObjectBeanArrayList.add(bean);
                                    }
                                    passengerFareObjectBean.setTaxObjectBeanArrayList(taxObjectBeanArrayList);
                                    pfbdObjectBean.setPassengerFareObjectBean(passengerFareObjectBean);
                                    pfbdObjectBeanArrayList.add(pfbdObjectBean);
                                }
                                aipiBean.setPfbdObjectBeanArrayList(pfbdObjectBeanArrayList);
                                piObject.setAipiBean(aipiBean);
                            }
                            mainPricedItineraryArray.add(piObject);
                            isConversionDone = true;
                        }
                    }
                    if (obj.getJSONObject("searchRS").has("errors")) {
                        displayResultError();
                    }
                }
            } else {
                displayResultError();
            }
        } catch (Exception e) {
            e.printStackTrace();
            isConversionDone = false;
        }
        sortByPrice();
        Singleton.getInstance().resultFlightsArrayList = mainPricedItineraryArray;
        ArrayList<PricedItineraryObject> forwardPIArrayList = new ArrayList<>();
        for (int i = 0; i < mainPricedItineraryArray.size(); i++) {
            PricedItineraryObject pricedItineraryObject = new PricedItineraryObject();
            AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
            aiBean.setDirId(mainPricedItineraryArray.get(i).getAiBean().getDirId());
            aiBean.setOriginDestinationOptionBeanArrayList(mainPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList());
            pricedItineraryObject.setAiBean(aiBean);
            pricedItineraryObject.setAipiBean(mainPricedItineraryArray.get(i).getAipiBean());
            forwardPIArrayList.add(pricedItineraryObject);
        }
        try {
            tempPricedItineraryArray.clear();
            for (int i = 0; i < forwardPIArrayList.size(); i++) {
                tempForwardPricedItineraryArray.clear();
                ArrayList<PricedItineraryObject> tempPiArr = new ArrayList<>();
                tempPiArr.add(forwardPIArrayList.get(i));
                for (int j = 0; j < forwardPIArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean optionBean = forwardPIArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    if (optionBean.getRefNum().equalsIgnoreCase("1")) {
                        tempForwardPricedItineraryArray.add(optionBean);
                    }
                    if (optionBean.getRefNum().equalsIgnoreCase("2")) {
                        tempReturnPricedItineraryArray.add(optionBean);
                    }
                }
                if (tempForwardPricedItineraryArray.size() > 0) {
                    Collections.sort(tempForwardPricedItineraryArray, new Comparator<OriginDestinationOptionBean>() {
                        @Override
                        public int compare(OriginDestinationOptionBean e1, OriginDestinationOptionBean e2) {
                            Long id1 = Long.valueOf(e1.getSegmentDepartureTime());
                            Long id2 = Long.valueOf(e2.getSegmentDepartureTime());
                            return id1.compareTo(id2);
                        }
                    });
                }
                for (int k = 0; k < tempForwardPricedItineraryArray.size(); k++) {
                    ArrayList<OriginDestinationOptionBean> tempStorageArray = new ArrayList<>();
                    tempStorageArray.add(tempForwardPricedItineraryArray.get(k));
                    tempPiArr.get(0).getAiBean().setOriginDestinationOptionBeanArrayList(tempStorageArray);
                    PricedItineraryObject pricedItineraryObject = new PricedItineraryObject();
                    AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                    aiBean.setDirId(tempPiArr.get(0).getAiBean().getDirId());
                    aiBean.setOriginDestinationOptionBeanArrayList(tempStorageArray);
                    pricedItineraryObject.setAiBean(aiBean);
                    pricedItineraryObject.setAipiBean(tempPiArr.get(0).getAipiBean());
                    tempPricedItineraryArray.add(pricedItineraryObject);
                }
            }
            Utils.printMessage(TAG, "tempPi size : " + tempPricedItineraryArray.size());
        } catch (Exception e) {
            Utils.printMessage(TAG, " referNum error : " + e.getMessage());
        }
        if (tempPricedItineraryArray.size() > 0) {
            pricedItineraryObjectArrayList.addAll(tempPricedItineraryArray);
            if (pricedItineraryObjectArrayList.size() > 0) {
                Utils.getOneWayFlightStopValues(this, tripType, isNonStopChecked, multiCityNonStopChecked, pricedItineraryObjectArrayList);
                stopsArray = Singleton.getInstance().resultStopsArrayList;
                updateStopsFlights(stopsArray, isNonStopChecked);
                if (tripType.equalsIgnoreCase("1")) {
                    handleMerchandiseRequest();
                    getAirportDetails();
                    getAirLineDetails();
                    closeLoading();
                }
                setFlightsCountsUI();
                handleHorizontalFlights(pricedItineraryObjectArrayList);
            } else {
                displayResultError();
            }
        } else {
            displayResultError();
        }
        return isConversionDone;
    }

    private void parseFareCombinationJSON(String data) {
        mainFCPricedItineraryArray.clear();
        Singleton.getInstance().fCResultFlightPIArrayList.clear();
        JSONObject obj = null;
        isFcDataParsing = true;
        Utils.printMessage(TAG, "parseFareCombinationJSON: " + isFcDataParsing);
        try {
            obj = new JSONObject(data);
            if (obj.has("searchRS")) {
                if (obj.getJSONObject("searchRS").has("pi")) {
                    JSONArray piArray = obj.getJSONObject("searchRS").getJSONArray("pi");
                    for (int i = 0; i < piArray.length(); i++) {
                        PricedItineraryObject piObject = new PricedItineraryObject();
                        if (piArray.getJSONObject(i).has("ai")) {
                            if (piArray.getJSONObject(i).getJSONObject("ai").has("odos")) {
                                if (piArray.getJSONObject(i).getJSONObject("ai").getJSONObject("odos").has("odo")) {
                                    JSONArray odoArray = piArray.getJSONObject(i).getJSONObject("ai").getJSONObject("odos").getJSONArray("odo");
                                    AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                                    aiBean.setDirId(Utils.checkStringVal(piArray.getJSONObject(i).getJSONObject("ai").getString("dirId")));
                                    long segmentFlightDuration = -1;
                                    Integer segmentDepartureTime = -1;
                                    ArrayList<OriginDestinationOptionBean> odoArrayList = new ArrayList<>();
                                    for (int j = 0; j < odoArray.length(); j++) {
                                        OriginDestinationOptionBean odoBean = new OriginDestinationOptionBean();
                                        odoBean.setRefNum(odoArray.getJSONObject(j).getString("refNum"));
                                        odoBean.setRph(odoArray.getJSONObject(j).getString("rph"));
                                        if (odoArray.getJSONObject(j).has("mOfcId")) {
                                            odoBean.setmOfcId(odoArray.getJSONObject(j).getString("mOfcId"));
                                        } else {
                                            odoBean.setmOfcId("");
                                        }
                                        odoBean.setTt(odoArray.getJSONObject(j).getString("tt"));
                                        odoBean.setRs(odoArray.getJSONObject(j).getString("rs"));
                                        odoBean.setCb(odoArray.getJSONObject(j).getString("cb"));
                                        odoBean.setPt(odoArray.getJSONObject(j).getString("pt"));
                                        ArrayList<FSDataBean> fsDataBeanArrayList = new ArrayList<>();
                                        String flightDuration = "";
                                        for (int k = 0; k < odoArray.getJSONObject(j).getJSONArray("fs").length(); k++) {
                                            JSONObject fsObj = odoArray.getJSONObject(j).getJSONArray("fs").getJSONObject(k);
                                            FSDataBean fsDataBean = new FSDataBean();
                                            fsDataBean.setDap(fsObj.getString("dap"));
                                            fsDataBean.setDt(fsObj.getString("dt"));
                                            fsDataBean.setAap(fsObj.getString("aap"));
                                            fsDataBean.setAt(fsObj.getString("at"));
                                            fsDataBean.setOal(fsObj.getString("oal"));
                                            fsDataBean.setMal(fsObj.getString("mal"));
                                            fsDataBean.setEq(fsObj.getString("eq"));
                                            fsDataBean.setCt(fsObj.getString("ct"));
                                            fsDataBean.setDur(fsObj.getString("dur"));
                                            fsDataBean.setFdur(fsObj.getString("fdur"));
                                            fsDataBean.setSt(fsObj.getString("st"));
                                            fsDataBean.setSi(fsObj.getString("si"));
                                            fsDataBean.setTi(fsObj.getString("ti"));
                                            fsDataBean.setFi(fsObj.getString("fi"));
                                            fsDataBean.setComment(fsObj.getString("comment"));
                                            fsDataBean.setAs(fsObj.getString("as"));
                                            fsDataBean.setFc(fsObj.getString("fc"));
                                            fsDataBean.setTofi(fsObj.getString("tofi"));
                                            fsDataBean.setFn(fsObj.getString("fn"));
                                            fsDataBean.setIs(fsObj.getString("is"));
                                            fsDataBean.setSq(fsObj.getString("sq"));
                                            fsDataBean.setArdt(fsObj.getString("ardt"));
                                            fsDataBean.setDdt(fsObj.getString("ddt"));
                                            fsDataBeanArrayList.add(fsDataBean);
                                            flightDuration = fsObj.getString("dur");
                                            AirportSectorBean bean = new AirportSectorBean();
                                            bean.setAap(fsObj.getString("aap"));
                                            bean.setDap(fsObj.getString("dap"));
                                            bean.setKey(fsObj.getString("mal") + "-" + fsObj.getString("eq"));
                                            if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                                                flightNamesArray.add(bean);
                                                airlineArrayList.add(fsObj.getString("mal"));
                                                airportArrayList.add(fsObj.getString("aap"));
                                                airportArrayList.add(fsObj.getString("dap"));
                                            }
                                        }
                                        long durationInMinutes = -1;
                                        String[] durArr = flightDuration.split(":");
                                        if (durArr.length > 0) {
                                            durationInMinutes = (Integer.parseInt(durArr[0]) * 60) + Integer.parseInt(durArr[1]);
                                        }
                                        odoBean.setFlightDuration(durationInMinutes);
                                        odoBean.setSegmentDepartureTime(Utils.dateFormaterToMinutes(fsDataBeanArrayList.get(0).getDdt()));
                                        odoBean.setFsDataBeanArrayList(fsDataBeanArrayList);
                                        if (odoBean.getRs().equalsIgnoreCase("Refundable")) {
                                            odoBean.setRefundType(1.0);
                                        } else {
                                            odoBean.setRefundType(2.0);
                                        }
                                        odoArrayList.add(odoBean);
                                        if (odoBean.getRefNum().equalsIgnoreCase("1")) {
                                            if (segmentFlightDuration == -1) {
                                                segmentFlightDuration = durationInMinutes;
                                                segmentDepartureTime = Utils.dateFormaterToMinutes(fsDataBeanArrayList.get(0).getDdt());
                                            } else {
                                                if (segmentFlightDuration > durationInMinutes) {
                                                    segmentFlightDuration = durationInMinutes;
                                                    segmentDepartureTime = Utils.dateFormaterToMinutes(fsDataBeanArrayList.get(0).getDdt());
                                                }
                                            }
                                        }
                                    }
                                    aiBean.setSegmentDepartureTime(segmentDepartureTime);
                                    aiBean.setSegmentFlightDuration(segmentFlightDuration);
                                    aiBean.setOriginDestinationOptionBeanArrayList(odoArrayList);
                                    piObject.setAiBean(aiBean);
                                }
                            }
                        }
                        if (piArray.getJSONObject(i).has("aipi")) {
                            AirItineraryPricingInfoBean aipiBean = new AirItineraryPricingInfoBean();
                            if (piArray.getJSONObject(i).getJSONObject("aipi").has("itf")) {
                                ItinTotalFareObjectBean itfBean = new ItinTotalFareObjectBean();
                                itfBean.setBf(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("bf"));
                                itfBean.settTx(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("tTx"));
                                itfBean.settSF(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("tSF"));
                                itfBean.setDisc(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("disc"));
                                itfBean.settFare(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("tFare"));
                                itfBean.setIc(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("ic"));
                                itfBean.setUsfc(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("usfc"));
                                itfBean.setPriceDiff(piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("itf").getString("tFare"));
                                aipiBean.setItfObject(itfBean);
                            }
                            aipiBean.setFq(piArray.getJSONObject(i).getJSONObject("aipi").getString("fq"));
                            JSONArray pfbdArray = piArray.getJSONObject(i).getJSONObject("aipi").getJSONObject("pfbds").getJSONArray("pfbd");
                            ArrayList<PFBDObjectBean> pfbdObjectBeanArrayList = new ArrayList<>();
                            for (int j = 0; j < pfbdArray.length(); j++) {
                                PFBDObjectBean pfbdObjectBean = new PFBDObjectBean();
                                pfbdObjectBean.setpType(pfbdArray.getJSONObject(j).getString("pType"));
                                pfbdObjectBean.setQty(pfbdArray.getJSONObject(j).getString("qty"));
                                pfbdObjectBean.setUsfcp(pfbdArray.getJSONObject(j).getString("usfcp"));
                                ArrayList<String> fbcArray = new ArrayList<>();
                                if (pfbdArray.getJSONObject(j).has("fbc")) {
                                    try {
                                        if (!pfbdArray.getJSONObject(j).isNull("fbc")) {
                                            for (int k = 0; k < pfbdArray.getJSONObject(j).getJSONArray("fbc").length(); k++) {
                                                fbcArray.add(pfbdArray.getJSONObject(j).getJSONArray("fbc").getString(k));
                                            }
                                            pfbdObjectBean.setFbcArray(fbcArray);
                                        }
                                    } catch (Exception e) {
                                    }
                                }
                                PassengerFareObjectBean passengerFareObjectBean = new PassengerFareObjectBean();
                                passengerFareObjectBean.setPbf(pfbdArray.getJSONObject(j).getJSONObject("pf").getString("pbf"));
                                passengerFareObjectBean.setPtFare(pfbdArray.getJSONObject(j).getJSONObject("pf").getString("ptFare"));
                                ArrayList<TaxObjectBean> taxObjectBeanArrayList = new ArrayList<>();
                                for (int k = 0; k < pfbdArray.getJSONObject(j).getJSONObject("pf").getJSONArray("txs").length(); k++) {
                                    TaxObjectBean bean = new TaxObjectBean();
                                    bean.setAmt(pfbdArray.getJSONObject(j).getJSONObject("pf").getJSONArray("txs").getJSONObject(k).getString("amt"));
                                    bean.setTc(pfbdArray.getJSONObject(j).getJSONObject("pf").getJSONArray("txs").getJSONObject(k).getString("tc"));
                                    taxObjectBeanArrayList.add(bean);
                                }
                                passengerFareObjectBean.setTaxObjectBeanArrayList(taxObjectBeanArrayList);
                                pfbdObjectBean.setPassengerFareObjectBean(passengerFareObjectBean);
                                pfbdObjectBeanArrayList.add(pfbdObjectBean);
                            }
                            aipiBean.setPfbdObjectBeanArrayList(pfbdObjectBeanArrayList);
                            piObject.setAipiBean(aipiBean);
                        }
                        mainFCPricedItineraryArray.add(piObject);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mainFCPricedItineraryArray.size() > 0) {
            isAirlineDataParsed = true;
            Singleton.getInstance().fCResultFlightPIArrayList = mainFCPricedItineraryArray;
            Utils.printMessage(TAG, "fc main array size : " + mainFCPricedItineraryArray.size());
        }
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private void getAirportDetails() {
        airportArrayList.add(source);
        airportArrayList.add(destination);
        airportData.resultData(FlightForwardResultsActivity.this, airportArrayList, new AirlinesInterface<HashMap<String, FlightAirportName>>() {
            @Override
            public void onSuccess(HashMap<String, FlightAirportName> response) {
                if (response.size() > 0) {
                    Utils.printMessage(TAG, " airportData DATA::" + response.size());
                    airportNamesMap.putAll(response);
                    adapterList.notifyDataSetChanged();
                    mAdapter.notifyDataSetChanged();
                    notifyData();
                } else {
                    closeLoading();
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(regularFace);
                    errorDescriptionText.setTypeface(regularFace);
                    searchButton.setTypeface(regularFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                    loadErrorType(Constants.WRONG_ERROR_PAGE);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    headerViewLayout.addView(errorView);
                }
            }

            @Override
            public void onFailed(String message) {
                Utils.printMessage(TAG, "DATA::" + message);
                displayResultError();
            }
        });
    }


    private void getAirLineDetails() {
        airlineArrayList.add(source);
        airlineArrayList.add(destination);
        airlinesData.resultData(FlightForwardResultsActivity.this, airlineArrayList, new AirlinesInterface<HashMap<String, String>>() {
            @Override
            public void onSuccess(HashMap<String, String> response) {
                if (response.size() > 0) {
                    Utils.printMessage(TAG, "airlinesData DATA::" + response.size());
                    airLineNamesMap.putAll(response);
                    if (loopCount == 0) {
                        isSecondServiceCalled = true;
                    }
                    loopCount++;
                    adapterList.notifyDataSetChanged();
                    mAdapter.notifyDataSetChanged();
                    notifyData();
                } else {
                    closeLoading();
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(regularFace);
                    errorDescriptionText.setTypeface(regularFace);
                    searchButton.setTypeface(regularFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                    loadErrorType(Constants.WRONG_ERROR_PAGE);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    headerViewLayout.addView(errorView);
                }
            }

            @Override
            public void onFailed(String message) {
                closeLoading();
                Utils.printMessage(TAG, "DATA::" + message);
                displayResultError();
            }
        });
//        airportArrayList.add(source);
//        airportArrayList.add(destination);
//        airportData.resultData(FlightForwardResultsActivity.this, airportArrayList, new AirlinesInterface<HashMap<String, FlightAirportName>>() {
//            @Override
//            public void onSuccess(HashMap<String, FlightAirportName> response) {
//                if (response.size() > 0) {
//                    Utils.printMessage(TAG, "DATA::" + response.size());
//                    airportNamesMap.putAll(response);
//                    notifyData();
//                }
//            }
//
//            @Override
//            public void onFailed(String message) {
//                Utils.printMessage(TAG, "DATA::" + message);
//                displayResultError();
//            }
//        });
    }

    private void notifyData() {
        Utils.printMessage(TAG, "notifyData  completed");
        adapterList.notifyDataSetChanged();
        mAdapter.notifyDataSetChanged();
        if (isAirlineDataParsed) {
            isReturnFlightClicked = true;
        }
        adapterList.notifyDataSetChanged();
//        if (airportNamesMap.size() > 0 && airLineNamesMap.size() > 0) {
//            Utils.printMessage(TAG, "airLine and airport  completed");
//            adapterList.notifyDataSetChanged();
//            mAdapter.notifyDataSetChanged();
//            if (isAirlineDataParsed) {
//                isReturnFlightClicked = true;
//            }
//        } else {
//            if (airportNamesMap.size() == 0) {
//                getAirportDetails();
//            }
//            if (airLineNamesMap.size() == 0) {
//                getAirLineDetails();
//            }
//        }
    }

    public void handlefilter() {
        if (loadingView != null) {
            return;
        } else {
            if (mainPricedItineraryArray.size() == 0) {
                return;
            } else {
                boolean isdetailfilter = false;
                Intent intent = new Intent(FlightForwardResultsActivity.this, FlightForwardFilterActivity.class);
                intent.putExtra(Constants.TRIP_TYPE, tripType);
                intent.putExtra(Constants.FORWARD_SOURCE, sourceName);
                intent.putExtra(Constants.FORWARD_DESTINATION, returnName);
                intent.putExtra(Constants.SECOND_FORWARD_SOURCE, secondsourceName);
                intent.putExtra(Constants.SECOND_FORWARD_DESTINATION, secondreturnName);
                intent.putExtra(Constants.THIRD_FORWARD_SOURCE, thirdsourceName);
                intent.putExtra(Constants.THIRD_FORWARD_DESTINATION, thirdreturnName);
                intent.putExtra(Constants.FORWARD_DETAILS, journeyDetails);
                intent.putExtra(Constants.PRICE_SELECTED_MINIMUM_VALUE, minValue);
                intent.putExtra(Constants.PRICE_SELECTED_MAXIMUM_VALUE, maxValue);
                intent.putExtra(Constants.ONE_WAY_NONSTOP_CHECKED, isNonStopChecked);
                intent.putExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, multiCityNonStopChecked);
                intent.putExtra(Constants.IS_DETAIL_FILTER, isdetailfilter);
                intent.putExtra(Constants.AIRLINE_NAME_HASHMAP, airLineNamesMap);
                intent.putExtra(Constants.AIRPORT_NAME_HASHMAP, airportNamesMap);
                intent.putExtra(Constants.AIRLINE_NAME, selectedMal);
                startActivityForResult(intent, FLIGHT_PRICE_FILTER);
                //overridePendingTransition(R.anim.translate_left_side, R.anim.translate_right_side);
            }
        }
    }

    private void sortByPrice() {
        if (mainPricedItineraryArray.size() > 0) {
            Collections.sort(mainPricedItineraryArray, new Comparator<PricedItineraryObject>() {
                @Override
                public int compare(PricedItineraryObject e1, PricedItineraryObject e2) {
                    int resultVal = 0;
                    String id1 = e1.getAipiBean().getItfObject().gettFare();
                    String id2 = e2.getAipiBean().getItfObject().gettFare();
                    Double price1 = 0.0;
                    Double price2 = 0.0;
                    try {
                        price1 = Double.parseDouble(id1);
                        price2 = Double.parseDouble(id2);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    int result = price1.compareTo(price2);
                    if (result > 0) {
                        resultVal = result;
                    } else if (result < 0) {
                        resultVal = result;
                    } else {
                        Long firstDur = Long.valueOf(e1.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getSegmentDepartureTime());
                        Long secondDur = Long.valueOf(e2.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getSegmentDepartureTime());
                        int durResult = firstDur.compareTo(secondDur);
                        if (durResult > 0) {
                            resultVal = durResult;
                        } else if (durResult < 0) {
                            resultVal = durResult;
                        } else {
                            Long firstFlight = e1.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFlightDuration();
                            Long secondFlight = e2.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFlightDuration();
                            resultVal = firstFlight.compareTo(secondFlight);
                        }
                    }
                    return resultVal;
                }
            });
        }
    }

    private void handleHorizontalFlights(ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList) {
        flightsArrayList.clear();
        for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
            for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                final OriginDestinationOptionBean odoObject = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                String refNum = odoObject.getRefNum();
                if (refNum.equalsIgnoreCase("1")) {
                    boolean isContains = false;
                    FlightBean flightBean = new FlightBean();
                    for (int k = 0; k < flightsArrayList.size(); k++) {
                        if (flightsArrayList.get(k).getImage().equalsIgnoreCase(odoObject.getFsDataBeanArrayList().get(0).getMal())) {
                            isContains = true;
                            break;
                        }
                    }
                    if (!isContains) {
                        flightBean.setImage(odoObject.getFsDataBeanArrayList().get(0).getMal());
                        flightBean.setPrice(pricedItineraryObjectArrayList.get(i).getAipiBean().getItfObject().gettFare());
                        flightsArrayList.add(flightBean);
                    }
                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void handleEarliestFlights() {
        pricedItineraryObjectArrayList.clear();
        for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
            for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                if (bean.getRefNum().equalsIgnoreCase("1")) {
                    int departureTimeVal = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                    if (departureTimeVal == 2) {
                        pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                    }
                }
            }
        }
        handleHorizontalFlights(pricedItineraryObjectArrayList);
        sortByLeastDuration();
        mAdapter.notifyDataSetChanged();
        adapterList.notifyDataSetChanged();
    }

    private void handleFlightsCount() {
        pricedItineraryObjectArrayList.clear();
        for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
            for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                if (bean.getRefNum().equalsIgnoreCase("1")) {
                    pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                }
            }
        }
        if (pricedItineraryObjectArrayList.size() == 0) {
            pricedItineraryObjectArrayList.addAll(tempPricedItineraryArray);
        }
        handleHorizontalFlights(pricedItineraryObjectArrayList);
        sortByLeastDuration();
        mAdapter.notifyDataSetChanged();
        adapterList.notifyDataSetChanged();
    }

    private void handleNonStopFlights() {
        pricedItineraryObjectArrayList.clear();
        for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
            for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                if (bean.getRefNum().equalsIgnoreCase("1")) {
                    int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                    if (stopType == 0) {
                        pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                    }
                }
            }
        }
        if (pricedItineraryObjectArrayList.size() == 0) {
            for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    if (bean.getRefNum().equalsIgnoreCase("1")) {
                        int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                        if (stopType == 1) {
                            pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                        }
                    }
                }
            }
        }
        handleHorizontalFlights(pricedItineraryObjectArrayList);
        sortByLeastDuration();
        mAdapter.notifyDataSetChanged();
        adapterList.notifyDataSetChanged();
    }

    private void setFlightsCountsUI() {
        if (!isTextUpdated && totalCount == 0 && earliestCount == 0 && fastestCount == 0) {
            isTextUpdated = true;
            for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    totalCount++;
                    if (bean.getRefNum().equalsIgnoreCase("1")) {
                        int departureTimeVal = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                        if (departureTimeVal == 2) {
                            earliestCount = earliestCount + 1;
                        }
                    }
                    if (bean.getRefNum().equalsIgnoreCase("1")) {
                        int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                        if (stopType == 0) {
                            fastestCount = fastestCount + 1;
                        }
                    }
                }
            }
            if (fastestCount == 0) {
                for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                    for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                        OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                        if (bean.getRefNum().equalsIgnoreCase("1")) {
                            int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                            if (stopType == 1) {
                                fastestCount = fastestCount + 1;
                            }
                        }
                    }
                }
            }
        }
        if (totalFlights) {
            totalFlightCountTextView.setText(String.valueOf(totalCount));
            totalFlightCountTextView.setTextColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.flight_select_color));
            totalFlightCountTextView.append(Utils.addFlightTextField(getString(R.string.label_flights), FlightForwardResultsActivity.this));
        } else {
            totalFlightCountTextView.setText(String.valueOf(totalCount));
            totalFlightCountTextView.setTextColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_header_label_color));
            totalFlightCountTextView.append(Utils.addFlightHoverTextField(getString(R.string.label_flights), FlightForwardResultsActivity.this));
        }
        if (earliestFlights) {
            flightCountEarlierTextView.setText(String.valueOf(earliestCount));
            flightCountEarlierTextView.setTextColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.flight_select_color));
            flightCountEarlierTextView.append(Utils.addFlightTextField(getString(R.string.label_earliest), FlightForwardResultsActivity.this));
        } else {
            flightCountEarlierTextView.setText(String.valueOf(earliestCount));
            flightCountEarlierTextView.setTextColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_header_label_color));
            flightCountEarlierTextView.append(Utils.addFlightHoverTextField(getString(R.string.label_earliest), FlightForwardResultsActivity.this));
        }
        if (fastestFlights) {
            flightCountFastestTextView.setText(String.valueOf(fastestCount));
            flightCountFastestTextView.setTextColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.flight_select_color));
            flightCountFastestTextView.append(Utils.addFlightTextField(getString(R.string.label_fastest), FlightForwardResultsActivity.this));
        } else {
            flightCountFastestTextView.setText(String.valueOf(fastestCount));
            flightCountFastestTextView.setTextColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_header_label_color));
            flightCountFastestTextView.append(Utils.addFlightHoverTextField(getString(R.string.label_fastest), FlightForwardResultsActivity.this));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FLIGHT_PRICE_FILTER) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    isFilterApplied = true;
                    handleFlightsCount();
                    handleDefaultFlightsUi();
                    pricedItineraryObjectArrayList.clear();
                    minValue = data.getStringExtra(Constants.PRICE_MINIMUM_VALUE);
                    maxValue = data.getStringExtra(Constants.PRICE_MAXIMUM_VALUE);
                    isNonStopChecked = data.getBooleanExtra(Constants.ONE_WAY_NONSTOP_CHECKED, false);
                    multiCityNonStopChecked = data.getBooleanExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, false);
                    double minVal = Double.parseDouble(minValue);
                    double maxVal = Double.parseDouble(maxValue);
                    for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                        double price = Double.parseDouble(tempPricedItineraryArray.get(i).getAipiBean().getItfObject().gettFare());
                        if (minVal <= price && maxVal >= price) {
                            pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                        }
                    }
                    AsyncTaskJson task = new AsyncTaskJson();
                    task.execute();
                    refundStatusArray = Singleton.getInstance().refundStatusArrayList;
                    boolean isRefundSelected = true;
                    boolean isNonRefundSelected = true;
                    if (refundStatusArray.size() == 2) {
                        for (FilterRefundSelectionItem refundVar : refundStatusArray) {
                            if (refundVar.isSelected()) {
                                if (refundVar.getRefundType() == 0) {
                                    isRefundSelected = false;
                                } else if (refundVar.getRefundType() == 1) {
                                    isNonRefundSelected = false;
                                }
                            }
                        }
                    }
                    if (!isRefundSelected && isNonRefundSelected) {
                        for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                            for (int j = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size() - 1; j >= 0; j--) {
                                String refNum = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getRefNum();
                                OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                                if (refNum.equalsIgnoreCase("1")) {
                                    if (!bean.getRs().equalsIgnoreCase("Refundable") && !bean.getRs().equalsIgnoreCase("Refundable Before Departure")) {
                                        Utils.printMessage(TAG, "bean.getRs() :: " + bean.getRs());
                                        pricedItineraryObjectArrayList.remove(i);
                                    }
                                }
                            }
                        }
                    } else if (isRefundSelected && !isNonRefundSelected) {
                        for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                            for (int j = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size() - 1; j >= 0; j--) {
                                String refNum = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getRefNum();
                                OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                                if (refNum.equalsIgnoreCase("1")) {
                                    if (!bean.getRs().equalsIgnoreCase("Non Refundable") && !bean.getRs().equalsIgnoreCase("null")) {
                                        Utils.printMessage(TAG, "bean.getRs() :: " + bean.getRs());
                                        pricedItineraryObjectArrayList.remove(i);
                                    }
                                }
                            }
                        }
                    }
                    Utils.getFlightStopValues(this, tripType, isNonStopChecked, multiCityNonStopChecked, pricedItineraryObjectArrayList);
                    stopsArray = Singleton.getInstance().resultStopsArrayList;
                    boolean isStopsNoneSelected = true;
                    ArrayList<Integer> selectedStopsList = new ArrayList<>();
                    if (stopsArray.size() != 0) {
                        for (FilterStopSelectionItem stopVar : stopsArray) {
                            if (stopVar.isSelected()) {
                                isStopsNoneSelected = false;
                                selectedStopsList.add(stopVar.getStopType());
                            }
                        }
                    }
                    if (!isStopsNoneSelected) {
                        for (FilterStopSelectionItem selectedVar : stopsArray) {
                            if (!selectedVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    OriginDestinationOptionBean odoBean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    ArrayList<Integer> selectedList = new ArrayList<>();
                                    String refNum = odoBean.getRefNum();
                                    String currentStopType = odoBean.getFsDataBeanArrayList().get(0).getSq();
                                    if (refNum.equalsIgnoreCase("1")) {
                                        selectedList.add(Integer.parseInt(currentStopType));
                                    }
                                    if (checkStopsList(selectedStopsList, selectedList).isEmpty()) {
                                        pricedItineraryObjectArrayList.remove(i);
                                    }
                                }
                            }
                        }
                    }
                    airlineArray = Singleton.getInstance().resultAirlinesArrayList;
                    boolean isAirlineNoneSelected = true;
                    ArrayList<String> selectedAirlineList = new ArrayList<>();
                    for (FilterAirlineSelectionItem airlineVar : airlineArray) {
                        if (airlineVar.isSelected()) {
                            isAirlineNoneSelected = false;
                            selectedAirlineList.add(airlineVar.getMal());
                        }
                    }
                    if (!isAirlineNoneSelected) {
                        for (FilterAirlineSelectionItem selectedAirlineVar : airlineArray) {
                            if (!selectedAirlineVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airlineList = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    String refNum = bean.getRefNum();
                                    if (refNum.equalsIgnoreCase("1")) {
                                        airlineList.add(bean.getFsDataBeanArrayList().get(0).getMal());
                                    }
                                    if (checkAirlineList(selectedAirlineList, airlineList).isEmpty()) {
                                        pricedItineraryObjectArrayList.remove(i);
                                    }
                                }
                            }
                        }
                    }
                    if (selectedAirlineList.size() > 0) {
                        for (FlightBean airlineVar : flightsArrayList) {
                            if (selectedAirlineList.contains(airlineVar.getImage())) {
                                airlineVar.setDataCleared(false);
                                airlineVar.setClicked(true);
                            }
                        }
                    }
                    airportListArray = Singleton.getInstance().resultAirportsArrayList;
                    tempArray1 = Singleton.getInstance().firstAirportsArrayList;
                    tempArray2 = Singleton.getInstance().secondAirportsArrayList;
                    tempArray3 = Singleton.getInstance().thirdAirportsArrayList;
                    boolean isNoneSelectedInSection1 = true;
                    boolean isNoneSelectedInSection2 = true;
                    boolean isNoneSelectedInSection3 = true;
                    ArrayList<String> firstAirportList = new ArrayList<>();
                    ArrayList<String> secondAirportList = new ArrayList<>();
                    ArrayList<String> thirdAirportList = new ArrayList<>();
                    for (FilterAirportSelectionItem airportItems : airportListArray) {
                        if (airportItems.isSelected()) {
                            if (airportItems.getAirportType() == 1) {
                                isNoneSelectedInSection1 = false;
                                firstAirportList.add(airportItems.getAirportCode());
                            } else if (airportItems.getAirportType() == 2) {
                                isNoneSelectedInSection2 = false;
                                secondAirportList.add(airportItems.getAirportCode());
                            } else {
                                isNoneSelectedInSection3 = false;
                                thirdAirportList.add(airportItems.getAirportCode());
                            }
                        }
                    }
                    if (!isNoneSelectedInSection1) {
                        for (FilterAirportSelectionItem selectedAirportVar : tempArray1) {
                            if (!selectedAirportVar.isSelected()) {
                                int airportType = selectedAirportVar.getAirportType();
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airportCodeArr = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                        airportCodeArr.add(bean.getFsDataBeanArrayList().get(k).getDap());
                                    }
                                    if (airportType == 1) {
                                        if (checkAirlineList(firstAirportList, airportCodeArr).isEmpty()) {
                                            pricedItineraryObjectArrayList.remove(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!isNoneSelectedInSection2) {
                        for (FilterAirportSelectionItem selectedAirportVar : tempArray2) {
                            if (!selectedAirportVar.isSelected()) {
                                int airportType = selectedAirportVar.getAirportType();
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airportCodeArr = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                        airportCodeArr.add(bean.getFsDataBeanArrayList().get(k).getAap());
                                    }
                                    if (airportType == 2) {
                                        if (checkAirlineList(secondAirportList, airportCodeArr).isEmpty()) {
                                            pricedItineraryObjectArrayList.remove(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!isNoneSelectedInSection3) {
                        for (FilterAirportSelectionItem selectedAirportVar : tempArray3) {
                            if (!selectedAirportVar.isSelected()) {
                                int airportType = selectedAirportVar.getAirportType();
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airportCodeArr = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                        airportCodeArr.add(bean.getFsDataBeanArrayList().get(k).getDap());
                                    }
                                    if (airportType == 3) {
                                        if (checkAirlineList(thirdAirportList, airportCodeArr).isEmpty()) {
                                            pricedItineraryObjectArrayList.remove(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    timeArray = Singleton.getInstance().resultTimeArrayList;
                    dapTimeListArray = Singleton.getInstance().departureTimeArrayList;
                    aapTimeListArray = Singleton.getInstance().arrivalTimeArrayList;
                    boolean dapFlightLayoutSelected = true;
                    boolean aapFlightLayoutSelected = true;
                    ArrayList<Integer> departTimeArr = new ArrayList<>();
                    ArrayList<Integer> arrivalTimeArr = new ArrayList<>();
                    for (int i = 0; i < timeArray.size(); i++) {
                        for (FilterTimeSelectionItem flightTimeSelected : dapTimeListArray) {
                            if (flightTimeSelected.isSelected()) {
                                dapFlightLayoutSelected = false;
                                if (!departTimeArr.contains(flightTimeSelected.getTimeZone())) {
                                    departTimeArr.add(flightTimeSelected.getTimeZone());
                                }
                            }
                        }
                        for (FilterTimeSelectionItem flightTimeSelected : aapTimeListArray) {
                            if (flightTimeSelected.isSelected()) {
                                aapFlightLayoutSelected = false;
                                if (!arrivalTimeArr.contains(flightTimeSelected.getTimeZone())) {
                                    arrivalTimeArr.add(flightTimeSelected.getTimeZone());
                                }
                            }
                        }
                    }
                    if (!dapFlightLayoutSelected) {
                        for (FilterTimeSelectionItem selectedTimeVar : dapTimeListArray) {
                            if (!selectedTimeVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    if (bean.getRefNum().equalsIgnoreCase("1")) {
                                        int dapFlightTime = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                                        if (!departTimeArr.contains(dapFlightTime)) {
                                            pricedItineraryObjectArrayList.remove(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!aapFlightLayoutSelected) {
                        for (FilterTimeSelectionItem selectedTimeVar : aapTimeListArray) {
                            if (!selectedTimeVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    if (bean.getRefNum().equalsIgnoreCase("1")) {
                                        int aapFlightTime = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(bean.getFsDataBeanArrayList().size() - 1).getArdt());
                                        if (!arrivalTimeArr.contains(aapFlightTime)) {
                                            pricedItineraryObjectArrayList.remove(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    tempAirlinePIArrayList.clear();
                    tempAirlinePIArrayList.addAll(pricedItineraryObjectArrayList);
                    Utils.printMessage(TAG, "Apply Button Pi list size :: " + pricedItineraryObjectArrayList.size());
                    if (pricedItineraryObjectArrayList.size() == 0) {
                        handleFilterResults();
                        pricedItineraryObjectArrayList.addAll(tempPricedItineraryArray);
                        displayErrorMessage(getString(R.string.label_no_flights_available));
                        return;
                    }
                    adapterList.notifyDataSetChanged();
                    mAdapter.notifyDataSetChanged();
                }
            }
            if (resultCode == RESULT_CANCELED) {
                if (data != null) {
                    isNonStopChecked = data.getBooleanExtra(Constants.ONE_WAY_NONSTOP_CHECKED, false);
                    multiCityNonStopChecked = data.getBooleanExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, false);
                    pricedItineraryObjectArrayList.clear();
                    pricedItineraryObjectArrayList.addAll(tempPricedItineraryArray);
                    AsyncTaskJson task = new AsyncTaskJson();
                    task.execute();
                    minValue = null;
                    maxValue = null;
                    setFlightsCountsUI();
                    fastestCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_view_bg));
                    earliestCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_view_bg));
                    totalCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.white_color));
                    fastestFlights = false;
                    totalFlights = true;
                    earliestFlights = false;
                    handleFlightsCount();
                    handleDefaultFlightsUi();
                    selectedMal = "";
                    for (FlightBean bean : flightsArrayList) {
                        bean.setClicked(false);
                        bean.setDataCleared(false);
                    }
                    Utils.printMessage(TAG, "Reset Button Pi list size :: " + pricedItineraryObjectArrayList.size());
                    if (pricedItineraryObjectArrayList.size() != 0) {
                        flightListLayout.removeView(noResultView);
                        noResultView = null;
                    }
                    adapterList.notifyDataSetChanged();
                    mAdapter.notifyDataSetChanged();
                }
            }
        } else if (requestCode == SEARCH_FLIGHT_RESULT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    headerViewLayout.removeView(loadingView);
                    loadingView = null;
                }
            } else if (resultCode == Constants.CHANGE_FLIGHT) {
                if (loadingView != null) {
                    headerViewLayout.removeView(loadingView);
                    loadingView = null;
                }
            }
        }
        if (requestCode == RETURN_FLIGHT_RESULT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    headerViewLayout.removeView(loadingView);
                    loadingView = null;
                    Singleton.getInstance().refundStatusArrayList.clear();
                    Singleton.getInstance().resultStopsArrayList.clear();
                    Singleton.getInstance().resultAirlinesArrayList.clear();
                    Singleton.getInstance().resultAirportsArrayList.clear();
                    Singleton.getInstance().firstAirportsArrayList.clear();
                    Singleton.getInstance().secondAirportsArrayList.clear();
                    Singleton.getInstance().thirdAirportsArrayList.clear();
                    Singleton.getInstance().resultTimeArrayList.clear();
                    Singleton.getInstance().departureTimeArrayList.clear();
                    Singleton.getInstance().arrivalTimeArrayList.clear();
                    minValue = null;
                    maxValue = null;
                    isFilterApplied = false;
                }
            }
        }
    }

    private void handleMerchandiseRequest() {
        JSONObject mainJSON = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        try {
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
            mainJSON.put("timeStamp", timeStamp);
            mainJSON.put("cls", selectedClass);
            JSONArray mDataArray = new JSONArray();
            for (int i = 0; i < flightNamesArray.size(); i++) {
                boolean isContains = false;
                for (int j = 0; j < mDataArray.length(); j++) {
                    if (mDataArray.get(j).toString().equalsIgnoreCase(flightNamesArray.get(i).getKey())) {
                        isContains = true;
                        break;
                    }
                }
                if (!isContains) {
                    mDataArray.put(flightNamesArray.get(i).getKey());
                }
            }
            mainJSON.put("mData", mDataArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(FlightForwardResultsActivity.this, FlightForwardResultsActivity.this, Constants.MERCHANDISE_BASE_URL,
                Constants.MERCHANDISE_SEARCH_URI, mainJSON.toString(), GET_MERCHANDISE_DATA, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String appendBodyTypeValue(HashMap<Integer, AmenitiesBean> dataHashMap, int val, String moreData) {
        AmenitiesBean bean = dataHashMap.get(val);
        String strValue = "";
        if (bean != null) {
            strValue = bean.getEn();
            if (isArabicLang) {
                strValue = bean.getAr();
            }
            String resultText = "";
            resultText = moreData + "==" + strValue;
            return bean.getIcon() + "==" + resultText;
        }
        return "";
    }

    private String appendMerchandiseValues(HashMap<String, AmenitiesBean> dataHashMap, String data, String moreData) {
        AmenitiesBean bean = dataHashMap.get(data);
        String strVal = "";
        if (bean != null) {
            strVal = bean.getEn();
            if (isArabicLang) {
                strVal = bean.getAr();
            }
            String resultText = "";
            if (moreData.equalsIgnoreCase("")) {
                resultText = strVal;
            } else {
                resultText = strVal + "==" + moreData;
            }
            return bean.getIcon() + "==" + resultText;
        }
        return "";
    }

    private String checkAmenitiesValue(HashMap<String, AmenitiesBean> dataHashMap, String data) {
        AmenitiesBean bean = dataHashMap.get(data);
        String strValue = "";
        if (bean != null) {
            strValue = bean.getEn();
            if (isArabicLang) {
                strValue = bean.getAr();
            }
        }
        return strValue;
    }

    private String checkBodyTypeAmenitiesValue(HashMap<Integer, AmenitiesBean> dataHashMap, int value) {
        AmenitiesBean bean = dataHashMap.get(value);
        String strValue = "";
        if (bean != null) {
            strValue = bean.getEn();
            if (isArabicLang) {
                strValue = bean.getAr();
            }
        }
        return strValue;
    }

    private boolean merchandiseDataUI(ArrayList<FlightAmenitiesBean> mainMarchandiseArrayList) {
        try {
            Utils.printMessage(TAG, "merchandiseDataUI  stated");
            FSDataBean fsDataBean = new FSDataBean();
            OriginDestinationOptionBean odoObject;
            for (int i = 0; i < mainMarchandiseArrayList.size(); i++) {
                FlightAmenitiesBean flightAmenitiesBean = new FlightAmenitiesBean();
                flightAmenitiesBean = mainMarchandiseArrayList.get(i);
                for (int j = 0; j < tempPricedItineraryArray.size(); j++) {
                    PricedItineraryObject pricedItineraryObject = tempPricedItineraryArray.get(j);
                    for (int l = 0; l < tempPricedItineraryArray.get(j).getAiBean().getOriginDestinationOptionBeanArrayList().size(); l++) {
                        odoObject = pricedItineraryObject.getAiBean().getOriginDestinationOptionBeanArrayList().get(l);
                        for (int k = 0; k < odoObject.getFsDataBeanArrayList().size(); k++) {
                            fsDataBean = odoObject.getFsDataBeanArrayList().get(k);
                            String flightName = fsDataBean.getMal() + "-" + fsDataBean.getEq();
                            String merchFlightName = flightAmenitiesBean.getFlightName();
                            if (flightName.contains(merchFlightName) &&
                                    fsDataBean.getDap().equalsIgnoreCase(flightAmenitiesBean.getDap()) &&
                                    fsDataBean.getAap().equalsIgnoreCase(flightAmenitiesBean.getAap())) {
                                ArrayList<String> arrayList = new ArrayList<>();
                                String aircraftVal[] = flightAmenitiesBean.getFlightName().split("-");
                                String aircraftType = Utils.getAircraftType(aircraftVal[1], FlightForwardResultsActivity.this);
                                if (!flightAmenitiesBean.getBodyType().isEmpty()) {
                                    for (Map.Entry<Integer, AmenitiesBean> entry : AmenitiesConstants.bodyTypeHashMap.entrySet()) {
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getBodyType())) {
                                                arrayList.add(appendBodyTypeValue(AmenitiesConstants.bodyTypeHashMap, entry.getKey(), aircraftType));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getBodyType())) {
                                                arrayList.add(appendBodyTypeValue(AmenitiesConstants.bodyTypeHashMap, entry.getKey(), aircraftType));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getSeatType().isEmpty()) {
                                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.seatTypeHashMap.entrySet()) {
                                        String moreData = "";
                                        String seatWidthText = "";
                                        String seatLengthText = "";
                                        String seatPitchText = "";
                                        if (!flightAmenitiesBean.getSeatWidth().isEmpty() && !flightAmenitiesBean.getSeatLength().isEmpty() && !flightAmenitiesBean.getSeatPitch().isEmpty()) {
                                            seatWidthText = getString(R.string.label_seat_width) + " " + flightAmenitiesBean.getSeatWidth();
                                            seatLengthText = getString(R.string.label_leg_room) + " " + flightAmenitiesBean.getSeatLength();
                                            seatPitchText = getString(R.string.label_pitch_size) + " " + flightAmenitiesBean.getSeatPitch();
                                            moreData = seatWidthText + ", " + seatLengthText + ", " + seatPitchText;
                                        } else if (!flightAmenitiesBean.getSeatWidth().isEmpty() && !flightAmenitiesBean.getSeatPitch().isEmpty()) {
                                            seatWidthText = getString(R.string.label_seat_width) + " " + flightAmenitiesBean.getSeatWidth();
                                            seatPitchText = getString(R.string.label_pitch_size) + " " + flightAmenitiesBean.getSeatPitch();
                                            moreData = seatWidthText + ", " + seatPitchText;
                                        }
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getSeatType())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.seatTypeHashMap, entry.getKey(), moreData));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getSeatType())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.seatTypeHashMap, entry.getKey(), moreData));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getSeatLayout().isEmpty()) {
                                    arrayList.add("seat_layout==" + getString(R.string.label_seat_layout) + " " + flightAmenitiesBean.getSeatLayout());
                                }
                                try {
                                    if (!flightAmenitiesBean.getMeals().isEmpty()) {
                                        for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.mealsHashMap.entrySet()) {
                                            if (isArabicLang) {
                                                if (entry.getValue().getAr().equals(flightAmenitiesBean.getMeals())) {
                                                    arrayList.add(appendMerchandiseValues(AmenitiesConstants.mealsHashMap, entry.getKey(), ""));
                                                }
                                            } else {
                                                if (entry.getValue().getEn().equals(flightAmenitiesBean.getMeals())) {
                                                    arrayList.add(appendMerchandiseValues(AmenitiesConstants.mealsHashMap, entry.getKey(), ""));
                                                }
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.getMessage();
                                }
                                if (!flightAmenitiesBean.getWifi().isEmpty()) {
                                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.wifiHashMap.entrySet()) {
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getWifi())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.wifiHashMap, entry.getKey(), ""));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getWifi())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.wifiHashMap, entry.getKey(), ""));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getAvod().isEmpty()) {
                                    if (flightAmenitiesBean.getAvod().equalsIgnoreCase("Yes")) {
                                        if (!flightAmenitiesBean.getVideoType().isEmpty()) {
                                            for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.avodVideoHashMap.entrySet()) {
                                                String screenSize = "";
                                                if (!flightAmenitiesBean.getAvodScreenSize().isEmpty()) {
                                                    screenSize = flightAmenitiesBean.getAvodScreenSize();
                                                }
                                                if (isArabicLang) {
                                                    if (entry.getValue().getAr().equals(flightAmenitiesBean.getVideoType())) {
                                                        arrayList.add(appendMerchandiseValues(AmenitiesConstants.avodVideoHashMap, entry.getKey(), screenSize));
                                                    }
                                                } else {
                                                    if (entry.getValue().getEn().equals(flightAmenitiesBean.getVideoType())) {
                                                        arrayList.add(appendMerchandiseValues(AmenitiesConstants.avodVideoHashMap, entry.getKey(), screenSize));
                                                    }
                                                }
                                            }
                                        } else {
                                            arrayList.add("avod==" + getString(R.string.label_avod));
                                        }
                                    }
                                    if (flightAmenitiesBean.getAvod().equalsIgnoreCase("Charge")) {
                                        if (!flightAmenitiesBean.getVideoType().isEmpty()) {
                                            for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.avodChargeVideoHashMap.entrySet()) {
                                                String screenSize = "";
                                                if (!flightAmenitiesBean.getAvodScreenSize().isEmpty()) {
                                                    screenSize = flightAmenitiesBean.getAvodScreenSize();
                                                }
                                                if (isArabicLang) {
                                                    if (entry.getValue().getAr().equals(flightAmenitiesBean.getVideoType())) {
                                                        arrayList.add(appendMerchandiseValues(AmenitiesConstants.avodChargeVideoHashMap, entry.getKey(), screenSize));
                                                    }
                                                } else {
                                                    if (entry.getValue().getEn().equals(flightAmenitiesBean.getVideoType())) {
                                                        arrayList.add(appendMerchandiseValues(AmenitiesConstants.avodChargeVideoHashMap, entry.getKey(), screenSize));
                                                    }
                                                }
                                            }
                                        } else {
                                            arrayList.add("avod==" + getString(R.string.label_avod) + " (" + getString(R.string.label_charge_text) + ")");
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getPowerType().isEmpty()) {
                                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.powerTypeHashMap.entrySet()) {
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getPowerType())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.powerTypeHashMap, entry.getKey(), ""));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getPowerType())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.powerTypeHashMap, entry.getKey(), ""));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getMobile().isEmpty()) {
                                    arrayList.add(appendMerchandiseValues(AmenitiesConstants.mobileHashMap, flightAmenitiesBean.getMobile(), ""));
                                }
                                if (!flightAmenitiesBean.getInfantCare().isEmpty()) {
                                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.infantCareHashMap.entrySet()) {
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getInfantCare())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.infantCareHashMap, entry.getKey(), ""));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getInfantCare())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.infantCareHashMap, entry.getKey(), ""));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getAlocohol().isEmpty()) {
                                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.alocoholHashMap.entrySet()) {
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getAlocohol())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.alocoholHashMap, entry.getKey(), ""));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getAlocohol())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.alocoholHashMap, entry.getKey(), ""));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getPrayerArea().isEmpty()) {
                                    arrayList.add(appendMerchandiseValues(AmenitiesConstants.prayerAreaHashMap, flightAmenitiesBean.getPrayerArea(), ""));
                                }
                                if (!flightAmenitiesBean.getShowerArea().isEmpty()) {
                                    arrayList.add(appendMerchandiseValues(AmenitiesConstants.showerAreaHashMap, flightAmenitiesBean.getShowerArea(), ""));
                                }
                                if (!flightAmenitiesBean.getRemarks().isEmpty()) {
                                    arrayList.add("==" + flightAmenitiesBean.getRemarks());
                                }
                                if (!flightAmenitiesBean.getProvideTransportation().isEmpty()) {
                                    arrayList.add("==" + getString(R.string.label_provide_transportation));
                                }
                                if (!flightAmenitiesBean.getProvideOtherInfo().isEmpty()) {
                                    arrayList.add("==" + flightAmenitiesBean.getProvideOtherInfo());
                                }
                                fsDataBean.setFlightAmenitiesBean(flightAmenitiesBean);
                                fsDataBean.setIconNameArrayList(arrayList);
                            }
                        }
                    }
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void updateStopsFlights(ArrayList<FilterStopSelectionItem> stopsArray, boolean isNonStopChecked) {
        if (isNonStopChecked) {
            for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                OriginDestinationOptionBean odoBean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                String currentStopType = odoBean.getFsDataBeanArrayList().get(0).getSq();
                if (currentStopType.equalsIgnoreCase("null")) {
                    pricedItineraryObjectArrayList.remove(i);
                } else if (!currentStopType.equalsIgnoreCase("0")) {
                    pricedItineraryObjectArrayList.remove(i);
                }
            }
            if (pricedItineraryObjectArrayList.size() == 0) {
                //displayErrorMessage("Non-stop Flights are not available");
                pricedItineraryObjectArrayList.addAll(tempPricedItineraryArray);
            }
        } else {
            boolean isStopsNoneSelected = true;
            ArrayList<Integer> selectedStopsList = new ArrayList<>();
            if (stopsArray.size() != 0) {
                for (FilterStopSelectionItem stopVar : stopsArray) {
                    if (stopVar.isSelected()) {
                        isStopsNoneSelected = false;
                        selectedStopsList.add(stopVar.getStopType());
                    }
                }
            }
            if (!isStopsNoneSelected) {
                for (FilterStopSelectionItem selectedVar : stopsArray) {
                    if (!selectedVar.isSelected()) {
                        for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                            OriginDestinationOptionBean odoBean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                            ArrayList<Integer> selectedList = new ArrayList<>();
                            String refNum = odoBean.getRefNum();
                            String currentStopType = odoBean.getFsDataBeanArrayList().get(0).getSq();
                            if (refNum.equalsIgnoreCase("1")) {
                                selectedList.add(Integer.parseInt(currentStopType));
                            }
                            if (checkStopsList(selectedStopsList, selectedList).isEmpty()) {
                                pricedItineraryObjectArrayList.remove(i);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        if (!flightsArrayList.get(position).getImage().isEmpty()) {
            selectedAirlineItem(position);
        }
    }

    @Override
    public void onSelectedItemClick(int position, int type) {
        if (tripType.equalsIgnoreCase("1")) {
            handleCheckAvailabilityRQ(position);
        } else {
            Singleton.getInstance().selectedPriceItinerarayObject.clear();
            Singleton.getInstance().selectedMerchandiseObject = mainMarchArrayList;
            String selectedRphNumber = pricedItineraryObjectArrayList.get(position).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getRph();
            String selectedFlightPrice = pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().gettFare();
            String mainRphNumber = "";
            ArrayList<PricedItineraryObject> selectedPiArr = new ArrayList<>();
            for (int i = 0; i < mainPricedItineraryArray.size(); i++) {
                for (int j = 0; j < mainPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean bean = mainPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    mainRphNumber = bean.getRph();
                    if (mainRphNumber.equalsIgnoreCase(selectedRphNumber)) {
                        selectedPiArr.add(mainPricedItineraryArray.get(i));
                    }
                }
            }
            if (selectedPiArr.size() > 0) {
                Singleton.getInstance().selectedPriceItinerarayObject = selectedPiArr;
                Intent intent = new Intent(FlightForwardResultsActivity.this, FlightReturnResultsActivity.class);
                intent.putExtra(Constants.TRIP_TYPE, tripType);
                intent.putExtra(Constants.SELECTED_CLASS_TYPE, selectedClassType);
                intent.putExtra(Constants.ONE_WAY_NONSTOP_CHECKED, isNonStopChecked);
                intent.putExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, multiCityNonStopChecked);
                intent.putExtra(Constants.FORWARD_SOURCE, source);
                intent.putExtra(Constants.FORWARD_SOURCE_NAME, sourceName);
                intent.putExtra(Constants.FORWARD_DESTINATION, destination);
                intent.putExtra(Constants.RETURN_SOURCE_NAME, returnName);
                intent.putExtra(Constants.FORWARD_DETAILS, journeyDetails);
                intent.putExtra(Constants.FORWARD_FLIGHT_DATE, forwardFlightDate);
                intent.putExtra(Constants.RETURN_FLIGHT_DATE, returnFlightDate);
                intent.putExtra(Constants.TOTAL_PASSENGERS_COUNT, travellerCount);
                intent.putExtra(Constants.AIRLINE_NAME_HASHMAP, airLineNamesMap);
                intent.putExtra(Constants.AIRPORT_NAME_HASHMAP, airportNamesMap);
                intent.putExtra(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                intent.putExtra(Constants.SELECTED_RPH_NUMBER, selectedRphNumber);
                intent.putExtra(Constants.SELECTED_FLIGHT_PRICE, selectedFlightPrice);
                startActivityForResult(intent, RETURN_FLIGHT_RESULT);
            }
        }
    }

    @Override
    public void dataResponse(String message, String callType) {
        if (message.equalsIgnoreCase("Success")) {
            Utils.printMessage(TAG, "Executing this");
            getRequestFromServer(requestJSON);
            if (tripType.equalsIgnoreCase("2"))
                getFareRequestFromServer(requestJSON);
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(regularFace);
            errorDescriptionText.setTypeface(regularFace);
            searchButton.setTypeface(regularFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(FlightForwardResultsActivity.this);
                    if (isInternetPresent) {
                        headerViewLayout.removeView(errorView);
                        getRequestFromServer(requestJSON);
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            headerViewLayout.addView(errorView);
        }
    }

    private class AsyncTaskJson extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            merchandiseDataUI(mainMarchArrayList);
            Utils.printMessage(TAG, "merchandiseDataUI  completed");
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            closeLoading();
            mAdapter.notifyDataSetChanged();
            adapterList.notifyDataSetChanged();
        }
    }

    private void handleCheckAvailabilityRQ(int position) {
        final String requestJsonData = handleFareRQRequest(position);
        Utils.printMessage(TAG, "requestJsonData : " + requestJsonData);
        if (isInternetPresent) {
            showLoading();
            HTTPAsync async = new HTTPAsync(FlightForwardResultsActivity.this, FlightForwardResultsActivity.this, Constants.FARE_RQ_URL,
                    "", requestJsonData, GET_FARE_RULE_RESULTS, HTTPAsync.METHOD_POST);
            async.execute();
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(regularFace);
            errorDescriptionText.setTypeface(regularFace);
            searchButton.setTypeface(regularFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(FlightForwardResultsActivity.this);
                    if (isInternetPresent) {
                        headerViewLayout.removeView(errorView);
                        showLoading();
                        HTTPAsync async = new HTTPAsync(FlightForwardResultsActivity.this, FlightForwardResultsActivity.this,
                                Constants.FARE_RQ_URL, "", requestJsonData, GET_FARE_RULE_RESULTS, HTTPAsync.METHOD_POST);
                        async.execute();
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            headerViewLayout.addView(errorView);
        }
    }

    private String handleFareRQRequest(int position) {
        JSONObject finalObj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        try {
            JSONObject mainJSON = new JSONObject();
            JSONObject sourceObj = new JSONObject();
            sourceObj = Utils.getAppSettingData(sourceObj, FlightForwardResultsActivity.this);
            sourceObj.put("isoCurrency", "SAR");
            sourceObj.put("echoToken", randomNumber);
            sourceObj.put("timeStamp", timeStamp);
            sourceObj.put("clientId", Constants.CLIENT_ID);
            sourceObj.put("clientSecret", Constants.CLIENT_SECRET);
            sourceObj.put("accessToken", Utils.getRequestAccessToken(FlightForwardResultsActivity.this));
            sourceObj.put("groupType", groupType);
            mainJSON.put("source", sourceObj);
            JSONArray arr = new JSONArray();
            if (tripType.equalsIgnoreCase("1")) {
                arr.put(createFSObject(pricedItineraryObjectArrayList.get(position).getAiBean().getOriginDestinationOptionBeanArrayList().get(0)));
            }
            JSONObject aiJson = new JSONObject();
            aiJson.put("dirId", pricedItineraryObjectArrayList.get(position).getAiBean().getDirId());
            JSONObject odoArrObj = new JSONObject();
            odoArrObj.put("odo", arr);
            aiJson.put("odos", odoArrObj);
            JSONObject pfbdObj = new JSONObject();
            pfbdObj.put("pfbd", buildPFBDArray(position));
            JSONObject itfObj = new JSONObject();
            itfObj.put("bf", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().getBf()));
            itfObj.put("tTx", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().gettTx()));
            itfObj.put("tSF", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().gettSF()));
            itfObj.put("disc", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().getDisc()));
            itfObj.put("tFare", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().gettFare()));
            itfObj.put("ic", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().getIc()));
            itfObj.put("usfc", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getItfObject().getUsfc()));
            JSONObject aipiObject = new JSONObject();
            aipiObject.put("itf", itfObj);
            aipiObject.put("pfbds", pfbdObj);
            aipiObject.put("fq", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getFq()));
            mainJSON.put("extra", "");
            mainJSON.put("source", sourceObj);
            mainJSON.put("ai", aiJson);
            mainJSON.put("aipi", aipiObject);
            mainJSON.put("ptq", Singleton.getInstance().ptqArr);
            aipiJSONObject = aipiObject.toString();
            aiJSONObject = aiJson.toString();
            finalObj.put("fareruleRQ", mainJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalObj.toString();
    }

    private JSONObject createFSObject(OriginDestinationOptionBean odoBean) {
        JSONArray array = new JSONArray();
        for (int i = 0; i < odoBean.getFsDataBeanArrayList().size(); i++) {
            FSDataBean dataBean = odoBean.getFsDataBeanArrayList().get(i);
            JSONObject detailsObj = new JSONObject();
            try {
                detailsObj.put("dap", Utils.checkStringValue(dataBean.getDap()));
                detailsObj.put("dt", Utils.checkStringValue(dataBean.getDt()));
                detailsObj.put("aap", Utils.checkStringValue(dataBean.getAap()));
                detailsObj.put("at", Utils.checkStringValue(dataBean.getAt()));
                detailsObj.put("oal", Utils.checkStringValue(dataBean.getOal()));
                detailsObj.put("mal", Utils.checkStringValue(dataBean.getMal()));
                detailsObj.put("eq", Utils.checkStringValue(dataBean.getEq()));
                detailsObj.put("ct", Utils.checkStringValue(dataBean.getCt()));
                detailsObj.put("dur", Utils.checkStringValue(dataBean.getDur()));
                detailsObj.put("fdur", Utils.checkStringValue(dataBean.getFdur()));
                detailsObj.put("st", Utils.checkStringValue(dataBean.getSt()));
                detailsObj.put("si", Utils.checkStringValue(dataBean.getSi()));
                detailsObj.put("ti", Utils.checkStringValue(dataBean.getTi()));
                detailsObj.put("fi", Utils.checkStringValue(dataBean.getFi()));
                detailsObj.put("comment", Utils.checkStringValue(dataBean.getComment()));
                detailsObj.put("as", Utils.checkStringValue(dataBean.getAs()));
                detailsObj.put("fc", Utils.checkStringValue(dataBean.getFc()));
                detailsObj.put("tofi", Utils.checkStringValue(dataBean.getTofi()));
                detailsObj.put("fn", Utils.checkStringValue(dataBean.getFn()));
                detailsObj.put("sq", Utils.checkStringValue(dataBean.getSq()));
                detailsObj.put("is", Utils.checkStringValue(dataBean.getIs()));
                detailsObj.put("ardt", Utils.checkStringValue(dataBean.getArdt()));
                detailsObj.put("ddt", Utils.checkStringValue(dataBean.getDdt()));
                array.put(detailsObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        JSONObject odoObj = new JSONObject();
        try {
            odoObj.put("refNum", Utils.checkStringValue(odoBean.getRefNum()));
            odoObj.put("rph", Utils.checkStringValue(odoBean.getRph()));
            odoObj.put("mOfcId", Utils.checkStringValue(odoBean.getmOfcId()));
            odoObj.put("fs", array);
            odoObj.put("tt", Utils.checkStringValue(odoBean.getTt()));
            odoObj.put("rs", Utils.checkStringValue(odoBean.getRs()));
            odoObj.put("cb", Utils.checkStringValue(odoBean.getCb()));
            odoObj.put("pt", Utils.checkStringValue(odoBean.getPt()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return odoObj;
    }

    private JSONArray buildPFBDArray(int position) {
        JSONArray pfbdArray = new JSONArray();
        for (PFBDObjectBean pfbdObjBean : pricedItineraryObjectArrayList.get(position).getAipiBean().getPfbdObjectBeanArrayList()) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("pType", Utils.checkStringValue(pfbdObjBean.getpType()));
                obj.put("qty", Utils.checkStringValue(pfbdObjBean.getQty()));
                obj.put("usfcp", Utils.checkStringValue(pfbdObjBean.getUsfcp()));
                JSONArray array = new JSONArray();
                if (pfbdObjBean.getFbcArray() != null) {
                    for (String str : pfbdObjBean.getFbcArray()) {
                        array.put(str);
                    }
                }
                obj.put("fbc", array);
                JSONObject pfObj = new JSONObject();
                pfObj.put("pbf", pfbdObjBean.getPassengerFareObjectBean().getPbf());
                pfObj.put("ptFare", pfbdObjBean.getPassengerFareObjectBean().getPtFare());
                JSONArray txArray = new JSONArray();
                for (TaxObjectBean tBean : pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList()) {
                    JSONObject tObj = new JSONObject();
                    tObj.put("amt", Utils.checkStringValue(tBean.getAmt()));
                    tObj.put("tc", Utils.checkStringValue(tBean.getTc()));
                    txArray.put(tObj);
                }
                pfObj.put("txs", txArray);
                obj.put("pf", pfObj);
                pfbdArray.put(obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return pfbdArray;
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private void displayResultError() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View errorView = inflater.inflate(R.layout.view_error_response, null);
        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
        errorText = (TextView) errorView.findViewById(R.id.error_text);
        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
        searchButton = (Button) errorView.findViewById(R.id.search_button);
        errorText.setTypeface(regularFace);
        errorDescriptionText.setTypeface(regularFace);
        searchButton.setTypeface(regularFace);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loadErrorType(Constants.RESULT_ERROR);
        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        headerViewLayout.addView(errorView);
    }

    private ArrayList<String> checkAirlineList(ArrayList<String> selectedList, ArrayList<String> beanList) {
        List<String> firstList = new ArrayList<>(selectedList);
        List<String> secondList = new ArrayList<>(beanList);
        ArrayList<String> resultList = new ArrayList<>(secondList);
        resultList.retainAll(firstList);
        return resultList;
    }

    private ArrayList<Integer> checkStopsList(ArrayList<Integer> selectedList, ArrayList<Integer> beanList) {
        List<Integer> firstList = new ArrayList<>(selectedList);
        List<Integer> secondList = new ArrayList<>(beanList);
        ArrayList<Integer> resultList = new ArrayList<>(secondList);
        resultList.retainAll(firstList);
        return resultList;
    }

    private void handleDefaultFlightsUi() {
        totalCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.white_color));
        earliestCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_view_bg));
        fastestCountLayout.setBackgroundColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_view_bg));
        totalFlightCountTextView.setTextColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.result_compo_duration));
        flightCountEarlierTextView.setTextColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_header_label_color));
        flightCountFastestTextView.setTextColor(Utils.getColor(FlightForwardResultsActivity.this, R.color.multi_header_label_color));
        totalFlightCountTextView.setText(String.valueOf(totalCount));
        totalFlightCountTextView.append(Utils.addFlightTextField(getString(R.string.label_flights), FlightForwardResultsActivity.this));
        flightCountEarlierTextView.setText(String.valueOf(earliestCount));
        flightCountEarlierTextView.append(Utils.addFlightHoverTextField(getString(R.string.label_earliest), FlightForwardResultsActivity.this));
        flightCountFastestTextView.setText(String.valueOf(fastestCount));
        flightCountFastestTextView.append(Utils.addFlightHoverTextField(getString(R.string.label_fastest), FlightForwardResultsActivity.this));
    }

    private void selectedAirlineItem(int position) {
//        if(isFilterApplied){
//            for (FlightBean bean : flightsArrayList) {
//                bean.setClicked(false);
//                bean.setDataCleared(false);
//            }
//        }
        selectedMal = "";
        FlightBean flightBean = flightsArrayList.get(position);
        boolean isStatus = flightsArrayList.get(position).isClicked();
        if (isStatus) {
            Utils.printMessage(TAG, "Clicked PI size : " + pricedItineraryObjectArrayList.size());
            flightBean.setClicked(false);
            flightBean.setDataCleared(false);
            if (totalFlights) {
                pricedItineraryObjectArrayList.clear();
                pricedItineraryObjectArrayList.addAll(tempPricedItineraryArray);
                handleHorizontalAirlineResults();
            } else if (earliestFlights) {
                pricedItineraryObjectArrayList.clear();
                for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                    ArrayList<String> fareValArr = new ArrayList<>();
                    for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                        OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                        if (bean.getRefNum().equalsIgnoreCase("1")) {
                            if (!fareValArr.contains(tempPricedItineraryArray.get(i).getAipiBean().getItfObject().gettFare())) {
                                fareValArr.add(tempPricedItineraryArray.get(i).getAipiBean().getItfObject().gettFare());
                                int departureTimeVal = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                                if (departureTimeVal == 2) {
                                    pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                                }
                            }
                        }
                    }
                }
            } else if (fastestFlights) {
                pricedItineraryObjectArrayList.clear();
                for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                    ArrayList<String> fareValArr = new ArrayList<>();
                    for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                        OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                        if (bean.getRefNum().equalsIgnoreCase("1")) {
                            if (!fareValArr.contains(tempPricedItineraryArray.get(i).getAipiBean().getItfObject().gettFare())) {
                                fareValArr.add(tempPricedItineraryArray.get(i).getAipiBean().getItfObject().gettFare());
                                int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                                if (stopType == 0) {
                                    pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                                }
                            }
                        }
                    }
                }
                if (pricedItineraryObjectArrayList.size() == 0) {
                    for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                        ArrayList<String> fareValArr = new ArrayList<>();
                        if (flightBean.getImage().equalsIgnoreCase(tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFsDataBeanArrayList().get(0).getMal())) {
                            OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                            if (bean.getRefNum().equalsIgnoreCase("1")) {
                                if (!fareValArr.contains(tempPricedItineraryArray.get(i).getAipiBean().getItfObject().gettFare())) {
                                    fareValArr.add(tempPricedItineraryArray.get(i).getAipiBean().getItfObject().gettFare());
                                    int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                                    if (stopType == 1) {
                                        pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            for (FlightBean bean : flightsArrayList) {
                bean.setClicked(false);
                bean.setDataCleared(false);
            }
            selectedMal = flightsArrayList.get(position).getImage();
            Utils.getSelectedAirlineValues(pricedItineraryObjectArrayList, selectedMal, true);
            flightBean.setClicked(true);
            Utils.printMessage(TAG, "Un Clicked PI size : " + pricedItineraryObjectArrayList.size());
            if (totalFlights) {
                pricedItineraryObjectArrayList.clear();
                if (isFilterApplied) {
                    pricedItineraryObjectArrayList.addAll(tempAirlinePIArrayList);
                } else {
                    pricedItineraryObjectArrayList.addAll(tempPricedItineraryArray);
                }
                handleHorizontalAirlineResults();
                if (pricedItineraryObjectArrayList.size() == 0) {
                    flightBean.setDataCleared(true);
                    displayErrorMessage(getString(R.string.label_no_flights_available));
                    handleFilterResults();
                    pricedItineraryObjectArrayList.addAll(tempPricedItineraryArray);
                }
            } else if (earliestFlights) {
                pricedItineraryObjectArrayList.clear();
                for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                    if (flightBean.getImage().equalsIgnoreCase(tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFsDataBeanArrayList().get(0).getMal())) {
                        OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                        if (bean.getRefNum().equalsIgnoreCase("1")) {
                            int departureTimeVal = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                            if (departureTimeVal == 2) {
                                pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            }
                        }
                    }
                }
            } else if (fastestFlights) {
                pricedItineraryObjectArrayList.clear();
                for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                    if (flightBean.getImage().equalsIgnoreCase(tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFsDataBeanArrayList().get(0).getMal())) {
                        OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                        if (bean.getRefNum().equalsIgnoreCase("1")) {
                            int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                            if (stopType == 0) {
                                pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            }
                        }
                    }
                }
                if (pricedItineraryObjectArrayList.size() == 0) {
                    for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                        if (flightBean.getImage().equalsIgnoreCase(tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFsDataBeanArrayList().get(0).getMal())) {
                            OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                            if (bean.getRefNum().equalsIgnoreCase("1")) {
                                int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                                if (stopType == 1) {
                                    pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                                }
                            }
                        }
                    }
                }
            }
        }
        mAdapter.notifyDataSetChanged();
        adapterList.notifyDataSetChanged();
    }

    private void handleFilterResults() {
        if (isFilterApplied) {
            Singleton.getInstance().refundStatusArrayList.clear();
            Singleton.getInstance().resultStopsArrayList.clear();
            Singleton.getInstance().resultAirlinesArrayList.clear();
            Singleton.getInstance().resultAirportsArrayList.clear();
            Singleton.getInstance().firstAirportsArrayList.clear();
            Singleton.getInstance().secondAirportsArrayList.clear();
            Singleton.getInstance().thirdAirportsArrayList.clear();
            Singleton.getInstance().resultTimeArrayList.clear();
            Singleton.getInstance().departureTimeArrayList.clear();
            Singleton.getInstance().arrivalTimeArrayList.clear();
            minValue = null;
            maxValue = null;
            isFilterApplied = false;
            isNonStopChecked = false;
            selectedMal = "";
            for (FlightBean bean : flightsArrayList) {
                bean.setClicked(false);
                bean.setDataCleared(false);
            }
        }
    }

    private void handleHorizontalAirlineResults() {
        boolean isAirlineNoneSelected = true;
        ArrayList<String> selectedAirlineList = new ArrayList<>();
        for (FlightBean airlineVar : flightsArrayList) {
            if (airlineVar.isClicked()) {
                isAirlineNoneSelected = false;
                selectedAirlineList.add(airlineVar.getImage());
            }
        }
        if (!isAirlineNoneSelected) {
            for (FlightBean selectedAirlineVar : flightsArrayList) {
                if (!selectedAirlineVar.isClicked()) {
                    for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                        ArrayList<String> airlineList = new ArrayList<>();
                        OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                        String refNum = bean.getRefNum();
                        if (refNum.equalsIgnoreCase("1")) {
                            airlineList.add(bean.getFsDataBeanArrayList().get(0).getMal());
                        }
                        if (checkAirlineList(selectedAirlineList, airlineList).isEmpty()) {
                            pricedItineraryObjectArrayList.remove(i);
                        }
                    }
                }
            }
        }
    }

    private void sortByLeastDuration() {
        if (pricedItineraryObjectArrayList.size() > 0) {
            Collections.sort(pricedItineraryObjectArrayList, new Comparator<PricedItineraryObject>() {
                @Override
                public int compare(PricedItineraryObject e1, PricedItineraryObject e2) {
                    int resultVal = 0;
                    String id1 = e1.getAipiBean().getItfObject().gettFare();
                    String id2 = e2.getAipiBean().getItfObject().gettFare();
                    Double price1 = 0.0;
                    Double price2 = 0.0;
                    try {
                        price1 = Double.parseDouble(id1);
                        price2 = Double.parseDouble(id2);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    int result = price1.compareTo(price2);
                    if (result > 0) {
                        resultVal = result;
                    } else if (result < 0) {
                        resultVal = result;
                    } else {
                        Long firstDur = Long.valueOf(e1.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getSegmentDepartureTime());
                        Long secondDur = Long.valueOf(e2.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getSegmentDepartureTime());
                        int durResult = firstDur.compareTo(secondDur);
                        if (durResult > 0) {
                            resultVal = durResult;
                        } else if (durResult < 0) {
                            resultVal = durResult;
                        } else {
                            Long firstFlight = e1.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFlightDuration();
                            Long secondFlight = e2.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFlightDuration();
                            resultVal = firstFlight.compareTo(secondFlight);
                        }
                    }
                    return resultVal;
                }
            });
        }
    }

    private void getTokenFromServer() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(FlightForwardResultsActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(FlightForwardResultsActivity.this, FlightForwardResultsActivity.this,
                Constants.CLIENT_AUTHENTICATION, "", obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isAppReLaunched) {
            Utils.printMessage(TAG, "App Successfully Re Launched");
            isAppReLaunched = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Utils.isAppIsInBackground(FlightForwardResultsActivity.this)) {
            isAppReLaunched = true;
        }
    }
}