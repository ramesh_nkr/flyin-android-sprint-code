package com.flyin.bookings;


import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

public class CountryActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskListener {
    private ImageView globalImage = null;
    private ImageView egyptImage = null;
    private LinearLayout globalLayout = null;
    private LinearLayout egyptLayout = null;
    private SharedPreferences pref = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontLight = Constants.FONT_ROBOTO_LIGHT;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        TextView globalTextView = (TextView) findViewById(R.id.global_text);
        TextView globalLinkText = (TextView) findViewById(R.id.global_link_text);
        TextView egyptTextView = (TextView) findViewById(R.id.egypt_text);
        TextView egyptLinkText = (TextView) findViewById(R.id.egypt_link_text);
        globalImage = (ImageView) findViewById(R.id.globe_image);
        egyptImage = (ImageView) findViewById(R.id.egypt_image);
        globalLayout = (LinearLayout) findViewById(R.id.global_layout);
        globalLayout.setOnClickListener(this);
        egyptLayout = (LinearLayout) findViewById(R.id.egypt_layout);
        egyptLayout.setOnClickListener(this);
        if (Utils.isArabicLangSelected(CountryActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontLight = Constants.FONT_ROBOTO_LIGHT;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface lightFace = Typeface.createFromAsset(getAssets(), fontLight);
        Typeface titleFace = Typeface.createFromAsset(getAssets(), fontText);
        globalTextView.setTypeface(lightFace);
        globalLinkText.setTypeface(lightFace);
        egyptTextView.setTypeface(lightFace);
        egyptLinkText.setTypeface(lightFace);
        pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        Editor ed = pref.edit();
        if (pref.getString(Constants.ISCOUNTRY_GLOBAL, "").isEmpty()) {
            globalImage.setVisibility(View.VISIBLE);
            egyptImage.setVisibility(View.GONE);
            ed.putString(Constants.ISCOUNTRY_GLOBAL, "GL").apply();
        } else if (pref.getString(Constants.ISCOUNTRY_GLOBAL, "").equalsIgnoreCase("GL")) {
            globalImage.setVisibility(View.VISIBLE);
        } else if (pref.getString(Constants.ISCOUNTRY_GLOBAL, "").equalsIgnoreCase("EG")) {
            egyptImage.setVisibility(View.VISIBLE);
        }
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_country);
        mTitleText.setTypeface(tf);
        backText.setTypeface(titleFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(CountryActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    @Override
    public void onClick(View v) {
        Editor ed = pref.edit();
        if (v == globalLayout) {
            globalImage.setVisibility(View.VISIBLE);
            egyptImage.setVisibility(View.GONE);
            ed.putString(Constants.ISCOUNTRY_GLOBAL, "GL").apply();
        } else if (v == egyptLayout) {
            globalImage.setVisibility(View.GONE);
            egyptImage.setVisibility(View.VISIBLE);
            ed.putString(Constants.ISCOUNTRY_GLOBAL, "EG").apply();
        }
        finish();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
    }
}
