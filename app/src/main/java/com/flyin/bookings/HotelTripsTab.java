package com.flyin.bookings;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.adapter.HotelTripsAdapter;
import com.flyin.bookings.listeners.CancelHotelBookingListener;
import com.flyin.bookings.model.BookingsObject;
import com.flyin.bookings.model.CancellationPolicyBean;
import com.flyin.bookings.model.EntityDetailsObjectBean;
import com.flyin.bookings.model.GeneralDetailsObjectBean;
import com.flyin.bookings.model.HotelDataBean;
import com.flyin.bookings.model.HotelDetailsObjectBean;
import com.flyin.bookings.model.HotelMoreInfoObject;
import com.flyin.bookings.model.PassengerDetailsObjectBean;
import com.flyin.bookings.model.PassengersBean;
import com.flyin.bookings.model.PersonalObjectBean;
import com.flyin.bookings.model.PriceBean;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

@SuppressWarnings("deprecation")
public class HotelTripsTab extends Fragment implements AsyncTaskListener, CancelHotelBookingListener {
    private ListView hotelListView;
    private HotelTripsAdapter hotelTripsAdapter;
    private ArrayList<BookingsObject> tempHotelBookingsArray = new ArrayList<>();
    private ArrayList<BookingsObject> hotelBookingMainArray = new ArrayList<>();
    private String selectedLang;
    private LayoutInflater inflater;
    private RelativeLayout loadingViewLayout;
    private View loadingView = null;
    private Typeface textFace;
    private String requestTripDataJSON = "";
    private static final int GET_HOTEL_TRIP_DETAILS = 2;
    private static final int GET_HOTEL_INFORMATION = 3;
    private static final int GET_CANCEL_POLICY = 4;
    private static final String TAG = "HotelTripsTab";
    private boolean isInternetPresent = false;
    private ImageView errorImage;
    private Button searchButton;
    private TextView errorText, errorDescriptionText;
    private MyReceiver myReceiver;
    private IntentFilter intentFilter;
    private Activity mActivity;
    private ProgressDialog barProgressDialog = null;
    private static final int PAYMENT_ACTIVITY = 5;

    public HotelTripsTab() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myReceiver = new MyReceiver();
        intentFilter = new IntentFilter("com.flyin.bookings.SEARCH_HOTELS");
        mActivity.registerReceiver(myReceiver, intentFilter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_hotel_trips, container, false);
        isInternetPresent = Utils.isConnectingToInternet(mActivity);
        loadingViewLayout = (RelativeLayout) view.findViewById(R.id.loading_view_layout);
        selectedLang = Constants.LANGUAGE_ENGLISH_CODE;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(mActivity)) {
            selectedLang = Constants.LANGUAGE_ARABIC_CODE;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        textFace = Typeface.createFromAsset(mActivity.getAssets(), fontText);
        hotelListView = (ListView) view.findViewById(R.id.hotel_trip_list);
        hotelListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
                Singleton.getInstance().selectedTripFlightObject = tempHotelBookingsArray.get(position);
                Intent intent = new Intent(mActivity, HotelTripDetailsActivity.class);
                startActivity(intent);
            }
        });
        if (isInternetPresent) {
            String requestTripJSON = tripDetailsTask();
            getTripDetailsRequestFromServer(requestTripJSON);
        } else {
            checkInternetConn();
        }
        return view;
    }

    @Override
    public void onSelectedItemClick(int position, int type, String value) {
        if (type == 0) {
            serviceCall(tempHotelBookingsArray.get(position), value);
        } else if (type == 1) {
            Singleton.getInstance().selectedTripFlightObject = tempHotelBookingsArray.get(position);
            Intent intent = new Intent(mActivity, MyTripsPaymentActivity.class);
            startActivityForResult(intent, PAYMENT_ACTIVITY);
        }
    }

    @Override
    public void onSelectedListItem(int position, String value, String hotelId) {

    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.SEARCH_HOTELS")) {
                if (hotelBookingMainArray.size() == 0) {
                    return;
                } else {
                    if (intent.getStringExtra("message") != null) {
                        handleSearch(intent.getStringExtra("message"));
                    }
                }
            }
        }
    }

    private void handleSearch(String searchText) {
        tempHotelBookingsArray.clear();
        if (searchText.isEmpty()) {
            tempHotelBookingsArray.addAll(hotelBookingMainArray);
        } else {
            try{
                for (BookingsObject bookings : hotelBookingMainArray) {
                    if (bookings.getGeneralDetailsObjectBean().getFlyinCode() != null) {
                        String flyinCode = bookings.getGeneralDetailsObjectBean().getFlyinCode();
                        if (!flyinCode.equalsIgnoreCase("null") || !flyinCode.equalsIgnoreCase("")) {
                            if (flyinCode.contains(searchText.toUpperCase())) {
                                tempHotelBookingsArray.add(bookings);
                                continue;
                            }
                        }
                    }
                    if (bookings.getEntityDetailsObjectBean().getHotelMoreInfo().getName() != null) {
                        String hotelName = bookings.getEntityDetailsObjectBean().getHotelMoreInfo().getName();
                        if (!hotelName.equalsIgnoreCase("null") || !hotelName.equalsIgnoreCase("")) {
                            String hotelText = searchText.substring(0, 1).toUpperCase() + searchText.substring(1);
                            if (hotelName.contains(hotelText)) {
                                tempHotelBookingsArray.add(bookings);
                                continue;
                            }
                        }
                    }

                    if (bookings.getEntityDetailsObjectBean().getHotelMoreInfo().getCity() != null) {
                        String cityName = bookings.getEntityDetailsObjectBean().getHotelMoreInfo().getCity();
                        if (!cityName.equalsIgnoreCase("null") || !cityName.equalsIgnoreCase("")) {
                            String cityText = searchText.substring(0, 1).toUpperCase() + searchText.substring(1);
                            if (cityName.contains(cityText)) {
                                tempHotelBookingsArray.add(bookings);
                                continue;
                            }
                        }
                    }
                }
            }catch (Exception e){
                Utils.printMessage(TAG, "Error : "+e.getMessage());
            }
        }
        hotelTripsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.unregisterReceiver(myReceiver);
    }

    private String tripDetailsTask() {
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(mActivity));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(mActivity));
            mainJSON.put("source", sourceJSON);
//            mainJSON.put("customerId", pref.getString(Constants.MEMBER_EMAIL, ""));
//            mainJSON.put("fromDate", "");
//            mainJSON.put("toDate", "");
//            mainJSON.put("referenceNo", "");
//            mainJSON.put("referenceType", Utils.checkStringValue("null"));
            mainJSON.put("searchType", "all_bookings");
            mainJSON.put("recordType", "Only Hotel");
            mainJSON.put("userUniqueId", pref.getString(Constants.USER_UNIQUE_ID, ""));
        } catch (Exception e) {
        }
        return mainJSON.toString();
    }

    public void getTripDetailsRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(mActivity, this, Constants.MY_TRIPS_URL, "", requestJSON,
                GET_HOTEL_TRIP_DETAILS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String hotelDetailsTask() {
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject hcpJSON = new JSONObject();
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("mandatorId", Constants.HOTEL_MANDATORY_ID);
            sourceJSON.put("timeStamp", timeStamp);
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(mActivity));
            hcpJSON.put("source", sourceJSON);
            JSONObject crtJSON = new JSONObject();
            crtJSON.put("hcid", "");
            crtJSON.put("language", selectedLang);
            JSONArray uniqueIDArray = new JSONArray();
            for (int i = 0; i < tempHotelBookingsArray.size(); i++) {
                if (tempHotelBookingsArray.get(i).getEntityDetailsObjectBean().getHotelDetailsBean() != null) {
                    String hotelId = tempHotelBookingsArray.get(i).getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getHotelUniueId();
                    if (!hotelId.isEmpty()) {
                        Boolean isAvailable = false;
                        for (int j = 0; j < uniqueIDArray.length(); j++) {
                            if (uniqueIDArray.getString(j).equalsIgnoreCase(hotelId)) {
                                isAvailable = true;
                                break;
                            }
                        }
                        if (!isAvailable) {
                            uniqueIDArray.put(hotelId);
                        }
                    }
                }
            }
            crtJSON.put("huid", uniqueIDArray);
            crtJSON.put("pt", "B|F");
            hcpJSON.put("crt", crtJSON);
            mainJSON.put("hcp", hcpJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void getHotelInfoRequestFromServer(String requestJSON) {
        HTTPAsync async = new HTTPAsync(mActivity, this, Constants.HOTEL_INFO_RQ_URL, "", requestJSON,
                GET_HOTEL_INFORMATION, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        if (serviceType == GET_HOTEL_TRIP_DETAILS) {
            Utils.printMessage(TAG, "Hotels DATA:: " + data);
            int maxLogSize = 1000;
            for (int i = 0; i <= data.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = end > data.length() ? data.length() : end;
                Log.v(TAG, data.substring(start, end));
            }
            if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
                inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = inflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(textFace);
                errorDescriptionText.setTypeface(textFace);
                searchButton.setTypeface(textFace);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadingViewLayout.removeView(errorView);
                        mActivity.finish();
                    }
                });
                loadErrorType(Constants.WRONG_ERROR_PAGE);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            } else {
                JSONObject obj = null;
                try {
                    obj = new JSONObject(data);
                    if (obj.has("bookingDetails")) {
                        if (obj.getJSONObject("bookingDetails").has("bookings")) {
                            JSONArray bookingsArr = obj.getJSONObject("bookingDetails").getJSONArray("bookings");
                            if (bookingsArr.length() > 0) {
                                ParseJSONTask task = new ParseJSONTask();
                                task.execute(data);
                            } else {
                                inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View errorView = inflater.inflate(R.layout.view_error_response, null);
                                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                                errorText = (TextView) errorView.findViewById(R.id.error_text);
                                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                                searchButton = (Button) errorView.findViewById(R.id.search_button);
                                errorText.setTypeface(textFace);
                                errorDescriptionText.setVisibility(View.GONE);
                                searchButton.setVisibility(View.GONE);
                                loadErrorType(Constants.RESULT_ERROR);
                                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT,
                                        ViewGroup.LayoutParams.MATCH_PARENT));
                                loadingViewLayout.addView(errorView);
                            }
                        }
                    } else if (obj.has("accessToken")) {
                        String accessToken = obj.getString("accessToken");
                        if (accessToken.equalsIgnoreCase("null")) {
                            closeLoading();
                        }
                        if (obj.has("status")) {
                            String statusType = obj.getString("status");
                            if (statusType.equalsIgnoreCase("FAILURE")) {
                                inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View errorView = inflater.inflate(R.layout.view_error_response, null);
                                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                                errorText = (TextView) errorView.findViewById(R.id.error_text);
                                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                                searchButton = (Button) errorView.findViewById(R.id.search_button);
                                errorText.setTypeface(textFace);
                                errorDescriptionText.setTypeface(textFace);
                                searchButton.setTypeface(textFace);
                                searchButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        loadingViewLayout.removeView(errorView);
                                        mActivity.finish();
                                    }
                                });
                                loadErrorType(Constants.WRONG_ERROR_PAGE);
                                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT,
                                        ViewGroup.LayoutParams.MATCH_PARENT));
                                loadingViewLayout.addView(errorView);
                            }
                        }
                    } else {
                        closeLoading();
                        inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View errorView = inflater.inflate(R.layout.view_error_response, null);
                        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                        errorText = (TextView) errorView.findViewById(R.id.error_text);
                        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                        searchButton = (Button) errorView.findViewById(R.id.search_button);
                        errorText.setTypeface(textFace);
                        errorDescriptionText.setTypeface(textFace);
                        searchButton.setTypeface(textFace);
                        searchButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadingViewLayout.removeView(errorView);
                                mActivity.finish();
                            }
                        });
                        loadErrorType(Constants.WRONG_ERROR_PAGE);
                        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        loadingViewLayout.addView(errorView);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (serviceType == GET_HOTEL_INFORMATION) {
            Utils.printMessage(TAG, "Hotels Information DATA:: " + data);
            JSONArray obj = null;
            try {
                obj = new JSONArray(data);
                for (int i = 0; i < tempHotelBookingsArray.size(); i++) {
                    BookingsObject bookings = tempHotelBookingsArray.get(i);
                    if (bookings.getEntityDetailsObjectBean().getHotelDetailsBean() != null) {
                        String uniqueId = bookings.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getHotelUniueId();
                        if (!uniqueId.isEmpty()) {
                            for (int j = 0; j < obj.length(); j++) {
                                if (uniqueId.equalsIgnoreCase(obj.getJSONObject(j).getString("huid"))) {
                                    if (obj.getJSONObject(j).has("hotel")) {
                                        String imagePath = Utils.checkStringVal(obj.getJSONObject(j).getJSONObject("hotel").getJSONObject("basicInfo").getString("mainImage"));
                                        String rating = Utils.checkStringVal(obj.getJSONObject(j).getJSONObject("hotel").getJSONObject("basicInfo").getString("starRating"));
                                        String address = Utils.checkStringVal(obj.getJSONObject(j).getJSONObject("hotel").getJSONObject("basicInfo").getString("addressText"));
                                        String name = Utils.checkStringVal(obj.getJSONObject(j).getJSONObject("hotel").getJSONObject("basicInfo").getString("name"));
                                        String city = Utils.checkStringVal(obj.getJSONObject(j).getJSONObject("hotel").getJSONObject("basicInfo").getJSONObject("city").getString("name"));
                                        String cancellationPolicy = Utils.checkStringVal(obj.getJSONObject(j).getJSONObject("hotel").getJSONObject("policy").getString("cancellationPolicy"));
                                        String checkInTime = Utils.checkStringVal(obj.getJSONObject(j).getJSONObject("hotel").getJSONObject("policy").getString("check_in"));
                                        String checkOutTime = Utils.checkStringVal(obj.getJSONObject(j).getJSONObject("hotel").getJSONObject("policy").getString("check_out"));
                                        HotelMoreInfoObject moreInfoObject = new HotelMoreInfoObject();
                                        moreInfoObject.setImagePath(imagePath);
                                        moreInfoObject.setRating(rating);
                                        moreInfoObject.setAddress(address);
                                        moreInfoObject.setName(name);
                                        moreInfoObject.setCity(city);
                                        moreInfoObject.setCancellationPolicy(cancellationPolicy);
                                        moreInfoObject.setCheckInTime(checkInTime);
                                        moreInfoObject.setCheckOutTime(checkOutTime);
                                        moreInfoObject.setHotelUniqueId(Utils.checkStringVal(obj.getJSONObject(j).getString("huid")));
                                        tempHotelBookingsArray.get(i).getEntityDetailsObjectBean().setHotelMoreInfo(moreInfoObject);
                                        hotelBookingMainArray.get(i).getEntityDetailsObjectBean().setHotelMoreInfo(moreInfoObject);
                                        break;
                                    } else {
                                        HotelMoreInfoObject moreInfoObject = new HotelMoreInfoObject();
                                        moreInfoObject.setImagePath("");
                                        moreInfoObject.setRating("");
                                        moreInfoObject.setAddress("");
                                        moreInfoObject.setName("");
                                        moreInfoObject.setCity("");
                                        moreInfoObject.setCancellationPolicy("");
                                        moreInfoObject.setCheckInTime("");
                                        moreInfoObject.setCheckOutTime("");
                                        moreInfoObject.setHotelUniqueId("");
                                        tempHotelBookingsArray.get(i).getEntityDetailsObjectBean().setHotelMoreInfo(moreInfoObject);
                                        hotelBookingMainArray.get(i).getEntityDetailsObjectBean().setHotelMoreInfo(moreInfoObject);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                hotelTripsAdapter = new HotelTripsAdapter(mActivity, tempHotelBookingsArray, HotelTripsTab.this);
                hotelListView.setAdapter(hotelTripsAdapter);
                hotelTripsAdapter.notifyDataSetChanged();
                closeLoading();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.printMessage(TAG, "Exception is .. " + e.getMessage());
            }
        } else if (serviceType == GET_CANCEL_POLICY) {
            Utils.printMessage(TAG, "HOTEL_BOOKING_CANCELLATION  :: " + data);
            JSONObject obj = null;
            try {
                obj = new JSONObject(data);
                if (obj.has("status")) {
                    if (obj.getString("status").equalsIgnoreCase("FAILURE")) {
                        String errorr = obj.getJSONArray("msg").getString(0);
                        //        String val2 = obj.getJSONArray("msg").getString(1);
                        //          String val3 = obj.getJSONArray("msg").getString(2);
                        ((MyTripsActivity) mActivity).displayErrorMessage(errorr, false);
                        closeProgress();
                    } else if (obj.getString("status").equalsIgnoreCase("SUCCESS")) {
                        closeProgress();
                        String requestTripJSON = tripDetailsTask();
                        getTripDetailsRequestFromServer(requestTripJSON);
                        ((MyTripsActivity) mActivity).displayErrorMessage("SUCCESS", true);
                    }
                } else if (obj.has("accessToken")) {
                    String accessToken = obj.getString("accessToken");
                    if (accessToken.equalsIgnoreCase("null")) {
                        closeProgress();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            closeProgress();
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(textFace);
        descriptionText.setVisibility(View.GONE);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingViewLayout.removeAllViews();
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        try {
            loadingViewLayout.removeView(loadingView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(mActivity.getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_network_error_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(mActivity.getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(mActivity.getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(mActivity.getResources().getString(R.string.label_no_results_found_message));
                searchButton.setText(mActivity.getResources().getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private class ParseJSONTask extends AsyncTask<String, Void, String> {
        JSONObject obj = null;

        @Override
        protected String doInBackground(String... urls) {
            try {
                obj = new JSONObject(urls[0]);
                if (obj.has("bookingDetails")) {
                    if (obj.getJSONObject("bookingDetails").has("bookings")) {
                        JSONArray bookingsArr = obj.getJSONObject("bookingDetails").getJSONArray("bookings");
                        if (bookingsArr.length() > 0) {
                            hotelBookingMainArray.clear();
                            tempHotelBookingsArray.clear();
                            ArrayList<BookingsObject> tempBookingsArr = new ArrayList<>();
                            for (int i = 0; i < bookingsArr.length(); i++) {
                                BookingsObject bookingsObject = new BookingsObject();
                                if (bookingsArr.getJSONObject(i).has("entityDetails")) {
                                    EntityDetailsObjectBean entityDetailsObjectBean = new EntityDetailsObjectBean();
                                    if (bookingsArr.getJSONObject(i).getJSONObject("entityDetails").has("hotelDetails")) {
                                        String data = bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getString("hotelDetails");
                                        Object json = new JSONTokener(data).nextValue();
                                        if (json instanceof JSONObject) {
                                            JSONObject hotelMainJsonObj = bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("hotelDetails");
                                            if (hotelMainJsonObj != null) {
                                                JSONObject hotelJsonObj = hotelMainJsonObj.getJSONObject("hotel");
                                                HotelDetailsObjectBean hotelDetailsObjectBean = new HotelDetailsObjectBean();
                                                HotelDataBean hotelsDataBean = new HotelDataBean();
                                                hotelsDataBean.setReferenceNo(Utils.checkStringVal(hotelJsonObj.getString("referenceNo")));
                                                hotelsDataBean.setName(Utils.checkStringVal(hotelJsonObj.getString("name")));
                                                hotelsDataBean.setRoomType(Utils.checkStringVal(hotelJsonObj.getString("roomType")));
                                                hotelsDataBean.setBoardBasis(Utils.checkStringVal(hotelJsonObj.getString("boardBasis")));
                                                hotelsDataBean.setReservationType(Utils.checkStringVal(hotelJsonObj.getString("reservationType")));
                                                hotelsDataBean.setAddress(Utils.checkStringVal(hotelJsonObj.getString("address")));
                                                hotelsDataBean.setCity(Utils.checkStringVal(hotelJsonObj.getString("city")));
                                                hotelsDataBean.setPhoneNo(Utils.checkStringVal(hotelJsonObj.getString("phoneNo")));
                                                hotelsDataBean.setEmail(Utils.checkStringVal(hotelJsonObj.getString("email")));
                                                hotelsDataBean.setCheckInDate(Utils.checkStringVal(hotelJsonObj.getString("checkInDate")));
                                                hotelsDataBean.setCheckOutDate(Utils.checkStringVal(hotelJsonObj.getString("checkOutDate")));
                                                hotelsDataBean.setNights(Utils.checkStringVal(hotelJsonObj.getString("nights")));
                                                hotelsDataBean.setRooms(Utils.checkStringVal(hotelJsonObj.getString("rooms")));
                                                hotelsDataBean.setHotelReservationNumber(Utils.checkStringVal(hotelJsonObj.getString("hotelReservationNumber")));
                                                if (hotelJsonObj.has("freeCancellationDate"))
                                                    hotelsDataBean.setFreeCancellationDate(Utils.checkStringVal(hotelJsonObj.getString("freeCancellationDate")));
                                                hotelsDataBean.setHotelUniueId(Utils.checkStringVal(hotelJsonObj.getString("hotelUniueId")));
                                                hotelDetailsObjectBean.setHotelDataBean(hotelsDataBean);
                                                if (bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("hotelDetails").has("cancellationPolicy")) {
                                                    CancellationPolicyBean cancellationPolicyBean = new CancellationPolicyBean();
                                                    ArrayList<String> notesArray = new ArrayList<>();
                                                    for (int j = 0; j < bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("hotelDetails").getJSONObject("cancellationPolicy").getJSONArray("notes").length(); j++) {
                                                        notesArray.add(Utils.checkStringVal(bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getJSONObject("hotelDetails").getJSONObject("cancellationPolicy").getJSONArray("notes").getString(j)));
                                                    }
                                                    cancellationPolicyBean.setNotesArray(notesArray);
                                                    hotelDetailsObjectBean.setCancellationPolicyObjectBean(cancellationPolicyBean);
                                                }
                                                entityDetailsObjectBean.setHotelDetailsBean(hotelDetailsObjectBean);
                                                bookingsObject.setHotelCheckInDate(Utils.dateFormatToHotelMinutesFormat(hotelDetailsObjectBean.getHotelDataBean().getCheckInDate()));
                                            }
                                        } else if (json instanceof String) {
                                            entityDetailsObjectBean.setHotelDetailsBean(null);
                                        }
                                    }
                                    if (bookingsArr.getJSONObject(i).getJSONObject("entityDetails").has("flightDetails")) {
                                        entityDetailsObjectBean.setFlightDetails(Utils.checkStringVal(bookingsArr.getJSONObject(i).getJSONObject("entityDetails").getString("flightDetails")));
                                    }
                                    bookingsObject.setEntityDetailsObjectBean(entityDetailsObjectBean);
                                }
                                if (bookingsArr.getJSONObject(i).has("generalDetails")) {
                                    GeneralDetailsObjectBean generalDetailsObjectBean = new GeneralDetailsObjectBean();
                                    generalDetailsObjectBean.setBookingStatus(Utils.checkStringVal(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("bookingStatus")));
                                    generalDetailsObjectBean.setFlyinCode(Utils.checkStringVal(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("flyinCode")));
                                    generalDetailsObjectBean.setBookingDate(Utils.checkStringVal(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("bookingDate")));
                                    generalDetailsObjectBean.setCustomerId(Utils.checkStringVal(bookingsArr.getJSONObject(i).getJSONObject("generalDetails").getString("customerId")));
                                    bookingsObject.setGeneralDetailsObjectBean(generalDetailsObjectBean);
                                }
                                if (bookingsArr.getJSONObject(i).has("passengerDetails")) {
                                    PassengerDetailsObjectBean passengerDetailsObjectBean = new PassengerDetailsObjectBean();
                                    if (bookingsArr.getJSONObject(i).getJSONObject("passengerDetails").has("passengers")) {
                                        JSONArray passengerArray = bookingsArr.getJSONObject(i).getJSONObject("passengerDetails").getJSONArray("passengers");
                                        ArrayList<PassengersBean> passengersBeanArrayList = new ArrayList<>();
                                        for (int j = 0; j < passengerArray.length(); j++) {
                                            PassengersBean passengersBean = new PassengersBean();
                                            passengersBean.setMealPreference(Utils.checkStringVal(passengerArray.getJSONObject(j).getString("mealPreference")));
                                            passengersBean.setSeatPreference(Utils.checkStringVal(passengerArray.getJSONObject(j).getString("seatPreference")));
                                            passengersBean.setSpecialPreference(Utils.checkStringVal(passengerArray.getJSONObject(j).getString("specialPreference")));
                                            if (passengerArray.getJSONObject(j).has("personalDetails")) {
                                                PersonalObjectBean personalDetailsObjectBeans = new PersonalObjectBean();
                                                personalDetailsObjectBeans.setTitle(Utils.checkStringVal(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("title")));
                                                personalDetailsObjectBeans.setFirstName(Utils.checkStringVal(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("firstName")));
                                                personalDetailsObjectBeans.setMiddleName(Utils.checkStringVal(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("middleName")));
                                                personalDetailsObjectBeans.setLastName(Utils.checkStringVal(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("lastName")));
                                                personalDetailsObjectBeans.setTicketNo(Utils.checkStringVal(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("ticketNo")));
                                                personalDetailsObjectBeans.setIsLeadPax(Utils.checkStringVal(passengerArray.getJSONObject(j).getJSONObject("personalDetails").getString("isLeadPax")));
                                                passengersBean.setPersonalDetailsObjectBean(personalDetailsObjectBeans);
                                            }
                                            passengersBeanArrayList.add(passengersBean);
                                            passengerDetailsObjectBean.setPassengersBeanArrayList(passengersBeanArrayList);
                                        }
                                        bookingsObject.setPassengerDetailsObjectBean(passengerDetailsObjectBean);
                                    }
                                }
                                if (bookingsArr.getJSONObject(i).has("priceDetails")) {
                                    PriceBean priceBean = new PriceBean();
                                    if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").has("price"))
                                        priceBean.setTotal(Utils.checkStringVal(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("total")));
                                    priceBean.setTax(Utils.checkStringVal(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("tax")));
                                    priceBean.setBaseFare(Utils.checkStringVal(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("baseFare")));
                                    priceBean.setUserCurrency(Utils.checkStringVal(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("userCurrency")));
                                    try {
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("cancellationCharge"))
                                            priceBean.setCancellationCharge(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("cancellationCharge"));
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("reIssueCharge"))
                                            priceBean.setReIssueCharge(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("reIssueCharge"));
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("extraBaggageAmount"))
                                            priceBean.setExtraBaggageAmount(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("extraBaggageAmount"));
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("couponDiscount"))
                                            priceBean.setCouponDiscount(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("couponDiscount"));
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("disountPrice"))
                                            priceBean.setDisountPrice(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("disountPrice"));
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("qitafAmount"))
                                            priceBean.setQitafAmount(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getString("qitafAmount"));
                                        if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").has("loyaltydetails")) {
                                            if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").has("earnpoint")) {
                                                priceBean.setEarnPoints(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").getString("earnpoint"));
                                            }
                                            if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").has("redeempoint")) {
                                                priceBean.setRedeemPoint(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").getString("redeempoint"));
                                            }
                                            if (bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").has("rdmdiscountusr")) {
                                                priceBean.setRdmDiscountusr(bookingsArr.getJSONObject(i).getJSONObject("priceDetails").getJSONObject("price").getJSONObject("loyaltydetails").getString("rdmdiscountusr"));
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.getMessage();
                                    }
                                    bookingsObject.setPriceBean(priceBean);
                                }
//                                if(!bookingsObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getHotelUniueId().isEmpty()){
//                                    tempBookingsArr.add(bookingsObject);
//                                }
                                tempBookingsArr.add(bookingsObject);
                            }
                            Collections.sort(tempBookingsArr, new Comparator<BookingsObject>() {
                                @Override
                                public int compare(BookingsObject e1, BookingsObject e2) {
                                    Long id1 = e1.getHotelCheckInDate();
                                    Long id2 = e2.getHotelCheckInDate();
                                    return id2.compareTo(id1);
                                }
                            });
                            hotelBookingMainArray.addAll(tempBookingsArr);
                            tempHotelBookingsArray.addAll(tempBookingsArr);
                        }
                    }
                }
                requestTripDataJSON = hotelDetailsTask();
                getHotelInfoRequestFromServer(requestTripDataJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            closeLoading();
        }
    }


    private void showProgress() {
        if (barProgressDialog == null) {
            barProgressDialog = new ProgressDialog(mActivity);
            barProgressDialog.setMessage(mActivity.getString(R.string.label_just_a_moment));
            barProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            barProgressDialog.setCancelable(false);
            barProgressDialog.show();
        }
    }

    private void closeProgress() {
        if (barProgressDialog != null) {
            barProgressDialog.dismiss();
            barProgressDialog = null;
        }
    }

    private void serviceCall(BookingsObject bookingsObject, String remark) {
        showProgress();
        try {
            String paxName = null;
            if (Integer.parseInt(bookingsObject.getPassengerDetailsObjectBean().getPassengersBeanArrayList().get(0).getPersonalDetailsObjectBean().getIsLeadPax()) == 1) {
                paxName = bookingsObject.getPassengerDetailsObjectBean().getPassengersBeanArrayList().get(0).getPersonalDetailsObjectBean().getFirstName() + " " + bookingsObject.getPassengerDetailsObjectBean().getPassengersBeanArrayList().get(0).getPersonalDetailsObjectBean().getLastName();
            } else {
                paxName = bookingsObject.getPassengerDetailsObjectBean().getPassengersBeanArrayList().get(0).getPersonalDetailsObjectBean().getFirstName() + " " + bookingsObject.getPassengerDetailsObjectBean().getPassengersBeanArrayList().get(0).getPersonalDetailsObjectBean().getLastName();
            }
            String json = makeJson(bookingsObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getReferenceNo(), paxName, remark);
            Utils.printMessage(TAG, "cancellation json data : " + json);
            new HTTPAsync(mActivity, HotelTripsTab.this, Constants.CANCEL_BOOKING, "", json,
                    GET_CANCEL_POLICY, HTTPAsync.METHOD_POST).execute();
        } catch (Exception e) {
            closeProgress();
        }
    }

    private String makeJson(String booking_id, String leadPaxName, String remark) {
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.HOTEL_DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(mActivity));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(mActivity));
            mainJSON.put("source", sourceJSON);
            mainJSON.put("bookingType", "Hotel");
            mainJSON.put("taskType", "Cancellation");
            mainJSON.put("bookingId", booking_id);
            mainJSON.put("registerCustomerId", pref.getString(Constants.MEMBER_EMAIL, ""));
            mainJSON.put("leadPaxName", leadPaxName);
            mainJSON.put("bnpl", "true");
            mainJSON.put("cnclremark", remark);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYMENT_ACTIVITY) {
            if (resultCode == mActivity.RESULT_OK) {
                if (data != null) {
                    isInternetPresent = Utils.isConnectingToInternet(mActivity);
                    if (isInternetPresent) {
                        String requestTripJSON = tripDetailsTask();
                        getTripDetailsRequestFromServer(requestTripJSON);
                    } else {
                        checkInternetConn();
                    }
                }
            }
        }
    }

    private void checkInternetConn() {
        inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View errorView = inflater.inflate(R.layout.view_error_response, null);
        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
        errorText = (TextView) errorView.findViewById(R.id.error_text);
        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
        searchButton = (Button) errorView.findViewById(R.id.search_button);
        errorText.setTypeface(textFace);
        errorDescriptionText.setTypeface(textFace);
        searchButton.setTypeface(textFace);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetPresent = Utils.isConnectingToInternet(mActivity);
                if (isInternetPresent) {
                    loadingViewLayout.removeView(errorView);
                    String requestTripJSON = tripDetailsTask();
                    getTripDetailsRequestFromServer(requestTripJSON);
                }
            }
        });
        loadErrorType(Constants.NETWORK_ERROR);
        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(errorView);
    }
}
