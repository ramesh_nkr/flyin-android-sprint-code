package com.flyin.bookings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("ALL")
public class ClaimPointsFragment extends Fragment implements AsyncTaskListener {
    private Typeface tf, textFace, boldFace;
    private Activity mActivity;
    private static final int GET_CLAIM_POINTS = 1;
    private LayoutInflater inflater = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private ImageView errorImage = null;
    private Button searchButton = null;
    private RelativeLayout loadingViewLayout = null;
    private View loadingView = null;
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private EditText bookingRefEditText;
    private static final String TAG = "ClaimPointsFragment";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rewards_claim_points_fragment, container, false);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        TextView rewardClaimHeaderText = (TextView) view.findViewById(R.id.rewards_calim_header_textview);
        bookingRefEditText = (EditText) view.findViewById(R.id.search_airport_view);
        Button submitButton = (Button) view.findViewById(R.id.submit_button);
        TextView rewardClaimTermsHeader = (TextView) view.findViewById(R.id.rewards_calim_terms_textview);
        TextView termsFirstDataText = (TextView) view.findViewById(R.id.terms_data_one);
        TextView termsSecondDataText = (TextView) view.findViewById(R.id.terms_data_two);
        TextView termsThirdDataText = (TextView) view.findViewById(R.id.terms_data_three);
        TextView termsFourthDataText = (TextView) view.findViewById(R.id.terms_data_four);
        loadingViewLayout = (RelativeLayout) view.findViewById(R.id.loading_view_layout);
        if (Utils.isArabicLangSelected(mActivity)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_DROIDKUFI_BOLD;
            rewardClaimHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            submitButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            rewardClaimTermsHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            termsFirstDataText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            termsSecondDataText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            termsThirdDataText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            termsFourthDataText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            view.findViewById(R.id.selectIcon_one).setRotation(180);
            view.findViewById(R.id.selectIcon_two).setRotation(180);
            view.findViewById(R.id.selectIcon_three).setRotation(180);
            view.findViewById(R.id.selectIcon_four).setRotation(180);
        }
        tf = Typeface.createFromAsset(mActivity.getAssets(), fontPath);
        textFace = Typeface.createFromAsset(mActivity.getAssets(), fontText);
        boldFace = Typeface.createFromAsset(mActivity.getAssets(), fontBold);
        rewardClaimHeaderText.setTypeface(textFace);
        bookingRefEditText.setTypeface(textFace);
        submitButton.setTypeface(textFace);
        rewardClaimTermsHeader.setTypeface(textFace);
        termsFirstDataText.setTypeface(textFace);
        termsSecondDataText.setTypeface(textFace);
        termsThirdDataText.setTypeface(textFace);
        termsFourthDataText.setTypeface(textFace);
        errorView = (RelativeLayout) view.findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) view.findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(textFace);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bookingIdNumer = bookingRefEditText.getText().toString().trim();
                if (bookingIdNumer.length() >= 7 && bookingIdNumer.length() <= 10) {
                    handleClaimPointsService();
                    View keyboard = mActivity.getCurrentFocus();
                    if (keyboard != null) {
                        InputMethodManager passportKeyboard = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        passportKeyboard.hideSoftInputFromWindow(keyboard.getWindowToken(), 0);
                    }
                } else if (bookingIdNumer.length() == 0) {
                    displayErrorMessage(getString(R.string.label_enter_booking_alert));
                    errorView.setBackgroundResource(R.color.error_message_background);
                } else {
                    displayErrorMessage(getString(R.string.label_check_booking_alert));
                    errorView.setBackgroundResource(R.color.error_message_background);
                }
            }
        });
        return view;
    }

    private void handleClaimPointsService() {
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        JSONObject obj = new JSONObject();
        try {
            obj.put("apikey", Constants.MERCHANDISE_API_KEY);
            obj.put("userid", pref.getString(Constants.USER_UNIQUE_ID, ""));
            obj.put("bookingCode", bookingRefEditText.getText().toString().toUpperCase());
        } catch (Exception e) {
            e.printStackTrace();
        }
        showLoading();
        HTTPAsync async = new HTTPAsync(mActivity, ClaimPointsFragment.this, Constants.LOYALTY_CLAIM_POINTS,
                "", obj.toString(), GET_CLAIM_POINTS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        JSONObject obj = null;
        closeLoading();
        Utils.printMessage(TAG, "Response :: " + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            someThingWentWrong();
        } else {
            if (serviceType == GET_CLAIM_POINTS) {
                try {
                    obj = new JSONObject(data);
                    if (obj.has("status")) {
                        String status = obj.getString("status");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            displayErrorMessage(mActivity.getString(R.string.label_email_sent_success_message));
                            errorView.setBackgroundResource(R.color.success_message_background);
                            bookingRefEditText.setText("");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(textFace);
        descriptionText.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(mActivity)) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private void someThingWentWrong() {
        inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View errorView = inflater.inflate(R.layout.view_error_response, null);
        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
        errorText = (TextView) errorView.findViewById(R.id.error_text);
        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
        searchButton = (Button) errorView.findViewById(R.id.search_button);
        errorText.setTypeface(textFace);
        errorDescriptionText.setTypeface(textFace);
        searchButton.setTypeface(textFace);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isConnectingToInternet(mActivity)) {
                    loadingViewLayout.removeView(errorView);
                    mActivity.finish();
                }
            }
        });
        loadErrorType(Constants.WRONG_ERROR_PAGE);
        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(errorView);
    }
}
