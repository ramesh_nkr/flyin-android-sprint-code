package com.flyin.bookings;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.adapter.PassengerInfoAdapter;
import com.flyin.bookings.dialog.SignInDialog;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.listeners.TravellerItemNameListener;
import com.flyin.bookings.listeners.TravellerItemPassportListener;
import com.flyin.bookings.listeners.TravellerItemUpdateListener;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.model.TravellerDetailsBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;
import com.webengage.sdk.android.User;
import com.webengage.sdk.android.WebEngage;

import net.time4j.android.ApplicationStarter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class FlightBookingPassengerActivity extends AppCompatActivity implements AsyncTaskListener, OnCustomItemSelectListener,
        TravellerItemUpdateListener, TravellerItemNameListener, TravellerItemPassportListener {
    private TextView personName = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private Button searchButton = null;
    private RelativeLayout loadingViewLayout = null;
    private ArrayList<TravellerDetailsBean> travellersList = null;
    private ArrayList<TravellerDetailsBean> travellerArrayList = new ArrayList<>();
    private Typeface titleFace = null;
    private static final String TAG = "TripSummaryPassengerActivity";
    private String randomNumber = "";
    private String aipiJSONObject = "";
    private String aiJSONObject = "";
    private String tinfJSONObject = "";
    private String bookRQJSONObject = "";
    private String userName = "";
    private String userFullName = "";
    private String loggedUser = "";
    private String userEmail = "";
    private String userMobile = "";
    private int adultCount = 0;
    private int childCount = 0;
    private int infantCount = 0;
    private String airlineName = "";
    private SharedPreferences preferences;
    private String sourceName = "";
    private String destinationName = "";
    private String secondSource = "";
    private String secondDestination = "";
    private String thirdSource = "";
    private String thirdDestination = "";
    private String forwardFlightDate = "";
    private String returnFlightDate = "";
    private String firstFlightDate = "";
    private String secondFlightDate = "";
    private String thirdFlightDate = "";
    private int tripType = 0;
    private boolean isPassportRequired = false;
    private String tdr = "";
    private String hold = "";
    private String usctp = "";
    private String baggageDetails = "";
    private String cabinType = "";
    private ProgressDialog barProgressDialog = null;
    private static final int GET_FETCH_RESULT = 1;
    private static final int GET_SEARCH_RESPONSE = 2;
    private String requestJsonData = "";
    private boolean isUserSignIn = false;
    private LayoutInflater inflater = null;
    private View loadingView = null;
    private static final int BOOK_FLIGHT = 3;
    private boolean isInternetPresent = false;
    private ImageView errorImage = null;
    private String currentDate = "";
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private MyReceiver myReceiver;
    private boolean isArabicLang = false;
    private String[] countryName = null;
    private String[] iqamaCountryNames = null;
    private boolean isAirportAvailable = false;
    private boolean isIqamaCountryAvailable = false;
    private SignInDialog signInDialog;
    private int loginDetailsSuccess = -1;
    private int infoSource = -1;
    private TextView mSignInText = null;
    private HashMap<String, FlightAirportName> airportNamesMap = new HashMap<>();
    private HashMap<String, String> airLineNamesMap = new HashMap<>();
    private Activity mActivity;
    private PassengerInfoAdapter passengerInfoAdapter = null;
    private static final int GET_ACCESS_TOKEN = 8;
    private static final int UPDATE_PASSENGER_INFO = 9;
    private static final int USER_LOGOUT_SERVICE = 10;
    private boolean isFareCombinationSelected = false;
    private ArrayList<String> airlineCodeArr = null;
    private String userEarnedPoints = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationStarter.initialize(this, true);
        setContentView(R.layout.activity_tripsummary_passenger);
        this.mActivity = FlightBookingPassengerActivity.this;
        isInternetPresent = Utils.isConnectingToInternet(mActivity);
        isArabicLang = false;
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        Calendar calendar = Calendar.getInstance();
        airlineCodeArr = new ArrayList<>();
        currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(calendar.getTime());
//        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
//        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
//        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
//        if (tokenTime == -1) {
//            getTokenFromServer();
//        } else {
//            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
//            if (diff > Long.parseLong(expireIn)) {
//                getTokenFromServer();
//            }
//        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            randomNumber = bundle.getString(Constants.BOOKING_RANDOM_NUMBER, "");
            aipiJSONObject = bundle.getString(Constants.AIPI_JSON_OBJECT, "");
            aiJSONObject = bundle.getString(Constants.AI_JSON_OBJECT, "");
            tdr = bundle.getString(Constants.TDR_VALUE, "");
            hold = bundle.getString(Constants.HOLD_VALUE, "");
            usctp = bundle.getString(Constants.USCTP_VALUE, "");
            baggageDetails = bundle.getString(Constants.BAGGAGE_INFO, "");
            cabinType = bundle.getString(Constants.CABIN_TYPE, "");
            airlineCodeArr = getIntent().getStringArrayListExtra(Constants.AIRLINE_NAME);
            adultCount = Integer.parseInt(bundle.getString(Constants.ADULT_COUNT, ""));
            childCount = Integer.parseInt(bundle.getString(Constants.CHILD_COUNT, ""));
            infantCount = Integer.parseInt(bundle.getString(Constants.INFANT_COUNT, ""));
            tripType = Integer.parseInt(bundle.getString(Constants.TRIP_TYPE, ""));
            sourceName = bundle.getString(Constants.FORWARD_SOURCE, "");
            destinationName = bundle.getString(Constants.FORWARD_DESTINATION, "");
            secondSource = bundle.getString(Constants.SECOND_FORWARD_SOURCE, "");
            secondDestination = bundle.getString(Constants.SECOND_FORWARD_DESTINATION, "");
            thirdSource = bundle.getString(Constants.THIRD_FORWARD_SOURCE, "");
            thirdDestination = bundle.getString(Constants.THIRD_FORWARD_DESTINATION, "");
            forwardFlightDate = bundle.getString(Constants.FORWARD_FLIGHT_DATE, "");
            returnFlightDate = bundle.getString(Constants.RETURN_FLIGHT_DATE, "");
            firstFlightDate = bundle.getString(Constants.FIRST_FLIGHT_DATE, "");
            secondFlightDate = bundle.getString(Constants.SECOND_FLIGHT_DATE, "");
            thirdFlightDate = bundle.getString(Constants.THIRD_FLIGHT_DATE, "");
            infoSource = Integer.parseInt(bundle.getString(Constants.INFO_SOURCE, ""));
            airportNamesMap = (HashMap<String, FlightAirportName>) bundle.get(Constants.AIRPORT_NAME_HASHMAP);
            airLineNamesMap = (HashMap<String, String>) bundle.get(Constants.AIRLINE_NAME_HASHMAP);
            isFareCombinationSelected = bundle.getBoolean(Constants.IS_FARE_COMBINATION, false);
            userEarnedPoints = bundle.getString(Constants.USER_EARNED_POINTS, "");
        }
        Utils.printMessage(TAG, "Airline list :: " + airlineCodeArr.toString());
        if (tdr.equalsIgnoreCase("Yes")) {
            isPassportRequired = true;
            if (isPassportRequired && tripType != 3) {
                countryName = new String[]{"BAH", "KHS", "MCT", "MSH", "SLL", "DOH", "AAN", "AUH", "AZI", "DHF", "DWC", "DXB",
                        "FJR", "RKT", "SHJ", "XNB", "XSB", "ZDY", "ZVJ", "DXB", "KWI", "KHS", "MCT", "MSH", "SLL", "ABT", "AHB",
                        "AJF", "AQI", "BHH", "DMM", "DMS", "DWD", "EAM", "EJH", "ELQ", "GIZ", "HAS", "HOF", "JED", "MED", "QJB",
                        "RAE", "RAH", "RUH", "SHW", "TIF", "TUI", "TUU", "ULH", "URY", "WAE", "YNB"};
                List airportList = Arrays.asList(countryName);
                if (airportList.contains(sourceName) && airportList.contains(destinationName)) {
                    isAirportAvailable = true;
                }
                iqamaCountryNames = new String[]{"RUH", "AHB", "DMM", "JED", "MED", "YNB", "ELQ", "GIZ", "HAL", "TUU", "TIF",
                        "ABT", "AJF", "EJH", "RAE", "BHH", "DWD", "URY", "EAM", "AQI", "RAH", "SHW", "ULH", "TUI", "WAE", "QJB"};
                List iqamaAirportList = Arrays.asList(iqamaCountryNames);
                if (iqamaAirportList.contains(sourceName) && iqamaAirportList.contains(destinationName)) {
                    isIqamaCountryAvailable = true;
                }
            }
        } else {
            isPassportRequired = false;
        }

        personName = (TextView) findViewById(R.id.passenger_header);
        TextView termsText = (TextView) findViewById(R.id.trip_summary_terms);
        Button continueButton = (Button) findViewById(R.id.continue_button);
        TextView firstFlightStart = (TextView) findViewById(R.id.first_flight_start);
        TextView firstFlightEnd = (TextView) findViewById(R.id.first_flight_end);
        TextView secondFlightStart = (TextView) findViewById(R.id.second_flight_start);
        TextView secondFlightEnd = (TextView) findViewById(R.id.second_flight_end);
        TextView thirdFlightStart = (TextView) findViewById(R.id.third_flight_start);
        TextView thirdFlightEnd = (TextView) findViewById(R.id.third_flight_end);
        TextView journeyDetails = (TextView) findViewById(R.id.journey_details);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        LinearLayout firstFlightLayout = (LinearLayout) findViewById(R.id.first_flight_layout);
        LinearLayout secondFlightLayout = (LinearLayout) findViewById(R.id.second_flight_layout);
        LinearLayout thirdFlightLayout = (LinearLayout) findViewById(R.id.third_flight_layout);
        TextView passengersCount = (TextView) findViewById(R.id.passengers_count);
        ImageView firstJourneyImage = (ImageView) findViewById(R.id.first_journey_image);
        ImageView secondJourneyImage = (ImageView) findViewById(R.id.second_journey_image);
        ImageView thirdJourneyImage = (ImageView) findViewById(R.id.third_journey_image);
        ListView travellerListView = (ListView) findViewById(R.id.passenger_info_listView);
        if (Utils.isArabicLangSelected(mActivity)) {
            isArabicLang = true;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            firstJourneyImage.setRotation(180);
            secondJourneyImage.setRotation(180);
            thirdJourneyImage.setRotation(180);
            personName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            firstFlightStart.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            firstFlightEnd.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            secondFlightStart.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            secondFlightEnd.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            thirdFlightStart.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            thirdFlightEnd.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            journeyDetails.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            termsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            continueButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        titleFace = Typeface.createFromAsset(getAssets(), fontTitle);
        personName.setTypeface(titleFace);
        termsText.setTypeface(titleFace);
        firstFlightStart.setTypeface(titleFace);
        firstFlightEnd.setTypeface(titleFace);
        secondFlightStart.setTypeface(titleFace);
        secondFlightEnd.setTypeface(titleFace);
        thirdFlightStart.setTypeface(titleFace);
        thirdFlightEnd.setTypeface(titleFace);
        journeyDetails.setTypeface(titleFace);
        passengersCount.setTypeface(titleFace);
        continueButton.setTypeface(tf);
        personName.setText(getString(R.string.label_welcome_title) + " " + getString(R.string.label_guest_title));
        try {
            if (tripType == 1 || tripType == 2) {
                if (airportNamesMap.containsKey(sourceName))
                    firstFlightStart.setText(airportNamesMap.get(sourceName).getCityName());
                if (airportNamesMap.containsKey(destinationName))
                    firstFlightEnd.setText(airportNamesMap.get(destinationName).getCityName());
            }
        } catch (Exception e) {
        }
        if (!forwardFlightDate.equalsIgnoreCase("") && returnFlightDate.equalsIgnoreCase("")) {
            journeyDetails.setText(Utils.formatDateToDate(forwardFlightDate, mActivity));
        } else if (!forwardFlightDate.equalsIgnoreCase("") && !returnFlightDate.equalsIgnoreCase("")) {
            journeyDetails.setText(Utils.formatDateToDate(forwardFlightDate, mActivity) + " - " + Utils.formatDateToDate(returnFlightDate, mActivity));
        } else if (!firstFlightDate.equalsIgnoreCase("") && secondFlightDate.equalsIgnoreCase("") && thirdFlightDate.equalsIgnoreCase("")) {
            journeyDetails.setText(Utils.formatDateToDate(firstFlightDate, mActivity));
        } else if (!firstFlightDate.equalsIgnoreCase("") && !secondFlightDate.equalsIgnoreCase("") && thirdFlightDate.equalsIgnoreCase("")) {
            journeyDetails.setText(Utils.formatDateToDate(firstFlightDate, mActivity) + " - " + Utils.formatDateToDate(secondFlightDate, mActivity));
        } else if (!firstFlightDate.equalsIgnoreCase("") && !secondFlightDate.equalsIgnoreCase("") && !thirdFlightDate.equalsIgnoreCase("")) {
            journeyDetails.setText(Utils.formatDateToDate(firstFlightDate, mActivity) + " - " + Utils.formatDateToDate(secondFlightDate, mActivity) + " - " + Utils.formatDateToDate(thirdFlightDate, mActivity));
        }
//        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (tripType == 1) {
            firstFlightLayout.setVisibility(View.VISIBLE);
            secondFlightLayout.setVisibility(View.GONE);
            thirdFlightLayout.setVisibility(View.GONE);
            firstJourneyImage.setImageResource(R.drawable.fromarrow);
            String lang = "en_GB";
            if (isArabicLang) {
                lang = "ar_SA";
            }
            try {
                firstFlightStart.setText(airportNamesMap.get(sourceName).getCityName());
                firstFlightEnd.setText(airportNamesMap.get(destinationName).getCityName());
            } catch (Exception e) {
            }
        } else if (tripType == 2) {
            firstFlightLayout.setVisibility(View.VISIBLE);
            secondFlightLayout.setVisibility(View.GONE);
            thirdFlightLayout.setVisibility(View.GONE);
            firstJourneyImage.setImageResource(R.drawable.fromtoarrows);
            try {
                firstFlightStart.setText(airportNamesMap.get(sourceName).getCityName());
                firstFlightEnd.setText(airportNamesMap.get(destinationName).getCityName());
            } catch (Exception e) {
            }
        } else if (tripType == 3) {
            firstFlightStart.setText(sourceName);
            firstFlightEnd.setText(destinationName);
            if (!secondSource.isEmpty()) {
                secondFlightLayout.setVisibility(View.VISIBLE);
                secondFlightStart.setText(", " + secondSource);
                if (isArabicLang) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        secondFlightStart.setText(secondSource + ", ");
                    }
                }
                secondFlightEnd.setText(secondDestination);
                secondJourneyImage.setImageResource(R.drawable.fromarrow);
            }
            if (!thirdSource.isEmpty()) {
                thirdFlightLayout.setVisibility(View.VISIBLE);
                thirdFlightStart.setText(", " + thirdSource);
                if (isArabicLang) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        thirdFlightStart.setText(thirdSource + ", ");
                    }
                }
                thirdFlightEnd.setText(thirdDestination);
                thirdJourneyImage.setImageResource(R.drawable.fromarrow);
            }
            firstFlightLayout.setVisibility(View.VISIBLE);
            firstJourneyImage.setImageResource(R.drawable.fromarrow);
        }
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(titleFace);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter("com.flyin.bookings.ERROR_MESSAGE");
        registerReceiver(myReceiver, intentFilter);
        init_customactionbar();
        travellersList = new ArrayList<>();
        String[] titleArray = getResources().getStringArray(R.array.title_selection);
        for (int i = 0; i < adultCount; i++) {
            TravellerDetailsBean travellerDetails = new TravellerDetailsBean();
            travellerDetails.setTravellerHeader(getString(R.string.label_add_traveller) + " " + (travellersList.size() + 1));
            travellerDetails.setTravellerName("(" + getString(R.string.SAdultLbl) + ")");
            travellerDetails.setTravellerHeaderName(travellerDetails.getTravellerHeader() + " " + travellerDetails.getTravellerName());
            travellerDetails.setTravellerEmail(getString(R.string.label_enter_your_details));
            travellerDetails.setIsExpanded(false);
            travellerDetails.setIsPassportSelected(true);
            travellerDetails.setDocumentType("2");
            travellerDetails.setPassengerType("ADT");
            travellerDetails.setTitle(titleArray[0]);
            travellersList.add(travellerDetails);
        }
        String[] childTitleArray = getResources().getStringArray(R.array.child_title_selection);
        for (int i = 0; i < childCount; i++) {
            TravellerDetailsBean travellerDetails = new TravellerDetailsBean();
            travellerDetails.setTravellerHeader(getString(R.string.label_add_traveller) + " " + (travellersList.size() + 1));
            travellerDetails.setTravellerName("(" + getString(R.string.SChildLbl) + ")");
            travellerDetails.setTravellerHeaderName(travellerDetails.getTravellerHeader() + " " + travellerDetails.getTravellerName());
            travellerDetails.setTravellerEmail(getString(R.string.label_enter_your_details));
            travellerDetails.setIsExpanded(false);
            travellerDetails.setIsPassportSelected(true);
            travellerDetails.setDocumentType("2");
            travellerDetails.setPassengerType("CHD");
            travellerDetails.setTitle(childTitleArray[0]);
            travellersList.add(travellerDetails);
        }
        for (int i = 0; i < infantCount; i++) {
            TravellerDetailsBean travellerDetails = new TravellerDetailsBean();
            travellerDetails.setTravellerHeader(getString(R.string.label_add_traveller) + " " + (travellersList.size() + 1));
            travellerDetails.setTravellerName("(" + getString(R.string.SInfantLbl) + ")");
            travellerDetails.setTravellerHeaderName(travellerDetails.getTravellerHeader() + " " + travellerDetails.getTravellerName());
            travellerDetails.setTravellerEmail(getString(R.string.label_enter_your_details));
            travellerDetails.setIsExpanded(false);
            travellerDetails.setIsPassportSelected(true);
            travellerDetails.setDocumentType("2");
            travellerDetails.setPassengerType("INF");
            travellerDetails.setTitle(childTitleArray[0]);
            travellersList.add(travellerDetails);
        }
        passengerInfoAdapter = new PassengerInfoAdapter(mActivity, travellersList);
        travellerListView.setAdapter(passengerInfoAdapter);
        travellerListView.setFocusable(false);
//        Utils.setListViewHeightBasedOnChildren(travellerListView);
        passengerInfoAdapter.notifyDataSetChanged();
        Singleton.getInstance().fetchTravellerArrayList.clear();
        travellerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Singleton.getInstance().travellersInfoBean = travellersList.get(position);
                if (travellerArrayList.size() > 0) {
                    Singleton.getInstance().fetchTravellerArrayList = travellerArrayList;
                }
                Intent intent = new Intent(mActivity, FlightBookingPassengerInfoActivity.class);
                intent.putExtra(Constants.IS_USER_LOGGED, isUserSignIn);
                intent.putExtra(Constants.TDR_VALUE, tdr);
                //intent.putExtra("traveller_list", travellerArrayList);
                intent.putStringArrayListExtra(Constants.AIRLINE_NAME, airlineCodeArr);
                intent.putExtra("isIqamaCountryAvailable", isIqamaCountryAvailable);
                intent.putExtra("isPassportRequired", isPassportRequired);
                intent.putExtra("isAirportAvailable", isAirportAvailable);
                intent.putExtra(Constants.SELECTED_USER_POSITION, String.valueOf(position));
                intent.putExtra(Constants.INFO_SOURCE, String.valueOf(infoSource));
                startActivityForResult(intent, UPDATE_PASSENGER_INFO);
            }
        });
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < travellersList.size(); i++) {
                    if (travellersList.get(i).getTitle().isEmpty() || travellersList.get(i).getTitle().equalsIgnoreCase("null")) {
                        displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_err_title_msg));
                        return;
                    }
                    if (travellersList.get(i).getFirstName().isEmpty()) {
                        displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_err_fname_msg));
                        return;
                    }
                    if (!Utils.isValidName(Utils.getNameWithoutChar(travellersList.get(i).getFirstName()))) {
                        displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " +
                                getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_firstName));
                        return;
                    }
                    for (int j = 0; j < travellersList.size(); j++) {
                        if (i < j) {
                            if (travellersList.get(i).getFirstName().equalsIgnoreCase(travellersList.get(j).getFirstName())) {
                                displayErrorMessage(travellersList.get(j).getTravellerHeader() + " - " +
                                        getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_firstName));
                                return;
                            }
                        }
                    }
                    if (travellersList.get(i).getLastName().isEmpty()) {
                        displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_err_lname_msg));
                        return;
                    }
                    if (!Utils.isValidName(Utils.getNameWithoutChar(travellersList.get(i).getLastName()))) {
                        displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.ErrInvalidNameMsg)
                                + " " + getString(R.string.label_traveller_lastName));
                        return;
                    }
                    if (Utils.getNameWithoutChar(travellersList.get(i).getFirstName()).equalsIgnoreCase(Utils.getNameWithoutChar(travellersList.get(i).getLastName()))) {
                        displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " +
                                getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_lastName));
                        return;
                    }
                    if (i == 0) {
                        if (travellersList.get(0).getEmail().isEmpty()) {
                            displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_email_message));
                            return;
                        }
                        if (!Utils.isValidEmail(travellersList.get(0).getEmail())) {
                            displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_email_valid_message));
                            return;
                        }
                        if (travellersList.get(0).getPhoneNo().isEmpty() || !Utils.isValidMobile(travellersList.get(0).getPhoneNo())) {
                            displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_err_phone_msg));
                            return;
                        }
                    }
                    if (airlineCodeArr.contains("XY")) {
                        if (!travellersList.get(i).getDateOfBirth().isEmpty()) {
                            if (travellersList.get(i).getPassengerType().equalsIgnoreCase("ADT")) {
                                if (Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) < (365 * 12)) {
                                    displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                                    return;
                                }
                            } else if (travellersList.get(i).getPassengerType().equalsIgnoreCase("CHD")) {
                                if (Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) > (365 * 12) ||
                                        Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) < 1) {
                                    displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                                    return;
                                }
                            } else if (travellersList.get(i).getPassengerType().equalsIgnoreCase("INF")) {
                                if (Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) > (365 * 2) ||
                                        Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) < 1) {
                                    displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                                    return;
                                }
                            }
                        }
                    }
                    if (!isPassportRequired) {
                        if (travellersList.get(i).getPassengerType().equalsIgnoreCase("CHD")) {
                            if (travellersList.get(i).getDateOfBirth().isEmpty() || Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) > (365 * 12) || Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) < 1) {
                                displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                                return;
                            }
                        }
                        if (travellersList.get(i).getPassengerType().equalsIgnoreCase("INF")) {
                            if (travellersList.get(i).getDateOfBirth().isEmpty() || Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) > (365 * 2) || Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) < 1) {
                                displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                                return;
                            }
                        }
                    }
                    if (isPassportRequired) {
                        if (travellersList.get(i).getDateOfBirth().isEmpty()) {
                            displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                            return;
                        }
                        if (travellersList.get(i).getPassengerType().equalsIgnoreCase("ADT")) {
                            if (Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) < (365 * 12)) {
                                displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                                return;
                            }
                        } else if (travellersList.get(i).getPassengerType().equalsIgnoreCase("CHD")) {
                            if (Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) > (365 * 12) ||
                                    Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) < 1) {
                                displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                                return;
                            }
                        } else if (travellersList.get(i).getPassengerType().equalsIgnoreCase("INF")) {
                            if (Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) > (365 * 2) ||
                                    Integer.parseInt(Utils.dateDifference(currentDate, travellersList.get(i).getDateOfBirth())) < 1) {
                                displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                                return;
                            }
                        }
                        if (travellersList.get(i).getDocumentType().equalsIgnoreCase("2")) {
                            if (travellersList.get(i).getPassportNumber().isEmpty() || !Utils.isValidPassport(travellersList.get(i).getPassportNumber(), Constants.PASSPOR_NUMBER)) {
                                displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_passport_valid_message));
                                return;
                            }
                        } else if (travellersList.get(i).getDocumentType().equalsIgnoreCase("3") && !travellersList.get(i).isIqamaSelected()) {
                            if (travellersList.get(i).getPassportNumber().isEmpty() || !Utils.isValidPassport(travellersList.get(i).getPassportNumber(), Constants.NATIONAL_ID)) {
                                displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_hint_national_id_msg));
                                return;
                            }
                        }
                        if (travellersList.get(i).getDocumentType().equalsIgnoreCase("4") && travellersList.get(i).isIqamaSelected()) {
                            if (airlineCodeArr.contains("SV") || airlineCodeArr.contains("XY")) {
                                if (travellersList.get(i).getPassportNumber().isEmpty() || !Utils.isValidPassport(travellersList.get(i).getPassportNumber(), Constants.IQAMA_ID)) {
                                    displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_err_iqama_number_msg));
                                    return;
                                }
                            }
                        }
                        if (travellersList.get(i).getNationality().isEmpty()) {
                            displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_nationality_message));
                            return;
                        }
                        if (travellersList.get(i).getDocumentType().equalsIgnoreCase("2") || (travellersList.get(i).getDocumentType().equalsIgnoreCase("3") && !travellersList.get(i).isIqamaSelected())) {
                            if (travellersList.get(i).getPassportIssuingCountry().isEmpty()) {
                                displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_passport_issuing_country_message));
                                return;
                            }
                        }
                        if (travellersList.get(i).getDocumentType().equalsIgnoreCase("2")) {
                            if (travellersList.get(i).getPassportExpiry().isEmpty() || Integer.parseInt(Utils.dateDifference(travellersList.get(i).getPassportExpiry(), currentDate)) < 180) {
                                displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_passport_expiry_message));
                                return;
                            }
                        }
                        if (travellersList.get(i).getDocumentType().equalsIgnoreCase("4") && travellersList.get(i).isIqamaSelected()) {
                            if (airlineCodeArr.contains("SV") || airlineCodeArr.contains("XY")) {
                                if (travellersList.get(i).getPassportExpiry().isEmpty()) {
                                    displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_err_iqama_expiry_msg));
                                    return;
                                }
                            }
                        }
                        if (travellersList.get(i).getDocumentType().equalsIgnoreCase("2")) {
                            if (airlineCodeArr.contains("XY")) {
                                if (travellersList.get(i).getPassportIssueDate().isEmpty()) {
                                    displayErrorMessage(travellersList.get(i).getTravellerHeader() + " - " + getString(R.string.label_error_passport_issue_message));
                                    return;
                                }
                            }
                        }
                    }
                }
                requestJsonData = handleBookRQRequest();
                if (isInternetPresent) {
                    showLoading();
                    if (isFareCombinationSelected) {
                        HTTPAsync async = new HTTPAsync(FlightBookingPassengerActivity.this, FlightBookingPassengerActivity.this,
                                Constants.FARE_COMBINATION_PRE_BOOK_RQ, "", requestJsonData, GET_SEARCH_RESPONSE, HTTPAsync.METHOD_POST);
                        async.execute();
                    } else {
                        HTTPAsync async = new HTTPAsync(FlightBookingPassengerActivity.this, FlightBookingPassengerActivity.this,
                                Constants.BOOK_RQ_URL, "", requestJsonData, GET_SEARCH_RESPONSE, HTTPAsync.METHOD_POST);
                        async.execute();
                    }
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(titleFace);
                    errorDescriptionText.setTypeface(titleFace);
                    searchButton.setTypeface(titleFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(FlightBookingPassengerActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                showLoading();
                                if (isFareCombinationSelected) {
                                    HTTPAsync async = new HTTPAsync(FlightBookingPassengerActivity.this, FlightBookingPassengerActivity.this,
                                            Constants.FARE_COMBINATION_PRE_BOOK_RQ, "", requestJsonData, GET_SEARCH_RESPONSE, HTTPAsync.METHOD_POST);
                                    async.execute();
                                } else {
                                    HTTPAsync async = new HTTPAsync(FlightBookingPassengerActivity.this, FlightBookingPassengerActivity.this,
                                            Constants.BOOK_RQ_URL, "", requestJsonData, GET_SEARCH_RESPONSE, HTTPAsync.METHOD_POST);
                                    async.execute();
                                }
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == UPDATE_PASSENGER_INFO) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    passengerInfoAdapter.notifyDataSetChanged();
                }
            }
        } else if (requestCode == BOOK_FLIGHT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    loadingViewLayout.removeView(loadingView);
                    loadingViewLayout.removeAllViews();
                }
            }
        } else {
            signInDialog.passFacebookResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onItemClick(int position) {
    }

    @Override
    public void onSelectedItemClick(int position, int type) {
    }

    @Override
    public void onNameItemClick(int position) {
    }

    @Override
    public void onPassportItemClick(int position, String value, boolean isIqamaChecked) {
    }

    @Override
    public void onItemClick(int position, int type, String value, String code, int selectedPosition, boolean isHijriChecked, boolean isIqamaSelected) {
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.ERROR_MESSAGE")) {
                String type = "";
                if (intent.hasExtra("type")) {
                    type = intent.getStringExtra("type");
                }
                if (type.equalsIgnoreCase("success")) {
                    displaySuccessMessage(intent.getStringExtra("message"), true);
                } else {
                    displaySuccessMessage(intent.getStringExtra("message"), false);
                }
            }
        }
    }

    private String handleBookRQRequest() {
        JSONObject finalObj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String currency = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        String userSelectedLang = "EN";
        if (Utils.isArabicLangSelected(FlightBookingPassengerActivity.this)) {
            userSelectedLang = "AR";
        } else {
            userSelectedLang = "EN";
        }
        try {
            JSONObject mainJSON = new JSONObject();
            JSONObject sourceObj = new JSONObject();
            sourceObj = Utils.getAppSettingData(sourceObj, FlightBookingPassengerActivity.this);
            sourceObj.put("isoCurrency", "SAR");
            sourceObj.put("echoToken", randomNumber);
            sourceObj.put("timeStamp", timeStamp);
            sourceObj.put("clientId", Constants.CLIENT_ID);
            sourceObj.put("clientSecret", Constants.CLIENT_SECRET);
            sourceObj.put("accessToken", Utils.getRequestAccessToken(FlightBookingPassengerActivity.this));
            sourceObj.put("groupType", groupType);
            sourceObj.put("language", userSelectedLang);
            JSONObject ai = new JSONObject(aiJSONObject);
            if (ai.has("odos")) {
                if (ai.getJSONObject("odos").has("odo")) {
                    JSONArray odoArr = ai.getJSONObject("odos").getJSONArray("odo");
                    for (int i = 0; i < odoArr.length(); i++) {
                        if (odoArr.getJSONObject(i).has("tripStatus")) {
                            odoArr.getJSONObject(i).remove("tripStatus");
                        }
                    }
                }
            }
            JSONObject aipi = new JSONObject(aipiJSONObject);
            mainJSON.put("source", sourceObj);
            mainJSON.put("ai", ai);
            mainJSON.put("aipi", aipi);
            mainJSON.put("tdr", tdr);
            mainJSON.put("hold", hold);
            mainJSON.put("tinf", addPassengerList());
            mainJSON.put("usc", currency);
            mainJSON.put("usctp", usctp);
            mainJSON.put("fbi", "");
            Utils.printMessage(TAG, "usctp::" + usctp);
            String userMail = preferences.getString(Constants.MEMBER_EMAIL, "");
            if (!userMail.equalsIgnoreCase("")) {
                loggedUser = userMail;
            } else {
                loggedUser = "null";
            }
            userEmail = travellersList.get(0).getEmail();
            userMobile = travellersList.get(0).getPhoneNo();
            mainJSON.put("email", userEmail.toLowerCase());
            mainJSON.put("ph", userMobile);
            mainJSON.put("loginId", Utils.checkStringValue(loggedUser));
            String mobilePcc = travellersList.get(0).getPhoneCode().replace("+", "");
            mainJSON.put("pcc", mobilePcc);
            mainJSON.put("adrs", "");
            finalObj.put("gws", buildGwsObject());
            finalObj.put("bookRQ", mainJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        bookRQJSONObject = finalObj.toString();
        return finalObj.toString();
    }

    private JSONObject addPassengerList() {
        JSONObject atObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < travellersList.size(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("pt", travellersList.get(i).getPassengerType());
                String personTitle = travellersList.get(i).getTitle();
                if (travellersList.get(i).getTitle().equalsIgnoreCase(getString(R.string.adult_title)) || travellersList.get(i).getTitle().equalsIgnoreCase("Mr")) {
                    personTitle = "Mr";
                } else if (travellersList.get(i).getTitle().equalsIgnoreCase(getString(R.string.label_miss)) || travellersList.get(i).getTitle().equalsIgnoreCase("Miss")) {
                    personTitle = "Miss";
                } else if (travellersList.get(i).getTitle().equalsIgnoreCase(getString(R.string.label_mrs)) || travellersList.get(i).getTitle().equalsIgnoreCase("Mrs")) {
                    personTitle = "Mrs";
                } else if (travellersList.get(i).getTitle().equalsIgnoreCase(getString(R.string.child_title)) || travellersList.get(i).getTitle().equalsIgnoreCase("Mstr")) {
                    personTitle = "Mstr";
                }
                obj.put("title", personTitle);
                obj.put("gn", Utils.getNameWithoutChar(travellersList.get(i).getFirstName()));
                obj.put("sn", Utils.getNameWithoutChar(travellersList.get(i).getLastName()));
                obj.put("mreq", travellersList.get(i).getMealPreferencesCode());
                obj.put("sreq", travellersList.get(i).getSeatPreferencesCode());
                obj.put("ffp", travellersList.get(i).getFrequentFlyerNumber());
                obj.put("remarks", travellersList.get(i).getSpecialAssistanceCode());
                obj.put("extra", "");
                userName = Utils.getNameWithoutChar(travellersList.get(0).getFirstName()) + " " + Utils.getNameWithoutChar(travellersList.get(0).getLastName());
                String userTitle = travellersList.get(0).getTitle();
                if (userTitle.equalsIgnoreCase(getString(R.string.adult_title)) || userTitle.equalsIgnoreCase("Mr")) {
                    userTitle = "Mr";
                }
                userFullName = userTitle + " " + Utils.getNameWithoutChar(travellersList.get(0).getFirstName()) + " " + Utils.getNameWithoutChar(travellersList.get(0).getLastName());
                if (isPassportRequired) {
                    JSONObject docObj = new JSONObject();
                    String passportExp = "";
                    if (travellersList.get(i).getDocumentType().equalsIgnoreCase("2") || travellersList.get(i).isIqamaSelected()) {
                        if (travellersList.get(i).isHijriChecked()) {
                            passportExp = Utils.convertGregToHijriDate(travellersList.get(i).getPassportExpiry());
                        } else {
                            passportExp = Utils.TimeStampFormatToDateFormat(travellersList.get(i).getPassportExpiry());
                        }
                    } else {
                        passportExp = "";
                    }
                    String passportIssueDate = "";
                    if (travellersList.get(i).getDocumentType().equalsIgnoreCase("2") && airlineCodeArr.contains("XY")) {
                        passportIssueDate = Utils.TimeStampFormatToDateFormat(travellersList.get(i).getPassportIssueDate());
                    } else {
                        passportIssueDate = "";
                    }
                    docObj.put("exd", passportExp);
                    docObj.put("efd", passportIssueDate);
                    String gender = "";
                    if (travellersList.get(i).getPassengerType().equalsIgnoreCase("ADT")) {
                        if (travellersList.get(i).getTitle().equalsIgnoreCase(getString(R.string.adult_title)) || travellersList.get(i).getTitle().equalsIgnoreCase("Mr")) {
                            gender = "Male";
                        } else {
                            gender = "Female";
                        }
                    } else {
                        if (travellersList.get(i).getTitle().equalsIgnoreCase(getString(R.string.child_title)) || travellersList.get(i).getTitle().equalsIgnoreCase("Mstr")) {
                            gender = "Male";
                        } else {
                            gender = "Female";
                        }
                    }
                    docObj.put("gen", gender);
                    String travellerBirth = "";
                    if (!travellersList.get(i).getDateOfBirth().equalsIgnoreCase("")) {
                        travellerBirth = Utils.TimeStampFormatToDateFormat(travellersList.get(i).getDateOfBirth());
                    } else {
                        travellerBirth = "";
                    }
                    docObj.put("bd", travellerBirth);
                    docObj.put("bp", travellersList.get(i).getNationalityCode().toUpperCase());
                    docObj.put("dType", travellersList.get(i).getDocumentType());
                    docObj.put("di", travellersList.get(i).getPassportNumber());
                    docObj.put("dil", travellersList.get(i).getPassportIssuingCountryCode().toUpperCase());
                    docObj.put("dia", travellersList.get(i).getPassportIssuingCountryCode().toUpperCase());
                    obj.put("doc", docObj);
                } else {
                    JSONObject docObj = new JSONObject();
                    String passengerGender = "";
                    if (travellersList.get(i).getPassengerType().equalsIgnoreCase("ADT")) {
                        if (travellersList.get(i).getTitle().equalsIgnoreCase(getString(R.string.adult_title)) || travellersList.get(i).getTitle().equalsIgnoreCase("Mr")) {
                            passengerGender = "Male";
                        } else {
                            passengerGender = "Female";
                        }
                    } else {
                        if (travellersList.get(i).getTitle().equalsIgnoreCase(getString(R.string.child_title)) || travellersList.get(i).getTitle().equalsIgnoreCase("Mstr")) {
                            passengerGender = "Male";
                        } else {
                            passengerGender = "Female";
                        }
                    }
                    docObj.put("gen", passengerGender);
                    String travellerBirth = "";
                    if (!travellersList.get(i).getDateOfBirth().equalsIgnoreCase("")) {
                        travellerBirth = Utils.TimeStampFormatToDateFormat(travellersList.get(i).getDateOfBirth());
                    } else {
                        travellerBirth = "";
                    }
                    docObj.put("bd", travellerBirth);
                    obj.put("doc", docObj);
                }
                if (!baggageDetails.isEmpty()) {
                    JSONArray bgoptsArray = new JSONArray();
                    JSONObject baggageObj = new JSONObject();
                    if (!cabinType.equalsIgnoreCase("")) {
                        baggageObj.put("bgc", baggageDetails + " " + cabinType + " Class");
                        baggageObj.put("wgt", baggageDetails + " " + cabinType + " Class");
                    } else {
                        baggageObj.put("bgc", baggageDetails);
                        baggageObj.put("wgt", baggageDetails);
                    }
                    String journeyType = "";
                    if (tripType == 1 || tripType == 3) {
                        journeyType = "O";
                    } else if (tripType == 2) {
                        journeyType = "R";
                    }
                    baggageObj.put("pcs", "");
                    baggageObj.put("fee", "");
                    baggageObj.put("type", "");
                    baggageObj.put("jt", journeyType);
                    bgoptsArray.put(baggageObj);
                    obj.put("bgopts", bgoptsArray);
                }
                jsonArray.put(obj);
            }
            atObject.put("at", jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tinfJSONObject = atObject.toString();
        return atObject;
    }

    private JSONObject buildGwsObject() {
        JSONObject gwsObj = new JSONObject();
        try {
            gwsObj.put("cchrgs", Utils.checkStringValue(""));
            gwsObj.put("reischrgs", Utils.checkStringValue(""));
            gwsObj.put("blkTkt", "false");
            gwsObj.put("isoCuntry", Utils.getUserCountry(FlightBookingPassengerActivity.this));
            gwsObj.put("affName", Utils.checkStringValue(""));
            gwsObj.put("affUnqId", Utils.checkStringValue(""));
            gwsObj.put("dmType", "nodeuat.flyin.com");
            gwsObj.put("dmCurrency", "SAR");
            JSONObject loyaltyObj = new JSONObject();
            SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
            if (!pref.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
                if (pref.getBoolean(Constants.IS_REWARDS_ENABLED, false)) {
                    if (!userEarnedPoints.isEmpty()) {
                        loyaltyObj.put("earnpoint", Integer.parseInt(userEarnedPoints));
                    } else {
                        loyaltyObj.put("earnpoint", Utils.checkStringValue("null"));
                    }
                } else {
                    loyaltyObj.put("earnpoint", Utils.checkStringValue("null"));
                }
            } else {
                loyaltyObj.put("earnpoint", Utils.checkStringValue("null"));
            }
            loyaltyObj.put("redeempoint", Utils.checkStringValue("null"));
            loyaltyObj.put("rdmdiscountusr", Utils.checkStringValue("null"));
            loyaltyObj.put("status", "PENDING");
            gwsObj.put("loyaltydetails", loyaltyObj);
        } catch (JSONException e) {
        }
        return gwsObj;
    }

    @SuppressWarnings("ResourceType")
    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setBackgroundResource(R.color.error_message_background);
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver);
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        JSONObject obj = null;
        closeLoading();
        Utils.printMessage(TAG, "RESPONSE::" + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            closeFetchResultLoading();
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(titleFace);
            errorDescriptionText.setTypeface(titleFace);
            searchButton.setTypeface(titleFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(FlightBookingPassengerActivity.this);
                    if (isInternetPresent) {
                        loadingViewLayout.removeView(errorView);
                        Intent intent = new Intent(FlightBookingPassengerActivity.this, MainSearchActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        } else {
            try {
                obj = new JSONObject(data);
                if (serviceType == GET_SEARCH_RESPONSE) {
                    if (obj.has("bookRS")) {
                        String bookRSValue = obj.getString("bookRS");
                        if (bookRSValue.equalsIgnoreCase("null")) {
                            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View errorView = inflater.inflate(R.layout.view_error_response, null);
                            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                            errorText = (TextView) errorView.findViewById(R.id.error_text);
                            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                            searchButton = (Button) errorView.findViewById(R.id.search_button);
                            errorText.setTypeface(titleFace);
                            errorDescriptionText.setTypeface(titleFace);
                            searchButton.setTypeface(titleFace);
                            searchButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    loadingViewLayout.removeView(errorView);
                                    Intent intent = new Intent(FlightBookingPassengerActivity.this, MainSearchActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });
                            loadErrorType(Constants.WRONG_ERROR_PAGE);
                            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            loadingViewLayout.addView(errorView);
                        } else if (obj.getJSONObject("bookRS").has("errors")) {
                            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View errorView = inflater.inflate(R.layout.view_error_response, null);
                            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                            errorText = (TextView) errorView.findViewById(R.id.error_text);
                            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                            searchButton = (Button) errorView.findViewById(R.id.search_button);
                            errorText.setTypeface(titleFace);
                            errorDescriptionText.setTypeface(titleFace);
                            searchButton.setTypeface(titleFace);
                            searchButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    loadingViewLayout.removeView(errorView);
                                    Intent intent = new Intent(FlightBookingPassengerActivity.this, MainSearchActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });
                            loadErrorType(Constants.WRONG_ERROR_PAGE);
                            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            loadingViewLayout.addView(errorView);
                        } else {
                            Intent intent = new Intent(FlightBookingPassengerActivity.this, TripSummaryPaymentActivity.class);
                            intent.putExtra(Constants.JSON_RESULT, data);
                            intent.putExtra(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                            intent.putExtra(Constants.AI_JSON_OBJECT, aiJSONObject);
                            intent.putExtra(Constants.AIPI_JSON_OBJECT, aipiJSONObject);
                            intent.putExtra(Constants.TINF_JSON_OBJECT, tinfJSONObject);
                            intent.putExtra(Constants.BOOKRQ_JSON_OBJECT, bookRQJSONObject);
                            intent.putExtra(Constants.USER_NAME, userName);
                            intent.putExtra(Constants.LOGGED_USER, loggedUser);
                            intent.putExtra(Constants.USER_FULL_NAME, userFullName);
                            intent.putExtra(Constants.USER_EMAIL, userEmail.toLowerCase());
                            intent.putExtra(Constants.USER_MOBILE, userMobile);
                            intent.putExtra(Constants.TDR_VALUE, tdr);
                            intent.putExtra(Constants.HOLD_VALUE, hold);
                            intent.putExtra(Constants.USCTP_VALUE, usctp);
                            intent.putExtra(Constants.AIRPORT_NAME_HASHMAP, airportNamesMap);
                            intent.putExtra(Constants.AIRLINE_NAME_HASHMAP, airLineNamesMap);
                            intent.putExtra(Constants.IS_FARE_COMBINATION, isFareCombinationSelected);
                            intent.putExtra(Constants.INFO_SOURCE, String.valueOf(infoSource));
                            startActivityForResult(intent, BOOK_FLIGHT);
                        }
                    }
                } else if (serviceType == GET_FETCH_RESULT) {
                    travellerArrayList.clear();
                    closeFetchResultLoading();
                    if (obj.has("customerDetails")) {
                        JSONArray customerDetailsArray = obj.getJSONArray("customerDetails");
                        for (int i = 0; i < customerDetailsArray.length(); i++) {
                            TravellerDetailsBean travellerBean = new TravellerDetailsBean();
                            if (customerDetailsArray.getJSONObject(i).has("basicDetail")) {
                                JSONObject basicDetailsObj = customerDetailsArray.getJSONObject(i).getJSONObject("basicDetail");
                                travellerBean.setTitle(basicDetailsObj.getString("title"));
                                travellerBean.setFirstName(basicDetailsObj.getString("firstName"));
                                travellerBean.setLastName(basicDetailsObj.getString("lastName"));
                                travellerBean.setPhoneNo(basicDetailsObj.getString("mobileNumber"));
                                travellerBean.setMobileAreaCode(basicDetailsObj.getString("mobileAreaCode"));
                                travellerBean.setAddress(basicDetailsObj.getString("address"));
                                travellerBean.setEmail(basicDetailsObj.getString("email"));
                                travellerBean.setProfileId(basicDetailsObj.getString("profileID"));
                            }
                            if (customerDetailsArray.getJSONObject(i).has("passportDetails") && !customerDetailsArray.getJSONObject(i).getJSONArray("passportDetails").isNull(0)) {
                                JSONArray passportDetailsArray = customerDetailsArray.getJSONObject(i).getJSONArray("passportDetails");
                                for (int j = 0; j < passportDetailsArray.length(); j++) {
                                    JSONObject passportDetailsObj = passportDetailsArray.getJSONObject(j);
                                    if (passportDetailsObj.has("docType")) {
                                        String documentType = passportDetailsObj.getString("docType");
                                        if (documentType.equalsIgnoreCase("2")) {
                                            travellerBean.setPassportIssueDate(Utils.checkStringVal(passportDetailsObj.getString("issuedate")));
                                            travellerBean.setPassportNumber(Utils.checkStringVal(passportDetailsObj.getString("passportNo")));
                                            travellerBean.setPassportExpiry(Utils.checkStringVal(passportDetailsObj.getString("expiryDate")));
                                            travellerBean.setDateOfBirth(Utils.checkStringVal(passportDetailsObj.getString("dateofBirth")));
                                            travellerBean.setNationality(Utils.checkStringVal(passportDetailsObj.getString("nationality")));
                                            travellerBean.setPassportIssuingCountry(Utils.checkStringVal(passportDetailsObj.getString("countryIssued")));
                                            travellerBean.setDocumentType(passportDetailsObj.getString("docType"));
                                        }
                                    }
                                }
                            }
                            if (Utils.checkStringVal(travellerBean.getFirstName()).isEmpty() && Utils.checkStringVal(travellerBean.getLastName()).isEmpty()) {
                                continue;
                            }
                            travellerArrayList.add(travellerBean);
                        }
                    }
                } else if (serviceType == GET_ACCESS_TOKEN) {
                    try {
                        obj = new JSONObject(data);
                        String accessToken = obj.getString("accessToken");
                        String expireIn = obj.getString("expireIn");
                        String refreshToken = obj.getString("refreshToken");
                        long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(Constants.ACCESS_TOKEN, accessToken);
                        editor.putString(Constants.EXPIRE_IN, expireIn);
                        editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                        editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                        editor.apply();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else if (serviceType == USER_LOGOUT_SERVICE) {
                    try {
                        obj = new JSONObject(data);
                        String statusVal = obj.getString("status");
                        if(statusVal.equalsIgnoreCase("SUCCESS")){
                            SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                            pref.edit().putString(Constants.MEMBER_EMAIL, "").apply();
                            pref.edit().putString(Constants.MEMBER_ACCESS_TOKEN, "").apply();
                            pref.edit().putString(Constants.MEMBER_TOKEN_TYPE, "").apply();
                            pref.edit().putString(Constants.USER_UNIQUE_ID, "").apply();
                            pref.edit().putBoolean(Constants.isFanClub, false).apply();
                            pref.edit().putBoolean(Constants.IS_REWARDS_ENABLED, false).apply();
                            checkUserAccount();
                            closeFetchResultLoading();
                        }else {
                            closeFetchResultLoading();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        closeFetchResultLoading();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {

    }

    private void init_customactionbar() {
        final ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.VISIBLE);
        ImageView mImageView = (ImageView) mCustomView.findViewById(R.id.first_home_icon);
        ImageView mHeaderImage = (ImageView) mCustomView.findViewById(R.id.first_home_logo);
        mSignInText = (TextView) mCustomView.findViewById(R.id.sing_in_button);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.home_back_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_home_text);
        mHeaderImage.setImageResource(R.drawable.screen_two);
        backText.setTypeface(titleFace);
        mSignInText.setTypeface(titleFace);
        if (isArabicLang) {
            mImageView.setScaleType(ImageView.ScaleType.FIT_START);
            mHeaderImage.setImageResource(R.drawable.arabic_two);
            mSignInText.setGravity(Gravity.END);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                mSignInText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            }
            LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
            );
            backText.setLayoutParams(textParams);
            textParams.setMargins(0, -5, 0, 0);
            mSignInText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            backText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
        }
        handleUserFetchResults();
        mSignInText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInDialog = new SignInDialog(mActivity);
                isInternetPresent = Utils.isConnectingToInternet(FlightBookingPassengerActivity.this);
                if (isInternetPresent) {
                    handleSignInSignOutFnctn();
                } else {
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(titleFace);
                    errorDescriptionText.setTypeface(titleFace);
                    searchButton.setTypeface(titleFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(FlightBookingPassengerActivity.this);
                            if (isInternetPresent) {
                                handleSignInSignOutFnctn();
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }

            }
        });
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    private void handleUserFetchResults() {
        preferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String memberAccessToken = preferences.getString(Constants.MEMBER_ACCESS_TOKEN, "");
        if (!memberAccessToken.equalsIgnoreCase("")) {
            isUserSignIn = true;
            mSignInText.setText(getString(R.string.label_action_sign_out));
            final String requestUserDetailsJSON = travellerDetailsTask();
            if (isInternetPresent) {
                travellerArrayList.clear();
                showFetchResultLoading();
                HTTPAsync async = new HTTPAsync(mActivity, FlightBookingPassengerActivity.this, Constants.FETCH_TRAVELLER, "", requestUserDetailsJSON,
                        GET_FETCH_RESULT, HTTPAsync.METHOD_POST);
                async.execute();
            } else {
                inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View errorView = inflater.inflate(R.layout.view_error_response, null);
                errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                errorText = (TextView) errorView.findViewById(R.id.error_text);
                errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                searchButton = (Button) errorView.findViewById(R.id.search_button);
                errorText.setTypeface(titleFace);
                errorDescriptionText.setTypeface(titleFace);
                searchButton.setTypeface(titleFace);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isInternetPresent = Utils.isConnectingToInternet(mActivity);
                        if (isInternetPresent) {
                            loadingViewLayout.removeView(errorView);
                            travellerArrayList.clear();
                            showFetchResultLoading();
                            HTTPAsync async = new HTTPAsync(mActivity, FlightBookingPassengerActivity.this, Constants.FETCH_TRAVELLER, "", requestUserDetailsJSON,
                                    GET_FETCH_RESULT, HTTPAsync.METHOD_POST);
                            async.execute();
                        }
                    }
                });
                loadErrorType(Constants.NETWORK_ERROR);
                errorView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                loadingViewLayout.addView(errorView);
            }
        } else if (memberAccessToken.equalsIgnoreCase("")) {
            isUserSignIn = false;
            mSignInText.setText(getString(R.string.label_action_signin));
            personName.setText(getString(R.string.label_welcome_title) + " " + getString(R.string.label_guest_title));
        }
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(titleFace);
        descriptionText.setVisibility(View.GONE);
        if (isArabicLang) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private String travellerDetailsTask() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(FlightBookingPassengerActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(FlightBookingPassengerActivity.this));
            mainJSON.put("source", sourceJSON);
            mainJSON.put("userUniqueId", pref.getString(Constants.USER_UNIQUE_ID, ""));
            personName.setText(getString(R.string.label_welcome_title) + ", " + pref.getString(Constants.MEMBER_EMAIL, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    private void showFetchResultLoading() {
        if (barProgressDialog == null) {
            barProgressDialog = new ProgressDialog(mActivity);
            barProgressDialog.setMessage(getString(R.string.label_wait_please_message));
            barProgressDialog.setProgressStyle(barProgressDialog.STYLE_SPINNER);
            barProgressDialog.setCancelable(false);
            barProgressDialog.show();
        }
    }

    private void closeFetchResultLoading() {
        if (barProgressDialog != null) {
            barProgressDialog.dismiss();
            barProgressDialog = null;
        }
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    private void displaySuccessMessage(String message, boolean isSucessMessage) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            if (isSucessMessage) {
                errorView.setBackgroundResource(R.color.success_message_background);
            } else {
                errorView.setBackgroundResource(R.color.error_message_background);
            }
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(mActivity.getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(mActivity.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private void getTokenFromServer() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(FlightBookingPassengerActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(FlightBookingPassengerActivity.this, FlightBookingPassengerActivity.this,
                Constants.CLIENT_AUTHENTICATION, "", obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void handleSignInSignOutFnctn() {
        if (isUserSignIn) {
            handleLogoutFunction();
//            preferences.edit().putString(Constants.MEMBER_EMAIL, "").apply();
//            preferences.edit().putString(Constants.MEMBER_ACCESS_TOKEN, "").apply();
//            preferences.edit().putString(Constants.MEMBER_TOKEN_TYPE, "").apply();
//            preferences.edit().putString(Constants.USER_UNIQUE_ID, "").apply();
//            preferences.edit().putBoolean(Constants.isFanClub, false).apply();
//            preferences.edit().putBoolean(Constants.IS_REWARDS_ENABLED, false).apply();
            signInDialog.callFacebookLogout();
            User weUser = WebEngage.get().user();
            weUser.logout();
            mSignInText.setText(getString(R.string.label_action_signin));
            personName.setText(getString(R.string.label_welcome_title) + " " + getString(R.string.label_guest_title));
            isUserSignIn = false;
        } else {
            signInDialog.setCancelable(false);
            loginDetailsSuccess = 1;
            signInDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    checkUserAccount();
                    loginDetailsSuccess = -1;
                }
            });
            signInDialog.show();
        }
    }

    private void handleLogoutFunction() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject obj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", pref.getString(Constants.MEMBER_ACCESS_TOKEN, ""));
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("timeStamp", timeStamp);
            obj.put("source", sourceJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        showFetchResultLoading();
        HTTPAsync async = new HTTPAsync(FlightBookingPassengerActivity.this, FlightBookingPassengerActivity.this,
                Constants.USER_LOGOUT, "", obj.toString(), USER_LOGOUT_SERVICE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void checkUserAccount() {
        final SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String memberAccessToken = preferences.getString(Constants.MEMBER_ACCESS_TOKEN, "");
        if (!memberAccessToken.equalsIgnoreCase("")) {
            mSignInText.setText(getString(R.string.label_action_sign_out));
            isUserSignIn = true;
            handleUserFetchResults();
        } else {
            mSignInText.setText(getString(R.string.label_action_signin));
            isUserSignIn = false;
            personName.setText(getString(R.string.label_welcome_title) + " " + getString(R.string.label_guest_title));
        }
    }
}
