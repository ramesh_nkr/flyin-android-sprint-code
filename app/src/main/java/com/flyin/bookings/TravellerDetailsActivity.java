package com.flyin.bookings;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.dialog.TravellerListDialog;
import com.flyin.bookings.listeners.TravellerItemUpdateListener;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.DbHandler;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TravellerDetailsActivity extends AppCompatActivity implements AsyncTaskListener {
    private TextView passengerTitle, passengerPassportCountry, passengerPassportExpiry, passengerBirth, passengerNationality, errorText, errorDescriptionText;
    private EditText passengerFirstName, passengerLastName, passengerMobile, passengerMail, passengerAddress, passengerPassportNo;
    private Typeface regularFace;
    private String title, firstName, lastName, email, mobile, address, passportNumber, passportCountry, passportExpiry, dateOfBirth, nationality, profileId, searchUrl;
    private TravellerItemUpdateListener travellerItemUpdateListener;
    private static final int GET_TRAVELER_RESPONSE = 1;
    private static final String TAG = "TravellerDetailsActivity";
    private boolean isEditProfile;
    private boolean isUserProfile;
    private LayoutInflater inflater;
    private View loadingView;
    private RelativeLayout loadingViewLayout;
    private boolean isInternetPresent = false;
    private ImageView errorImage;
    private Button searchButton;
    private Calendar calendar;
    private String currentDate = "";
    private String passportExp = "";
    private String passengerDOB = "";
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private MyReceiver myReceiver;
    private boolean isArabicLang;
    private static final int SEARCH_NATIONALITY_RESULT = 4;
    private static final int SEARCH_PASSPORT_RESULT = 5;
    private CheckBox alHilalCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travellerdetails);
        isInternetPresent = Utils.isConnectingToInternet(TravellerDetailsActivity.this);
        calendar = Calendar.getInstance();
        currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(calendar.getTime());
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        isArabicLang = false;
        passengerTitle = (TextView) findViewById(R.id.passenger_title);
        passengerFirstName = (EditText) findViewById(R.id.passenger_first_name);
        passengerLastName = (EditText) findViewById(R.id.passenger_last_name);
        passengerMobile = (EditText) findViewById(R.id.passenger_mobile);
        passengerMail = (EditText) findViewById(R.id.passenger_mail);
        passengerAddress = (EditText) findViewById(R.id.passenger_address);
        passengerPassportNo = (EditText) findViewById(R.id.passenger_passport_no);
        passengerPassportCountry = (TextView) findViewById(R.id.passenger_passport_country);
        passengerPassportExpiry = (TextView) findViewById(R.id.passenger_passport_expiry);
        passengerBirth = (TextView) findViewById(R.id.passenger_birth);
        passengerNationality = (TextView) findViewById(R.id.passenger_nationality);
        LinearLayout titleLayout = (LinearLayout) findViewById(R.id.passenger_title_layout);
        LinearLayout passportCountryLayout = (LinearLayout) findViewById(R.id.passport_country_layout);
        LinearLayout nationalityLayout = (LinearLayout) findViewById(R.id.nationality_layout);
        TextView travellerHeader = (TextView) findViewById(R.id.traveller_details_header);
        TextView travellerPassengerInformation = (TextView) findViewById(R.id.traveller_passenger_information);
        TextView additionalInformation = (TextView) findViewById(R.id.traveller_additional_information);
        Button saveButton = (Button) findViewById(R.id.save_button);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        alHilalCheckBox = (CheckBox) findViewById(R.id.fan_club_checkbox);
        if (Utils.isArabicLangSelected(TravellerDetailsActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegular = Constants.FONT_DROIDKUFI_REGULAR;
            isArabicLang = true;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                passengerTitle.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerFirstName.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerLastName.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerMobile.setTextDirection(View.TEXT_DIRECTION_LTR);
                passengerMail.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerAddress.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerPassportNo.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerPassportCountry.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerPassportExpiry.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerBirth.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerNationality.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
            travellerHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            travellerPassengerInformation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            additionalInformation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerFirstName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerLastName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerMobile.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerMail.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerAddress.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerPassportNo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerPassportCountry.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerPassportExpiry.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerBirth.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerNationality.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            saveButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            alHilalCheckBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        regularFace = Typeface.createFromAsset(getAssets(), fontRegular);
        travellerHeader.setTypeface(tf);
        travellerPassengerInformation.setTypeface(tf);
        additionalInformation.setTypeface(tf);
        saveButton.setTypeface(tf);
        passengerTitle.setTypeface(regularFace);
        passengerFirstName.setTypeface(regularFace);
        passengerLastName.setTypeface(regularFace);
        passengerMobile.setTypeface(regularFace);
        passengerMail.setTypeface(regularFace);
        passengerAddress.setTypeface(regularFace);
        passengerPassportNo.setTypeface(regularFace);
        passengerPassportCountry.setTypeface(regularFace);
        passengerPassportExpiry.setTypeface(regularFace);
        passengerBirth.setTypeface(regularFace);
        passengerNationality.setTypeface(regularFace);
        alHilalCheckBox.setTypeface(regularFace);
        passengerFirstName.setHint(Utils.getMandatoryFieldText(getString(R.string.label_traveller_firstName)));
        passengerLastName.setHint(Utils.getMandatoryFieldText(getString(R.string.label_traveller_lastName)));
        passengerMobile.setHint(Utils.getMandatoryFieldText(getString(R.string.label_traveller_mobile)));
        passengerMail.setHint(Utils.getMandatoryFieldText(getString(R.string.label_traveller_email)));
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_flight, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setTypeface(tf);
        backText.setTypeface(regularFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(TravellerDetailsActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(regularFace);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter("com.flyin.bookings.ERROR_MESSAGE");
        registerReceiver(myReceiver, intentFilter);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isEditProfile = bundle.getBoolean(Constants.IS_PROFILE_UPDATED, false);
            if (isEditProfile) {
                isUserProfile = bundle.getBoolean(Constants.USER_PROFILE_UPDATED, false);
                if (!isUserProfile) {
                    title = bundle.getString(Constants.USER_TITLE, "");
                    firstName = bundle.getString(Constants.USER_FIRST_NAME, "");
                    lastName = bundle.getString(Constants.USER_LAST_NAME, "");
                    email = bundle.getString(Constants.USER_MAIL, "");
                    mobile = bundle.getString(Constants.USER_PHONE, "");
                    address = bundle.getString(Constants.USER_ADDRESS, "");
                    passportNumber = bundle.getString(Constants.USER_PASSPORT_NUMBER, "");
                    passportCountry = bundle.getString(Constants.USER_PASSPORT_COUNTRY, "");
                    passportExpiry = bundle.getString(Constants.USER_PASSPORT_EXPIRY, "");
                    dateOfBirth = bundle.getString(Constants.USER_BIRTH, "");
                    nationality = bundle.getString(Constants.USER_NATIONALITY, "");
                    profileId = bundle.getString(Constants.USER_PROFILE_ID, "");
                    searchUrl = Constants.EDIT_TRAVELLER;
                    mTitleText.setText(R.string.label_user_edit_traveller);
                } else {
                    title = bundle.getString(Constants.USER_TITLE, "");
                    firstName = bundle.getString(Constants.USER_FIRST_NAME, "");
                    lastName = bundle.getString(Constants.USER_LAST_NAME, "");
                    email = bundle.getString(Constants.USER_MAIL, "");
                    mobile = bundle.getString(Constants.USER_PHONE, "");
                    address = bundle.getString(Constants.USER_ADDRESS, "");
                    passportNumber = bundle.getString(Constants.USER_PASSPORT_NUMBER, "");
                    passportCountry = bundle.getString(Constants.USER_PASSPORT_COUNTRY, "");
                    passportExpiry = bundle.getString(Constants.USER_PASSPORT_EXPIRY, "");
                    dateOfBirth = bundle.getString(Constants.USER_BIRTH, "");
                    nationality = bundle.getString(Constants.USER_NATIONALITY, "");
                    searchUrl = Constants.EDIT_PROFILE;
                    mTitleText.setText(R.string.label_user_edit_profile);
                    //passengerMail.setEnabled(false);
                    alHilalCheckBox.setVisibility(View.VISIBLE);
                }
            } else {
                profileId = "";
                searchUrl = Constants.ADD_TRAVELLER;
                mTitleText.setText(R.string.label_user_add_traveller);
                title = "";
            }
        }
        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        if (preferences.getBoolean(Constants.isFanClub, false)) {
            alHilalCheckBox.setChecked(true);
        } else {
            alHilalCheckBox.setChecked(false);
        }
        String userTitle = title;
        if (!Utils.isArabicLangSelected(TravellerDetailsActivity.this)) {
            if (userTitle.equalsIgnoreCase("Mr")) {
                userTitle = getResources().getString(R.string.adult_title);
            } else if (userTitle.equalsIgnoreCase("Miss")) {
                userTitle = getResources().getString(R.string.label_miss);
            } else if (userTitle.equalsIgnoreCase("Mrs")) {
                userTitle = getResources().getString(R.string.label_mrs);
            } else if (userTitle.equalsIgnoreCase("Mstr")) {
                userTitle = getResources().getString(R.string.child_title);
            }
        }
        passengerTitle.setText(userTitle);
        passengerFirstName.setText(firstName);
        passengerLastName.setText(lastName);
        passengerMobile.setText(mobile);
        passengerMail.setText(email);
        passengerAddress.setText(address);
        passengerPassportNo.setText(passportNumber);
        DbHandler handler = new DbHandler(TravellerDetailsActivity.this);
        passengerPassportCountry.setText(handler.getCountryNameText(passportCountry));
        if (passportExpiry == null) {
            passengerPassportExpiry.setText("");
        }
        if (passportExpiry != null && !passportExpiry.equalsIgnoreCase("")) {
            passengerPassportExpiry.setText(Utils.formatTimeStampToPassportDate(passportExpiry));
        }
        if (dateOfBirth == null) {
            passengerBirth.setText("");
        }
        if (dateOfBirth != null && !dateOfBirth.equalsIgnoreCase("")) {
            passengerBirth.setText(Utils.formatTimeStampToPassportDate(dateOfBirth));
        }
        passengerNationality.setText(handler.getCountryNameText(nationality));
        String[] titleArray = getResources().getStringArray(R.array.title_selection);
        if (passengerTitle.getText().toString().isEmpty()) {
            passengerTitle.setText(titleArray[0]);
        }
        titleLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog titleDialog = new TravellerListDialog(TravellerDetailsActivity.this, Constants.TITLE_SELECTION, null, "", null, false);
                titleDialog.setCancelable(true);
                titleDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (titleDialog != null) {
                            if (!titleDialog.selectedListItem.isEmpty()) {
                                title = titleDialog.selectedListItem;
                                passengerTitle.setText(title);
                                if (!titleDialog.selectedListItem.equalsIgnoreCase("")) {
                                    if (travellerItemUpdateListener != null) {
                                        travellerItemUpdateListener.onItemClick(0, Constants.TITLE_SELECTION, title, titleDialog.selectedListCode, 0, false, false);
                                    }
                                }
                            }
                        }
                    }
                });
                titleDialog.show();
            }
        });
        passportCountryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(TravellerDetailsActivity.this, SearchCountryActivity.class);
                startActivityForResult(it, SEARCH_PASSPORT_RESULT);
            }
        });
        passengerPassportExpiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isArabicLang) {
                    String languageToLoad = "en";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                }
                Calendar c = Calendar.getInstance();
                int mYear = 0;
                int mMonth = 0;
                int mDay = 0;
                if (passengerPassportExpiry.getText().toString().isEmpty()) {
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    String selectedPassportExp = passengerPassportExpiry.getText().toString();
                    String[] expDate = selectedPassportExp.split("-");
                    mYear = Integer.parseInt(expDate[2]);
                    mMonth = Integer.parseInt(expDate[1]) - 1;
                    mDay = Integer.parseInt(expDate[0]);
                }
                final DatePickerDialog dialog = new DatePickerDialog(TravellerDetailsActivity.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        String selectedDate = "";
                        calendar.set(year, month, day);
                        selectedDate = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(calendar.getTime());
                        if (isArabicLang) {
                            String languageToLoad = "ar";
                            Locale locale = new Locale(languageToLoad);
                            Locale.setDefault(locale);
                            Configuration config = new Configuration();
                            config.locale = locale;
                            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                        }
                        passengerPassportExpiry.setText(selectedDate);
                    }
                }, mYear, mMonth, mDay);
                dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                dialog.show();
            }
        });
        passengerBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isArabicLang) {
                    String languageToLoad = "en";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                }
                Calendar c = Calendar.getInstance();
                int mYear = 0;
                int mMonth = 0;
                int mDay = 0;
                if (passengerBirth.getText().toString().isEmpty()) {
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    String selectedBirthDate = passengerBirth.getText().toString();
                    String[] expDate = selectedBirthDate.split("-");
                    mYear = Integer.parseInt(expDate[2]);
                    mMonth = Integer.parseInt(expDate[1]) - 1;
                    mDay = Integer.parseInt(expDate[0]);
                }
                final DatePickerDialog dialog = new DatePickerDialog(TravellerDetailsActivity.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String selectedDate = "";
                        calendar.set(year, monthOfYear, dayOfMonth);
                        selectedDate = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(calendar.getTime());
                        if (isArabicLang) {
                            String languageToLoad = "ar";
                            Locale locale = new Locale(languageToLoad);
                            Locale.setDefault(locale);
                            Configuration config = new Configuration();
                            config.locale = locale;
                            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                        }
                        passengerBirth.setText(selectedDate);
                    }
                }, mYear, mMonth, mDay);
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
                dialog.show();
            }
        });
        nationalityLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(TravellerDetailsActivity.this, SearchCountryActivity.class);
                startActivityForResult(it, SEARCH_NATIONALITY_RESULT);
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passengerTitle.getText().toString().isEmpty()) {
                    displayErrorMessage(getString(R.string.label_err_title_msg));
                    return;
                }
                if (passengerFirstName.getText().toString().isEmpty()) {
                    displayErrorMessage(getString(R.string.label_err_fname_msg));
                    return;
                }
                if (!Utils.isValidName(passengerFirstName.getText().toString())) {
                    displayErrorMessage(getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_firstName));
                    return;
                }
                if (passengerLastName.getText().toString().isEmpty()) {
                    displayErrorMessage(getString(R.string.label_err_lname_msg));
                    return;
                }
                if (!Utils.isValidName(passengerLastName.getText().toString())) {
                    displayErrorMessage(getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_lastName));
                    return;
                }
                if (passengerMobile.getText().toString().isEmpty() || !Utils.isValidMobile(passengerMobile.getText().toString())) {
                    displayErrorMessage(getString(R.string.label_err_phone_msg));
                    return;
                }
                if (!Utils.isValidEmail(passengerMail.getText().toString())) {
                    displayErrorMessage(getString(R.string.label_error_email_valid_message));
                    return;
                }
                if (!passengerPassportNo.getText().toString().isEmpty()) {
                    if (!Utils.isValidPassport(passengerPassportNo.getText().toString(), Constants.PASSPOR_NUMBER)) {
                        displayErrorMessage(getString(R.string.label_error_passport_valid_message));
                        return;
                    }
                }
                if (!passengerPassportExpiry.getText().toString().isEmpty()) {
                    if (Integer.parseInt(Utils.dateDifference(passengerPassportExpiry.getText().toString(), currentDate)) < 0) {
                        displayErrorMessage(getString(R.string.label_error_passport_expiry_message));
                        return;
                    }
                }
                if (!passengerBirth.getText().toString().isEmpty()) {
                    if (Integer.parseInt(Utils.dateDifference(currentDate, passengerBirth.getText().toString())) < 1) {
                        displayErrorMessage(getString(R.string.label_error_dob_message));
                        return;
                    }
                }
                final String travelerRequestJSON = travelerDetailsTask();
                isInternetPresent = Utils.isConnectingToInternet(TravellerDetailsActivity.this);
                if (isInternetPresent) {
                    getTravelerRequestFromServer(travelerRequestJSON);
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(regularFace);
                    errorDescriptionText.setTypeface(regularFace);
                    searchButton.setTypeface(regularFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(TravellerDetailsActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                getTravelerRequestFromServer(travelerRequestJSON);
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        });
    }

    private String travelerDetailsTask() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            //mainJSON.put("customerId", pref.getString(Constants.MEMBER_EMAIL, ""));
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(TravellerDetailsActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(TravellerDetailsActivity.this));
            mainJSON.put("source", sourceJSON);
            mainJSON.put("userUniqueId", pref.getString(Constants.USER_UNIQUE_ID, ""));
            JSONObject customerDetailsJSON = new JSONObject();
            String userTitle = "";
            if (passengerTitle.getText().toString().equalsIgnoreCase(getString(R.string.adult_title)) || passengerTitle.getText().toString().equalsIgnoreCase("Mr")) {
                userTitle = "Mr";
            } else if (passengerTitle.getText().toString().equalsIgnoreCase(getString(R.string.label_miss)) || passengerTitle.getText().toString().equalsIgnoreCase("Miss")) {
                userTitle = "Miss";
            } else if (passengerTitle.getText().toString().equalsIgnoreCase(getString(R.string.label_mrs)) || passengerTitle.getText().toString().equalsIgnoreCase("Mrs")) {
                userTitle = "Mrs";
            } else if (passengerTitle.getText().toString().equalsIgnoreCase(getString(R.string.child_title)) || passengerTitle.getText().toString().equalsIgnoreCase("Mstr")) {
                userTitle = "Mstr";
            }
            customerDetailsJSON.put("title", userTitle);
            customerDetailsJSON.put("firstName", passengerFirstName.getText().toString());
            customerDetailsJSON.put("lastName", passengerLastName.getText().toString());
            customerDetailsJSON.put("mobileNumber", passengerMobile.getText().toString());
            customerDetailsJSON.put("mobileAreaCode", "");
            customerDetailsJSON.put("email", passengerMail.getText().toString().toLowerCase());
            customerDetailsJSON.put("address", passengerAddress.getText().toString());
            customerDetailsJSON.put("country", Utils.getUserCountry(TravellerDetailsActivity.this));
//            customerDetailsJSON.put("contactEmailId", passengerMail.getText().toString().toLowerCase());
            if (isEditProfile) {
                if (!isUserProfile) {
                    customerDetailsJSON.put("profileId", profileId);
                } else {
                    if (alHilalCheckBox.isChecked()) {
                        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(Constants.isFanClub, true).apply();
                        customerDetailsJSON.put("groupType", "ALHILAL");
                    } else {
                        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(Constants.isFanClub, false).apply();
                        customerDetailsJSON.put("groupType", Utils.checkStringValue("null"));
                    }
                    mainJSON.put("registerEmailId", pref.getString(Constants.MEMBER_EMAIL, ""));
                }
            }
            mainJSON.put("customerBasicDetails", customerDetailsJSON);
            if (!passengerPassportExpiry.getText().toString().isEmpty()) {
                passportExp = Utils.birthDateToTimeStampFormat(passengerPassportExpiry.getText().toString());
            } else {
                passportExp = "";
            }
            if (!passengerBirth.getText().toString().isEmpty()) {
                passengerDOB = Utils.birthDateToTimeStampFormat(passengerBirth.getText().toString());
            } else {
                passengerDOB = "";
            }
            DbHandler dbHandler = new DbHandler(TravellerDetailsActivity.this);
            String countryCode = dbHandler.getCountryCode(passengerPassportCountry.getText().toString());
            String nationalityCode = dbHandler.getCountryCode(passengerNationality.getText().toString());
            JSONObject passportDetailsJSON = new JSONObject();
            passportDetailsJSON.put("docType", 2);
            passportDetailsJSON.put("passportNo", Utils.checkStringValue(passengerPassportNo.getText().toString()));
            passportDetailsJSON.put("countryIssued", Utils.checkStringValue(countryCode));
            passportDetailsJSON.put("expiryDate", Utils.checkStringValue(passportExp));
            passportDetailsJSON.put("dateofBirth", Utils.checkStringValue(passengerDOB));
            passportDetailsJSON.put("nationality", Utils.checkStringValue(nationalityCode));
            passportDetailsJSON.put("issueDate", Utils.checkStringValue(""));
            mainJSON.put("passportDetails", passportDetailsJSON);
//            JSONObject hPrefJSON = new JSONObject();
            mainJSON.put("htlPref", null);
//            JSONObject fPrefJSON = new JSONObject();
            mainJSON.put("fltPref", null);
//            JSONObject ffpJSON = new JSONObject();
            mainJSON.put("ffp", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private JSONObject extraJsonParma() {
        JSONObject parmaObj = new JSONObject();
        return null;
    }

    private void getTravelerRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(TravellerDetailsActivity.this, TravellerDetailsActivity.this, searchUrl, "", requestJSON,
                GET_TRAVELER_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "DATA::" + data);
        closeLoading();
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(regularFace);
            errorDescriptionText.setTypeface(regularFace);
            searchButton.setTypeface(regularFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadingViewLayout.removeView(errorView);
                    finish();
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        } else {
            JSONObject obj = new JSONObject();
            try {
                obj = new JSONObject(data);
                if (obj.has("status")) {
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        if (isUserProfile) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(TravellerDetailsActivity.this);
                            alertDialog.setCancelable(false);
                            alertDialog.setMessage(getString(R.string.label_profile_update_success_message));
                            alertDialog.setNegativeButton(getString(R.string.label_selected_flight_message), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    boolean isTravelerProfileUpdated = false;
                                    Intent it = new Intent(TravellerDetailsActivity.this, UserProfileActivity.class);
                                    it.putExtra(Constants.IS_USER_PROFILE_UPDATED, isTravelerProfileUpdated);
                                    setResult(RESULT_OK, it);
                                    finish();
                                }
                            });
                            alertDialog.show();
                        } else {
                            boolean isTravelerDataUpdated = false;
                            Intent it = new Intent(TravellerDetailsActivity.this, UserProfileActivity.class);
                            it.putExtra(Constants.IS_EDIT_PROFILE_UPDATED, isTravelerDataUpdated);
                            setResult(RESULT_OK, it);
                            finish();
                        }
                    } else {
                        if (obj.has("msg")) {
                            JSONArray msgArr = obj.getJSONArray("msg");
                            Utils.printMessage(TAG, "Err MSG :: " + msgArr.getString(0));
                            displayErrorMessage(msgArr.getString(0));
                            return;
                        } else {
                            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View errorView = inflater.inflate(R.layout.view_error_response, null);
                            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                            errorText = (TextView) errorView.findViewById(R.id.error_text);
                            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                            searchButton = (Button) errorView.findViewById(R.id.search_button);
                            errorText.setTypeface(regularFace);
                            errorDescriptionText.setTypeface(regularFace);
                            searchButton.setTypeface(regularFace);
                            searchButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    loadingViewLayout.removeView(errorView);
                                    finish();
                                }
                            });
                            loadErrorType(Constants.WRONG_ERROR_PAGE);
                            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            loadingViewLayout.addView(errorView);
                        }
                    }
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(regularFace);
                    errorDescriptionText.setTypeface(regularFace);
                    searchButton.setTypeface(regularFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadingViewLayout.removeView(errorView);
                            finish();
                        }
                    });
                    loadErrorType(Constants.WRONG_ERROR_PAGE);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(regularFace);
        descriptionText.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(TravellerDetailsActivity.this)) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.ERROR_MESSAGE")) {
                displayErrorMessage(intent.getStringExtra("message"));
            }
        }
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver);
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(getResources().getString(R.string.label_network_error_message));
                searchButton.setText(getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getResources().getString(R.string.label_no_results_found_message));
                searchButton.setText(getResources().getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //  super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SEARCH_PASSPORT_RESULT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String passportCountryName = data.getStringExtra(Constants.SELECTED_COUNTRY_NAME);
                    if (!passportCountryName.equalsIgnoreCase("")) {
                        Utils.printMessage(TAG, "passport :: " + passportCountryName);
                        passengerPassportCountry.setText(passportCountryName);
                    }
                }
            }
        }
        if (requestCode == SEARCH_NATIONALITY_RESULT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String nationalityCountryName = data.getStringExtra(Constants.SELECTED_COUNTRY_NAME);
                    if (!nationalityCountryName.equalsIgnoreCase("")) {
                        Utils.printMessage(TAG, "nationality :: " + nationalityCountryName);
                        passengerNationality.setText(nationalityCountryName);
                    }
                }
            }
        }
    }
}
