package com.flyin.bookings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.adapter.UserAdapter;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.TravellerDetailsBean;
import com.flyin.bookings.model.UserProfileBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

public class UserProfileActivity extends AppCompatActivity implements OnCustomItemSelectListener, AsyncTaskListener {
    private ArrayList<TravellerDetailsBean> travellerArrayList;
    private ArrayList<UserProfileBean> travellerProfileList;
    private ListView listView;
    private UserAdapter listPersons;
    private static final int GET_TRAVELER_PROFILE = 1;
    private static final int GET_TRAVELER_DETAILS = 2;
    private static final int GET_DELETE_TRAVELER_DETAILS = 3;
    private static final int EDIT_TRAVELER_DETAILS = 4;
    private static final int GET_EDIT_PROFILE = 5;
    private static final String TAG = "UserProfileActivity";
    private LinearLayout userMobileLayout = null;
    private RelativeLayout loadingViewLayout = null;
    private TextView personName = null;
    private TextView personEmail = null;
    private TextView personPhone = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private Typeface titleFace = null;
    private String profileId = "";
    private LayoutInflater inflater = null;
    private View loadingView = null;
    private boolean isInternetPresent = false;
    private ImageView errorImage = null;
    private Button searchButton = null;
    private String requestUserDetailsJSON = "";
    private String emailId = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userprofile);
        isInternetPresent = Utils.isConnectingToInternet(UserProfileActivity.this);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        travellerArrayList = new ArrayList<>();
        travellerProfileList = new ArrayList<>();
        personName = (TextView) findViewById(R.id.person_name);
        personEmail = (TextView) findViewById(R.id.person_email);
        personPhone = (TextView) findViewById(R.id.person_phone);
        TextView personNameList = (TextView) findViewById(R.id.person_names_list);
        Button addButton = (Button) findViewById(R.id.add_button);
        userMobileLayout = (LinearLayout) findViewById(R.id.user_mobile_layout);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        ImageView profileEditImage = (ImageView) findViewById(R.id.profile_edit_image);
        if (Utils.isArabicLangSelected(UserProfileActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            personName.setGravity(Gravity.END);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                personName.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            }
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        titleFace = Typeface.createFromAsset(getAssets(), fontTitle);
        personName.setTypeface(tf);
        personEmail.setTypeface(titleFace);
        personPhone.setTypeface(titleFace);
        personNameList.setTypeface(tf);
        addButton.setTypeface(tf);
        listView = (ListView) findViewById(R.id.user_list_view);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserProfileActivity.this, TravellerDetailsActivity.class);
                intent.putExtra(Constants.IS_PROFILE_UPDATED, false);
                startActivityForResult(intent, EDIT_TRAVELER_DETAILS);
            }
        });
        profileEditImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travellerProfileList.size() == 0) {
                    return;
                }
                Intent intent = new Intent(UserProfileActivity.this, TravellerDetailsActivity.class);
                intent.putExtra(Constants.USER_PROFILE_UPDATED, true);
                intent.putExtra(Constants.IS_PROFILE_UPDATED, true);
                intent.putExtra(Constants.USER_TITLE, Utils.checkStringVal(travellerProfileList.get(0).getTitle()));
                intent.putExtra(Constants.USER_FIRST_NAME, Utils.checkStringVal(travellerProfileList.get(0).getFirstName()));
                intent.putExtra(Constants.USER_LAST_NAME, Utils.checkStringVal(travellerProfileList.get(0).getLastName()));
                intent.putExtra(Constants.USER_MAIL, Utils.checkStringVal(travellerProfileList.get(0).getEmail()));
                intent.putExtra(Constants.USER_PHONE, Utils.checkStringVal(travellerProfileList.get(0).getMobileNo()));
                intent.putExtra(Constants.USER_ADDRESS, Utils.checkStringVal(travellerProfileList.get(0).getAddress()));
                intent.putExtra(Constants.USER_PASSPORT_NUMBER, Utils.checkStringVal(travellerProfileList.get(0).getPassportNumber()));
                intent.putExtra(Constants.USER_PASSPORT_COUNTRY, Utils.checkStringVal(travellerProfileList.get(0).getPassportIssuingCountry()));
                intent.putExtra(Constants.USER_PASSPORT_EXPIRY, Utils.checkStringVal(travellerProfileList.get(0).getPassportExpiry()));
                intent.putExtra(Constants.USER_BIRTH, Utils.checkStringVal(travellerProfileList.get(0).getDateOfBirth()));
                intent.putExtra(Constants.USER_NATIONALITY, Utils.checkStringVal(travellerProfileList.get(0).getNationality()));
                startActivityForResult(intent, GET_EDIT_PROFILE);
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_user_myProfile);
        mTitleText.setTypeface(tf);
        backText.setTypeface(titleFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(UserProfileActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        requestUserDetailsJSON = travelerDetailsTask();
        if (isInternetPresent) {
            getTravelerProfileRequestFromServer(requestUserDetailsJSON);
            getTravelerDetailsRequestFromServer(requestUserDetailsJSON);
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(titleFace);
            errorDescriptionText.setTypeface(titleFace);
            searchButton.setTypeface(titleFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(UserProfileActivity.this);
                    if (isInternetPresent) {
                        loadingViewLayout.removeView(errorView);
                        getTravelerProfileRequestFromServer(requestUserDetailsJSON);
                        getTravelerDetailsRequestFromServer(requestUserDetailsJSON);
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
    }

    @Override
    public void onItemClick(int position) {
        boolean expandStatus = travellerArrayList.get(position).getIsExpanded();
        for (TravellerDetailsBean travellerDetailsBean : travellerArrayList) {
            travellerDetailsBean.setIsExpanded(false);
        }
        travellerArrayList.get(position).setIsExpanded(!expandStatus);
        listPersons.notifyDataSetChanged();
    }

    @Override
    public void onSelectedItemClick(int position, int type) {
        switch (type) {
            case Constants.EDIT_BUTTON_SELECTION:
                Intent intent = new Intent(UserProfileActivity.this, TravellerDetailsActivity.class);
                intent.putExtra(Constants.USER_PROFILE_UPDATED, false);
                intent.putExtra(Constants.IS_PROFILE_UPDATED, true);
                intent.putExtra(Constants.USER_TITLE, Utils.checkStringVal(travellerArrayList.get(position).getTitle()));
                intent.putExtra(Constants.USER_FIRST_NAME, Utils.checkStringVal(travellerArrayList.get(position).getFirstName()));
                intent.putExtra(Constants.USER_LAST_NAME, Utils.checkStringVal(travellerArrayList.get(position).getLastName()));
                intent.putExtra(Constants.USER_MAIL, Utils.checkStringVal(travellerArrayList.get(position).getEmail()));
                intent.putExtra(Constants.USER_PHONE, Utils.checkStringVal(travellerArrayList.get(position).getPhoneNo()));
                intent.putExtra(Constants.USER_ADDRESS, Utils.checkStringVal(travellerArrayList.get(position).getAddress()));
                intent.putExtra(Constants.USER_PASSPORT_NUMBER, Utils.checkStringVal(travellerArrayList.get(position).getPassportNumber()));
                intent.putExtra(Constants.USER_PASSPORT_COUNTRY, Utils.checkStringVal(travellerArrayList.get(position).getPassportIssuingCountry()));
                intent.putExtra(Constants.USER_PASSPORT_EXPIRY, Utils.checkStringVal(travellerArrayList.get(position).getPassportExpiry()));
                intent.putExtra(Constants.USER_BIRTH, Utils.checkStringVal(travellerArrayList.get(position).getDateOfBirth()));
                intent.putExtra(Constants.USER_NATIONALITY, Utils.checkStringVal(travellerArrayList.get(position).getNationality()));
                intent.putExtra(Constants.USER_PROFILE_ID, Utils.checkStringVal(travellerArrayList.get(position).getProfileId()));
                startActivityForResult(intent, EDIT_TRAVELER_DETAILS);
                break;
            case Constants.DELETE_BUTTON_SELECTION:
                profileId = travellerArrayList.get(position).getProfileId();
                final String travelerDeleteRequestJSON = travelerDeleteDetailsTask();
                isInternetPresent = Utils.isConnectingToInternet(UserProfileActivity.this);
                if (isInternetPresent) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(UserProfileActivity.this);
                    alertDialog.setCancelable(true);
                    alertDialog.setTitle(getString(R.string.delete_traveller_label));
                    alertDialog.setMessage(getString(R.string.label_delete_traveller_msg));
                    alertDialog.setNegativeButton(getString(R.string.label_selected_flight_message), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            showLoading();
                            HTTPAsync async = new HTTPAsync(UserProfileActivity.this, UserProfileActivity.this, Constants.DELETE_TRAVELLER, "", travelerDeleteRequestJSON,
                                    GET_DELETE_TRAVELER_DETAILS, HTTPAsync.METHOD_POST);
                            async.execute();
                        }
                    });
                    alertDialog.setPositiveButton(getString(R.string.label_no_button).toLowerCase(), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                } else {
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(titleFace);
                    errorDescriptionText.setTypeface(titleFace);
                    searchButton.setTypeface(titleFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isInternetPresent = Utils.isConnectingToInternet(UserProfileActivity.this);
                            if (isInternetPresent) {
                                loadingViewLayout.removeView(errorView);
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(UserProfileActivity.this);
                                alertDialog.setCancelable(true);
                                alertDialog.setTitle(getResources().getString(R.string.delete_traveller_label));
                                alertDialog.setMessage(getResources().getString(R.string.label_delete_traveller_msg));
                                alertDialog.setNegativeButton(getResources().getString(R.string.label_selected_flight_message), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        showLoading();
                                        HTTPAsync async = new HTTPAsync(UserProfileActivity.this, UserProfileActivity.this, Constants.DELETE_TRAVELLER, "", travelerDeleteRequestJSON,
                                                GET_DELETE_TRAVELER_DETAILS, HTTPAsync.METHOD_POST);
                                        async.execute();
                                    }
                                });
                                alertDialog.setPositiveButton(getResources().getString(R.string.label_no_button).toLowerCase(), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                                alertDialog.show();
                            }
                        }
                    });
                    loadErrorType(Constants.NETWORK_ERROR);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
                break;
        }
    }

    private void getTravelerProfileRequestFromServer(String requestJSON) {
        HTTPAsync async = new HTTPAsync(UserProfileActivity.this, UserProfileActivity.this, Constants.USER_PROFILE, "", requestJSON,
                GET_TRAVELER_PROFILE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void getTravelerDetailsRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(UserProfileActivity.this, UserProfileActivity.this, Constants.FETCH_TRAVELLER, "", requestJSON,
                GET_TRAVELER_DETAILS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String travelerDeleteDetailsTask() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(UserProfileActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(UserProfileActivity.this));
            mainJSON.put("source", sourceJSON);
//            mainJSON.put("customerId", pref.getString(Constants.MEMBER_EMAIL, ""));
            mainJSON.put("profileId", profileId);
            mainJSON.put("userUniqueId", pref.getString(Constants.USER_UNIQUE_ID, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        JSONObject obj = new JSONObject();
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(titleFace);
            errorDescriptionText.setTypeface(titleFace);
            searchButton.setTypeface(titleFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(UserProfileActivity.this);
                    if (isInternetPresent) {
                        loadingViewLayout.removeView(errorView);
                        finish();
                    }
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        } else if (serviceType == GET_TRAVELER_PROFILE) {
            Utils.printMessage(TAG, "Profile DATA:: " + data);
            try {
                obj = new JSONObject(data);
                if (obj.has("registerEmailId")) {
                    emailId = obj.getString("registerEmailId");
                    personEmail.setText(emailId);
                    UserProfileBean userProfileBean = new UserProfileBean();
                    userProfileBean.setEmail(emailId);
                    if (obj.has("customerBasicDetails")) {
                        JSONObject customerBasicDetails = obj.getJSONObject("customerBasicDetails");
                        if (customerBasicDetails.has("title")) {
                            userProfileBean.setTitle(customerBasicDetails.getString("title"));
                        }
                        if (customerBasicDetails.has("firstName")) {
                            userProfileBean.setFirstName(customerBasicDetails.getString("firstName"));
                        }
                        if (customerBasicDetails.has("middleName")) {
                            userProfileBean.setMiddleName(customerBasicDetails.getString("middleName"));
                        }
                        if (customerBasicDetails.has("lastName")) {
                            userProfileBean.setLastName(customerBasicDetails.getString("lastName"));
                        }
                        if (customerBasicDetails.has("mobileNumber")) {
                            userProfileBean.setMobileNo(customerBasicDetails.getString("mobileNumber"));
                        }
                        if (customerBasicDetails.has("mobileAreaCode")) {
                            userProfileBean.setMobileAreaCode(customerBasicDetails.getString("mobileAreaCode"));
                        }
                        if (customerBasicDetails.has("address")) {
                            userProfileBean.setAddress(customerBasicDetails.getString("address"));
                        }
                        if (customerBasicDetails.has("city")) {
                            userProfileBean.setCity(customerBasicDetails.getString("city"));
                        }
                        if (customerBasicDetails.has("country")) {
                            userProfileBean.setCountry(customerBasicDetails.getString("country"));
                        }
                        if (customerBasicDetails.has("groupType")) {
                            if (!customerBasicDetails.getString("groupType").equalsIgnoreCase("null") || customerBasicDetails.getString("groupType") != null) {
                                String groupType = customerBasicDetails.getString("groupType");
                                if (groupType.equalsIgnoreCase("ALHILAL")) {
                                    SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putBoolean(Constants.isFanClub, true);
                                    editor.apply();
                                } else {
                                    SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putBoolean(Constants.isFanClub, false);
                                    editor.apply();
                                }
                            } else {
                                SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putBoolean(Constants.isFanClub, false);
                                editor.apply();
                            }
                        }
                        if (customerBasicDetails.has("contactEmailId")) {
                            userProfileBean.setContactEmailId(customerBasicDetails.getString("contactEmailId"));
                        }
                    }
                    if (obj.has("passportDetails")) {
                        String passportData = obj.getString("passportDetails");
                        Object json = new JSONTokener(passportData).nextValue();
                        if (json instanceof JSONObject) {
                            JSONObject passportDetails = obj.getJSONObject("passportDetails");
                            if (passportDetails.has("issuedate")) {
                                userProfileBean.setPassportIssueDate(passportDetails.getString("issuedate"));
                            }
                            if (passportDetails.has("passportNo")) {
                                userProfileBean.setPassportNumber(passportDetails.getString("passportNo"));
                            }
                            if (passportDetails.has("expiryDate")) {
                                userProfileBean.setPassportExpiry(passportDetails.getString("expiryDate"));
                            }
                            if (passportDetails.has("dateofBirth")) {
                                userProfileBean.setDateOfBirth(passportDetails.getString("dateofBirth"));
                            }
                            if (passportDetails.has("nationality")) {
                                userProfileBean.setNationality(passportDetails.getString("nationality"));
                            }
                            if (passportDetails.has("countryIssued")) {
                                userProfileBean.setPassportIssuingCountry(passportDetails.getString("countryIssued"));
                            }
                        } else if (json instanceof JSONArray) {
                            if (!obj.getJSONArray("passportDetails").isNull(0)) {
                                JSONArray passportDetailsArray = obj.getJSONArray("passportDetails");
                                for (int i = 0; i < passportDetailsArray.length(); i++) {
                                    JSONObject passportDetails = passportDetailsArray.getJSONObject(i);
                                    if (passportDetails.has("docType")) {
                                        String documentType = passportDetails.getString("docType");
                                        if (documentType.equalsIgnoreCase("2")) {
                                            if (passportDetails.has("issuedate")) {
                                                userProfileBean.setPassportIssueDate(passportDetails.getString("issuedate"));
                                            }
                                            if (passportDetails.has("passportNo")) {
                                                userProfileBean.setPassportNumber(passportDetails.getString("passportNo"));
                                            }
                                            if (passportDetails.has("expiryDate")) {
                                                userProfileBean.setPassportExpiry(passportDetails.getString("expiryDate"));
                                            }
                                            if (passportDetails.has("dateofBirth")) {
                                                userProfileBean.setDateOfBirth(passportDetails.getString("dateofBirth"));
                                            }
                                            if (passportDetails.has("nationality")) {
                                                userProfileBean.setNationality(passportDetails.getString("nationality"));
                                            }
                                            if (passportDetails.has("countryIssued")) {
                                                userProfileBean.setPassportIssuingCountry(passportDetails.getString("countryIssued"));
                                            }
                                            userProfileBean.setDocType(passportDetails.getString("docType"));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    travellerProfileList.add(userProfileBean);
                    if (userProfileBean.getTitle().equalsIgnoreCase("") || userProfileBean.getTitle().equalsIgnoreCase("null") || userProfileBean.getFirstName().equalsIgnoreCase("null") || userProfileBean.getLastName().equalsIgnoreCase("null") || userProfileBean.getFirstName().equalsIgnoreCase("") || userProfileBean.getLastName().equalsIgnoreCase("")) {
                        personName.setVisibility(View.INVISIBLE);
                    } else {
                        personName.setText(userProfileBean.getTitle() + " " + userProfileBean.getFirstName() + " " + userProfileBean.getLastName());
                        personName.setVisibility(View.VISIBLE);
                    }
                    if (userProfileBean.getMobileNo().equalsIgnoreCase("null") || userProfileBean.getMobileNo().equalsIgnoreCase("")) {
                        userMobileLayout.setVisibility(View.GONE);
                    } else {
                        personPhone.setText(userProfileBean.getMobileNo());
                        userMobileLayout.setVisibility(View.VISIBLE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (serviceType == GET_TRAVELER_DETAILS) {
            Utils.printMessage(TAG, "Traveller DATA:: " + data);
            closeLoading();
            try {
                obj = new JSONObject(data);
                if (obj.has("customerDetails")) {
                    JSONArray customerDetailsArray = obj.getJSONArray("customerDetails");
                    for (int i = 0; i < customerDetailsArray.length(); i++) {
                        TravellerDetailsBean travellerBean = new TravellerDetailsBean();
                        if (customerDetailsArray.getJSONObject(i).has("basicDetail")) {
                            JSONObject basicDetailsObj = customerDetailsArray.getJSONObject(i).getJSONObject("basicDetail");
                            travellerBean.setTitle(Utils.checkStringVal(basicDetailsObj.getString("title")));
                            travellerBean.setFirstName(Utils.checkStringVal(basicDetailsObj.getString("firstName")));
                            travellerBean.setLastName(Utils.checkStringVal(basicDetailsObj.getString("lastName")));
                            travellerBean.setPhoneNo(Utils.checkStringVal(basicDetailsObj.getString("mobileNumber")));
                            travellerBean.setMobileAreaCode(Utils.checkStringVal(basicDetailsObj.getString("mobileAreaCode")));
                            travellerBean.setAddress(Utils.checkStringVal(basicDetailsObj.getString("address")));
                            travellerBean.setEmail(Utils.checkStringVal(basicDetailsObj.getString("email")));
                            travellerBean.setProfileId(Utils.checkStringVal(basicDetailsObj.getString("profileID")));
                            travellerBean.setCountry(Utils.checkStringVal(basicDetailsObj.getString("country")));
                            travellerBean.setRegUser(basicDetailsObj.getBoolean("regUser"));
                        }
                        if (customerDetailsArray.getJSONObject(i).has("passportDetails") && !customerDetailsArray.getJSONObject(i).getJSONArray("passportDetails").isNull(0)) {
                            JSONArray passportDetailsArray = customerDetailsArray.getJSONObject(i).getJSONArray("passportDetails");
                            for (int j = 0; j < passportDetailsArray.length(); j++) {
                                JSONObject passportDetailsObj = passportDetailsArray.getJSONObject(j);
                                if (passportDetailsObj.has("docType")) {
                                    String documentType = passportDetailsObj.getString("docType");
                                    if (documentType.equalsIgnoreCase("2")) {
                                        travellerBean.setPassportIssueDate(Utils.checkStringVal(passportDetailsObj.getString("issuedate")));
                                        travellerBean.setPassportNumber(Utils.checkStringVal(passportDetailsObj.getString("passportNo")));
                                        travellerBean.setPassportExpiry(Utils.checkStringVal(passportDetailsObj.getString("expiryDate")));
                                        travellerBean.setDateOfBirth(Utils.checkStringVal(passportDetailsObj.getString("dateofBirth")));
                                        travellerBean.setNationality(Utils.checkStringVal(passportDetailsObj.getString("nationality")));
                                        travellerBean.setPassportIssuingCountry(Utils.checkStringVal(passportDetailsObj.getString("countryIssued")));
                                        travellerBean.setDocumentType(passportDetailsObj.getString("docType"));
                                    }
                                }
                            }
                        }
                        if (!travellerBean.getFirstName().equalsIgnoreCase("") && !travellerBean.getLastName().equalsIgnoreCase("")) {
                            travellerArrayList.add(travellerBean);
                        }
                    }
                    listPersons = new UserAdapter(this, travellerArrayList, UserProfileActivity.this);
                    listView.setAdapter(listPersons);
                    listPersons.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (serviceType == GET_DELETE_TRAVELER_DETAILS) {
            Utils.printMessage(TAG, "Deleted Traveller DATA:: " + data);
            closeLoading();
            try {
                obj = new JSONObject(data);
                if (obj.has("status")) {
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        travellerArrayList.clear();
                        getTravelerDetailsRequestFromServer(requestUserDetailsJSON);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (serviceType == GET_EDIT_PROFILE) {
            Utils.printMessage(TAG, "Edit Profile DATA:: " + data);
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(titleFace);
        descriptionText.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(UserProfileActivity.this)) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_TRAVELER_DETAILS) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    boolean isTravelerDataUpdated;
                    isTravelerDataUpdated = data.getBooleanExtra("", Boolean.parseBoolean(Constants.IS_EDIT_PROFILE_UPDATED));
                    if (!isTravelerDataUpdated) {
                        travellerArrayList.clear();
                        getTravelerDetailsRequestFromServer(requestUserDetailsJSON);
                    }
                }
            }
        }
        if (requestCode == GET_EDIT_PROFILE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    boolean isTravelerProfileUpdated;
                    isTravelerProfileUpdated = data.getBooleanExtra("", Boolean.parseBoolean(Constants.IS_USER_PROFILE_UPDATED));
                    if (!isTravelerProfileUpdated) {
                        travellerProfileList.clear();
                        getTravelerProfileRequestFromServer(requestUserDetailsJSON);
                    }
                }
            }
        }
    }

    private String travelerDetailsTask() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(UserProfileActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(UserProfileActivity.this));
            mainJSON.put("source", sourceJSON);
            //mainJSON.put("customerId", pref.getString(Constants.MEMBER_EMAIL, ""));
            mainJSON.put("userUniqueId", pref.getString(Constants.USER_UNIQUE_ID, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void loadErrorType(int type) {

        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }
}
