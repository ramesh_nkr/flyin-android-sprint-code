package com.flyin.bookings;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.flyin.bookings.util.Utils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.webengage.sdk.android.WebEngage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Utils.printMessage(TAG, "Data Payload: " + remoteMessage.getData().toString());
        if (remoteMessage.getData().size() > 0) {
            try {
                Map<String, String> data = remoteMessage.getData();
                if (data != null) {
                    sendPushNotification(data.get("title"), data.get("message"));
                    if (data.containsKey("source") && "webengage".equals(data.get("source"))) {
                     //   WebEngage.get().receive(data);
                        // sendPushNotification(data.get("title"), data.get("message"));
                    }
                }
                //   JSONObject json = new JSONObject(remoteMessage.getData().toString());
            } catch (Exception e) {
                Utils.printMessage(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendPushNotification(String messageBody, String title) {
        Intent intent = new Intent(this, MainSearchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /*/ Request code /*/, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher).setContentTitle(title)
                .setContentText(messageBody).setAutoCancel(true)
                .setSound(defaultSoundUri).setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /*/ ID of notification /*/, notificationBuilder.build());
    }
}
