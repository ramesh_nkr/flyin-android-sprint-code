package com.flyin.bookings;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.flyin.bookings.adapter.TermsAdapter;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.TermsDetailsBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class TermsConditionsActivity extends AppCompatActivity implements OnCustomItemSelectListener {
    private ListView listView;
    private TermsAdapter termsAdapter;
    private ArrayList<TermsDetailsBean> termsArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termsconditions);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        boolean isArabicLang = false;
        if (Utils.isArabicLangSelected(TermsConditionsActivity.this)) {
            isArabicLang = true;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        TextView termsHeader = (TextView) findViewById(R.id.terms_header);
        TextView termsText = (TextView) findViewById(R.id.terms_text);
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontTitle);
        termsHeader.setTypeface(tf);
        termsText.setTypeface(textFace);
        listView = (ListView) findViewById(R.id.terms_list_view);
        if (isArabicLang) {
            termsHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            termsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        TermsDetailsBean termsDetailsBean = new TermsDetailsBean();
        termsDetailsBean.setTermsHeaderName(getString(R.string.label_terms_making_our_booking_title));
        termsDetailsBean.setIsExpanded(false);
        termsDetailsBean.setText(getString(R.string.label_terms_booking_text));
        termsArrayList.add(termsDetailsBean);
        termsDetailsBean = new TermsDetailsBean();
        termsDetailsBean.setTermsHeaderName(getString(R.string.label_terms_payment_terms));
        termsDetailsBean.setIsExpanded(false);
        termsDetailsBean.setText(getString(R.string.label_terms_payment_text));
        termsArrayList.add(termsDetailsBean);
        termsDetailsBean = new TermsDetailsBean();
        termsDetailsBean.setTermsHeaderName(getString(R.string.label_terms_price));
        termsDetailsBean.setIsExpanded(false);
        termsDetailsBean.setText(getString(R.string.label_terms_price_text));
        termsArrayList.add(termsDetailsBean);
        termsDetailsBean = new TermsDetailsBean();
        termsDetailsBean.setTermsHeaderName(getString(R.string.label_terms_transfer_booking));
        termsDetailsBean.setIsExpanded(false);
        termsDetailsBean.setText(getString(R.string.label_terms_transfer_text));
        termsArrayList.add(termsDetailsBean);
        termsDetailsBean = new TermsDetailsBean();
        termsDetailsBean.setTermsHeaderName(getString(R.string.label_terms_information_accuracy));
        termsDetailsBean.setIsExpanded(false);
        termsDetailsBean.setText(getString(R.string.label_terms_information_text));
        termsArrayList.add(termsDetailsBean);
        termsDetailsBean = new TermsDetailsBean();
        termsDetailsBean.setTermsHeaderName(getString(R.string.label_terms_liability));
        termsDetailsBean.setIsExpanded(false);
        termsDetailsBean.setText(getString(R.string.label_terms_liability_text));
        termsArrayList.add(termsDetailsBean);
        termsDetailsBean = new TermsDetailsBean();
        termsDetailsBean.setTermsHeaderName(getString(R.string.label_terms_suppliers));
        termsDetailsBean.setIsExpanded(false);
        termsDetailsBean.setText(getString(R.string.label_terms_suppliers_text));
        termsArrayList.add(termsDetailsBean);
        termsDetailsBean = new TermsDetailsBean();
        termsDetailsBean.setTermsHeaderName(getString(R.string.label_terms_complaints));
        termsDetailsBean.setIsExpanded(false);
        termsDetailsBean.setText(getString(R.string.label_terms_complaints_text));
        termsArrayList.add(termsDetailsBean);
        termsDetailsBean = new TermsDetailsBean();
        termsDetailsBean.setTermsHeaderName(getString(R.string.label_terms_personal_info));
        termsDetailsBean.setIsExpanded(false);
        termsDetailsBean.setText(getString(R.string.label_terms_personal_info_text));
        termsArrayList.add(termsDetailsBean);
        termsDetailsBean = new TermsDetailsBean();
        termsDetailsBean.setTermsHeaderName(getString(R.string.label_terms_privacy));
        termsDetailsBean.setIsExpanded(false);
        termsDetailsBean.setText(getString(R.string.label_terms_privacy_text));
        termsArrayList.add(termsDetailsBean);
        termsDetailsBean = new TermsDetailsBean();
        termsDetailsBean.setTermsHeaderName(getString(R.string.label_terms_responsibility));
        termsDetailsBean.setIsExpanded(false);
        termsDetailsBean.setText(getString(R.string.label_terms_responsibility_text));
        termsArrayList.add(termsDetailsBean);
        termsAdapter = new TermsAdapter(this, termsArrayList, TermsConditionsActivity.this);
        listView.setAdapter(termsAdapter);
        termsAdapter.notifyDataSetChanged();
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_terms_titlebar);
        mTitleText.setTypeface(tf);
        backText.setTypeface(textFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(TermsConditionsActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
            mActionBar.setDisplayShowCustomEnabled(true);
        }

        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    @Override
    public void onItemClick(int position) {
        boolean expandStatus = termsArrayList.get(position).isExpanded();
        for (TermsDetailsBean termsDetailsBean : termsArrayList) {
            termsDetailsBean.setIsExpanded(false);
        }
        int duration = 500;
        int offset = 0;
        termsArrayList.get(position).setIsExpanded(!expandStatus);
        termsAdapter.notifyDataSetChanged();
        listView.smoothScrollToPositionFromTop(position, offset, duration);
    }

    @Override
    public void onSelectedItemClick(int position, int type) {
    }
}