package com.flyin.bookings.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

public class HotelListLoadingDialog extends Dialog {
    private Context context;

    public HotelListLoadingDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_hotel_list);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.loading_bar);
        TextView loadingText = (TextView) findViewById(R.id.loading_text_view);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        if (Utils.isArabicLangSelected(context)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        Typeface regularFace = Typeface.createFromAsset(context.getAssets(), fontTitle);
        loadingText.setText(context.getString(R.string.label_hold_message) + " " + context.getString(R.string.searching_deals_message));
        loadingText.setTypeface(regularFace);
        if (Utils.isArabicLangSelected(context)) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimension(R.dimen.hotel_city_text_size));
        }
    }
}
