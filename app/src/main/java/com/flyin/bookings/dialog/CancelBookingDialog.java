package com.flyin.bookings.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.model.BookingsObject;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

public class CancelBookingDialog extends Dialog {
    private Context context;
    private BookingsObject bookingsObject;
    public boolean isCanceled = false;
    public String selectedCancelRemark = "";

    public CancelBookingDialog(Context context, BookingsObject bookingsObject) {
        super(context);
        this.context = context;
        this.bookingsObject = bookingsObject;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_cancel_booking);
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_MEDIUM;
        if (Utils.isArabicLangSelected(context)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        TextView cancelMyBookingHeader = (TextView) findViewById(R.id.cancel_my_booking_text);
        TextView closeDialogText = (TextView) findViewById(R.id.close_text);
        ImageView closeDialogImage = (ImageView) findViewById(R.id.close_image);
        TextView hotelNameText = (TextView) findViewById(R.id.hotel_name_text);
        TextView hotelAddressText = (TextView) findViewById(R.id.hotel_address_text);
        RatingBar hotelRating = (RatingBar) findViewById(R.id.ratingBar);
        TextView roomNameText = (TextView) findViewById(R.id.room_name_text);
        TextView hotelCurrencyText = (TextView) findViewById(R.id.hotel_currency_text);
        TextView hotelPriceText = (TextView) findViewById(R.id.hotel_price_text);
        TextView cancelReasonHeader = (TextView) findViewById(R.id.cancel_reason_header);
        RadioButton firstCancelReason = (RadioButton) findViewById(R.id.first_reason);
        RadioButton secondCancelReason = (RadioButton) findViewById(R.id.second_reason);
        RadioButton thirdCancelReason = (RadioButton) findViewById(R.id.third_reason);
        RadioButton fourthCancelReason = (RadioButton) findViewById(R.id.fourth_reason);
        RadioButton fifthCancelReason = (RadioButton) findViewById(R.id.fifth_reason);
        TextView feedBackReasonText = (TextView) findViewById(R.id.reason_feed_back_text);
        TextView cancelBookingContactText = (TextView) findViewById(R.id.cancel_booking_contact_text);
        TextView doNotCancelText = (TextView) findViewById(R.id.do_not_cancel_text);
        final RadioGroup remarkRadioGroup = (RadioGroup) findViewById(R.id.remark_radio_group);
        Typeface titleFace = Typeface.createFromAsset(context.getAssets(), fontTitle);
        Typeface textFace = Typeface.createFromAsset(context.getAssets(), fontText);
        cancelMyBookingHeader.setTypeface(textFace);
        closeDialogText.setTypeface(titleFace);
        hotelNameText.setTypeface(textFace);
        hotelAddressText.setTypeface(titleFace);
        roomNameText.setTypeface(titleFace);
        hotelCurrencyText.setTypeface(textFace);
        hotelPriceText.setTypeface(textFace);
        cancelReasonHeader.setTypeface(titleFace);
        firstCancelReason.setTypeface(titleFace);
        secondCancelReason.setTypeface(titleFace);
        thirdCancelReason.setTypeface(titleFace);
        fourthCancelReason.setTypeface(titleFace);
        fifthCancelReason.setTypeface(titleFace);
        feedBackReasonText.setTypeface(titleFace);
        cancelBookingContactText.setTypeface(titleFace);
        doNotCancelText.setTypeface(titleFace);
        if (Utils.isArabicLangSelected(context)) {
            cancelMyBookingHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            closeDialogText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_text_size));
            hotelNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            hotelAddressText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            roomNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            hotelPriceText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.hotel_city_text_size));
            hotelCurrencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.hotel_city_text_size));
            cancelReasonHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_text_size));
            firstCancelReason.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            secondCancelReason.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            thirdCancelReason.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            fourthCancelReason.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            fifthCancelReason.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            feedBackReasonText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            cancelBookingContactText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            doNotCancelText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            roomNameText.setGravity(Gravity.START);
            hotelAddressText.setGravity(Gravity.START);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                hotelAddressText.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                roomNameText.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
        }
        hotelNameText.setText(bookingsObject.getEntityDetailsObjectBean().getHotelMoreInfo().getName());
        String hotelAddress = (bookingsObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getAddress()).replace("\n", " ");
        hotelAddressText.setText(hotelAddress);
        hotelRating.setRating(Float.parseFloat(bookingsObject.getEntityDetailsObjectBean().getHotelMoreInfo().getRating()));
        roomNameText.setText(bookingsObject.getEntityDetailsObjectBean().getHotelDetailsBean().getHotelDataBean().getRoomType());
        String currencyName = "";
        if (bookingsObject.getPriceBean().getUserCurrency().equalsIgnoreCase("SAR")) {
            currencyName = context.getString(R.string.label_SAR_currency_name);
        } else {
            currencyName = bookingsObject.getPriceBean().getUserCurrency();
        }
        if (Utils.isArabicLangSelected(context)) {
            hotelCurrencyText.setText(bookingsObject.getPriceBean().getTotal() + " ");
            hotelPriceText.setText(currencyName);
        } else {
            hotelCurrencyText.setText(currencyName);
            hotelPriceText.setText(" " + bookingsObject.getPriceBean().getTotal());
        }
        closeDialogText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                isCanceled = false;
            }
        });
        closeDialogImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                isCanceled = false;
            }
        });
        doNotCancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (remarkRadioGroup.getCheckedRadioButtonId() != -1) {
                    int id = remarkRadioGroup.getCheckedRadioButtonId();
                    View radioButton = remarkRadioGroup.findViewById(id);
                    int radioId = remarkRadioGroup.indexOfChild(radioButton);
                    RadioButton btn = (RadioButton) remarkRadioGroup.getChildAt(radioId);
                    String selectedRemark = (String) btn.getText();
                    if (selectedRemark.equalsIgnoreCase("I found a better place to stay on flyin.com") || selectedRemark.equalsIgnoreCase("وجدت خيار أفضل لفندق آخر في فلاي إن")) {
                        selectedCancelRemark = "I found a better place to stay on flyin.com";
                    } else if (selectedRemark.equalsIgnoreCase("I found a better place to stay on different website") || selectedRemark.equalsIgnoreCase("وجدت خيار أفضل للفندق في موقع آخر")) {
                        selectedCancelRemark = "I found a better place to stay on different website";
                    } else if (selectedRemark.equalsIgnoreCase("I need to change the details of my reservation") || selectedRemark.equalsIgnoreCase("أرغب في تغيير تفاصيل الحجز")) {
                        selectedCancelRemark = "I need to change the details of my reservation";
                    } else if (selectedRemark.equalsIgnoreCase("I am no longer visiting the destination") || selectedRemark.equalsIgnoreCase("غيرت رأي في السفر إلى هذه الوجهة")) {
                        selectedCancelRemark = "I am no longer visiting the destination";
                    } else if (selectedRemark.equalsIgnoreCase("For personal reasons") || selectedRemark.equalsIgnoreCase("للأسباب شخصية")) {
                        selectedCancelRemark = "For personal reasons";
                    }
                } else {
                    selectedCancelRemark = "";
                }
                isCanceled = true;
                dismiss();
            }
        });
    }
}
