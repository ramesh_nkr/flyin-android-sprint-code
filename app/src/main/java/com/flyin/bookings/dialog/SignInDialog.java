package com.flyin.bookings.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.flyin.bookings.AboutusActivity;
import com.flyin.bookings.R;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;
import com.webengage.sdk.android.User;
import com.webengage.sdk.android.WebEngage;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressWarnings("ConstantConditions")
public class SignInDialog extends Dialog implements com.flyin.bookings.services.AsyncTaskListener {
    private Context context;
    private ProgressDialog barProgressDialog = null;
    private EditText userEmail, userPassword, forgotUserEmail;
    private TextView pleaseSingInHeader = null;
    private TextView signUpText = null;
    private TextView signInText = null;
    private Button signInButton = null;
    private RelativeLayout signInLayout = null;
    private RelativeLayout forgotDetailsLayout = null;
    private LinearLayout forgotDetailsLoginLayout = null;
    private LinearLayout forgotPasswordTextLayout = null;
    private boolean isSingUpClicked = true;
    private static final int GET_SIGNIN_RESPONSE = 1;
    private static final int GET_SIGNUP_RESPONSE = 2;
    private static final int GET_FORGOT_RESPONSE = 3;
    private static final int GET_FACEBOOK_RESPONSE = 4;
    private SharedPreferences preferences = null;
    private static final String TAG = "SignInDialog";
    private String email, forgotEmail, password;
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private MyReceiver myReceiver;
    private IntentFilter intentFilter;
    private CallbackManager callbackManager;
    private LoginButton fbLoginButton;
    private String fbId = "";
    private String fbFirstName = "";
    private String fbLastName = "";
    private String fbLink = "";
    private String fbEmail = "";
    private boolean isRegistrationCall = false;
    private CheckBox checkBox;
    private CheckBox rewardsCheckBox;
    private LinearLayout rewardsViewLayout = null;
    private ArrayList<String> userArralits = new ArrayList();
    private String requestTermsUrl = "";
    private TextView rewardTermsText = null;

    public SignInDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        FacebookSdk.sdkInitialize(context);
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.dialog_signin);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        requestTermsUrl = Constants.EN_ENROLL_TERMS_URL;
        ImageView closeImage = (ImageView) findViewById(R.id.close_btn);
        ImageView closeMark = (ImageView) findViewById(R.id.close_button);
        ImageView hideImage = (ImageView) findViewById(R.id.hide_button);
        ImageView facebookLinkImage = (ImageView) findViewById(R.id.facebook_image);
        userEmail = (EditText) findViewById(R.id.user_email);
        userPassword = (EditText) findViewById(R.id.user_pwd);
        forgotUserEmail = (EditText) findViewById(R.id.forgot_details_email);
        pleaseSingInHeader = (TextView) findViewById(R.id.please_signIn_header);
        TextView forgotPasswordHeader = (TextView) findViewById(R.id.forgot_password_header);
        TextView passwordDescription = (TextView) findViewById(R.id.password_description);
        TextView emailSentHeader = (TextView) findViewById(R.id.email_sent_header);
        TextView emailSentDescription = (TextView) findViewById(R.id.email_sent_description);
        signUpText = (TextView) findViewById(R.id.sign_up_text);
        signInText = (TextView) findViewById(R.id.sign_in_text);
        TextView forgotPasswordText = (TextView) findViewById(R.id.forgot_password);
        signInButton = (Button) findViewById(R.id.user_submit);
        Button resetButton = (Button) findViewById(R.id.forgot_details_reset_button);
        Button forgotSignInButton = (Button) findViewById(R.id.forgot_details_singIn_button);
        LinearLayout signUpLayout = (LinearLayout) findViewById(R.id.singUp_layout);
        RelativeLayout singUpTextLayout = (RelativeLayout) findViewById(R.id.sign_up_text_layout);
        forgotPasswordTextLayout = (LinearLayout) findViewById(R.id.forgot_password_layout);
        signInLayout = (RelativeLayout) findViewById(R.id.sign_in_layout);
        forgotDetailsLayout = (RelativeLayout) findViewById(R.id.forgot_details_layout);
        forgotDetailsLoginLayout = (LinearLayout) findViewById(R.id.forgot_details_singIn_layout);
        fbLoginButton = (LoginButton) findViewById(R.id.fb_login_button);
        LinearLayout facebookLinkLayout = (LinearLayout) findViewById(R.id.facebook_layout);
        checkBox = (CheckBox) findViewById(R.id.fan_club_checkbox);
        rewardsCheckBox = (CheckBox) findViewById(R.id.rewards_checkbox);
        rewardTermsText = (TextView) findViewById(R.id.rewards_conditions_text);
        rewardsViewLayout = (LinearLayout) findViewById(R.id.rewards_view_layout);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        if (Utils.isArabicLangSelected(context)) {
            requestTermsUrl = Constants.AR_ENROLL_TERMS_URL;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                userEmail.setTextDirection(View.TEXT_DIRECTION_RTL);
                userPassword.setTextDirection(View.TEXT_DIRECTION_RTL);
                forgotUserEmail.setTextDirection(View.TEXT_DIRECTION_RTL);
                userPassword.setGravity(Gravity.CENTER);
                closeImage.setScaleType(ImageView.ScaleType.FIT_START);
                closeMark.setScaleType(ImageView.ScaleType.FIT_START);
                hideImage.setScaleType(ImageView.ScaleType.FIT_START);
            }
            pleaseSingInHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            userEmail.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.hotel_city_text_size));
            userPassword.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.hotel_city_text_size));
            signInButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            signUpText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_text_size));
            signInText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_text_size));
            forgotPasswordText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_text_size));
            forgotPasswordHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_text_size));
            passwordDescription.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.hotel_city_text_size));
            forgotUserEmail.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.hotel_city_text_size));
            resetButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            emailSentHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_text_size));
            emailSentDescription.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.hotel_city_text_size));
            forgotSignInButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            checkBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            rewardsCheckBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            rewardTermsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_city_text));
            errorMessageText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.arabic_text_size));
        }
        Typeface tf = Typeface.createFromAsset(context.getAssets(), fontPath);
        Typeface titleFace = Typeface.createFromAsset(context.getAssets(), fontTitle);
        pleaseSingInHeader.setTypeface(tf);
        userEmail.setTypeface(titleFace);
        userPassword.setTypeface(titleFace);
        signInButton.setTypeface(tf);
        resetButton.setTypeface(tf);
        forgotSignInButton.setTypeface(tf);
        signUpText.setTypeface(titleFace);
        signInText.setTypeface(titleFace);
        forgotPasswordText.setTypeface(titleFace);
        fbLoginButton.setTypeface(titleFace);
        forgotPasswordHeader.setTypeface(tf);
        passwordDescription.setTypeface(titleFace);
        forgotUserEmail.setTypeface(titleFace);
        emailSentHeader.setTypeface(titleFace);
        emailSentDescription.setTypeface(titleFace);
        checkBox.setTypeface(titleFace);
        rewardsCheckBox.setTypeface(titleFace);
        errorMessageText.setTypeface(titleFace);
        rewardTermsText.setTypeface(titleFace);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        closeMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        hideImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        signUpText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pleaseSingInHeader.setText(context.getResources().getString(R.string.label_signUp_text));
                signInButton.setText(context.getResources().getString(R.string.label_signUp));
                forgotPasswordTextLayout.setVisibility(View.GONE);
                signUpText.setVisibility(View.GONE);
                signInText.setVisibility(View.VISIBLE);
                isSingUpClicked = false;
                checkBox.setVisibility(View.VISIBLE);
                rewardsViewLayout.setVisibility(View.VISIBLE);
            }
        });
        signInText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pleaseSingInHeader.setText(context.getResources().getString(R.string.label_singIn_text));
                signInButton.setText(context.getResources().getString(R.string.label_signIn));
                forgotPasswordTextLayout.setVisibility(View.VISIBLE);
                signUpText.setVisibility(View.VISIBLE);
                signInText.setVisibility(View.GONE);
                isSingUpClicked = true;
                checkBox.setChecked(false);
                checkBox.setVisibility(View.GONE);
                rewardsViewLayout.setVisibility(View.GONE);
            }
        });
        rewardTermsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, AboutusActivity.class);
                in.putExtra(Constants.HTML_WEB_VIEW, requestTermsUrl);
                in.putExtra(Constants.ACTIONBAR_HEADER, context.getString(R.string.label_reward_points));
                in.putExtra("Calling_From", "Dialog");
                context.startActivity(in);
            }
        });
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = userEmail.getText().toString().toLowerCase();
                password = userPassword.getText().toString();
                if (userEmail.getText().toString().isEmpty()) {
                    displayErrorMessage(context.getResources().getString(R.string.label_error_email_message));
                    return;
                }
                if (!Utils.isValidEmail(userEmail.getText().toString())) {
                    displayErrorMessage(context.getResources().getString(R.string.label_error_email_valid_message));
                    return;
                }
                if (userPassword.getText().toString().isEmpty()) {
                    displayErrorMessage(context.getResources().getString(R.string.label_error_password_message));
                    return;
                }
                if (!isSingUpClicked) {
                    String requestSignUpJSON = userSingUpTask();
                    getSignUpRequestFromServer(requestSignUpJSON);
                } else {
                    String requestJSON = userSingInTask();
                    getSignInRequestFromServer(requestJSON);
                }
            }
        });
        forgotPasswordTextLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInLayout.setVisibility(View.GONE);
                forgotDetailsLayout.setVisibility(View.VISIBLE);
                forgotDetailsLoginLayout.setVisibility(View.GONE);
            }
        });
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotEmail = forgotUserEmail.getText().toString().toLowerCase();
                if (forgotEmail.isEmpty()) {
                    displayErrorMessage(context.getResources().getString(R.string.label_error_email_message));
                    return;
                }
                if (!Utils.isValidEmail(forgotEmail)) {
                    displayErrorMessage(context.getResources().getString(R.string.label_error_email_valid_message));
                    return;
                }
                if (!forgotEmail.isEmpty() && Utils.isValidEmail(forgotEmail)) {
                    String forgotPasswordRequestJSON = userForgotDetailsTask();
                    getForgotPasswordRequestFromServer(forgotPasswordRequestJSON);
                }
            }
        });
        forgotSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotDetailsLoginLayout.setVisibility(View.GONE);
                signInLayout.setVisibility(View.VISIBLE);
            }
        });
        facebookLinkImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fbLoginButton.performClick();
            }
        });
        fbLoginButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        fbLoginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Utils.printMessage(TAG, "Facebook Login Successful!");
                GraphRequestAsyncTask request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if (response.getError() != null) {
                            // handle error
                        } else {
                            fbId = object.optString("id");
                            fbFirstName = object.optString("first_name");
                            fbLastName = object.optString("last_name");
                            fbLink = object.optString("link");
                            fbEmail = object.optString("email");
                            if (fbEmail.isEmpty()) {
                                displayErrorMessage(context.getString(R.string.facebook_no_email_msg));
                                return;
                            }
                            String fbLoginJSON = handleFacebookRequest();
                            getFbRequestFromServer(fbLoginJSON);
                        }
                    }
                }).executeAsync();
            }

            @Override
            public void onCancel() {
                Utils.printMessage(TAG, "Facebook Login cancelled!");
            }

            @Override
            public void onError(FacebookException e) {
                Utils.printMessage(TAG, "Facebook Login failed!!" + e.getMessage());
            }
        });
    }

    private String userSingInTask() {
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("userName", userEmail.getText().toString().toLowerCase());
            mainJSON.put("password", userPassword.getText().toString());
            mainJSON.put("grantType", "password");
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(context));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(context));
            mainJSON.put("source", sourceJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void getSignInRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(context, SignInDialog.this, Constants.USER_LOGIN, "", requestJSON,
                GET_SIGNIN_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String userSingUpTask() {
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("userName", userEmail.getText().toString().toLowerCase());
            mainJSON.put("password", userPassword.getText().toString());
            if (checkBox.isChecked()) {
                mainJSON.put("groupType", "ALHILAL");
            } else {
                mainJSON.put("groupType", Utils.checkStringValue("null"));
            }
            String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
            String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
            String timeStamp = currentDateFormat + "T" + currentTimeFormat;
            if (rewardsCheckBox.isChecked()) {
                mainJSON.put("rewardJoinDate", timeStamp);
            } else {
                mainJSON.put("rewardJoinDate", Utils.checkStringValue("null"));
            }
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(context));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(context));
            mainJSON.put("source", sourceJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void getSignUpRequestFromServer(String requestSignUpJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(context, SignInDialog.this, Constants.USER_SIGNUP, "", requestSignUpJSON,
                GET_SIGNUP_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String userForgotDetailsTask() {
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("userName", forgotUserEmail.getText().toString().toLowerCase());
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(context));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(context));
            mainJSON.put("source", sourceJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void getForgotPasswordRequestFromServer(String forgotPasswordRequestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(context, SignInDialog.this, Constants.USER_FORGOT_PASSWORD, "", forgotPasswordRequestJSON,
                GET_FORGOT_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String handleFacebookRequest() {
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(context));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(context));
            String facebookLink = URLEncoder.encode(fbLink, "utf-8");
            mainJSON.put("source", sourceJSON);
            mainJSON.put("id", fbId);
            mainJSON.put("firstName", fbFirstName);
            mainJSON.put("middleName", "");
            mainJSON.put("lastName", fbLastName);
            mainJSON.put("socialLink", facebookLink);
            mainJSON.put("email", fbEmail.toLowerCase());
            mainJSON.put("mediaType", "Facebook");
            mainJSON.put("dateOfBirth", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void getFbRequestFromServer(String requestJSON) {
        showLoading();
        HTTPAsync async = new HTTPAsync(context, SignInDialog.this, Constants.SOCIAL_MEDIA_FACEBOOK_URL, "", requestJSON,
                GET_FACEBOOK_RESPONSE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "DATA:: " + data);
        closeLoading();
        JSONObject obj = null;
        try {
            obj = new JSONObject(data);
            if (obj.has("accessToken")) {
                String accessToken = obj.getString("accessToken");
                if (accessToken.equalsIgnoreCase("null") && serviceType == GET_SIGNIN_RESPONSE) {
                    displayErrorMessage(context.getResources().getString(R.string.label_login_fail_message));
                } else if (!accessToken.equalsIgnoreCase("null") && serviceType == GET_SIGNIN_RESPONSE) {
                    if (obj.has("status")) {
                        String status = obj.getString("status");
                        if (status.equalsIgnoreCase("FAILURE")) {
                            displayErrorMessage(context.getResources().getString(R.string.label_login_fail_message));
                        }
                    } else {
                        String memberTokenType = obj.getString("tokenType");
                        String userUniqueId = obj.getString("userUniqueId");
                        boolean isAlHilalUser = false;
                        boolean rewardEnabled = false;
                        if (obj.has("groupType")) {
                            if (!obj.getString("groupType").equalsIgnoreCase("null") || obj.getString("groupType") != null) {
                                String groupType = obj.getString("groupType");
                                if (groupType.equalsIgnoreCase("ALHILAL")) {
                                    isAlHilalUser = true;
                                }
                            }
                        }
                        if (obj.has("flyinRewardEnable")) {
                            rewardEnabled = obj.getBoolean("flyinRewardEnable");
                        }
                        if (isRegistrationCall) {
                            Intent intent = new Intent();
                            intent.setAction("com.flyin.bookings.ERROR_MESSAGE");
                            intent.putExtra("message", context.getString(R.string.registration_success_msg));
                            intent.putExtra("type", "success");
                            context.sendBroadcast(intent);
                        }
                        preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                        userArralits.add(userUniqueId);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(Constants.MEMBER_EMAIL, userEmail.getText().toString().toLowerCase());
                        editor.putString(Constants.MEMBER_ACCESS_TOKEN, accessToken);
                        editor.putString(Constants.MEMBER_TOKEN_TYPE, memberTokenType);
                        editor.putString(Constants.USER_UNIQUE_ID, userUniqueId);
                        editor.putBoolean(Constants.isFanClub, isAlHilalUser);
                        editor.putBoolean(Constants.IS_REWARDS_ENABLED, rewardEnabled);
                        editor.apply();
                        User weUser = WebEngage.get().user();
                        weUser.login(userUniqueId);
                        dismiss();
                    }
                } else if (accessToken.equalsIgnoreCase("null") && serviceType == GET_SIGNUP_RESPONSE) {
                    displayErrorMessage(context.getResources().getString(R.string.label_signUp_fail_message));
                } else if (!accessToken.equalsIgnoreCase("null") && serviceType == GET_SIGNUP_RESPONSE) {
                    String status = obj.getString("status");
                    //Utils.printMessage(TAG, "userUniqueId Reg :: " + obj.getString("userUniqueId"));
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        String requestJSON = userSingInTask();
                        HTTPAsync async = new HTTPAsync(context, SignInDialog.this, Constants.USER_LOGIN, "", requestJSON,
                                GET_SIGNIN_RESPONSE, HTTPAsync.METHOD_POST);
                        async.execute();
                        isRegistrationCall = true;
                    }
                } else if (serviceType == GET_FORGOT_RESPONSE) {
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("FAILURE")) {
                        displayErrorMessage(forgotEmail + "" + context.getResources().getString(R.string.label_forgot_email_fail_message));
                    } else {
                        signInLayout.setVisibility(View.GONE);
                        forgotDetailsLayout.setVisibility(View.GONE);
                        forgotDetailsLoginLayout.setVisibility(View.VISIBLE);
                        final InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        keyboard.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                        forgotUserEmail.setText(null);
                    }
                } else if (!accessToken.equalsIgnoreCase("null") && serviceType == GET_FACEBOOK_RESPONSE) {
                    String userUniqueId = "";
                    if (obj.has("userUniqueId")) {
                        userUniqueId = obj.getString("userUniqueId");
                    }
                    preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Constants.MEMBER_EMAIL, fbEmail.toLowerCase());
                    editor.putString(Constants.MEMBER_ACCESS_TOKEN, accessToken);
                    editor.putString(Constants.USER_UNIQUE_ID, userUniqueId);
                    boolean isAlHilalUser = false;
                    boolean rewardEnabled = false;
                    if (obj.has("groupType")) {
                        if (!obj.getString("groupType").equalsIgnoreCase("null") || obj.getString("groupType") != null) {
                            String groupType = obj.getString("groupType");
                            if (groupType.equalsIgnoreCase("ALHILAL")) {
                                isAlHilalUser = true;
                            }
                        }
                    }
                    if (obj.has("flyinRewardEnable")) {
                        rewardEnabled = obj.getBoolean("flyinRewardEnable");
                    }
                    editor.putBoolean(Constants.isFanClub, isAlHilalUser);
                    editor.putBoolean(Constants.IS_REWARDS_ENABLED, rewardEnabled);
                    editor.apply();
                    dismiss();
                }
            } else if (serviceType == GET_SIGNUP_RESPONSE) {
                String status = obj.getString("status");
                if (status.equalsIgnoreCase("FAILURE")) {
                    displayErrorMessage(context.getResources().getString(R.string.label_signUp_fail_message));
                }
            } else if (serviceType == GET_SIGNIN_RESPONSE) {
                String status = obj.getString("status");
                if (status.equalsIgnoreCase("FAILURE")) {
                    displayErrorMessage(context.getResources().getString(R.string.label_login_fail_message));
                }
            }
            if (serviceType == GET_FORGOT_RESPONSE) {
                String status = obj.getString("status");
                if (status.equalsIgnoreCase("FAILURE")) {
                    displayErrorMessage(forgotEmail + "" + context.getResources().getString(R.string.label_forgot_email_fail_message));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        if (barProgressDialog == null) {
            barProgressDialog = new ProgressDialog(context);
            barProgressDialog.setMessage(context.getResources().getString(R.string.label_wait_please_message));
            barProgressDialog.setProgressStyle(barProgressDialog.STYLE_SPINNER);
            barProgressDialog.setCancelable(false);
            barProgressDialog.show();
        }
    }

    private void closeLoading() {
        if (barProgressDialog != null) {
            barProgressDialog.dismiss();
            barProgressDialog = null;
        }
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 1) {
            params.height = context.getResources().getDimensionPixelSize(R.dimen.result_combo_flag);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = context.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(context.getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(context.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    public void passFacebookResult(int reqCode, int resCode, Intent data) {
        try {
            callbackManager.onActivityResult(reqCode, resCode, data);
        } catch (Exception e) {
        }
    }

    public void callFacebookLogout() {
        FacebookSdk.sdkInitialize(context);
        LoginManager.getInstance().logOut();
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.ERROR_MESSAGE")) {
                displaySuccessMessage(intent.getStringExtra("message"));
            }
        }
    }

    private void displaySuccessMessage(String message) {
        Intent intent = new Intent();
        intent.setAction("com.thresholdsoft.myapplication.ERROR_MESSAGE");
        intent.putExtra("message", message);
        context.sendBroadcast(intent);
    }
}
