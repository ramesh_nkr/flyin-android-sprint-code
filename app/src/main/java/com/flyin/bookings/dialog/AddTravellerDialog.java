package com.flyin.bookings.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.adapter.RoomInformationAdapter;
import com.flyin.bookings.listeners.RoomInformationListener;
import com.flyin.bookings.model.FhRoomPassengerDetailsBean;
import com.flyin.bookings.model.RoomCombinationObjectBean;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class AddTravellerDialog extends Dialog implements RoomInformationListener {
    private Activity activity = null;
    private TextView classTypeText = null;
    private Button addRoomButton = null;
    private RoomInformationAdapter roomInformationAdapter = null;
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private int maxRoomCount = 6;
    public String classType = "";
    public String classTypeCode = "Economy";
    public ArrayList<FhRoomPassengerDetailsBean> roomPassengerArrayList = null;
    public ArrayList<FhRoomPassengerDetailsBean> tempRoomPassengerArrayList = null;
    private FhRoomPassengerDetailsBean fhRoomPassengerBean = null;
    private boolean isClassTypeNeed;

    public AddTravellerDialog(Activity activity, ArrayList<FhRoomPassengerDetailsBean> roomPassengerArrayList, String cabinType,
                              boolean isClassTypeNeed) {
        super(activity);
        this.activity = activity;
        this.roomPassengerArrayList = roomPassengerArrayList;
        this.classType = cabinType;
        this.isClassTypeNeed = isClassTypeNeed;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_traveller);
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_MEDIUM;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        ImageView closeIcon = (ImageView) findViewById(R.id.close_icon);
        TextView sortFilterHeader = (TextView) findViewById(R.id.sort_filter_header);
        Button applyButton = (Button) findViewById(R.id.apply_button);
        ListView roomInfoListView = (ListView) findViewById(R.id.room_list_view);
        tempRoomPassengerArrayList = new ArrayList<>();
        classType = activity.getResources().getString(R.string.label_economy_class);
        if (Utils.isArabicLangSelected(activity)) {
            sortFilterHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            applyButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        for (int i = 0; i < roomPassengerArrayList.size(); i++) {
            FhRoomPassengerDetailsBean bean1 = roomPassengerArrayList.get(i);
            FhRoomPassengerDetailsBean bean = new FhRoomPassengerDetailsBean();
            bean.setAdultCnt(bean1.getAdultCnt());
            bean.setChildAge(bean1.getChildAge());
            bean.setAge(bean1.getAge());
            bean.setChildAgeArray(new ArrayList<Integer>(bean1.getChildAgeArray()));
            bean.setChildCnt(bean1.getChildCnt());
            bean.setClassType(bean1.getClassType());
            bean.setInfantCnt(bean1.getInfantCnt());
            tempRoomPassengerArrayList.add(bean);
        }
        Typeface titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        Typeface textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        sortFilterHeader.setTypeface(titleFace);
        applyButton.setTypeface(textFace);
        closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                roomPassengerArrayList.clear();
                roomPassengerArrayList.addAll(tempRoomPassengerArrayList);
                dismiss();
            }
        });
        View footerView = getLayoutInflater().inflate(R.layout.view_room_footer, null);
        addRoomButton = (Button) footerView.findViewById(R.id.add_room_button);
        LinearLayout classTypeLayout = (LinearLayout) footerView.findViewById(R.id.class_type_layout);
        classTypeText = (TextView) footerView.findViewById(R.id.class_type_text);
        if (!isClassTypeNeed) {
            classTypeLayout.setVisibility(View.GONE);
        }
        if (Utils.isArabicLangSelected(activity)) {
            addRoomButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            classTypeText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        addRoomButton.setTypeface(textFace);
        classTypeText.setTypeface(titleFace);
        addRoomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int totalAdultCnt = 0;
                int totalChidlCnt = 0;
                int totalInfantCnt = 0;
                for (int i = 0; i < tempRoomPassengerArrayList.size(); i++) {
                    totalAdultCnt = totalAdultCnt + tempRoomPassengerArrayList.get(i).getAdultCnt();
                    totalChidlCnt = totalChidlCnt + tempRoomPassengerArrayList.get(i).getChildCnt();
                    totalInfantCnt = totalInfantCnt + tempRoomPassengerArrayList.get(i).getInfantCnt();
                }
                if (!checkIsValidRecordsCount(totalAdultCnt, totalChidlCnt, totalInfantCnt, false, false)) {
                    return;
                }
                if (totalAdultCnt + totalChidlCnt + totalInfantCnt < 9) {
                    if (tempRoomPassengerArrayList.size() >= maxRoomCount) {
                        return;
                    }
                    if (tempRoomPassengerArrayList.size() < maxRoomCount) {
                        fhRoomPassengerBean = new FhRoomPassengerDetailsBean();
                        fhRoomPassengerBean.setAdultCnt(1);
                        fhRoomPassengerBean.setChildCnt(0);
                        fhRoomPassengerBean.setInfantCnt(0);
                        fhRoomPassengerBean.getChildAgeArray().add(2);
                        fhRoomPassengerBean.getChildAgeArray().add(2);
                        fhRoomPassengerBean.getChildAgeArray().add(2);
                        fhRoomPassengerBean.getChildAgeArray().add(2);
                        fhRoomPassengerBean.getChildAgeArray().add(2);
                        fhRoomPassengerBean.setExpanded(false);
                        tempRoomPassengerArrayList.add(fhRoomPassengerBean);
                        roomInformationAdapter.notifyDataSetChanged();
                    }
                    if (tempRoomPassengerArrayList.size() >= maxRoomCount) {
                        //displayErrorMessage(activity.getResources().getString(R.string.label_maximum_rooms));
                        addRoomButton.setVisibility(View.GONE);
                    }
                }
            }
        });
        classTypeText.setText(classType);
        classTypeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog classTypeDialog = new TravellerListDialog(activity, Constants.CLASS_TYPE_SELECTION, null, "", null, false);
                classTypeDialog.setCancelable(true);
                classTypeDialog.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (classTypeDialog != null) {
                            if (!classTypeDialog.selectedListItem.isEmpty()) {
                                classType = classTypeDialog.selectedListItem;
                                classTypeCode = classTypeDialog.selectedListCode;
                                classTypeText.setText(classType);
                            }
                        }
                    }
                });
                classTypeDialog.show();
            }
        });
        roomInfoListView.addFooterView(footerView);
        roomInformationAdapter = new RoomInformationAdapter(activity, tempRoomPassengerArrayList, AddTravellerDialog.this);
        roomInfoListView.setAdapter(roomInformationAdapter);
        roomInformationAdapter.notifyDataSetChanged();
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(titleFace);
        if (tempRoomPassengerArrayList.size() >= maxRoomCount) {
            addRoomButton.setVisibility(View.GONE);
        }
    }

    /**
     * Check validation for passenger total count
     **/
    private boolean checkIsValidRecordsCount(int adults, int children, int infant, boolean isInfantsClicked, boolean isAdultDecrement) {
        boolean isValid = true;
        if (adults == infant && isInfantsClicked) {
            displayErrorMessage(activity.getResources().getString(R.string.label_err_infants_msg));
            isValid = false;
        } else if (isAdultDecrement) {
            if (adults == infant) {
                displayErrorMessage(activity.getResources().getString(R.string.label_err_infants_msg));
                isValid = false;
            } else {
                isValid = true;
            }
        } else if (adults + children + infant >= 9 && !isAdultDecrement) {
            displayErrorMessage(activity.getResources().getString(R.string.label_err_tot_trav_msg));
            isValid = false;
        }
        return isValid;
    }

    /**
     * Displaying Error message
     **/
    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = activity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = activity.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-90);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(activity.getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(activity.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    @Override
    public void onItemClick(int position, int type, boolean isIncrement) {
        int totalAdultCount = 0;
        int totalChidlCount = 0;
        int totalInfantCount = 0;
        for (int i = 0; i < tempRoomPassengerArrayList.size(); i++) {
            totalAdultCount = totalAdultCount + tempRoomPassengerArrayList.get(i).getAdultCnt();
            totalChidlCount = totalChidlCount + tempRoomPassengerArrayList.get(i).getChildCnt();
            totalInfantCount = totalInfantCount + tempRoomPassengerArrayList.get(i).getInfantCnt();
        }
        int adultCount = tempRoomPassengerArrayList.get(position).getAdultCnt();
        int childCount = tempRoomPassengerArrayList.get(position).getChildCnt();
        int infantCount = tempRoomPassengerArrayList.get(position).getInfantCnt();
        if (type == 1) {
            if (isIncrement) {
                if (!checkIsValidRecordsCount(totalAdultCount, totalChidlCount, totalInfantCount, false, false)) {
                    return;
                }
                if (adultCount < 6) {
                    adultCount++;
                }
                if (adultCount == 6) {
                    displayErrorMessage(activity.getResources().getString(R.string.label_room_max_adults));
                }
                tempRoomPassengerArrayList.get(position).setAdultCnt(adultCount);
            } else {
                if (adultCount == 1) {
                    return;
                }
                if (!checkIsValidRecordsCount(totalAdultCount, totalChidlCount, totalInfantCount, true, true)) {
                    return;
                }
                if (!checkIsValidRecordsCount(adultCount, childCount, infantCount, true, true)) {
                    return;
                }
                adultCount--;
                tempRoomPassengerArrayList.get(position).setAdultCnt(adultCount);
            }
        } else if (type == 2) {
            if (isIncrement) {
                if (!checkIsValidRecordsCount(totalAdultCount, totalChidlCount, totalInfantCount, false, false)) {
                    return;
                }
                if (childCount < 5) {
                    childCount++;
                }
                if (childCount == 5) {
                    displayErrorMessage(activity.getResources().getString(R.string.label_room_max_child));
                }
                tempRoomPassengerArrayList.get(position).setChildCnt(childCount);
                tempRoomPassengerArrayList.get(position).setExpanded(true);
            } else {
                if (childCount == 0) {
                    return;
                }
                childCount--;
                tempRoomPassengerArrayList.get(position).setChildCnt(childCount);
                if (childCount > 0) {
                    switch (childCount) {
                        case 1:
                            tempRoomPassengerArrayList.get(position).getChildAgeArray().set(1, 0);
                            break;
                        case 2:
                            tempRoomPassengerArrayList.get(position).getChildAgeArray().set(2, 0);
                            break;
                        case 3:
                            tempRoomPassengerArrayList.get(position).getChildAgeArray().set(3, 0);
                            break;
                        case 4:
                            tempRoomPassengerArrayList.get(position).getChildAgeArray().set(4, 0);
                            break;
                    }
                    tempRoomPassengerArrayList.get(position).setExpanded(true);
                } else {
                    tempRoomPassengerArrayList.get(position).getChildAgeArray().set(0, 0);
                    tempRoomPassengerArrayList.get(position).setExpanded(false);
                }
            }
        } else if (type == 3) {
            if (isIncrement) {
                if (!checkIsValidRecordsCount(totalAdultCount, totalChidlCount, totalInfantCount, true, false)) {
                    return;
                }
                if (!checkIsValidRecordsCount(adultCount, childCount, infantCount, true, false)) {
                    return;
                }
                if (infantCount < 5) {
                    infantCount++;
                }
                tempRoomPassengerArrayList.get(position).setInfantCnt(infantCount);
            } else {
                if (infantCount == 0) {
                    return;
                }
                infantCount--;
                tempRoomPassengerArrayList.get(position).setInfantCnt(infantCount);
            }
        }
        roomInformationAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemCancelClick(int position) {
        tempRoomPassengerArrayList.remove(position);
        if (tempRoomPassengerArrayList.size() < maxRoomCount) {
            addRoomButton.setVisibility(View.VISIBLE);
        }
        roomInformationAdapter.notifyDataSetChanged();
    }

    @Override
    public void onChildItemClick(int position, int tag, int value) {
        tempRoomPassengerArrayList.get(position).getChildAgeArray().set(tag, value);
    }

    @Override
    public void onRoomItemClick(int position, int roomPosition) {
    }

    @Override
    public void onShowRoomDetailsClick(int position) {
    }

    @Override
    public void onShowMoreClick(int position, String hotelId, RoomCombinationObjectBean rcObj) {
    }
}
