package com.flyin.bookings.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.adapter.TravellerListAdapter;
import com.flyin.bookings.model.AirMealList;
import com.flyin.bookings.model.CountryListBean;
import com.flyin.bookings.model.CountryNamesList;
import com.flyin.bookings.model.TravellerDetailsBean;
import com.flyin.bookings.util.CSVFile;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.DbHandler;
import com.flyin.bookings.util.Utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TravellerListDialog extends Dialog {
    private Activity activity;
    private TextView headerText;
    private ListView travellerListView;
    public String selectedListItem = "";
    public String selectedListCode = "";
    private ArrayList<String> tempArrayList = null;
    private ArrayList<String> tempArrayListCode = null;
    private ArrayList<AirMealList> airMealList = null;
    private ArrayList<CountryNamesList> countryNameList = null;
    private ArrayList<TravellerDetailsBean> travellerList;
    private ArrayList<CountryListBean> countryNamesArray;
    private TravellerListAdapter travellerDropDownAdapter;
    private int selectionList = 0;
    public int selectedItemPosition = -1;
    public int selectedChildAge = 0;
    private String passengerType;
    private Typeface titleFace;
    private boolean isAirportAvailable;
    private boolean isArabicLang;

    public TravellerListDialog(Activity activity, int selectionList, ArrayList<TravellerDetailsBean> travellerList, String passengerType, ArrayList<CountryListBean> countryNamesArray, boolean isAirportAvailable) {
        super(activity);
        this.activity = activity;
        this.selectionList = selectionList;
        this.travellerList = travellerList;
        this.passengerType = passengerType;
        this.countryNamesArray = countryNamesArray;
        this.isAirportAvailable = isAirportAvailable;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_traveller_list);
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        isArabicLang = false;
        if (Utils.isArabicLangSelected(activity)) {
            isArabicLang = true;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
        }
        titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        headerText = (TextView) findViewById(R.id.list_header_text);
        headerText.setTypeface(titleFace);
        tempArrayList = new ArrayList<>();
        tempArrayListCode = new ArrayList<>();
        airMealList = new ArrayList<>();
        countryNameList = new ArrayList<>();
        travellerListView = (ListView) findViewById(R.id.traveller_dialog_list);
        switch (selectionList) {
            case Constants.PASSENGER_LIST:
                headerText.setText(R.string.label_traveller_title_traveller_list);
                for (int i = 0; i < travellerList.size(); i++) {
                    String travellerName = Utils.checkStringVal(travellerList.get(i).getTitle()) + " " + Utils.checkStringVal(travellerList.get(i).getFirstName()) + " " + Utils.checkStringVal(travellerList.get(i).getLastName());
                    tempArrayList.add(travellerName);
                    tempArrayListCode.add(travellerName);
                }
                break;
            case Constants.TITLE_SELECTION:
                headerText.setText(R.string.label_traveller_title);
                if (passengerType.equalsIgnoreCase("ADT")) {
                    String[] titleArray = activity.getResources().getStringArray(R.array.title_selection);
                    List<String> titleArrayList = Arrays.asList(titleArray);
                    String[] titleArrayCode = activity.getResources().getStringArray(R.array.passenger_title_selection);
                    List<String> titleCodeArrayList = Arrays.asList(titleArrayCode);
                    for (int i = 0; i < titleArrayList.size(); i++) {
                        tempArrayList.add(String.valueOf(titleArrayList.get(i)));
                        tempArrayListCode.add(String.valueOf(titleCodeArrayList.get(i)));
                    }
                } else if (passengerType.equalsIgnoreCase("CHD") || passengerType.equalsIgnoreCase("INF")) {
                    String[] childTitleArray = activity.getResources().getStringArray(R.array.child_title_selection);
                    List<String> childTitleListArray = Arrays.asList(childTitleArray);
                    String[] childTitleCodeArray = activity.getResources().getStringArray(R.array.passenger_child_selection);
                    List<String> childTitleCodeListArray = Arrays.asList(childTitleCodeArray);
                    for (int i = 0; i < childTitleListArray.size(); i++) {
                        tempArrayList.add(String.valueOf(childTitleListArray.get(i)));
                        tempArrayListCode.add(String.valueOf(childTitleCodeListArray.get(i)));
                    }
                } else {
                    String[] titleArray = activity.getResources().getStringArray(R.array.title_selection);
                    List<String> titleArrayList = Arrays.asList(titleArray);
                    String[] titleArrayCode = activity.getResources().getStringArray(R.array.passenger_title_selection);
                    List<String> titleCodeArrayList = Arrays.asList(titleArrayCode);
                    for (int i = 0; i < titleArrayList.size(); i++) {
                        tempArrayList.add(String.valueOf(titleArrayList.get(i)));
                        tempArrayListCode.add(String.valueOf(titleCodeArrayList.get(i)));
                    }
                }
                break;
            case Constants.PASSPORT_COUNTRY_SELECTION:
                headerText.setText(R.string.label_traveller_title_passport_country);
                if (isAirportAvailable) {
                    if (isArabicLang) {
                        for (int i = 0; i < countryNamesArray.size(); i++) {
                            tempArrayList.add(countryNamesArray.get(i).getCountryNameArabic());
                            tempArrayListCode.add(countryNamesArray.get(i).getIsoCountryCode());
                        }
                    } else {
                        for (int i = 0; i < countryNamesArray.size(); i++) {
                            tempArrayList.add(countryNamesArray.get(i).getCountryName());
                            tempArrayListCode.add(countryNamesArray.get(i).getIsoCountryCode());
                        }
                    }
                } else {
                    DbHandler dbHandlerCountry = new DbHandler(activity);
                    countryNameList = dbHandlerCountry.getCountryNameList();
                    for (int i = 0; i < countryNameList.size(); i++) {
                        tempArrayList.add(String.valueOf(countryNameList.get(i).getCountryName()));
                        tempArrayListCode.add(String.valueOf(countryNameList.get(i).getCountryName()));
                    }
                }
                break;
            case Constants.IQAMA_NATIONALITY:
                headerText.setText(R.string.label_traveller_title_nationality);
                if (isAirportAvailable) {
                    if (isArabicLang) {
                        for (int i = 0; i < countryNamesArray.size(); i++) {
                            tempArrayList.add(countryNamesArray.get(i).getCountryNameArabic());
                            tempArrayListCode.add(countryNamesArray.get(i).getIsoCountryCode());
                        }
                    } else {
                        for (int i = 0; i < countryNamesArray.size(); i++) {
                            tempArrayList.add(countryNamesArray.get(i).getCountryName());
                            tempArrayListCode.add(countryNamesArray.get(i).getIsoCountryCode());
                        }
                    }
                } else {
                    DbHandler dbHandlerNationality = new DbHandler(activity);
                    countryNameList = dbHandlerNationality.getCountryNameList();
                    for (int i = 0; i < countryNameList.size(); i++) {
                        tempArrayList.add(String.valueOf(countryNameList.get(i).getCountryName()));
                        tempArrayListCode.add(String.valueOf(countryNameList.get(i).getCountryName()));
                    }
                }
                break;
            case Constants.NATIONALITY_SELECTION:
                headerText.setText(R.string.label_traveller_title_nationality);
                if (isAirportAvailable) {
                    if (isArabicLang) {
                        for (int i = 0; i < countryNamesArray.size(); i++) {
                            tempArrayList.add(countryNamesArray.get(i).getCountryNameArabic());
                            tempArrayListCode.add(countryNamesArray.get(i).getIsoCountryCode());
                        }
                    } else {
                        for (int i = 0; i < countryNamesArray.size(); i++) {
                            tempArrayList.add(countryNamesArray.get(i).getCountryName());
                            tempArrayListCode.add(countryNamesArray.get(i).getIsoCountryCode());
                        }
                    }
                } else {
                    DbHandler dbHandlerNationality = new DbHandler(activity);
                    countryNameList = dbHandlerNationality.getCountryNameList();
                    for (int i = 0; i < countryNameList.size(); i++) {
                        tempArrayList.add(String.valueOf(countryNameList.get(i).getCountryName()));
                        tempArrayListCode.add(String.valueOf(countryNameList.get(i).getCountryName()));
                    }
                }
                break;
            case Constants.MEAL_SELECTION:
                headerText.setText(R.string.label_traveller_title_air_meal);
                DbHandler dbHandler = new DbHandler(activity);
                airMealList = dbHandler.getAirMealList();
                for (int i = 0; i < airMealList.size(); i++) {
                    tempArrayList.add(String.valueOf(airMealList.get(i).getAirMealName()));
                    tempArrayListCode.add(String.valueOf(airMealList.get(i).getAirMealName()));
                }
                break;
            case Constants.SEAT_SELECTION:
                headerText.setText(R.string.label_traveller_title_seat);
                String[] seatList = activity.getResources().getStringArray(R.array.seat_selection);
                List<String> seatArrayList = Arrays.asList(seatList);
                String[] seatCodeList = activity.getResources().getStringArray(R.array.seat_code_selection);
                List<String> seatCodeArrayList = Arrays.asList(seatCodeList);
                for (int i = 0; i < seatArrayList.size(); i++) {
                    tempArrayList.add(String.valueOf(seatArrayList.get(i)));
                    tempArrayListCode.add(String.valueOf(seatCodeArrayList.get(i)));
                }
                break;
            case Constants.SPECIAL_ASSISTANCE_SELECTION:
                headerText.setText(R.string.label_traveller_title_special_assistance);
                String[] assistanceList = activity.getResources().getStringArray(R.array.special_assistance_selection);
                List<String> assistanceArrayList = Arrays.asList(assistanceList);
                String[] assistanceCodeList = activity.getResources().getStringArray(R.array.special_assistance_code_selection);
                List<String> assistanceCodeArrayList = Arrays.asList(assistanceCodeList);
                for (int i = 0; i < assistanceArrayList.size(); i++) {
                    tempArrayList.add(String.valueOf(assistanceArrayList.get(i)));
                    tempArrayListCode.add(String.valueOf(assistanceCodeArrayList.get(i)));
                }
                break;
            case Constants.PASSENGER_FREQUENT_NAME:
                headerText.setText(R.string.label_select_frequent_name);
                InputStream inputStream = getContext().getResources().openRawResource(R.raw.ffplist);
                CSVFile csvFile = new CSVFile(inputStream);
                tempArrayList.clear();
                tempArrayList = csvFile.get_frequentnameList(Constants.airlinecode, isArabicLang);
                tempArrayListCode = tempArrayList;
                break;
            case Constants.CHILD_AGE_SELECTION:
                headerText.setText(R.string.label_child_age_selection_title);
                ArrayList<String> childAgeArrayList = new ArrayList<>();
                String childAgeArray = "";
                for (int i = 2; i <= 12; i++) {
                    if (i == 2) {
                        childAgeArray = i + " " + activity.getResources().getString(R.string.label_child_age_two_years);
                    } else if (i > 2 && i < 11) {
                        childAgeArray = i + " " + activity.getResources().getString(R.string.label_child_age_three_ten);
                    } else {
                        childAgeArray = i + " " + activity.getResources().getString(R.string.label_child_age_eleven_twelve);
                    }
                    childAgeArrayList.add(childAgeArray);
                }
                for (int i = 0; i < childAgeArrayList.size(); i++) {
                    tempArrayList.add(childAgeArrayList.get(i));
                    tempArrayListCode.add(String.valueOf(i));
                }
                break;
            case Constants.CLASS_TYPE_SELECTION:
                headerText.setText(R.string.label_select_class_title);
                String[] classArray = activity.getResources().getStringArray(R.array.class_type_selection);
                List<String> classArrayList = Arrays.asList(classArray);
                String[] classArrayCode = activity.getResources().getStringArray(R.array.class_type_selection_code);
                List<String> classCodeArrayList = Arrays.asList(classArrayCode);
                for (int i = 0; i < classArrayList.size(); i++) {
                    tempArrayList.add(String.valueOf(classArrayList.get(i)));
                    tempArrayListCode.add(String.valueOf(classCodeArrayList.get(i)));
                }
                break;
        }
        travellerDropDownAdapter = new TravellerListAdapter(activity, tempArrayList);
        travellerListView.setAdapter(travellerDropDownAdapter);
        travellerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedListItem = tempArrayList.get(position);
                selectedListCode = tempArrayListCode.get(position);
                selectedItemPosition = position;
                selectedChildAge = position + 2;
                dismiss();
            }
        });
    }
}
