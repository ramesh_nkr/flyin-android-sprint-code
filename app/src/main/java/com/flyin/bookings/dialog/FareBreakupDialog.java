package com.flyin.bookings.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

public class FareBreakupDialog extends Dialog {
    private Activity activity;
    private Double baseFare = 0.0;
    private Double taxesFare = 0.0;
    private Double totalFare = 0.0;

    public FareBreakupDialog(Activity activity, Double baseFare, Double taxesFare, Double totalFare) {
        super(activity);
        this.activity = activity;
        this.baseFare = baseFare;
        this.taxesFare = taxesFare;
        this.totalFare = totalFare;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_fare_breakup);
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_MEDIUM;
        if (Utils.isArabicLangSelected(activity)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        TextView fareBreakUpHeader = (TextView) findViewById(R.id.fare_break_up_header);
        TextView baseFareText = (TextView) findViewById(R.id.base_fare_text);
        TextView baseFarePrice = (TextView) findViewById(R.id.base_fare_price);
        TextView taxesFeesText = (TextView) findViewById(R.id.taxes_fees_text);
        TextView taxesFeesPrice = (TextView) findViewById(R.id.taxes_fees_price);
        TextView totalPriceText = (TextView) findViewById(R.id.total_price);
        TextView totalPriceFare = (TextView) findViewById(R.id.total_price_fare);
        ImageView closeIcon = (ImageView) findViewById(R.id.close_image);
        Typeface titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        Typeface textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        fareBreakUpHeader.setTypeface(textFace);
        baseFareText.setTypeface(titleFace);
        baseFarePrice.setTypeface(titleFace);
        taxesFeesText.setTypeface(titleFace);
        taxesFeesPrice.setTypeface(titleFace);
        totalPriceText.setTypeface(textFace);
        totalPriceFare.setTypeface(textFace);
        if (Utils.isArabicLangSelected(activity)) {
            fareBreakUpHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            baseFareText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            baseFarePrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            taxesFeesText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            taxesFeesPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
            totalPriceText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            totalPriceFare.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        String currencyResult = Utils.getPriceCurrencyCode(activity);
        String baseFareResult = "";
        String taxesFareResult = "";
        String totalFareResult = "";
        if (Utils.isArabicLangSelected(activity)) {
            baseFareResult = Utils.convertDecimalToUserPrice(baseFare, activity) + " " + currencyResult;
            taxesFareResult = Utils.convertDecimalToUserPrice(taxesFare, activity) + " " + currencyResult;
            totalFareResult = Utils.convertDecimalToUserPrice(totalFare, activity) + " " + currencyResult;
        } else {
            baseFareResult = currencyResult + " " + Utils.convertDecimalToUserPrice(baseFare, activity);
            taxesFareResult = currencyResult + " " + Utils.convertDecimalToUserPrice(taxesFare, activity);
            totalFareResult = currencyResult + " " + Utils.convertDecimalToUserPrice(totalFare, activity);
        }
        baseFarePrice.setText(baseFareResult);
        taxesFeesPrice.setText(taxesFareResult);
        totalPriceFare.setText(totalFareResult);
    }
}
