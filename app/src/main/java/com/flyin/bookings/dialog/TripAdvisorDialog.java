package com.flyin.bookings.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.flyin.bookings.R;
import com.flyin.bookings.adapter.TripDialogAdapter;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.TripDialogBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.ArrayList;

public class TripAdvisorDialog extends Dialog implements OnCustomItemSelectListener {
    private String hotelname = "";
    private Activity activity = null;
    private ArrayList<TripDialogBean> tripArrayList = new ArrayList();
    private ArrayList<TripDialogBean> tripArrayListTempary = new ArrayList();
    private TripDialogAdapter tripDialogAdapter = null;

    public TripAdvisorDialog(Activity activity, String hotelname, ArrayList<TripDialogBean> array) {
        super(activity);
        this.activity = activity;
        this.hotelname = hotelname;
        this.tripArrayList = array;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.tripadvisor_detail_dialog);
        String fontText = Constants.FONT_ROBOTO_MEDIUM;
        if (Utils.isArabicLangSelected(activity)) {
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        ImageView imageClose = (ImageView) findViewById(R.id.close_image);
        TextView headerTextView = (TextView) findViewById(R.id.main_header_text);
        ListView listView = (ListView) findViewById(R.id.listview);
        Typeface textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        headerTextView.setTypeface(textFace);
        if (Utils.isArabicLangSelected(activity)) {
            headerTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_text_size));
        }
        headerTextView.setText(hotelname);
        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tripArrayListTempary.addAll(tripArrayList);
        tripDialogAdapter = new TripDialogAdapter(activity, tripArrayList, this);
        listView.setAdapter(tripDialogAdapter);
        tripDialogAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int position) {
        try {
            TripDialogBean bean = tripArrayList.get(position);
            boolean expandStatus = bean.isExpanded();
            if (bean.getItemType() == TripDialogBean.RATING_HEADER) {
                if (expandStatus) {
                    tripArrayList.get(position).setExpanded(!expandStatus);
                    for (int i = 0; i < tripArrayListTempary.size(); i++) {
                        if (tripArrayListTempary.get(i).getItemType() == TripDialogBean.RATING_ITEM) {
                            for (TripDialogBean bean2 : tripArrayList) {
                                if (bean2.getItemType() == TripDialogBean.RATING_ITEM) {
                                    tripArrayList.remove(bean2);
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    tripArrayList.get(position).setExpanded(!expandStatus);
                    int count = position;
                    for (int i = 0; i < tripArrayListTempary.size(); i++) {
                        if (tripArrayListTempary.get(i).getItemType() == TripDialogBean.RATING_ITEM) {
                            count = count + 1;
                            tripArrayList.add(count, tripArrayListTempary.get(i));
                        }
                    }
                }
            } else if (bean.getItemType() == TripDialogBean.REVIEW_HEADER) {
                if (expandStatus) {
                    tripArrayList.get(position).setExpanded(!expandStatus);
                    for (int i = 0; i < tripArrayListTempary.size(); i++) {
                        if (tripArrayListTempary.get(i).getItemType() == TripDialogBean.REVIEW_ITEM) {
                            for (TripDialogBean bean2 : tripArrayList) {
                                if (bean2.getItemType() == TripDialogBean.REVIEW_ITEM) {
                                    tripArrayList.remove(bean2);
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    tripArrayList.get(position).setExpanded(!expandStatus);
                    int count = position;
                    for (int i = 0; i < tripArrayListTempary.size(); i++) {
                        if (tripArrayListTempary.get(i).getItemType() == TripDialogBean.REVIEW_ITEM) {
                            count = count + 1;
                            tripArrayList.add(count, tripArrayListTempary.get(i));
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        tripDialogAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSelectedItemClick(int position, int type) {
    }
}