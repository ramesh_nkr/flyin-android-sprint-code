package com.flyin.bookings.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.R;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.FSDataBean;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.OriginDestinationOptionBean;
import com.flyin.bookings.model.PricedItineraryObject;
import com.flyin.bookings.model.RoundImageTransform;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.HashMap;

@SuppressWarnings("deprecation")
public class ReturnFlightReviewDialog extends Dialog {
    private Activity activity = null;
    private static final String TAG = "ReturnFlightReviewDialog";
    private PricedItineraryObject pricedItineraryObject;
    private HashMap<String, FlightAirportName> airportNamesMap;
    private HashMap<String, String> airLineNamesMap;
    private String trvellerCount;
    private String sourceName, returnName;
    private String ddtPreviousTime;
    private OnCustomItemSelectListener listener;
    private int position;
    private String tripType;
    private String totalPackagePrice = "";
    private String returnFlightPrice = "";
    private String payFlightPrice;

    public ReturnFlightReviewDialog(Activity activity, PricedItineraryObject pricedItineraryObject, HashMap<String,
            FlightAirportName> airportNamesMap, HashMap<String, String> airLineNamesMap, String trvellerCount, String sourceName,
                                    String returnName, OnCustomItemSelectListener listener, int position, String tripType,
                                    String totalPackagePrice, String returnFlightPrice, String payFlightPrice) {
        super(activity);
        this.activity = activity;
        this.pricedItineraryObject = pricedItineraryObject;
        this.airportNamesMap = airportNamesMap;
        this.airLineNamesMap = airLineNamesMap;
        this.trvellerCount = trvellerCount;
        this.sourceName = sourceName;
        this.returnName = returnName;
        this.listener = listener;
        this.position = position;
        this.tripType = tripType;
        this.totalPackagePrice = totalPackagePrice;
        this.returnFlightPrice = returnFlightPrice;
        this.payFlightPrice = payFlightPrice;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.flight_review_dialog);
        TextView headerTextView = (TextView) findViewById(R.id.main_header_text);
        ImageView imageClose = (ImageView) findViewById(R.id.close_image);
        LinearLayout flightInfoLayout = (LinearLayout) findViewById(R.id.flights_details_layout);
        TextView refundableTextView = (TextView) findViewById(R.id.re_fund);
        TextView reviewPriceTextView = (TextView) findViewById(R.id.review_price);
        TextView priceTextView = (TextView) findViewById(R.id.price);
        TextView decimalTextView = (TextView) findViewById(R.id.price_decimal);
        TextView tripTextView = (TextView) findViewById(R.id.trip_text);
        TextView trvellerCountTextView = (TextView) findViewById(R.id.traveller_text);
        TextView currencyText = (TextView) findViewById(R.id.currency_text);
        Button continueButton = (Button) findViewById(R.id.continue_your_flight);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        String fontDefault = Constants.FONT_DROIDKUFI_REGULAR;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        boolean isArabicLang = false;
        if (Utils.isArabicLangSelected(activity)) {
            isArabicLang = true;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontDefault = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_DROIDKUFI_BOLD;
            headerTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_hotel_header_text));
            refundableTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
            currencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            priceTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.text_size));
            decimalTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            trvellerCountTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
            continueButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));
            reviewPriceTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.hotel_city_text_size));

        }
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), fontPath);
        Typeface titleFace = Typeface.createFromAsset(activity.getAssets(), fontTitle);
        Typeface textFace = Typeface.createFromAsset(activity.getAssets(), fontText);
        Typeface defaultFace = Typeface.createFromAsset(activity.getAssets(), fontDefault);
        Typeface boldFace = Typeface.createFromAsset(activity.getAssets(), fontBold);
        headerTextView.setTypeface(tf);
        refundableTextView.setTypeface(titleFace);
        reviewPriceTextView.setTypeface(titleFace);
        currencyText.setTypeface(titleFace);
        priceTextView.setTypeface(boldFace);
        decimalTextView.setTypeface(titleFace);
        trvellerCountTextView.setTypeface(titleFace);
        tripTextView.setTypeface(titleFace);
        continueButton.setTypeface(boldFace);
        continueButton.setText(activity.getString(R.string.label_reviewPage_continue_button));
        if (pricedItineraryObject.getRefundablestate()) {
            refundableTextView.setTextColor(Utils.getColor(activity, R.color.refund_text_color));
            refundableTextView.setText(activity.getString(R.string.RefLbl));
        } else {
            refundableTextView.setTextColor(Utils.getColor(activity, R.color.non_refund_text_color));
            refundableTextView.setText(activity.getString(R.string.NonRefLbl));
        }
        reviewPriceTextView.setVisibility(View.VISIBLE);
        if (tripType.equalsIgnoreCase("1")) {
            tripTextView.setText(activity.getString(R.string.label_search_oneway));
            tripTextView.setVisibility(View.GONE);
        } else if (tripType.equalsIgnoreCase("2")) {
            tripTextView.setText(activity.getString(R.string.label_search_roundtrip));
        }
        Double finalPrice = 0.0;
        Double pkgPrice = 0.0;
        Double returnPrice = 0.0;
        if (pricedItineraryObject.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).isOtherFlight()) {
            pkgPrice = Double.valueOf(totalPackagePrice);
            if (pkgPrice.equals(Double.valueOf(payFlightPrice))) {
                finalPrice = pkgPrice;
            } else {
                if (!returnFlightPrice.isEmpty()) {
                    returnPrice = Double.valueOf(returnFlightPrice);
                }
                finalPrice = Double.parseDouble(pricedItineraryObject.getAipiBean().getItfObject().getPriceDiff());
            }
        } else {
            pkgPrice = Double.valueOf(totalPackagePrice);
            if (pkgPrice.equals(Double.valueOf(payFlightPrice))) {
                finalPrice = pkgPrice;
            } else {
                if (!returnFlightPrice.isEmpty()) {
                    returnPrice = Double.valueOf(returnFlightPrice);
                }
                finalPrice = (pkgPrice - returnPrice) + Double.valueOf(payFlightPrice);
            }
        }
        String actualPriceResult = Utils.convertonly_PriceToUserSelectionPrice(finalPrice, activity.getApplicationContext());
        String priceResult[] = actualPriceResult.split("\\.");
        SharedPreferences pref = activity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String currencyResult = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        if (Utils.isArabicLangSelected(activity)) {
            currencyText.setTypeface(titleFace);
            priceTextView.setTypeface(boldFace);
            decimalTextView.setTypeface(titleFace);
            currencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            priceTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.text_size));
            decimalTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
            priceTextView.setText("." + priceResult[0] + " ");
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 0, 0, 0);
            currencyText.setLayoutParams(params);
            currencyText.setText(priceResult[1]);
            if (currencyResult.equalsIgnoreCase("SAR")) {
                decimalTextView.setText(activity.getString(R.string.label_SAR_currency_name));
            } else {
                decimalTextView.setText(currencyResult);
            }
        } else {
            currencyText.setText(currencyResult);
            priceTextView.setText(priceResult[0] + ".");
            decimalTextView.setText(priceResult[1]);
        }
        if (Integer.parseInt(trvellerCount) == 1) {
            trvellerCountTextView.setText(String.format(activity.getResources().getString(R.string.label_traveller), trvellerCount, activity.getResources().getString(R.string.label_add_traveller)));
        } else {
            trvellerCountTextView.setText(String.format(activity.getResources().getString(R.string.label_traveller), trvellerCount, activity.getResources().getString(R.string.label_search_flight_travellers)));
        }
        headerTextView.setText(String.format(activity.getResources().getString(R.string.label_flight_review), sourceName));
        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    dismiss();
                    listener.onSelectedItemClick(position, 1);
                }
            }
        });
        final OriginDestinationOptionBean data = pricedItineraryObject.getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
        int count = 0;
        FSDataBean fsDataBean = new FSDataBean();
        for (int i = 0; i < data.getFsDataBeanArrayList().size(); i++) {
            fsDataBean = data.getFsDataBeanArrayList().get(i);
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_flight_information, null);
            flightInfoLayout.addView(view);
            ImageView airlineImage = (ImageView) view.findViewById(R.id.flight_image);
            TextView airwaysName = (TextView) view.findViewById(R.id.airways_name);
            TextView airwaysCode = (TextView) view.findViewById(R.id.airways_code);
            TextView departureCode = (TextView) view.findViewById(R.id.departure_code);
            TextView layoverTime = (TextView) view.findViewById(R.id.layover_time);
            TextView arrivalCode = (TextView) view.findViewById(R.id.arrival_code);
            TextView departureName = (TextView) view.findViewById(R.id.departure_from);
            TextView departureTime = (TextView) view.findViewById(R.id.departure_time);
            TextView departLocation = (TextView) view.findViewById(R.id.departure_location);
            TextView departTerminal = (TextView) view.findViewById(R.id.departure_terminal);
            TextView arrivalLocation = (TextView) view.findViewById(R.id.arrival_location);
            TextView arrivalTerminal = (TextView) view.findViewById(R.id.arrival_terminal);
            TextView arrivalName = (TextView) view.findViewById(R.id.arrival_to);
            TextView arrivalTime = (TextView) view.findViewById(R.id.arrival_time);
            LinearLayout merchandiseDataLayout = (LinearLayout) view.findViewById(R.id.flight_less_info_layout);
            View amenitiesView = (View) view.findViewById(R.id.amenities_view);
            LinearLayout layoverLayout = (LinearLayout) view.findViewById(R.id.layover_layout);
            LinearLayout imageScrollLayout = (LinearLayout) view.findViewById(R.id.image_scroll_layout);
            LinearLayout firstDepartFromLayout = (LinearLayout) view.findViewById(R.id.departure_from_layout);
            LinearLayout firstDepartToLayout = (LinearLayout) view.findViewById(R.id.return_to_layout);
            final LinearLayout merchandiseInfoLayout = (LinearLayout) view.findViewById(R.id.merchandise_info_layout);
            ImageView wifiIcon = (ImageView) view.findViewById(R.id.wifi_icon);
            TextView wifiText = (TextView) view.findViewById(R.id.wifi_text);
            ImageView avodIcon = (ImageView) view.findViewById(R.id.avod_icon);
            TextView avodText = (TextView) view.findViewById(R.id.avod_text);
            ImageView seatIcon = (ImageView) view.findViewById(R.id.layout_icon);
            TextView seatText = (TextView) view.findViewById(R.id.layout_text);
            ImageView mealsIcon = (ImageView) view.findViewById(R.id.fresh_meal_icon);
            TextView mealsText = (TextView) view.findViewById(R.id.fresh_meal_text);
            TextView classType = (TextView) view.findViewById(R.id.class_type);
            TextView classTypeName = (TextView) view.findViewById(R.id.class_name_text);
            TextView baggageInfo = (TextView) view.findViewById(R.id.baggage_information);
            LinearLayout baggageInfoLayout = (LinearLayout) view.findViewById(R.id.baggage_layout);
            LinearLayout viewMoreLayout = (LinearLayout) view.findViewById(R.id.view_more_layout);
            final TextView viewMoreText = (TextView) view.findViewById(R.id.view_more_text);
            final ImageView viewMoreImage = (ImageView) view.findViewById(R.id.view_more_image);
            TextView merchandiseTextData = (TextView) view.findViewById(R.id.merchandise_text_data);
            TextView layoverText = (TextView) view.findViewById(R.id.layover_text);
            ImageView busIcon = (ImageView) view.findViewById(R.id.depart_bus_image);
            ImageView returnBusIcon = (ImageView) view.findViewById(R.id.return_bus_image);
            final LinearLayout defaultAmenitiesLayout = (LinearLayout) view.findViewById(R.id.default_amenities_layout);
            final LinearLayout flightAmenitiesLayout = (LinearLayout) view.findViewById(R.id.flight_amenities_layout);
            TextView flightAmenitiesText = (TextView) view.findViewById(R.id.flight_amenities_text);
            LinearLayout viewLessLayout = (LinearLayout) view.findViewById(R.id.view_less_layout);
            LinearLayout.LayoutParams stopsParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
            firstDepartFromLayout.setLayoutParams(stopsParam);
            firstDepartToLayout.setLayoutParams(stopsParam);
            if (isArabicLang) {
                baggageInfo.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                airwaysName.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                airwaysCode.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                departLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                departTerminal.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                arrivalLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                arrivalTerminal.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                departureCode.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                layoverTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                arrivalCode.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                departureName.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                arrivalName.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                departureTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                arrivalTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                layoverText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                wifiText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                avodText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                seatText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                mealsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                viewMoreText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_airline_name_text));
                merchandiseTextData.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                flightAmenitiesText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_time_text));
                classType.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                classTypeName.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.arabic_city_text));
                departureCode.setGravity(Gravity.START);
                arrivalCode.setGravity(Gravity.END);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    departureCode.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                    arrivalCode.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                }
            }
            airwaysName.setTypeface(boldFace);
            airwaysCode.setTypeface(titleFace);
            departureCode.setTypeface(titleFace);
            layoverTime.setTypeface(boldFace);
            departLocation.setTypeface(titleFace);
            departTerminal.setTypeface(titleFace);
            arrivalLocation.setTypeface(titleFace);
            arrivalTerminal.setTypeface(titleFace);
            arrivalCode.setTypeface(titleFace);
            classType.setTypeface(titleFace);
            classTypeName.setTypeface(boldFace);
            departureName.setTypeface(titleFace);
            departureTime.setTypeface(boldFace);
            baggageInfo.setTypeface(textFace);
            arrivalName.setTypeface(titleFace);
            arrivalTime.setTypeface(boldFace);
            layoverText.setTypeface(titleFace);
            wifiText.setTypeface(titleFace);
            avodText.setTypeface(titleFace);
            seatText.setTypeface(titleFace);
            mealsText.setTypeface(titleFace);
            viewMoreText.setTypeface(titleFace);
            flightAmenitiesText.setTypeface(titleFace);
            merchandiseTextData.setTypeface(defaultFace);
            if (count == 0) {
                layoverLayout.setVisibility(View.GONE);
                if (data.getFsDataBeanArrayList().get(0).getEq().equalsIgnoreCase("BUS")) {
                    busIcon.setVisibility(View.VISIBLE);
                }
            } else {
                layoverLayout.setVisibility(View.VISIBLE);
                data.setHeadingMessage(Utils.getDifferenceBetweenTwodaysInHrMinsFormat(fsDataBean.getDdt(), ddtPreviousTime, activity));
                if (data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getEq().equalsIgnoreCase("BUS")) {
                    returnBusIcon.setVisibility(View.VISIBLE);
                }
            }
            ddtPreviousTime = data.getFsDataBeanArrayList().get(i).getArdt();
            String departAirportName = airportNamesMap.get(fsDataBean.getDap()).getAirpotName();
            String arrivalAirportName = airportNamesMap.get(fsDataBean.getAap()).getAirpotName();
            String journeyAirline = airLineNamesMap.get(fsDataBean.getMal());
            String firstFlightName = fsDataBean.getMal() + " " + fsDataBean.getFn();
            airwaysCode.setText(String.format(activity.getResources().getString(R.string.label_airways_code_view), firstFlightName));
            String imagePath = Constants.IMAGE_BASE_URL + fsDataBean.getMal() + Constants.IMAGE_FILE_FORMAT;
            layoverTime.setText(Utils.formatDurationToString(data.getFsDataBeanArrayList().get(i).getDur(), activity));
            Glide.with(activity).load(imagePath).placeholder(R.drawable.imagethumb_search).into(airlineImage);
            departLocation.setText(departAirportName);
            arrivalLocation.setText(arrivalAirportName);
            airwaysName.setText(journeyAirline);
            departureCode.setText(fsDataBean.getDap());
            arrivalCode.setText(fsDataBean.getAap());
            departureName.setText(airportNamesMap.get(fsDataBean.getDap()).getCityName());
            arrivalName.setText(airportNamesMap.get(fsDataBean.getAap()).getCityName());
            departureTime.setText(Utils.formatDateToTime(fsDataBean.getDdt(), activity));
            arrivalTime.setText(Utils.formatDateToTime(fsDataBean.getArdt(), activity));
            String layoverTimeVal = activity.getResources().getString(R.string.label_tripSummary_layover_title) + ": " + data.getHeadingMessage() + " " + activity.getResources().getString(R.string.label_in) + " " + departAirportName;
            layoverText.setText(layoverTimeVal);
            if (fsDataBean.getDt().equalsIgnoreCase("null")) {
                departTerminal.setVisibility(View.GONE);
            } else {
                departTerminal.setVisibility(View.VISIBLE);
                departTerminal.setText(activity.getString(R.string.label_terminal) + " " + fsDataBean.getDt());
            }
            if (data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getAt().equalsIgnoreCase("null")) {
                arrivalTerminal.setVisibility(View.GONE);
            } else {
                arrivalTerminal.setVisibility(View.VISIBLE);
                arrivalTerminal.setText(activity.getString(R.string.label_terminal) + " " + data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getAt());
            }
            String cabinName = data.getFsDataBeanArrayList().get(0).getCt();
            String journeyCabinType = "";
            if (cabinName.equalsIgnoreCase("Economy")) {
                journeyCabinType = activity.getString(R.string.label_search_flight_economy);
            } else if (cabinName.equalsIgnoreCase("Business")) {
                journeyCabinType = activity.getString(R.string.label_search_flight_business);
            } else if (cabinName.equalsIgnoreCase("First")) {
                journeyCabinType = activity.getString(R.string.label_search_flight_first);
            }
            classType.setText(activity.getResources().getString(R.string.label_cabin_class) + ":");
            classTypeName.setText(journeyCabinType);
            String checkInDetails = Utils.checkStringVal(data.getCb());
            if (!checkInDetails.equalsIgnoreCase("")) {
                String baggageInformation = "";
                String[] baggageDetails = checkInDetails.split(" ");
                if (baggageDetails[1].equalsIgnoreCase("Kilos")) {
                    baggageInformation = baggageDetails[0] + " " + activity.getString(R.string.label_kilo);
                } else if (baggageDetails[1].equalsIgnoreCase("Pieces")) {
                    baggageInformation = baggageDetails[0] + " " + activity.getString(R.string.label_pieces);
                } else {
                    baggageInformation = checkInDetails;
                }
                if (isArabicLang) {
                    baggageInfo.setText(activity.getString(R.string.label_reviewPage_baggageInfo) + " '" + baggageInformation + "' (" + activity.getString(R.string.label_per_person_text) + ")");
                } else {
                    baggageInfo.setText(activity.getString(R.string.label_reviewPage_baggageInfo) + " " + activity.getString(R.string.label_reviewPage_checkIn) + " '" + baggageInformation + "' (" + activity.getString(R.string.label_per_person_text) + ")");
                }
            } else {
                baggageInfoLayout.setVisibility(View.GONE);
            }
            count++;
            if (fsDataBean.getFlightAmenitiesBean() != null) {
                merchandiseDataLayout.setVisibility(View.VISIBLE);
                amenitiesView.setVisibility(View.VISIBLE);
                if (!fsDataBean.getFlightAmenitiesBean().getWifi().isEmpty()) {
                    wifiIcon.setImageResource(R.drawable.wifi);
                    wifiText.setTextColor(Utils.getColor(activity, R.color.result_compo_duration));
                }
                if (!fsDataBean.getFlightAmenitiesBean().getAvod().isEmpty() && !fsDataBean.getFlightAmenitiesBean().getAvod().equalsIgnoreCase("NA")) {
                    avodIcon.setImageResource(R.drawable.avod);
                    avodText.setTextColor(Utils.getColor(activity, R.color.result_compo_duration));
                }
                if (!fsDataBean.getFlightAmenitiesBean().getMeals().isEmpty()) {
                    mealsIcon.setImageResource(R.drawable.fresh_meal);
                    mealsText.setTextColor(Utils.getColor(activity, R.color.result_compo_duration));
                }

                if (!fsDataBean.getFlightAmenitiesBean().getSeatLayout().isEmpty()) {
                    seatIcon.setImageResource(R.drawable.seat_layout);
                    seatText.setTextColor(Utils.getColor(activity, R.color.button_bg_blue));
                    seatText.setText(fsDataBean.getFlightAmenitiesBean().getSeatLayout());
                } else {
                    seatIcon.setVisibility(View.GONE);
                    seatText.setVisibility(View.GONE);
                }
                if (!fsDataBean.getFlightAmenitiesBean().getAircraftImage().isEmpty() || !fsDataBean.getFlightAmenitiesBean().getAircraftImage().equalsIgnoreCase("")) {
                    View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                    ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                    TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                    int imageScaledWidth = activity.getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                    int imageScaledHeight = activity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                    String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                            Constants.IMAGE_QUALITY + "" + fsDataBean.getFlightAmenitiesBean().getAircraftImage();
                    Glide.with(activity).load(url).placeholder(R.drawable.imagethumb_search)
                            .bitmapTransform(new RoundImageTransform(activity, 0, 0)).into(flightMerchandiseImage);
                    imageDescriptionText.setText(activity.getString(R.string.label_aircraft));
                    imageDescriptionText.setTypeface(titleFace);
                    imageScrollLayout.addView(scrollImgView);
                }
                if (!fsDataBean.getFlightAmenitiesBean().getClassOverviewImage().isEmpty() || !fsDataBean.getFlightAmenitiesBean().getClassOverviewImage().equalsIgnoreCase("")) {
                    View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                    ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                    TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                    int imageScaledWidth = activity.getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                    int imageScaledHeight = activity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                    String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                            Constants.IMAGE_QUALITY + "" + fsDataBean.getFlightAmenitiesBean().getClassOverviewImage();
                    Glide.with(activity).load(url).placeholder(R.drawable.imagethumb_search)
                            .bitmapTransform(new RoundImageTransform(activity, 0, 0)).into(flightMerchandiseImage);
                    imageDescriptionText.setText(activity.getString(R.string.label_class_overview));
                    imageDescriptionText.setTypeface(titleFace);
                    imageScrollLayout.addView(scrollImgView);
                }
                if (!fsDataBean.getFlightAmenitiesBean().getSeatImage().isEmpty() || !fsDataBean.getFlightAmenitiesBean().getSeatImage().equalsIgnoreCase("")) {
                    View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                    ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                    TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                    int imageScaledWidth = activity.getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                    int imageScaledHeight = activity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                    String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                            Constants.IMAGE_QUALITY + "" + fsDataBean.getFlightAmenitiesBean().getSeatImage();
                    Glide.with(activity).load(url).placeholder(R.drawable.imagethumb_search)
                            .bitmapTransform(new RoundImageTransform(activity, 0, 0)).into(flightMerchandiseImage);
                    imageDescriptionText.setText(activity.getString(R.string.label_seat_image));
                    imageDescriptionText.setTypeface(titleFace);
                    imageScrollLayout.addView(scrollImgView);
                }
                if (!fsDataBean.getFlightAmenitiesBean().getFacilityImage().isEmpty() || !fsDataBean.getFlightAmenitiesBean().getFacilityImage().equalsIgnoreCase("")) {
                    View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                    ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                    TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                    int imageScaledWidth = activity.getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                    int imageScaledHeight = activity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                    String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                            Constants.IMAGE_QUALITY + "" + fsDataBean.getFlightAmenitiesBean().getFacilityImage();
                    Glide.with(activity).load(url).placeholder(R.drawable.imagethumb_search)
                            .bitmapTransform(new RoundImageTransform(activity, 0, 0)).into(flightMerchandiseImage);
                    imageDescriptionText.setText(activity.getString(R.string.label_facility_image));
                    imageDescriptionText.setTypeface(titleFace);
                    imageScrollLayout.addView(scrollImgView);
                }
                addNameIconsToText(merchandiseTextData, fsDataBean);
                viewMoreLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        flightAmenitiesLayout.setVisibility(View.VISIBLE);
                        defaultAmenitiesLayout.setVisibility(View.GONE);
                    }
                });
                viewLessLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        flightAmenitiesLayout.setVisibility(View.GONE);
                        defaultAmenitiesLayout.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                merchandiseDataLayout.setVisibility(View.GONE);
                // amenitiesView.setVisibility(View.GONE);
            }
        }
    }

    private void addNameIconsToText(TextView merchandiseTextData, FSDataBean fsDataBean) {
        merchandiseTextData.setText("");
        if (Utils.isArabicLangSelected(activity)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                merchandiseTextData.setTextDirection(View.TEXT_DIRECTION_ANY_RTL);
            }
        }
        SpannableStringBuilder builder = new SpannableStringBuilder();
        int flightAmenitiesCnt = 0;
        for (int i = 0; i < fsDataBean.getIconNameArrayList().size(); i++) {
            String iconNameArray = fsDataBean.getIconNameArrayList().get(i);
            if (flightAmenitiesCnt != 0 && flightAmenitiesCnt % 2 == 0) {
                builder.append("\n");
            }
            String[] s = iconNameArray.split("==");
            try {
                if (s.length == 2) {
                    if (!s[0].isEmpty()) {
                        int resId = activity.getResources().getIdentifier(s[0], "drawable", activity.getPackageName());
                        Drawable icon1 = activity.getResources().getDrawable(resId);
                        if (icon1 != null) {
                            int imageScaledSize = activity.getResources().getDimensionPixelSize(R.dimen.search_filter_margin);
                            icon1.setBounds(0, 0, (icon1.getIntrinsicWidth() - imageScaledSize), (icon1.getIntrinsicHeight() - imageScaledSize));
                        }
                        builder.append(" ");
                        builder.setSpan(new ImageSpan(icon1, ImageSpan.ALIGN_BASELINE), builder.length() - 1, builder.length(), 0);
                        builder.append(" " + s[1] + "  ");
                        flightAmenitiesCnt++;
                    } else {
                        if (flightAmenitiesCnt != 0 && flightAmenitiesCnt % 2 == 0) {
                            builder.append(s[1]);
                        } else {
                            builder.append("\n" + s[1]);
                        }
                    }
                } else {
                    int resId = activity.getResources().getIdentifier(s[0], "drawable", activity.getPackageName());
                    Drawable icon1 = activity.getResources().getDrawable(resId);
                    if (icon1 != null) {
                        int imageScaledSize = activity.getResources().getDimensionPixelSize(R.dimen.search_filter_margin);
                        icon1.setBounds(0, 0, (icon1.getIntrinsicWidth() - imageScaledSize), (icon1.getIntrinsicHeight() - imageScaledSize));
                    }
                    builder.append(" ");
                    builder.setSpan(new ImageSpan(icon1, ImageSpan.ALIGN_BASELINE), builder.length() - 1, builder.length(), 0);
                    builder.append(" " + s[1]);
                    builder.append(Utils.addFlightAmenitiesField(s[2], activity));
                    if (s[0].equalsIgnoreCase("avod")) {
                        flightAmenitiesCnt += 1;
                    } else {
                        flightAmenitiesCnt += 2;
                    }
                }
            } catch (Exception e) {
                Utils.printMessage(TAG, "ex:" + e.getMessage());
            }
        }
        merchandiseTextData.setText(builder);
    }
}
