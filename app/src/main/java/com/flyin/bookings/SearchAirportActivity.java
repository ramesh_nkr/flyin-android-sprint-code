package com.flyin.bookings;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyin.bookings.adapter.AirportAdapter;
import com.flyin.bookings.listeners.AirlinesInterface;
import com.flyin.bookings.model.PredectiveSearchModel;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.DbHandler;
import com.flyin.bookings.util.GPSTracker;
import com.flyin.bookings.util.LocationSearchData;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class SearchAirportActivity extends AppCompatActivity implements AsyncTaskListener {
    private static final String TAG = "SearchAirportActivity";
    private TextView cancelAirport;
    private LinearLayout searchAirportScreenLayout, searchAirportTextView, recentSearchText;
    private Typeface textFace;
    private ListView searchListView;
    private AirportAdapter airportAdapter;
    private EditText inputSearch;
    private ProgressDialog barProgressDialog = null;
    private boolean mIsRefreshing = false;
    private ArrayList<PredectiveSearchModel> predectiveSearchList = null;
    private String searchString = "";
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private static final int GET_RESPONSE = 1;
    private boolean isInternetPresent = false;
    private ImageView errorImage = null;
    private Button searchButton = null;
    private RelativeLayout headerViewLayout = null;
    private ProgressBar progressBar = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_airport);
        searchListView = (ListView) findViewById(R.id.search_airports_listView);
        DbHandler dbHandler = new DbHandler(SearchAirportActivity.this);
        predectiveSearchList = new ArrayList<>();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        headerViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        isInternetPresent = Utils.isConnectingToInternet(SearchAirportActivity.this);
        predectiveSearchList = dbHandler.getRecentAirportsListPredict();
        airportAdapter = new AirportAdapter(SearchAirportActivity.this, predectiveSearchList);
        searchListView.setAdapter(airportAdapter);
        airportAdapter.notifyDataSetChanged();
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        TextView searchAirportText = (TextView) findViewById(R.id.search_airport_title);
        TextView currentLocationText = (TextView) findViewById(R.id.current_location_title);
        TextView recentlySearchedText = (TextView) findViewById(R.id.recently_searched_title);
        cancelAirport = (TextView) findViewById(R.id.cancel_airport);
        searchAirportScreenLayout = (LinearLayout) findViewById(R.id.search_airport_screen_layout);
        inputSearch = (EditText) findViewById(R.id.search_airport_view);
        searchAirportTextView = (LinearLayout) findViewById(R.id.search_airport_textView);
        recentSearchText = (LinearLayout) findViewById(R.id.recent_search_text);
        LinearLayout currentLocationLayout = (LinearLayout) findViewById(R.id.current_location_layout);
        ImageView clearTextImage = (ImageView) findViewById(R.id.clear_text_image);
        if (Utils.isArabicLangSelected(SearchAirportActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_DROIDKUFI_BOLD;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                searchAirportText.setTextDirection(View.TEXT_DIRECTION_RTL);
                inputSearch.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
            currentLocationText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            recentlySearchedText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            searchAirportText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            inputSearch.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            cancelAirport.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textFace = Typeface.createFromAsset(getAssets(), fontTitle);
        Typeface boldFace = Typeface.createFromAsset(getAssets(), fontBold);
        searchAirportText.setTypeface(textFace);
        currentLocationText.setTypeface(boldFace);
        recentlySearchedText.setTypeface(boldFace);
        cancelAirport.setTypeface(textFace);
        inputSearch.setTypeface(textFace);
        searchAirportText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchAirportTextView.setVisibility(View.GONE);
                searchAirportScreenLayout.setVisibility(View.VISIBLE);
                recentSearchText.setVisibility(View.GONE);
                inputSearch.requestFocus();
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                predectiveSearchList = new ArrayList<>();
                airportAdapter = new AirportAdapter(SearchAirportActivity.this, predectiveSearchList);
                searchListView.setAdapter(airportAdapter);
            }
        });
        cancelAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchAirportTextView.setVisibility(View.VISIBLE);
                searchAirportScreenLayout.setVisibility(View.GONE);
                recentSearchText.setVisibility(View.VISIBLE);
                final InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.hideSoftInputFromWindow(cancelAirport.getWindowToken(), 0);
                inputSearch.setText("");
                onStop();
                DbHandler dbHandler = new DbHandler(SearchAirportActivity.this);
                predectiveSearchList = dbHandler.getRecentAirportsListPredict();
                airportAdapter = new AirportAdapter(SearchAirportActivity.this, predectiveSearchList);
                searchListView.setAdapter(airportAdapter);
                airportAdapter.notifyDataSetChanged();
            }
        });
        clearTextImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputSearch.setText("");
                predectiveSearchList.clear();
                airportAdapter.notifyDataSetChanged();
            }
        });
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if ((inputSearch.getText().toString()).trim().length() >= 3) {
                    searchFlights(inputSearch.getText().toString().trim());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
        searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
                String airportCode = predectiveSearchList.get(position).getAirportCode();
                String airportName = predectiveSearchList.get(position).getAirportName();
                String airportCity = predectiveSearchList.get(position).getCityName();
                String countryName = predectiveSearchList.get(position).getCountryName();
                int cityId = predectiveSearchList.get(position).getCityId();
                Intent it = getIntent();
                it.putExtra(Constants.FROM_AIRPORT_CODE, airportCode);
                it.putExtra(Constants.FROM_AIRPORT, airportName);
                it.putExtra(Constants.FROM_AIRPORT_CITY, airportCity);
                it.putExtra(Constants.CITY_ID_VALUE, String.valueOf(cityId));
                setResult(RESULT_OK, it);
                DbHandler dbHandler = new DbHandler(SearchAirportActivity.this);
                dbHandler.addRecentAirport(airportCode, airportName, airportCity, countryName);
                finish();
            }
        });
        currentLocationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadGPSValues();
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_airport_city_title);
        mTitleText.setTypeface(tf);
        backText.setTypeface(textFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(SearchAirportActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    private static void requestPermission(final Context context) {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION)) {
            new AlertDialog.Builder(context)
                    .setMessage(context.getResources().getString(R.string.permission_storage))
                    .setPositiveButton(R.string.label_selected_flight_message, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                    2);
                        }
                    }).show();
        } else {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    2);
        }
    }

    private void showLoading() {
        if (barProgressDialog == null) {
            barProgressDialog = new ProgressDialog(SearchAirportActivity.this);
            barProgressDialog.setMessage(getString(R.string.label_wait_please_message));
            barProgressDialog.setProgressStyle(barProgressDialog.STYLE_SPINNER);
            barProgressDialog.setCancelable(false);
            barProgressDialog.show();
        }
    }

    private void closeLoading() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if (barProgressDialog != null) {
                    barProgressDialog.dismiss();
                    barProgressDialog = null;
                }
            }
        }, 500);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (barProgressDialog != null) {
            barProgressDialog.dismiss();
            barProgressDialog = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Utils.printMessage(TAG, "onRequestPermissionsResult::" + grantResults.length);
        switch (requestCode) {
            case 2: {
                if (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    loadGPSValues();
                    Utils.printMessage(TAG, "permission granted");
                } else {
                    Utils.printMessage(TAG, "permission granted failure");
                }
                return;
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setRefreshing(boolean isVisible) {
        if (isVisible) {
            if (!mIsRefreshing) {
                mIsRefreshing = true;
                progressBar.setVisibility(View.VISIBLE);
            }
        } else {
            if (mIsRefreshing) {
                mIsRefreshing = false;
                progressBar.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void searchFlights(final String searchText) {
        if (isInternetPresent) {
            if (searchText.length() >= 3) {
                searchString = searchText;
                setRefreshing(true);
                getRequestFromServer();
            }
        } else {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(SearchAirportActivity.this);
                    if (isInternetPresent) {
                        headerViewLayout.removeView(errorView);
                        if (searchText.length() >= 3) {
                            searchString = searchText;
                            setRefreshing(true);
                            getRequestFromServer();
                        }
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            headerViewLayout.addView(errorView);
        }
    }

    /**
     * Requesting flight to server using get method
     **/
    private void getRequestFromServer() {
        try {
            new HTTPAsync(SearchAirportActivity.this, SearchAirportActivity.this,
                    Constants.LIVE_PREDECTIVE_SEARCH_URL, "" + URLEncoder.encode("" + searchString, "UTF-8"), GET_RESPONSE, HTTPAsync.METHOD_GET)
                    .execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "DATA:: " + data);
        setRefreshing(false);
        predectiveSearchList.clear();
        JSONArray jsonArray = null;
        if (!data.equalsIgnoreCase("")) {
            try {
                jsonArray = new JSONArray(data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    PredectiveSearchModel predectiveSearchModel = new PredectiveSearchModel();
                    predectiveSearchModel.setText(jsonArray.getJSONObject(i).getString("text"));
                    predectiveSearchModel.setScore(jsonArray.getJSONObject(i).getInt("score"));
                    predectiveSearchModel.setAirportCode(jsonArray.getJSONObject(i).getJSONObject("payload").getString("airportCode"));
                    predectiveSearchModel.setAirportName(jsonArray.getJSONObject(i).getJSONObject("payload").getString("airportName"));
                    predectiveSearchModel.setCityId(jsonArray.getJSONObject(i).getJSONObject("payload").getInt("cityId"));
                    predectiveSearchModel.setCityName(jsonArray.getJSONObject(i).getJSONObject("payload").getString("cityName"));
                    predectiveSearchModel.setCountryCode(jsonArray.getJSONObject(i).getJSONObject("payload").getString("countryCode"));
                    predectiveSearchModel.setCountryName(jsonArray.getJSONObject(i).getJSONObject("payload").getString("countryName"));
                    predectiveSearchModel.setProvinceId(jsonArray.getJSONObject(i).getJSONObject("payload").getString("provinceId"));
                    predectiveSearchModel.setProvinceName(jsonArray.getJSONObject(i).getJSONObject("payload").getString("provinceName"));
                    predectiveSearchList.add(predectiveSearchModel);
                }
                airportAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {

    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getResources().getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getResources().getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getResources().getString(R.string.label_network_error));
                errorDescriptionText.setText(getResources().getString(R.string.label_network_error_message));
                searchButton.setText(getResources().getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getResources().getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getResources().getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getResources().getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getResources().getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getResources().getString(R.string.label_no_results_found_message));
                searchButton.setText(getResources().getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private void loadGPSValues() {
        if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(SearchAirportActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(SearchAirportActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                ) {
            requestPermission(SearchAirportActivity.this);
            return;
        }
        GPSTracker gps = new GPSTracker(SearchAirportActivity.this);
        if (gps.canGetLocation()) {
            LocationSearchData searchData = new LocationSearchData();
            showLoading();
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            if (latitude == 0.0 && longitude == 0.0) {
                if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(SearchAirportActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                        PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(SearchAirportActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                        ) {
                    requestPermission(SearchAirportActivity.this);
                    return;
                } else {
                    closeLoading();
                }
            } else {
                String json = makeJson(SearchAirportActivity.this, latitude, longitude);
                searchData.resultData(SearchAirportActivity.this, json, new AirlinesInterface<ArrayList<PredectiveSearchModel>>() {
                    @Override
                    public void onSuccess(ArrayList<PredectiveSearchModel> response) {
                        if (response.size() > 0) {
                            predectiveSearchList.clear();
                            Utils.printMessage(TAG, response.get(0).getAirportName());
                            Utils.printMessage(TAG, response.get(0).getCityName());
                            predectiveSearchList.addAll(response);
                            airportAdapter.notifyDataSetChanged();
                            closeLoading();
                        }
                    }

                    @Override
                    public void onFailed(String message) {
                        Utils.printMessage(TAG, message);
                        closeLoading();
                        Toast.makeText(SearchAirportActivity.this, getString(R.string.label_error_flights), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            gps.showSettingsAlert();
        }
    }

    private String makeJson(Context contex, double lat, double lng) {
        final SharedPreferences preferences = contex.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String language = preferences.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
            mainJSON.put("lang", language);
            mainJSON.put("lat", lat);
            mainJSON.put("lng", lng);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }
}
