package com.flyin.bookings.model;

import java.io.Serializable;
import java.util.ArrayList;

public class AirItineraryObjectBean implements Serializable {
    private String dirId = "";
    private ArrayList<OriginDestinationOptionBean> originDestinationOptionBeanArrayList;
    private long segmentFlightDuration;
    private int segmentDepartureTime;

    public long getSegmentFlightDuration() {
        return segmentFlightDuration;
    }

    public void setSegmentFlightDuration(long segmentFlightDuration) {
        this.segmentFlightDuration = segmentFlightDuration;
    }

    public int getSegmentDepartureTime() {
        return segmentDepartureTime;
    }

    public void setSegmentDepartureTime(int segmentDepartureTime) {
        this.segmentDepartureTime = segmentDepartureTime;
    }

    public String getDirId() {
        return dirId;
    }

    public void setDirId(String dirId) {
        this.dirId = dirId;
    }

    public ArrayList<OriginDestinationOptionBean> getOriginDestinationOptionBeanArrayList() {
        return originDestinationOptionBeanArrayList;
    }

    public void setOriginDestinationOptionBeanArrayList(ArrayList<OriginDestinationOptionBean> originDestinationOptionBeanArrayList) {
        this.originDestinationOptionBeanArrayList = originDestinationOptionBeanArrayList;
    }
}
