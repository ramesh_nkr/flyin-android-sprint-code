package com.flyin.bookings.model;

import java.io.Serializable;

public class TravellerDetailsBean implements Serializable {
    private boolean isExpanded;
    private String travellerHeaderName;
    private String ticketNumber;
    private String travellerHeader;
    private String travellerName;
    private Integer rowId = 0;
    private String title = "";
    private String titleCode = "";
    private String firstName = "";
    private String middleName = "";
    private String lastName = "";
    private String email = "";
    private String phoneNo = "";
    private int countrycode_position = 0;
    private String passportNumber = "";
    private String passportIssuingCountry = "";
    private String passportExpiry = "";
    private String dateOfBirth = "";
    private String nationality = "";
    private String mealPreferences = "";
    private String seatPreferences = "";
    private String specialAssistance = "";
    private String headingMessage = "";
    private String passengerType = "";
    private boolean isSecondPassenger;
    private String passportIssuingCountryCode = "";
    private String nationalityCode = "";
    private String mealPreferencesCode = "";
    private String mobileAreaCode = "";
    private String profileId = "";
    private String address = "";
    private String passportIssueDate = "";
    private boolean isSpecialRequestExpanded;
    private boolean isSpecialRequestOpened;
    private boolean isPassportSelected;
    private boolean isfrequentflyerExpanded;
    private String frequentflyername = "";
    private String frequentFlyerNumber = "";
    private String documentType = "";
    private boolean isIqamaStuffExpanded;
    private boolean isIqamaSelected;
    private String iqamaNationalityCode = "";
    private String iqamaIDNumber = "";
    private String iqamaIDNationality = "";
    private String iqamaIDExpiryDate = "";
    private boolean isHijriChecked;
    private String seatPreferencesCode = "";
    private String specialAssistanceCode = "";
    private int mResId = 0;
    private String phoneCode = "";
    private String travellerEmail = "";
    private String country = "";
    private boolean isRegUser;

    public String getTravellerHeader() {
        return travellerHeader;
    }

    public void setTravellerHeader(String travellerHeader) {
        this.travellerHeader = travellerHeader;
    }

    public String getTravellerName() {
        return travellerName;
    }

    public void setTravellerName(String travellerName) {
        this.travellerName = travellerName;
    }

    public boolean getIsExpanded() {
        return isExpanded;
    }

    public void setIsExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

    public String getTravellerHeaderName() {
        return travellerHeaderName;
    }

    public void setTravellerHeaderName(String travellerHeaderName) {
        this.travellerHeaderName = travellerHeaderName;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public boolean isIqamaStuffExpanded() {
        return isIqamaStuffExpanded;
    }

    public void setIsIqamaStuffExpanded(boolean isIqamaStuffExpanded) {
        this.isIqamaStuffExpanded = isIqamaStuffExpanded;
    }

    public boolean isIqamaSelected() {
        return isIqamaSelected;
    }

    public void setIqamaSelected(boolean iqamaSelected) {
        isIqamaSelected = iqamaSelected;
    }

    public boolean isSpecialRequestExpanded() {
        return isSpecialRequestExpanded;
    }

    public boolean isfrequentflyerExpanded() {
        return isfrequentflyerExpanded;
    }

    public void setIsSpecialRequestExpanded(boolean isSpecialRequestExpanded) {
        this.isSpecialRequestExpanded = isSpecialRequestExpanded;
    }

    public void setIsfrequentflyerExpanded(boolean isfrequentflyerExpanded) {
        this.isfrequentflyerExpanded = isfrequentflyerExpanded;
    }

    public boolean isSpecialRequestOpened() {
        return isSpecialRequestOpened;
    }

    public void setIsSpecialRequestOpened(boolean isSpecialRequestOpened) {
        this.isSpecialRequestOpened = isSpecialRequestOpened;
    }

    public boolean isPassportSelected() {
        return isPassportSelected;
    }

    public void setIsPassportSelected(boolean isPassportSelected) {
        this.isPassportSelected = isPassportSelected;
    }

    public boolean isHijriChecked() {
        return isHijriChecked;
    }

    public void setHijriChecked(boolean hijriChecked) {
        isHijriChecked = hijriChecked;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getMobileAreaCode() {
        return mobileAreaCode;
    }

    public void setMobileAreaCode(String mobileAreaCode) {
        this.mobileAreaCode = mobileAreaCode;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public boolean isSecondPassenger() {
        return isSecondPassenger;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassportIssueDate() {
        return passportIssueDate;
    }

    public void setPassportIssueDate(String passportIssueDate) {
        this.passportIssueDate = passportIssueDate;
    }

    public void setIsSecondPassenger(boolean isSecondPassenger) {
        this.isSecondPassenger = isSecondPassenger;
    }

    public String getPassportIssuingCountryCode() {
        return passportIssuingCountryCode;
    }

    public void setPassportIssuingCountryCode(String passportIssuingCountryCode) {
        this.passportIssuingCountryCode = passportIssuingCountryCode;
    }

    public String getIqamaNationalityCode() {
        return iqamaNationalityCode;
    }

    public void setIqamaNationalityCode(String iqamaNationalityCode) {
        this.iqamaNationalityCode = iqamaNationalityCode;
    }

    public String getIqamaIDNumberCode() {
        return iqamaIDNumber;
    }

    public void setIqamaIDNumberCode(String iqamaIDNumberCode) {
        this.iqamaIDNumber = iqamaIDNumberCode;

    }

    public String getIqamaIDExpiryDate() {
        return iqamaIDExpiryDate;
    }

    public void setIqamaIDExpiryDate(String iqamaIDExpiryDate) {
        this.iqamaIDExpiryDate = iqamaIDExpiryDate;
    }

    public String getIqamaNationality() {
        return iqamaIDNationality;
    }

    public void setIqamaNationality(String iqamaIDNationality) {
        this.iqamaIDNationality = iqamaIDNationality;
    }

    public String getNationalityCode() {
        return nationalityCode;
    }

    public void setNationalityCode(String nationalityCode) {
        this.nationalityCode = nationalityCode;
    }

    public String getMealPreferencesCode() {
        return mealPreferencesCode;
    }

    public void setMealPreferencesCode(String mealPreferencesCode) {
        this.mealPreferencesCode = mealPreferencesCode;
    }

    public String getSeatPreferencesCode() {
        return seatPreferencesCode;
    }

    public void setSeatPreferencesCode(String seatPreferencesCode) {
        this.seatPreferencesCode = seatPreferencesCode;
    }

    public String getSpecialAssistanceCode() {
        return specialAssistanceCode;
    }

    public void setSpecialAssistanceCode(String specialAssistanceCode) {
        this.specialAssistanceCode = specialAssistanceCode;
    }

    public Integer getRowId() {
        return rowId;
    }

    public void setRowId(Integer rowId) {
        this.rowId = rowId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleCode() {
        return titleCode;
    }

    public void setTitleCode(String titleCode) {
        this.titleCode = titleCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
//        String filtername = Utils.getOnlyStrings(middleName);
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public int getmResId() {
        return mResId;
    }

    public void setmResId(int mResId) {
        this.mResId = mResId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportIssuingCountry() {
        return passportIssuingCountry;
    }

    public void setPassportIssuingCountry(String passportIssuingCountry) {
        this.passportIssuingCountry = passportIssuingCountry;
    }

    public String getPassportExpiry() {
        return passportExpiry;
    }

    public void setPassportExpiry(String passportExpiry) {
        this.passportExpiry = passportExpiry;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMealPreferences() {
        return mealPreferences;
    }

    public void setMealPreferences(String mealPreferences) {
        this.mealPreferences = mealPreferences;
    }

    public String getSeatPreferences() {
        return seatPreferences;
    }

    public void setSeatPreferences(String seatPreferences) {
        this.seatPreferences = seatPreferences;
    }

    public String getSpecialAssistance() {
        return specialAssistance;
    }

    public void setSpecialAssistance(String specialAssistance) {
        this.specialAssistance = specialAssistance;
    }

    public String getHeadingMessage() {
        return headingMessage;
    }

    public void setHeadingMessage(String headingMessage) {
        this.headingMessage = headingMessage;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }

    public String getFrequentflyername() {
        return frequentflyername;
    }

    public void setFrequentflyername(String frequentflyername) {
        this.frequentflyername = frequentflyername;
    }

    public String getFrequentFlyerNumber() {
        return frequentFlyerNumber;
    }

    public void setFrequentFlyerNumber(String frequentFlyerNumber) {
        this.frequentFlyerNumber = frequentFlyerNumber;
    }

    public int getCountrycode_position() {
        return countrycode_position;
    }

    public void setCountrycode_position(int countrycode_position) {
        this.countrycode_position = countrycode_position;
    }

    public String getTravellerEmail() {
        return travellerEmail;
    }

    public void setTravellerEmail(String travellerEmail) {
        this.travellerEmail = travellerEmail;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isRegUser() {
        return isRegUser;
    }

    public void setRegUser(boolean regUser) {
        isRegUser = regUser;
    }
}
