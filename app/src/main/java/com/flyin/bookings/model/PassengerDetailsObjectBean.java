package com.flyin.bookings.model;

import java.util.ArrayList;

public class PassengerDetailsObjectBean {
    private ArrayList<PassengersBean> passengersBeanArrayList;

    public ArrayList<PassengersBean> getPassengersBeanArrayList() {
        return passengersBeanArrayList;
    }

    public void setPassengersBeanArrayList(ArrayList<PassengersBean> passengersBeanArrayList) {
        this.passengersBeanArrayList = passengersBeanArrayList;
    }
}
