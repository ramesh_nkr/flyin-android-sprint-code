package com.flyin.bookings.model;

import java.io.Serializable;
import java.util.ArrayList;

public class OriginDestinationOptionBean implements Serializable {
    private String refNum;
    private String rph;
    private String tt;
    private String rs;
    private String cb;
    private String pt;
    private String headerMessage;
    private boolean isHeader;
    private boolean isSelected;
    private long flightDuration;
    private int stopsCount;
    private boolean isLayover;
    private String headingMessage;
    private ArrayList<FSDataBean> fsDataBeanArrayList;
    private Double refundType = 0.0;
    private String mOfcId = "";
    private boolean isOtherFlight = false;

    public Double getRefundType() {
        return refundType;
    }

    public void setRefundType(Double refundType) {
        this.refundType = refundType;
    }

    public int getSegmentDepartureTime() {
        return segmentDepartureTime;
    }

    public void setSegmentDepartureTime(int segmentDepartureTime) {
        this.segmentDepartureTime = segmentDepartureTime;
    }

    public String getHeadingMessage() {
        return headingMessage;
    }

    public void setHeadingMessage(String headingMessage) {
        this.headingMessage = headingMessage;
    }

    private int segmentDepartureTime;

    public boolean isLayover() {
        return isLayover;
    }

    public void setIsLayover(boolean isLayover) {
        this.isLayover = isLayover;
    }

    public int getStopsCount() {
        return stopsCount;
    }

    public void setStopsCount(int stopsCount) {
        this.stopsCount = stopsCount;
    }

    public String getHeaderMessage() {
        return headerMessage;
    }

    public void setHeaderMessage(String headerMessage) {
        this.headerMessage = headerMessage;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setIsHeader(boolean isHeader) {
        this.isHeader = isHeader;
    }

    public long getFlightDuration() {
        return flightDuration;
    }

    public void setFlightDuration(long flightDuration) {
        this.flightDuration = flightDuration;
    }

    public String getRefNum() {
        return refNum;
    }

    public void setRefNum(String refNum) {
        this.refNum = refNum;
    }

    public String getRph() {
        return rph;
    }

    public void setRph(String rph) {
        this.rph = rph;
    }

    public String getTt() {
        return tt;
    }

    public void setTt(String tt) {
        this.tt = tt;
    }

    public String getRs() {
        return rs;
    }

    public void setRs(String rs) {
        this.rs = rs;
    }

    public String getCb() {
        return cb;
    }

    public void setCb(String cb) {
        this.cb = cb;
    }

    public String getPt() {
        return pt;
    }

    public void setPt(String pt) {
        this.pt = pt;
    }

    public ArrayList<FSDataBean> getFsDataBeanArrayList() {
        return fsDataBeanArrayList;
    }

    public void setFsDataBeanArrayList(ArrayList<FSDataBean> fsDataBeanArrayList) {
        this.fsDataBeanArrayList = fsDataBeanArrayList;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getmOfcId() {
        return mOfcId;
    }

    public void setmOfcId(String mOfcId) {
        this.mOfcId = mOfcId;
    }

    public boolean isOtherFlight() {
        return isOtherFlight;
    }

    public void setOtherFlight(boolean otherFlight) {
        isOtherFlight = otherFlight;
    }
}
