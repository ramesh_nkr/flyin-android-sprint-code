package com.flyin.bookings.model;

public class EntityDetailsObjectBean {
    private FlightDetailsObjectBean flightDetailsBean;
    private String hotelDetails;
    private HotelDetailsObjectBean hotelDetailsBean;
    private String flightDetails;
    private HotelMoreInfoObject hotelMoreInfo;

    public FlightDetailsObjectBean getFlightDetailsBean() {
        return flightDetailsBean;
    }

    public void setFlightDetailsBean(FlightDetailsObjectBean flightDetailsBean) {
        this.flightDetailsBean = flightDetailsBean;
    }

    public String getHotelDetails() {
        return hotelDetails;
    }

    public void setHotelDetails(String hotelDetails) {
        this.hotelDetails = hotelDetails;
    }

    public HotelDetailsObjectBean getHotelDetailsBean() {
        return hotelDetailsBean;
    }

    public void setHotelDetailsBean(HotelDetailsObjectBean hotelDetailsBean) {
        this.hotelDetailsBean = hotelDetailsBean;
    }

    public String getFlightDetails() {
        return flightDetails;
    }

    public void setFlightDetails(String flightDetails) {
        this.flightDetails = flightDetails;
    }

    public HotelMoreInfoObject getHotelMoreInfo() {
        return hotelMoreInfo;
    }

    public void setHotelMoreInfo(HotelMoreInfoObject hotelMoreInfo) {
        this.hotelMoreInfo = hotelMoreInfo;
    }
}
