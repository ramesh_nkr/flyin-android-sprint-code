package com.flyin.bookings.model;

import java.util.ArrayList;

public class FlightDetailsObjectBean {
    private CancellationPolicyBean cancellationPolicyObjectBean;
    private ArrayList<FlightsDataBean> flightsDataBeanArrayList;
    private String gdsPnr;

    public CancellationPolicyBean getCancellationPolicyObjectBean() {
        return cancellationPolicyObjectBean;
    }

    public void setCancellationPolicyObjectBean(CancellationPolicyBean cancellationPolicyObjectBean) {
        this.cancellationPolicyObjectBean = cancellationPolicyObjectBean;
    }

    public ArrayList<FlightsDataBean> getFlightsDataBeanArrayList() {
        return flightsDataBeanArrayList;
    }

    public void setFlightsDataBeanArrayList(ArrayList<FlightsDataBean> flightsDataBeanArrayList) {
        this.flightsDataBeanArrayList = flightsDataBeanArrayList;
    }

    public String getGdsPnr() {
        return gdsPnr;
    }

    public void setGdsPnr(String gdsPnr) {
        this.gdsPnr = gdsPnr;
    }
}
