package com.flyin.bookings.model;

public class AirportsList {
    private String airportId;
    private String airportCode;
    private String allAirport;
    private String airportName;
    private String airportCity;
    private String airportCityName;
    private String countryName;
    private String countryLanguage;
    private Double latitude;
    private Double longitude;
    private String misspelledCityName;
    private Double distance;
    private String isoCountryCodeTwo;
    private String isoCountryCodeThree;

    public String getAllAirport() {
        return allAirport;
    }

    public void setAllAirport(String allAirport) {
        this.allAirport = allAirport;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getMisspelledCityName() {
        return misspelledCityName;
    }

    public void setMisspelledCityName(String misspelledCityName) {
        this.misspelledCityName = misspelledCityName;
    }

    public String getAirportId() {
        return airportId;
    }

    public void setAirportId(String airportId) {
        this.airportId = airportId;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getAirportCity() {
        return airportCity;
    }

    public void setAirportCity(String airportCity) {
        this.airportCity = airportCity;
    }

    public String getAirportCityName() {
        return airportCityName;
    }

    public void setAirportCityName(String airportCityName) {
        this.airportCityName = airportCityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryLanguage() {
        return countryLanguage;
    }

    public void setCountryLanguage(String countryLanguage) {
        this.countryLanguage = countryLanguage;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getIsoCountryCodeTwo() {
        return isoCountryCodeTwo;
    }

    public void setIsoCountryCodeTwo(String isoCountryCodeTwo) {
        this.isoCountryCodeTwo = isoCountryCodeTwo;
    }

    public String getIsoCountryCodeThree() {
        return isoCountryCodeThree;
    }

    public void setIsoCountryCodeThree(String isoCountryCodeThree) {
        this.isoCountryCodeThree = isoCountryCodeThree;
    }
}
