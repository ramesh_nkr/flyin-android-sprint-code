package com.flyin.bookings.model;

import java.util.ArrayList;

public class CancellationPolicyBean {
    private ArrayList<String> notesArray;

    public ArrayList<String> getNotesArray() {
        return notesArray;
    }

    public void setNotesArray(ArrayList<String> notesArray) {
        this.notesArray = notesArray;
    }
}
