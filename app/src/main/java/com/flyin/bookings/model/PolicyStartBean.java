package com.flyin.bookings.model;

public class PolicyStartBean {
    private String d;
    private String min;
    private String hou;

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getHou() {
        return hou;
    }

    public void setHou(String hou) {
        this.hou = hou;
    }
}
