package com.flyin.bookings.model;

import java.util.ArrayList;

public class PFBDObjectBean {
    private String pType;
    private String qty;
    private String usfcp;
    private ArrayList<String> fbcArray = null;
    private PassengerFareObjectBean passengerFareObjectBean;
    private String refNumber;

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getUsfcp() {
        return usfcp;
    }

    public void setUsfcp(String usfcp) {
        this.usfcp = usfcp;
    }

    public ArrayList<String> getFbcArray() {
        return fbcArray;
    }

    public void setFbcArray(ArrayList<String> fbcArray) {
        this.fbcArray = fbcArray;
    }

    public PassengerFareObjectBean getPassengerFareObjectBean() {
        return passengerFareObjectBean;
    }

    public void setPassengerFareObjectBean(PassengerFareObjectBean passengerFareObjectBean) {
        this.passengerFareObjectBean = passengerFareObjectBean;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }
}
