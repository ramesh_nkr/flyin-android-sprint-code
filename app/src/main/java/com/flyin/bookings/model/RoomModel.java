package com.flyin.bookings.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RoomModel implements Parcelable {

    public static final Creator<RoomModel> CREATOR = new Creator<RoomModel>() {

        @Override
        public RoomModel createFromParcel(Parcel source) {
            return new RoomModel(source);
        }

        @Override
        public RoomModel[] newArray(int size) {
            return new RoomModel[size];
        }
    };
    String room_type;
    String brakfast;
    String free_cancel_date = "";
    String price;
    String currency;
    String night_count;
    String room_count;
    String wdp;
    String isRcObject;
    int na;
    int nc;
    String hotelrequestJson;
    String room_json;
    private double hotelRoomSortPrice = 0.0;
    private String hotelJson;

    public double getTempPrice() {
        return tempPrice;
    }

    public void setTempPrice(double tempPrice) {
        this.tempPrice = tempPrice;
    }

    double tempPrice;

    private RoomModel(Parcel in) {
        this.room_type = in.readString();
        this.brakfast = in.readString();
        this.free_cancel_date = in.readString();
        this.price = in.readString();
        this.currency = in.readString();
        this.night_count = in.readString();
        this.room_count = in.readString();
        this.wdp = in.readString();
        this.room_json = in.readString();
        this.isRcObject = in.readString();
        this.hotelrequestJson = in.readString();
        this.na = in.readInt();
        this.hotelJson = in.readString();
    }

    public RoomModel() {

    }

    public int getNa() {
        return na;
    }

    public void setNa(int na) {
        this.na = na;
    }

    public int getNc() {
        return nc;
    }

    public void setNc(int nc) {
        this.nc = nc;
    }

    public String getHotelrequestJson() {
        return hotelrequestJson;
    }

    public void setHotelrequestJson(String hotelrequestJson) {
        this.hotelrequestJson = hotelrequestJson;
    }

    public String getRoom_json() {
        return room_json;
    }

    public void setRoom_json(String room_json) {
        this.room_json = room_json;
    }

    public String getIsRcObject() {
        return isRcObject;
    }

    public void setRcObject(String rcObject) {
        isRcObject = rcObject;
    }

    public String getWdp() {
        return wdp;
    }

    public void setWdp(String wdp) {
        this.wdp = wdp;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public String getBrakfast() {
        return brakfast;
    }

    public void setBrakfast(String brakfast) {
        this.brakfast = brakfast;
    }

    public String getFree_cancel_date() {
        return free_cancel_date;
    }

    public void setFree_cancel_date(String free_cancel_date) {
        this.free_cancel_date = free_cancel_date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getNight_count() {
        return night_count;
    }

    public void setNight_count(String night_count) {
        this.night_count = night_count;
    }

    public String getRoom_count() {
        return room_count;
    }

    public void setRoom_count(String room_count) {
        this.room_count = room_count;
    }

    public double getHotelRoomSortPrice() {
        return hotelRoomSortPrice;
    }

    public void setHotelRoomSortPrice(double hotelRoomSortPrice) {
        this.hotelRoomSortPrice = hotelRoomSortPrice;
    }

    public String getHotelJson() {
        return hotelJson;
    }

    public void setHotelJson(String hotelJson) {
        this.hotelJson = hotelJson;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(room_type);
        dest.writeString(brakfast);
        dest.writeString(free_cancel_date);
        dest.writeString(price);
        dest.writeString(currency);
        dest.writeString(night_count);
        dest.writeString(room_count);
        dest.writeString(wdp);
        dest.writeString(isRcObject);
        dest.writeString(room_json);
        dest.writeString(hotelrequestJson);
        dest.writeInt(na);
        dest.writeString(hotelJson);
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        /*RoomModel model = (RoomModel) o;
        return price.equals(model.getPrice());*/
        if (obj instanceof RoomModel) {
            RoomModel pp = (RoomModel) obj;
            return (pp.getBrakfast().equalsIgnoreCase(this.brakfast) && pp.getPrice().equalsIgnoreCase(this.price));
        } else {
            return false;
        }
      /*  if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        RoomModel guest = (RoomModel) obj;
        //return (price == guest.price || (price != null && price.equals(guest.getPrice()))) && (brakfast == guest.brakfast || (brakfast != null && brakfast.equals(guest.getBrakfast())));

        if (this.price.equalsIgnoreCase(guest.getPrice()))
            return true;
        else
            return false;*/
    }

    @Override
    public int hashCode() {
        // return price.hashCode();
      /*  final int prime = 31;
        int result = 1;
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((brakfast == null) ? 0 : brakfast.hashCode());
        return price.hashCode();*/
        int hashcode = 0;
        hashcode = (int) Double.parseDouble(price) * 20;
        hashcode += brakfast.hashCode();
        return hashcode;
    }
}
