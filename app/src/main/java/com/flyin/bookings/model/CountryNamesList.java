package com.flyin.bookings.model;

public class CountryNamesList {
    private String countryId;
    private String countryName;
    private String countryNameInArab;
    private String countryShortCode;
    private String countryCode;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryNameInArab() {
        return countryNameInArab;
    }

    public void setCountryNameInArab(String countryNameInArab) {
        this.countryNameInArab = countryNameInArab;
    }

    public String getCountryShortCode() {
        return countryShortCode;
    }

    public void setCountryShortCode(String countryShortCode) {
        this.countryShortCode = countryShortCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
