package com.flyin.bookings.model;

public class CancellationPolicyInfoBean {
    private ChargesObjectBean chargesObjectBean;
    private PolicyStartBean policyStartBean;
    private PolicyEndBean policyEndBean;

    public ChargesObjectBean getChargesObjectBean() {
        return chargesObjectBean;
    }

    public void setChargesObjectBean(ChargesObjectBean chargesObjectBean) {
        this.chargesObjectBean = chargesObjectBean;
    }

    public PolicyStartBean getPolicyStartBean() {
        return policyStartBean;
    }

    public void setPolicyStartBean(PolicyStartBean policyStartBean) {
        this.policyStartBean = policyStartBean;
    }

    public PolicyEndBean getPolicyEndBean() {
        return policyEndBean;
    }

    public void setPolicyEndBean(PolicyEndBean policyEndBean) {
        this.policyEndBean = policyEndBean;
    }
}
