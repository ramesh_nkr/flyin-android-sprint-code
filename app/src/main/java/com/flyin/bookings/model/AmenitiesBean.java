package com.flyin.bookings.model;

public class AmenitiesBean {
    private String en;
    private String ar;
    private String icon;

    public AmenitiesBean(String en, String ar, String icon) {
        this.en = en;
        this.ar = ar;
        this.icon = icon;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getAr() {
        return ar;
    }

    public void setAr(String ar) {
        this.ar = ar;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
