package com.flyin.bookings.model;

import java.io.Serializable;
import java.util.ArrayList;

public class AirItineraryPricingInfoBean implements Serializable {
    private ItinTotalFareObjectBean itfObject;
    private ArrayList<PFBDObjectBean> pfbdObjectBeanArrayList;
    private String fq;

    public ItinTotalFareObjectBean getItfObject() {
        return itfObject;
    }

    public void setItfObject(ItinTotalFareObjectBean itfObject) {
        this.itfObject = itfObject;
    }

    public ArrayList<PFBDObjectBean> getPfbdObjectBeanArrayList() {
        return pfbdObjectBeanArrayList;
    }

    public void setPfbdObjectBeanArrayList(ArrayList<PFBDObjectBean> pfbdObjectBeanArrayList) {
        this.pfbdObjectBeanArrayList = pfbdObjectBeanArrayList;
    }

    public String getFq() {
        return fq;
    }

    public void setFq(String fq) {
        this.fq = fq;
    }
}
