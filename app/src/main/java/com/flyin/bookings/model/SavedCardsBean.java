package com.flyin.bookings.model;

public class SavedCardsBean {
    private String cardHolderName;
    private String cardNumber;
    private String ExpiryDate;
    private String cardNumberFirst;
    private String cardNumberEnd;
    private String cardType;
    private String cardUID;

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }

    public String getCardNumberFirst() {
        return cardNumberFirst;
    }

    public void setCardNumberFirst(String cardNumberFirst) {
        this.cardNumberFirst = cardNumberFirst;
    }

    public String getCardNumberEnd() {
        return cardNumberEnd;
    }

    public void setCardNumberEnd(String cardNumberEnd) {
        this.cardNumberEnd = cardNumberEnd;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardUID() {
        return cardUID;
    }

    public void setCardUID(String cardUID) {
        this.cardUID = cardUID;
    }
}
