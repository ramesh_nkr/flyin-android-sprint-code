package com.flyin.bookings.model;

import java.util.ArrayList;

public class RoomsInformationBean {
    private String id;
    private String crsc;
    private String ac;
    private String fc;
    private RoomPriceObjectBean roomPriceObjectBean;
    private RoomWODiscountPriceBean roomWODiscountPriceBean;
    private RoomNetPriceBean roomNetPriceBean;
    private String rcd;
    private String rn;
    private String ri;
    private String mcd;
    private String mn;
    private RoomOccupancyObject roomOccupancyObject;
    private RoomProductInformationObject roomProductInformationObject;
    private CancellationInfoBean cancellationInfoBean;
    private ArrayList<SupplierObjectBean> supplierObjectBeenArray;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCrsc() {
        return crsc;
    }

    public void setCrsc(String crsc) {
        this.crsc = crsc;
    }

    public String getAc() {
        return ac;
    }

    public void setAc(String ac) {
        this.ac = ac;
    }

    public String getFc() {
        return fc;
    }

    public void setFc(String fc) {
        this.fc = fc;
    }

    public RoomPriceObjectBean getRoomPriceObjectBean() {
        return roomPriceObjectBean;
    }

    public void setRoomPriceObjectBean(RoomPriceObjectBean roomPriceObjectBean) {
        this.roomPriceObjectBean = roomPriceObjectBean;
    }

    public RoomWODiscountPriceBean getRoomWODiscountPriceBean() {
        return roomWODiscountPriceBean;
    }

    public void setRoomWODiscountPriceBean(RoomWODiscountPriceBean roomWODiscountPriceBean) {
        this.roomWODiscountPriceBean = roomWODiscountPriceBean;
    }

    public RoomNetPriceBean getRoomNetPriceBean() {
        return roomNetPriceBean;
    }

    public void setRoomNetPriceBean(RoomNetPriceBean roomNetPriceBean) {
        this.roomNetPriceBean = roomNetPriceBean;
    }

    public String getRcd() {
        return rcd;
    }

    public void setRcd(String rcd) {
        this.rcd = rcd;
    }

    public String getRn() {
        return rn;
    }

    public void setRn(String rn) {
        this.rn = rn;
    }

    public String getRi() {
        return ri;
    }

    public void setRi(String ri) {
        this.ri = ri;
    }

    public String getMcd() {
        return mcd;
    }

    public void setMcd(String mcd) {
        this.mcd = mcd;
    }

    public String getMn() {
        return mn;
    }

    public void setMn(String mn) {
        this.mn = mn;
    }

    public RoomOccupancyObject getRoomOccupancyObject() {
        return roomOccupancyObject;
    }

    public void setRoomOccupancyObject(RoomOccupancyObject roomOccupancyObject) {
        this.roomOccupancyObject = roomOccupancyObject;
    }

    public RoomProductInformationObject getRoomProductInformationObject() {
        return roomProductInformationObject;
    }

    public void setRoomProductInformationObject(RoomProductInformationObject roomProductInformationObject) {
        this.roomProductInformationObject = roomProductInformationObject;
    }

    public CancellationInfoBean getCancellationInfoBean() {
        return cancellationInfoBean;
    }

    public void setCancellationInfoBean(CancellationInfoBean cancellationInfoBean) {
        this.cancellationInfoBean = cancellationInfoBean;
    }

    public ArrayList<SupplierObjectBean> getSupplierObjectBeenArray() {
        return supplierObjectBeenArray;
    }

    public void setSupplierObjectBeenArray(ArrayList<SupplierObjectBean> supplierObjectBeenArray) {
        this.supplierObjectBeenArray = supplierObjectBeenArray;
    }
}
