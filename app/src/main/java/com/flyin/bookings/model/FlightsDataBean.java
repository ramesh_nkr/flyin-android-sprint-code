package com.flyin.bookings.model;

public class FlightsDataBean {
    private String departureAirport;
    private String arrivalAirport;
    private String operatingAirline;
    private String marketingAirline;
    private String duration;
    private String departureDate;
    private String arrivalDate;
    private String equipmentType;
    private String classType;
    private String terminalinfo;
    private String baggageInfo;
    private String airlinePNR;
    private String journeyType;
    private String journeyOrder;
    private String legOrder;
    private String flightNumber;
    private boolean isLayover;
    private int stopsCount;
    private int roundStopCount;
    private String arrivalAirportName;
    private String departureAirportName;
    private String headingMessage;
    private String headerName;
    private String refundStatusType = "";

    public String getHeadingMessage() {
        return headingMessage;
    }

    public void setHeadingMessage(String headingMessage) {
        this.headingMessage = headingMessage;
    }

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public String getArrivalAirportName() {
        return arrivalAirportName;
    }

    public void setArrivalAirportName(String arrivalAirportName) {
        this.arrivalAirportName = arrivalAirportName;
    }

    public String getDepartureAirportName() {
        return departureAirportName;
    }

    public void setDepartureAirportName(String departureAirportName) {
        this.departureAirportName = departureAirportName;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public String getOperatingAirline() {
        return operatingAirline;
    }

    public void setOperatingAirline(String operatingAirline) {
        this.operatingAirline = operatingAirline;
    }

    public String getMarketingAirline() {
        return marketingAirline;
    }

    public void setMarketingAirline(String marketingAirline) {
        this.marketingAirline = marketingAirline;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public String getTerminalinfo() {
        return terminalinfo;
    }

    public void setTerminalinfo(String terminalinfo) {
        this.terminalinfo = terminalinfo;
    }

    public String getBaggageInfo() {
        return baggageInfo;
    }

    public void setBaggageInfo(String baggageInfo) {
        this.baggageInfo = baggageInfo;
    }

    public String getAirlinePNR() {
        return airlinePNR;
    }

    public void setAirlinePNR(String airlinePNR) {
        this.airlinePNR = airlinePNR;
    }

    public String getJourneyType() {
        return journeyType;
    }

    public void setJourneyType(String journeyType) {
        this.journeyType = journeyType;
    }

    public String getJourneyOrder() {
        return journeyOrder;
    }

    public void setJourneyOrder(String journeyOrder) {
        this.journeyOrder = journeyOrder;
    }

    public String getLegOrder() {
        return legOrder;
    }

    public void setLegOrder(String legOrder) {
        this.legOrder = legOrder;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public boolean isLayover() {
        return isLayover;
    }

    public void setIsLayover(boolean isLayover) {
        this.isLayover = isLayover;
    }

    public int getStopsCount() {
        return stopsCount;
    }

    public void setStopsCount(int stopsCount) {
        this.stopsCount = stopsCount;
    }

    public int getRoundStopCount() {
        return roundStopCount;
    }

    public void setRoundStopCount(int roundStopCount) {
        this.roundStopCount = roundStopCount;
    }

    public String getRefundStatusType() {
        return refundStatusType;
    }

    public void setRefundStatusType(String refundStatusType) {
        this.refundStatusType = refundStatusType;
    }
}
