package com.flyin.bookings.model;

public class CurrencyBean {
    String currency;
    String exchangeRate;
    String bufferRate;

    public String getBufferRate() {
        return bufferRate;
    }

    public void setBufferRate(String bufferRate) {
        this.bufferRate = bufferRate;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        CurrencyBean model = (CurrencyBean) o;
        return currency.equals(model.getCurrency());
    }

    @Override
    public int hashCode() {
        // super.hashCode();
        return currency.hashCode();
    }
}
