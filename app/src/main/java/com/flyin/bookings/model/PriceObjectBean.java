package com.flyin.bookings.model;

public class PriceObjectBean {
    private String val;
    private String cur;

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getCur() {
        return cur;
    }

    public void setCur(String cur) {
        this.cur = cur;
    }
}
