package com.flyin.bookings.model;

import java.util.ArrayList;

public class FhRoomPassengerDetailsBean {
    private int adultCnt;
    private int childCnt;
    private int infantCnt;
    private ArrayList<Integer> childAgeArray = new ArrayList<>(5);
    private boolean isExpanded;
    private String classType;
    private int age;
    private int childAge;
    private boolean isChild;

    public int getAdultCnt() {
        return adultCnt;
    }

    public void setAdultCnt(int adultCnt) {
        this.adultCnt = adultCnt;
    }

    public int getChildCnt() {
        return childCnt;
    }

    public void setChildCnt(int childCnt) {
        this.childCnt = childCnt;
    }

    public int getInfantCnt() {
        return infantCnt;
    }

    public void setInfantCnt(int infantCnt) {
        this.infantCnt = infantCnt;
    }

    public ArrayList<Integer> getChildAgeArray() {
        return childAgeArray;
    }

    public void setChildAgeArray(ArrayList<Integer> childAgeArray) {
        this.childAgeArray = childAgeArray;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getChildAge() {
        return childAge;
    }

    public void setChildAge(int childAge) {
        this.childAge = childAge;
    }

    public boolean isChild() {
        return isChild;
    }

    public void setIsChild(boolean isChild) {
        this.isChild = isChild;
    }
}
