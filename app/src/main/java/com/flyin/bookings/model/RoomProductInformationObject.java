package com.flyin.bookings.model;

public class RoomProductInformationObject {
    private String SearchId;
    private String ProductKey;
    private String ProcessId;
    private String UniqueKey;
    private String Source;
    private String FreeCancellationDate;

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getBedding() {
        return bedding;
    }

    public void setBedding(String bedding) {
        this.bedding = bedding;
    }

    private String ProductName;
    private String bedding;

    public String getSearchId() {
        return SearchId;
    }

    public void setSearchId(String searchId) {
        SearchId = searchId;
    }

    public String getProductKey() {
        return ProductKey;
    }

    public void setProductKey(String productKey) {
        ProductKey = productKey;
    }

    public String getProcessId() {
        return ProcessId;
    }

    public void setProcessId(String processId) {
        ProcessId = processId;
    }

    public String getUniqueKey() {
        return UniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        UniqueKey = uniqueKey;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getFreeCancellationDate() {
        return FreeCancellationDate;
    }

    public void setFreeCancellationDate(String freeCancellationDate) {
        FreeCancellationDate = freeCancellationDate;
    }
}
