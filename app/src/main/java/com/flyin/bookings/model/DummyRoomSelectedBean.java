package com.flyin.bookings.model;

import java.util.ArrayList;

public class DummyRoomSelectedBean {
    private double price;
    private String name;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<RoomModel> getRcmodel() {
        return rcmodel;
    }

    public void setRcmodel(ArrayList<RoomModel> rcmodel) {
        this.rcmodel = rcmodel;
    }

    private ArrayList<RoomModel> rcmodel;
}
