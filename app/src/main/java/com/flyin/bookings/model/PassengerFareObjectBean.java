package com.flyin.bookings.model;

import java.util.ArrayList;

public class PassengerFareObjectBean {
    private String pbf;
    private ArrayList<TaxObjectBean> taxObjectBeanArrayList;
    private String ptFare;

    public String getPbf() {
        return pbf;
    }

    public void setPbf(String pbf) {
        this.pbf = pbf;
    }

    public ArrayList<TaxObjectBean> getTaxObjectBeanArrayList() {
        return taxObjectBeanArrayList;
    }

    public void setTaxObjectBeanArrayList(ArrayList<TaxObjectBean> taxObjectBeanArrayList) {
        this.taxObjectBeanArrayList = taxObjectBeanArrayList;
    }

    public String getPtFare() {
        return ptFare;
    }

    public void setPtFare(String ptFare) {
        this.ptFare = ptFare;
    }
}
