package com.flyin.bookings.model;

import java.io.Serializable;

public class TripAdvisorBean implements Serializable {
    String huid;
    String writeReview, numReview, ratingImageUrl;

    public String getHuid() {
        return huid;
    }

    public void setHuid(String huid) {
        this.huid = huid;
    }

    public String getWriteReview() {
        return writeReview;
    }

    public void setWriteReview(String writeReview) {
        this.writeReview = writeReview;
    }

    public String getNumReview() {
        return numReview;
    }

    public void setNumReview(String numReview) {
        this.numReview = numReview;
    }

    public String getRatingImageUrl() {
        return ratingImageUrl;
    }

    public void setRatingImageUrl(String ratingImageUrl) {
        this.ratingImageUrl = ratingImageUrl;
    }
}
