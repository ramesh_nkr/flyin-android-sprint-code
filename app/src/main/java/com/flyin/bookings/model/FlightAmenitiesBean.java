package com.flyin.bookings.model;

public class FlightAmenitiesBean {
    private String classType;
    private String seatPitch;
    private String avod;
    private String seatWidth;
    private String videoType;
    private String seatLength;
    private String laptopPower;
    private String seatType;
    private String powerType;
    private String noOfSeats;
    private String meals;
    private String wifi;
    private String infantCare;
    private String alocohol;
    private String avodScreenSize;
    private String remarks;
    private String provideTransportation;
    private String seatLayout;
    private String prayerArea;
    private String provideOtherInfo;
    private String aircraftImage;
    private String classOverviewImage;
    private String seatImage;
    private String facilityImage;
    private String mobile;
    private String showerArea;
    private String bodyType;

    private String dap;

    public String getAap() {
        return aap;
    }

    public void setAap(String aap) {
        this.aap = aap;
    }

    public String getDap() {
        return dap;
    }

    public void setDap(String dap) {
        this.dap = dap;
    }

    private String aap;

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    private String flightName;

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public String getSeatPitch() {
        return seatPitch;
    }

    public void setSeatPitch(String seatPitch) {
        this.seatPitch = seatPitch;
    }

    public String getAvod() {
        return avod;
    }

    public void setAvod(String avod) {
        this.avod = avod;
    }

    public String getSeatWidth() {
        return seatWidth;
    }

    public void setSeatWidth(String seatWidth) {
        this.seatWidth = seatWidth;
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public String getSeatLength() {
        return seatLength;
    }

    public void setSeatLength(String seatLength) {
        this.seatLength = seatLength;
    }

    public String getLaptopPower() {
        return laptopPower;
    }

    public void setLaptopPower(String laptopPower) {
        this.laptopPower = laptopPower;
    }

    public String getSeatType() {
        return seatType;
    }

    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }

    public String getPowerType() {
        return powerType;
    }

    public void setPowerType(String powerType) {
        this.powerType = powerType;
    }

    public String getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(String noOfSeats) {
        this.noOfSeats = noOfSeats;
    }

    public String getMeals() {
        return meals;
    }

    public void setMeals(String meals) {
        this.meals = meals;
    }

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi = wifi;
    }

    public String getInfantCare() {
        return infantCare;
    }

    public void setInfantCare(String infantCare) {
        this.infantCare = infantCare;
    }

    public String getAlocohol() {
        return alocohol;
    }

    public void setAlocohol(String alocohol) {
        this.alocohol = alocohol;
    }

    public String getAvodScreenSize() {
        return avodScreenSize;
    }

    public void setAvodScreenSize(String avodScreenSize) {
        this.avodScreenSize = avodScreenSize;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getProvideTransportation() {
        return provideTransportation;
    }

    public void setProvideTransportation(String provideTransportation) {
        this.provideTransportation = provideTransportation;
    }

    public String getSeatLayout() {
        return seatLayout;
    }

    public void setSeatLayout(String seatLayout) {
        this.seatLayout = seatLayout;
    }

    public String getPrayerArea() {
        return prayerArea;
    }

    public void setPrayerArea(String prayerArea) {
        this.prayerArea = prayerArea;
    }

    public String getProvideOtherInfo() {
        return provideOtherInfo;
    }

    public void setProvideOtherInfo(String provideOtherInfo) {
        this.provideOtherInfo = provideOtherInfo;
    }

    public String getAircraftImage() {
        return aircraftImage;
    }

    public void setAircraftImage(String aircraftImage) {
        this.aircraftImage = aircraftImage;
    }

    public String getClassOverviewImage() {
        return classOverviewImage;
    }

    public void setClassOverviewImage(String classOverviewImage) {
        this.classOverviewImage = classOverviewImage;
    }

    public String getSeatImage() {
        return seatImage;
    }

    public void setSeatImage(String seatImage) {
        this.seatImage = seatImage;
    }

    public String getFacilityImage() {
        return facilityImage;
    }

    public void setFacilityImage(String facilityImage) {
        this.facilityImage = facilityImage;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getShowerArea() {
        return showerArea;
    }

    public void setShowerArea(String showerArea) {
        this.showerArea = showerArea;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }
}
