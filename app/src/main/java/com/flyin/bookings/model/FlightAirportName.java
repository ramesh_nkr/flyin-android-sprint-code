package com.flyin.bookings.model;

import java.io.Serializable;

public class FlightAirportName implements Serializable {
    private String cityCode = "", cityName = "", airpotName = "";
    private String airlineName = "", airLineCode = "";

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getAirLineCode() {
        return airLineCode;
    }

    public void setAirLineCode(String airLineCode) {
        this.airLineCode = airLineCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAirpotName() {
        return airpotName;
    }

    public void setAirpotName(String airpotName) {
        this.airpotName = airpotName;
    }
}
