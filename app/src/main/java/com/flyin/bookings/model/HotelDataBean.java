package com.flyin.bookings.model;

public class HotelDataBean {
    private String referenceNo = "";
    private String name = "";
    private String roomType = "";
    private String boardBasis = "";
    private String reservationType = "";
    private String address = "";
    private String city = "";
    private String phoneNo = "";
    private String email = "";
    private String checkInDate = "";
    private String checkOutDate = "";
    private String nights = "";
    private String rooms = "";
    private String hotelReservationNumber = "";
    private String hotelUniueId = "";
    private String freeCancellationDate;

    public String getFreeCancellationDate() {
        return freeCancellationDate;
    }

    public void setFreeCancellationDate(String freeCancellationDate) {
        this.freeCancellationDate = freeCancellationDate;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getBoardBasis() {
        return boardBasis;
    }

    public void setBoardBasis(String boardBasis) {
        this.boardBasis = boardBasis;
    }

    public String getReservationType() {
        return reservationType;
    }

    public void setReservationType(String reservationType) {
        this.reservationType = reservationType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public String getNights() {
        return nights;
    }

    public void setNights(String nights) {
        this.nights = nights;
    }

    public String getRooms() {
        return rooms;
    }

    public void setRooms(String rooms) {
        this.rooms = rooms;
    }

    public String getHotelReservationNumber() {
        return hotelReservationNumber;
    }

    public void setHotelReservationNumber(String hotelReservationNumber) {
        this.hotelReservationNumber = hotelReservationNumber;
    }

    public String getHotelUniueId() {
        return hotelUniueId;
    }

    public void setHotelUniueId(String hotelUniueId) {
        this.hotelUniueId = hotelUniueId;
    }
}
