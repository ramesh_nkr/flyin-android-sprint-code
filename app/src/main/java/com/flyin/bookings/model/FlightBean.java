package com.flyin.bookings.model;

public class FlightBean {
    private String image, price;
    private boolean isClicked;
    private boolean isDataCleared;

    public boolean isClicked() {
        return isClicked;
    }

    public void setClicked(boolean clicked) {
        isClicked = clicked;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isDataCleared() {
        return isDataCleared;
    }

    public void setDataCleared(boolean dataCleared) {
        isDataCleared = dataCleared;
    }
}
