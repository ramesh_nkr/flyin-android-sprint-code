package com.flyin.bookings.model;

public class DataArrayBean {
    private String imageurl;
    private String description_en;
    private String description_ar;
    private String title_en;
    private String title_ar;
    private String title;
    private String frm_airport_code;
    private String to_airport_code;
    private String departure_date;
    private String arrival_date;
    private String pax;

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getDescription_en() {
        return description_en;
    }

    public void setDescription_en(String description_en) {
        this.description_en = description_en;
    }

    public String getDescription_ar() {
        return description_ar;
    }

    public void setDescription_ar(String description_ar) {
        this.description_ar = description_ar;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getTitle_ar() {
        return title_ar;
    }

    public void setTitle_ar(String title_ar) {
        this.title_ar = title_ar;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFrm_airport_code() {
        return frm_airport_code;
    }

    public void setFrm_airport_code(String frm_airport_code) {
        this.frm_airport_code = frm_airport_code;
    }

    public String getTo_airport_code() {
        return to_airport_code;
    }

    public void setTo_airport_code(String to_airport_code) {
        this.to_airport_code = to_airport_code;
    }

    public String getDeparture_date() {
        return departure_date;
    }

    public void setDeparture_date(String departure_date) {
        this.departure_date = departure_date;
    }

    public String getArrival_date() {
        return arrival_date;
    }

    public void setArrival_date(String arrival_date) {
        this.arrival_date = arrival_date;
    }

    public String getPax() {
        return pax;
    }

    public void setPax(String pax) {
        this.pax = pax;
    }
}
