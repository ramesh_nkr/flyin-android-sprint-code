package com.flyin.bookings.model;

public class PassengersBean {
    private PersonalObjectBean personalDetailsObjectBean;
    private String mealPreference;
    private String seatPreference;
    private String specialPreference;
    private boolean isSecondPassenger;

    public PersonalObjectBean getPersonalDetailsObjectBean() {
        return personalDetailsObjectBean;
    }

    public void setPersonalDetailsObjectBean(PersonalObjectBean personalDetailsObjectBean) {
        this.personalDetailsObjectBean = personalDetailsObjectBean;
    }

    public String getMealPreference() {
        return mealPreference;
    }

    public void setMealPreference(String mealPreference) {
        this.mealPreference = mealPreference;
    }

    public String getSeatPreference() {
        return seatPreference;
    }

    public void setSeatPreference(String seatPreference) {
        this.seatPreference = seatPreference;
    }

    public String getSpecialPreference() {
        return specialPreference;
    }

    public void setSpecialPreference(String specialPreference) {
        this.specialPreference = specialPreference;
    }

    public boolean isSecondPassenger() {
        return isSecondPassenger;
    }

    public void setIsSecondPassenger(boolean isSecondPassenger) {
        this.isSecondPassenger = isSecondPassenger;
    }
}
