package com.flyin.bookings.model;

public class HotelDetailsObjectBean {
    private HotelDataBean hotelDataBean;
    private CancellationPolicyBean cancellationPolicyObjectBean;

    public HotelDataBean getHotelDataBean() {
        return hotelDataBean;
    }

    public void setHotelDataBean(HotelDataBean hotelDataBean) {
        this.hotelDataBean = hotelDataBean;
    }

    public CancellationPolicyBean getCancellationPolicyObjectBean() {
        return cancellationPolicyObjectBean;
    }

    public void setCancellationPolicyObjectBean(CancellationPolicyBean cancellationPolicyObjectBean) {
        this.cancellationPolicyObjectBean = cancellationPolicyObjectBean;
    }
}
