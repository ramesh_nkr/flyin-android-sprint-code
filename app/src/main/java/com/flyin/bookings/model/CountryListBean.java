package com.flyin.bookings.model;

public class CountryListBean {
    private String countryNameArabic = "";
    private String countryName = "";
    private String isoCountryCode = "";
    private String isoCountryCode3L = "";

    public String getCountryNameArabic() {
        return countryNameArabic;
    }

    public void setCountryNameArabic(String countryNameArabic) {
        this.countryNameArabic = countryNameArabic;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getIsoCountryCode() {
        return isoCountryCode;
    }

    public void setIsoCountryCode(String isoCountryCode) {
        this.isoCountryCode = isoCountryCode;
    }

    public String getIsoCountryCode3L() {
        return isoCountryCode3L;
    }

    public void setIsoCountryCode3L(String isoCountryCode3L) {
        this.isoCountryCode3L = isoCountryCode3L;
    }
}
