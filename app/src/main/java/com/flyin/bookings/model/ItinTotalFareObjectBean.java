package com.flyin.bookings.model;

public class ItinTotalFareObjectBean {
    private String bf;
    private String tTx;
    private String tSF;
    private String disc;
    private String tFare;
    private String ic;
    private String usfc;
    private String priceDiff;

    public String getBf() {
        return bf;
    }

    public void setBf(String bf) {
        this.bf = bf;
    }

    public String gettTx() {
        return tTx;
    }

    public void settTx(String tTx) {
        this.tTx = tTx;
    }

    public String gettSF() {
        return tSF;
    }

    public void settSF(String tSF) {
        this.tSF = tSF;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public String gettFare() {
        return tFare;
    }

    public void settFare(String tFare) {
        this.tFare = tFare;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getUsfc() {
        return usfc;
    }

    public void setUsfc(String usfc) {
        this.usfc = usfc;
    }

    public String getPriceDiff() {
        return priceDiff;
    }

    public void setPriceDiff(String priceDiff) {
        this.priceDiff = priceDiff;
    }
}
