package com.flyin.bookings.model;

public class LoyaltyBookingInfoBean {
    private LoyaltyBookingDataBean loyaltyBookingDataBean = null;
    private String product = "";
    private String date = "";
    private String transactionType = "";
    private int points = 0;
    private String bookingCode = "";
    private String productCode = "0";
    private HotelMoreInfoObject hotelMoreInfoObject = null;
    private boolean isExpanded;
    private long departureDate;
    private String description = "";

    public LoyaltyBookingDataBean getLoyaltyBookingDataBean() {
        return loyaltyBookingDataBean;
    }

    public void setLoyaltyBookingDataBean(LoyaltyBookingDataBean loyaltyBookingDataBean) {
        this.loyaltyBookingDataBean = loyaltyBookingDataBean;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public HotelMoreInfoObject getHotelMoreInfoObject() {
        return hotelMoreInfoObject;
    }

    public void setHotelMoreInfoObject(HotelMoreInfoObject hotelMoreInfoObject) {
        this.hotelMoreInfoObject = hotelMoreInfoObject;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public long getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(long departureDate) {
        this.departureDate = departureDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
