package com.flyin.bookings.model;

public class AirportCityNameList {
    private String zPk;
    private String zEnt;
    private String zOpt;
    private String airportCode;
    private String airportName;
    private String airportCity;
    private String countryName;
    private String language;

    public String getzPk() {
        return zPk;
    }

    public void setzPk(String zPk) {
        this.zPk = zPk;
    }

    public String getzEnt() {
        return zEnt;
    }

    public void setzEnt(String zEnt) {
        this.zEnt = zEnt;
    }

    public String getzOpt() {
        return zOpt;
    }

    public void setzOpt(String zOpt) {
        this.zOpt = zOpt;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getAirportCity() {
        return airportCity;
    }

    public void setAirportCity(String airportCity) {
        this.airportCity = airportCity;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
