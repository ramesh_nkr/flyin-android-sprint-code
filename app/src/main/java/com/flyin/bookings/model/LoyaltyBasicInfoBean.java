package com.flyin.bookings.model;

public class LoyaltyBasicInfoBean {
    private String userid = "";
    private int availablePoints = 0;
    private int pendingPoints = 0;
    private int totalPoints = 0;
    private String tier = "";
    private String memberSince = "";

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getAvailablePoints() {
        return availablePoints;
    }

    public void setAvailablePoints(int availablePoints) {
        this.availablePoints = availablePoints;
    }

    public int getPendingPoints() {
        return pendingPoints;
    }

    public void setPendingPoints(int pendingPoints) {
        this.pendingPoints = pendingPoints;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(String memberSince) {
        this.memberSince = memberSince;
    }
}
