package com.flyin.bookings.model;

public class TermsDetailsBean {
    private String text;
    private boolean isExpanded;
    private String termsHeaderName;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setIsExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

    public String getTermsHeaderName() {
        return termsHeaderName;
    }

    public void setTermsHeaderName(String termsHeaderName) {
        this.termsHeaderName = termsHeaderName;
    }
}
