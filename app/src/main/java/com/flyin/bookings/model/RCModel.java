/*
package com.flyin.bookings.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.util.ArrayList;

*/
/**
 * Created by Vivaan Sharma on 01-06-2016.
 *//*

public class RCModel implements Parcelable{
    String groupName="";
    String rcJsonObject;
    ArrayList<RoomModel> roomModels;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getRcJsonObject() {
        return rcJsonObject;
    }

    public void setRcJsonObject(String rcJsonObject) {
        this.rcJsonObject = rcJsonObject;
    }

    public ArrayList<RoomModel> getRoomModels() {
        return roomModels;
    }

    public void setRoomModels(ArrayList<RoomModel> roomModels) {
        this.roomModels = roomModels;
    }

    public static Creator<RCModel> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.groupName);
        dest.writeString(this.rcJsonObject);
        dest.writeTypedList(this.roomModels);
    }

    public RCModel() {
    }

    protected RCModel(Parcel in) {
        this.groupName = in.readString();
        this.rcJsonObject = in.readParcelable(JSONObject.class.getClassLoader());
        this.roomModels = in.createTypedArrayList(RoomModel.CREATOR);
    }

    public static final Creator<RCModel> CREATOR = new Creator<RCModel>() {
        @Override
        public RCModel createFromParcel(Parcel source) {
            return new RCModel(source);
        }

        @Override
        public RCModel[] newArray(int size) {
            return new RCModel[size];
        }
    };
}
*/
