package com.flyin.bookings.model;

import java.io.Serializable;

public class PricedItineraryObject implements Serializable {
    private AirItineraryObjectBean aiBean;
    private AirItineraryPricingInfoBean aipiBean;
    private boolean refundable_state;
    private boolean isFareCombinationRs = false;

    public AirItineraryObjectBean getAiBean() {
        return aiBean;
    }

    public void setAiBean(AirItineraryObjectBean aiBean) {
        this.aiBean = aiBean;
    }

    public AirItineraryPricingInfoBean getAipiBean() {
        return aipiBean;
    }

    public void setAipiBean(AirItineraryPricingInfoBean aipiBean) {
        this.aipiBean = aipiBean;
    }

    public boolean getRefundablestate() {
        for (int j = aiBean.getOriginDestinationOptionBeanArrayList().size() - 1; j >= 0; j--) {
            String refNum = aiBean.getOriginDestinationOptionBeanArrayList().get(j).getRefNum();
            OriginDestinationOptionBean bean = aiBean.getOriginDestinationOptionBeanArrayList().get(j);
            if (bean.getRs().equalsIgnoreCase("Refundable") || bean.getRs().equalsIgnoreCase("Refundable Before Departure")) {
                return true;
            }
        }
        return false;
    }

    public boolean isFareCombinationRs() {
        return isFareCombinationRs;
    }

    public void setFareCombinationRs(boolean fareCombinationRs) {
        isFareCombinationRs = fareCombinationRs;
    }
}
