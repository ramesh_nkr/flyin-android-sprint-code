package com.flyin.bookings.model;

import java.io.Serializable;

public class RewardsStatementBean implements Serializable {

    private String headerName, headerDateTime, headerPoints;
    private String headerImage, flightImage, hotelImage;
    private String flightsrc, flightDest, flightsrcDate, flightDestDate;
    private int hotel, flight;

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public String getHeaderDateTime() {
        return headerDateTime;
    }

    public void setHeaderDateTime(String headerDateTime) {
        this.headerDateTime = headerDateTime;
    }

    public String getHeaderPoints() {
        return headerPoints;
    }

    public void setHeaderPoints(String headerPoints) {
        this.headerPoints = headerPoints;
    }

    public String getHeaderImage() {
        return headerImage;
    }

    public void setHeaderImage(String headerImage) {
        this.headerImage = headerImage;
    }

    public String getFlightImage() {
        return flightImage;
    }

    public void setFlightImage(String flightImage) {
        this.flightImage = flightImage;
    }

    public String getHotelImage() {
        return hotelImage;
    }

    public void setHotelImage(String hotelImage) {
        this.hotelImage = hotelImage;
    }

    public String getFlightsrc() {
        return flightsrc;
    }

    public void setFlightsrc(String flightsrc) {
        this.flightsrc = flightsrc;
    }

    public String getFlightDest() {
        return flightDest;
    }

    public void setFlightDest(String flightDest) {
        this.flightDest = flightDest;
    }

    public String getFlightsrcDate() {
        return flightsrcDate;
    }

    public void setFlightsrcDate(String flightsrcDate) {
        this.flightsrcDate = flightsrcDate;
    }

    public String getFlightDestDate() {
        return flightDestDate;
    }

    public void setFlightDestDate(String flightDestDate) {
        this.flightDestDate = flightDestDate;
    }

    public int getHotel() {
        return hotel;
    }

    public void setHotel(int hotel) {
        this.hotel = hotel;
    }

    public int getFlight() {
        return flight;
    }

    public void setFlight(int flight) {
        this.flight = flight;
    }
}
