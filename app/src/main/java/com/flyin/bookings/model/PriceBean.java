package com.flyin.bookings.model;

public class PriceBean {
    private String total = "";
    private String tax = "";
    private String baseFare = "";
    private String userCurrency = "";
    private String cancellationCharge = "";
    private String reIssueCharge = "";
    private String extraBaggageAmount = "";
    private String couponDiscount = "";
    private String disountPrice = "";
    private String qitafAmount = "";
    private String earnPoints = "";
    private String redeemPoint = "";
    private String rdmDiscountusr = "";

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(String baseFare) {
        this.baseFare = baseFare;
    }

    public String getUserCurrency() {
        return userCurrency;
    }

    public void setUserCurrency(String userCurrency) {
        this.userCurrency = userCurrency;
    }

    public String getCancellationCharge() {
        return cancellationCharge;
    }

    public void setCancellationCharge(String cancellationCharge) {
        this.cancellationCharge = cancellationCharge;
    }

    public String getReIssueCharge() {
        return reIssueCharge;
    }

    public void setReIssueCharge(String reIssueCharge) {
        this.reIssueCharge = reIssueCharge;
    }

    public String getExtraBaggageAmount() {
        return extraBaggageAmount;
    }

    public void setExtraBaggageAmount(String extraBaggageAmount) {
        this.extraBaggageAmount = extraBaggageAmount;
    }

    public String getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getDisountPrice() {
        return disountPrice;
    }

    public void setDisountPrice(String disountPrice) {
        this.disountPrice = disountPrice;
    }

    public String getQitafAmount() {
        return qitafAmount;
    }

    public void setQitafAmount(String qitafAmount) {
        this.qitafAmount = qitafAmount;
    }

    public String getEarnPoints() {
        return earnPoints;
    }

    public void setEarnPoints(String earnPoints) {
        this.earnPoints = earnPoints;
    }

    public String getRedeemPoint() {
        return redeemPoint;
    }

    public void setRedeemPoint(String redeemPoint) {
        this.redeemPoint = redeemPoint;
    }

    public String getRdmDiscountusr() {
        return rdmDiscountusr;
    }

    public void setRdmDiscountusr(String rdmDiscountusr) {
        this.rdmDiscountusr = rdmDiscountusr;
    }
}
