package com.flyin.bookings.model;

public class RoomOccupancyObject {
    private String na;
    private String nc;

    public String getNa() {
        return na;
    }

    public void setNa(String na) {
        this.na = na;
    }

    public String getNc() {
        return nc;
    }

    public void setNc(String nc) {
        this.nc = nc;
    }
}
