package com.flyin.bookings.model;

import java.util.ArrayList;

public class CancellationInfoBean {
    private LastFreeCancellationBean lastFreeCancellationBean;
    private ArrayList<CancellationPolicyInfoBean> cancellationPolicyArray;

    public LastFreeCancellationBean getLastFreeCancellationBean() {
        return lastFreeCancellationBean;
    }

    public void setLastFreeCancellationBean(LastFreeCancellationBean lastFreeCancellationBean) {
        this.lastFreeCancellationBean = lastFreeCancellationBean;
    }

    public ArrayList<CancellationPolicyInfoBean> getCancellationPolicyArray() {
        return cancellationPolicyArray;
    }

    public void setCancellationPolicyArray(ArrayList<CancellationPolicyInfoBean> cancellationPolicyArray) {
        this.cancellationPolicyArray = cancellationPolicyArray;
    }
}
