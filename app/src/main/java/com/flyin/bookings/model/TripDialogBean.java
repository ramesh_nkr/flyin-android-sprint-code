package com.flyin.bookings.model;

public class TripDialogBean {
    public static final int RATING_HEADER = 1;
    public static final int RATING_ITEM = 2;
    public static final int REVIEW_HEADER = 3;
    public static final int REVIEW_ITEM = 4;
    private int itemType;
    private String ratingImage;
    private String PoweredImage;
    private String ratingText;
    private String title;
    private String publishedDate;
    private String user;
    private String review;
    private boolean isExpanded;

    public String getPoweredImage() {
        return PoweredImage;
    }

    public void setPoweredImage(String poweredImage) {
        PoweredImage = poweredImage;
    }

    public String getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(String reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    private String reviewsCount;

    public static int getRatingHeader() {
        return RATING_HEADER;
    }

    public static int getRatingItem() {
        return RATING_ITEM;
    }

    public static int getReviewHeader() {
        return REVIEW_HEADER;
    }

    public static int getReviewItem() {
        return REVIEW_ITEM;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public String getRatingImage() {
        return ratingImage;
    }

    public void setRatingImage(String ratingImage) {
        this.ratingImage = ratingImage;
    }

    public String getRatingText() {
        return ratingText;
    }

    public void setRatingText(String ratingText) {
        this.ratingText = ratingText;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }
}
