package com.flyin.bookings.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

public class HotelModel implements Parcelable, Cloneable {

    public static final Creator<HotelModel> CREATOR = new Creator<HotelModel>() {

        @Override
        public HotelModel createFromParcel(Parcel source) {
            return new HotelModel(source);
        }

        @Override
        public HotelModel[] newArray(int size) {
            return new HotelModel[size];
        }
    };

    String wdp = "";
    String p = "";
    String hna = "";
    String cur = "";
    String huid = "";
    String latitude = "";
    String longitude = "";
    String star = "";
    String distric = "";
    String image = "";
    String fullAddress = "";
    String dur = "";
    String city = "";
    String country = "";
    String date = "";
    String room_count = "";
    String chainName = "";
    String cityId = "";
    String hphoneCode = "";
    String hphoneNo = "";
    String hfaxCode = "";
    String hfaxNo = "";
    String check_in_date;
    String check_out_date;
    String hemail;
    String roomFacilityText = "";
    ArrayList<String> adultcountlist = new ArrayList<>();
    HashMap<String, ArrayList<String>> child_count_list = null;
    HashMap<String, ArrayList<RoomModel>> rcModelArrayList;
    ArrayList<RoomModel> roomlist;
    String[] imagelist;
    String exterior, lobby, rooms, cancel_policy, child_policy, pet, check_in,
            check_out, hotel_type;
    private String[] activity, service, general, topFeatures;
    private String hotelJson;
    private int rank;
    private int hotelRoomSortPrice = 0;
    private String reviewCount = "";
    private String reviewImgUrl = "";
    int bnplDateDiff = 0;
    private String htValue = "";
    private String mapRoomJson = "";

    String roomJson;

    private HotelModel(Parcel in) {
        this.wdp = in.readString();
        this.p = in.readString();
        this.huid = in.readString();
        this.hna = in.readString();
        this.cur = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.star = in.readString();
        this.distric = in.readString();
        this.image = in.readString();
        this.image = in.readString();
        this.imagelist = in.createStringArray();
        this.exterior = in.readString();
        this.lobby = in.readString();
        this.rooms = in.readString();
        this.cancel_policy = in.readString();
        this.child_policy = in.readString();
        this.pet = in.readString();
        this.check_in = in.readString();
        this.check_out = in.readString();
        this.dur = in.readString();
        this.city = in.readString();
        this.hfaxCode = in.readString();
        this.hfaxNo = in.readString();
        this.hphoneCode = in.readString();
        this.hphoneNo = in.readString();
        this.cityId = in.readString();
        this.check_in_date = in.readString();
        this.check_out_date = in.readString();
        this.hemail = in.readString();
        this.hotelJson = in.readString();
        this.date = in.readString();
        this.hotel_type = in.readString();
        this.country = in.readString();
        this.chainName = in.readString();
        this.activity = in.createStringArray();
        this.service = in.createStringArray();
        this.general = in.createStringArray();
        this.adultcountlist = in.readArrayList(null);
        this.child_count_list = in.readHashMap(null);
        this.rcModelArrayList = in.readHashMap(null);
        this.room_count = in.readString();
        this.roomJson = in.readString();
        this.roomlist = in.readArrayList(RoomModel.class.getClassLoader());
        this.roomFacilityText = in.readString();
        this.rank = in.readInt();
        this.htValue = in.readString();
        this.mapRoomJson = in.readString();
    }

    public HotelModel() {

    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        HotelModel another = null;
        try {
            another = (HotelModel) super.clone();
        } catch (CloneNotSupportedException e) {
        }
        return another;
    }

    public static Creator<HotelModel> getCREATOR() {
        return CREATOR;
    }

    public String getCheck_out_date() {
        return check_out_date;
    }

    public void setCheck_out_date(String check_out_date) {
        this.check_out_date = check_out_date;
    }

    public String getCheck_in_date() {
        return check_in_date;
    }

    public void setCheck_in_date(String check_in_date) {
        this.check_in_date = check_in_date;
    }

    public String getHemail() {
        return hemail;
    }

    public void setHemail(String hemail) {
        this.hemail = hemail;
    }

    public String getHfaxNo() {
        return hfaxNo;
    }

    public void setHfaxNo(String hfaxNo) {
        this.hfaxNo = hfaxNo;
    }

    public String getHfaxCode() {
        return hfaxCode;
    }

    public void setHfaxCode(String hfaxCode) {
        this.hfaxCode = hfaxCode;
    }

    public String getHphoneNo() {
        return hphoneNo;
    }

    public void setHphoneNo(String hphoneNo) {
        this.hphoneNo = hphoneNo;
    }

    public String getHphoneCode() {
        return hphoneCode;
    }

    public void setHphoneCode(String hphoneCode) {
        this.hphoneCode = hphoneCode;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getChainName() {
        return chainName;
    }

    public void setChainName(String chainName) {
        this.chainName = chainName;
    }

    public String getRoom_count() {
        return room_count;
    }

    public void setRoom_count(String room_count) {
        this.room_count = room_count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<String> getAdultcountlist() {
        return adultcountlist;
    }

    public void setAdultcountlist(ArrayList<String> adultcountlist) {
        this.adultcountlist = adultcountlist;
    }

    public HashMap<String, ArrayList<String>> getChild_count_list() {
        return child_count_list;
    }

    public void setChild_count_list(
            HashMap<String, ArrayList<String>> child_count_list) {
        this.child_count_list = child_count_list;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDur() {
        return dur;
    }

    public void setDur(String dur) {
        this.dur = dur;
    }

    public HashMap<String, ArrayList<RoomModel>> getRcModelArrayList() {
        return rcModelArrayList;
    }

    public void setRcModelArrayList(HashMap<String, ArrayList<RoomModel>> rcModelArrayList) {
        this.rcModelArrayList = rcModelArrayList;
    }

    public String getHotel_type() {
        return hotel_type;
    }

    public void setHotel_type(String hotel_type) {
        this.hotel_type = hotel_type;
    }

    public String[] getActivity() {
        return activity;
    }

    public void setActivity(String[] activity) {
        this.activity = activity;
    }

    public String[] getService() {
        return service;
    }

    public void setService(String[] service) {
        this.service = service;
    }

    public String[] getGeneral() {
        return general;
    }

    public void setGeneral(String[] general) {
        this.general = general;
    }

    public String getExterior() {
        return exterior;
    }

    public void setExterior(String exterior) {
        this.exterior = exterior;
    }

    public String getLobby() {
        return lobby;
    }

    public void setLobby(String lobby) {
        this.lobby = lobby;
    }

    public String getRooms() {
        return rooms;
    }

    public void setRooms(String rooms) {
        this.rooms = rooms;
    }

    public String getCancel_policy() {
        return cancel_policy;
    }

    public void setCancel_policy(String cancel_policy) {
        this.cancel_policy = cancel_policy;
    }

    public String getChild_policy() {
        return child_policy;
    }

    public void setChild_policy(String child_policy) {
        this.child_policy = child_policy;
    }

    public String getPet() {
        return pet;
    }

    public void setPet(String pet) {
        this.pet = pet;
    }

    public String getCheck_in() {
        return check_in;
    }

    public void setCheck_in(String check_in) {
        this.check_in = check_in;
    }

    public String getCheck_out() {
        return check_out;
    }

    public void setCheck_out(String check_out) {
        this.check_out = check_out;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String[] getImagelist() {
        return imagelist;
    }

    public void setImagelist(String[] imagelist) {
        this.imagelist = imagelist;
    }

    public ArrayList<RoomModel> getRoomlist() {
        return roomlist;
    }

    public void setRoomlist(ArrayList<RoomModel> roomlist) {
        this.roomlist = roomlist;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getDistric() {
        return distric;
    }

    public void setDistric(String addrss) {
        this.distric = addrss;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getHuid() {
        return huid;
    }

    public void setHuid(String huid) {
        this.huid = huid;
    }

    public String getCur() {
        return cur;
    }

    public void setCur(String cur) {
        this.cur = cur;
    }

    public String getWdp() {
        return wdp;
    }

    public void setWdp(String wdp) {
        this.wdp = wdp;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getHna() {
        return hna;
    }

    public void setHna(String hna) {
        this.hna = hna;
    }


    public int getBnplDateDiff() {
        return bnplDateDiff;
    }

    public void setBnplDateDiff(int bnplDateDiff) {
        this.bnplDateDiff = bnplDateDiff;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(wdp);
        dest.writeString(p);
        dest.writeString(hna);
        dest.writeString(cur);
        dest.writeString(huid);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(star);
        dest.writeString(distric);
        dest.writeString(image);
        dest.writeString(fullAddress);
        dest.writeStringArray(imagelist);
        dest.writeString(exterior);
        dest.writeString(lobby);
        dest.writeString(rooms);
        dest.writeString(cancel_policy);
        dest.writeString(child_policy);
        dest.writeString(pet);
        dest.writeString(check_in);
        dest.writeString(dur);
        dest.writeString(check_out);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(check_in_date);
        dest.writeString(check_out_date);
        dest.writeString(date);
        dest.writeString(chainName);
        dest.writeString(room_count);
        dest.writeStringArray(activity);
        dest.writeStringArray(service);
        dest.writeStringArray(general);
        dest.writeString(hotel_type);
        dest.writeList(adultcountlist);
        dest.writeMap(child_count_list);
        dest.writeMap(rcModelArrayList);
        dest.writeList(roomlist);
        dest.writeString(hotelJson);
        dest.writeString(hfaxCode);
        dest.writeString(hfaxNo);
        dest.writeString(hphoneCode);
        dest.writeString(hphoneNo);
        dest.writeString(cityId);
        dest.writeString(hemail);
        dest.writeString(roomJson);
        dest.writeInt(rank);
        dest.writeString(htValue);
        dest.writeString(mapRoomJson);
    }

    public String getHotelJson() {
        return hotelJson;
    }

    public void setHotelJson(String hotelJson) {
        this.hotelJson = hotelJson;
    }

    public String[] getTopFeatures() {
        return topFeatures;
    }

    public void setTopFeatures(String[] topFeatures) {
        this.topFeatures = topFeatures;
    }

    public int getHotelRoomSortPrice() {
        return hotelRoomSortPrice;
    }

    public void setHotelRoomSortPrice(int hotelRoomSortPrice) {
        this.hotelRoomSortPrice = hotelRoomSortPrice;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getReviewImgUrl() {
        return reviewImgUrl;
    }

    public void setReviewImgUrl(String reviewImgUrl) {
        this.reviewImgUrl = reviewImgUrl;
    }

    public String getHotelTypeValue() {
        return htValue;
    }

    public void setHotelTypeValue(String hotelTypeValue) {
        this.htValue = hotelTypeValue;
    }

    public String getMapRoomJson() {
        return mapRoomJson;
    }

    public void setMapRoomJson(String mapRoomJson) {
        this.mapRoomJson = mapRoomJson;
    }
}