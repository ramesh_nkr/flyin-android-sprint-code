package com.flyin.bookings.model;

public class FilterAirlineSelectionItem {
    private String mal;
    private boolean isSelected;

    public FilterAirlineSelectionItem(String mal, boolean isSelected) {
        this.mal = mal;
        this.isSelected = isSelected;
    }

    public String getMal() {
        return mal;
    }

    public void setMal(String mal) {
        this.mal = mal;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
