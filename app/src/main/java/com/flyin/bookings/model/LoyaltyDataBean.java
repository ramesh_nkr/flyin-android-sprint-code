package com.flyin.bookings.model;

import java.util.ArrayList;

public class LoyaltyDataBean {
    private LoyaltyBasicInfoBean loyaltyBasicInfoBean;
    private ArrayList<LoyaltyTransactionInfoBean> transactionInfoList;
    private ArrayList<LoyaltyBookingInfoBean> loyaltyBookingInfoArrList = null;
    private HotelMoreInfoObject hotelMoreInfoObject = null;

    public LoyaltyBasicInfoBean getLoyaltyBasicInfoBean() {
        return loyaltyBasicInfoBean;
    }

    public void setLoyaltyBasicInfoBean(LoyaltyBasicInfoBean loyaltyBasicInfoBean) {
        this.loyaltyBasicInfoBean = loyaltyBasicInfoBean;
    }

    public ArrayList<LoyaltyTransactionInfoBean> getTransactionInfoList() {
        return transactionInfoList;
    }

    public void setTransactionInfoList(ArrayList<LoyaltyTransactionInfoBean> transactionInfoList) {
        this.transactionInfoList = transactionInfoList;
    }

    public ArrayList<LoyaltyBookingInfoBean> getLoyaltyBookingInfoArrList() {
        return loyaltyBookingInfoArrList;
    }

    public void setLoyaltyBookingInfoArrList(ArrayList<LoyaltyBookingInfoBean> loyaltyBookingInfoArrList) {
        this.loyaltyBookingInfoArrList = loyaltyBookingInfoArrList;
    }

    public HotelMoreInfoObject getHotelMoreInfoObject() {
        return hotelMoreInfoObject;
    }

    public void setHotelMoreInfoObject(HotelMoreInfoObject hotelMoreInfoObject) {
        this.hotelMoreInfoObject = hotelMoreInfoObject;
    }
}
