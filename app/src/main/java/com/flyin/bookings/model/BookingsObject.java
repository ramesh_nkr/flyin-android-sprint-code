package com.flyin.bookings.model;

public class BookingsObject {
    private EntityDetailsObjectBean entityDetailsObjectBean;
    private GeneralDetailsObjectBean generalDetailsObjectBean;
    private PassengerDetailsObjectBean passengerDetailsObjectBean;
    private PriceBean priceBean;
    private long flightDepartDate;
    private long hotelCheckInDate;

    public EntityDetailsObjectBean getEntityDetailsObjectBean() {
        return entityDetailsObjectBean;
    }

    public void setEntityDetailsObjectBean(EntityDetailsObjectBean entityDetailsObjectBean) {
        this.entityDetailsObjectBean = entityDetailsObjectBean;
    }

    public GeneralDetailsObjectBean getGeneralDetailsObjectBean() {
        return generalDetailsObjectBean;
    }

    public void setGeneralDetailsObjectBean(GeneralDetailsObjectBean generalDetailsObjectBean) {
        this.generalDetailsObjectBean = generalDetailsObjectBean;
    }

    public PassengerDetailsObjectBean getPassengerDetailsObjectBean() {
        return passengerDetailsObjectBean;
    }

    public void setPassengerDetailsObjectBean(PassengerDetailsObjectBean passengerDetailsObjectBean) {
        this.passengerDetailsObjectBean = passengerDetailsObjectBean;
    }

    public PriceBean getPriceBean() {
        return priceBean;
    }

    public void setPriceBean(PriceBean priceBean) {
        this.priceBean = priceBean;
    }

    public long getFlightDepartDate() {
        return flightDepartDate;
    }

    public void setFlightDepartDate(long flightDepartDate) {
        this.flightDepartDate = flightDepartDate;
    }

    public long getHotelCheckInDate() {
        return hotelCheckInDate;
    }

    public void setHotelCheckInDate(long hotelCheckInDate) {
        this.hotelCheckInDate = hotelCheckInDate;
    }
}
