package com.flyin.bookings.model;

public class PersonalObjectBean {
    private String title;
    private String firstName;
    private String middleName;
    private String lastName;
    private String ticketNo;
    private String isLeadPax;
    private String passengerType;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getIsLeadPax() {
        return isLeadPax;
    }

    public void setIsLeadPax(String isLeadPax) {
        this.isLeadPax = isLeadPax;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }
}
