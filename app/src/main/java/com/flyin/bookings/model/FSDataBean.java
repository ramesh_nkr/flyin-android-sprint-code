package com.flyin.bookings.model;

import java.io.Serializable;
import java.util.ArrayList;

public class FSDataBean implements Serializable {
    private String dap;
    private String dt;
    private String aap;
    private String at;
    private String oal;
    private String mal;
    private String eq;
    private String ct;
    private String dur;
    private String fdur;
    private String st;
    private String si;
    private String ti;
    private String fi;
    private String comment;
    private String as;
    private String fc;
    private String tofi;
    private String fn;
    private String is;
    private String sq;
    private String ardt;
    private String ddt;
    private boolean isMerchandiseExpanded = false;
    private ArrayList<String> iconNameArrayList;

    private FlightAmenitiesBean flightAmenitiesBean;

    public FlightAmenitiesBean getFlightAmenitiesBean() {
        return flightAmenitiesBean;
    }

    public void setFlightAmenitiesBean(FlightAmenitiesBean flightAmenitiesBean) {
        this.flightAmenitiesBean = flightAmenitiesBean;
    }

    public ArrayList<String> getIconNameArrayList() {
        return iconNameArrayList;
    }

    public void setIconNameArrayList(ArrayList<String> iconNameArrayList) {
        this.iconNameArrayList = iconNameArrayList;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    private String airlineName;


    public String getDap() {
        return dap;
    }

    public void setDap(String dap) {
        this.dap = dap;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getAap() {
        return aap;
    }

    public void setAap(String aap) {
        this.aap = aap;
    }

    public String getAt() {
        return at;
    }

    public void setAt(String at) {
        this.at = at;
    }

    public String getOal() {
        return oal;
    }

    public void setOal(String oal) {
        this.oal = oal;
    }

    public String getMal() {
        return mal;
    }

    public void setMal(String mal) {
        this.mal = mal;
    }

    public String getEq() {
        return eq;
    }

    public void setEq(String eq) {
        this.eq = eq;
    }

    public String getCt() {
        return ct;
    }

    public void setCt(String ct) {
        this.ct = ct;
    }

    public String getDur() {
        return dur;
    }

    public void setDur(String dur) {
        this.dur = dur;
    }

    public String getFdur() {
        return fdur;
    }

    public void setFdur(String fdur) {
        this.fdur = fdur;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public String getSi() {
        return si;
    }

    public void setSi(String si) {
        this.si = si;
    }

    public String getTi() {
        return ti;
    }

    public void setTi(String ti) {
        this.ti = ti;
    }

    public String getFi() {
        return fi;
    }

    public void setFi(String fi) {
        this.fi = fi;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAs() {
        return as;
    }

    public void setAs(String as) {
        this.as = as;
    }

    public String getFc() {
        return fc;
    }

    public void setFc(String fc) {
        this.fc = fc;
    }

    public String getTofi() {
        return tofi;
    }

    public void setTofi(String tofi) {
        this.tofi = tofi;
    }

    public String getFn() {
        return fn;
    }

    public void setFn(String fn) {
        this.fn = fn;
    }

    public String getIs() {
        return is;
    }

    public void setIs(String is) {
        this.is = is;
    }

    public String getSq() {
        return sq;
    }

    public void setSq(String sq) {
        this.sq = sq;
    }

    public String getArdt() {
        return ardt;
    }

    public void setArdt(String ardt) {
        this.ardt = ardt;
    }

    public String getDdt() {
        return ddt;
    }

    public void setDdt(String ddt) {
        this.ddt = ddt;
    }

    public boolean isMerchandiseExpanded() {
        return isMerchandiseExpanded;
    }

    public void setMerchandiseExpanded(boolean merchandiseExpanded) {
        isMerchandiseExpanded = merchandiseExpanded;
    }
}
