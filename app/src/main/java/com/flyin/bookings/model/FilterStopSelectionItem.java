package com.flyin.bookings.model;

public class FilterStopSelectionItem {
    private String stopMessage;
    private int stopType;
    private boolean isSelected;

    public String getStopMessage() {
        return stopMessage;
    }

    public void setStopMessage(String stopMessage) {
        this.stopMessage = stopMessage;
    }

    public int getStopType() {
        return stopType;
    }

    public void setStopType(int stopType) {
        this.stopType = stopType;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
