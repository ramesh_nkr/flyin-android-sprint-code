package com.flyin.bookings.model;

import org.json.JSONArray;

import java.util.ArrayList;

public class Singleton {
    private static Singleton singleton = new Singleton();
    public ArrayList<PricedItineraryObject> selectedPriceItinerarayObject = new ArrayList<>();
    public ArrayList<FlightAmenitiesBean> selectedMerchandiseObject = new ArrayList<>();
    public ArrayList<PricedItineraryObject> resultFlightsArrayList = new ArrayList<>();
    public ArrayList<PricedItineraryObject> returnFlightPIArrayList = new ArrayList<>();
    public ArrayList<PricedItineraryObject> fCResultFlightPIArrayList = new ArrayList<>();
    public ArrayList<FilterRefundSelectionItem> refundStatusArrayList = new ArrayList<>();
    public ArrayList<FilterStopSelectionItem> resultStopsArrayList = new ArrayList<>();
    public ArrayList<FilterAirlineSelectionItem> resultAirlinesArrayList = new ArrayList<>();
    public ArrayList<FilterAirlineSelectionItem> returnResultAirlinesArrayList = new ArrayList<>();
    public ArrayList<FilterAirportSelectionItem> resultAirportsArrayList = new ArrayList<>();
    public ArrayList<FilterAirportSelectionItem> firstAirportsArrayList = new ArrayList<>();
    public ArrayList<FilterAirportSelectionItem> secondAirportsArrayList = new ArrayList<>();
    public ArrayList<FilterAirportSelectionItem> thirdAirportsArrayList = new ArrayList<>();
    public ArrayList<FilterTimeSelectionItem> resultTimeArrayList = new ArrayList<>();
    public ArrayList<FilterTimeSelectionItem> departureTimeArrayList = new ArrayList<>();
    public ArrayList<FilterTimeSelectionItem> arrivalTimeArrayList = new ArrayList<>();
    public ArrayList<FilterAirlineSelectionItem> resultComboAirlinesArrayList = new ArrayList<>();
    public ArrayList<FilterAirportSelectionItem> resultComboAirportsArrayList = new ArrayList<>();
    public ArrayList<FilterAirportSelectionItem> firstResultAirportsArrayList = new ArrayList<>();
    public ArrayList<FilterAirportSelectionItem> secondResultAirportsArrayList = new ArrayList<>();
    public ArrayList<FilterAirportSelectionItem> thirdResultAirportsArrayList = new ArrayList<>();
    public ArrayList<FilterStopSelectionItem> resultComboStopsArrayList = new ArrayList<>();
    public ArrayList<FilterTimeSelectionItem> resultComboTimeArrayList = new ArrayList<>();
    public ArrayList<FilterTimeSelectionItem> departureComboTimeArrayList = new ArrayList<>();
    public ArrayList<FilterTimeSelectionItem> arrivalComboTimeArrayList = new ArrayList<>();
    public ArrayList<FhRoomPassengerDetailsBean> roomPassengerInfoList = new ArrayList<>();
    public ArrayList<FhRoomPassengerDetailsBean> updatedRoomPassengerInfoList = new ArrayList<>();
    public ArrayList<TravellerDetailsBean> fetchTravellerArrayList = new ArrayList<>();
    public BookingsObject selectedTripFlightObject = null;
    public JSONArray ptqArr = new JSONArray();
    public TravellerDetailsBean travellersInfoBean = null;

    private Singleton() {
    }

    public String model = "";
    public String hotelString = "";
    public boolean isForHotel = false;
    public String adultCount = "";
    public String childCount = "";
    public String hotelTotalPrice = "";

    public static Singleton getInstance() {
        return singleton;
    }

    protected static void demoMethod() {
        System.out.println("demoMethod for singleton");
    }
}
