package com.flyin.bookings.model;

public class LoyaltyBookingHotelBean {
    private String uniqueId = "";
    private String checkIn = "";
    private String checkOut = "";
    private String hotelName = "";
    private String image = "";
    private String rating = "";
    private String cityName = "";
    private String countryName = "";
    private HotelMoreInfoObject hotelMoreInfoObject = null;

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public HotelMoreInfoObject getHotelMoreInfoObject() {
        return hotelMoreInfoObject;
    }

    public void setHotelMoreInfoObject(HotelMoreInfoObject hotelMoreInfoObject) {
        this.hotelMoreInfoObject = hotelMoreInfoObject;
    }
}
