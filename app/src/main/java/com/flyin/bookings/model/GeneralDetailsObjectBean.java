package com.flyin.bookings.model;

public class GeneralDetailsObjectBean {
    private String bookingStatus;
    private String flyinCode;
    private String bookingDate;
    private String customerId;

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getFlyinCode() {
        return flyinCode;
    }

    public void setFlyinCode(String flyinCode) {
        this.flyinCode = flyinCode;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
