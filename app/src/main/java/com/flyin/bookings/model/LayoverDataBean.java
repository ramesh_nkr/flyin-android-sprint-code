package com.flyin.bookings.model;

public class LayoverDataBean {
    private String departureName = "";
    private String arrivalName = "";
    private String headerMessage = "";
    private int stopsCount;
    private String journeyType = "";
    private String legOrder = "";

    public String getDepartureName() {
        return departureName;
    }

    public void setDepartureName(String departureName) {
        this.departureName = departureName;
    }

    public String getArrivalName() {
        return arrivalName;
    }

    public void setArrivalName(String arrivalName) {
        this.arrivalName = arrivalName;
    }

    public String getHeaderMessage() {
        return headerMessage;
    }

    public void setHeaderMessage(String headerMessage) {
        this.headerMessage = headerMessage;
    }

    public int getStopsCount() {
        return stopsCount;
    }

    public void setStopsCount(int stopsCount) {
        this.stopsCount = stopsCount;
    }

    public String getJourneyType() {
        return journeyType;
    }

    public void setJourneyType(String journeyType) {
        this.journeyType = journeyType;
    }

    public String getLegOrder() {
        return legOrder;
    }

    public void setLegOrder(String legOrder) {
        this.legOrder = legOrder;
    }
}
