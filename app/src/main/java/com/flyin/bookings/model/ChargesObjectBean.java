package com.flyin.bookings.model;

public class ChargesObjectBean {
    private String ty;
    private String val;

    public String getTy() {
        return ty;
    }

    public void setTy(String ty) {
        this.ty = ty;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
