package com.flyin.bookings.model;

public class AirMealList {
    private String airMealCode;
    private String airMealName;
    private String language;

    public String getAirMealCode() {
        return airMealCode;
    }

    public void setAirMealCode(String airMealCode) {
        this.airMealCode = airMealCode;
    }

    public String getAirMealName() {
        return airMealName;
    }

    public void setAirMealName(String airMealName) {
        this.airMealName = airMealName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
