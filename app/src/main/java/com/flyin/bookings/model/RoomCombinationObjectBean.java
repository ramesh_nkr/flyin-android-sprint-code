package com.flyin.bookings.model;

import java.util.ArrayList;

public class RoomCombinationObjectBean {
    private ArrayList<RoomsInformationBean> roomsInformationArrayList;
    private String ad;
    private String dur;
    private PriceObjectBean priceObjectBean;
    private WODiscountPriceBean woDiscountPriceBean;
    private NetPriceBean netPriceBean;
    private String skm;
    private HotelInformationObjectBean hotelInformationObjectBean;
    private Double markupValue;
    private Double payPrice;
    private boolean isOtherRoomSelected;

    public Double getRoomFinalPrice() {
        return roomFinalPrice;
    }

    public void setRoomFinalPrice(Double roomFinalPrice) {
        this.roomFinalPrice = roomFinalPrice;
    }

    private Double roomFinalPrice = 0.0;

    public ArrayList<RoomsInformationBean> getRoomsInformationArrayList() {
        return roomsInformationArrayList;
    }

    public void setRoomsInformationArrayList(ArrayList<RoomsInformationBean> roomsInformationArrayList) {
        this.roomsInformationArrayList = roomsInformationArrayList;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getDur() {
        return dur;
    }

    public void setDur(String dur) {
        this.dur = dur;
    }

    public PriceObjectBean getPriceObjectBean() {
        return priceObjectBean;
    }

    public void setPriceObjectBean(PriceObjectBean priceObjectBean) {
        this.priceObjectBean = priceObjectBean;
    }

    public WODiscountPriceBean getWoDiscountPriceBean() {
        return woDiscountPriceBean;
    }

    public void setWoDiscountPriceBean(WODiscountPriceBean woDiscountPriceBean) {
        this.woDiscountPriceBean = woDiscountPriceBean;
    }

    public NetPriceBean getNetPriceBean() {
        return netPriceBean;
    }

    public void setNetPriceBean(NetPriceBean netPriceBean) {
        this.netPriceBean = netPriceBean;
    }

    public String getSkm() {
        return skm;
    }

    public void setSkm(String skm) {
        this.skm = skm;
    }

    public HotelInformationObjectBean getHotelInformationObjectBean() {
        return hotelInformationObjectBean;
    }

    public void setHotelInformationObjectBean(HotelInformationObjectBean hotelInformationObjectBean) {
        this.hotelInformationObjectBean = hotelInformationObjectBean;
    }

    public Double getMarkupValue() {
        return markupValue;
    }

    public void setMarkupValue(Double markupValue) {
        this.markupValue = markupValue;
    }

    public Double getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(Double payPrice) {
        this.payPrice = payPrice;
    }

    public boolean isOtherRoomSelected() {
        return isOtherRoomSelected;
    }

    public void setOtherRoomSelected(boolean otherRoomSelected) {
        isOtherRoomSelected = otherRoomSelected;
    }
}
