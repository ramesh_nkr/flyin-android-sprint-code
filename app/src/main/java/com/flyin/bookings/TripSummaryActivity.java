package com.flyin.bookings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.text.style.UnderlineSpan;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.dialog.FareBreakupDialog;
import com.flyin.bookings.dialog.SignInDialog;
import com.flyin.bookings.listeners.AirlinesInterface;
import com.flyin.bookings.model.AirItineraryObjectBean;
import com.flyin.bookings.model.AirItineraryPricingInfoBean;
import com.flyin.bookings.model.AirportSectorBean;
import com.flyin.bookings.model.AmenitiesBean;
import com.flyin.bookings.model.FSDataBean;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.FlightAmenitiesBean;
import com.flyin.bookings.model.ItinTotalFareObjectBean;
import com.flyin.bookings.model.OriginDestinationOptionBean;
import com.flyin.bookings.model.PFBDObjectBean;
import com.flyin.bookings.model.PassengerFareObjectBean;
import com.flyin.bookings.model.RoundImageTransform;
import com.flyin.bookings.model.TaxObjectBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.AirlinesData;
import com.flyin.bookings.util.AirportData;
import com.flyin.bookings.util.AmenitiesConstants;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;
import com.webengage.sdk.android.User;
import com.webengage.sdk.android.WebEngage;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@SuppressWarnings("deprecation")
public class TripSummaryActivity extends AppCompatActivity implements AsyncTaskListener {
    private Typeface tf, titleFace, textFace, defaultFace, boldFace;
    private Button continueButton;
    private LinearLayout flightInfoLayout, flightRetrunInfoLayout, flightMultiTripInfoLayout;
    private int tripType = 0;
    private int adultCount = 0;
    private double adultTotalFare = 0;
    private double adultTaxFare = 0;
    private int childCount = 0;
    private double childTotalFare = 0;
    private double childTaxFare = 0;
    private int infantCount = 0;
    private double infantTotalFare = 0;
    private double infantTaxFare = 0;
    private static final String TAG = "TripSummaryActivity";
    private boolean isSingInClicked = true;
    private ArrayList<AirItineraryObjectBean> airItineraryArray = new ArrayList<>();
    private ArrayList<AirItineraryPricingInfoBean> airItineraryPricingInfoBeanArray = new ArrayList<>();
    private PFBDObjectBean pfbdObjBean = new PFBDObjectBean();
    private String randomNumber = "";
    private String aipiJSONObject = "";
    private String aiJSONObject = "";
    private String tdr = "";
    private String hold = "";
    private String usctp = "";
    private String resultData = "";
    private String selectedLang = "";
    private String baggageDetails = "";
    private String cabinType = "";
    private SignInDialog signInDialog;
    private String sourceName = "";
    private String destinationName = "";
    private String secondSource = "";
    private String secondDestination = "";
    private String thirdSource = "";
    private String thirdDestination = "";
    private String forwardFlightDate = "";
    private String returnFlightDate = "";
    private String firstFlightDate = "";
    private String secondFlightDate = "";
    private String thirdFlightDate = "";
    private String ddtPreviousTime = "";
    private MyReceiver myReceiver;
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private LayoutInflater inflater;
    private boolean isArabicLang = false;
    private ArrayList<String> airlineCodeArr = null;
    private String airlineCodeName = "";
    private int infoSource = -1;
    private int loginDetailsSuccess = -1;
    private TextView yourTripHeaderText = null;
    private TextView tripTypeHeader = null;
    private ArrayList<OriginDestinationOptionBean> showOdoArrayList = null;
    private TextView showAllDetailsText = null;
    private boolean isDataExpanded = false;
    private TextView signInText = null;
    private LinearLayout signInLayout = null;
    private LinearLayout signInHeaderLayout = null;
    private Double baseFare = 0.0;
    private Double taxesFare = 0.0;
    private Double totalFare = 0.0;
    private FareBreakupDialog fareBreakupDialog = null;
    private String flightReturnName = "";
    private boolean isFirstNonStopFlight = false;
    private boolean isSecondNonStopFlight = false;
    private boolean isThirdNonStopFlight = false;
    private String selectedClass = "";
    private View loadingView = null;
    private RelativeLayout headerViewLayout = null;
    private static final int GET_MERCHANDISE_DATA = 3;
    private static final int GET_MERCHANDISE_AIRPORT_DATA = 4;
    private ArrayList<AirportSectorBean> flightNamesArray = null;
    private ArrayList<FlightAmenitiesBean> mainMarchArrayList = null;
    private String journeyClassType = "";
    private HashMap<String, String> routesMap = new HashMap<>();
    private ArrayList<String> iconNameArrayList = null;
    private ArrayList<String> airportArrayList = new ArrayList<>();
    private ArrayList<String> airlineArrayList = new ArrayList<>();
    private HashMap<String, FlightAirportName> airportNamesMap = new HashMap<>();
    private HashMap<String, String> airLineNamesMap = new HashMap<>();
    private AirportData airportData = new AirportData();
    private AirlinesData airlinesData = new AirlinesData();
    private String destinationNameText = "";
    private boolean isFareCombinationSelected = false;
    private RelativeLayout refundPolicyLayout = null;
    private ArrayList<String> fareTypeArray = null;
    private TextView rewardsPointsText = null;
    private LinearLayout rewardsLayout = null;
    private static final int GET_LOYALTY_POINTS = 5;
    private final static int TRAVELLERS_PAGE = 6;
    private static final int GET_ACCESS_TOKEN = 8;
    private int passengerCount;
    private String userEarnedPoints = "";
    private ImageView errorImage = null;
    private Button searchButton = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tripsummary);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            resultData = bundle.getString(Constants.JSON_RESULT, "");
            tripType = Integer.parseInt(bundle.getString(Constants.TRIP_TYPE, ""));
            randomNumber = bundle.getString(Constants.BOOKING_RANDOM_NUMBER, "");
            sourceName = bundle.getString(Constants.FORWARD_SOURCE, "");
            destinationName = bundle.getString(Constants.FORWARD_DESTINATION, "");
            secondSource = bundle.getString(Constants.SECOND_FORWARD_SOURCE, "");
            secondDestination = bundle.getString(Constants.SECOND_FORWARD_DESTINATION, "");
            thirdSource = bundle.getString(Constants.THIRD_FORWARD_SOURCE, "");
            thirdDestination = bundle.getString(Constants.THIRD_FORWARD_DESTINATION, "");
            forwardFlightDate = bundle.getString(Constants.FORWARD_FLIGHT_DATE, "");
            returnFlightDate = bundle.getString(Constants.RETURN_FLIGHT_DATE, "");
            firstFlightDate = bundle.getString(Constants.FIRST_FLIGHT_DATE, "");
            secondFlightDate = bundle.getString(Constants.SECOND_FLIGHT_DATE, "");
            thirdFlightDate = bundle.getString(Constants.THIRD_FLIGHT_DATE, "");
            flightReturnName = bundle.getString(Constants.RETURN_SOURCE_NAME, "");
        }
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        String fontDefault = Constants.FONT_DROIDKUFI_REGULAR;
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        selectedLang = Constants.LANGUAGE_ENGLISH_CODE;
        isArabicLang = false;
        airlineCodeArr = new ArrayList<>();
        flightNamesArray = new ArrayList<>();
        mainMarchArrayList = new ArrayList<>();
        iconNameArrayList = new ArrayList<>();
        fareTypeArray = new ArrayList<>();
//        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
//        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
//        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
//        if (tokenTime == -1) {
//            getTokenFromServer();
//        } else {
//            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
//            if (diff > Long.parseLong(expireIn)) {
//                getTokenFromServer();
//            }
//        }
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        yourTripHeaderText = (TextView) findViewById(R.id.your_trip_header);
        TextView lockPackageText = (TextView) findViewById(R.id.lock_package_text);
        TextView ticketsCountHeader = (TextView) findViewById(R.id.tickets_count_header);
        TextView classTypeHeader = (TextView) findViewById(R.id.class_type_header);
        tripTypeHeader = (TextView) findViewById(R.id.trip_type_header);
        TextView changeFlightText = (TextView) findViewById(R.id.change_flight_text);
        showAllDetailsText = (TextView) findViewById(R.id.show_all_text);
        signInLayout = (LinearLayout) findViewById(R.id.sing_in_button_layout);
        flightInfoLayout = (LinearLayout) findViewById(R.id.flights_details_layout);
        flightRetrunInfoLayout = (LinearLayout) findViewById(R.id.flight_return_details_layout);
        flightMultiTripInfoLayout = (LinearLayout) findViewById(R.id.flight_multiTrip_details_layout);
        TextView totalPrice = (TextView) findViewById(R.id.total_price);
        TextView totalPriceFare = (TextView) findViewById(R.id.total_price_fare);
        TextView taxesFareText = (TextView) findViewById(R.id.taxes_fare_text);
        TextView fareBreakUpText = (TextView) findViewById(R.id.fare_break_up_text);
        signInText = (TextView) findViewById(R.id.sign_in_text);
        TextView orTextView = (TextView) findViewById(R.id.or_text_view);
        continueButton = (Button) findViewById(R.id.continue_button);
        ImageView tripBackgroundImage = (ImageView) findViewById(R.id.trip_bg_image);
        signInHeaderLayout = (LinearLayout) findViewById(R.id.sing_in_layout);
        headerViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        refundPolicyLayout = (RelativeLayout) findViewById(R.id.refund_policy_layout);
        TextView refundTypeHeader = (TextView) findViewById(R.id.refund_type_header);
        TextView refundTypeDescription = (TextView) findViewById(R.id.refund_type_description);
        rewardsPointsText = (TextView) findViewById(R.id.rewards_flight_dest_textview);
        rewardsLayout = (LinearLayout) findViewById(R.id.reward_points_layout);
        if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
            selectedLang = Constants.LANGUAGE_ARABIC_CODE;
            isArabicLang = true;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontDefault = Constants.FONT_DROIDKUFI_REGULAR;
            fontBold = Constants.FONT_DROIDKUFI_BOLD;
            yourTripHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_header));
            lockPackageText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            ticketsCountHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            classTypeHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            tripTypeHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            changeFlightText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            showAllDetailsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            totalPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            totalPriceFare.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            taxesFareText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
            fareBreakUpText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
            signInText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            orTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            continueButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            refundTypeHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            refundTypeDescription.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
            rewardsPointsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.results_view_arabic));
        }
        tf = Typeface.createFromAsset(getAssets(), fontPath);
        titleFace = Typeface.createFromAsset(getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(getAssets(), fontText);
        defaultFace = Typeface.createFromAsset(getAssets(), fontDefault);
        boldFace = Typeface.createFromAsset(getAssets(), fontBold);
        yourTripHeaderText.setTypeface(tf);
        lockPackageText.setTypeface(titleFace);
        ticketsCountHeader.setTypeface(titleFace);
        classTypeHeader.setTypeface(titleFace);
        tripTypeHeader.setTypeface(titleFace);
        changeFlightText.setTypeface(titleFace);
        showAllDetailsText.setTypeface(titleFace);
        totalPrice.setTypeface(boldFace);
        totalPriceFare.setTypeface(boldFace);
        taxesFareText.setTypeface(titleFace);
        fareBreakUpText.setTypeface(titleFace);
        signInText.setTypeface(tf);
        orTextView.setTypeface(titleFace);
        continueButton.setTypeface(tf);
        refundTypeHeader.setTypeface(titleFace);
        refundTypeDescription.setTypeface(titleFace);
        rewardsPointsText.setTypeface(titleFace);
        tripBackgroundImage.setBackgroundColor(Utils.getColor(TripSummaryActivity.this, R.color.transparent_black_color));
        signInLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInDialog = new SignInDialog(TripSummaryActivity.this);
                if (!isSingInClicked) {
                    signInHeaderLayout.setVisibility(View.VISIBLE);
                    SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                    preferences.edit().putString(Constants.MEMBER_EMAIL, "").apply();
                    preferences.edit().putString(Constants.MEMBER_ACCESS_TOKEN, "").apply();
                    preferences.edit().putString(Constants.MEMBER_TOKEN_TYPE, "").apply();
                    preferences.edit().putString(Constants.USER_UNIQUE_ID, "").apply();
                    preferences.edit().putBoolean(Constants.isFanClub, false).apply();
                    preferences.edit().putBoolean(Constants.IS_REWARDS_ENABLED, false).apply();
                    signInDialog.callFacebookLogout();
                    User weUser = WebEngage.get().user();
                    weUser.logout();
                    signInText.setText(getResources().getString(R.string.label_review_sign_in));
                    isSingInClicked = true;
                    continueButton.setText(getString(R.string.continue_as_guiest));
                } else {
                    signInDialog.setCancelable(false);
                    loginDetailsSuccess = 1;
                    signInDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                            String memberAccessToken = preferences.getString(Constants.MEMBER_ACCESS_TOKEN, "");
                            if (!memberAccessToken.equalsIgnoreCase("")) {
                                signInHeaderLayout.setVisibility(View.GONE);
                                isSingInClicked = false;
                                continueButton.setText(getString(R.string.continue_booking));
                                if (!preferences.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
                                    if (preferences.getBoolean(Constants.IS_REWARDS_ENABLED, false)) {
                                        handleEranLoyaltyRequest();
                                    }
                                }
                            } else {
                                signInHeaderLayout.setVisibility(View.VISIBLE);
                                signInText.setText(getResources().getString(R.string.label_review_sign_in));
                                isSingInClicked = true;
                                continueButton.setText(getString(R.string.continue_as_guiest));
                            }
                            loginDetailsSuccess = -1;
                            signInLayout.setClickable(true);
                        }
                    });
                    signInDialog.show();
                    signInLayout.setClickable(false);
                }
            }
        });
        parseJSON(resultData);
        for (int i = 0; i < airItineraryPricingInfoBeanArray.size(); i++) {
            for (int j = 0; j < airItineraryPricingInfoBeanArray.get(i).getPfbdObjectBeanArrayList().size(); j++) {
                pfbdObjBean = airItineraryPricingInfoBeanArray.get(i).getPfbdObjectBeanArrayList().get(j);
                if (!pfbdObjBean.getRefNumber().isEmpty()) {
                    if (pfbdObjBean.getRefNumber().equalsIgnoreCase("1")) {
                        if (pfbdObjBean.getpType().equalsIgnoreCase("ADT")) {
                            if (pfbdObjBean.getPassengerFareObjectBean() != null) {
                                Double pbfValue = Double.valueOf(pfbdObjBean.getPassengerFareObjectBean().getPbf());
                                adultTotalFare = (pbfValue) * (Integer.parseInt(pfbdObjBean.getQty()));
                                if (pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().size() > 1) {
                                    adultTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt()) + (Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(1).getAmt()))));
                                } else {
                                    adultTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt())));
                                }
                            }
                            adultCount = Integer.parseInt(pfbdObjBean.getQty());
                        } else if (pfbdObjBean.getpType().equalsIgnoreCase("CHD")) {
                            if (pfbdObjBean.getPassengerFareObjectBean() != null) {
                                Double pbfValue = Double.valueOf(pfbdObjBean.getPassengerFareObjectBean().getPbf());
                                childTotalFare = (pbfValue) * Integer.parseInt(pfbdObjBean.getQty());
                                if (pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().size() > 1) {
                                    childTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt()) + (Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(1).getAmt()))));
                                } else {
                                    childTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt())));
                                }
                            }
                            childCount = Integer.parseInt(pfbdObjBean.getQty());
                        } else if (pfbdObjBean.getpType().equalsIgnoreCase("INF")) {
                            if (pfbdObjBean.getPassengerFareObjectBean() != null) {
                                Double pbfValue = Double.valueOf(pfbdObjBean.getPassengerFareObjectBean().getPbf());
                                infantTotalFare = pbfValue * Integer.parseInt(pfbdObjBean.getQty());
                                if (pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().size() > 1) {
                                    infantTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt()) + (Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(1).getAmt()))));
                                } else {
                                    infantTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt())));
                                }
                            }
                            infantCount = Integer.parseInt(pfbdObjBean.getQty());
                        }
                    } else if (pfbdObjBean.getRefNumber().equalsIgnoreCase("2")) {
                        if (pfbdObjBean.getpType().equalsIgnoreCase("ADT")) {
                            if (pfbdObjBean.getPassengerFareObjectBean() != null) {
                                Double pbfValue = Double.valueOf(pfbdObjBean.getPassengerFareObjectBean().getPbf());
                                adultTotalFare = adultTotalFare + (pbfValue) * (Integer.parseInt(pfbdObjBean.getQty()));
                                if (pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().size() > 1) {
                                    adultTaxFare = adultTaxFare + Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt()) + (Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(1).getAmt()))));
                                } else {
                                    adultTaxFare = adultTaxFare + Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt())));
                                }
                            }
                            adultCount = Integer.parseInt(pfbdObjBean.getQty());
                        } else if (pfbdObjBean.getpType().equalsIgnoreCase("CHD")) {
                            if (pfbdObjBean.getPassengerFareObjectBean() != null) {
                                Double pbfValue = Double.valueOf(pfbdObjBean.getPassengerFareObjectBean().getPbf());
                                childTotalFare = childTotalFare + (pbfValue) * Integer.parseInt(pfbdObjBean.getQty());
                                if (pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().size() > 1) {
                                    childTaxFare = childTaxFare + Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt()) + (Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(1).getAmt()))));
                                } else {
                                    childTaxFare = childTaxFare + Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt())));
                                }
                            }
                            childCount = Integer.parseInt(pfbdObjBean.getQty());
                        } else if (pfbdObjBean.getpType().equalsIgnoreCase("INF")) {
                            if (pfbdObjBean.getPassengerFareObjectBean() != null) {
                                Double pbfValue = Double.valueOf(pfbdObjBean.getPassengerFareObjectBean().getPbf());
                                infantTotalFare = infantTotalFare + pbfValue * Integer.parseInt(pfbdObjBean.getQty());
                                if (pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().size() > 1) {
                                    infantTaxFare = infantTaxFare + Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt()) + (Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(1).getAmt()))));
                                } else {
                                    infantTaxFare = infantTaxFare + Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt())));
                                }
                            }
                            infantCount = Integer.parseInt(pfbdObjBean.getQty());
                        }
                        Double totalTaxFare = adultTaxFare + childTaxFare + infantTaxFare;
                        Double totalPayPrice = adultTotalFare + childTotalFare + infantTotalFare + totalTaxFare;
                        baseFare = adultTotalFare + childTotalFare + infantTotalFare;
                        int totalCount = adultCount + childCount + infantCount;
                        if (totalCount > 1) {
                            ticketsCountHeader.setText(totalCount + " " + getString(R.string.label_tickets));
                        } else {
                            ticketsCountHeader.setText(totalCount + " " + getString(R.string.label_ticket));
                        }
                        taxesFare = totalTaxFare;
                        totalFare = totalPayPrice;
                        String currencyResult = Utils.getPriceCurrencyCode(TripSummaryActivity.this);
                        if (isArabicLang) {
                            totalPriceFare.setText(Utils.convertPriceToUserSelectionPrice(totalPayPrice, TripSummaryActivity.this) + " " + currencyResult);
                        } else {
                            totalPriceFare.setText(currencyResult + " " + Utils.convertPriceToUserSelectionPrice(totalPayPrice, TripSummaryActivity.this));
                        }
                    }
                } else {
                    if (pfbdObjBean.getpType().equalsIgnoreCase("ADT")) {
                        if (pfbdObjBean.getPassengerFareObjectBean() != null) {
                            Double pbfValue = Double.valueOf(pfbdObjBean.getPassengerFareObjectBean().getPbf());
                            adultTotalFare = (pbfValue) * (Integer.parseInt(pfbdObjBean.getQty()));
                            if (pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().size() > 1) {
                                adultTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt()) + (Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(1).getAmt()))));
                            } else {
                                adultTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt())));
                            }
                        }
                        adultCount = Integer.parseInt(pfbdObjBean.getQty());
                    } else if (pfbdObjBean.getpType().equalsIgnoreCase("CHD")) {
                        if (pfbdObjBean.getPassengerFareObjectBean() != null) {
                            Double pbfValue = Double.valueOf(pfbdObjBean.getPassengerFareObjectBean().getPbf());
                            childTotalFare = (pbfValue) * Integer.parseInt(pfbdObjBean.getQty());
                            if (pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().size() > 1) {
                                childTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt()) + (Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(1).getAmt()))));
                            } else {
                                childTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt())));
                            }
                        }
                        childCount = Integer.parseInt(pfbdObjBean.getQty());
                    } else if (pfbdObjBean.getpType().equalsIgnoreCase("INF")) {
                        if (pfbdObjBean.getPassengerFareObjectBean() != null) {
                            Double pbfValue = Double.valueOf(pfbdObjBean.getPassengerFareObjectBean().getPbf());
                            infantTotalFare = pbfValue * Integer.parseInt(pfbdObjBean.getQty());
                            if (pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().size() > 1) {
                                infantTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt()) + (Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(1).getAmt()))));
                            } else {
                                infantTaxFare = Integer.parseInt(pfbdObjBean.getQty()) * ((Double.parseDouble(pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList().get(0).getAmt())));
                            }
                        }
                        infantCount = Integer.parseInt(pfbdObjBean.getQty());
                    }
                    Double totalTaxFare = adultTaxFare + childTaxFare + infantTaxFare;
                    Double totalPayPrice = adultTotalFare + childTotalFare + infantTotalFare + totalTaxFare;
                    baseFare = adultTotalFare + childTotalFare + infantTotalFare;
                    int totalCount = adultCount + childCount + infantCount;
                    if (totalCount > 1) {
                        ticketsCountHeader.setText(totalCount + " " + getString(R.string.label_tickets));
                    } else {
                        ticketsCountHeader.setText(totalCount + " " + getString(R.string.label_ticket));
                    }
                    taxesFare = totalTaxFare;
                    totalFare = totalPayPrice;
                    String currencyResult = Utils.getPriceCurrencyCode(TripSummaryActivity.this);
                    if (isArabicLang) {
                        totalPriceFare.setText(Utils.convertPriceToUserSelectionPrice(totalPayPrice, TripSummaryActivity.this) + " " + currencyResult);
                    } else {
                        totalPriceFare.setText(currencyResult + " " + Utils.convertPriceToUserSelectionPrice(totalPayPrice, TripSummaryActivity.this));
                    }
                }
            }
        }
        passengerCount = adultCount + childCount + infantCount;
        changeFlightText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = getIntent();
                setResult(Constants.CHANGE_FLIGHT, it);
                finish();
            }
        });
        fareBreakUpText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fareBreakupDialog = new FareBreakupDialog(TripSummaryActivity.this, baseFare, taxesFare, totalFare);
                fareBreakupDialog.setCancelable(true);
                fareBreakupDialog.show();
            }
        });
        if (airlineCodeArr.contains("SV")) {
            airlineCodeName = "SV";
        } else if (airlineCodeArr.contains("XY")) {
            airlineCodeName = "XY";
        }
        String journeyCabinType = "";
        if (journeyClassType.equalsIgnoreCase("Economy")) {
            journeyCabinType = getString(R.string.label_search_flight_economy);
            selectedClass = "Y";
        } else if (journeyClassType.equalsIgnoreCase("Business")) {
            journeyCabinType = getString(R.string.label_search_flight_business);
            selectedClass = "B";
        } else if (journeyClassType.equalsIgnoreCase("First")) {
            journeyCabinType = getString(R.string.label_search_flight_first);
            selectedClass = "F";
        } else {
            journeyCabinType = getString(R.string.label_search_flight_economy);
            selectedClass = "Y";
        }
        classTypeHeader.setText(journeyCabinType);
        handleMerchandiseRequest();
        showAllDetailsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isDataExpanded) {
                    isDataExpanded = true;
                    String lessLabel = getString(R.string.label_show_less_details);
                    SpannableString content = new SpannableString(lessLabel);
                    content.setSpan(new UnderlineSpan(), 0, lessLabel.length(), 0);
                    showAllDetailsText.setText(content);
                    flightInfoLayout.removeAllViews();
                    flightRetrunInfoLayout.removeAllViews();
                    flightMultiTripInfoLayout.removeAllViews();
                    loadMoreData();
                } else {
                    isDataExpanded = false;
                    String allLabel = getString(R.string.label_show_all_details);
                    SpannableString content = new SpannableString(allLabel);
                    content.setSpan(new UnderlineSpan(), 0, allLabel.length(), 0);
                    showAllDetailsText.setText(content);
                    flightInfoLayout.removeAllViews();
                    flightRetrunInfoLayout.removeAllViews();
                    flightMultiTripInfoLayout.removeAllViews();
                    loadLessData();
                }
            }
        });
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TripSummaryActivity.this, FlightBookingPassengerActivity.class);
                intent.putExtra(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                intent.putExtra(Constants.AIPI_JSON_OBJECT, aipiJSONObject);
                intent.putExtra(Constants.AI_JSON_OBJECT, aiJSONObject);
                intent.putExtra(Constants.TDR_VALUE, tdr);
                intent.putExtra(Constants.HOLD_VALUE, hold);
                intent.putExtra(Constants.USCTP_VALUE, usctp);
                intent.putExtra(Constants.BAGGAGE_INFO, baggageDetails);
                intent.putExtra(Constants.CABIN_TYPE, cabinType);
                intent.putStringArrayListExtra(Constants.AIRLINE_NAME, airlineCodeArr);
                intent.putExtra(Constants.ADULT_COUNT, String.valueOf(adultCount));
                intent.putExtra(Constants.CHILD_COUNT, String.valueOf(childCount));
                intent.putExtra(Constants.INFANT_COUNT, String.valueOf(infantCount));
                intent.putExtra(Constants.FORWARD_SOURCE, sourceName);
                intent.putExtra(Constants.FORWARD_DESTINATION, destinationName);
                intent.putExtra(Constants.SECOND_FORWARD_SOURCE, secondSource);
                intent.putExtra(Constants.SECOND_FORWARD_DESTINATION, secondDestination);
                intent.putExtra(Constants.THIRD_FORWARD_SOURCE, thirdSource);
                intent.putExtra(Constants.THIRD_FORWARD_DESTINATION, thirdDestination);
                intent.putExtra(Constants.FORWARD_FLIGHT_DATE, forwardFlightDate);
                intent.putExtra(Constants.RETURN_FLIGHT_DATE, returnFlightDate);
                intent.putExtra(Constants.FIRST_FLIGHT_DATE, firstFlightDate);
                intent.putExtra(Constants.SECOND_FLIGHT_DATE, secondFlightDate);
                intent.putExtra(Constants.THIRD_FLIGHT_DATE, thirdFlightDate);
                intent.putExtra(Constants.TRIP_TYPE, String.valueOf(tripType));
                intent.putExtra(Constants.INFO_SOURCE, String.valueOf(infoSource));
                intent.putExtra(Constants.AIRPORT_NAME_HASHMAP, airportNamesMap);
                intent.putExtra(Constants.AIRLINE_NAME_HASHMAP, airLineNamesMap);
                intent.putExtra(Constants.IS_FARE_COMBINATION, isFareCombinationSelected);
                intent.putExtra(Constants.USER_EARNED_POINTS, userEarnedPoints);
                startActivityForResult(intent, TRAVELLERS_PAGE);
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_flight, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.VISIBLE);
        ImageView mImageView = (ImageView) mCustomView.findViewById(R.id.first_home_icon);
        ImageView mCenterView = (ImageView) mCustomView.findViewById(R.id.first_home_logo);
        TextView mSigninText = (TextView) mCustomView.findViewById(R.id.sing_in_button);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.home_back_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_home_text);
        mSigninText.setVisibility(View.GONE);
        backText.setTypeface(titleFace);
        if (isArabicLang) {
            mImageView.setScaleType(ImageView.ScaleType.FIT_START);
            mCenterView.setImageResource(R.drawable.arabic_one);
            backText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -5, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = getIntent();
                setResult(RESULT_OK, it);
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(titleFace);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter("com.flyin.bookings.ERROR_MESSAGE");
        registerReceiver(myReceiver, intentFilter);
    }

    @Override
    protected void onActivityResult(int reqCode, int resCode, Intent data) {
        if (loginDetailsSuccess == 1) {
            signInDialog.passFacebookResult(reqCode, resCode, data);
        }
        if (reqCode == TRAVELLERS_PAGE) {
            if (resCode == RESULT_OK) {
                if (data != null) {
                    SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                    if (!pref.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
                        if (pref.getBoolean(Constants.IS_REWARDS_ENABLED, false)) {
                            handleEranLoyaltyRequest();
                        }
                    }
                }
            }
        }
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.ERROR_MESSAGE")) {
                displayErrorMessage(intent.getStringExtra("message"));
            }
        }
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        if (errorView.getAlpha() == 0) {
            errorView.setBackgroundResource(R.color.success_message_background);
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private void parseJSON(String data) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(data);
            if (obj.has("fareruleRS")) {
                if (obj.getJSONObject("fareruleRS").has("ai")) {
                    JSONObject aiObject = obj.getJSONObject("fareruleRS").getJSONObject("ai");
                    aiJSONObject = aiObject.toString();
                    if (aiObject.has("odos")) {
                        Constants.airlinecode.clear();
                        if (aiObject.getJSONObject("odos").has("odo")) {
                            JSONArray odoArray = aiObject.getJSONObject("odos").getJSONArray("odo");
                            AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                            aiBean.setDirId(aiObject.getString("dirId"));
                            ArrayList<OriginDestinationOptionBean> odoArrayList = new ArrayList<>();
                            showOdoArrayList = new ArrayList<>();
                            for (int i = 0; i < odoArray.length(); i++) {
                                int count = 0;
                                OriginDestinationOptionBean odoBean = new OriginDestinationOptionBean();
                                ArrayList<FSDataBean> fsDataBeanArrayList = new ArrayList<>();
                                for (int j = 0; j < odoArray.getJSONObject(i).getJSONArray("fs").length(); j++) {
                                    JSONObject fsObj = odoArray.getJSONObject(i).getJSONArray("fs").getJSONObject(j);
                                    FSDataBean fsDataBean = new FSDataBean();
                                    fsDataBean.setDap(fsObj.getString("dap"));
                                    fsDataBean.setDt(fsObj.getString("dt"));
                                    fsDataBean.setAap(fsObj.getString("aap"));
                                    fsDataBean.setAt(fsObj.getString("at"));
                                    fsDataBean.setOal(fsObj.getString("oal"));
                                    fsDataBean.setMal(fsObj.getString("mal"));
                                    fsDataBean.setEq(fsObj.getString("eq"));
                                    fsDataBean.setCt(fsObj.getString("ct"));
                                    fsDataBean.setDur(fsObj.getString("dur"));
                                    fsDataBean.setFdur(fsObj.getString("fdur"));
                                    fsDataBean.setSt(fsObj.getString("st"));
                                    fsDataBean.setSi(fsObj.getString("si"));
                                    fsDataBean.setTi(fsObj.getString("ti"));
                                    fsDataBean.setFi(fsObj.getString("fi"));
                                    fsDataBean.setComment(fsObj.getString("comment"));
                                    fsDataBean.setAs(fsObj.getString("as"));
                                    fsDataBean.setFc(fsObj.getString("fc"));
                                    fsDataBean.setTofi(fsObj.getString("tofi"));
                                    fsDataBean.setFn(fsObj.getString("fn"));
                                    if (fsObj.has("is")) {
                                        fsDataBean.setIs(fsObj.getString("is"));
                                    } else {
                                        fsDataBean.setIs("-1");
                                    }
                                    fsDataBean.setSq(fsObj.getString("sq"));
                                    fsDataBean.setArdt(fsObj.getString("ardt"));
                                    fsDataBean.setDdt(fsObj.getString("ddt"));
                                    journeyClassType = fsObj.getString("ct");
                                    airlineCodeArr.add(fsObj.getString("mal"));
                                    AirportSectorBean bean = new AirportSectorBean();
                                    bean.setAap(fsObj.getString("aap"));
                                    bean.setDap(fsObj.getString("dap"));
                                    bean.setKey(fsObj.getString("mal") + "-" + fsObj.getString("eq"));
                                    flightNamesArray.add(bean);
                                    fsDataBeanArrayList.add(fsDataBean);
                                    odoBean.setStopsCount(odoArray.getJSONObject(i).getJSONArray("fs").length() - 1);
                                    if (count == 0) {
                                        odoBean.setIsLayover(false);
                                        String str = odoArray.getJSONObject(i).getJSONArray("fs").getJSONObject(odoArray.getJSONObject(i).getJSONArray("fs").length() - 1).getString("aap");
                                        odoBean.setHeadingMessage(fsDataBean.getDap() + " - " + str);
                                    } else {
                                        odoBean.setIsLayover(true);
                                    }
                                    count++;
                                }
                                odoBean.setRefNum(odoArray.getJSONObject(i).getString("refNum"));
                                odoBean.setRph(odoArray.getJSONObject(i).getString("rph"));
                                odoBean.setTt(odoArray.getJSONObject(i).getString("tt"));
                                odoBean.setRs(odoArray.getJSONObject(i).getString("rs"));
                                fareTypeArray.add(odoArray.getJSONObject(i).getString("rs"));
                                odoBean.setCb(odoArray.getJSONObject(i).getString("cb"));
                                odoBean.setPt(odoArray.getJSONObject(i).getString("pt"));
                                odoBean.setFsDataBeanArrayList(fsDataBeanArrayList);
                                odoArrayList.add(odoBean);
                            }
                            infoSource = Integer.parseInt(odoArrayList.get(0).getFsDataBeanArrayList().get(0).getIs());
                            showOdoArrayList.addAll(odoArrayList);
                            loadAirportcode(showOdoArrayList);
                            airItineraryArray.add(aiBean);
                        }
                    }
                }
                if (obj.getJSONObject("fareruleRS").has("aipi")) {
                    JSONObject aipiObject = obj.getJSONObject("fareruleRS").getJSONObject("aipi");
                    aipiJSONObject = aipiObject.toString();
                    AirItineraryPricingInfoBean aipiBean = new AirItineraryPricingInfoBean();
                    if (aipiObject.has("itf")) {
                        String itfData = aipiObject.getString("itf");
                        Object json = new JSONTokener(itfData).nextValue();
                        if (json instanceof JSONObject) {
                            isFareCombinationSelected = false;
                            ItinTotalFareObjectBean itfBean = new ItinTotalFareObjectBean();
                            itfBean.setBf(aipiObject.getJSONObject("itf").getString("bf"));
                            itfBean.settTx(aipiObject.getJSONObject("itf").getString("tTx"));
                            itfBean.settSF(aipiObject.getJSONObject("itf").getString("tSF"));
                            itfBean.setDisc(aipiObject.getJSONObject("itf").getString("disc"));
                            itfBean.settFare(aipiObject.getJSONObject("itf").getString("tFare"));
                            itfBean.setIc(aipiObject.getJSONObject("itf").getString("ic"));
                            itfBean.setUsfc(aipiObject.getJSONObject("itf").getString("usfc"));
                            Double price = Double.parseDouble(aipiObject.getJSONObject("itf").getString("tFare"));
                            usctp = Utils.convertPriceToUserSelectionPrice(price, TripSummaryActivity.this);
                            Utils.printMessage(TAG, "Price Value : " + usctp);
                            aipiBean.setItfObject(itfBean);
                        } else if (json instanceof JSONArray) {
                            isFareCombinationSelected = true;
                            ArrayList<ItinTotalFareObjectBean> itfArray = new ArrayList<>();
                            for (int j = 0; j < aipiObject.getJSONArray("itf").length(); j++) {
                                JSONArray itfArr = aipiObject.getJSONArray("itf");
                                ItinTotalFareObjectBean itfBean = new ItinTotalFareObjectBean();
                                itfBean.setBf(itfArr.getJSONObject(j).getString("bf"));
                                itfBean.settTx(itfArr.getJSONObject(j).getString("tTx"));
                                itfBean.settSF(itfArr.getJSONObject(j).getString("tSF"));
                                itfBean.setDisc(itfArr.getJSONObject(j).getString("disc"));
                                itfBean.settFare(itfArr.getJSONObject(j).getString("tFare"));
                                itfBean.setIc(itfArr.getJSONObject(j).getString("ic"));
                                itfBean.setUsfc(itfArr.getJSONObject(j).getString("usfc"));
                                itfArray.add(itfBean);
//                                JSONArray innerJsonArray = itfArr.getJSONArray(j);
//                                JSONObject jsonObject = innerJsonArray.getJSONObject(0);
                                aipiBean.setItfObject(itfBean);
                            }
                            Double price = 0.0;
                            if (itfArray.size() > 1) {
                                price = Double.parseDouble(itfArray.get(0).gettFare()) + Double.parseDouble(itfArray.get(itfArray.size() - 1).gettFare());
                            } else {
                                price = Double.parseDouble(itfArray.get(0).gettFare());
                            }
                            usctp = Utils.convertPriceToUserSelectionPrice(price, TripSummaryActivity.this);
                            Utils.printMessage(TAG, "Price Value : " + usctp);
                        }
                    }
                    aipiBean.setFq(aipiObject.getString("fq"));
                    JSONArray pfbdArray = aipiObject.getJSONObject("pfbds").getJSONArray("pfbd");
                    ArrayList<PFBDObjectBean> pfbdObjectBeanArrayList = new ArrayList<>();
                    for (int i = 0; i < pfbdArray.length(); i++) {
                        PFBDObjectBean pfbdObjectBean = new PFBDObjectBean();
                        if (pfbdArray.getJSONObject(i).has("refNum")) {
                            pfbdObjectBean.setRefNumber(pfbdArray.getJSONObject(i).getString("refNum"));
                        } else {
                            pfbdObjectBean.setRefNumber("");
                        }
                        pfbdObjectBean.setpType(pfbdArray.getJSONObject(i).getString("pType"));
                        pfbdObjectBean.setQty(pfbdArray.getJSONObject(i).getString("qty"));
                        pfbdObjectBean.setUsfcp(pfbdArray.getJSONObject(i).getString("usfcp"));
                        ArrayList<String> fbcArray = new ArrayList<>();
                        if (pfbdArray.getJSONObject(i).has("fbc")) {
                            try {
                                if (!pfbdArray.getJSONObject(i).isNull("fbc")) {
                                    for (int k = 0; k < pfbdArray.getJSONObject(i).getJSONArray("fbc").length(); k++) {
                                        fbcArray.add(pfbdArray.getJSONObject(i).getJSONArray("fbc").getString(k));
                                    }
                                    pfbdObjectBean.setFbcArray(fbcArray);
                                }
                            } catch (Exception e) {
                            }
                        }
                        PassengerFareObjectBean passengerFareObjectBean = null;
                        if (pfbdArray.getJSONObject(i).has("pf")) {
                            if (!pfbdArray.getJSONObject(i).isNull("pf")) {
                                passengerFareObjectBean = new PassengerFareObjectBean();
                                passengerFareObjectBean.setPbf(pfbdArray.getJSONObject(i).getJSONObject("pf").getString("pbf"));
                                passengerFareObjectBean.setPtFare(pfbdArray.getJSONObject(i).getJSONObject("pf").getString("ptFare"));
                                ArrayList<TaxObjectBean> taxObjectBeanArrayList = new ArrayList<>();
                                for (int k = 0; k < pfbdArray.getJSONObject(i).getJSONObject("pf").getJSONArray("txs").length(); k++) {
                                    TaxObjectBean bean = new TaxObjectBean();
                                    bean.setAmt(pfbdArray.getJSONObject(i).getJSONObject("pf").getJSONArray("txs").getJSONObject(k).getString("amt"));
                                    bean.setTc(pfbdArray.getJSONObject(i).getJSONObject("pf").getJSONArray("txs").getJSONObject(k).getString("tc"));
                                    taxObjectBeanArrayList.add(bean);
                                }
                                passengerFareObjectBean.setTaxObjectBeanArrayList(taxObjectBeanArrayList);
                            }
                        }
                        pfbdObjectBean.setPassengerFareObjectBean(passengerFareObjectBean);
                        pfbdObjectBeanArrayList.add(pfbdObjectBean);
                    }
                    aipiBean.setPfbdObjectBeanArrayList(pfbdObjectBeanArrayList);
                    airItineraryPricingInfoBeanArray.add(aipiBean);
                }
                if (obj.getJSONObject("fareruleRS").has("tdr")) {
                    tdr = obj.getJSONObject("fareruleRS").getString("tdr");
                }
                if (obj.getJSONObject("fareruleRS").has("hold")) {
                    hold = obj.getJSONObject("fareruleRS").getString("hold");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utils.printMessage(TAG, "Exception is .. " + e.getMessage());
        }
        if (fareTypeArray.contains("Refundable") || fareTypeArray.contains("Refundable Before Departure")) {
            refundPolicyLayout.setVisibility(View.VISIBLE);
        } else {
            refundPolicyLayout.setVisibility(View.GONE);
        }
    }

    private void loadLessData() {
        for (int i = 0; i < showOdoArrayList.size(); i++) {
            setForwardFlightDetailsInfo(showOdoArrayList.get(i), (i + 1));
        }
        if (showOdoArrayList.size() == 1) {
            if (isFirstNonStopFlight) {
                showAllDetailsText.setVisibility(View.GONE);
            }
        } else if (showOdoArrayList.size() == 2) {
            if (isFirstNonStopFlight && isSecondNonStopFlight) {
                showAllDetailsText.setVisibility(View.GONE);
            }
        } else {
            if (isFirstNonStopFlight && isSecondNonStopFlight && isThirdNonStopFlight) {
                showAllDetailsText.setVisibility(View.GONE);
            }
        }
    }

    private void setForwardFlightDetailsInfo(final OriginDestinationOptionBean data, int loopCount) {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_trip_confirmation, null);
        if (loopCount == 1) {
            flightInfoLayout.addView(view);
        } else if (loopCount == 2) {
            flightRetrunInfoLayout.setVisibility(View.VISIBLE);
            flightRetrunInfoLayout.addView(view);
        } else if (loopCount == 3) {
            flightMultiTripInfoLayout.setVisibility(View.VISIBLE);
            flightMultiTripInfoLayout.addView(view);
        }
        LinearLayout odoHeaderLayout = (LinearLayout) view.findViewById(R.id.odo_header_layout);
        TextView departHeaderText = (TextView) view.findViewById(R.id.departure_text);
        TextView departStops = (TextView) view.findViewById(R.id.departure_stops);
        ImageView airlineImage = (ImageView) view.findViewById(R.id.airline_image);
        TextView departFrom = (TextView) view.findViewById(R.id.departure_from);
        TextView departLocation = (TextView) view.findViewById(R.id.departure_location);
        TextView departTerminal = (TextView) view.findViewById(R.id.departure_terminal);
        TextView journeyAirlineName = (TextView) view.findViewById(R.id.journey_airline_name);
        TextView journeyAirlineDetails = (TextView) view.findViewById(R.id.journey_airline_details);
        TextView arrivalTo = (TextView) view.findViewById(R.id.arrival_to);
        TextView arrivalLocation = (TextView) view.findViewById(R.id.arrival_location);
        TextView arrivalTerminal = (TextView) view.findViewById(R.id.arrival_terminal);
        TextView flightStops = (TextView) view.findViewById(R.id.flight_stops);
        TextView flightDuration = (TextView) view.findViewById(R.id.flight_duration);
        TextView flightRefundStatus = (TextView) view.findViewById(R.id.flight_refund_status);
        ImageView busIcon = (ImageView) view.findViewById(R.id.depart_bus_image);
        ImageView returnBusIcon = (ImageView) view.findViewById(R.id.return_bus_image);
        TextView classType = (TextView) view.findViewById(R.id.class_type);
        TextView baggageInfo = (TextView) view.findViewById(R.id.baggage_information);
        RelativeLayout stopNamesLayout = (RelativeLayout) view.findViewById(R.id.stops_names_layout);
        RelativeLayout stopsLayout = (RelativeLayout) view.findViewById(R.id.stops_layout);
        TextView departLayoverTime = (TextView) view.findViewById(R.id.layover_time);
        LinearLayout imageScrollLayout = (LinearLayout) view.findViewById(R.id.image_scroll_layout);
        LinearLayout firstDepartFromLayout = (LinearLayout) view.findViewById(R.id.departure_from_layout);
        LinearLayout firstDepartToLayout = (LinearLayout) view.findViewById(R.id.return_to_layout);
        View stopsEmptyView = view.findViewById(R.id.stops_empty_view);
        LinearLayout merchandiseDataLayout = (LinearLayout) view.findViewById(R.id.flight_less_info_layout);
        ImageView layoutIcon = (ImageView) view.findViewById(R.id.layout_icon);
        TextView seatLayoutText = (TextView) view.findViewById(R.id.layout_text);
        ImageView wifiIcon = (ImageView) view.findViewById(R.id.wifi_icon);
        TextView wifiText = (TextView) view.findViewById(R.id.wifi_text);
        ImageView avodIcon = (ImageView) view.findViewById(R.id.avod_icon);
        TextView avodText = (TextView) view.findViewById(R.id.avod_text);
        ImageView freshMealIcon = (ImageView) view.findViewById(R.id.fresh_meal_icon);
        TextView freshMealText = (TextView) view.findViewById(R.id.fresh_meal_text);
        LinearLayout viewMoreLayout = (LinearLayout) view.findViewById(R.id.view_more_layout);
        final TextView viewMoreText = (TextView) view.findViewById(R.id.view_more_text);
        TextView merchandiseTextData = (TextView) view.findViewById(R.id.merchandise_text_data);
        final LinearLayout defaultAmenitiesLayout = (LinearLayout) view.findViewById(R.id.default_amenities_layout);
        final LinearLayout flightAmenitiesLayout = (LinearLayout) view.findViewById(R.id.flight_amenities_layout);
        TextView flightAmenitiesText = (TextView) view.findViewById(R.id.flight_amenities_text);
        LinearLayout viewLessLayout = (LinearLayout) view.findViewById(R.id.view_less_layout);
        LinearLayout baggageInfoLayout = (LinearLayout) view.findViewById(R.id.baggage_layout);
        if (isArabicLang) {
            departHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            departStops.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            departFrom.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            departLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
            departTerminal.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
            journeyAirlineName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            journeyAirlineDetails.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            arrivalTo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            arrivalLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
            arrivalTerminal.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
            flightStops.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            flightDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            flightRefundStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            classType.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            baggageInfo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            departLayoverTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            seatLayoutText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_airline_name_text));
            wifiText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_airline_name_text));
            avodText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_airline_name_text));
            freshMealText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_airline_name_text));
            viewMoreText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_airline_name_text));
            merchandiseTextData.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
            flightAmenitiesText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            flightDuration.setGravity(Gravity.END);
            flightStops.setGravity(Gravity.END);
            merchandiseTextData.setGravity(Gravity.START);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                merchandiseTextData.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            flightStops.setLayoutParams(params);
            params.setMargins(0, -5, 0, 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                flightDuration.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                flightStops.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            }
        }
        departHeaderText.setTypeface(titleFace);
        departStops.setTypeface(titleFace);
        departFrom.setTypeface(titleFace);
        departLocation.setTypeface(textFace);
        departTerminal.setTypeface(textFace);
        journeyAirlineName.setTypeface(titleFace);
        journeyAirlineDetails.setTypeface(titleFace);
        arrivalTo.setTypeface(titleFace);
        arrivalLocation.setTypeface(textFace);
        arrivalTerminal.setTypeface(textFace);
        flightStops.setTypeface(titleFace);
        flightDuration.setTypeface(titleFace);
        flightRefundStatus.setTypeface(titleFace);
        classType.setTypeface(titleFace);
        baggageInfo.setTypeface(textFace);
        departLayoverTime.setTypeface(textFace);
        seatLayoutText.setTypeface(boldFace);
        wifiText.setTypeface(boldFace);
        avodText.setTypeface(boldFace);
        freshMealText.setTypeface(boldFace);
        viewMoreText.setTypeface(titleFace);
        merchandiseTextData.setTypeface(defaultFace);
        flightAmenitiesText.setTypeface(tf);
        getairlinecodeList(data.getFsDataBeanArrayList());
        odoHeaderLayout.setVisibility(View.VISIBLE);
        if (data.getFsDataBeanArrayList().get(0).getEq().equalsIgnoreCase("BUS")) {
            busIcon.setVisibility(View.VISIBLE);
        }
        if (data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getEq().equalsIgnoreCase("BUS")) {
            returnBusIcon.setVisibility(View.VISIBLE);
        }
        String departureDate = "";
        if (isArabicLang) {
            departureDate = Utils.formatTimeStampToDate(data.getFsDataBeanArrayList().get(0).getDdt(), TripSummaryActivity.this);
        } else {
            departureDate = Utils.formatDateToDay(data.getFsDataBeanArrayList().get(0).getDdt(), TripSummaryActivity.this);
        }
        departStops.setText(departureDate);
        String departureAirportName = airportNamesMap.get(data.getFsDataBeanArrayList().get(0).getDap()).getAirpotName();
        String arrivalAirportName = airportNamesMap.get(data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getAap()).getAirpotName();
        String journeyAirline = airLineNamesMap.get(data.getFsDataBeanArrayList().get(0).getMal());
        String imagePath = Constants.IMAGE_BASE_URL + data.getFsDataBeanArrayList().get(0).getMal() + Constants.IMAGE_FILE_FORMAT;
        Glide.with(TripSummaryActivity.this).load(imagePath).placeholder(R.drawable.imagethumb_search).into(airlineImage);
        journeyAirlineName.setText(journeyAirline);
        String cabinName = data.getFsDataBeanArrayList().get(0).getCt();
        String journeyCabinType = "";
        if (cabinName.equalsIgnoreCase("Economy")) {
            journeyCabinType = getString(R.string.label_search_flight_economy);
        } else if (cabinName.equalsIgnoreCase("Business")) {
            journeyCabinType = getString(R.string.label_search_flight_business);
        } else if (cabinName.equalsIgnoreCase("First")) {
            journeyCabinType = getString(R.string.label_search_flight_first);
        }
        classType.setText(getString(R.string.label_cabin_class) + ":");
        classType.append(Utils.addDurationField(journeyCabinType));
        journeyAirlineDetails.setText(data.getFsDataBeanArrayList().get(0).getMal() + " " + data.getFsDataBeanArrayList().get(0).getFn());
        String airportCity = airportNamesMap.get(data.getFsDataBeanArrayList().get(0).getDap()).getCityName();
        String departureTime = Utils.formatDateToTime(data.getFsDataBeanArrayList().get(0).getDdt(), TripSummaryActivity.this);
        departFrom.setText(departureTime + ", " + airportCity);
        departFrom.append(Utils.addCountryCodeField(data.getFsDataBeanArrayList().get(0).getDap()));
        departLocation.setText(departureAirportName);
        String airportCity1 = airportNamesMap.get(data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getAap()).getCityName();
        if (destinationNameText.isEmpty()) {
            destinationNameText = airportNamesMap.get(data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getAap()).getCityName();
            yourTripHeaderText.setText(getString(R.string.label_your_trip_to) + " " + destinationNameText);
        }
        String arrivalCityTime = Utils.formattimeToString(data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getArdt(), TripSummaryActivity.this);
        arrivalTo.setText(arrivalCityTime + ", " + airportCity1);
        arrivalTo.append(Utils.addCountryCodeField(data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getAap()));
        arrivalLocation.setText(arrivalAirportName);
        String message = "";
        if (data.getFsDataBeanArrayList().get(0).getSq().equalsIgnoreCase("0")) {
            android.widget.LinearLayout.LayoutParams stopsParam = new android.widget.LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
            firstDepartFromLayout.setLayoutParams(stopsParam);
            firstDepartToLayout.setLayoutParams(stopsParam);
            message = getString(R.string.label_non_stop_text) + " ";
            if (loopCount == 1) {
                isFirstNonStopFlight = true;
            } else if (loopCount == 2) {
                isSecondNonStopFlight = true;
            } else {
                isThirdNonStopFlight = true;
            }
        } else if (data.getFsDataBeanArrayList().get(0).getSq().equalsIgnoreCase("1")) {
            android.widget.LinearLayout.LayoutParams stopsParam = new android.widget.LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.28f);
            firstDepartFromLayout.setLayoutParams(stopsParam);
            android.widget.LinearLayout.LayoutParams stopsViewParam = new android.widget.LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.44f);
            stopsEmptyView.setLayoutParams(stopsViewParam);
            android.widget.LinearLayout.LayoutParams returnStopsParam = new android.widget.LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.28f);
            firstDepartToLayout.setLayoutParams(returnStopsParam);
            message = data.getFsDataBeanArrayList().get(0).getSq() + " " + getString(R.string.label_stop_text) + " ";
        } else {
            message = data.getFsDataBeanArrayList().get(0).getSq() + " " + getString(R.string.label_stops_text) + " ";
        }
        flightStops.setText(message);
        if (data.getFsDataBeanArrayList().get(0).getDt().equalsIgnoreCase("null")) {
            departTerminal.setVisibility(View.GONE);
        } else {
            departTerminal.setVisibility(View.VISIBLE);
            departTerminal.setText(getString(R.string.label_terminal) + " " + data.getFsDataBeanArrayList().get(0).getDt());
        }
        if (data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getAt().equalsIgnoreCase("null")) {
            arrivalTerminal.setVisibility(View.GONE);
        } else {
            arrivalTerminal.setVisibility(View.VISIBLE);
            arrivalTerminal.setText(getString(R.string.label_terminal) + " " + data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getAt());
        }
        flightDuration.setText(getString(R.string.label_duration_text) + ":");
        flightDuration.append(Utils.addDurationField(Utils.formatDurationToString(data.getFsDataBeanArrayList().get(0).getDur(), TripSummaryActivity.this)));
        if (data.getRs().equalsIgnoreCase("Refundable") || data.getRs().equalsIgnoreCase("Refundable Before Departure")) {
            flightRefundStatus.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.success_message_background));
            flightRefundStatus.setText(" " + getString(R.string.RefLbl));
        } else {
            flightRefundStatus.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.review_non_refund_color));
            flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
        }
        if (data.getTt().equalsIgnoreCase("1")) {
            tripTypeHeader.setText(getString(R.string.label_search_oneway));
            if (data.getRefNum().equalsIgnoreCase("1")) {
                departHeaderText.setText(R.string.label_search_flight_departure);
            }
        }
        if (data.getTt().equalsIgnoreCase("2")) {
            tripTypeHeader.setText(getString(R.string.label_search_roundtrip));
            if (data.getRefNum().equalsIgnoreCase("1")) {
                departHeaderText.setText(R.string.label_search_flight_departure);
            }
            if (data.getRefNum().equalsIgnoreCase("2")) {
                departHeaderText.setText(R.string.label_search_flight_return);
            }
        }
        if (data.getTt().equalsIgnoreCase("3")) {
            tripTypeHeader.setText(getString(R.string.label_search_multicity));
            if (data.getRefNum().equalsIgnoreCase("1")) {
                departHeaderText.setText(R.string.label_search_flight_departure);
            }
            if (data.getRefNum().equalsIgnoreCase("2")) {
                departHeaderText.setText(R.string.label_search_flight_departure);
            }
            if (data.getRefNum().equalsIgnoreCase("3")) {
                departHeaderText.setText(R.string.label_search_flight_departure);
            }
        }
        String checkInDetails = Utils.checkStringVal(data.getCb());
        if (!checkInDetails.equalsIgnoreCase("")) {
            String baggageInformation = "";
            String[] baggageDetails = checkInDetails.split(" ");
            if (baggageDetails[1].equalsIgnoreCase("Kilos")) {
                baggageInformation = baggageDetails[0] + " " + getString(R.string.label_kilo);
            } else if (baggageDetails[1].equalsIgnoreCase("Pieces")) {
                baggageInformation = baggageDetails[0] + " " + getString(R.string.label_pieces);
            } else {
                baggageInformation = checkInDetails;
            }
            if (isArabicLang) {
                baggageInfo.setText(getString(R.string.label_reviewPage_baggageInfo) + " '" + baggageInformation + "' (" + getString(R.string.label_per_person_text) + ")");
            } else {
                baggageInfo.setText(getString(R.string.label_reviewPage_baggageInfo) + " " + getString(R.string.label_reviewPage_checkIn) + " '" + baggageInformation + "' (" + getString(R.string.label_per_person_text) + ")");
            }
        } else {
            baggageInfoLayout.setVisibility(View.GONE);
        }
        cabinType = data.getFsDataBeanArrayList().get(0).getCt();
        String firstFlightName = data.getFsDataBeanArrayList().get(0).getMal() + "-" + data.getFsDataBeanArrayList().get(0).getEq();
        String secondFlightName = "";
        int stopQuantity = Integer.parseInt(data.getFsDataBeanArrayList().get(0).getSq());
        if (stopQuantity > 0) {
            secondFlightName = data.getFsDataBeanArrayList().get(1).getMal() + "-" + data.getFsDataBeanArrayList().get(1).getEq();
        }
        Utils.printMessage(TAG, "SIZE::" + mainMarchArrayList.size());
//        if(mainMarchArrayList.size() > 0){
//            closeLoading();
//        }
        int flightNameCount = 0;
        for (int i = 0; i < mainMarchArrayList.size(); i++) {
            if (flightNameCount == 1) {
                secondFlightName = "";
            }
            if (firstFlightName.contains(mainMarchArrayList.get(i).getFlightName()) || secondFlightName.contains(mainMarchArrayList.get(i).getFlightName())) {
                iconNameArrayList = new ArrayList<>();
                imageScrollLayout.removeAllViews();
                merchandiseDataLayout.setVisibility(View.VISIBLE);
                String aircraftVal[] = mainMarchArrayList.get(i).getFlightName().split("-");
                String aircraftType = Utils.getAircraftType(aircraftVal[1], TripSummaryActivity.this);
                if (!mainMarchArrayList.get(i).getBodyType().isEmpty()) {
                    for (Map.Entry<Integer, AmenitiesBean> entry : AmenitiesConstants.bodyTypeHashMap.entrySet()) {
                        if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                            if (entry.getValue().getAr().equals(mainMarchArrayList.get(i).getBodyType())) {
                                appendBodyTypeValue(AmenitiesConstants.bodyTypeHashMap, entry.getKey(), aircraftType);
                            }
                        } else {
                            if (entry.getValue().getEn().equals(mainMarchArrayList.get(i).getBodyType())) {
                                appendBodyTypeValue(AmenitiesConstants.bodyTypeHashMap, entry.getKey(), aircraftType);
                            }
                        }
                    }
                }
                if (!mainMarchArrayList.get(i).getSeatType().isEmpty()) {
                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.seatTypeHashMap.entrySet()) {
                        String moreData = "";
                        String seatWidthText = "";
                        String seatLengthText = "";
                        String seatPitchText = "";
                        if (!mainMarchArrayList.get(i).getSeatWidth().isEmpty() && !mainMarchArrayList.get(i).getSeatLength().isEmpty() && !mainMarchArrayList.get(i).getSeatPitch().isEmpty()) {
                            seatWidthText = getString(R.string.label_seat_width) + " " + mainMarchArrayList.get(i).getSeatWidth();
                            seatLengthText = getString(R.string.label_leg_room) + " " + mainMarchArrayList.get(i).getSeatLength();
                            seatPitchText = getString(R.string.label_pitch_size) + " " + mainMarchArrayList.get(i).getSeatPitch();
                            moreData = seatWidthText + ", " + seatLengthText + ", " + seatPitchText;
                        } else if (!mainMarchArrayList.get(i).getSeatWidth().isEmpty() && !mainMarchArrayList.get(i).getSeatPitch().isEmpty()) {
                            seatWidthText = getString(R.string.label_seat_width) + " " + mainMarchArrayList.get(i).getSeatWidth();
                            seatPitchText = getString(R.string.label_pitch_size) + " " + mainMarchArrayList.get(i).getSeatPitch();
                            moreData = seatWidthText + ", " + seatPitchText;
                        }
                        if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                            if (entry.getValue().getAr().equals(mainMarchArrayList.get(i).getSeatType())) {
                                appendMerchandiseValues(AmenitiesConstants.seatTypeHashMap, entry.getKey(), moreData);
                            }
                        } else {
                            if (entry.getValue().getEn().equals(mainMarchArrayList.get(i).getSeatType())) {
                                appendMerchandiseValues(AmenitiesConstants.seatTypeHashMap, entry.getKey(), moreData);
                            }
                        }
                    }
                }
                if (!mainMarchArrayList.get(i).getSeatLayout().isEmpty()) {
                    layoutIcon.setImageResource(R.drawable.seat_layout);
                    seatLayoutText.setText(mainMarchArrayList.get(i).getSeatLayout());
                    seatLayoutText.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.result_compo_duration));
                    iconNameArrayList.add("seat_layout==" + getString(R.string.label_seat_layout) + " " + mainMarchArrayList.get(i).getSeatLayout());
                }
                if (mainMarchArrayList.get(i).getSeatLayout().isEmpty()) {
                    layoutIcon.setVisibility(View.GONE);
                    seatLayoutText.setVisibility(View.GONE);
                }
                if (!mainMarchArrayList.get(i).getMeals().isEmpty()) {
                    freshMealIcon.setImageResource(R.drawable.fresh_meal);
                    freshMealText.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.result_compo_duration));
                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.mealsHashMap.entrySet()) {
                        if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                            if (entry.getValue().getAr().equals(mainMarchArrayList.get(i).getMeals())) {
                                appendMerchandiseValues(AmenitiesConstants.mealsHashMap, entry.getKey(), "");
                            }
                        } else {
                            if (entry.getValue().getEn().equals(mainMarchArrayList.get(i).getMeals())) {
                                appendMerchandiseValues(AmenitiesConstants.mealsHashMap, entry.getKey(), "");
                            }
                        }
                    }
                }
                if (!mainMarchArrayList.get(i).getWifi().isEmpty()) {
                    wifiIcon.setImageResource(R.drawable.wifi);
                    wifiText.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.result_compo_duration));
                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.wifiHashMap.entrySet()) {
                        if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                            if (entry.getValue().getAr().equals(mainMarchArrayList.get(i).getWifi())) {
                                appendMerchandiseValues(AmenitiesConstants.wifiHashMap, entry.getKey(), "");
                            }
                        } else {
                            if (entry.getValue().getEn().equals(mainMarchArrayList.get(i).getWifi())) {
                                appendMerchandiseValues(AmenitiesConstants.wifiHashMap, entry.getKey(), "");
                            }
                        }
                    }
                }
                if (!mainMarchArrayList.get(i).getAvod().isEmpty()) {
                    if (mainMarchArrayList.get(i).getAvod().equalsIgnoreCase("Yes")) {
                        avodIcon.setImageResource(R.drawable.avod);
                        avodText.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.result_compo_duration));
                        if (!mainMarchArrayList.get(i).getVideoType().isEmpty()) {
                            for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.avodVideoHashMap.entrySet()) {
                                String screenSize = "";
                                if (!mainMarchArrayList.get(i).getAvodScreenSize().isEmpty()) {
                                    screenSize = mainMarchArrayList.get(i).getAvodScreenSize();
                                }
                                if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                                    if (entry.getValue().getAr().equals(mainMarchArrayList.get(i).getVideoType())) {
                                        appendMerchandiseValues(AmenitiesConstants.avodVideoHashMap, entry.getKey(), screenSize);
                                    }
                                } else {
                                    if (entry.getValue().getEn().equals(mainMarchArrayList.get(i).getVideoType())) {
                                        appendMerchandiseValues(AmenitiesConstants.avodVideoHashMap, entry.getKey(), screenSize);
                                    }
                                }
                            }
                        } else {
                            iconNameArrayList.add("avod==" + getString(R.string.label_avod));
                        }
                    }
                    if (mainMarchArrayList.get(i).getAvod().equalsIgnoreCase("Charge")) {
                        avodIcon.setImageResource(R.drawable.avod);
                        avodText.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.result_compo_duration));
                        if (!mainMarchArrayList.get(i).getVideoType().isEmpty()) {
                            for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.avodChargeVideoHashMap.entrySet()) {
                                String screenSize = "";
                                if (!mainMarchArrayList.get(i).getAvodScreenSize().isEmpty()) {
                                    screenSize = mainMarchArrayList.get(i).getAvodScreenSize();
                                }
                                if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                                    if (entry.getValue().getAr().equals(mainMarchArrayList.get(i).getVideoType())) {
                                        appendMerchandiseValues(AmenitiesConstants.avodChargeVideoHashMap, entry.getKey(), screenSize);
                                    }
                                } else {
                                    if (entry.getValue().getEn().equals(mainMarchArrayList.get(i).getVideoType())) {
                                        appendMerchandiseValues(AmenitiesConstants.avodChargeVideoHashMap, entry.getKey(), screenSize);
                                    }
                                }
                            }
                        } else {
                            iconNameArrayList.add("avod==" + getString(R.string.label_avod) + " (" + getString(R.string.label_charge_text) + ")");
                        }
                    }
                }
                if (!mainMarchArrayList.get(i).getPowerType().isEmpty()) {
                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.powerTypeHashMap.entrySet()) {
                        if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                            if (entry.getValue().getAr().equals(mainMarchArrayList.get(i).getPowerType())) {
                                appendMerchandiseValues(AmenitiesConstants.powerTypeHashMap, entry.getKey(), "");
                            }
                        } else {
                            if (entry.getValue().getEn().equals(mainMarchArrayList.get(i).getPowerType())) {
                                appendMerchandiseValues(AmenitiesConstants.powerTypeHashMap, entry.getKey(), "");
                            }
                        }
                    }
                }
                if (!mainMarchArrayList.get(i).getMobile().isEmpty()) {
                    appendMerchandiseValues(AmenitiesConstants.mobileHashMap, mainMarchArrayList.get(i).getMobile(), "");
                }
                if (!mainMarchArrayList.get(i).getInfantCare().isEmpty()) {
                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.infantCareHashMap.entrySet()) {
                        if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                            if (entry.getValue().getAr().equals(mainMarchArrayList.get(i).getInfantCare())) {
                                appendMerchandiseValues(AmenitiesConstants.infantCareHashMap, entry.getKey(), "");
                            }
                        } else {
                            if (entry.getValue().getEn().equals(mainMarchArrayList.get(i).getInfantCare())) {
                                appendMerchandiseValues(AmenitiesConstants.infantCareHashMap, entry.getKey(), "");
                            }
                        }
                    }
                }
                if (!mainMarchArrayList.get(i).getAlocohol().isEmpty()) {
                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.alocoholHashMap.entrySet()) {
                        if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                            if (entry.getValue().getAr().equals(mainMarchArrayList.get(i).getAlocohol())) {
                                appendMerchandiseValues(AmenitiesConstants.alocoholHashMap, entry.getKey(), "");
                            }
                        } else {
                            if (entry.getValue().getEn().equals(mainMarchArrayList.get(i).getAlocohol())) {
                                appendMerchandiseValues(AmenitiesConstants.alocoholHashMap, entry.getKey(), "");
                            }
                        }
                    }
                }
                if (!mainMarchArrayList.get(i).getPrayerArea().isEmpty()) {
                    appendMerchandiseValues(AmenitiesConstants.prayerAreaHashMap, mainMarchArrayList.get(i).getPrayerArea(), "");
                }
                if (!mainMarchArrayList.get(i).getShowerArea().isEmpty()) {
                    appendMerchandiseValues(AmenitiesConstants.showerAreaHashMap, mainMarchArrayList.get(i).getShowerArea(), "");
                }
                if (!mainMarchArrayList.get(i).getAircraftImage().isEmpty()) {
                    View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                    ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                    TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                    int imageScaledWidth = getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                    int imageScaledHeight = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                    String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                            Constants.IMAGE_QUALITY + "" + mainMarchArrayList.get(i).getAircraftImage();
                    Glide.with(TripSummaryActivity.this).load(url).placeholder(R.drawable.imagethumb_search)
                            .bitmapTransform(new RoundImageTransform(TripSummaryActivity.this, 0, 0)).into(flightMerchandiseImage);
                    imageDescriptionText.setText(getString(R.string.label_aircraft));
                    imageDescriptionText.setTypeface(titleFace);
                    imageScrollLayout.addView(scrollImgView);
                }
                if (!mainMarchArrayList.get(i).getClassOverviewImage().isEmpty()) {
                    View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                    ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                    TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                    int imageScaledWidth = getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                    int imageScaledHeight = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                    String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                            Constants.IMAGE_QUALITY + "" + mainMarchArrayList.get(i).getClassOverviewImage();
                    Glide.with(TripSummaryActivity.this).load(url).placeholder(R.drawable.imagethumb_search)
                            .bitmapTransform(new RoundImageTransform(TripSummaryActivity.this, 0, 0)).into(flightMerchandiseImage);
                    imageDescriptionText.setText(getString(R.string.label_class_overview));
                    imageDescriptionText.setTypeface(titleFace);
                    imageScrollLayout.addView(scrollImgView);
                }
                if (!mainMarchArrayList.get(i).getSeatImage().isEmpty()) {
                    View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                    ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                    TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                    int imageScaledWidth = getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                    int imageScaledHeight = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                    String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                            Constants.IMAGE_QUALITY + "" + mainMarchArrayList.get(i).getSeatImage();
                    Glide.with(TripSummaryActivity.this).load(url).placeholder(R.drawable.imagethumb_search)
                            .bitmapTransform(new RoundImageTransform(TripSummaryActivity.this, 0, 0)).into(flightMerchandiseImage);
                    imageDescriptionText.setText(getString(R.string.label_seat_image));
                    imageDescriptionText.setTypeface(titleFace);
                    imageScrollLayout.addView(scrollImgView);
                }
                if (!mainMarchArrayList.get(i).getFacilityImage().isEmpty()) {
                    View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                    ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                    TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                    int imageScaledWidth = getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                    int imageScaledHeight = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                    String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                            Constants.IMAGE_QUALITY + "" + mainMarchArrayList.get(i).getFacilityImage();
                    Glide.with(TripSummaryActivity.this).load(url).placeholder(R.drawable.imagethumb_search)
                            .bitmapTransform(new RoundImageTransform(TripSummaryActivity.this, 0, 0)).into(flightMerchandiseImage);
                    imageDescriptionText.setText(getString(R.string.label_facility_image));
                    imageDescriptionText.setTypeface(titleFace);
                    imageScrollLayout.addView(scrollImgView);
                }
                if (!mainMarchArrayList.get(i).getRemarks().isEmpty()) {
                    iconNameArrayList.add("==" + mainMarchArrayList.get(i).getRemarks());
                }
                if (!mainMarchArrayList.get(i).getProvideTransportation().isEmpty()) {
                    iconNameArrayList.add("==" + getString(R.string.label_provide_transportation));
                }
                if (!mainMarchArrayList.get(i).getProvideOtherInfo().isEmpty()) {
                    iconNameArrayList.add("==" + mainMarchArrayList.get(i).getProvideOtherInfo());
                }
                addNameIconsToText(merchandiseTextData, loopCount);
            }
            flightNameCount++;
        }
        viewMoreLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flightAmenitiesLayout.setVisibility(View.VISIBLE);
                defaultAmenitiesLayout.setVisibility(View.GONE);
            }
        });
        viewLessLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flightAmenitiesLayout.setVisibility(View.GONE);
                defaultAmenitiesLayout.setVisibility(View.VISIBLE);
            }
        });
        ArrayList<FSDataBean> fsBeanArrayList = new ArrayList<>();
        int departStopQuantity = Integer.parseInt(data.getFsDataBeanArrayList().get(0).getSq());
        fsBeanArrayList.addAll(data.getFsDataBeanArrayList());
        addStops((departStopQuantity + 1), fsBeanArrayList, stopNamesLayout, stopsLayout, departLayoverTime);
    }

    @Override
    public void onBackPressed() {
        Intent it = getIntent();
        setResult(RESULT_OK, it);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        final SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        final String memberAccessToken = preferences.getString(Constants.MEMBER_ACCESS_TOKEN, "");
        if (!memberAccessToken.equalsIgnoreCase("")) {
            signInHeaderLayout.setVisibility(View.GONE);
            isSingInClicked = false;
            continueButton.setText(getString(R.string.continue_booking));
        } else if (memberAccessToken.equalsIgnoreCase("")) {
            signInHeaderLayout.setVisibility(View.VISIBLE);
            signInText.setText(getString(R.string.label_review_sign_in));
            isSingInClicked = true;
            continueButton.setText(getString(R.string.continue_as_guiest));
            rewardsLayout.setVisibility(View.GONE);
        }
    }

    private void getairlinecodeList(ArrayList<FSDataBean> fsDataBeanArrayList) {
        for (int i = 0; i < fsDataBeanArrayList.size(); i++) {
            String airline_code = fsDataBeanArrayList.get(i).getMal();
            if (Constants.airlinecode.size() == 0) {
                Constants.airlinecode.add(0, airline_code);
            }
            for (int j = 0; j < Constants.airlinecode.size(); j++) {
                if (airline_code.equals(Constants.airlinecode.get(j))) {
                    break;
                }
                if (j == Constants.airlinecode.size() - 1) {
                    Constants.airlinecode.add(Constants.airlinecode.size(), airline_code);
                }
            }
        }
    }

    private void addStops(int count, ArrayList<FSDataBean> fsBeanArrayList, RelativeLayout stopNamesLayout,
                          RelativeLayout stopsLayout, TextView layoverTime) {
        Display device = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int deviceWidth = device.getWidth();
        int splitWidth = deviceWidth - getResources().getDimensionPixelSize(R.dimen.flag_margin);
        int cellSpaceWidth = splitWidth / count;
        int nameSpaceWidth = getResources().getDimensionPixelSize(R.dimen.signInPage_view_gap) / count;
        if (cellSpaceWidth != 0 && nameSpaceWidth != 0) {
            int imageScaledSize = getResources().getDimensionPixelSize(R.dimen.padding_size);
            String departPreviousStop = "";
            String departPreviousTime = "";
            if (count > 1) {
                int stopCount = 0;
                for (int i = 1; i < count; i++) {
                    layoverTime.setVisibility(View.VISIBLE);
                    departPreviousStop = fsBeanArrayList.get(stopCount).getAap();
                    int xVal = ((int) (cellSpaceWidth * i) - imageScaledSize / 2);
                    int yVal = getResources().getDimensionPixelSize(R.dimen.contact_email_margin);
                    int stopXVal = ((cellSpaceWidth * i) - (nameSpaceWidth * i));
                    ImageView iv = new ImageView(this);
                    iv.setImageResource(R.drawable.yello_circle);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(imageScaledSize, imageScaledSize);
                    if (Utils.isArabicLangSelected(this)) {
                        params.rightMargin = xVal;
                    } else {
                        params.leftMargin = xVal;
                    }
                    params.topMargin = yVal;
                    stopsLayout.addView(iv, params);
                    RelativeLayout.LayoutParams stopParams = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    TextView tv = new TextView(this);
                    tv.setText(departPreviousStop);
                    tv.setTypeface(titleFace);
                    tv.setTextColor(getResources().getColor(R.color.flight_combo_depart_color));
                    if (Utils.isArabicLangSelected(this)) {
                        stopParams.rightMargin = stopXVal;
                        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                    } else {
                        stopParams.leftMargin = stopXVal;
                        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                    }
                    stopNamesLayout.addView(tv, stopParams);
                    String currentTime = fsBeanArrayList.get(stopCount + 1).getDdt();
                    departPreviousTime = fsBeanArrayList.get(stopCount).getArdt();
                    layoverTime.setText(Utils.getDaysInHrMinsFormat(currentTime, departPreviousTime, this));
                    stopCount++;
                }
            }
        }
    }

    private void loadMoreData() {
        for (int i = 0; i < showOdoArrayList.size(); i++) {
            setFlightDepartMoreInfo(showOdoArrayList.get(i), (i + 1));
        }
    }

    private void setFlightDepartMoreInfo(final OriginDestinationOptionBean data, int loopCount) {
        int count = 0;
        for (int i = 0; i < data.getFsDataBeanArrayList().size(); i++) {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_trip_confirmation, null);
            if (loopCount == 1) {
                flightInfoLayout.addView(view);
            } else if (loopCount == 2) {
                flightRetrunInfoLayout.setVisibility(View.VISIBLE);
                flightRetrunInfoLayout.addView(view);
            } else if (loopCount == 3) {
                flightMultiTripInfoLayout.setVisibility(View.VISIBLE);
                flightMultiTripInfoLayout.addView(view);
            }
            LinearLayout odoHeaderLayout = (LinearLayout) view.findViewById(R.id.odo_header_layout);
            TextView departHeaderText = (TextView) view.findViewById(R.id.departure_text);
            TextView departStops = (TextView) view.findViewById(R.id.departure_stops);
            ImageView airlineImage = (ImageView) view.findViewById(R.id.airline_image);
            TextView departFrom = (TextView) view.findViewById(R.id.departure_from);
            TextView departLocation = (TextView) view.findViewById(R.id.departure_location);
            TextView departTerminal = (TextView) view.findViewById(R.id.departure_terminal);
            TextView journeyAirlineName = (TextView) view.findViewById(R.id.journey_airline_name);
            TextView journeyAirlineDetails = (TextView) view.findViewById(R.id.journey_airline_details);
            TextView arrivalTo = (TextView) view.findViewById(R.id.arrival_to);
            TextView arrivalLocation = (TextView) view.findViewById(R.id.arrival_location);
            TextView arrivalTerminal = (TextView) view.findViewById(R.id.arrival_terminal);
            TextView flightStops = (TextView) view.findViewById(R.id.flight_stops);
            TextView flightDuration = (TextView) view.findViewById(R.id.flight_duration);
            TextView flightRefundStatus = (TextView) view.findViewById(R.id.flight_refund_status);
            LinearLayout layoverLayout = (LinearLayout) view.findViewById(R.id.layover_layout);
            TextView layoverText = (TextView) view.findViewById(R.id.layover_text);
            ImageView busIcon = (ImageView) view.findViewById(R.id.depart_bus_image);
            ImageView returnBusIcon = (ImageView) view.findViewById(R.id.return_bus_image);
            TextView classType = (TextView) view.findViewById(R.id.class_type);
            TextView baggageInfo = (TextView) view.findViewById(R.id.baggage_information);
            LinearLayout baggageInfoLayout = (LinearLayout) view.findViewById(R.id.baggage_layout);
            LinearLayout imageScrollLayout = (LinearLayout) view.findViewById(R.id.image_scroll_layout);
            LinearLayout firstDepartFromLayout = (LinearLayout) view.findViewById(R.id.departure_from_layout);
            LinearLayout firstDepartToLayout = (LinearLayout) view.findViewById(R.id.return_to_layout);
            LinearLayout merchandiseDataLayout = (LinearLayout) view.findViewById(R.id.flight_less_info_layout);
            ImageView layoutIcon = (ImageView) view.findViewById(R.id.layout_icon);
            TextView seatLayoutText = (TextView) view.findViewById(R.id.layout_text);
            ImageView wifiIcon = (ImageView) view.findViewById(R.id.wifi_icon);
            TextView wifiText = (TextView) view.findViewById(R.id.wifi_text);
            ImageView avodIcon = (ImageView) view.findViewById(R.id.avod_icon);
            TextView avodText = (TextView) view.findViewById(R.id.avod_text);
            ImageView freshMealIcon = (ImageView) view.findViewById(R.id.fresh_meal_icon);
            TextView freshMealText = (TextView) view.findViewById(R.id.fresh_meal_text);
            LinearLayout viewMoreLayout = (LinearLayout) view.findViewById(R.id.view_more_layout);
            final TextView viewMoreText = (TextView) view.findViewById(R.id.view_more_text);
            TextView merchandiseTextData = (TextView) view.findViewById(R.id.merchandise_text_data);
            final LinearLayout defaultAmenitiesLayout = (LinearLayout) view.findViewById(R.id.default_amenities_layout);
            final LinearLayout flightAmenitiesLayout = (LinearLayout) view.findViewById(R.id.flight_amenities_layout);
            TextView flightAmenitiesText = (TextView) view.findViewById(R.id.flight_amenities_text);
            LinearLayout viewLessLayout = (LinearLayout) view.findViewById(R.id.view_less_layout);
            LinearLayout.LayoutParams stopsParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
            firstDepartFromLayout.setLayoutParams(stopsParam);
            firstDepartToLayout.setLayoutParams(stopsParam);
            if (isArabicLang) {
                departHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                departStops.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
                layoverText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                departFrom.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                departLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
                departTerminal.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
                journeyAirlineName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                journeyAirlineDetails.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                arrivalTo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                arrivalLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
                arrivalTerminal.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
                flightStops.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                flightDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                flightRefundStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                classType.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                baggageInfo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                seatLayoutText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_airline_name_text));
                wifiText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_airline_name_text));
                avodText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_airline_name_text));
                freshMealText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_airline_name_text));
                viewMoreText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_airline_name_text));
                merchandiseTextData.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
                flightAmenitiesText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
                flightDuration.setGravity(Gravity.END);
                flightStops.setGravity(Gravity.END);
                merchandiseTextData.setGravity(Gravity.START);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    merchandiseTextData.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                }
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                flightStops.setLayoutParams(params);
                params.setMargins(0, -5, 0, 0);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    flightDuration.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                    flightStops.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                }
            }
            departHeaderText.setTypeface(titleFace);
            departStops.setTypeface(titleFace);
            departFrom.setTypeface(titleFace);
            departLocation.setTypeface(textFace);
            departTerminal.setTypeface(textFace);
            journeyAirlineName.setTypeface(titleFace);
            journeyAirlineDetails.setTypeface(titleFace);
            arrivalTo.setTypeface(titleFace);
            arrivalLocation.setTypeface(textFace);
            arrivalTerminal.setTypeface(textFace);
            flightStops.setTypeface(titleFace);
            flightDuration.setTypeface(titleFace);
            flightRefundStatus.setTypeface(titleFace);
            layoverText.setTypeface(titleFace);
            classType.setTypeface(titleFace);
            baggageInfo.setTypeface(textFace);
            seatLayoutText.setTypeface(boldFace);
            wifiText.setTypeface(boldFace);
            avodText.setTypeface(boldFace);
            freshMealText.setTypeface(boldFace);
            viewMoreText.setTypeface(titleFace);
            merchandiseTextData.setTypeface(defaultFace);
            flightAmenitiesText.setTypeface(tf);
            if (count == 0) {
                odoHeaderLayout.setVisibility(View.VISIBLE);
                layoverLayout.setVisibility(View.GONE);
                if (data.getFsDataBeanArrayList().get(0).getEq().equalsIgnoreCase("BUS")) {
                    busIcon.setVisibility(View.VISIBLE);
                }
            } else {
                flightDuration.setVisibility(View.INVISIBLE);
                odoHeaderLayout.setVisibility(View.GONE);
                layoverLayout.setVisibility(View.VISIBLE);
                data.setHeadingMessage(Utils.getDifferenceBetweenTwodaysInHrMinsFormat(data.getFsDataBeanArrayList().get(i).getDdt(), ddtPreviousTime, TripSummaryActivity.this));
                if (data.getFsDataBeanArrayList().get(data.getFsDataBeanArrayList().size() - 1).getEq().equalsIgnoreCase("BUS")) {
                    returnBusIcon.setVisibility(View.VISIBLE);
                }
            }
            ddtPreviousTime = data.getFsDataBeanArrayList().get(i).getArdt();
            String departureDate = "";
            if (isArabicLang) {
                departureDate = Utils.formatTimeStampToDate(data.getFsDataBeanArrayList().get(i).getDdt(), TripSummaryActivity.this);
            } else {
                departureDate = Utils.formatDateToDay(data.getFsDataBeanArrayList().get(i).getDdt(), TripSummaryActivity.this);
            }
            departStops.setText(departureDate);
            String departureAirportName = airportNamesMap.get(data.getFsDataBeanArrayList().get(i).getDap()).getAirpotName();
            String arrivalAirportName = airportNamesMap.get(data.getFsDataBeanArrayList().get(i).getAap()).getAirpotName();
            String journeyAirline = airLineNamesMap.get(data.getFsDataBeanArrayList().get(i).getMal());
            String imagePath = Constants.IMAGE_BASE_URL + data.getFsDataBeanArrayList().get(i).getMal() + Constants.IMAGE_FILE_FORMAT;
            Glide.with(TripSummaryActivity.this).load(imagePath).placeholder(R.drawable.imagethumb_search).into(airlineImage);
            journeyAirlineName.setText(journeyAirline);
            layoverText.setText(getString(R.string.label_tripSummary_layover_title) + ": " + data.getHeadingMessage() + " " + departureAirportName);
            String cabinName = data.getFsDataBeanArrayList().get(i).getCt();
            String journeyCabinType = "";
            if (cabinName.equalsIgnoreCase("Economy")) {
                journeyCabinType = getString(R.string.label_search_flight_economy);
            } else if (cabinName.equalsIgnoreCase("Business")) {
                journeyCabinType = getString(R.string.label_search_flight_business);
            } else if (cabinName.equalsIgnoreCase("First")) {
                journeyCabinType = getString(R.string.label_search_flight_first);
            }
            classType.setText(getString(R.string.label_cabin_class) + ":");
            classType.append(Utils.addDurationField(journeyCabinType));
            journeyAirlineDetails.setText(data.getFsDataBeanArrayList().get(i).getMal() + " " + data.getFsDataBeanArrayList().get(i).getFn());
            String airportCity = airportNamesMap.get(data.getFsDataBeanArrayList().get(i).getDap()).getCityName();
            String departureTime = Utils.formatDateToTime(data.getFsDataBeanArrayList().get(i).getDdt(), TripSummaryActivity.this);
            departFrom.setText(departureTime + ", " + airportCity);
            departFrom.append(Utils.addCountryCodeField(data.getFsDataBeanArrayList().get(i).getDap()));
            departLocation.setText(departureAirportName);
            String airportCity1 = airportNamesMap.get(data.getFsDataBeanArrayList().get(i).getAap()).getCityName();
            String arrivalCityTime = Utils.formattimeToString(data.getFsDataBeanArrayList().get(i).getArdt(), TripSummaryActivity.this);
            arrivalTo.setText(arrivalCityTime + ", " + airportCity1);
            arrivalTo.append(Utils.addCountryCodeField(data.getFsDataBeanArrayList().get(i).getAap()));
            arrivalLocation.setText(arrivalAirportName);
            String message = "";
            if (data.getFsDataBeanArrayList().get(i).getSq().equalsIgnoreCase("0")) {
                message = getString(R.string.label_non_stop_text) + " ";
            } else if (data.getFsDataBeanArrayList().get(i).getSq().equalsIgnoreCase("1")) {
                message = data.getFsDataBeanArrayList().get(i).getSq() + " " + getString(R.string.label_stop_text) + " ";
            } else {
                message = data.getFsDataBeanArrayList().get(i).getSq() + " " + getString(R.string.label_stops_text) + " ";
            }
            flightStops.setText(message);
            if (data.getFsDataBeanArrayList().get(i).getDt().equalsIgnoreCase("null")) {
                departTerminal.setVisibility(View.GONE);
            } else {
                departTerminal.setVisibility(View.VISIBLE);
                departTerminal.setText(getString(R.string.label_terminal) + " " + data.getFsDataBeanArrayList().get(i).getDt());
            }
            if (data.getFsDataBeanArrayList().get(i).getAt().equalsIgnoreCase("null")) {
                arrivalTerminal.setVisibility(View.GONE);
            } else {
                arrivalTerminal.setVisibility(View.VISIBLE);
                arrivalTerminal.setText(getString(R.string.label_terminal) + " " + data.getFsDataBeanArrayList().get(i).getAt());
            }
            flightDuration.setText(getString(R.string.label_duration_text) + ":");
            flightDuration.append(Utils.addDurationField(Utils.formatDurationToString(data.getFsDataBeanArrayList().get(i).getDur(), TripSummaryActivity.this)));
            if (data.getRs().equalsIgnoreCase("Refundable") || data.getRs().equalsIgnoreCase("Refundable Before Departure")) {
                flightRefundStatus.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.success_message_background));
                flightRefundStatus.setText(" " + getString(R.string.RefLbl));
            } else {
                flightRefundStatus.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.review_non_refund_color));
                flightRefundStatus.setText(" " + getString(R.string.NonRefLbl));
            }
            if (data.getTt().equalsIgnoreCase("1")) {
                if (data.getRefNum().equalsIgnoreCase("1")) {
                    departHeaderText.setText(R.string.label_search_flight_departure);
                }
            }
            if (data.getTt().equalsIgnoreCase("2")) {
                if (data.getRefNum().equalsIgnoreCase("1")) {
                    departHeaderText.setText(R.string.label_search_flight_departure);
                }
                if (data.getRefNum().equalsIgnoreCase("2")) {
                    departHeaderText.setText(R.string.label_search_flight_return);
                }
            }
            if (data.getTt().equalsIgnoreCase("3")) {
                if (data.getRefNum().equalsIgnoreCase("1")) {
                    departHeaderText.setText(R.string.label_search_flight_departure);
                }
                if (data.getRefNum().equalsIgnoreCase("2")) {
                    departHeaderText.setText(R.string.label_search_flight_departure);
                }
                if (data.getRefNum().equalsIgnoreCase("3")) {
                    departHeaderText.setText(R.string.label_search_flight_departure);
                }
            }
            String checkInDetails = Utils.checkStringVal(data.getCb());
            if (!checkInDetails.equalsIgnoreCase("")) {
                String baggageInformation = "";
                String[] baggageDetails = checkInDetails.split(" ");
                if (baggageDetails[1].equalsIgnoreCase("Kilos")) {
                    baggageInformation = baggageDetails[0] + " " + getString(R.string.label_kilo);
                } else if (baggageDetails[1].equalsIgnoreCase("Pieces")) {
                    baggageInformation = baggageDetails[0] + " " + getString(R.string.label_pieces);
                } else {
                    baggageInformation = checkInDetails;
                }
                if (isArabicLang) {
                    baggageInfo.setText(getString(R.string.label_reviewPage_baggageInfo) + " '" + baggageInformation + "' (" + getString(R.string.label_per_person_text) + ")");
                } else {
                    baggageInfo.setText(getString(R.string.label_reviewPage_baggageInfo) + " " + getString(R.string.label_reviewPage_checkIn) + " '" + baggageInformation + "' (" + getString(R.string.label_per_person_text) + ")");
                }
            } else {
                baggageInfoLayout.setVisibility(View.GONE);
            }
            count++;
            String firstFlightName = data.getFsDataBeanArrayList().get(i).getMal() + "-" + data.getFsDataBeanArrayList().get(i).getEq();
            String firstAircraftType = data.getFsDataBeanArrayList().get(i).getEq();
            for (int j = 0; j < mainMarchArrayList.size(); j++) {
                if (firstFlightName.contains(mainMarchArrayList.get(j).getFlightName()) &&
                        data.getFsDataBeanArrayList().get(i).getDap().equalsIgnoreCase(mainMarchArrayList.get(j).getDap()) &&
                        data.getFsDataBeanArrayList().get(i).getAap().equalsIgnoreCase(mainMarchArrayList.get(j).getAap())) {
                    iconNameArrayList = new ArrayList<>();
                    imageScrollLayout.removeAllViews();
                    merchandiseDataLayout.setVisibility(View.VISIBLE);
                    String aircraftType = Utils.getAircraftType(firstAircraftType, TripSummaryActivity.this);
                    if (!mainMarchArrayList.get(j).getBodyType().isEmpty()) {
                        for (Map.Entry<Integer, AmenitiesBean> entry : AmenitiesConstants.bodyTypeHashMap.entrySet()) {
                            if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                                if (entry.getValue().getAr().equals(mainMarchArrayList.get(j).getBodyType())) {
                                    appendBodyTypeValue(AmenitiesConstants.bodyTypeHashMap, entry.getKey(), aircraftType);
                                }
                            } else {
                                if (entry.getValue().getEn().equals(mainMarchArrayList.get(j).getBodyType())) {
                                    appendBodyTypeValue(AmenitiesConstants.bodyTypeHashMap, entry.getKey(), aircraftType);
                                }
                            }
                        }
                    }
                    if (!mainMarchArrayList.get(j).getSeatType().isEmpty()) {
                        for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.seatTypeHashMap.entrySet()) {
                            String moreData = "";
                            String seatWidthText = "";
                            String seatLengthText = "";
                            String seatPitchText = "";
                            if (!mainMarchArrayList.get(j).getSeatWidth().isEmpty() && !mainMarchArrayList.get(j).getSeatLength().isEmpty() && !mainMarchArrayList.get(j).getSeatPitch().isEmpty()) {
                                seatWidthText = getString(R.string.label_seat_width) + " " + mainMarchArrayList.get(j).getSeatWidth();
                                seatLengthText = getString(R.string.label_leg_room) + " " + mainMarchArrayList.get(j).getSeatLength();
                                seatPitchText = getString(R.string.label_pitch_size) + " " + mainMarchArrayList.get(j).getSeatPitch();
                                moreData = seatWidthText + ", " + seatLengthText + ", " + seatPitchText;
                            } else if (!mainMarchArrayList.get(j).getSeatWidth().isEmpty() && !mainMarchArrayList.get(j).getSeatPitch().isEmpty()) {
                                seatWidthText = getString(R.string.label_seat_width) + " " + mainMarchArrayList.get(j).getSeatWidth();
                                seatPitchText = getString(R.string.label_pitch_size) + " " + mainMarchArrayList.get(j).getSeatPitch();
                                moreData = seatWidthText + ", " + seatPitchText;
                            }
                            if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                                if (entry.getValue().getAr().equals(mainMarchArrayList.get(j).getSeatType())) {
                                    appendMerchandiseValues(AmenitiesConstants.seatTypeHashMap, entry.getKey(), moreData);
                                }
                            } else {
                                if (entry.getValue().getEn().equals(mainMarchArrayList.get(j).getSeatType())) {
                                    appendMerchandiseValues(AmenitiesConstants.seatTypeHashMap, entry.getKey(), moreData);
                                }
                            }
                        }
                    }
                    if (!mainMarchArrayList.get(j).getSeatLayout().isEmpty()) {
                        layoutIcon.setImageResource(R.drawable.seat_layout);
                        seatLayoutText.setText(mainMarchArrayList.get(j).getSeatLayout());
                        seatLayoutText.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.result_compo_duration));
                        iconNameArrayList.add("seat_layout==" + getString(R.string.label_seat_layout) + " " + mainMarchArrayList.get(j).getSeatLayout());
                    }
                    if (mainMarchArrayList.get(j).getSeatLayout().isEmpty()) {
                        layoutIcon.setVisibility(View.GONE);
                        seatLayoutText.setVisibility(View.GONE);
                    }
                    if (!mainMarchArrayList.get(j).getMeals().isEmpty()) {
                        freshMealIcon.setImageResource(R.drawable.fresh_meal);
                        freshMealText.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.result_compo_duration));
                        for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.mealsHashMap.entrySet()) {
                            if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                                if (entry.getValue().getAr().equals(mainMarchArrayList.get(j).getMeals())) {
                                    appendMerchandiseValues(AmenitiesConstants.mealsHashMap, entry.getKey(), "");
                                }
                            } else {
                                if (entry.getValue().getEn().equals(mainMarchArrayList.get(j).getMeals())) {
                                    appendMerchandiseValues(AmenitiesConstants.mealsHashMap, entry.getKey(), "");
                                }
                            }
                        }
                    }
                    if (!mainMarchArrayList.get(j).getWifi().isEmpty()) {
                        wifiIcon.setImageResource(R.drawable.wifi);
                        wifiText.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.result_compo_duration));
                        for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.wifiHashMap.entrySet()) {
                            if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                                if (entry.getValue().getAr().equals(mainMarchArrayList.get(j).getWifi())) {
                                    appendMerchandiseValues(AmenitiesConstants.wifiHashMap, entry.getKey(), "");
                                }
                            } else {
                                if (entry.getValue().getEn().equals(mainMarchArrayList.get(j).getWifi())) {
                                    appendMerchandiseValues(AmenitiesConstants.wifiHashMap, entry.getKey(), "");
                                }
                            }
                        }
                    }
                    if (!mainMarchArrayList.get(j).getAvod().isEmpty()) {
                        if (mainMarchArrayList.get(j).getAvod().equalsIgnoreCase("Yes")) {
                            if (!mainMarchArrayList.get(j).getVideoType().isEmpty()) {
                                avodIcon.setImageResource(R.drawable.avod);
                                avodText.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.result_compo_duration));
                                for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.avodVideoHashMap.entrySet()) {
                                    String screenSize = "";
                                    if (!mainMarchArrayList.get(j).getAvodScreenSize().isEmpty()) {
                                        screenSize = mainMarchArrayList.get(j).getAvodScreenSize();
                                    }
                                    if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                                        if (entry.getValue().getAr().equals(mainMarchArrayList.get(j).getVideoType())) {
                                            appendMerchandiseValues(AmenitiesConstants.avodVideoHashMap, entry.getKey(), screenSize);
                                        }
                                    } else {
                                        if (entry.getValue().getEn().equals(mainMarchArrayList.get(j).getVideoType())) {
                                            appendMerchandiseValues(AmenitiesConstants.avodVideoHashMap, entry.getKey(), screenSize);
                                        }
                                    }
                                }
                            }
                        }
                        if (mainMarchArrayList.get(j).getAvod().equalsIgnoreCase("Charge")) {
                            if (!mainMarchArrayList.get(j).getVideoType().isEmpty()) {
                                avodIcon.setImageResource(R.drawable.avod);
                                avodText.setTextColor(Utils.getColor(TripSummaryActivity.this, R.color.result_compo_duration));
                                for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.avodChargeVideoHashMap.entrySet()) {
                                    String screenSize = "";
                                    if (!mainMarchArrayList.get(j).getAvodScreenSize().isEmpty()) {
                                        screenSize = mainMarchArrayList.get(j).getAvodScreenSize();
                                    }
                                    if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                                        if (entry.getValue().getAr().equals(mainMarchArrayList.get(j).getVideoType())) {
                                            appendMerchandiseValues(AmenitiesConstants.avodChargeVideoHashMap, entry.getKey(), screenSize);
                                        }
                                    } else {
                                        if (entry.getValue().getEn().equals(mainMarchArrayList.get(j).getVideoType())) {
                                            appendMerchandiseValues(AmenitiesConstants.avodChargeVideoHashMap, entry.getKey(), screenSize);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!mainMarchArrayList.get(j).getPowerType().isEmpty()) {
                        for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.powerTypeHashMap.entrySet()) {
                            if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                                if (entry.getValue().getAr().equals(mainMarchArrayList.get(j).getPowerType())) {
                                    appendMerchandiseValues(AmenitiesConstants.powerTypeHashMap, entry.getKey(), "");
                                }
                            } else {
                                if (entry.getValue().getEn().equals(mainMarchArrayList.get(j).getPowerType())) {
                                    appendMerchandiseValues(AmenitiesConstants.powerTypeHashMap, entry.getKey(), "");
                                }
                            }
                        }
                    }
                    if (!mainMarchArrayList.get(j).getMobile().isEmpty()) {
                        appendMerchandiseValues(AmenitiesConstants.mobileHashMap, mainMarchArrayList.get(j).getMobile(), "");
                    }
                    if (!mainMarchArrayList.get(j).getInfantCare().isEmpty()) {
                        for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.infantCareHashMap.entrySet()) {
                            if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                                if (entry.getValue().getAr().equals(mainMarchArrayList.get(j).getInfantCare())) {
                                    appendMerchandiseValues(AmenitiesConstants.infantCareHashMap, entry.getKey(), "");
                                }
                            } else {
                                if (entry.getValue().getEn().equals(mainMarchArrayList.get(j).getInfantCare())) {
                                    appendMerchandiseValues(AmenitiesConstants.infantCareHashMap, entry.getKey(), "");
                                }
                            }
                        }
                    }
                    if (!mainMarchArrayList.get(j).getAlocohol().isEmpty()) {
                        for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.alocoholHashMap.entrySet()) {
                            if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                                if (entry.getValue().getAr().equals(mainMarchArrayList.get(j).getAlocohol())) {
                                    appendMerchandiseValues(AmenitiesConstants.alocoholHashMap, entry.getKey(), "");
                                }
                            } else {
                                if (entry.getValue().getEn().equals(mainMarchArrayList.get(j).getAlocohol())) {
                                    appendMerchandiseValues(AmenitiesConstants.alocoholHashMap, entry.getKey(), "");
                                }
                            }
                        }
                    }
                    if (!mainMarchArrayList.get(j).getPrayerArea().isEmpty()) {
                        appendMerchandiseValues(AmenitiesConstants.prayerAreaHashMap, mainMarchArrayList.get(j).getPrayerArea(), "");
                    }
                    if (!mainMarchArrayList.get(j).getShowerArea().isEmpty()) {
                        appendMerchandiseValues(AmenitiesConstants.showerAreaHashMap, mainMarchArrayList.get(j).getShowerArea(), "");
                    }
                    if (!mainMarchArrayList.get(j).getAircraftImage().isEmpty() || !mainMarchArrayList.get(j).getAircraftImage().equalsIgnoreCase("")) {
                        View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                        ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                        TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                        int imageScaledWidth = getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                        int imageScaledHeight = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                        String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                                Constants.IMAGE_QUALITY + "" + mainMarchArrayList.get(j).getAircraftImage();
                        Glide.with(TripSummaryActivity.this).load(url).placeholder(R.drawable.imagethumb_search)
                                .bitmapTransform(new RoundImageTransform(TripSummaryActivity.this, 0, 0)).into(flightMerchandiseImage);
                        imageDescriptionText.setText(getString(R.string.label_aircraft));
                        imageDescriptionText.setTypeface(titleFace);
                        imageScrollLayout.addView(scrollImgView);
                    }
                    if (!mainMarchArrayList.get(j).getClassOverviewImage().isEmpty() || !mainMarchArrayList.get(j).getClassOverviewImage().equalsIgnoreCase("")) {
                        View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                        ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                        TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                        int imageScaledWidth = getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                        int imageScaledHeight = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                        String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                                Constants.IMAGE_QUALITY + "" + mainMarchArrayList.get(j).getClassOverviewImage();
                        Glide.with(TripSummaryActivity.this).load(url).placeholder(R.drawable.imagethumb_search)
                                .bitmapTransform(new RoundImageTransform(TripSummaryActivity.this, 0, 0)).into(flightMerchandiseImage);
                        imageDescriptionText.setText(getString(R.string.label_class_overview));
                        imageDescriptionText.setTypeface(titleFace);
                        imageScrollLayout.addView(scrollImgView);
                    }
                    if (!mainMarchArrayList.get(j).getSeatImage().isEmpty() || !mainMarchArrayList.get(j).getSeatImage().equalsIgnoreCase("")) {
                        View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                        ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                        TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                        int imageScaledWidth = getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                        int imageScaledHeight = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                        String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                                Constants.IMAGE_QUALITY + "" + mainMarchArrayList.get(j).getSeatImage();
                        Glide.with(TripSummaryActivity.this).load(url).placeholder(R.drawable.imagethumb_search)
                                .bitmapTransform(new RoundImageTransform(TripSummaryActivity.this, 0, 0)).into(flightMerchandiseImage);
                        imageDescriptionText.setText(getString(R.string.label_seat_image));
                        imageDescriptionText.setTypeface(titleFace);
                        imageScrollLayout.addView(scrollImgView);
                    }
                    if (!mainMarchArrayList.get(j).getFacilityImage().isEmpty() || !mainMarchArrayList.get(j).getFacilityImage().equalsIgnoreCase("")) {
                        View scrollImgView = inflater.inflate(R.layout.view_flights_merchandise, null);
                        ImageView flightMerchandiseImage = (ImageView) scrollImgView.findViewById(R.id.flight_merchandise_image);
                        TextView imageDescriptionText = (TextView) scrollImgView.findViewById(R.id.image_description_text);
                        int imageScaledWidth = getResources().getDimensionPixelSize(R.dimen.image_scale_height);
                        int imageScaledHeight = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
                        String url = Constants.IMAGE_SCALING_URL + imageScaledWidth + "x" + imageScaledHeight +
                                Constants.IMAGE_QUALITY + "" + mainMarchArrayList.get(j).getFacilityImage();
                        Glide.with(TripSummaryActivity.this).load(url).placeholder(R.drawable.imagethumb_search)
                                .bitmapTransform(new RoundImageTransform(TripSummaryActivity.this, 0, 0)).into(flightMerchandiseImage);
                        imageDescriptionText.setText(getString(R.string.label_facility_image));
                        imageDescriptionText.setTypeface(titleFace);
                        imageScrollLayout.addView(scrollImgView);
                    }
                    if (!mainMarchArrayList.get(j).getRemarks().isEmpty()) {
                        iconNameArrayList.add("==" + mainMarchArrayList.get(j).getRemarks());
                    }
                    if (!mainMarchArrayList.get(j).getProvideTransportation().isEmpty()) {
                        iconNameArrayList.add("==" + getString(R.string.label_provide_transportation));
                    }
                    if (!mainMarchArrayList.get(j).getProvideOtherInfo().isEmpty()) {
                        iconNameArrayList.add("==" + mainMarchArrayList.get(j).getProvideOtherInfo());
                    }
                }
                addNameIconsToText(merchandiseTextData, loopCount);
            }
            viewMoreLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flightAmenitiesLayout.setVisibility(View.VISIBLE);
                    defaultAmenitiesLayout.setVisibility(View.GONE);
                }
            });
            viewLessLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flightAmenitiesLayout.setVisibility(View.GONE);
                    defaultAmenitiesLayout.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void handleMerchandiseRequest() {
        JSONObject mainJSON = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        try {
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
            mainJSON.put("timeStamp", timeStamp);
            mainJSON.put("cls", selectedClass);
            JSONArray mDataArray = new JSONArray();
            for (int i = 0; i < flightNamesArray.size(); i++) {
                boolean isContains = false;
                for (int j = 0; j < mDataArray.length(); j++) {
                    if (mDataArray.get(j).toString().equalsIgnoreCase(flightNamesArray.get(i).getKey())) {
                        isContains = true;
                        break;
                    }
                }
                if (!isContains) {
                    mDataArray.put(flightNamesArray.get(i).getKey());
                }
            }
            mainJSON.put("mData", mDataArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        showLoading();
        HTTPAsync async = new HTTPAsync(TripSummaryActivity.this, TripSummaryActivity.this, Constants.MERCHANDISE_BASE_URL,
                Constants.MERCHANDISE_SEARCH_URI, mainJSON.toString(), GET_MERCHANDISE_DATA, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String receivedMerchandiseData = "";

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "DATA::" + data);
        if (!data.equalsIgnoreCase("") || !data.equalsIgnoreCase("null")) {
            closeLoading();
            JSONObject obj = null;
            if (serviceType == GET_MERCHANDISE_DATA) {
                receivedMerchandiseData = data;
                SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                if (airportNamesMap.size() > 0) {
                    if (rewardsLayout.getVisibility() == View.GONE) {
                        if (!pref.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
                            if (pref.getBoolean(Constants.IS_REWARDS_ENABLED, false)) {
                                handleEranLoyaltyRequest();
                            } else {
                                closeLoading();
                            }
                        } else {
                            closeLoading();
                        }
                    } else {
                        closeLoading();
                    }
                } else {
                    closeLoading();
                }
                try {
                    obj = new JSONObject(data);
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        if (obj.has("data")) {
                            boolean isRoutesAvailable = false;
                            for (int i = 0; i < flightNamesArray.size(); i++) {
                                String key = flightNamesArray.get(i).getKey();
                                if (obj.getJSONObject("data").has(key)) {
                                    JSONArray flightNameArray = obj.getJSONObject("data").getJSONArray(key);
                                    for (int j = 0; j < flightNameArray.length(); j++) {
                                        if (flightNameArray.getJSONObject(j).has("route")) {
                                            JSONArray routeArr = flightNameArray.getJSONObject(j).getJSONArray("route");
                                            if (routeArr.length() > 0) {
                                                isRoutesAvailable = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (isRoutesAvailable) {
                                handleAirportCountry();
                            } else {
                                parseMerchantData();
                                loadLessData();
                            }
                        }
                    }
                } catch (Exception e) {
                }
            } else if (serviceType == GET_MERCHANDISE_AIRPORT_DATA) {
                SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                if (rewardsLayout.getVisibility() == View.GONE) {
                    if (!pref.getString(Constants.USER_UNIQUE_ID, "").isEmpty()) {
                        if (pref.getBoolean(Constants.IS_REWARDS_ENABLED, false)) {
                            handleEranLoyaltyRequest();
                        } else {
                            closeLoading();
                        }
                    } else {
                        closeLoading();
                    }
                } else {
                    closeLoading();
                }
                try {
                    obj = new JSONObject(data);
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        routesMap.clear();
                        if (obj.has("data")) {
                            JSONArray dataArr = obj.getJSONArray("data");
                            for (int i = 0; i < dataArr.length(); i++) {
                                routesMap.put(dataArr.getJSONObject(i).getString("iata"), dataArr.getJSONObject(i).getString("cc"));
                            }
                            parseMerchantData();
                            loadLessData();
                        }
                    }
                } catch (Exception e) {
                }
            } else if (serviceType == GET_LOYALTY_POINTS) {
                closeLoading();
                try {
                    obj = new JSONObject(data);
                    String status = obj.getString("status");
                    closeLoading();
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        String points = obj.getString("earnPoints");
                        Utils.printMessage(TAG, "earn points::" + points);
                        userEarnedPoints = points;
                        rewardsLayout.setVisibility(View.VISIBLE);
                        rewardsPointsText.setText(String.format(getResources().getString(R.string.label_rewards_earned_points), points));
                    } else {
                        Utils.printMessage(TAG, "earn points::" + obj.getString("message"));
                        rewardsLayout.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                }
            } else if (serviceType == GET_ACCESS_TOKEN) {
                try {
                    obj = new JSONObject(data);
                    String accessToken = obj.getString("accessToken");
                    String expireIn = obj.getString("expireIn");
                    String refreshToken = obj.getString("refreshToken");
                    long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                    SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Constants.ACCESS_TOKEN, accessToken);
                    editor.putString(Constants.EXPIRE_IN, expireIn);
                    editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                    editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                    editor.apply();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(getString(R.string.label_checking_availability_title));
        loadingText.setTypeface(titleFace);
        descriptionText.setTypeface(titleFace);
        descriptionText.setText(R.string.label_checking_availability_message);
        if (isArabicLang) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            descriptionText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        headerViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        headerViewLayout.removeView(loadingView);
    }

    private void appendBodyTypeValue(HashMap<Integer, AmenitiesBean> dataHashMap, int val, String moreData) {
        AmenitiesBean bean = dataHashMap.get(val);
        String strValue = "";
        if (bean != null) {
            strValue = bean.getEn();
            if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                strValue = bean.getAr();
            }
            String resultText = "";
            resultText = moreData + "==" + strValue;
            iconNameArrayList.add(bean.getIcon() + "==" + resultText);
        }
    }

    private void appendMerchandiseValues(HashMap<String, AmenitiesBean> dataHashMap, String data, String moreData) {
        AmenitiesBean bean = dataHashMap.get(data);
        String strVal = "";
        if (bean != null) {
            strVal = bean.getEn();
            if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                strVal = bean.getAr();
            }
            String resultText = "";
            if (moreData.equalsIgnoreCase("")) {
                resultText = strVal;
            } else {
                resultText = strVal + "==" + moreData;
            }
            iconNameArrayList.add(bean.getIcon() + "==" + resultText);
        }
    }

    private String checkAmenitiesValue(HashMap<String, AmenitiesBean> dataHashMap, String data) {
        AmenitiesBean bean = dataHashMap.get(data);
        String strValue = "";
        if (bean != null) {
            strValue = bean.getEn();
            if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                strValue = bean.getAr();
            }
        }
        return strValue;
    }

    private String checkBodyTypeAmenitiesValue(HashMap<Integer, AmenitiesBean> dataHashMap, int value) {
        AmenitiesBean bean = dataHashMap.get(value);
        String strValue = "";
        if (bean != null) {
            strValue = bean.getEn();
            if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
                strValue = bean.getAr();
            }
        }
        return strValue;
    }

    private void handleAirportCountry() {
        ArrayList<String> list = new ArrayList<String>();
        for (OriginDestinationOptionBean odo : showOdoArrayList) {
            for (FSDataBean fs : odo.getFsDataBeanArrayList()) {
                if (!list.contains(fs.getDap())) {
                    list.add(fs.getDap());
                }
                if (!list.contains(fs.getAap())) {
                    list.add(fs.getAap());
                }
            }
        }
        JSONArray array = new JSONArray();
        for (String str : list) {
            array.put(str);
        }
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
            mainJSON.put("lang", selectedLang);
            mainJSON.put("ac", array);
        } catch (Exception e) {
        }
        Utils.printMessage(TAG, "mainJSON::" + mainJSON.toString());
        if (airportNamesMap.size() > 0) {
            closeLoading();
            Utils.printMessage(TAG, "Empty Arr");
        }
        showLoading();
        HTTPAsync async = new HTTPAsync(TripSummaryActivity.this, TripSummaryActivity.this, Constants.MERCHANDISE_BASE_URL,
                Constants.MERCHANDISE_AIRPORT_URI, mainJSON.toString(), GET_MERCHANDISE_AIRPORT_DATA, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void parseMerchantData() {
        try {
            JSONObject obj = new JSONObject(receivedMerchandiseData);
            String status = obj.getString("status");
            if (status.equalsIgnoreCase("SUCCESS")) {
                if (obj.has("data")) {
                    for (int i = 0; i < flightNamesArray.size(); i++) {
                        String key = flightNamesArray.get(i).getKey();
                        if (obj.getJSONObject("data").has(key)) {
                            JSONArray flightNameArray = obj.getJSONObject("data").getJSONArray(key);
                            for (int j = 0; j < flightNameArray.length(); j++) {
                                boolean isAvailableSector = false;
                                if (flightNameArray.getJSONObject(j).has("route")) {
                                    JSONArray routeArr = flightNameArray.getJSONObject(j).getJSONArray("route");
                                    if (routeArr.length() > 0) {
                                        //call route array to get the updated values
                                        for (int k = 0; k < routeArr.length(); k++) {
                                            JSONObject routeObj = routeArr.getJSONObject(k);
                                            String dap = flightNamesArray.get(i).getDap();
                                            String aap = flightNamesArray.get(i).getAap();
                                            if (routeObj.getString("sourceCountry").equalsIgnoreCase(routesMap.get(dap)) &&
                                                    routeObj.getString("destCountry").equalsIgnoreCase(routesMap.get(aap))) {
                                                //sectors
                                                for (int z = 0; z < routeObj.getJSONArray("sectors").length(); z++) {
                                                    JSONObject sectorObj = routeObj.getJSONArray("sectors").getJSONObject(z);
                                                    if ((sectorObj.getString("originSec").trim().equalsIgnoreCase(dap) || sectorObj.getString("originSec").trim().equalsIgnoreCase("all")) &&
                                                            (sectorObj.getString("destSec").trim().equalsIgnoreCase(aap) || sectorObj.getString("destSec").trim().equalsIgnoreCase("all"))) {
                                                        isAvailableSector = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        isAvailableSector = true;
                                    }
                                } else {
                                    isAvailableSector = true;
                                }
                                if (!isAvailableSector) {
                                    continue;
                                }
                                if (flightNameArray.getJSONObject(j).has("amenities")) {
                                    FlightAmenitiesBean amenitiesBean = new FlightAmenitiesBean();
                                    JSONObject amenitiesObj = flightNameArray.getJSONObject(j).getJSONObject("amenities");
                                    amenitiesBean.setFlightName(key);
                                    amenitiesBean.setAap(flightNamesArray.get(i).getAap());
                                    amenitiesBean.setDap(flightNamesArray.get(i).getDap());
                                    if (amenitiesObj.has("classType"))
                                        amenitiesBean.setClassType(amenitiesObj.getString("classType"));
                                    if (amenitiesObj.has("seatPitch")) {
                                        amenitiesBean.setSeatPitch(amenitiesObj.getString("seatPitch"));
                                    } else {
                                        amenitiesBean.setSeatPitch("");
                                    }
                                    if (amenitiesObj.has("avod")) {
                                        String avodType = amenitiesObj.getString("avod");
                                        if (avodType.equalsIgnoreCase("Yes")) {
                                            if (amenitiesObj.has("videoType")) {
                                                amenitiesBean.setVideoType(checkAmenitiesValue(AmenitiesConstants.avodVideoHashMap, amenitiesObj.getString("videoType")));
                                            } else {
                                                amenitiesBean.setVideoType("");
                                            }
                                        } else if (avodType.equalsIgnoreCase("Charge")) {
                                            if (amenitiesObj.has("videoType")) {
                                                amenitiesBean.setVideoType(checkAmenitiesValue(AmenitiesConstants.avodChargeVideoHashMap, amenitiesObj.getString("videoType")));
                                            } else {
                                                amenitiesBean.setVideoType("");
                                            }
                                        }
                                        amenitiesBean.setAvod(avodType);
                                    } else {
                                        amenitiesBean.setAvod("");
                                    }
                                    if (amenitiesObj.has("seatWidth")) {
                                        amenitiesBean.setSeatWidth(amenitiesObj.getString("seatWidth"));
                                    } else {
                                        amenitiesBean.setSeatWidth("");
                                    }
                                    if (amenitiesObj.has("seatLength")) {
                                        amenitiesBean.setSeatLength(amenitiesObj.getString("seatLength"));
                                    } else {
                                        amenitiesBean.setSeatLength("");
                                    }
                                    if (amenitiesObj.has("laptopPower")) {
                                        amenitiesBean.setLaptopPower(amenitiesObj.getString("laptopPower"));
                                    } else {
                                        amenitiesBean.setLaptopPower("");
                                    }
                                    if (amenitiesObj.has("seatType")) {
                                        amenitiesBean.setSeatType(checkAmenitiesValue(AmenitiesConstants.seatTypeHashMap, amenitiesObj.getString("seatType")));
                                    } else {
                                        amenitiesBean.setSeatType("");
                                    }
                                    if (amenitiesObj.has("powerType")) {
                                        amenitiesBean.setPowerType(checkAmenitiesValue(AmenitiesConstants.powerTypeHashMap, amenitiesObj.getString("powerType")));
                                    } else {
                                        amenitiesBean.setPowerType("");
                                    }
                                    if (amenitiesObj.has("mobile")) {
                                        amenitiesBean.setMobile(amenitiesObj.getString("mobile"));
                                    } else {
                                        amenitiesBean.setMobile("");
                                    }
                                    if (amenitiesObj.has("prayerArea")) {
                                        amenitiesBean.setPrayerArea(amenitiesObj.getString("prayerArea"));
                                    } else {
                                        amenitiesBean.setPrayerArea("");
                                    }
                                    if (amenitiesObj.has("noOfSeats"))
                                        amenitiesBean.setNoOfSeats(amenitiesObj.getString("noOfSeats"));
                                    if (amenitiesObj.has("meals")) {
                                        amenitiesBean.setMeals(checkAmenitiesValue(AmenitiesConstants.mealsHashMap, amenitiesObj.getString("meals")));
                                    } else {
                                        amenitiesBean.setMeals("");
                                    }
                                    if (amenitiesObj.has("wifi")) {
                                        amenitiesBean.setWifi(checkAmenitiesValue(AmenitiesConstants.wifiHashMap, amenitiesObj.getString("wifi")));
                                    } else {
                                        amenitiesBean.setWifi("");
                                    }
                                    if (amenitiesObj.has("infantCare")) {
                                        amenitiesBean.setInfantCare(checkAmenitiesValue(AmenitiesConstants.infantCareHashMap, amenitiesObj.getString("infantCare")));
                                    } else {
                                        amenitiesBean.setInfantCare("");
                                    }
                                    if (amenitiesObj.has("alocohol")) {
                                        amenitiesBean.setAlocohol(checkAmenitiesValue(AmenitiesConstants.alocoholHashMap, amenitiesObj.getString("alocohol")));
                                    } else {
                                        amenitiesBean.setAlocohol("");
                                    }
                                    if (amenitiesObj.has("avodScreenSize")) {
                                        amenitiesBean.setAvodScreenSize(amenitiesObj.getString("avodScreenSize"));
                                    } else {
                                        amenitiesBean.setAvodScreenSize("");
                                    }
                                    if (amenitiesObj.has("remarks")) {
                                        amenitiesBean.setRemarks(amenitiesObj.getString("remarks"));
                                    } else {
                                        amenitiesBean.setRemarks("");
                                    }
                                    if (amenitiesObj.has("provideTransportation")) {
                                        amenitiesBean.setProvideTransportation(amenitiesObj.getString("provideTransportation"));
                                    } else {
                                        amenitiesBean.setProvideTransportation("");
                                    }
                                    if (amenitiesObj.has("seatLayout")) {
                                        amenitiesBean.setSeatLayout(amenitiesObj.getString("seatLayout"));
                                    } else {
                                        amenitiesBean.setSeatLayout("");
                                    }
                                    if (amenitiesObj.has("provideOtherInfo")) {
                                        amenitiesBean.setProvideOtherInfo(amenitiesObj.getString("provideOtherInfo"));
                                    } else {
                                        amenitiesBean.setProvideOtherInfo("");
                                    }
                                    if (amenitiesObj.has("aircraftImage")) {
                                        amenitiesBean.setAircraftImage(amenitiesObj.getString("aircraftImage"));
                                    } else {
                                        amenitiesBean.setAircraftImage("");
                                    }
                                    if (amenitiesObj.has("classOverviewImage")) {
                                        amenitiesBean.setClassOverviewImage(amenitiesObj.getString("classOverviewImage"));
                                    } else {
                                        amenitiesBean.setClassOverviewImage("");
                                    }
                                    if (amenitiesObj.has("seatImage")) {
                                        amenitiesBean.setSeatImage(amenitiesObj.getString("seatImage"));
                                    } else {
                                        amenitiesBean.setSeatImage("");
                                    }
                                    if (amenitiesObj.has("facilityImage")) {
                                        amenitiesBean.setFacilityImage(amenitiesObj.getString("facilityImage"));
                                    } else {
                                        amenitiesBean.setFacilityImage("");
                                    }
                                    if (amenitiesObj.has("showerArea")) {
                                        amenitiesBean.setShowerArea(amenitiesObj.getString("showerArea"));
                                    } else {
                                        amenitiesBean.setShowerArea("");
                                    }
                                    if (amenitiesObj.has("bodyType")) {
                                        int bodyTypeVal = Integer.parseInt(amenitiesObj.getString("bodyType"));
                                        amenitiesBean.setBodyType(checkBodyTypeAmenitiesValue(AmenitiesConstants.bodyTypeHashMap, bodyTypeVal));
                                    } else {
                                        amenitiesBean.setBodyType("");
                                    }
                                    mainMarchArrayList.add(amenitiesBean);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        Utils.printMessage(TAG, "mainMarchArrayList::" + mainMarchArrayList.size());
    }

    private void addNameIconsToText(TextView merchandiseTextData, int loopCount) {
        if (loopCount == 1) {
            merchandiseTextData.setText("");
        } else if (loopCount == 2) {
            merchandiseTextData.setText("");
        } else if (loopCount == 3) {
            merchandiseTextData.setText("");
        }
        if (Utils.isArabicLangSelected(TripSummaryActivity.this)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                merchandiseTextData.setTextDirection(View.TEXT_DIRECTION_ANY_RTL);
            }
        }
        SpannableStringBuilder builder = new SpannableStringBuilder();
        int flightAmenitiesCnt = 0;
        for (int i = 0; i < iconNameArrayList.size(); i++) {
            if (flightAmenitiesCnt != 0 && flightAmenitiesCnt % 2 == 0) {
                builder.append("\n");
            }
            String[] s = iconNameArrayList.get(i).split("==");
            try {
                if (s.length == 2) {
                    if (!s[0].isEmpty()) {
                        int resId = getResources().getIdentifier(s[0], "drawable", getPackageName());
                        Drawable icon1 = getResources().getDrawable(resId);
                        if (icon1 != null) {
                            int imageScaledSize = getResources().getDimensionPixelSize(R.dimen.search_filter_margin);
                            icon1.setBounds(0, 0, (icon1.getIntrinsicWidth() - imageScaledSize), (icon1.getIntrinsicHeight() - imageScaledSize));
                        }
                        builder.append(" ");
                        builder.setSpan(new ImageSpan(icon1, ImageSpan.ALIGN_BASELINE), builder.length() - 1, builder.length(), 0);
                        builder.append(" " + s[1] + "  ");
                        flightAmenitiesCnt++;
                    } else {
                        if (flightAmenitiesCnt != 0 && flightAmenitiesCnt % 2 == 0) {
                            builder.append(s[1]);
                        } else {
                            builder.append("\n" + s[1]);
                        }
                    }
                } else {
                    int resId = getResources().getIdentifier(s[0], "drawable", getPackageName());
                    Drawable icon1 = getResources().getDrawable(resId);
                    if (icon1 != null) {
                        int imageScaledSize = getResources().getDimensionPixelSize(R.dimen.search_filter_margin);
                        icon1.setBounds(0, 0, (icon1.getIntrinsicWidth() - imageScaledSize), (icon1.getIntrinsicHeight() - imageScaledSize));
                    }
                    builder.append(" ");
                    builder.setSpan(new ImageSpan(icon1, ImageSpan.ALIGN_BASELINE), builder.length() - 1, builder.length(), 0);
                    builder.append(" " + s[1]);
                    builder.append(Utils.addFlightAmenitiesField(s[2], TripSummaryActivity.this));
                    if (s[0].equalsIgnoreCase("avod")) {
                        flightAmenitiesCnt += 1;
                    } else {
                        flightAmenitiesCnt += 2;
                    }
                }
            } catch (Exception e) {
                Utils.printMessage(TAG, "ex:" + e.getMessage());
            }
        }
        merchandiseTextData.setText(builder);
    }

    private void loadAirportcode(ArrayList<OriginDestinationOptionBean> showOdoArray) {
        airportArrayList.clear();
        airlineArrayList.clear();
        try {
            for (int i = 0; i < showOdoArray.size(); i++) {
                for (int j = 0; j < showOdoArray.get(i).getFsDataBeanArrayList().size(); j++) {
                    airportArrayList.add(showOdoArray.get(i).getFsDataBeanArrayList().get(j).getAap());
                    airportArrayList.add(showOdoArray.get(i).getFsDataBeanArrayList().get(j).getDap());
                    airlineArrayList.add(showOdoArray.get(i).getFsDataBeanArrayList().get(j).getMal());
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }
        getAirportDetails();
    }

    private void getAirportDetails() {
        airlineArrayList.add(showOdoArrayList.get(0).getFsDataBeanArrayList().get(0).getDap());
        airlineArrayList.add(showOdoArrayList.get(0).getFsDataBeanArrayList().get(showOdoArrayList.get(0).getFsDataBeanArrayList().size() - 1).getAap());
        airlinesData.resultData(TripSummaryActivity.this, airlineArrayList, new AirlinesInterface<HashMap<String, String>>() {
            @Override
            public void onSuccess(HashMap<String, String> response) {
                if (response.size() > 0) {
                    airLineNamesMap.clear();
                    Utils.printMessage(TAG, "airLineNamesMap ::" + response.size());
                    airLineNamesMap.putAll(response);
                }
            }

            @Override
            public void onFailed(String message) {
                Utils.printMessage(TAG, "DATA::" + message);

            }
        });
        airportArrayList.add(showOdoArrayList.get(0).getFsDataBeanArrayList().get(0).getDap());
        airportArrayList.add(showOdoArrayList.get(0).getFsDataBeanArrayList().get(showOdoArrayList.get(0).getFsDataBeanArrayList().size() - 1).getAap());
        airportData.resultData(TripSummaryActivity.this, airportArrayList, new AirlinesInterface<HashMap<String, FlightAirportName>>() {
            @Override
            public void onSuccess(HashMap<String, FlightAirportName> response) {
                if (response.size() > 0) {
                    airportNamesMap.clear();
                    Utils.printMessage(TAG, "airportNamesMap ::" + response.size());
                    airportNamesMap.putAll(response);
                }
            }

            @Override
            public void onFailed(String message) {
                Utils.printMessage(TAG, "DATA::" + message);

            }
        });
    }

    private void handleEranLoyaltyRequest() {
        String earnPoints = handleEarnLoyaltyPoints();
        showLoading();
        HTTPAsync async = new HTTPAsync(TripSummaryActivity.this, TripSummaryActivity.this, Constants.CALCULATE_EARN_POINTS,
                "", earnPoints, GET_LOYALTY_POINTS, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private String handleEarnLoyaltyPoints() {
        JSONObject sourceObj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        try {
            String ddt = "";
            String ardt = "";
            String sourceSector = "";
            String sourceCountry = "";
            String destinationSector = "";
            String destingationCountry = "";
            ddt = showOdoArrayList.get(0).getFsDataBeanArrayList().get(0).getDdt();
            ardt = showOdoArrayList.get(showOdoArrayList.size() - 1).getFsDataBeanArrayList().get(0).getDdt();
            for (int i = 0; i < showOdoArrayList.size(); i++) {
                sourceSector = showOdoArrayList.get(0).getFsDataBeanArrayList().get(0).getDap();
                sourceCountry = airportNamesMap.get(sourceSector).getCityCode();
                destinationSector = showOdoArrayList.get(0).getFsDataBeanArrayList().get(showOdoArrayList.get(0).getFsDataBeanArrayList().size() - 1).getAap();
                destingationCountry = airportNamesMap.get(destinationSector).getCityCode();
            }
            sourceName = sourceSector;
            destinationName = destinationSector;
            JSONObject flightObj = new JSONObject();
            JSONObject routeObj = new JSONObject();
            sourceObj.put("apikey", Constants.MERCHANDISE_API_KEY);
            sourceObj.put("userid", pref.getString(Constants.USER_UNIQUE_ID, ""));
            sourceObj.put("product", "flight");
            sourceObj.put("currency", "SAR");
            sourceObj.put("basefareAmount", baseFare); //Double.parseDouble(Utils.convertOtherCurrencytoSarCurrency(baseFare, TripSummaryActivity.this))
            sourceObj.put("totalAmount", totalFare); //Double.parseDouble(Utils.convertOtherCurrencytoSarCurrency(totalFare, TripSummaryActivity.this))
            sourceObj.put("companyId", Constants.LOYALTY_COMPANY_ID);
            flightObj.put("gds", Utils.checkInfoSourceVal(infoSource));
            flightObj.put("airline", showOdoArrayList.get(0).getFsDataBeanArrayList().get(0).getMal());
            flightObj.put("nop", passengerCount);
            flightObj.put("cartype", "Full cost Carrier");
            routeObj.put("destinationSector", destinationSector);
            routeObj.put("destinationCountry", destingationCountry);
            routeObj.put("sourceCountry", sourceCountry);
            routeObj.put("sourceSector", sourceSector);
            flightObj.put("route", routeObj);
            sourceObj.put("flight", flightObj);
            sourceObj.put("bookingDate", timeStamp + ".000Z");
            sourceObj.put("travelPeriodFrom", ddt + ".000Z");
            sourceObj.put("travelPeriodTo", ardt + ".000Z");
            sourceObj.put("groupType", groupType);
            Utils.printMessage("handleEarnLoyaltyPoints", sourceObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sourceObj.toString();
    }

    private void getTokenFromServer() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(TripSummaryActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(TripSummaryActivity.this, TripSummaryActivity.this,
                Constants.CLIENT_AUTHENTICATION, "", obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }
}