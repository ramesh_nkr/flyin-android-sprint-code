package com.flyin.bookings;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.adapter.CountryAdapter;
import com.flyin.bookings.dialog.SignInDialog;
import com.flyin.bookings.dialog.TravellerListDialog;
import com.flyin.bookings.model.CountryListBean;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.model.TravellerDetailsBean;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.DbHandler;
import com.flyin.bookings.util.Utils;

import net.alhazmy13.hijridatepicker.HijriCalendarDialog;
import net.alhazmy13.hijridatepicker.HijriCalendarView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FlightBookingPassengerInfoActivity extends AppCompatActivity implements HijriCalendarView.OnDateSetListener {
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private int selectedUserPosition = -1;
    private TravellerDetailsBean travellersBean = null;
    private SignInDialog signInDialog;
    private static final int SEARCH_NATIONALITY_RESULT = 4;
    private static final int SEARCH_PASSPORT_RESULT = 5;
    private static final int SEARCH_MOBILE_CODE = 6;
    private static final int SEARCH_IQAMA_NATIONALITY = 7;
    private int loginDetailsSuccess = -1;
    private static final String TAG = "FlightBookingPassengerInfoActivity";
    private String tdr = "";
    private boolean isUserSignIn;
    private ArrayList<TravellerDetailsBean> travellerArrayList;
    private boolean isAirportAvailable;
    private ArrayList<CountryListBean> countryNamesArray;
    private boolean isPassportRequired;
    protected CountryAdapter mAdapter;
    private boolean isIqamaCountryAvailable;
    private int infoSource = -1;
    private boolean isArabicLang = false;
    private String passportSelectedDate = "";
    private String passportIssuedDate = "";
    private String currentDate = "";
    private String adultDateOfBirth = "";
    private String childDateOfBirth = "";
    private String infantDateOfBirth = "";
    String dateOfBirthText = "";
    String passportExpiryText = "";
    String passportIssueText = "";
    String countryNameText = "";
    String countryCodeText = "";
    String nationalityNameText = "";
    String nationalityCodeText = "";
    private Calendar calendar;
    private HijriCalendarDialog.Language hijriLanguage = null;
    private TextView passengerBirth = null;
    private TextView passengerTitle = null;
    private TextView passPortCountry = null;
    private TextView passengerNationality = null;
    private TextView mealPrefText = null;
    private TextView seatPrefText = null;
    private TextView specialAssistText = null;
    private LinearLayout travellerAccountLayout = null;
    private LinearLayout countryLayout = null;
    private LinearLayout nationalityLayout = null;
    private LinearLayout splReqLayout = null;
    private LinearLayout passportExpiryLayout = null;
    private EditText passengerFirstName;
    private EditText passengerLastName;
    private EditText passengerEmail;
    private EditText passengerPassportNumber;
    private TextView passportExpiry = null;
    private TextView passportNumberHeader = null;
    private TextView nationalityHeader = null;
    private TextView passportCountryHeader = null;
    private TextView passportExpiryHeader = null;
    private ImageView splRequestImage = null;
    private RadioGroup passportTypeRadio;
    private RadioButton passportRadio;
    private RadioButton nationalIdRadio;
    private RadioButton iqamaIdRadio;
    private EditText passengerMobile;
    private ImageView countryImage = null;
    private TextView countryCodeNumberText = null;
    private CheckBox hijriCheckBox;
    private LinearLayout passportIssuedLayout = null;
    private TextView passportIssuedText = null;
    private ImageView passengerBirthCloseIcon = null;
    private ImageView nationalityCloseIcon = null;
    private ImageView passportCountryCloseIcon = null;
    private ImageView passportExpiryCloseIcon = null;
    private ImageView passportIssueCloseIcon = null;
    private ImageView mealPrefCloseIcon = null;
    private ImageView seatPrefCloseIcon = null;
    private ImageView specialAsstCloseIcon = null;
    private LinearLayout frequent_req_layout = null;
    private TextView flyer_guide_pref = null;
    private EditText freq_number_pref;
    private ImageView frequent_layout_image = null;
    private ImageView flyerGuideCloseIcon = null;
    private ArrayList<String> airlineCodeArr = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flight_traveller_details);
        travellerArrayList = new ArrayList<>();
        hijriLanguage = HijriCalendarDialog.Language.English;
        calendar = Calendar.getInstance();
        airlineCodeArr = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isUserSignIn = bundle.getBoolean(Constants.IS_USER_LOGGED, false);
            selectedUserPosition = Integer.parseInt(bundle.getString(Constants.SELECTED_USER_POSITION, ""));
            tdr = bundle.getString(Constants.TDR_VALUE, "");
            airlineCodeArr = getIntent().getStringArrayListExtra(Constants.AIRLINE_NAME);
            isPassportRequired = bundle.getBoolean("isPassportRequired", false);
            isAirportAvailable = bundle.getBoolean("isAirportAvailable", false);
            isIqamaCountryAvailable = bundle.getBoolean("isIqamaCountryAvailable", false);
            //travellerArrayList = (ArrayList<TravellerDetailsBean>) getIntent().getSerializableExtra("traveller_list");
            infoSource = Integer.parseInt(bundle.getString(Constants.INFO_SOURCE, ""));
        }
        travellerArrayList.addAll(Singleton.getInstance().fetchTravellerArrayList);
        currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(calendar.getTime());
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_MEDIUM;
        isArabicLang = false;
        if (Utils.isArabicLangSelected(FlightBookingPassengerInfoActivity.this)) {
            isArabicLang = true;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
        }
        Typeface titleFace = Typeface.createFromAsset(getAssets(), fontTitle);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontText);
        travellersBean = Singleton.getInstance().travellersInfoBean;
        passengerTitle = (TextView) findViewById(R.id.passenger_title);
        TextView travellerAccount = (TextView) findViewById(R.id.traveller_account_text);
        TextView additionalInfo = (TextView) findViewById(R.id.traveller_additional_information);
        TextView specialRequest = (TextView) findViewById(R.id.special_requests_text);
        passPortCountry = (TextView) findViewById(R.id.passenger_passport_country);
        passengerNationality = (TextView) findViewById(R.id.passenger_nationality);
        mealPrefText = (TextView) findViewById(R.id.passenger_meal_pref);
        seatPrefText = (TextView) findViewById(R.id.passenger_seat_pref);
        specialAssistText = (TextView) findViewById(R.id.passenger_special_assistance);
        LinearLayout singInLayout = (LinearLayout) findViewById(R.id.sing_in_layout);
        travellerAccountLayout = (LinearLayout) findViewById(R.id.traveller_account_text_layout);
        RelativeLayout additionalInfoLayout = (RelativeLayout) findViewById(R.id.additional_info_layout);
        RelativeLayout specialRequestLayout = (RelativeLayout) findViewById(R.id.special_requests_layout);
        LinearLayout titleLayout = (LinearLayout) findViewById(R.id.title_layout);
        LinearLayout passengerBirthLayout = (LinearLayout) findViewById(R.id.passenger_birth_layout);
        countryLayout = (LinearLayout) findViewById(R.id.passenger_passport_country_layout);
        nationalityLayout = (LinearLayout) findViewById(R.id.passenger_nationality_layout);
        LinearLayout mealPrefLayout = (LinearLayout) findViewById(R.id.meal_pref_layout);
        LinearLayout seatPrefLayout = (LinearLayout) findViewById(R.id.seat_pref_layout);
        LinearLayout specialAssistLayout = (LinearLayout) findViewById(R.id.special_assistance_layout);
        LinearLayout splRequestLayout = (LinearLayout) findViewById(R.id.special_requests_text_layout);
        splReqLayout = (LinearLayout) findViewById(R.id.special_req_layout);
        passportExpiryLayout = (LinearLayout) findViewById(R.id.passport_expiry_layout);
        LinearLayout passengermobileLayout = (LinearLayout) findViewById(R.id.passenger_mobile_number_layout);
        passengerFirstName = (EditText) findViewById(R.id.passenger_first_name);
        passengerLastName = (EditText) findViewById(R.id.passenger_last_name);
        passengerEmail = (EditText) findViewById(R.id.passenger_email_address);
        passengerBirth = (TextView) findViewById(R.id.passenger_birth);
        passengerPassportNumber = (EditText) findViewById(R.id.passenger_passport_no);
        passportExpiry = (TextView) findViewById(R.id.passenger_passport_expiry);
        TextView titleHeader = (TextView) findViewById(R.id.passenger_title_header);
        TextView firstNameHeader = (TextView) findViewById(R.id.passenger_first_name_header);
        TextView lastNameHeader = (TextView) findViewById(R.id.passenger_last_name_header);
        TextView emailHeader = (TextView) findViewById(R.id.passenger_email_address_header);
        TextView mobileHeader = (TextView) findViewById(R.id.passenger_mobile_number_header);
        TextView dateOfBirthHeader = (TextView) findViewById(R.id.passenger_birth_header);
        passportNumberHeader = (TextView) findViewById(R.id.passenger_passport_no_header);
        nationalityHeader = (TextView) findViewById(R.id.passenger_nationality_header);
        passportCountryHeader = (TextView) findViewById(R.id.passenger_passport_country_header);
        passportExpiryHeader = (TextView) findViewById(R.id.passenger_passport_expiry_header);
        TextView mealPrefHeader = (TextView) findViewById(R.id.passenger_meal_pref_header);
        TextView seatPrefHeader = (TextView) findViewById(R.id.passenger_seat_pref_header);
        TextView specialAsstHeader = (TextView) findViewById(R.id.special_assistance_header);
        splRequestImage = (ImageView) findViewById(R.id.special_requests_image);
        passportTypeRadio = (RadioGroup) findViewById(R.id.passport_type_radio_group);
        passportRadio = (RadioButton) findViewById(R.id.passport_radio);
        nationalIdRadio = (RadioButton) findViewById(R.id.national_id_radio);
        iqamaIdRadio = (RadioButton) findViewById(R.id.iqama_id_radio);
        passengerMobile = (EditText) findViewById(R.id.passenger_mobile_numbers);
        LinearLayout countryCodeLayout = (LinearLayout) findViewById(R.id.country_code_layout);
        countryImage = (ImageView) findViewById(R.id.image);
        countryCodeNumberText = (TextView) findViewById(R.id.country_code_number_text);
        hijriCheckBox = (CheckBox) findViewById(R.id.checkBox);
        TextView orTextView = (TextView) findViewById(R.id.or_text_view);
        passportIssuedLayout = (LinearLayout) findViewById(R.id.passport_issued_layout);
        TextView passportIssuedHeader = (TextView) findViewById(R.id.passenger_passport_issued_header);
        passportIssuedText = (TextView) findViewById(R.id.passenger_passport_issued);
        passengerBirthCloseIcon = (ImageView) findViewById(R.id.passenger_birth_close_icon);
        nationalityCloseIcon = (ImageView) findViewById(R.id.nationality_close_icon);
        passportCountryCloseIcon = (ImageView) findViewById(R.id.passport_country_close_icon);
        passportExpiryCloseIcon = (ImageView) findViewById(R.id.passport_expiry_close_icon);
        passportIssueCloseIcon = (ImageView) findViewById(R.id.passport_issued_close_icon);
        mealPrefCloseIcon = (ImageView) findViewById(R.id.meal_pref_close_icon);
        seatPrefCloseIcon = (ImageView) findViewById(R.id.seat_pref_close_icon);
        specialAsstCloseIcon = (ImageView) findViewById(R.id.special_assistance_close_icon);
        Button doneButton = (Button) findViewById(R.id.done_button);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        LinearLayout frequent_flyer_text_layout = (LinearLayout) findViewById(R.id.frequent_flyer_text_layout);
        frequent_req_layout = (LinearLayout) findViewById(R.id.frequent_req_layout);
        LinearLayout flyer_guide_layout = (LinearLayout) findViewById(R.id.flyer_guide_layout);
        TextView frequentflyerlayout_title = (TextView) findViewById(R.id.frequent_flyer_layout_text);
        TextView flyerguide_header = (TextView) findViewById(R.id.flyer_guide_header);
        TextView freqnumber_header = (TextView) findViewById(R.id.freq_number_header);
        flyer_guide_pref = (TextView) findViewById(R.id.flyer_guide_pref);
        freq_number_pref = (EditText) findViewById(R.id.freq_number_pref);
        frequent_layout_image = (ImageView) findViewById(R.id.frequent_layout_image);
        flyerGuideCloseIcon = (ImageView) findViewById(R.id.flyer_guide_close_icon);
        errorMessageText.setTypeface(titleFace);
        titleHeader.append(Utils.getMandatoryField());
        firstNameHeader.append(Utils.getMandatoryField());
        lastNameHeader.append(Utils.getMandatoryField());
        emailHeader.append(Utils.getMandatoryField());
        mobileHeader.append(Utils.getMandatoryField());
        dateOfBirthHeader.append(Utils.getMandatoryField());
        passportNumberHeader.append(Utils.getMandatoryField());
        passportCountryHeader.append(Utils.getMandatoryField());
        passportExpiryHeader.append(Utils.getMandatoryField());
        passportIssuedHeader.append(Utils.getMandatoryField());
        nationalityHeader.append(Utils.getMandatoryField());
        frequentflyerlayout_title.setTypeface(titleFace);
        flyerguide_header.setTypeface(titleFace);
        freqnumber_header.setTypeface(titleFace);
        flyer_guide_pref.setTypeface(titleFace);
        freq_number_pref.setTypeface(titleFace);
        if (Utils.isArabicLangSelected(FlightBookingPassengerInfoActivity.this)) {
            hijriLanguage = HijriCalendarDialog.Language.Arabic;
            travellerAccount.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            orTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            titleHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            firstNameHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            lastNameHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            emailHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            mobileHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            dateOfBirthHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passportNumberHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            nationalityHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passportCountryHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passportExpiryHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passportIssuedHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            mealPrefHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            seatPrefHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            specialAsstHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerFirstName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerLastName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerEmail.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerMobile.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            countryCodeNumberText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passengerBirth.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            additionalInfo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerPassportNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passportExpiry.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passportIssuedText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            passportRadio.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            nationalIdRadio.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            iqamaIdRadio.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            passPortCountry.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            passengerNationality.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            mealPrefText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            seatPrefText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            specialAssistText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            specialRequest.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            hijriCheckBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            doneButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                passengerTitle.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerFirstName.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerLastName.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerEmail.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerMobile.setTextDirection(View.TEXT_DIRECTION_LTR);
                passengerBirth.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerPassportNumber.setTextDirection(View.TEXT_DIRECTION_RTL);
                passPortCountry.setTextDirection(View.TEXT_DIRECTION_RTL);
                passportExpiry.setTextDirection(View.TEXT_DIRECTION_RTL);
                passportIssuedText.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengerNationality.setTextDirection(View.TEXT_DIRECTION_RTL);
                seatPrefText.setTextDirection(View.TEXT_DIRECTION_RTL);
                mealPrefText.setTextDirection(View.TEXT_DIRECTION_RTL);
                passengermobileLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                specialAssistText.setTextDirection(View.TEXT_DIRECTION_RTL);
                titleHeader.setGravity(Gravity.START);
                titleHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                firstNameHeader.setGravity(Gravity.START);
                firstNameHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                lastNameHeader.setGravity(Gravity.START);
                lastNameHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                emailHeader.setGravity(Gravity.START);
                emailHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                mobileHeader.setGravity(Gravity.START);
                mobileHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                dateOfBirthHeader.setGravity(Gravity.START);
                dateOfBirthHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                passportNumberHeader.setGravity(Gravity.START);
                passportNumberHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                nationalityHeader.setGravity(Gravity.START);
                nationalityHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                passportCountryHeader.setGravity(Gravity.START);
                passportCountryHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                passportExpiryHeader.setGravity(Gravity.START);
                passportExpiryHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                passportIssuedHeader.setGravity(Gravity.START);
                passportIssuedHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                mealPrefHeader.setGravity(Gravity.START);
                mealPrefHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                seatPrefHeader.setGravity(Gravity.START);
                seatPrefHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                specialAsstHeader.setGravity(Gravity.START);
                specialAsstHeader.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                flyer_guide_pref.setTextDirection(View.TEXT_DIRECTION_RTL);
                freq_number_pref.setTextDirection(View.TEXT_DIRECTION_RTL);
                flyerguide_header.setGravity(Gravity.START);
                flyerguide_header.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                freqnumber_header.setGravity(Gravity.START);
                freqnumber_header.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            }
            frequentflyerlayout_title.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            flyerguide_header.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            freqnumber_header.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            flyer_guide_pref.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            freq_number_pref.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        travellerAccount.setTypeface(textFace);
        titleHeader.setTypeface(titleFace);
        passengerTitle.setTypeface(titleFace);
        firstNameHeader.setTypeface(titleFace);
        passengerFirstName.setTypeface(titleFace);
        lastNameHeader.setTypeface(titleFace);
        passengerLastName.setTypeface(titleFace);
        emailHeader.setTypeface(titleFace);
        passengerEmail.setTypeface(titleFace);
        mobileHeader.setTypeface(titleFace);
        passengerMobile.setTypeface(titleFace);
        dateOfBirthHeader.setTypeface(titleFace);
        passengerBirth.setTypeface(titleFace);
        additionalInfo.setTypeface(titleFace);
        passportNumberHeader.setTypeface(titleFace);
        passengerPassportNumber.setTypeface(titleFace);
        passportCountryHeader.setTypeface(titleFace);
        passPortCountry.setTypeface(titleFace);
        passportExpiryHeader.setTypeface(titleFace);
        passportIssuedHeader.setTypeface(titleFace);
        passportExpiry.setTypeface(titleFace);
        passportIssuedText.setTypeface(titleFace);
        nationalityHeader.setTypeface(titleFace);
        passengerNationality.setTypeface(titleFace);
        specialRequest.setTypeface(titleFace);
        mealPrefHeader.setTypeface(titleFace);
        mealPrefText.setTypeface(titleFace);
        seatPrefHeader.setTypeface(titleFace);
        seatPrefText.setTypeface(titleFace);
        specialAsstHeader.setTypeface(titleFace);
        specialAssistText.setTypeface(titleFace);
        passportRadio.setTypeface(titleFace);
        nationalIdRadio.setTypeface(titleFace);
        iqamaIdRadio.setTypeface(titleFace);
        hijriCheckBox.setTypeface(titleFace);
        doneButton.setTypeface(textFace);
        if (isUserSignIn) {
            singInLayout.setVisibility(View.VISIBLE);
        } else {
            singInLayout.setVisibility(View.GONE);
        }
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.flight_hotel_result).setVisibility(View.VISIBLE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_view_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_label);
        RelativeLayout passengerInfoLayout = (RelativeLayout) mCustomView.findViewById(R.id.passenger_info_layout);
        final TextView headerLabelText = (TextView) mCustomView.findViewById(R.id.city_names_text);
        passengerInfoLayout.setVisibility(View.GONE);
        backText.setTypeface(titleFace);
        headerLabelText.setTypeface(titleFace);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (travellersBean.getFirstName().isEmpty() && travellersBean.getLastName().isEmpty()) {
//                    travellersBean.setTravellerHeaderName(travellersBean.getTravellerHeaderName());
//                    headerLabelText.setText(travellersBean.getTravellerHeaderName());
//                } else {
//                    travellersBean.setTravellerHeaderName(travellersBean.getFirstName() + " " + travellersBean.getLastName());
//                    headerLabelText.setText(travellersBean.getFirstName() + " " + travellersBean.getLastName());
//                }
                Intent it = getIntent();
                setResult(RESULT_OK, it);
                finish();
            }
        });
        if (Utils.isArabicLangSelected(FlightBookingPassengerInfoActivity.this)) {
            backText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            headerLabelText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.about_header_text_size));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
            headerLabelText.setGravity(Gravity.END);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                headerLabelText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            }
        }
        if (travellersBean.getFirstName().isEmpty() && travellersBean.getLastName().isEmpty()) {
            headerLabelText.setText(travellersBean.getTravellerHeaderName());
        } else {
            headerLabelText.setText(travellersBean.getFirstName() + " " + travellersBean.getLastName());
        }
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        if (isAirportAvailable) {
            nationalIdRadio.setVisibility(View.VISIBLE);
            countryNamesArray = new ArrayList<>();
            CountryListBean countryList = new CountryListBean();
            countryList.setCountryNameArabic("البحرين");
            countryList.setCountryName("Bahrain");
            countryList.setIsoCountryCode("BH");
            countryList.setIsoCountryCode3L("BHR");
            countryNamesArray.add(countryList);
            countryList = new CountryListBean();
            countryList.setCountryNameArabic("الكويت");
            countryList.setCountryName("Kuwait");
            countryList.setIsoCountryCode("KW");
            countryList.setIsoCountryCode3L("KWT");
            countryNamesArray.add(countryList);
            countryList = new CountryListBean();
            countryList.setCountryNameArabic("عمان");
            countryList.setCountryName("Oman");
            countryList.setIsoCountryCode("OM");
            countryList.setIsoCountryCode3L("OMN");
            countryNamesArray.add(countryList);
            countryList = new CountryListBean();
            countryList.setCountryNameArabic("قطر");
            countryList.setCountryName("Qatar");
            countryList.setIsoCountryCode("QA");
            countryList.setIsoCountryCode3L("QAT");
            countryNamesArray.add(countryList);
            countryList = new CountryListBean();
            countryList.setCountryNameArabic("المملكة العربية السعودية");
            countryList.setCountryName("Saudi Arabia");
            countryList.setIsoCountryCode("SA");
            countryList.setIsoCountryCode3L("SAU");
            countryNamesArray.add(countryList);
            countryList = new CountryListBean();
            countryList.setCountryNameArabic("الإمارات العربية المتحدة");
            countryList.setCountryName("United Arab Emirates");
            countryList.setIsoCountryCode("AE");
            countryList.setIsoCountryCode3L("ARE");
            countryNamesArray.add(countryList);
        } else {
            nationalIdRadio.setVisibility(View.GONE);
        }
        if (isIqamaCountryAvailable) {
            if (airlineCodeArr.contains("SV") || airlineCodeArr.contains("XY")) {
                iqamaIdRadio.setVisibility(View.VISIBLE);
            } else {
                iqamaIdRadio.setVisibility(View.GONE);
            }
        } else {
            iqamaIdRadio.setVisibility(View.GONE);
        }
        if (selectedUserPosition != 0) {
            passengermobileLayout.setVisibility(View.GONE);
            mobileHeader.setVisibility(View.GONE);
            passengerEmail.setVisibility(View.GONE);
            emailHeader.setVisibility(View.GONE);
        } else {
            passengermobileLayout.setVisibility(View.VISIBLE);
            mobileHeader.setVisibility(View.VISIBLE);
            passengerEmail.setVisibility(View.VISIBLE);
            emailHeader.setVisibility(View.VISIBLE);
        }
        if (tdr.equalsIgnoreCase("Yes")) {
            isPassportRequired = true;
            additionalInfoLayout.setVisibility(View.VISIBLE);
            if (travellersBean.getPassengerType().equalsIgnoreCase("ADT")) {
                passengerBirthLayout.setVisibility(View.VISIBLE);
            }
        } else {
            isPassportRequired = false;
            additionalInfoLayout.setVisibility(View.GONE);
            if (travellersBean.getPassengerType().equalsIgnoreCase("ADT")) {
                passengerBirthLayout.setVisibility(View.GONE);
            }
        }
        passportRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passportRadio.setChecked(true);
                nationalIdRadio.setChecked(false);
                iqamaIdRadio.setChecked(false);
                travellersBean.setHijriChecked(false);
                passportTypeRadio.setVisibility(View.VISIBLE);
                passportNumberHeader.setText(getString(R.string.label_traveller_passport));
                passportNumberHeader.append(Utils.getMandatoryField());
                passengerPassportNumber.setHint(getString(R.string.label_error_passport_valid_message));
                passengerPassportNumber.setText("");
                nationalityHeader.setVisibility(View.VISIBLE);
                nationalityLayout.setVisibility(View.VISIBLE);
                passportCountryHeader.setVisibility(View.VISIBLE);
                countryLayout.setVisibility(View.VISIBLE);
                passportExpiryLayout.setVisibility(View.VISIBLE);
                nationalityCloseIcon.setVisibility(View.INVISIBLE);
                passportCountryCloseIcon.setVisibility(View.INVISIBLE);
                passportExpiryCloseIcon.setVisibility(View.INVISIBLE);
                passportIssueCloseIcon.setVisibility(View.INVISIBLE);
                hijriCheckBox.setVisibility(View.GONE);
                passportExpiryHeader.setText(getString(R.string.label_traveller_passportExp));
                passportExpiryHeader.append(Utils.getMandatoryField());
                travellersBean.setIqamaSelected(false);
                travellersBean.setDocumentType("2");
                travellersBean.setIsPassportSelected(true);
                travellersBean.setPassportNumber("");
                travellersBean.setPassportIssuingCountry("");
                travellersBean.setPassportIssuingCountryCode("");
                travellersBean.setNationality("");
                travellersBean.setPassportExpiry("");
                travellersBean.setPassportIssueDate("");
                passportExpiry.setText("");
                passportIssuedText.setText("");
                passengerNationality.setText("");
                passPortCountry.setText("");
                if (airlineCodeArr.contains("XY")) {
                    passportIssuedLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        nationalIdRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passportRadio.setChecked(false);
                nationalIdRadio.setChecked(true);
                iqamaIdRadio.setChecked(false);
                travellersBean.setHijriChecked(false);
                passportTypeRadio.setVisibility(View.VISIBLE);
                passportNumberHeader.setText(getString(R.string.label_national_id_text));
                passportNumberHeader.append(Utils.getMandatoryField());
                passengerPassportNumber.setHint(getString(R.string.label_hint_national_id_msg));
                passengerPassportNumber.setText("");
                nationalityHeader.setVisibility(View.VISIBLE);
                nationalityLayout.setVisibility(View.VISIBLE);
                passportCountryHeader.setVisibility(View.VISIBLE);
                countryLayout.setVisibility(View.VISIBLE);
                passportExpiryLayout.setVisibility(View.GONE);
                hijriCheckBox.setVisibility(View.GONE);
                passportIssuedLayout.setVisibility(View.GONE);
                nationalityCloseIcon.setVisibility(View.INVISIBLE);
                passportCountryCloseIcon.setVisibility(View.INVISIBLE);
                passportExpiryCloseIcon.setVisibility(View.INVISIBLE);
                passportIssueCloseIcon.setVisibility(View.INVISIBLE);
                travellersBean.setIqamaSelected(false);
                travellersBean.setDocumentType("3");
                travellersBean.setIsPassportSelected(false);
                travellersBean.setPassportNumber("");
                travellersBean.setPassportIssuingCountry("");
                travellersBean.setPassportIssuingCountryCode("");
                travellersBean.setNationality("");
                travellersBean.setPassportIssueDate("");
                passportExpiry.setText("");
                passportIssuedText.setText("");
                passengerNationality.setText("");
                passPortCountry.setText("");
            }
        });
        iqamaIdRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passportRadio.setChecked(false);
                nationalIdRadio.setChecked(false);
                iqamaIdRadio.setChecked(true);
                passportNumberHeader.setText(getString(R.string.iqama_title_string));
                passportNumberHeader.append(Utils.getMandatoryField());
                passengerPassportNumber.setHint(getString(R.string.iqama_title_string));
                passengerPassportNumber.setText("");
                nationalityHeader.setVisibility(View.VISIBLE);
                nationalityLayout.setVisibility(View.VISIBLE);
                passportExpiryLayout.setVisibility(View.VISIBLE);
                passportCountryHeader.setVisibility(View.GONE);
                nationalityCloseIcon.setVisibility(View.INVISIBLE);
                passportCountryCloseIcon.setVisibility(View.INVISIBLE);
                passportExpiryCloseIcon.setVisibility(View.INVISIBLE);
                passportIssueCloseIcon.setVisibility(View.INVISIBLE);
                countryLayout.setVisibility(View.GONE);
                passportExpiryHeader.setText(getString(R.string.label_traveller_details_iqama_expiry));
                passportExpiryHeader.append(Utils.getMandatoryField());
                passportExpiry.setHint(getString(R.string.label_hint_passport_expiry_msg));
                hijriCheckBox.setVisibility(View.VISIBLE);
                passportIssuedLayout.setVisibility(View.GONE);
                travellersBean.setIqamaSelected(true);
                travellersBean.setDocumentType("4");
                hijriCheckBox.setChecked(true);
                travellersBean.setIsPassportSelected(false);
                travellersBean.setPassportNumber("");
                travellersBean.setHijriChecked(true);
                travellersBean.setPassportIssuingCountry("");
                travellersBean.setPassportIssuingCountryCode("");
                travellersBean.setNationality("");
                travellersBean.setPassportExpiry("");
                travellersBean.setPassportIssueDate("");
                passportExpiry.setText("");
                passportIssuedText.setText("");
                passengerNationality.setText("");
                passPortCountry.setText("");
            }
        });
        if ((travellersBean.getPassengerType().equalsIgnoreCase("ADT") ||
                travellersBean.getPassengerType().equalsIgnoreCase("CHD")) && infoSource == 0) {
            specialRequestLayout.setVisibility(View.VISIBLE);
        } else {
            specialRequestLayout.setVisibility(View.GONE);
        }
        passengerTitle.setText(travellersBean.getTitle());
        titleLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog titleDialog = new TravellerListDialog(FlightBookingPassengerInfoActivity.this, Constants.TITLE_SELECTION, null, travellersBean.getPassengerType(), null, false);
                titleDialog.setCancelable(true);
                titleDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (titleDialog != null) {
                            if (!titleDialog.selectedListItem.equalsIgnoreCase("")) {
                                travellersBean.setTitle(titleDialog.selectedListItem);
                                travellersBean.setTravellerHeader(travellersBean.getTitle() + " " + travellersBean.getFirstName() + " " + travellersBean.getLastName());
                                passengerTitle.setText(travellersBean.getTitle());
                            }
                        }
                    }
                });
                titleDialog.show();
            }
        });
        passengerFirstName.setText(travellersBean.getFirstName());
        passengerFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                travellersBean.setFirstName(passengerFirstName.getText().toString());
            }
        });
        passengerFirstName.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!passengerFirstName.getText().toString().equalsIgnoreCase("")) {
                        travellersBean.setTravellerHeader(travellersBean.getTitle() + " " + travellersBean.getFirstName() + " " + travellersBean.getLastName());
                    }
                }
            }
        });
        passengerLastName.setText(travellersBean.getLastName());
        passengerLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                travellersBean.setLastName(passengerLastName.getText().toString());
            }
        });
        passengerLastName.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!passengerLastName.getText().toString().equalsIgnoreCase("")) {
                        travellersBean.setTravellerHeader(travellersBean.getTitle() + " " + travellersBean.getFirstName() + " " + travellersBean.getLastName());
                    }
                }
            }
        });
        if (travellersBean.getmResId() == 0) {
            countryImage.setImageResource(R.drawable.ksa);
            travellersBean.setPhoneCode("+966");
        } else {
            countryImage.setImageResource(travellersBean.getmResId());
        }
        countryCodeNumberText.setText(travellersBean.getPhoneCode());
        countryCodeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(FlightBookingPassengerInfoActivity.this, SearchCountryActivity.class);
                intent1.putExtra(Constants.IS_MOBILE_NUMBER_CLICKED, true);
                String selectedPhoneCode = travellersBean.getPhoneCode();
                if (!selectedPhoneCode.isEmpty()) {
                    intent1.putExtra(Constants.SELECTED_VALUE, selectedPhoneCode);
                }
                startActivityForResult(intent1, SEARCH_MOBILE_CODE);

            }
        });
        passengerMobile.setText(travellersBean.getPhoneNo());
        passengerMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                travellersBean.setPhoneNo(passengerMobile.getText().toString());
            }
        });
        passengerEmail.setText(travellersBean.getEmail());
        passengerEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                travellersBean.setEmail(passengerEmail.getText().toString());
            }
        });
        passengerBirth.setText(travellersBean.getDateOfBirth());
        if (!travellersBean.getDateOfBirth().isEmpty()) {
            passengerBirthCloseIcon.setVisibility(View.VISIBLE);
        }
        passengerBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateofBirthForPassenger();
            }
        });
        if (travellersBean.getDocumentType().equalsIgnoreCase("2")) {
            passportRadio.setChecked(true);
            nationalIdRadio.setChecked(false);
            iqamaIdRadio.setChecked(false);
            travellersBean.setHijriChecked(false);
            travellersBean.setIqamaSelected(false);
            travellersBean.setIsPassportSelected(true);
            passportTypeRadio.setVisibility(View.VISIBLE);
            nationalityHeader.setVisibility(View.VISIBLE);
            nationalityLayout.setVisibility(View.VISIBLE);
            passportCountryHeader.setVisibility(View.VISIBLE);
            countryLayout.setVisibility(View.VISIBLE);
            passportExpiryLayout.setVisibility(View.VISIBLE);
            hijriCheckBox.setVisibility(View.GONE);
            passportNumberHeader.setText(getString(R.string.label_traveller_passport));
            passportNumberHeader.append(Utils.getMandatoryField());
            passportExpiryHeader.setText(getString(R.string.label_traveller_passportExp));
            passportExpiryHeader.append(Utils.getMandatoryField());
            passengerPassportNumber.setHint(getString(R.string.label_error_passport_valid_message));
            if (travellersBean.getPassportNumber().isEmpty()) {
                passengerPassportNumber.setText("");
            } else {
                passengerPassportNumber.setText(travellersBean.getPassportNumber());
            }
            if (travellersBean.getNationality().isEmpty()) {
                passengerNationality.setText("");
            } else {
                passengerNationality.setText(travellersBean.getNationality());
            }
            if (travellersBean.getPassportIssuingCountry().isEmpty()) {
                passPortCountry.setText("");
            } else {
                passPortCountry.setText(travellersBean.getPassportIssuingCountry());
            }
            if (travellersBean.getPassportExpiry().isEmpty()) {
                passportExpiry.setText("");
            } else {
                passportExpiry.setText(travellersBean.getPassportExpiry());
            }
            if (travellersBean.getPassportIssueDate().isEmpty()) {
                passportIssuedText.setText("");
            } else {
                passportIssuedText.setText(travellersBean.getPassportIssueDate());
            }
            if (airlineCodeArr.contains("XY")) {
                passportIssuedLayout.setVisibility(View.VISIBLE);
            }
        } else if (travellersBean.getDocumentType().equalsIgnoreCase("3")) {
            passportRadio.setChecked(false);
            nationalIdRadio.setChecked(true);
            iqamaIdRadio.setChecked(false);
            travellersBean.setIqamaSelected(false);
            travellersBean.setHijriChecked(false);
            travellersBean.setIsPassportSelected(false);
            passportTypeRadio.setVisibility(View.VISIBLE);
            nationalityHeader.setVisibility(View.VISIBLE);
            nationalityLayout.setVisibility(View.VISIBLE);
            passportCountryHeader.setVisibility(View.VISIBLE);
            countryLayout.setVisibility(View.VISIBLE);
            passportExpiryLayout.setVisibility(View.GONE);
            hijriCheckBox.setVisibility(View.GONE);
            passportIssuedLayout.setVisibility(View.GONE);
            passportNumberHeader.setText(getString(R.string.label_national_id_text));
            passportNumberHeader.append(Utils.getMandatoryField());
            passengerPassportNumber.setHint(getString(R.string.label_hint_national_id_msg));
            if (travellersBean.getPassportNumber().isEmpty()) {
                passengerPassportNumber.setText("");
            } else {
                passengerPassportNumber.setText(travellersBean.getPassportNumber());
            }
            if (travellersBean.getNationality().isEmpty()) {
                passengerNationality.setText("");
            } else {
                passengerNationality.setText(travellersBean.getNationality());
            }
            if (travellersBean.getPassportIssuingCountry().isEmpty()) {
                passPortCountry.setText("");
            } else {
                passPortCountry.setText(travellersBean.getPassportIssuingCountry());
            }
            travellersBean.setPassportIssueDate("");
            passportExpiry.setText("");
            passportIssuedText.setText("");
        } else if (travellersBean.getDocumentType().equalsIgnoreCase("4")) {
            passportRadio.setChecked(false);
            nationalIdRadio.setChecked(false);
            iqamaIdRadio.setChecked(true);
            travellersBean.setIqamaSelected(true);
            travellersBean.setIsPassportSelected(false);
            nationalityHeader.setVisibility(View.VISIBLE);
            nationalityLayout.setVisibility(View.VISIBLE);
            passportExpiryLayout.setVisibility(View.VISIBLE);
            passportCountryHeader.setVisibility(View.GONE);
            countryLayout.setVisibility(View.GONE);
            hijriCheckBox.setVisibility(View.VISIBLE);
            passportIssuedLayout.setVisibility(View.GONE);
            passportNumberHeader.setText(getString(R.string.iqama_title_string));
            passportNumberHeader.append(Utils.getMandatoryField());
            nationalityCloseIcon.setVisibility(View.INVISIBLE);
            passportCountryCloseIcon.setVisibility(View.INVISIBLE);
            passportExpiryCloseIcon.setVisibility(View.INVISIBLE);
            passportIssueCloseIcon.setVisibility(View.INVISIBLE);
            passportExpiryHeader.setText(getString(R.string.label_traveller_details_iqama_expiry));
            passportExpiryHeader.append(Utils.getMandatoryField());
            passportExpiry.setHint(getString(R.string.label_hint_passport_expiry_msg));
            passengerPassportNumber.setHint(getString(R.string.iqama_title_string));
            if (travellersBean.getPassportNumber().isEmpty()) {
                passengerPassportNumber.setText("");
            } else {
                passengerPassportNumber.setText(travellersBean.getPassportNumber());
            }
            if (travellersBean.getNationality().isEmpty()) {
                passengerNationality.setText("");
            } else {
                passengerNationality.setText(travellersBean.getNationality());
            }
            if (travellersBean.getPassportExpiry().isEmpty()) {
                passportExpiry.setText("");
            } else {
                passportExpiry.setText(travellersBean.getPassportExpiry());
            }
            travellersBean.setPassportIssuingCountry("");
            travellersBean.setPassportIssuingCountryCode("");
            travellersBean.setPassportIssueDate("");
            passportIssuedText.setText("");
            passPortCountry.setText("");
        }
        passengerBirthCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travellersBean.setDateOfBirth("");
                passengerBirthCloseIcon.setVisibility(View.INVISIBLE);
                passengerBirth.setText("");
                travellersBean.setPassportIssueDate("");
                passportIssueCloseIcon.setVisibility(View.INVISIBLE);
                passportIssuedText.setText("");
            }
        });
        passengerPassportNumber.setText(travellersBean.getPassportNumber());
        passengerPassportNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                travellersBean.setPassportNumber(passengerPassportNumber.getText().toString());
            }
        });
        if (!travellersBean.getPassportIssuingCountry().isEmpty()) {
            passportCountryCloseIcon.setVisibility(View.VISIBLE);
        }
        passPortCountry.setText(travellersBean.getPassportIssuingCountry());
        countryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!travellersBean.isIqamaSelected()) {
                    if (travellersBean.isPassportSelected()) {
                        View passportView = getCurrentFocus();
                        if (passportView != null) {
                            InputMethodManager passportKeyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            passportKeyboard.hideSoftInputFromWindow(passportView.getWindowToken(), 0);
                        }
                        Intent intent = new Intent(FlightBookingPassengerInfoActivity.this, SearchCountryActivity.class);
                        intent.putExtra(Constants.IS_MOBILE_NUMBER_CLICKED, false);
                        String selectedPassportCountry = travellersBean.getPassportIssuingCountry();
                        if (!selectedPassportCountry.isEmpty()) {
                            intent.putExtra(Constants.SELECTED_VALUE, selectedPassportCountry);
                        }
                        startActivityForResult(intent, SEARCH_PASSPORT_RESULT);
                    } else {
                        final TravellerListDialog nationalityDialog = new TravellerListDialog(FlightBookingPassengerInfoActivity.this, Constants.NATIONALITY_SELECTION, null, "", countryNamesArray, isAirportAvailable);
                        nationalityDialog.setCancelable(true);
                        nationalityDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                if (nationalityDialog != null) {
                                    if (travellersBean.getDocumentType().equalsIgnoreCase("3")) {
                                        if (isAirportAvailable) {
                                            if (!nationalityDialog.selectedListItem.equalsIgnoreCase("")) {
                                                travellersBean.setPassportIssuingCountry(nationalityDialog.selectedListItem);
                                                travellersBean.setPassportIssuingCountryCode(nationalityDialog.selectedListCode);
                                                passPortCountry.setText(travellersBean.getPassportIssuingCountry());
                                                if (!travellersBean.getPassportIssuingCountry().isEmpty()) {
                                                    passportCountryCloseIcon.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        });
                        nationalityDialog.show();
                    }
                }
            }
        });
        passportCountryCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travellersBean.setPassportIssuingCountry("");
                travellersBean.setPassportIssuingCountryCode("");
                passportCountryCloseIcon.setVisibility(View.INVISIBLE);
                passPortCountry.setText("");
            }
        });
        if (!travellersBean.getPassportExpiry().isEmpty()) {
            passportExpiryCloseIcon.setVisibility(View.VISIBLE);
        }
        passportExpiry.setText(travellersBean.getPassportExpiry());
        passportExpiry.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  if (travellersBean.isIqamaSelected()) {
                                                      if (travellersBean.isHijriChecked()) {
                                                          setPassportExpiryDate(true, true);
                                                      } else {
                                                          setPassportExpiryDate(false, true);
                                                      }
                                                  } else {
                                                      setPassportExpiryDate(false, false);
                                                  }
                                                  passportExpiry.setText(travellersBean.getPassportExpiry());
                                              }
                                          }
        );
        passportExpiryCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travellersBean.setPassportExpiry("");
                passportExpiryCloseIcon.setVisibility(View.INVISIBLE);
                passportExpiry.setText("");
            }
        });
        if (!travellersBean.getPassportIssueDate().isEmpty()) {
            passportIssueCloseIcon.setVisibility(View.VISIBLE);
        }
        passportIssuedText.setText(travellersBean.getPassportIssueDate());
        passportIssuedText.setOnClickListener(new View.OnClickListener() {
                                                  @Override
                                                  public void onClick(View v) {
                                                      if (!travellersBean.getDateOfBirth().equalsIgnoreCase("") || !travellersBean.getDateOfBirth().isEmpty()) {
                                                          setPassportIssuedDate();
                                                      } else {
                                                          displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_issue_date_alert_msg));
                                                          return;
                                                      }
                                                  }
                                              }
        );
        passportIssueCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travellersBean.setPassportIssueDate("");
                passportIssueCloseIcon.setVisibility(View.INVISIBLE);
                passportIssuedText.setText("");
            }
        });
        if (!travellersBean.getNationality().isEmpty()) {
            nationalityCloseIcon.setVisibility(View.VISIBLE);
        }
        passengerNationality.setText(travellersBean.getNationality());
        nationalityLayout.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {
                                                     if (!travellersBean.isIqamaSelected()) {
                                                         if (travellersBean.isPassportSelected()) {
                                                             View nationalityView = getCurrentFocus();
                                                             if (nationalityView != null) {
                                                                 InputMethodManager nationalityKeyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                                 nationalityKeyboard.hideSoftInputFromWindow(nationalityView.getWindowToken(), 0);
                                                             }
                                                             Intent it = new Intent(FlightBookingPassengerInfoActivity.this, SearchCountryActivity.class);
                                                             String selectedNationality = travellersBean.getNationality();
                                                             it.putExtra(Constants.IS_MOBILE_NUMBER_CLICKED, false);
                                                             if (!selectedNationality.isEmpty()) {
                                                                 it.putExtra(Constants.SELECTED_VALUE, selectedNationality);
                                                             }
                                                             startActivityForResult(it, SEARCH_NATIONALITY_RESULT);

                                                         } else {
                                                             final TravellerListDialog nationalityDialog = new TravellerListDialog(FlightBookingPassengerInfoActivity.this, Constants.NATIONALITY_SELECTION, null, "", countryNamesArray, isAirportAvailable);
                                                             nationalityDialog.setCancelable(true);
                                                             nationalityDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                                                 @Override
                                                                 public void onDismiss(DialogInterface dialog) {
                                                                     if (nationalityDialog != null) {
                                                                         if (travellersBean.getDocumentType().equalsIgnoreCase("3")) {
                                                                             if (isAirportAvailable) {
                                                                                 if (!nationalityDialog.selectedListItem.equalsIgnoreCase("")) {
                                                                                     travellersBean.setNationality(nationalityDialog.selectedListItem);
                                                                                     travellersBean.setNationalityCode(nationalityDialog.selectedListCode);
                                                                                     passengerNationality.setText(travellersBean.getNationality());
                                                                                     if (!travellersBean.getDocumentType().equalsIgnoreCase("4")) {
                                                                                         travellersBean.setPassportIssuingCountry(nationalityDialog.selectedListItem);
                                                                                         travellersBean.setPassportIssuingCountryCode(nationalityDialog.selectedListCode);
                                                                                     }
                                                                                     passPortCountry.setText(travellersBean.getPassportIssuingCountry());
                                                                                     if (!travellersBean.getNationality().isEmpty()) {
                                                                                         nationalityCloseIcon.setVisibility(View.VISIBLE);
                                                                                     }
                                                                                     if (!travellersBean.getPassportIssuingCountry().isEmpty()) {
                                                                                         passportCountryCloseIcon.setVisibility(View.VISIBLE);
                                                                                     }
                                                                                 }
                                                                             }
                                                                         }
                                                                     }
                                                                 }
                                                             });
                                                             nationalityDialog.show();
                                                         }
                                                     } else if (travellersBean.isIqamaSelected()) {
                                                         View nationalityView = getCurrentFocus();
                                                         if (nationalityView != null) {
                                                             InputMethodManager nationalityKeyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                             nationalityKeyboard.hideSoftInputFromWindow(nationalityView.getWindowToken(), 0);
                                                         }
                                                         Intent it = new Intent(FlightBookingPassengerInfoActivity.this, SearchCountryActivity.class);
                                                         String selectedNationality = travellersBean.getNationality();
                                                         it.putExtra(Constants.IS_MOBILE_NUMBER_CLICKED, false);
                                                         if (!selectedNationality.isEmpty()) {
                                                             it.putExtra(Constants.SELECTED_VALUE, selectedNationality);
                                                         }
                                                         startActivityForResult(it, SEARCH_NATIONALITY_RESULT);
                                                     }
                                                 }
                                             }
        );
        nationalityCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travellersBean.setNationality("");
                nationalityCloseIcon.setVisibility(View.INVISIBLE);
                passengerNationality.setText("");
            }
        });
        if (travellersBean.isSpecialRequestOpened()) {
            specialRequestLayout.setVisibility(View.VISIBLE);
            splRequestImage.setImageResource(R.drawable.top_arrow);
        }
        if (travellersBean.isHijriChecked()) {
            hijriCheckBox.setChecked(true);
        } else {
            hijriCheckBox.setChecked(false);
        }
        if (travellersBean.isSpecialRequestExpanded()) {
            splReqLayout.setVisibility(View.VISIBLE);
            splRequestImage.setImageResource(R.drawable.top_arrow);
        } else {
            splReqLayout.setVisibility(View.GONE);
            splRequestImage.setImageResource(R.drawable.down_arrow);
        }
        hijriCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travellersBean.isHijriChecked()) {
                    travellersBean.setHijriChecked(false);
                    hijriCheckBox.setChecked(false);
                    travellersBean.setPassportExpiry("");
                    passportExpiryCloseIcon.setVisibility(View.INVISIBLE);
                    passportExpiry.setText("");
                } else {
                    travellersBean.setHijriChecked(true);
                    hijriCheckBox.setChecked(true);
                    travellersBean.setPassportExpiry("");
                    passportExpiryCloseIcon.setVisibility(View.INVISIBLE);
                    passportExpiry.setText("");
                }
            }
        });
        splRequestLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travellersBean.isSpecialRequestExpanded()) {
                    splReqLayout.setVisibility(View.GONE);
                    splRequestImage.setImageResource(R.drawable.down_arrow);
                    travellersBean.setIsSpecialRequestExpanded(false);
                } else {
                    splReqLayout.setVisibility(View.VISIBLE);
                    splRequestImage.setImageResource(R.drawable.top_arrow);
                    travellersBean.setIsSpecialRequestExpanded(true);
                }
            }
        });
        if (!travellersBean.getMealPreferences().isEmpty()) {
            mealPrefCloseIcon.setVisibility(View.VISIBLE);
        }
        mealPrefText.setText(travellersBean.getMealPreferences());
        mealPrefLayout.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  final TravellerListDialog mealDialog = new TravellerListDialog(FlightBookingPassengerInfoActivity.this, Constants.MEAL_SELECTION, null, "", null, false);
                                                  mealDialog.setCancelable(true);
                                                  mealDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                                      @Override
                                                      public void onDismiss(DialogInterface dialog) {
                                                          if (mealDialog != null) {
                                                              if (!mealDialog.selectedListItem.equalsIgnoreCase("")) {
                                                                  travellersBean.setIsSpecialRequestOpened(true);
                                                                  travellersBean.setMealPreferences(mealDialog.selectedListItem);
                                                                  travellersBean.setMealPreferencesCode(Utils.getFlightMealName(FlightBookingPassengerInfoActivity.this, mealDialog.selectedListItem));
                                                                  Utils.printMessage(TAG, "Meal Code :: " + Utils.getFlightMealName(FlightBookingPassengerInfoActivity.this, mealDialog.selectedListItem));
                                                                  travellersBean.setIsSpecialRequestOpened(true);
                                                                  mealPrefText.setText(travellersBean.getMealPreferences());
                                                                  mealPrefCloseIcon.setVisibility(View.VISIBLE);
                                                              }
                                                          }
                                                      }
                                                  });
                                                  mealDialog.show();
                                              }
                                          }
        );
        mealPrefCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travellersBean.setMealPreferences("");
                travellersBean.setMealPreferencesCode("");
                mealPrefCloseIcon.setVisibility(View.INVISIBLE);
                mealPrefText.setText("");

            }
        });
        if (!travellersBean.getSeatPreferences().isEmpty()) {
            seatPrefCloseIcon.setVisibility(View.VISIBLE);
        }
        seatPrefText.setText(travellersBean.getSeatPreferences());
        seatPrefLayout.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  final TravellerListDialog seatDialog = new TravellerListDialog(FlightBookingPassengerInfoActivity.this, Constants.SEAT_SELECTION, null, "", null, false);
                                                  seatDialog.setCancelable(true);
                                                  seatDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                                      @Override
                                                      public void onDismiss(DialogInterface dialog) {
                                                          if (seatDialog != null) {
                                                              if (!seatDialog.selectedListItem.equalsIgnoreCase("")) {
                                                                  travellersBean.setIsSpecialRequestOpened(true);
                                                                  travellersBean.setSeatPreferences(seatDialog.selectedListItem);
                                                                  travellersBean.setSeatPreferencesCode(seatDialog.selectedListCode);
                                                                  travellersBean.setIsSpecialRequestOpened(true);
                                                                  seatPrefText.setText(travellersBean.getSeatPreferences());
                                                                  seatPrefCloseIcon.setVisibility(View.VISIBLE);
                                                              }
                                                          }
                                                      }
                                                  });
                                                  seatDialog.show();
                                              }
                                          }
        );
        seatPrefCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travellersBean.setSeatPreferences("");
                travellersBean.setSeatPreferencesCode("");
                seatPrefCloseIcon.setVisibility(View.INVISIBLE);
                seatPrefText.setText("");
            }
        });
        if (!travellersBean.getSpecialAssistance().isEmpty()) {
            specialAsstCloseIcon.setVisibility(View.VISIBLE);
        }
        specialAssistText.setText(travellersBean.getSpecialAssistance());
        specialAssistLayout.setOnClickListener(new View.OnClickListener() {
                                                   @Override
                                                   public void onClick(View v) {
                                                       final TravellerListDialog splAsstDialog = new TravellerListDialog(FlightBookingPassengerInfoActivity.this, Constants.SPECIAL_ASSISTANCE_SELECTION, null, "", null, false);
                                                       splAsstDialog.setCancelable(true);
                                                       splAsstDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                                           @Override
                                                           public void onDismiss(DialogInterface dialog) {
                                                               if (splAsstDialog != null) {
                                                                   if (!splAsstDialog.selectedListItem.equalsIgnoreCase("")) {
                                                                       travellersBean.setIsSpecialRequestOpened(true);
                                                                       travellersBean.setSpecialAssistance(splAsstDialog.selectedListItem);
                                                                       travellersBean.setSpecialAssistanceCode(splAsstDialog.selectedListCode);
                                                                       travellersBean.setIsSpecialRequestOpened(true);
                                                                       specialAssistText.setText(travellersBean.getSpecialAssistance());
                                                                       specialAsstCloseIcon.setVisibility(View.VISIBLE);
                                                                   }
                                                               }
                                                           }
                                                       });
                                                       splAsstDialog.show();
                                                   }
                                               }
        );
        specialAsstCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travellersBean.setSpecialAssistance("");
                travellersBean.setSpecialAssistanceCode("");
                specialAsstCloseIcon.setVisibility(View.INVISIBLE);
                specialAssistText.setText("");

            }
        });
        if (travellersBean.isfrequentflyerExpanded()) {
            frequent_req_layout.setVisibility(View.VISIBLE);
            frequent_layout_image.setImageResource(R.drawable.top_arrow);
        } else {
            frequent_req_layout.setVisibility(View.GONE);
            frequent_layout_image.setImageResource(R.drawable.down_arrow);
        }
        if (!travellersBean.getFrequentflyername().isEmpty()) {
            flyerGuideCloseIcon.setVisibility(View.VISIBLE);
        }
        flyerGuideCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travellersBean.setFrequentflyername("");
                flyerGuideCloseIcon.setVisibility(View.GONE);
                flyer_guide_pref.setText("");

            }
        });
        flyer_guide_pref.setText(travellersBean.getFrequentflyername());
        frequent_flyer_text_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travellersBean.isfrequentflyerExpanded()) {
                    frequent_req_layout.setVisibility(View.GONE);
                    frequent_layout_image.setImageResource(R.drawable.down_arrow);
                    travellersBean.setIsfrequentflyerExpanded(false);
                } else {
                    frequent_req_layout.setVisibility(View.VISIBLE);
                    frequent_layout_image.setImageResource(R.drawable.top_arrow);
                    travellersBean.setIsfrequentflyerExpanded(true);
                }

            }
        });
        freq_number_pref.setText(travellersBean.getFrequentFlyerNumber());
        freq_number_pref.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                travellersBean.setFrequentFlyerNumber(freq_number_pref.getText().toString());
            }
        });
        flyer_guide_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog freqnameDialog = new TravellerListDialog(FlightBookingPassengerInfoActivity.this, Constants.PASSENGER_FREQUENT_NAME, null, "", null, false);
                freqnameDialog.setCancelable(true);
                freqnameDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (freqnameDialog != null) {
                            if (!freqnameDialog.selectedListItem.equalsIgnoreCase("")) {
                                travellersBean.setIsSpecialRequestOpened(true);
                                travellersBean.setFrequentflyername(freqnameDialog.selectedListItem);
                                flyer_guide_pref.setText(travellersBean.getFrequentflyername());
                                flyerGuideCloseIcon.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });
                freqnameDialog.show();
            }
        });
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travellersBean.getFirstName().isEmpty()) {
                    displayErrorMessage(getResources().getString(R.string.label_err_fname_msg));
                    return;
                }
                if (!Utils.isValidName(Utils.getNameWithoutChar(travellersBean.getFirstName()))) {
                    displayErrorMessage(travellersBean.getTravellerHeader() + " - " +
                            getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_firstName));
                    return;
                }
                if (travellersBean.getLastName().isEmpty()) {
                    displayErrorMessage(getResources().getString(R.string.label_err_lname_msg));
                    return;
                }
                if (!Utils.isValidName(Utils.getNameWithoutChar(travellersBean.getLastName()))) {
                    displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.ErrInvalidNameMsg)
                            + " " + getString(R.string.label_traveller_lastName));
                    return;
                }
                if (Utils.getNameWithoutChar(travellersBean.getFirstName()).equalsIgnoreCase(Utils.getNameWithoutChar(travellersBean.getLastName()))) {
                    displayErrorMessage(travellersBean.getTravellerHeader() + " - " +
                            getString(R.string.ErrInvalidNameMsg) + " " + getString(R.string.label_traveller_lastName));
                    return;
                }
                travellersBean.setTravellerHeaderName(travellersBean.getFirstName() + " " + travellersBean.getLastName());
                if (selectedUserPosition == 0) {
                    if (travellersBean.getEmail().isEmpty()) {
                        displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_email_message));
                        return;
                    }
                    if (!Utils.isValidEmail(travellersBean.getEmail())) {
                        displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_email_valid_message));
                        return;
                    }
                    if (travellersBean.getPhoneNo().isEmpty() || !Utils.isValidMobile(travellersBean.getPhoneNo())) {
                        displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_err_phone_msg));
                        return;
                    }
                    travellersBean.setTravellerEmail(travellersBean.getEmail());
                } else {
                    travellersBean.setTravellerEmail("");
                }
                if (airlineCodeArr.contains("XY")) {
                    if (!travellersBean.getDateOfBirth().isEmpty()) {
                        if (travellersBean.getPassengerType().equalsIgnoreCase("ADT")) {
                            if (Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) < (365 * 12)) {
                                displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                                return;
                            }
                        } else if (travellersBean.getPassengerType().equalsIgnoreCase("CHD")) {
                            if (Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) > (365 * 12) ||
                                    Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) < 1) {
                                displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                                return;
                            }
                        } else if (travellersBean.getPassengerType().equalsIgnoreCase("INF")) {
                            if (Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) > (365 * 2) ||
                                    Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) < 1) {
                                displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                                return;
                            }
                        }
                    }
                }
                if (!isPassportRequired) {
                    if (travellersBean.getPassengerType().equalsIgnoreCase("CHD")) {
                        if (travellersBean.getDateOfBirth().isEmpty() || Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) > (365 * 12) || Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) < 1) {
                            displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                            return;
                        }
                    }
                    if (travellersBean.getPassengerType().equalsIgnoreCase("INF")) {
                        if (travellersBean.getDateOfBirth().isEmpty() || Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) > (365 * 2) || Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) < 1) {
                            displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                            return;
                        }
                    }
                }
                if (isPassportRequired) {
                    if (travellersBean.getDateOfBirth().isEmpty()) {
                        displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                        return;
                    }
                    if (travellersBean.getPassengerType().equalsIgnoreCase("ADT")) {
                        if (Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) < (365 * 12)) {
                            displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                            return;
                        }
                    } else if (travellersBean.getPassengerType().equalsIgnoreCase("CHD")) {
                        if (Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) > (365 * 12) ||
                                Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) < 1) {
                            displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                            return;
                        }
                    } else if (travellersBean.getPassengerType().equalsIgnoreCase("INF")) {
                        if (Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) > (365 * 2) ||
                                Integer.parseInt(Utils.dateDifference(currentDate, travellersBean.getDateOfBirth())) < 1) {
                            displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_dob_message));
                            return;
                        }
                    }
                    if (travellersBean.getDocumentType().equalsIgnoreCase("2")) {
                        if (travellersBean.getPassportNumber().isEmpty() || !Utils.isValidPassport(travellersBean.getPassportNumber(), Constants.PASSPOR_NUMBER)) {
                            displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_passport_valid_message));
                            return;
                        }
                    } else if (travellersBean.getDocumentType().equalsIgnoreCase("3") && !travellersBean.isIqamaSelected()) {
                        if (travellersBean.getPassportNumber().isEmpty() || !Utils.isValidPassport(travellersBean.getPassportNumber(), Constants.NATIONAL_ID)) {
                            displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_hint_national_id_msg));
                            return;
                        }
                    }
                    if (travellersBean.getDocumentType().equalsIgnoreCase("4") && travellersBean.isIqamaSelected()) {
                        if (airlineCodeArr.contains("SV") || airlineCodeArr.contains("XY")) {
                            if (travellersBean.getPassportNumber().isEmpty() || !Utils.isValidPassport(travellersBean.getPassportNumber(), Constants.IQAMA_ID)) {
                                displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_err_iqama_number_msg));
                                return;
                            }
                        }
                    }
                    if (travellersBean.getNationality().isEmpty()) {
                        displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_nationality_message));
                        return;
                    }
                    if (travellersBean.getDocumentType().equalsIgnoreCase("2") || (travellersBean.getDocumentType().equalsIgnoreCase("3") && !travellersBean.isIqamaSelected())) {
                        if (travellersBean.getPassportIssuingCountry().isEmpty()) {
                            displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_passport_issuing_country_message));
                            return;
                        }
                    }
                    if (travellersBean.getDocumentType().equalsIgnoreCase("2")) {
                        if (travellersBean.getPassportExpiry().isEmpty() || Integer.parseInt(Utils.dateDifference(travellersBean.getPassportExpiry(), currentDate)) < 180) {
                            displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_passport_expiry_message));
                            return;
                        }
                    }
                    if (travellersBean.getDocumentType().equalsIgnoreCase("4") && travellersBean.isIqamaSelected()) {
                        if (airlineCodeArr.contains("SV") || airlineCodeArr.contains("XY")) {
                            if (travellersBean.getPassportExpiry().isEmpty()) {
                                displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_err_iqama_expiry_msg));
                                return;
                            }
                        }
                    }
                    if (travellersBean.getDocumentType().equalsIgnoreCase("2")) {
                        if (airlineCodeArr.contains("XY")) {
                            if (travellersBean.getPassportIssueDate().isEmpty()) {
                                displayErrorMessage(travellersBean.getTravellerHeader() + " - " + getString(R.string.label_error_passport_issue_message));
                                return;
                            }
                        }
                    }
                }
                Intent it = getIntent();
                setResult(RESULT_OK, it);
                finish();
            }
        });
        travellerAccountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TravellerListDialog travellerDialog = new TravellerListDialog(FlightBookingPassengerInfoActivity.this, Constants.PASSENGER_LIST, travellerArrayList, "", null, isAirportAvailable);
                travellerDialog.setCancelable(true);
                travellerAccountLayout.setClickable(false);
                travellerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (travellerDialog != null) {
                            int selectedItemPosition = travellerDialog.selectedItemPosition;
                            if (!travellerDialog.selectedListItem.equalsIgnoreCase("")) {
                                String passengerTitle = "";
                                String actualTitle = Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getTitle());
                                if (travellersBean.getPassengerType().equalsIgnoreCase("ADT")) {
                                    if (actualTitle.equalsIgnoreCase(getString(R.string.child_title)) || actualTitle.equalsIgnoreCase("")) {
                                        passengerTitle = getString(R.string.adult_title);
                                    } else {
                                        passengerTitle = actualTitle;
                                    }
                                } else if (travellersBean.getPassengerType().equalsIgnoreCase("CHD") || travellersBean.getPassengerType().equalsIgnoreCase("INF")) {
                                    if (actualTitle.equalsIgnoreCase(getString(R.string.adult_title)) || actualTitle.equalsIgnoreCase(getString(R.string.label_mrs)) || actualTitle.equalsIgnoreCase("")) {
                                        passengerTitle = getString(R.string.child_title);
                                    } else {
                                        passengerTitle = actualTitle;
                                    }
                                }
                                travellersBean.setTitle(passengerTitle);
                                travellersBean.setFirstName(Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getFirstName()));
                                travellersBean.setLastName(Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getLastName()));
                                travellersBean.setEmail(Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getEmail().toLowerCase()));
                                travellersBean.setPhoneNo(Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getPhoneNo()));
                                dateOfBirthText = Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getDateOfBirth());
                                if (dateOfBirthText.equalsIgnoreCase("")) {
                                    travellersBean.setDateOfBirth("");
                                } else {
                                    travellersBean.setDateOfBirth(Utils.formatTimeStampToPassportDate(dateOfBirthText));
                                }
                                travellersBean.setPassportNumber(Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getPassportNumber()));
                                DbHandler handler = new DbHandler(FlightBookingPassengerInfoActivity.this);
                                countryNameText = Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getPassportIssuingCountry());
                                if (countryNameText.equalsIgnoreCase("")) {
                                    travellersBean.setPassportIssuingCountry("");
                                } else {
                                    travellersBean.setPassportIssuingCountry(handler.getCountryNameText(countryNameText));
                                }
                                countryCodeText = Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getPassportIssuingCountry());
                                if (countryCodeText.equalsIgnoreCase("")) {
                                    travellersBean.setPassportIssuingCountryCode("");
                                } else {
                                    travellersBean.setPassportIssuingCountryCode(countryCodeText);
                                }
                                passportExpiryText = Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getPassportExpiry());
                                if (passportExpiryText.equalsIgnoreCase("")) {
                                    travellersBean.setPassportExpiry("");
                                } else {
                                    travellersBean.setPassportExpiry(Utils.formatTimeStampToPassportDate(passportExpiryText));
                                    //Utils.printMessage(TAG, "Gregorian date :: "+IsoChronology.INSTANCE.date(date));
                                }
                                passportIssueText = Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getPassportIssueDate());
                                if (passportIssueText.equalsIgnoreCase("")) {
                                    travellersBean.setPassportIssueDate("");
                                } else {
                                    travellersBean.setPassportIssueDate(Utils.formatTimeStampToPassportDate(passportIssueText));
                                }
                                nationalityNameText = Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getNationality());
                                if (nationalityNameText.equalsIgnoreCase("")) {
                                    travellersBean.setNationality("");
                                } else {
                                    travellersBean.setNationality(handler.getCountryNameText(nationalityNameText));
                                    if (!travellersBean.getDocumentType().equalsIgnoreCase("4")) {
                                        travellersBean.setPassportIssuingCountry(handler.getCountryNameText(nationalityNameText));
                                    }
                                }
                                nationalityCodeText = Utils.checkStringVal(travellerArrayList.get(selectedItemPosition).getNationality());
                                if (nationalityCodeText.equalsIgnoreCase("")) {
                                    travellersBean.setNationalityCode("");
                                } else {
                                    travellersBean.setNationalityCode(nationalityCodeText);
                                    if (!travellersBean.getDocumentType().equalsIgnoreCase("4")) {
                                        travellersBean.setPassportIssuingCountryCode(nationalityCodeText);
                                    }
                                }
                                travellersBean.setTravellerHeader(travellersBean.getTitle() + " " + travellersBean.getFirstName() + " " + travellersBean.getLastName());
                            }
                        }
                        travellerAccountLayout.setClickable(true);
                        passengerTitle.setText(travellersBean.getTitle());
                        passengerFirstName.setText(travellersBean.getFirstName());
                        passengerLastName.setText(travellersBean.getLastName());
                        passengerMobile.setText(travellersBean.getPhoneNo());
                        passengerEmail.setText(travellersBean.getEmail());
                        passengerBirth.setText(travellersBean.getDateOfBirth());
                        passengerPassportNumber.setText(travellersBean.getPassportNumber());
                        passengerNationality.setText(travellersBean.getNationality());
                        if (!travellersBean.getDateOfBirth().isEmpty() || !travellersBean.getDateOfBirth().equalsIgnoreCase("")) {
                            passengerBirthCloseIcon.setVisibility(View.VISIBLE);
                        }
                        if (!travellersBean.getNationality().isEmpty() || !travellersBean.getNationality().equalsIgnoreCase("")) {
                            nationalityCloseIcon.setVisibility(View.VISIBLE);
                        }
                        passPortCountry.setText(travellersBean.getPassportIssuingCountry());
                        if (!travellersBean.getPassportIssuingCountry().isEmpty() || !travellersBean.getPassportIssuingCountry().equalsIgnoreCase("")) {
                            passportCountryCloseIcon.setVisibility(View.VISIBLE);
                        }
                        passportExpiry.setText(travellersBean.getPassportExpiry());
                        if (!travellersBean.getPassportExpiry().isEmpty() || !travellersBean.getPassportExpiry().equalsIgnoreCase("")) {
                            passportExpiryCloseIcon.setVisibility(View.VISIBLE);
                        }
                        passportIssuedText.setText(travellersBean.getPassportIssueDate());
                        if (!travellersBean.getPassportIssueDate().isEmpty() || !travellersBean.getPassportIssueDate().equalsIgnoreCase("")) {
                            passportIssueCloseIcon.setVisibility(View.VISIBLE);
                        }
                    }
                });
                travellerDialog.show();
            }
        });
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private void setDateofBirthForPassenger() {
        if (isArabicLang) {
            String languageToLoad = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }
        Calendar c = Calendar.getInstance();
        int mYear = 0;
        int mMonth = 0;
        int mDay = 0;
        if (travellersBean.getDateOfBirth().isEmpty()) {
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        } else {
            String selectedPassportExp = travellersBean.getDateOfBirth();
            String[] expDate = selectedPassportExp.split("-");
            mYear = Integer.parseInt(expDate[2]);
            mMonth = Integer.parseInt(expDate[1]) - 1;
            mDay = Integer.parseInt(expDate[0]);
        }
        final DatePickerDialog dialog = new DatePickerDialog(FlightBookingPassengerInfoActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (isArabicLang) {
                    String languageToLoad = "ar";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                }
                if (travellersBean.getPassengerType().equalsIgnoreCase("ADT")) {
                    adultDateOfBirth = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(year);
                    travellersBean.setDateOfBirth(adultDateOfBirth);
                }
                if (travellersBean.getPassengerType().equalsIgnoreCase("CHD")) {
                    childDateOfBirth = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(year);
                    travellersBean.setDateOfBirth(childDateOfBirth);
                }
                if (travellersBean.getPassengerType().equalsIgnoreCase("INF")) {
                    infantDateOfBirth = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(year);
                    travellersBean.setDateOfBirth(infantDateOfBirth);
                }
                passengerBirth.setText(travellersBean.getDateOfBirth());
                passengerBirthCloseIcon.setVisibility(View.VISIBLE);
            }
        }, mYear, mMonth, mDay);
        if (travellersBean.getPassengerType().equalsIgnoreCase("CHD")) {
            c.add(Calendar.YEAR, -12);
            dialog.getDatePicker().setMinDate(c.getTimeInMillis());
            Calendar maxDate = Calendar.getInstance();
            maxDate.add(Calendar.YEAR, -2);
            dialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
        } else if (travellersBean.getPassengerType().equalsIgnoreCase("INF")) {
            dialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
            Calendar minDate = Calendar.getInstance();
            minDate.add(Calendar.YEAR, -2);
            try {
                dialog.getDatePicker().setMinDate(minDate.getTimeInMillis());
            } catch (Exception e) {
            }
        } else {
            c.add(Calendar.YEAR, -12);
            dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        }
        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, getString(R.string.label_selected_flight_message), dialog);
        dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, getString(R.string.label_cancel_button), dialog);
        dialog.show();
    }

    private void setPassportIssuedDate() {
        if (isArabicLang) {
            String languageToLoad = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }
        int mYear = 0;
        int mMonth = 0;
        int mDay = 0;
        if (travellersBean.getPassportIssueDate().isEmpty() || travellersBean.getPassportIssueDate().equalsIgnoreCase("")) {
            String passengerDOB = travellersBean.getDateOfBirth();
            String[] dateOfBirth = passengerDOB.split("-");
            mYear = Integer.parseInt(dateOfBirth[2]);
            mMonth = Integer.parseInt(dateOfBirth[1]) - 1;
            mDay = Integer.parseInt(dateOfBirth[0]);
        } else {
            passportIssuedDate = travellersBean.getPassportIssueDate();
            String[] expDate = passportIssuedDate.split("-");
            mYear = Integer.parseInt(expDate[2]);
            mMonth = Integer.parseInt(expDate[1]) - 1;
            mDay = Integer.parseInt(expDate[0]);
        }
        final DatePickerDialog dialog = new DatePickerDialog(FlightBookingPassengerInfoActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                calendar.set(year, month, day);
                passportIssuedDate = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(calendar.getTime());
                if (isArabicLang) {
                    String languageToLoad = "ar";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                }
                travellersBean.setPassportIssueDate(passportIssuedDate);
                passportIssuedText.setText(passportIssuedDate);
                passportIssueCloseIcon.setVisibility(View.VISIBLE);
            }
        }, mYear, mMonth, mDay);
        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, this.getString(R.string.label_selected_flight_message), dialog);
        dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, this.getString(R.string.label_cancel_button), dialog);
        String stringDate = travellersBean.getDateOfBirth();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date minDate = null;
        try {
            minDate = format.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (minDate != null) {
            dialog.getDatePicker().setMinDate(minDate.getTime());
        }
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
        dialog.show();
    }

    @Override
    public void onDateSet(int year, int month, int day) {
        calendar.set(year, month, day);
        passportSelectedDate = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(calendar.getTime());
        travellersBean.setPassportExpiry(passportSelectedDate);
        passportExpiry.setText(travellersBean.getPassportExpiry());
        if (!travellersBean.getPassportExpiry().isEmpty()) {
            passportExpiryCloseIcon.setVisibility(View.VISIBLE);
        }
    }

    private void setPassportExpiryDate(boolean isHijriChecked, boolean isIqamaSelected) {
        if (!isHijriChecked) {
            if (isArabicLang) {
                String languageToLoad = "en";
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            }
            Calendar c = Calendar.getInstance();
            int mYear = 0;
            int mMonth = 0;
            int mDay = 0;
            if (travellersBean.getPassportExpiry().isEmpty() || travellersBean.getPassportExpiry().equalsIgnoreCase("")) {
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
            } else {
                passportSelectedDate = travellersBean.getPassportExpiry();
                String[] expDate = passportSelectedDate.split("-");
                mYear = Integer.parseInt(expDate[2]);
                mMonth = Integer.parseInt(expDate[1]) - 1;
                mDay = Integer.parseInt(expDate[0]);
            }
            final DatePickerDialog dialog = new DatePickerDialog(FlightBookingPassengerInfoActivity.this, new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    calendar.set(year, month, day);
                    passportSelectedDate = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(calendar.getTime());
                    if (isArabicLang) {
                        String languageToLoad = "ar";
                        Locale locale = new Locale(languageToLoad);
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                    }
                    travellersBean.setPassportExpiry(passportSelectedDate);
                    passportExpiry.setText(travellersBean.getPassportExpiry());
                    if (!travellersBean.getPassportExpiry().isEmpty()) {
                        passportExpiryCloseIcon.setVisibility(View.VISIBLE);
                    }
                }
            }, mYear, mMonth, mDay);
            dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, this.getString(R.string.label_selected_flight_message), dialog);
            dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, this.getString(R.string.label_cancel_button), dialog);
            dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            if (isIqamaSelected) {
                Calendar maxDate = Calendar.getInstance();
                maxDate.add(Calendar.YEAR, 5);
                dialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
            }
            dialog.setOnCancelListener(new OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    if (isArabicLang) {
                        String languageToLoad = "ar";
                        Locale locale = new Locale(languageToLoad);
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                    }
                }
            });
            dialog.show();
        } else {
            Calendar c = Calendar.getInstance();
            String hijriDate = Utils.hijriCalendarDate(true);
            String[] convertedDate = hijriDate.split("-");
            int mYear = 0;
            int mMonth = 0;
            int mDay = 0;
            if (travellersBean.getPassportExpiry().isEmpty() || travellersBean.getPassportExpiry().equalsIgnoreCase("")) {
                mYear = Integer.parseInt(convertedDate[2]);
                mMonth = Integer.parseInt(convertedDate[1]);
                mDay = Integer.parseInt(convertedDate[0]);
            } else {
                passportSelectedDate = travellersBean.getPassportExpiry();
                String[] expDate = passportSelectedDate.split("-");
                mYear = Integer.parseInt(expDate[2]);
                mMonth = Integer.parseInt(expDate[1]) - 1;
                mDay = Integer.parseInt(expDate[0]);
            }
            int minHijriYear = Integer.parseInt(convertedDate[2]);
            int maxHijriYear = minHijriYear + 5;
            int minGregorianYear = c.get(Calendar.YEAR);
            int maxGregorianYear = minGregorianYear + 5;
            new HijriCalendarDialog.Builder(this)
                    .setOnDateSetListener(FlightBookingPassengerInfoActivity.this)
                    .setMinMaxHijriYear(minHijriYear, maxHijriYear)
                    .setMinMaxGregorianYear(minGregorianYear, maxGregorianYear)
                    .setMode(HijriCalendarDialog.Mode.Hijri)
                    .setUILanguage(hijriLanguage)
                    .setDefaultHijriDate(mDay, mMonth, mYear)
                    .setEnableScrolling(false)
                    .show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SEARCH_NATIONALITY_RESULT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String nationalityCountryName = data.getStringExtra(Constants.SELECTED_COUNTRY_NAME);
                    if (!nationalityCountryName.equalsIgnoreCase("")) {
                        travellersBean.setNationality(nationalityCountryName);
                        travellersBean.setNationalityCode(data.getStringExtra(Constants.SELECTED_COUNTRY_ISO_CODE));
                        if (!travellersBean.getDocumentType().equalsIgnoreCase("4")) {
                            travellersBean.setPassportIssuingCountry(nationalityCountryName);
                            travellersBean.setPassportIssuingCountryCode(data.getStringExtra(Constants.SELECTED_COUNTRY_ISO_CODE));
                        }
                    }
                    passengerNationality.setText(travellersBean.getNationality());
                    passPortCountry.setText(travellersBean.getPassportIssuingCountry());
                    if (!travellersBean.getNationality().isEmpty()) {
                        nationalityCloseIcon.setVisibility(View.VISIBLE);
                    }
                    if (!travellersBean.getPassportIssuingCountry().isEmpty()) {
                        passportCountryCloseIcon.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
        if (requestCode == SEARCH_PASSPORT_RESULT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String passportCountryName = data.getStringExtra(Constants.SELECTED_COUNTRY_NAME);
                    if (!passportCountryName.equalsIgnoreCase("")) {
                        travellersBean.setPassportIssuingCountry(passportCountryName);
                        travellersBean.setPassportIssuingCountryCode(data.getStringExtra(Constants.SELECTED_COUNTRY_ISO_CODE));
                    }
                    passPortCountry.setText(travellersBean.getPassportIssuingCountry());
                    if (!travellersBean.getPassportIssuingCountry().isEmpty()) {
                        passportCountryCloseIcon.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
        if (requestCode == SEARCH_MOBILE_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String countryCode = data.getStringExtra(Constants.SELECTED_COUNTRY_CODE);
                    if (!countryCode.equalsIgnoreCase("")) {
                        int flagCode = Integer.parseInt(data.getStringExtra(Constants.SELECTED_COUNTRY_FLAG));
                        travellersBean.setPhoneCode(countryCode);
                        travellersBean.setmResId(flagCode);
                        countryImage.setImageResource(flagCode);
                        countryCodeNumberText.setText(countryCode);
                    }
                }
            }
        }
        if (requestCode == SEARCH_IQAMA_NATIONALITY) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String iqamaCountryName = data.getStringExtra(Constants.SELECTED_COUNTRY_NAME);
                    if (!iqamaCountryName.equalsIgnoreCase("")) {
                        travellersBean.setIqamaNationality(iqamaCountryName);
                        travellersBean.setIqamaNationalityCode(data.getStringExtra(Constants.SELECTED_COUNTRY_ISO_CODE));
                    }
                }
            }
        }
        if (loginDetailsSuccess == 1) {
            signInDialog.passFacebookResult(requestCode, resultCode, data);
        }
    }
}
