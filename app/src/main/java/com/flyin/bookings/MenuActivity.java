package com.flyin.bookings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import java.util.Locale;

public class MenuActivity extends AppCompatActivity {
    private TextView menuCurrencyText, menuCurrencyCodeText;
    private ImageView arabicCheckView, englishCheckView;
    private String aboutUsData = "";
    private String privacyPolicyData = "";
    private TextView countryValueTextView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_DROIDKUFI_REGULAR;
        String fontRegular = Constants.FONT_ROBOTO_REGULAR;
        TextView menuHeader = (TextView) findViewById(R.id.menu_header);
        TextView menuProfile = (TextView) findViewById(R.id.menu_profile);
        TextView menuTrips = (TextView) findViewById(R.id.menu_trips);
        TextView menuSocial = (TextView) findViewById(R.id.menu_social);
        TextView menuChangePassword = (TextView) findViewById(R.id.menu_change_password);
        TextView menuArabicLanguage = (TextView) findViewById(R.id.menu_arabic_language);
        TextView menuEnglishLanguage = (TextView) findViewById(R.id.menu_english_language);
        TextView menuCurrency = (TextView) findViewById(R.id.menu_currency);
        menuCurrencyText = (TextView) findViewById(R.id.menu_currency_text);
        menuCurrencyCodeText = (TextView) findViewById(R.id.menu_currency_code_text);
        TextView menuAboutFlyin = (TextView) findViewById(R.id.menu_about_flyin);
        TextView menuLegal = (TextView) findViewById(R.id.menu_legal);
        TextView menuPrivacyPolicy = (TextView) findViewById(R.id.menu_privacy_policy);
        TextView menuContact = (TextView) findViewById(R.id.menu_contact);
        TextView menuSavedCards = (TextView) findViewById(R.id.saved_card_text);
        countryValueTextView = (TextView) findViewById(R.id.menu_country_text);
        View rewardView = (View) findViewById(R.id.reward_view);
        arabicCheckView = (ImageView) findViewById(R.id.user_arabic);
        englishCheckView = (ImageView) findViewById(R.id.user_english);
        RelativeLayout profileLayout = (RelativeLayout) findViewById(R.id.user_profile_layout);
        RelativeLayout myTripsLayout = (RelativeLayout) findViewById(R.id.my_trips_layout);
        RelativeLayout socialLayout = (RelativeLayout) findViewById(R.id.user_social_layout);
        RelativeLayout changePasswordLayout = (RelativeLayout) findViewById(R.id.user_change_password_layout);
        RelativeLayout arabicLayout = (RelativeLayout) findViewById(R.id.user_language_arabic);
        RelativeLayout englishLayout = (RelativeLayout) findViewById(R.id.user_language_english);
        RelativeLayout currencyLayout = (RelativeLayout) findViewById(R.id.user_currency_layout);
        RelativeLayout savedCardsLayout = (RelativeLayout) findViewById(R.id.saved_cards_layout);
        RelativeLayout countryLayout = (RelativeLayout) findViewById(R.id.user_country_layout);
        RelativeLayout aboutFlyinLayout = (RelativeLayout) findViewById(R.id.about_us_layout);
        RelativeLayout termsLayout = (RelativeLayout) findViewById(R.id.terms_layout);
        RelativeLayout privacyPolicyLayout = (RelativeLayout) findViewById(R.id.privacy_policy_layout);
        RelativeLayout contactLayout = (RelativeLayout) findViewById(R.id.contact_layout);
        LinearLayout userSingInView = (LinearLayout) findViewById(R.id.user_signIn_view);
        LinearLayout userPasswordView = (LinearLayout) findViewById(R.id.change_password_layout);
        TextView androidVersionText = (TextView) findViewById(R.id.android_version_text);
        TextView androidBuildText = (TextView) findViewById(R.id.android_build_text);
        TextView rewardsTextView = (TextView) findViewById(R.id.menu_rewards);
        RelativeLayout rewardsLayout = (RelativeLayout) findViewById(R.id.rewards_layout);
        aboutUsData = "file:///android_asset/aboutus-english.html";
        privacyPolicyData = "file:///android_asset/privacypolicy-english.html";
        if (Utils.isArabicLangSelected(MenuActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            fontRegular = Constants.FONT_ROBOTO_REGULAR;
            findViewById(R.id.my_profile_arrow).setRotation(180);
            findViewById(R.id.my_trips_arrow).setRotation(180);
            findViewById(R.id.rewards_arrow).setRotation(180);
            findViewById(R.id.social_arrow).setRotation(180);
            findViewById(R.id.change_password_arrow).setRotation(180);
            findViewById(R.id.user_currency_list).setRotation(180);
            findViewById(R.id.user_country_list).setRotation(180);
            findViewById(R.id.about_arrow).setRotation(180);
            findViewById(R.id.legal_arrow).setRotation(180);
            findViewById(R.id.privacy_policy_arrow).setRotation(180);
            findViewById(R.id.contact_arrow).setRotation(180);
            findViewById(R.id.saved_card_arrow).setRotation(180);
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.JELLY_BEAN_MR1) {
                menuCurrencyCodeText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
            }
            aboutUsData = "file:///android_asset/aboutus-arbic.html";
            privacyPolicyData = "file:///android_asset/privacypolicy-arabic.html";
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontTitle);
        Typeface arabicTextFace = Typeface.createFromAsset(getAssets(), fontText);
        Typeface defaultFace = Typeface.createFromAsset(getAssets(), fontRegular);
        menuHeader.setTypeface(tf);
        menuProfile.setTypeface(textFace);
        menuTrips.setTypeface(textFace);
        rewardsTextView.setTypeface(textFace);
        menuSocial.setTypeface(textFace);
        menuChangePassword.setTypeface(textFace);
        menuArabicLanguage.setTypeface(arabicTextFace);
        menuEnglishLanguage.setTypeface(textFace);
        menuCurrency.setTypeface(textFace);
        menuCurrencyText.setTypeface(textFace);
        menuSavedCards.setTypeface(textFace);
        menuCurrencyCodeText.setTypeface(defaultFace);
        menuAboutFlyin.setTypeface(textFace);
        menuLegal.setTypeface(textFace);
        menuPrivacyPolicy.setTypeface(textFace);
        menuContact.setTypeface(textFace);
        androidVersionText.setTypeface(textFace);
        androidBuildText.setTypeface(textFace);
        final SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String memberAccessToken = pref.getString(Constants.MEMBER_ACCESS_TOKEN, "");
        if (!memberAccessToken.equalsIgnoreCase("")) {
            userSingInView.setVisibility(View.VISIBLE);
            userPasswordView.setVisibility(View.VISIBLE);
            menuHeader.setText(getString(R.string.label_welcome_title) + ", " + pref.getString(Constants.MEMBER_EMAIL, ""));
        } else {
            userSingInView.setVisibility(View.GONE);
            userPasswordView.setVisibility(View.GONE);
            menuHeader.setText(getResources().getText(R.string.label_welcome_title) + " " + getResources().getText(R.string.label_guest_title));
        }
        String selectedLanguage = pref.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
        if (selectedLanguage.equalsIgnoreCase(Constants.LANGUAGE_ENGLISH_CODE)) {
            arabicCheckView.setVisibility(View.INVISIBLE);
            englishCheckView.setVisibility(View.VISIBLE);
        } else {
            arabicCheckView.setVisibility(View.VISIBLE);
            englishCheckView.setVisibility(View.INVISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
            String languageToLoad = "ar";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
        profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, UserProfileActivity.class);
                startActivity(intent);
            }
        });
        myTripsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, MyTripsActivity.class);
                startActivity(intent);
            }
        });
        rewardsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pref.getBoolean(Constants.IS_REWARDS_ENABLED, false)) {
                    Intent intent = new Intent(MenuActivity.this, RewardsActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MenuActivity.this, EnrollRewardsActivity.class);
                    startActivity(intent);
                }
            }
        });
        countryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, CountryActivity.class);
                startActivity(intent);
            }
        });
        savedCardsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, SavedCardsActivity.class);
                startActivity(intent);
            }
        });
        socialLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, SocialActivity.class);
                startActivity(intent);
            }
        });
        changePasswordLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
        if (selectedLanguage.equalsIgnoreCase("ar_SA")) {
            englishLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arabicCheckView.setVisibility(View.INVISIBLE);
                    englishCheckView.setVisibility(View.VISIBLE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString(Constants.USER_SELECTED_LANGUAGE, Constants.LANGUAGE_ENGLISH_CODE);
                    editor.apply();
                    Intent intent = new Intent(MenuActivity.this, SplashScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
        }
        if (selectedLanguage.equalsIgnoreCase("en_GB")) {
            arabicLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arabicCheckView.setVisibility(View.VISIBLE);
                    englishCheckView.setVisibility(View.INVISIBLE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString(Constants.USER_SELECTED_LANGUAGE, Constants.LANGUAGE_ARABIC_CODE);
                    editor.apply();
                    Intent intent = new Intent(MenuActivity.this, SplashScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
        }
        currencyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, CurrencyActivity.class);
                startActivity(intent);
            }
        });
        aboutFlyinLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MenuActivity.this, AboutusActivity.class);
                in.putExtra(Constants.HTML_WEB_VIEW, aboutUsData);
                in.putExtra(Constants.ACTIONBAR_HEADER, getString(R.string.label_user_aboutFlyin));
                startActivity(in);
            }
        });
        termsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, TermsConditionsActivity.class);
                startActivity(intent);
            }
        });
        privacyPolicyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MenuActivity.this, AboutusActivity.class);
                in.putExtra(Constants.HTML_WEB_VIEW, privacyPolicyData);
                in.putExtra(Constants.ACTIONBAR_HEADER, getString(R.string.label_user_privacy_policy));
                startActivity(in);
            }
        });
        contactLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, ContactusActivity.class);
                startActivity(intent);
            }
        });

        PackageManager manager = this.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        androidVersionText.setText(getString(R.string.label_android_version) + " " + info.versionName);
        androidBuildText.setText(getString(R.string.label_android_build) + " " + info.versionCode);
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_user_menu);
        mTitleText.setTypeface(tf);
        backText.setTypeface(textFace);
        doneButton.setVisibility(View.GONE);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (Utils.isArabicLangSelected(MenuActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        mActionBar.setCustomView(mCustomView, layout);
        mActionBar.setDisplayShowCustomEnabled(true);
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        if (pref.getString(Constants.ISCOUNTRY_GLOBAL, "GL").equalsIgnoreCase("GL")) {
            countryValueTextView.setText(getResources().getString(R.string.label_global));
        } else {
            countryValueTextView.setText(getResources().getString(R.string.label_egypt));
        }
        String savedCurrency = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        switch (savedCurrency) {
            case "GBP":
                menuCurrencyText.setText(getString(R.string.label_currency_gbp));
                menuCurrencyCodeText.setText(getString(R.string.label_currency_gbp_code));
                break;
            case "QAR":
                menuCurrencyText.setText(getString(R.string.label_currency_qmr));
                menuCurrencyCodeText.setText(getString(R.string.label_currency_qmr_code));
                break;
            case "OMR":
                menuCurrencyText.setText(getString(R.string.label_currency_omr));
                menuCurrencyCodeText.setText(getString(R.string.label_currency_omr_code));
                break;
            case "KWD":
                menuCurrencyText.setText(getString(R.string.label_currnecy_kwd));
                menuCurrencyCodeText.setText(getString(R.string.label_currnecy_kwd_code));
                break;
            case "EGP":
                menuCurrencyText.setText(getString(R.string.label_currency_egp));
                menuCurrencyCodeText.setText(getString(R.string.label_currency_egp_code));
                break;
            case "BHD":
                menuCurrencyText.setText(getString(R.string.label_currency_bhd));
                menuCurrencyCodeText.setText(getString(R.string.label_currency_bhd_code));
                break;
            case "AED":
                menuCurrencyText.setText(getString(R.string.label_currency_aed));
                menuCurrencyCodeText.setText(getString(R.string.label_currency_aed_code));
                break;
            case "EUR":
                menuCurrencyText.setText(getString(R.string.label_currency_eur));
                menuCurrencyCodeText.setText(getString(R.string.label_currency_eur_code));
                break;
            case "USD":
                menuCurrencyText.setText(getString(R.string.label_currency_usd));
                menuCurrencyCodeText.setText(getString(R.string.label_currency_usd_code));
                break;
            case "SAR":
                menuCurrencyText.setText(getString(R.string.label_currency_sar));
                menuCurrencyCodeText.setText(getString(R.string.label_currency_sar_code));
                break;
            default:
                break;
        }
    }
}
