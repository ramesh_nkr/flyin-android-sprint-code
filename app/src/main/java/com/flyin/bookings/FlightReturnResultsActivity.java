package com.flyin.bookings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyin.bookings.adapter.FlightRoundTripAdapter;
import com.flyin.bookings.adapter.HorizontalScrollRecyclerViewAdapter;
import com.flyin.bookings.listeners.OnCustomItemSelectListener;
import com.flyin.bookings.model.AirItineraryObjectBean;
import com.flyin.bookings.model.AirItineraryPricingInfoBean;
import com.flyin.bookings.model.AmenitiesBean;
import com.flyin.bookings.model.FSDataBean;
import com.flyin.bookings.model.FilterAirlineSelectionItem;
import com.flyin.bookings.model.FilterAirportSelectionItem;
import com.flyin.bookings.model.FilterRefundSelectionItem;
import com.flyin.bookings.model.FilterStopSelectionItem;
import com.flyin.bookings.model.FilterTimeSelectionItem;
import com.flyin.bookings.model.FlightAirportName;
import com.flyin.bookings.model.FlightAmenitiesBean;
import com.flyin.bookings.model.FlightBean;
import com.flyin.bookings.model.ItinTotalFareObjectBean;
import com.flyin.bookings.model.OriginDestinationOptionBean;
import com.flyin.bookings.model.PFBDObjectBean;
import com.flyin.bookings.model.PricedItineraryObject;
import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.model.TaxObjectBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.AmenitiesConstants;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class FlightReturnResultsActivity extends AppCompatActivity implements AsyncTaskListener, OnCustomItemSelectListener {
    private Typeface regularFace = null;
    private String tripType = "";
    private String source = "";
    private String destination = "";
    private String secondSource = "";
    private boolean isInternetPresent = false;
    private boolean isArabicLang = false;
    private String travellerCount = "";
    private ArrayList<PricedItineraryObject> mainPricedItineraryArray = null;
    private ArrayList<PricedItineraryObject> tempPricedItineraryArray = null;
    private ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList = null;
    private ArrayList<OriginDestinationOptionBean> tempFlightItineraryArray = null;
    private ArrayList<FilterStopSelectionItem> stopsArray = null;
    private ArrayList<FilterAirlineSelectionItem> airlineArray = null;
    private ArrayList<FilterAirportSelectionItem> airportListArray = null;
    private ArrayList<FilterAirportSelectionItem> tempArray1 = null;
    private ArrayList<FilterAirportSelectionItem> tempArray2 = null;
    private ArrayList<FilterAirportSelectionItem> tempArray3 = null;
    private ArrayList<FilterTimeSelectionItem> timeArray = null;
    private ArrayList<FilterTimeSelectionItem> dapTimeListArray = null;
    private ArrayList<FilterTimeSelectionItem> aapTimeListArray = null;
    private ArrayList<FilterRefundSelectionItem> refundStatusArray = null;
    private ArrayList<PricedItineraryObject> tempAirlinePIArrayList = null;
    private ArrayList<FlightBean> flightsArrayList = new ArrayList();
    private ArrayList<PricedItineraryObject> mainFCPricedItineraryArray = null;
    private TextView errorText, errorDescriptionText = null;
    private TextView totalFlightCountTextView = null;
    private TextView flightCountEarlierTextView = null;
    private TextView flightCountFastestTextView = null;
    private LayoutInflater inflater = null;
    private ImageView errorImage = null;
    private Button searchButton = null;
    private View loadingView = null;
    private RelativeLayout headerViewLayout = null;
    private static final int GET_FARE_RULE_RESULTS = 2;
    private static final int FLIGHT_PRICE_FILTER = 3;
    private HashMap<String, FlightAirportName> airportNamesMap = new HashMap<>();
    private HashMap<String, String> airLineNamesMap = new HashMap<>();
    private boolean isTextUpdated = false;
    private static final String TAG = "FlightReturnResultsActivity";
    private boolean fastestFlights = false;
    private boolean earliestFlights = false;
    private boolean totalFlights = false;
    private String secondDestination = "";
    private String thirdSource = "";
    private String thirdDestination = "";
    private String journeyDetails = "";
    private String forwardFlightDate = "";
    private String returnFlightDate = "";
    private String sourceName = "";
    private String returnName = "";
    private String secondsourceName = "";
    private String secondreturnName = "";
    private String thirdsourceName = "";
    private String thirdreturnName = "";
    private boolean isNonStopChecked = false;
    private boolean multiCityNonStopChecked = false;
    private String minValue, maxValue;
    private String selectedClassType = "";
    private static final int SEARCH_FLIGHT_RESULT = 6;
    private static final int GET_ACCESS_TOKEN = 8;
    private ArrayList<FlightAmenitiesBean> mainMarchArrayList = null;
    private HorizontalScrollRecyclerViewAdapter mAdapter;
    private View noResultView = null;
    private RelativeLayout flightListLayout;
    private int earliestCount = 0;
    private int fastestCount = 0;
    private int totalCount = 0;
    private LinearLayout totalCountLayout = null;
    private LinearLayout earliestCountLayout = null;
    private LinearLayout fastestCountLayout = null;
    private String aiJSONObject = "";
    private String aipiJSONObject = "";
    private TextView errorMessageText = null;
    private RelativeLayout errorView = null;
    private boolean isFilterApplied = false;
    private PricedItineraryObject piObj = new PricedItineraryObject();
    private String selectedRphNumber = "";
    private String selectedFlightPrice = "";
    private FlightRoundTripAdapter roundTripAdapterList;
    private String randomNumber = "";
    private String selectedMal = "";
    private String returnFlightPrice = "";
    private String forwardFlightPrice = "";
    private String totalPackagePrice = "";
    private AirItineraryPricingInfoBean defaultAipiOdo = null;
    private OriginDestinationOptionBean firstAiOdo = null;
    private AirItineraryPricingInfoBean firstAiPiOdo = null;
    private ArrayList<String> rphNumberList = null;
    private ArrayList<String> tripStatusArr = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_results_page);
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.hide();
        }
//        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
//        long tokenTime = preferences.getLong(Constants.TOKEN_GENERATED_TIME, -1);
//        String expireIn = preferences.getString(Constants.EXPIRE_IN, "0");
//        if (tokenTime == -1) {
//            getTokenFromServer();
//        } else {
//            long diff = (Calendar.getInstance().getTimeInMillis() / 1000) - tokenTime;
//            if (diff > Long.parseLong(expireIn)) {
//                getTokenFromServer();
//            }
//        }
        isInternetPresent = Utils.isConnectingToInternet(FlightReturnResultsActivity.this);
        mainPricedItineraryArray = new ArrayList<>();
        tempPricedItineraryArray = new ArrayList<>();
        tempFlightItineraryArray = new ArrayList<>();
        mainMarchArrayList = new ArrayList<>();
        pricedItineraryObjectArrayList = new ArrayList<>();
        tempAirlinePIArrayList = new ArrayList<>();
        airlineArray = new ArrayList<>();
        mainFCPricedItineraryArray = new ArrayList<>();
        rphNumberList = new ArrayList<>();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontMedium = Constants.FONT_ROBOTO_BOLD;
        String fontNormal = Constants.FONT_ROBOTO_MEDIUM;
        isArabicLang = false;
        if (Utils.isArabicLangSelected(FlightReturnResultsActivity.this)) {
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontNormal = Constants.FONT_DROIDKUFI_REGULAR;
            fontMedium = Constants.FONT_DROIDKUFI_BOLD;
            isArabicLang = true;
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Constants.TRIP_TYPE)) {
                tripType = bundle.getString(Constants.TRIP_TYPE, "");
            }
            selectedClassType = bundle.getString(Constants.SELECTED_CLASS_TYPE, "");
            source = bundle.getString(Constants.FORWARD_SOURCE, "");
            destination = bundle.getString(Constants.FORWARD_DESTINATION, "");
            travellerCount = bundle.getString(Constants.TOTAL_PASSENGERS_COUNT, "1");
            sourceName = bundle.getString(Constants.FORWARD_SOURCE_NAME, "");
            returnName = bundle.getString(Constants.RETURN_SOURCE_NAME, "");
            journeyDetails = bundle.getString(Constants.FORWARD_DETAILS, "");
            thirdSource = bundle.getString(Constants.THIRD_FORWARD_SOURCE, "");
            thirdDestination = bundle.getString(Constants.THIRD_FORWARD_DESTINATION, "");
            forwardFlightDate = bundle.getString(Constants.FORWARD_FLIGHT_DATE, "");
            returnFlightDate = bundle.getString(Constants.RETURN_FLIGHT_DATE, "");
            secondsourceName = bundle.getString(Constants.SECOND_FORWARD_SOURCE_NAME, "");
            secondreturnName = bundle.getString(Constants.SECOND_FORWARD_DESTINATION_NAME, "");
            thirdsourceName = bundle.getString(Constants.THIRD_FORWARD_SOURCE_NAME, "");
            thirdreturnName = bundle.getString(Constants.THIRD_FORWARD_DESTINATION_NAME, "");
            isNonStopChecked = bundle.getBoolean(Constants.ONE_WAY_NONSTOP_CHECKED, false);
            multiCityNonStopChecked = bundle.getBoolean(Constants.MULTI_TRIP_NONSTOP_CHECKED, false);
            airLineNamesMap = (HashMap<String, String>) bundle.get(Constants.AIRLINE_NAME_HASHMAP);
            airportNamesMap = (HashMap<String, FlightAirportName>) bundle.get(Constants.AIRPORT_NAME_HASHMAP);
            selectedRphNumber = bundle.getString(Constants.SELECTED_RPH_NUMBER, "");
            selectedFlightPrice = bundle.getString(Constants.SELECTED_FLIGHT_PRICE, "");
            randomNumber = bundle.getString(Constants.BOOKING_RANDOM_NUMBER, "");
            Singleton.getInstance().refundStatusArrayList.clear();
            Singleton.getInstance().resultStopsArrayList.clear();
            Singleton.getInstance().returnResultAirlinesArrayList.clear();
            Singleton.getInstance().resultAirportsArrayList.clear();
            Singleton.getInstance().firstAirportsArrayList.clear();
            Singleton.getInstance().secondAirportsArrayList.clear();
            Singleton.getInstance().thirdAirportsArrayList.clear();
            Singleton.getInstance().resultTimeArrayList.clear();
            Singleton.getInstance().departureTimeArrayList.clear();
            Singleton.getInstance().arrivalTimeArrayList.clear();
        }
        mainPricedItineraryArray.clear();
        mainFCPricedItineraryArray.clear();
        mainPricedItineraryArray.addAll(Singleton.getInstance().selectedPriceItinerarayObject);
        mainFCPricedItineraryArray.addAll(Singleton.getInstance().fCResultFlightPIArrayList);
        Utils.printMessage(TAG, "Fc array size :: " + mainFCPricedItineraryArray.size() + " Main Arr size :: " + mainPricedItineraryArray.size());
        ArrayList<PricedItineraryObject> returnFlightArr = new ArrayList<>();
        ArrayList<PricedItineraryObject> returnFCFlightArr = new ArrayList<>();
        try {
            returnFlightArr.clear();
            for (int i = 0; i < mainPricedItineraryArray.size(); i++) {
                PricedItineraryObject pricedItineraryObject = new PricedItineraryObject();
                AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                if (!mainPricedItineraryArray.get(i).getAiBean().getDirId().isEmpty() || mainPricedItineraryArray.get(i).getAiBean().getDirId() != null) {
                    aiBean.setDirId(mainPricedItineraryArray.get(i).getAiBean().getDirId());
                }
                aiBean.setOriginDestinationOptionBeanArrayList(mainPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList());
                pricedItineraryObject.setAiBean(aiBean);
                pricedItineraryObject.setAipiBean(mainPricedItineraryArray.get(i).getAipiBean());
                returnFlightArr.add(pricedItineraryObject);
            }
            returnFCFlightArr.clear();
            for (int i = 0; i < mainFCPricedItineraryArray.size(); i++) {
                PricedItineraryObject pricedItineraryObject = new PricedItineraryObject();
                AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                if (!mainFCPricedItineraryArray.get(i).getAiBean().getDirId().isEmpty() || mainFCPricedItineraryArray.get(i).getAiBean().getDirId() != null) {
                    aiBean.setDirId(mainFCPricedItineraryArray.get(i).getAiBean().getDirId());
                }
                aiBean.setOriginDestinationOptionBeanArrayList(mainFCPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList());
                pricedItineraryObject.setAiBean(aiBean);
                pricedItineraryObject.setAipiBean(mainFCPricedItineraryArray.get(i).getAipiBean());
                returnFCFlightArr.add(pricedItineraryObject);
            }
            Utils.printMessage(TAG, "Main Arr size :: " + returnFlightArr.size() + " FC Arr Size :: " + returnFCFlightArr.size());
        } catch (Exception e) {
        }
        mainMarchArrayList.addAll(Singleton.getInstance().selectedMerchandiseObject);
        ArrayList<OriginDestinationOptionBean> tempForwardPricedItineraryArray = new ArrayList<>();
        try {
            tempPricedItineraryArray.clear();
            try {
                for (int i = 0; i < returnFlightArr.size(); i++) {
                    tempFlightItineraryArray.clear();
                    tempForwardPricedItineraryArray.clear();
                    ArrayList<PricedItineraryObject> tempPiArr = new ArrayList<>();
                    tempPiArr.add(returnFlightArr.get(i));
                    String flightPrice = tempPiArr.get(0).getAipiBean().getItfObject().gettFare();
                    for (int j = 0; j < returnFlightArr.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                        OriginDestinationOptionBean optionBean = returnFlightArr.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                        if (optionBean.getRefNum().equalsIgnoreCase("1")) {
                            if (flightPrice.equalsIgnoreCase(selectedFlightPrice)) {
                                defaultAipiOdo = returnFlightArr.get(i).getAipiBean();
                                tempForwardPricedItineraryArray.add(optionBean);
                            }
                        }
                        if (optionBean.getRefNum().equalsIgnoreCase("2")) {
                            if (flightPrice.equalsIgnoreCase(selectedFlightPrice)) {
                                optionBean.setOtherFlight(false);
                            } else {
                                optionBean.setOtherFlight(true);
                            }
                            tempFlightItineraryArray.add(optionBean);
                        }
                    }
                    for (int j = 0; j < tempForwardPricedItineraryArray.size(); j++) {
                        if (flightPrice.equalsIgnoreCase(selectedFlightPrice)) {
                            if (tempForwardPricedItineraryArray.get(j).getRph().equalsIgnoreCase(selectedRphNumber)) {
                                ArrayList<OriginDestinationOptionBean> tempStorageArray = new ArrayList<>();
                                tempStorageArray.add(tempForwardPricedItineraryArray.get(j));
                                tempPiArr.get(0).getAiBean().setOriginDestinationOptionBeanArrayList(tempStorageArray);
                                PricedItineraryObject pricedItineraryObject = new PricedItineraryObject();
                                AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                                aiBean.setDirId(tempPiArr.get(0).getAiBean().getDirId());
                                aiBean.setOriginDestinationOptionBeanArrayList(tempStorageArray);
                                pricedItineraryObject.setAiBean(aiBean);
                                pricedItineraryObject.setAipiBean(tempPiArr.get(0).getAipiBean());
                                pricedItineraryObject.setFareCombinationRs(false);
                                piObj = pricedItineraryObject;
                                totalPackagePrice = piObj.getAipiBean().getItfObject().gettFare();
                                tempPricedItineraryArray.add(pricedItineraryObject);
                            }
                        }
                    }
                    for (int k = 0; k < returnFCFlightArr.size(); k++) {
                        for (int j = 0; j < returnFCFlightArr.get(k).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                            OriginDestinationOptionBean optionBean = returnFCFlightArr.get(k).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                            if (optionBean.getRph().equalsIgnoreCase(selectedRphNumber)) {
                                forwardFlightPrice = returnFCFlightArr.get(k).getAipiBean().getItfObject().gettFare();
                                Double pkgPrice = 0.0, forwardPrice = 0.0;
                                if (!totalPackagePrice.isEmpty()) {
                                    pkgPrice = Double.parseDouble(totalPackagePrice);
                                }
                                if (!forwardFlightPrice.isEmpty()) {
                                    forwardPrice = Double.parseDouble(forwardFlightPrice);
                                }
                                Double resultPayPrice = pkgPrice - forwardPrice;
                                returnFlightPrice = Utils.convertonly_PriceToUserSelectionPrice(resultPayPrice, getApplicationContext());
                            }
                        }
                    }
                    for (int k = 0; k < tempFlightItineraryArray.size(); k++) {
                        ArrayList<OriginDestinationOptionBean> tempStorageArray = new ArrayList<>();
                        tempStorageArray.add(tempFlightItineraryArray.get(k));
                        if (!rphNumberList.contains(tempStorageArray.get(0).getRph())) {
                            rphNumberList.add(tempStorageArray.get(0).getRph());
                            tempPiArr.get(0).getAiBean().setOriginDestinationOptionBeanArrayList(tempStorageArray);
                            PricedItineraryObject pricedItineraryObject = new PricedItineraryObject();
                            AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                            aiBean.setDirId(tempPiArr.get(0).getAiBean().getDirId());
                            aiBean.setOriginDestinationOptionBeanArrayList(tempStorageArray);
                            pricedItineraryObject.setAiBean(aiBean);
                            AirItineraryPricingInfoBean airItineraryPricingInfoBean = new AirItineraryPricingInfoBean();
                            airItineraryPricingInfoBean.setPfbdObjectBeanArrayList(tempPiArr.get(0).getAipiBean().getPfbdObjectBeanArrayList());
                            airItineraryPricingInfoBean.setFq(tempPiArr.get(0).getAipiBean().getFq());
                            ItinTotalFareObjectBean itinTotalFareObjectBean = new ItinTotalFareObjectBean();
                            itinTotalFareObjectBean.setBf(tempPiArr.get(0).getAipiBean().getItfObject().getBf());
                            itinTotalFareObjectBean.setDisc(tempPiArr.get(0).getAipiBean().getItfObject().getDisc());
                            itinTotalFareObjectBean.setIc(tempPiArr.get(0).getAipiBean().getItfObject().getIc());
                            itinTotalFareObjectBean.settFare(tempPiArr.get(0).getAipiBean().getItfObject().gettFare());
                            itinTotalFareObjectBean.settSF(tempPiArr.get(0).getAipiBean().getItfObject().gettSF());
                            itinTotalFareObjectBean.settTx(tempPiArr.get(0).getAipiBean().getItfObject().gettTx());
                            itinTotalFareObjectBean.setUsfc(tempPiArr.get(0).getAipiBean().getItfObject().getUsfc());
                            //totalPackagePrice = piObj.getAipiBean().getItfObject().gettFare();
                            if (!totalPackagePrice.isEmpty()) {
                                Double fltPkgPrice = Double.parseDouble(Utils.convertonly_PriceToUserSelectionPrice(Double.parseDouble(tempPiArr.get(0).getAipiBean().getItfObject().gettFare()), getApplicationContext()));
                                Double pkgPrice = Double.parseDouble(Utils.convertonly_PriceToUserSelectionPrice(Double.parseDouble(totalPackagePrice), getApplicationContext()));
                                if (fltPkgPrice - pkgPrice == 0.0) {
                                    itinTotalFareObjectBean.setPriceDiff(String.valueOf(pkgPrice));
                                    pricedItineraryObject.setFareCombinationRs(false);
                                } else {
                                    Double priceDiff = 0.0;
                                    if (!returnFlightPrice.isEmpty()) {
                                        priceDiff = pkgPrice + fltPkgPrice - Double.parseDouble(returnFlightPrice);
                                    } else {
                                        priceDiff = pkgPrice + fltPkgPrice;
                                    }
                                    itinTotalFareObjectBean.setPriceDiff(String.valueOf(priceDiff));
                                    pricedItineraryObject.setFareCombinationRs(true);
                                }
                                airItineraryPricingInfoBean.setItfObject(itinTotalFareObjectBean);
                                pricedItineraryObject.setAipiBean(airItineraryPricingInfoBean);
                                tempPricedItineraryArray.add(pricedItineraryObject);
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
            for (int k = 0; k < returnFCFlightArr.size(); k++) {
                for (int j = 0; j < returnFCFlightArr.get(k).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean optionBean = returnFCFlightArr.get(k).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    if (optionBean.getRph().equalsIgnoreCase(selectedRphNumber)) {
                        forwardFlightPrice = returnFCFlightArr.get(k).getAipiBean().getItfObject().gettFare();
                        Double pkgPrice = Double.parseDouble(totalPackagePrice);
                        Double forwardPrice = Double.parseDouble(forwardFlightPrice);
                        Double resultPayPrice = pkgPrice - forwardPrice;
                        returnFlightPrice = String.valueOf(resultPayPrice);
                    }
                }
            }
            ArrayList<OriginDestinationOptionBean> tempForwardPIArr = new ArrayList<>();
            rphNumberList.clear();
            for (int i = 0; i < returnFlightArr.size(); i++) {
                tempFlightItineraryArray.clear();
                tempForwardPIArr.clear();
                ArrayList<PricedItineraryObject> tempPiArr = new ArrayList<>();
                tempPiArr.add(returnFlightArr.get(i));
                String flightPrice = tempPiArr.get(0).getAipiBean().getItfObject().gettFare();
                for (int j = 0; j < tempPiArr.get(0).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean optionBean = tempPiArr.get(0).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    if (optionBean.getRefNum().equalsIgnoreCase("1")) {
                        if (flightPrice.equalsIgnoreCase(selectedFlightPrice)) {
                            defaultAipiOdo = returnFlightArr.get(i).getAipiBean();
                            tempForwardPIArr.add(optionBean);
                        }
                    }
                    if (optionBean.getRefNum().equalsIgnoreCase("2")) {
                        if (flightPrice.equalsIgnoreCase(selectedFlightPrice)) {
                            optionBean.setOtherFlight(false);
                        } else {
                            optionBean.setOtherFlight(true);
                        }
                        tempFlightItineraryArray.add(optionBean);
                    }
                }
                for (int j = 0; j < tempForwardPIArr.size(); j++) {
                    if (flightPrice.equalsIgnoreCase(selectedFlightPrice)) {
                        if (tempForwardPIArr.get(j).getRph().equalsIgnoreCase(selectedRphNumber)) {
                            ArrayList<OriginDestinationOptionBean> tempStorageArray = new ArrayList<>();
                            tempStorageArray.add(tempForwardPIArr.get(j));
                            tempPiArr.get(0).getAiBean().setOriginDestinationOptionBeanArrayList(tempStorageArray);
                            PricedItineraryObject pricedItineraryObject = new PricedItineraryObject();
                            AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                            aiBean.setDirId(tempPiArr.get(0).getAiBean().getDirId());
                            aiBean.setOriginDestinationOptionBeanArrayList(tempStorageArray);
                            pricedItineraryObject.setAiBean(aiBean);
                            pricedItineraryObject.setAipiBean(tempPiArr.get(0).getAipiBean());
                            pricedItineraryObject.setFareCombinationRs(false);
                            piObj = pricedItineraryObject;
                            totalPackagePrice = piObj.getAipiBean().getItfObject().gettFare();
                            //tempPricedItineraryArray.add(pricedItineraryObject);
                        }
                    }
                }
                for (int k = 0; k < tempFlightItineraryArray.size(); k++) {
                    ArrayList<OriginDestinationOptionBean> tempStorageArray = new ArrayList<>();
                    tempStorageArray.add(tempFlightItineraryArray.get(k));
                    if (!rphNumberList.contains(tempStorageArray.get(0).getRph())) {
                        rphNumberList.add(tempStorageArray.get(0).getRph());
                        tempPiArr.get(0).getAiBean().setOriginDestinationOptionBeanArrayList(tempStorageArray);
                        PricedItineraryObject pricedItineraryObject = new PricedItineraryObject();
                        AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                        aiBean.setDirId(tempPiArr.get(0).getAiBean().getDirId());
                        aiBean.setOriginDestinationOptionBeanArrayList(tempStorageArray);
                        pricedItineraryObject.setAiBean(aiBean);
                        AirItineraryPricingInfoBean airItineraryPricingInfoBean = new AirItineraryPricingInfoBean();
                        airItineraryPricingInfoBean.setPfbdObjectBeanArrayList(tempPiArr.get(0).getAipiBean().getPfbdObjectBeanArrayList());
                        airItineraryPricingInfoBean.setFq(tempPiArr.get(0).getAipiBean().getFq());
                        ItinTotalFareObjectBean itinTotalFareObjectBean = new ItinTotalFareObjectBean();
                        itinTotalFareObjectBean.setBf(tempPiArr.get(0).getAipiBean().getItfObject().getBf());
                        itinTotalFareObjectBean.setDisc(tempPiArr.get(0).getAipiBean().getItfObject().getDisc());
                        itinTotalFareObjectBean.setIc(tempPiArr.get(0).getAipiBean().getItfObject().getIc());
                        itinTotalFareObjectBean.settFare(tempPiArr.get(0).getAipiBean().getItfObject().gettFare());
                        itinTotalFareObjectBean.settSF(tempPiArr.get(0).getAipiBean().getItfObject().gettSF());
                        itinTotalFareObjectBean.settTx(tempPiArr.get(0).getAipiBean().getItfObject().gettTx());
                        itinTotalFareObjectBean.setUsfc(tempPiArr.get(0).getAipiBean().getItfObject().getUsfc());
                        //totalPackagePrice = piObj.getAipiBean().getItfObject().gettFare();
                        Double fltPkgPrice = Double.parseDouble(Utils.convertonly_PriceToUserSelectionPrice(Double.parseDouble(tempPiArr.get(0).getAipiBean().getItfObject().gettFare()), getApplicationContext()));
                        Double pkgPrice = Double.parseDouble(Utils.convertonly_PriceToUserSelectionPrice(Double.parseDouble(totalPackagePrice), getApplicationContext()));
                        if (fltPkgPrice - pkgPrice == 0.0) {
                            itinTotalFareObjectBean.setPriceDiff(String.valueOf(pkgPrice));
                            pricedItineraryObject.setFareCombinationRs(false);
                        } else {
                            Double priceDiff = 0.0;
                            if (!returnFlightPrice.isEmpty()) {
                                priceDiff = pkgPrice + fltPkgPrice - Double.parseDouble(returnFlightPrice);
                            } else {
                                priceDiff = pkgPrice + fltPkgPrice;
                            }
                            itinTotalFareObjectBean.setPriceDiff(String.valueOf(priceDiff));
                            pricedItineraryObject.setFareCombinationRs(true);
                        }

                        airItineraryPricingInfoBean.setItfObject(itinTotalFareObjectBean);
                        pricedItineraryObject.setAipiBean(airItineraryPricingInfoBean);
                        //tempPricedItineraryArray.add(pricedItineraryObject);
                    }
                }
            }
            try {
                if (!forwardFlightPrice.isEmpty() && !returnFlightPrice.isEmpty()) {
                    for (int i = 0; i < returnFCFlightArr.size(); i++) {
                        ArrayList<OriginDestinationOptionBean> tempFCFlightItineraryArray = new ArrayList<>();
                        ArrayList<PricedItineraryObject> tempPiArr = new ArrayList<>();
                        tempPiArr.add(returnFCFlightArr.get(i));
                        String flightPrice = tempPiArr.get(0).getAipiBean().getItfObject().gettFare();
                        for (int j = 0; j < returnFCFlightArr.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                            OriginDestinationOptionBean optionBean = returnFCFlightArr.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                            if (optionBean.getRefNum().equalsIgnoreCase("1")) {
                                if (optionBean.getRph().equalsIgnoreCase(selectedRphNumber)) {
                                    firstAiPiOdo = returnFCFlightArr.get(i).getAipiBean();
                                    forwardFlightPrice = returnFCFlightArr.get(i).getAipiBean().getItfObject().gettFare();
                                }
                            }
                            if (optionBean.getRefNum().equalsIgnoreCase("2")) {
                                tempFCFlightItineraryArray.add(optionBean);
                            }
                        }
                        for (int k = 0; k < tempFCFlightItineraryArray.size(); k++) {
                            ArrayList<OriginDestinationOptionBean> tempStorageArray = new ArrayList<>();
                            tempStorageArray.add(tempFCFlightItineraryArray.get(k));
                            if (!rphNumberList.contains(tempFCFlightItineraryArray.get(k).getRph())) {
                                tempPiArr.get(0).getAiBean().setOriginDestinationOptionBeanArrayList(tempStorageArray);
                                PricedItineraryObject pricedItineraryObject = new PricedItineraryObject();
                                AirItineraryObjectBean aiBean = new AirItineraryObjectBean();
                                aiBean.setDirId(tempPiArr.get(0).getAiBean().getDirId());
                                aiBean.setOriginDestinationOptionBeanArrayList(tempStorageArray);
                                pricedItineraryObject.setAiBean(aiBean);
                                AirItineraryPricingInfoBean airItineraryPricingInfoBean = new AirItineraryPricingInfoBean();
                                airItineraryPricingInfoBean.setPfbdObjectBeanArrayList(tempPiArr.get(0).getAipiBean().getPfbdObjectBeanArrayList());
                                airItineraryPricingInfoBean.setFq(tempPiArr.get(0).getAipiBean().getFq());
                                ItinTotalFareObjectBean itinTotalFareObjectBean = new ItinTotalFareObjectBean();
                                itinTotalFareObjectBean.setBf(tempPiArr.get(0).getAipiBean().getItfObject().getBf());
                                itinTotalFareObjectBean.setDisc(tempPiArr.get(0).getAipiBean().getItfObject().getDisc());
                                itinTotalFareObjectBean.setIc(tempPiArr.get(0).getAipiBean().getItfObject().getIc());
                                itinTotalFareObjectBean.settFare(tempPiArr.get(0).getAipiBean().getItfObject().gettFare());
                                itinTotalFareObjectBean.settSF(tempPiArr.get(0).getAipiBean().getItfObject().gettSF());
                                itinTotalFareObjectBean.settTx(tempPiArr.get(0).getAipiBean().getItfObject().gettTx());
                                itinTotalFareObjectBean.setUsfc(tempPiArr.get(0).getAipiBean().getItfObject().getUsfc());
                                String actualPrice = Utils.convertonly_PriceToUserSelectionPrice(Double.parseDouble(tempPiArr.get(0).getAipiBean().getItfObject().gettFare()), getApplicationContext());
                                String forwardPrice = Utils.convertonly_PriceToUserSelectionPrice(Double.parseDouble(forwardFlightPrice), getApplicationContext());
                                String pkgPrice = Utils.convertonly_PriceToUserSelectionPrice(Double.parseDouble(totalPackagePrice), getApplicationContext());
                                Double resultPrice = 0.0;
                                if (Double.parseDouble(actualPrice) != Double.parseDouble(pkgPrice)) {
                                    resultPrice = (Double.parseDouble(actualPrice) + Double.parseDouble(forwardPrice)) - Double.parseDouble(pkgPrice);
                                }
                                itinTotalFareObjectBean.setPriceDiff(String.valueOf(resultPrice + Double.parseDouble(pkgPrice)));
                                airItineraryPricingInfoBean.setItfObject(itinTotalFareObjectBean);
                                pricedItineraryObject.setAipiBean(airItineraryPricingInfoBean);
                                pricedItineraryObject.setFareCombinationRs(true);
                                tempPricedItineraryArray.add(pricedItineraryObject);
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
        OriginDestinationOptionBean odoObj = tempPricedItineraryArray.get(0).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
        try {
            ArrayList<PricedItineraryObject> filterPiArr = new ArrayList<>();
            pricedItineraryObjectArrayList.clear();
            for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                        pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                        filterPiArr.add(tempPricedItineraryArray.get(i));
                    }
                }
            }
            sortByPrice();
            if (filterPiArr.size() > 0) {
                Singleton.getInstance().returnFlightPIArrayList = filterPiArr;
            }
        } catch (Exception e) {
        }
        AsyncTaskJson task = new AsyncTaskJson();
        task.execute();
        TextView backTextView = (TextView) findViewById(R.id.back_text);
        TextView sourceNameTextView = (TextView) findViewById(R.id.source_name);
        TextView destinationNameTextView = (TextView) findViewById(R.id.destination_name);
        TextView journeyDateTextView = (TextView) findViewById(R.id.journey_datetext);
        TextView travellerCountTextView = (TextView) findViewById(R.id.traveller_count);
        TextView filterText = (TextView) findViewById(R.id.filter_text);
        TextView classTypeTextView = (TextView) findViewById(R.id.class_type);
        LinearLayout filterLayout = (LinearLayout) findViewById(R.id.filter_layout);
        LinearLayout returnBackLayout = (LinearLayout) findViewById(R.id.layout_back);
        ListView returnListView = (ListView) findViewById(R.id.flights_listview);
        flightListLayout = (RelativeLayout) findViewById(R.id.rec_view_layout);
        headerViewLayout = (RelativeLayout) findViewById(R.id.header_view_layout);
        View headerView = getLayoutInflater().inflate(R.layout.flight_info_header_layout, null);
        LinearLayout returnHeaderLayout = (LinearLayout) headerView.findViewById(R.id.round_trip_header_layout);
        ImageView forwardPlaneImage = (ImageView) headerView.findViewById(R.id.forward_plane_image);
        TextView forwardHeaderText = (TextView) headerView.findViewById(R.id.forward_header_text);
        ImageView forwardFlightImage = (ImageView) headerView.findViewById(R.id.forward_flight_image);
        TextView forwardFlightDetailsText = (TextView) headerView.findViewById(R.id.forward_flight_details_text);
        TextView forwardFlightInfo = (TextView) headerView.findViewById(R.id.forward_flight_info);
        TextView refundTypeText = (TextView) headerView.findViewById(R.id.refund_type_text);
        TextView totalPassengerText = (TextView) headerView.findViewById(R.id.total_passengers_text);
        TextView priceDecimalText = (TextView) headerView.findViewById(R.id.price_decimal_text);
        TextView flightPriceText = (TextView) headerView.findViewById(R.id.price_text);
        TextView currencyText = (TextView) headerView.findViewById(R.id.currency_text);
        LinearLayout flightInfoHeaderLayout = (LinearLayout) headerView.findViewById(R.id.flight_info_header_layout);
        TextView tripTypeTextView = (TextView) headerView.findViewById(R.id.trip_type_textview);
        ImageView flightHeaderImage = (ImageView) headerView.findViewById(R.id.flight_image);
        TextView flightHeaderText = (TextView) headerView.findViewById(R.id.flight_journey_text);
        View headerDividerView = (View) headerView.findViewById(R.id.header_divider_view);
        totalCountLayout = (LinearLayout) headerView.findViewById(R.id.total_count_layout);
        earliestCountLayout = (LinearLayout) headerView.findViewById(R.id.earliest_count_layout);
        fastestCountLayout = (LinearLayout) headerView.findViewById(R.id.fastest_count_layout);
        totalFlightCountTextView = (TextView) headerView.findViewById(R.id.total_flightcount);
        flightCountEarlierTextView = (TextView) headerView.findViewById(R.id.flight_count_earliest);
        flightCountFastestTextView = (TextView) headerView.findViewById(R.id.flight_count_fastest);
        RecyclerView recyclerView = (RecyclerView) headerView.findViewById(R.id.flights_recyclelistview);
        ImageView flightFlowImage = (ImageView) findViewById(R.id.flight_flow_image);
        returnHeaderLayout.setVisibility(View.VISIBLE);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        if (isArabicLang) {
            errorMessageText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        mAdapter = new HorizontalScrollRecyclerViewAdapter(FlightReturnResultsActivity.this, flightsArrayList, airLineNamesMap,
                FlightReturnResultsActivity.this, "2");
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        regularFace = Typeface.createFromAsset(getAssets(), fontTitle);
        Typeface mediumFace = Typeface.createFromAsset(getAssets(), fontNormal);
        Typeface boldFace = Typeface.createFromAsset(getAssets(), fontMedium);
        backTextView.setTypeface(regularFace);
        sourceNameTextView.setTypeface(regularFace);
        destinationNameTextView.setTypeface(regularFace);
        journeyDateTextView.setTypeface(regularFace);
        travellerCountTextView.setTypeface(regularFace);
        classTypeTextView.setTypeface(regularFace);
        totalFlightCountTextView.setTypeface(regularFace);
        flightCountEarlierTextView.setTypeface(regularFace);
        flightCountFastestTextView.setTypeface(regularFace);
        flightHeaderText.setTypeface(mediumFace);
        forwardHeaderText.setTypeface(mediumFace);
        forwardFlightDetailsText.setTypeface(regularFace);
        totalPassengerText.setTypeface(regularFace);
        forwardFlightInfo.setTypeface(regularFace);
        refundTypeText.setTypeface(regularFace);
        currencyText.setTypeface(regularFace);
        flightPriceText.setTypeface(boldFace);
        priceDecimalText.setTypeface(regularFace);
        errorMessageText.setTypeface(regularFace);
        filterText.setTypeface(regularFace);
        tripTypeTextView.setTypeface(regularFace);
        flightHeaderImage.setRotation(180);
        if (isArabicLang) {
            backTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            sourceNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            destinationNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            journeyDateTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            travellerCountTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            classTypeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            totalFlightCountTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            flightCountEarlierTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            flightCountFastestTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            flightHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            flightFlowImage.setRotation(180);
            flightHeaderImage.setRotation(360);
            forwardPlaneImage.setRotation(180);
            forwardHeaderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            forwardFlightDetailsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            totalPassengerText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_airline_name_text));
            forwardFlightInfo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
            refundTypeText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
            currencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
            flightPriceText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
            priceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_time_text));
            filterText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            tripTypeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
        }
        String imagePath = Constants.IMAGE_BASE_URL + odoObj.getFsDataBeanArrayList().get(0).getMal() + Constants.IMAGE_FILE_FORMAT;
        Glide.with(FlightReturnResultsActivity.this).load(imagePath).placeholder(R.drawable.imagethumb_search).into(forwardFlightImage);
        String dateDiff = "";
        try {
            dateDiff = Utils.dayDiff(odoObj.getFsDataBeanArrayList().get(0).getDdt(), odoObj.getFsDataBeanArrayList().get(odoObj.getFsDataBeanArrayList().size() - 1).getArdt());
        } catch (Exception e) {
        }
        String forwardDepartTime = Utils.formatDateToTime(odoObj.getFsDataBeanArrayList().get(0).getDdt(), FlightReturnResultsActivity.this);
        String forwardArrTime = Utils.formatDateToTime(odoObj.getFsDataBeanArrayList().get(odoObj.getFsDataBeanArrayList().size() - 1).getArdt(), FlightReturnResultsActivity.this);
        String forwardSourceCode = odoObj.getFsDataBeanArrayList().get(0).getDap();
        String forwardDestCode = odoObj.getFsDataBeanArrayList().get(odoObj.getFsDataBeanArrayList().size() - 1).getAap();
        if (dateDiff.isEmpty()) {
            if (isArabicLang) {
                forwardFlightDetailsText.setText("(" + forwardDestCode + ")" + forwardArrTime + " - " + "(" + forwardSourceCode + ")" + forwardDepartTime);
            } else {
                forwardFlightDetailsText.setText(forwardDepartTime + "(" + forwardSourceCode + ")" + " - " + forwardArrTime + "(" + forwardDestCode + ")");
            }
        } else {
            if (isArabicLang) {
                forwardFlightDetailsText.setText("(" + forwardDestCode + ")" + "(" + dateDiff + ")" + forwardArrTime + " - " + "(" + forwardSourceCode + ")" + forwardDepartTime);
            } else {
                forwardFlightDetailsText.setText(forwardDepartTime + "(" + forwardSourceCode + ")" + " - " + forwardArrTime + "(" + dateDiff + ")" + "(" + forwardDestCode + ")");
            }
        }
        if (Integer.parseInt(travellerCount) == 1) {
            totalPassengerText.setText(travellerCount + " " + getResources().getString(R.string.label_add_traveller));
        } else {
            totalPassengerText.setText(travellerCount + " " + getResources().getString(R.string.label_search_flight_travellers));
        }
        int stopCount = -1;
        try {
            stopCount = Integer.parseInt(odoObj.getFsDataBeanArrayList().get(0).getSq());
        } catch (Exception e) {
            Utils.printMessage(TAG, "SQ : " + stopCount);
        }
        String stopType = "";
        if (stopCount == 0) {
            stopType = getString(R.string.label_non_stop_text);
        } else if (stopCount == 1) {
            if (isArabicLang) {
                stopType = getString(R.string.label_stop_text) + " " + stopCount;
            } else {
                stopType = stopCount + " " + getString(R.string.label_stop_text);
            }
        } else {
            if (isArabicLang) {
                stopType = getString(R.string.label_stops_text) + " " + stopCount;
            } else {
                stopType = stopCount + " " + getString(R.string.label_stops_text);
            }
        }
        forwardFlightInfo.setText(airLineNamesMap.get(odoObj.getFsDataBeanArrayList().get(0).getMal()) + " " + odoObj.getFsDataBeanArrayList().get(0).getMal() + "-" + odoObj.getFsDataBeanArrayList().get(0).getFn() + ", " + stopType + ", ");
        if (piObj.getRefundablestate()) {
            forwardFlightInfo.append(Utils.addRefundTextField(getString(R.string.RefLbl), FlightReturnResultsActivity.this));
        } else {
            forwardFlightInfo.append(Utils.addNonRefundTextField(getString(R.string.NonRefLbl), FlightReturnResultsActivity.this));
        }
        Double price = Double.valueOf(piObj.getAipiBean().getItfObject().gettFare());
        String actualPriceResult = Utils.convertonly_PriceToUserSelectionPrice(price, getApplicationContext());
        String priceResult[] = actualPriceResult.split("\\.");
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String currencyResult = pref.getString(Constants.SELECTED_CURRENCY_CODE, "SAR");
        if (isArabicLang) {
            currencyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            flightPriceText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
            priceDecimalText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_city_text));
            flightPriceText.setText("." + priceResult[0] + " ");
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new RecyclerView.LayoutParams(
                    RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));
            params.setMargins(0, 0, 0, 0);
            currencyText.setLayoutParams(params);
            currencyText.setText(priceResult[1]);
            if (currencyResult.equalsIgnoreCase("SAR")) {
                priceDecimalText.setText(getString(R.string.label_SAR_currency_name));
            } else {
                priceDecimalText.setText(currencyResult);
            }
//            else if(currencyResult.equalsIgnoreCase("EGP")){
//                priceDecimalText.setText(getString(R.string.label_EGP_currency_name));
//            }
        } else {
            currencyText.setText(currencyResult);
            flightPriceText.setText(priceResult[0] + ".");
            priceDecimalText.setText(priceResult[1]);
        }
        String journeyCabinType = "";
        if (selectedClassType.equalsIgnoreCase("Economy")) {
            journeyCabinType = getString(R.string.label_search_flight_economy);
        } else if (selectedClassType.equalsIgnoreCase("Business")) {
            journeyCabinType = getString(R.string.label_search_flight_business);
        } else if (selectedClassType.equalsIgnoreCase("First")) {
            journeyCabinType = getString(R.string.label_search_flight_first);
        }
        classTypeTextView.setText(journeyCabinType);
        sourceNameTextView.setText(sourceName);
        destinationNameTextView.setText(returnName);
        forwardHeaderText.setText(String.format(getResources().getString(R.string.label_select_your_header_flight), sourceName, returnName));
        flightHeaderText.setText(String.format(getResources().getString(R.string.label_select_your_return_flight), returnName, sourceName));
        if (tripType.equalsIgnoreCase("1")) {
            flightInfoHeaderLayout.setVisibility(View.GONE);
            headerDividerView.setVisibility(View.GONE);
            flightFlowImage.setImageResource(R.drawable.rightarrow);
            journeyDateTextView.setText(Utils.formatDateToString(forwardFlightDate, FlightReturnResultsActivity.this));
            tripTypeTextView.setText(getString(R.string.label_search_oneway));
        } else {
            flightInfoHeaderLayout.setVisibility(View.VISIBLE);
            headerDividerView.setVisibility(View.VISIBLE);
            flightFlowImage.setImageResource(R.drawable.vice_versa);
            tripTypeTextView.setText(getString(R.string.label_search_roundtrip));
            journeyDateTextView.setText(String.format(getResources().getString(R.string.label_placeholder_to), Utils.formatDateToString(forwardFlightDate, FlightReturnResultsActivity.this), Utils.formatDateToString(returnFlightDate, FlightReturnResultsActivity.this)));
        }
        travellerCountTextView.setText(travellerCount);
        returnBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = getIntent();
                setResult(RESULT_OK, it);
                finish();
            }
        });
        earliestCountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                earliestCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.white_color));
                fastestCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_view_bg));
                totalCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_view_bg));
                fastestFlights = false;
                totalFlights = false;
                earliestFlights = true;
                handleEarliestFlights();
                setFlightsCountsUI();
            }
        });
        totalCountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                earliestCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_view_bg));
                fastestCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_view_bg));
                totalCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.white_color));
                fastestFlights = false;
                totalFlights = true;
                earliestFlights = false;
                handleFlightsCount();
                setFlightsCountsUI();
                handleFilterResults();
            }
        });
        fastestCountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                earliestCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_view_bg));
                fastestCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.white_color));
                totalCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_view_bg));
                fastestFlights = true;
                totalFlights = false;
                earliestFlights = false;
                handleNonStopFlights();
                setFlightsCountsUI();
            }
        });
        returnListView.addHeaderView(headerView);
        roundTripAdapterList = new FlightRoundTripAdapter(FlightReturnResultsActivity.this, FlightReturnResultsActivity.this,
                pricedItineraryObjectArrayList, airportNamesMap, airLineNamesMap, travellerCount, sourceName, returnName,
                FlightReturnResultsActivity.this, tripType, selectedClassType, totalPackagePrice, returnFlightPrice);
        returnListView.setAdapter(roundTripAdapterList);
        roundTripAdapterList.notifyDataSetChanged();
        returnListView.setFocusable(false);
        totalFlights = true;
        setFlightsCountsUI();
        handleNonStopFlights();
        handleEarliestFlights();
        handleFlightsCount();
        filterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handlefilter();
            }
        });
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView waitText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setTypeface(regularFace);
        waitText.setTypeface(regularFace);
        if (isArabicLang) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            waitText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        headerViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        try {
            headerViewLayout.removeView(loadingView);
            loadingView = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "DATA::" + data);
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            closeLoading();
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(regularFace);
            errorDescriptionText.setTypeface(regularFace);
            searchButton.setTypeface(regularFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            headerViewLayout.addView(errorView);
        } else if (serviceType == GET_FARE_RULE_RESULTS) {
            JSONObject resultDataObj = null;
            String tdrResult = "";
            try {
                resultDataObj = new JSONObject(data);
                tdrResult = resultDataObj.getJSONObject("fareruleRS").getString("tdr");
            } catch (Exception e) {
                Utils.printMessage(TAG, "Exception occured::" + e.getMessage());
            }
            if (tdrResult == null || tdrResult.equalsIgnoreCase("")) {
                closeLoading();
                displayErrorMessage(getResources().getString(R.string.label_selected_flight_error_message));
                return;
            } else {
                tripStatusArr.clear();
                try {
                    resultDataObj = new JSONObject(data);
                    if (resultDataObj.getJSONObject("fareruleRS").has("ai")) {
                        if (resultDataObj.getJSONObject("fareruleRS").getJSONObject("ai").has("odos")) {
                            if (resultDataObj.getJSONObject("fareruleRS").getJSONObject("ai").getJSONObject("odos").has("odo")) {
                                JSONArray odoArray = resultDataObj.getJSONObject("fareruleRS").getJSONObject("ai").getJSONObject("odos").getJSONArray("odo");
                                for (int j = 0; j < odoArray.length(); j++) {
                                    tripStatusArr.add(odoArray.getJSONObject(j).getString("tripStatus"));
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                }
                if (tripStatusArr.contains("Fail")) {
                    closeLoading();
                    displayErrorMessage(getResources().getString(R.string.label_selected_flight_error_message));
                    return;
                } else {
                    Intent intent = new Intent(FlightReturnResultsActivity.this, TripSummaryActivity.class);
                    intent.putExtra(Constants.JSON_RESULT, data);
                    intent.putExtra(Constants.TRIP_TYPE, String.valueOf(tripType));
                    intent.putExtra(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                    intent.putExtra(Constants.AIPI_JSON_OBJECT, aipiJSONObject);
                    intent.putExtra(Constants.AI_JSON_OBJECT, aiJSONObject);
                    intent.putExtra(Constants.FORWARD_SOURCE, source);
                    intent.putExtra(Constants.FORWARD_DESTINATION, destination);
                    intent.putExtra(Constants.SECOND_FORWARD_SOURCE, secondSource);
                    intent.putExtra(Constants.SECOND_FORWARD_DESTINATION, secondDestination);
                    intent.putExtra(Constants.THIRD_FORWARD_SOURCE, thirdSource);
                    intent.putExtra(Constants.THIRD_FORWARD_DESTINATION, thirdDestination);
                    intent.putExtra(Constants.FORWARD_FLIGHT_DATE, forwardFlightDate);
                    intent.putExtra(Constants.RETURN_FLIGHT_DATE, returnFlightDate);
                    startActivityForResult(intent, SEARCH_FLIGHT_RESULT);
                }
            }
        } else if (serviceType == GET_ACCESS_TOKEN) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(data);
                String accessToken = obj.getString("accessToken");
                String expireIn = obj.getString("expireIn");
                String refreshToken = obj.getString("refreshToken");
                long timeInSecs = Calendar.getInstance().getTimeInMillis() / 1000;
                SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Constants.ACCESS_TOKEN, accessToken);
                editor.putString(Constants.EXPIRE_IN, expireIn);
                editor.putString(Constants.REFRESH_TOKEN, refreshToken);
                editor.putLong(Constants.TOKEN_GENERATED_TIME, timeInSecs);
                editor.apply();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    public void handlefilter() {
        if (loadingView != null) {
            return;
        } else {
            if (mainPricedItineraryArray.size() == 0) {
                return;
            } else {
                boolean isdetailfilter = false;
                Intent intent = new Intent(FlightReturnResultsActivity.this, FlightReturnFilterActivity.class);
                intent.putExtra(Constants.TRIP_TYPE, tripType);
                intent.putExtra(Constants.FORWARD_SOURCE, sourceName);
                intent.putExtra(Constants.FORWARD_DESTINATION, returnName);
                intent.putExtra(Constants.SECOND_FORWARD_SOURCE, secondsourceName);
                intent.putExtra(Constants.SECOND_FORWARD_DESTINATION, secondreturnName);
                intent.putExtra(Constants.THIRD_FORWARD_SOURCE, thirdsourceName);
                intent.putExtra(Constants.THIRD_FORWARD_DESTINATION, thirdreturnName);
                intent.putExtra(Constants.FORWARD_DETAILS, journeyDetails);
                intent.putExtra(Constants.PRICE_SELECTED_MINIMUM_VALUE, minValue);
                intent.putExtra(Constants.PRICE_SELECTED_MAXIMUM_VALUE, maxValue);
                intent.putExtra(Constants.ONE_WAY_NONSTOP_CHECKED, isNonStopChecked);
                intent.putExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, multiCityNonStopChecked);
                intent.putExtra(Constants.IS_DETAIL_FILTER, isdetailfilter);
                intent.putExtra(Constants.AIRLINE_NAME_HASHMAP, airLineNamesMap);
                intent.putExtra(Constants.AIRPORT_NAME_HASHMAP, airportNamesMap);
                intent.putExtra(Constants.AIRLINE_NAME, selectedMal);
                intent.putExtra(Constants.FORWARD_FLIGHT_PRICE, forwardFlightPrice);
                intent.putExtra(Constants.PACKAGE_FLIGHT_PRICE, totalPackagePrice);
                startActivityForResult(intent, FLIGHT_PRICE_FILTER);
                //overridePendingTransition(R.anim.translate_left_side, R.anim.translate_right_side);
            }
        }
    }

    private void handleHorizontalFlights(ArrayList<PricedItineraryObject> pricedItineraryObjectArrayList) {
        flightsArrayList.clear();
        for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
            final PricedItineraryObject pricedItineraryObject = pricedItineraryObjectArrayList.get(i);
            for (int j = 0; j < pricedItineraryObject.getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                final OriginDestinationOptionBean odoObject = pricedItineraryObject.getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                String refNum = odoObject.getRefNum();
                if (refNum.equalsIgnoreCase("2")) {
                    boolean isContains = false;
                    FlightBean flightBean = new FlightBean();
                    for (int k = 0; k < flightsArrayList.size(); k++) {
                        if (flightsArrayList.get(k).getImage().equalsIgnoreCase(odoObject.getFsDataBeanArrayList().get(0).getMal())) {
                            isContains = true;
                            break;
                        }
                    }
                    if (!isContains) {
                        flightBean.setImage(odoObject.getFsDataBeanArrayList().get(0).getMal());
                        flightBean.setPrice(pricedItineraryObject.getAipiBean().getItfObject().getPriceDiff());
                        flightsArrayList.add(flightBean);
                    }
                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void handleEarliestFlights() {
        pricedItineraryObjectArrayList.clear();
        ArrayList<OriginDestinationOptionBean> tempOdoArray = new ArrayList<>();
        for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
            for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                if (bean.getRefNum().equalsIgnoreCase("2")) {
                    int departureTimeVal = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                    if (departureTimeVal == 2) {
                        pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                        tempOdoArray.add(bean);
                    }
                }
            }
        }
        handleHorizontalFlights(pricedItineraryObjectArrayList);
        mAdapter.notifyDataSetChanged();
        roundTripAdapterList.notifyDataSetChanged();
    }

    private void handleFlightsCount() {
        pricedItineraryObjectArrayList.clear();
        for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
            for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                    pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                }
            }
        }
        handleHorizontalFlights(pricedItineraryObjectArrayList);
        mAdapter.notifyDataSetChanged();
        roundTripAdapterList.notifyDataSetChanged();
    }

    private void handleNonStopFlights() {
        pricedItineraryObjectArrayList.clear();
        ArrayList<OriginDestinationOptionBean> tempOdoArray = new ArrayList<>();
        for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
            for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                if (bean.getRefNum().equalsIgnoreCase("2")) {
                    int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                    if (stopType == 0) {
                        pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                        tempOdoArray.add(bean);
                    }
                }
            }
        }
        if (pricedItineraryObjectArrayList.size() == 0) {
            pricedItineraryObjectArrayList.clear();
            for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                        int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                        if (stopType == 1) {
                            pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            tempOdoArray.add(bean);
                        }
                    }
                }
            }
        }
        handleHorizontalFlights(pricedItineraryObjectArrayList);
        mAdapter.notifyDataSetChanged();
        roundTripAdapterList.notifyDataSetChanged();
    }

    private void setFlightsCountsUI() {
        if (!isTextUpdated && totalCount == 0 && earliestCount == 0 && fastestCount == 0) {
            isTextUpdated = true;
            for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                    totalCount++;
                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                        int departureTimeVal = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                        if (departureTimeVal == 2) {
                            earliestCount = earliestCount + 1;
                        }
                    }
                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                        int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                        if (stopType == 0) {
                            fastestCount = fastestCount + 1;
                        }
                    }
                }
            }
            if (fastestCount == 0) {
                for (int i = 0; i < pricedItineraryObjectArrayList.size(); i++) {
                    for (int j = 0; j < pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                        OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                        if (bean.getRefNum().equalsIgnoreCase("2")) {
                            int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                            if (stopType == 1) {
                                fastestCount = fastestCount + 1;
                            }
                        }
                    }
                }
            }
        }
        if (totalFlights) {
            totalFlightCountTextView.setText(String.valueOf(totalCount));
            totalFlightCountTextView.setTextColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.flight_select_color));
            totalFlightCountTextView.append(Utils.addFlightTextField(getString(R.string.label_flights), FlightReturnResultsActivity.this));
        } else {
            totalFlightCountTextView.setText(String.valueOf(totalCount));
            totalFlightCountTextView.setTextColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_header_label_color));
            totalFlightCountTextView.append(Utils.addFlightHoverTextField(getString(R.string.label_flights), FlightReturnResultsActivity.this));
        }
        if (earliestFlights) {
            flightCountEarlierTextView.setText(String.valueOf(earliestCount));
            flightCountEarlierTextView.setTextColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.flight_select_color));
            flightCountEarlierTextView.append(Utils.addFlightTextField(getString(R.string.label_earliest), FlightReturnResultsActivity.this));
        } else {
            flightCountEarlierTextView.setText(String.valueOf(earliestCount));
            flightCountEarlierTextView.setTextColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_header_label_color));
            flightCountEarlierTextView.append(Utils.addFlightHoverTextField(getString(R.string.label_earliest), FlightReturnResultsActivity.this));
        }
        if (fastestFlights) {
            flightCountFastestTextView.setText(String.valueOf(fastestCount));
            flightCountFastestTextView.setTextColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.flight_select_color));
            flightCountFastestTextView.append(Utils.addFlightTextField(getString(R.string.label_fastest), FlightReturnResultsActivity.this));
        } else {
            flightCountFastestTextView.setText(String.valueOf(fastestCount));
            flightCountFastestTextView.setTextColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_header_label_color));
            flightCountFastestTextView.append(Utils.addFlightHoverTextField(getString(R.string.label_fastest), FlightReturnResultsActivity.this));
        }
        mAdapter.notifyDataSetChanged();
        roundTripAdapterList.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FLIGHT_PRICE_FILTER) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    isFilterApplied = true;
                    handleFlightsCount();
                    handleDefaultFlightsUi();
                    pricedItineraryObjectArrayList.clear();
                    minValue = data.getStringExtra(Constants.PRICE_MINIMUM_VALUE);
                    maxValue = data.getStringExtra(Constants.PRICE_MAXIMUM_VALUE);
                    isNonStopChecked = data.getBooleanExtra(Constants.ONE_WAY_NONSTOP_CHECKED, false);
                    multiCityNonStopChecked = data.getBooleanExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, false);
                    double minVal = Double.parseDouble(minValue);
                    double maxVal = Double.parseDouble(maxValue);
                    Utils.printMessage(TAG, "Min Value :: " + minVal + " Max Value :: " + maxVal);
                    for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                        if (tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getRefNum().equalsIgnoreCase("2")) {
                            double price = Double.parseDouble(tempPricedItineraryArray.get(i).getAipiBean().getItfObject().getPriceDiff());
                            if (minVal <= price && maxVal >= price) {
                                pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            }
                        }
                    }
                    AsyncTaskJson task = new AsyncTaskJson();
                    task.execute();
                    refundStatusArray = Singleton.getInstance().refundStatusArrayList;
                    boolean isRefundSelected = true;
                    boolean isNonRefundSelected = true;
                    if (refundStatusArray.size() == 2) {
                        for (FilterRefundSelectionItem refundVar : refundStatusArray) {
                            if (refundVar.isSelected()) {
                                if (refundVar.getRefundType() == 0) {
                                    isRefundSelected = false;
                                } else if (refundVar.getRefundType() == 1) {
                                    isNonRefundSelected = false;
                                }
                            }
                        }
                    }
                    if (!isRefundSelected && isNonRefundSelected) {
                        for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                            for (int j = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size() - 1; j >= 0; j--) {
                                String refNum = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getRefNum();
                                OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                                if (refNum.equalsIgnoreCase("2")) {
                                    if (!bean.getRs().equalsIgnoreCase("Refundable") && !bean.getRs().equalsIgnoreCase("Refundable Before Departure")) {
                                        pricedItineraryObjectArrayList.remove(i);
                                    }
                                }
                            }
                        }
                    } else if (isRefundSelected && !isNonRefundSelected) {
                        for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                            for (int j = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size() - 1; j >= 0; j--) {
                                String refNum = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j).getRefNum();
                                OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                                if (refNum.equalsIgnoreCase("2")) {
                                    if (!bean.getRs().equalsIgnoreCase("Non Refundable") && !bean.getRs().equalsIgnoreCase("null")) {
                                        pricedItineraryObjectArrayList.remove(i);
                                    }
                                }
                            }
                        }
                    }
                    Utils.getFlightStopValues(this, tripType, isNonStopChecked, multiCityNonStopChecked, pricedItineraryObjectArrayList);
                    stopsArray = Singleton.getInstance().resultStopsArrayList;
                    updateStopsFlights(stopsArray, isNonStopChecked);
                    airlineArray = Singleton.getInstance().returnResultAirlinesArrayList;
                    boolean isAirlineNoneSelected = true;
                    ArrayList<String> selectedAirlineList = new ArrayList<>();
                    for (FilterAirlineSelectionItem airlineVar : airlineArray) {
                        if (airlineVar.isSelected()) {
                            isAirlineNoneSelected = false;
                            selectedAirlineList.add(airlineVar.getMal());
                        }
                    }
                    if (!isAirlineNoneSelected) {
                        for (FilterAirlineSelectionItem selectedAirlineVar : airlineArray) {
                            if (!selectedAirlineVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airlineList = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                                        airlineList.add(bean.getFsDataBeanArrayList().get(0).getMal());
                                    }
                                    if (checkAirlineList(selectedAirlineList, airlineList).isEmpty()) {
                                        pricedItineraryObjectArrayList.remove(i);
                                    }
                                }
                            }
                        }
                    }
                    if (selectedAirlineList.size() > 0) {
                        for (FlightBean airlineVar : flightsArrayList) {
                            if (selectedAirlineList.contains(airlineVar.getImage())) {
                                airlineVar.setDataCleared(false);
                                airlineVar.setClicked(true);
                            }
                        }
                    }
                    airportListArray = Singleton.getInstance().resultAirportsArrayList;
                    tempArray1 = Singleton.getInstance().firstAirportsArrayList;
                    tempArray2 = Singleton.getInstance().secondAirportsArrayList;
                    tempArray3 = Singleton.getInstance().thirdAirportsArrayList;
                    boolean isNoneSelectedInSection1 = true;
                    boolean isNoneSelectedInSection2 = true;
                    boolean isNoneSelectedInSection3 = true;
                    ArrayList<String> firstAirportList = new ArrayList<>();
                    ArrayList<String> secondAirportList = new ArrayList<>();
                    ArrayList<String> thirdAirportList = new ArrayList<>();
                    for (FilterAirportSelectionItem airportItems : airportListArray) {
                        if (airportItems.isSelected()) {
                            if (airportItems.getAirportType() == 1) {
                                isNoneSelectedInSection1 = false;
                                firstAirportList.add(airportItems.getAirportCode());
                            } else if (airportItems.getAirportType() == 2) {
                                isNoneSelectedInSection2 = false;
                                secondAirportList.add(airportItems.getAirportCode());
                            } else {
                                isNoneSelectedInSection3 = false;
                                thirdAirportList.add(airportItems.getAirportCode());
                            }
                        }
                    }
                    if (!isNoneSelectedInSection1) {
                        for (FilterAirportSelectionItem selectedAirportVar : tempArray1) {
                            if (!selectedAirportVar.isSelected()) {
                                int airportType = selectedAirportVar.getAirportType();
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airportCodeArr = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                                        for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                            airportCodeArr.add(bean.getFsDataBeanArrayList().get(k).getDap());
                                        }
                                        if (airportType == 1) {
                                            if (checkAirlineList(firstAirportList, airportCodeArr).isEmpty()) {
                                                pricedItineraryObjectArrayList.remove(i);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!isNoneSelectedInSection2) {
                        for (FilterAirportSelectionItem selectedAirportVar : tempArray2) {
                            if (!selectedAirportVar.isSelected()) {
                                int airportType = selectedAirportVar.getAirportType();
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airportCodeArr = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                                        for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                            airportCodeArr.add(bean.getFsDataBeanArrayList().get(k).getAap());
                                        }
                                        if (airportType == 2) {
                                            if (checkAirlineList(secondAirportList, airportCodeArr).isEmpty()) {
                                                pricedItineraryObjectArrayList.remove(i);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!isNoneSelectedInSection3) {
                        for (FilterAirportSelectionItem selectedAirportVar : tempArray3) {
                            int airportType = selectedAirportVar.getAirportType();
                            if (!selectedAirportVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    ArrayList<String> airportCodeArr = new ArrayList<>();
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                                        for (int k = bean.getFsDataBeanArrayList().size() - 1; k >= 0; k--) {
                                            airportCodeArr.add(bean.getFsDataBeanArrayList().get(k).getDap());
                                        }
                                        if (airportType == 3) {
                                            if (checkAirlineList(thirdAirportList, airportCodeArr).isEmpty()) {
                                                pricedItineraryObjectArrayList.remove(i);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    timeArray = Singleton.getInstance().resultTimeArrayList;
                    dapTimeListArray = Singleton.getInstance().departureTimeArrayList;
                    aapTimeListArray = Singleton.getInstance().arrivalTimeArrayList;
                    boolean dapFlightLayoutSelected = true;
                    boolean aapFlightLayoutSelected = true;
                    ArrayList<Integer> departTimeArr = new ArrayList<>();
                    ArrayList<Integer> arrivalTimeArr = new ArrayList<>();
                    for (int i = 0; i < timeArray.size(); i++) {
                        for (FilterTimeSelectionItem flightTimeSelected : dapTimeListArray) {
                            if (flightTimeSelected.isSelected()) {
                                dapFlightLayoutSelected = false;
                                if (!departTimeArr.contains(flightTimeSelected.getTimeZone())) {
                                    departTimeArr.add(flightTimeSelected.getTimeZone());
                                }
                            }
                        }
                        for (FilterTimeSelectionItem flightTimeSelected : aapTimeListArray) {
                            if (flightTimeSelected.isSelected()) {
                                aapFlightLayoutSelected = false;
                                if (!arrivalTimeArr.contains(flightTimeSelected.getTimeZone())) {
                                    arrivalTimeArr.add(flightTimeSelected.getTimeZone());
                                }
                            }
                        }
                    }
                    if (!dapFlightLayoutSelected) {
                        for (FilterTimeSelectionItem selectedTimeVar : dapTimeListArray) {
                            if (!selectedTimeVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                                        int dapFlightTime = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                                        if (!departTimeArr.contains(dapFlightTime)) {
                                            pricedItineraryObjectArrayList.remove(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!aapFlightLayoutSelected) {
                        for (FilterTimeSelectionItem selectedTimeVar : aapTimeListArray) {
                            if (!selectedTimeVar.isSelected()) {
                                for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                                    OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                                    if (bean.getRefNum().equalsIgnoreCase("2")) {
                                        int aapFlightTime = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(bean.getFsDataBeanArrayList().size() - 1).getArdt());
                                        if (!arrivalTimeArr.contains(aapFlightTime)) {
                                            pricedItineraryObjectArrayList.remove(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    tempAirlinePIArrayList.clear();
                    tempAirlinePIArrayList.addAll(pricedItineraryObjectArrayList);
                    Utils.printMessage(TAG, "Apply Pi list size :: " + pricedItineraryObjectArrayList.size());
                    if (pricedItineraryObjectArrayList.size() == 0) {
                        handleFilterResults();
                        pricedItineraryObjectArrayList.clear();
                        for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                            for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                                OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                                if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                                    pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                                }
                            }
                        }
                        displayErrorMessage(getString(R.string.label_no_flights_available));
                        return;
                    }
                    roundTripAdapterList.notifyDataSetChanged();
                    mAdapter.notifyDataSetChanged();
                }
            }
            if (resultCode == RESULT_CANCELED) {
                if (data != null) {
                    isNonStopChecked = data.getBooleanExtra(Constants.ONE_WAY_NONSTOP_CHECKED, false);
                    multiCityNonStopChecked = data.getBooleanExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, false);
                    pricedItineraryObjectArrayList.clear();
                    for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                        for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                            OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                            if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                                pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            }
                        }
                    }
                    AsyncTaskJson task = new AsyncTaskJson();
                    task.execute();
                    minValue = null;
                    maxValue = null;
                    setFlightsCountsUI();
                    fastestCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_view_bg));
                    earliestCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_view_bg));
                    totalCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.white_color));
                    fastestFlights = false;
                    totalFlights = true;
                    earliestFlights = false;
                    handleFlightsCount();
                    handleDefaultFlightsUi();
                    selectedMal = "";
                    isFilterApplied = false;
                    for (FlightBean bean : flightsArrayList) {
                        bean.setClicked(false);
                        bean.setDataCleared(false);
                    }
                    Utils.printMessage(TAG, "Reset Button Pi list size :: " + pricedItineraryObjectArrayList.size());
                    if (pricedItineraryObjectArrayList.size() != 0) {
                        flightListLayout.removeView(noResultView);
                        noResultView = null;
                    }
                    roundTripAdapterList.notifyDataSetChanged();
                    mAdapter.notifyDataSetChanged();
                }
            }
        } else if (requestCode == SEARCH_FLIGHT_RESULT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    headerViewLayout.removeView(loadingView);
                    loadingView = null;
                }
            } else if (resultCode == Constants.CHANGE_FLIGHT) {
                if (loadingView != null) {
                    headerViewLayout.removeView(loadingView);
                    loadingView = null;
                    finish();
                }
            }
        }
    }

    private String appendBodyTypeValue(HashMap<Integer, AmenitiesBean> dataHashMap, int val, String moreData) {
        AmenitiesBean bean = dataHashMap.get(val);
        String strValue = "";
        if (bean != null) {
            strValue = bean.getEn();
            if (isArabicLang) {
                strValue = bean.getAr();
            }
            String resultText = "";
            resultText = moreData + "==" + strValue;
            return bean.getIcon() + "==" + resultText;
        }
        return "";
    }

    private String appendMerchandiseValues(HashMap<String, AmenitiesBean> dataHashMap, String data, String moreData) {
        AmenitiesBean bean = dataHashMap.get(data);
        String strVal = "";
        if (bean != null) {
            strVal = bean.getEn();
            if (isArabicLang) {
                strVal = bean.getAr();
            }
            String resultText = "";
            if (moreData.equalsIgnoreCase("")) {
                resultText = strVal;
            } else {
                resultText = strVal + "==" + moreData;
            }
            return bean.getIcon() + "==" + resultText;
        }
        return "";
    }

    private boolean merchandiseDataUI(ArrayList<FlightAmenitiesBean> mainMarchandiseArrayList) {
        try {
            FSDataBean fsDataBean = new FSDataBean();
            for (int i = 0; i < mainMarchandiseArrayList.size(); i++) {
                FlightAmenitiesBean flightAmenitiesBean = new FlightAmenitiesBean();
                OriginDestinationOptionBean odoObject;
                flightAmenitiesBean = mainMarchandiseArrayList.get(i);
                for (int j = 0; j < tempPricedItineraryArray.size(); j++) {
                    PricedItineraryObject pricedItineraryObject = tempPricedItineraryArray.get(j);
                    for (int l = 0; l < tempPricedItineraryArray.get(j).getAiBean().getOriginDestinationOptionBeanArrayList().size(); l++) {
                        odoObject = pricedItineraryObject.getAiBean().getOriginDestinationOptionBeanArrayList().get(l);
                        for (int k = 0; k < odoObject.getFsDataBeanArrayList().size(); k++) {
                            fsDataBean = odoObject.getFsDataBeanArrayList().get(k);
                            String flightName = fsDataBean.getMal() + "-" + fsDataBean.getEq();
                            String merchFlightName = flightAmenitiesBean.getFlightName();
                            if (flightName.contains(merchFlightName) &&
                                    fsDataBean.getDap().equalsIgnoreCase(flightAmenitiesBean.getDap()) &&
                                    fsDataBean.getAap().equalsIgnoreCase(flightAmenitiesBean.getAap())) {
                                ArrayList<String> arrayList = new ArrayList<>();
                                String aircraftVal[] = flightAmenitiesBean.getFlightName().split("-");
                                String aircraftType = Utils.getAircraftType(aircraftVal[1], FlightReturnResultsActivity.this);
                                if (!flightAmenitiesBean.getBodyType().isEmpty()) {
                                    for (Map.Entry<Integer, AmenitiesBean> entry : AmenitiesConstants.bodyTypeHashMap.entrySet()) {
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getBodyType())) {
                                                arrayList.add(appendBodyTypeValue(AmenitiesConstants.bodyTypeHashMap, entry.getKey(), aircraftType));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getBodyType())) {
                                                arrayList.add(appendBodyTypeValue(AmenitiesConstants.bodyTypeHashMap, entry.getKey(), aircraftType));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getSeatType().isEmpty()) {
                                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.seatTypeHashMap.entrySet()) {
                                        String moreData = "";
                                        String seatWidthText = "";
                                        String seatLengthText = "";
                                        String seatPitchText = "";
                                        if (!flightAmenitiesBean.getSeatWidth().isEmpty() && !flightAmenitiesBean.getSeatLength().isEmpty() && !flightAmenitiesBean.getSeatPitch().isEmpty()) {
                                            seatWidthText = getString(R.string.label_seat_width) + " " + flightAmenitiesBean.getSeatWidth();
                                            seatLengthText = getString(R.string.label_leg_room) + " " + flightAmenitiesBean.getSeatLength();
                                            seatPitchText = getString(R.string.label_pitch_size) + " " + flightAmenitiesBean.getSeatPitch();
                                            moreData = seatWidthText + ", " + seatLengthText + ", " + seatPitchText;
                                        } else if (!flightAmenitiesBean.getSeatWidth().isEmpty() && !flightAmenitiesBean.getSeatPitch().isEmpty()) {
                                            seatWidthText = getString(R.string.label_seat_width) + " " + flightAmenitiesBean.getSeatWidth();
                                            seatPitchText = getString(R.string.label_pitch_size) + " " + flightAmenitiesBean.getSeatPitch();
                                            moreData = seatWidthText + ", " + seatPitchText;
                                        }
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getSeatType())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.seatTypeHashMap, entry.getKey(), moreData));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getSeatType())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.seatTypeHashMap, entry.getKey(), moreData));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getSeatLayout().isEmpty()) {
                                    arrayList.add("seat_layout==" + getString(R.string.label_seat_layout) + " " + flightAmenitiesBean.getSeatLayout());
                                }
                                try {
                                    if (!flightAmenitiesBean.getMeals().isEmpty()) {
                                        for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.mealsHashMap.entrySet()) {
                                            if (isArabicLang) {
                                                if (entry.getValue().getAr().equals(flightAmenitiesBean.getMeals())) {
                                                    arrayList.add(appendMerchandiseValues(AmenitiesConstants.mealsHashMap, entry.getKey(), ""));
                                                }
                                            } else {
                                                if (entry.getValue().getEn().equals(flightAmenitiesBean.getMeals())) {
                                                    arrayList.add(appendMerchandiseValues(AmenitiesConstants.mealsHashMap, entry.getKey(), ""));
                                                }
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.getMessage();
                                }
                                if (!flightAmenitiesBean.getWifi().isEmpty()) {
                                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.wifiHashMap.entrySet()) {
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getWifi())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.wifiHashMap, entry.getKey(), ""));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getWifi())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.wifiHashMap, entry.getKey(), ""));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getAvod().isEmpty()) {
                                    if (flightAmenitiesBean.getAvod().equalsIgnoreCase("Yes")) {
                                        if (!flightAmenitiesBean.getVideoType().isEmpty()) {
                                            for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.avodVideoHashMap.entrySet()) {
                                                String screenSize = "";
                                                if (!flightAmenitiesBean.getAvodScreenSize().isEmpty()) {
                                                    screenSize = flightAmenitiesBean.getAvodScreenSize();
                                                }
                                                if (isArabicLang) {
                                                    if (entry.getValue().getAr().equals(flightAmenitiesBean.getVideoType())) {
                                                        arrayList.add(appendMerchandiseValues(AmenitiesConstants.avodVideoHashMap, entry.getKey(), screenSize));
                                                    }
                                                } else {
                                                    if (entry.getValue().getEn().equals(flightAmenitiesBean.getVideoType())) {
                                                        arrayList.add(appendMerchandiseValues(AmenitiesConstants.avodVideoHashMap, entry.getKey(), screenSize));
                                                    }
                                                }
                                            }
                                        } else {
                                            arrayList.add("avod==" + getString(R.string.label_avod));
                                        }
                                    }
                                    if (flightAmenitiesBean.getAvod().equalsIgnoreCase("Charge")) {
                                        if (!flightAmenitiesBean.getVideoType().isEmpty()) {
                                            for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.avodChargeVideoHashMap.entrySet()) {
                                                String screenSize = "";
                                                if (!flightAmenitiesBean.getAvodScreenSize().isEmpty()) {
                                                    screenSize = flightAmenitiesBean.getAvodScreenSize();
                                                }
                                                if (isArabicLang) {
                                                    if (entry.getValue().getAr().equals(flightAmenitiesBean.getVideoType())) {
                                                        arrayList.add(appendMerchandiseValues(AmenitiesConstants.avodChargeVideoHashMap, entry.getKey(), screenSize));
                                                    }
                                                } else {
                                                    if (entry.getValue().getEn().equals(flightAmenitiesBean.getVideoType())) {
                                                        arrayList.add(appendMerchandiseValues(AmenitiesConstants.avodChargeVideoHashMap, entry.getKey(), screenSize));
                                                    }
                                                }
                                            }
                                        } else {
                                            arrayList.add("avod==" + getString(R.string.label_avod) + " (" + getString(R.string.label_charge_text) + ")");
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getPowerType().isEmpty()) {
                                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.powerTypeHashMap.entrySet()) {
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getPowerType())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.powerTypeHashMap, entry.getKey(), ""));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getPowerType())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.powerTypeHashMap, entry.getKey(), ""));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getMobile().isEmpty()) {
                                    arrayList.add(appendMerchandiseValues(AmenitiesConstants.mobileHashMap, flightAmenitiesBean.getMobile(), ""));
                                }
                                if (!flightAmenitiesBean.getInfantCare().isEmpty()) {
                                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.infantCareHashMap.entrySet()) {
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getInfantCare())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.infantCareHashMap, entry.getKey(), ""));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getInfantCare())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.infantCareHashMap, entry.getKey(), ""));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getAlocohol().isEmpty()) {
                                    for (Map.Entry<String, AmenitiesBean> entry : AmenitiesConstants.alocoholHashMap.entrySet()) {
                                        if (isArabicLang) {
                                            if (entry.getValue().getAr().equals(flightAmenitiesBean.getAlocohol())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.alocoholHashMap, entry.getKey(), ""));
                                            }
                                        } else {
                                            if (entry.getValue().getEn().equals(flightAmenitiesBean.getAlocohol())) {
                                                arrayList.add(appendMerchandiseValues(AmenitiesConstants.alocoholHashMap, entry.getKey(), ""));
                                            }
                                        }
                                    }
                                }
                                if (!flightAmenitiesBean.getPrayerArea().isEmpty()) {
                                    arrayList.add(appendMerchandiseValues(AmenitiesConstants.prayerAreaHashMap, flightAmenitiesBean.getPrayerArea(), ""));
                                }
                                if (!flightAmenitiesBean.getShowerArea().isEmpty()) {
                                    arrayList.add(appendMerchandiseValues(AmenitiesConstants.showerAreaHashMap, flightAmenitiesBean.getShowerArea(), ""));
                                }
                                if (!flightAmenitiesBean.getRemarks().isEmpty()) {
                                    arrayList.add("==" + flightAmenitiesBean.getRemarks());
                                }
                                if (!flightAmenitiesBean.getProvideTransportation().isEmpty()) {
                                    arrayList.add("==" + getString(R.string.label_provide_transportation));
                                }
                                if (!flightAmenitiesBean.getProvideOtherInfo().isEmpty()) {
                                    arrayList.add("==" + flightAmenitiesBean.getProvideOtherInfo());
                                }
                                fsDataBean.setFlightAmenitiesBean(flightAmenitiesBean);
                                fsDataBean.setIconNameArrayList(arrayList);
                            }
                        }
                    }
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void updateStopsFlights(ArrayList<FilterStopSelectionItem> stopsArray, boolean isNonStopChecked) {
        if (isNonStopChecked) {
            for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                OriginDestinationOptionBean odoBean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                String currentStopType = odoBean.getFsDataBeanArrayList().get(0).getSq();
                if (currentStopType.equalsIgnoreCase("null")) {
                    pricedItineraryObjectArrayList.remove(i);
                } else if (!currentStopType.equalsIgnoreCase("0")) {
                    pricedItineraryObjectArrayList.remove(i);
                }
            }
            if (pricedItineraryObjectArrayList.size() == 0) {
                //displayErrorMessage("Non-stop Flights are not available");
                pricedItineraryObjectArrayList.clear();
                for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                    for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                        OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                        if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                            pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                        }
                    }
                }
            }
        } else {
            boolean isStopsNoneSelected = true;
            ArrayList<Integer> selectedStopsList = new ArrayList<>();
            if (stopsArray.size() != 0) {
                for (FilterStopSelectionItem stopVar : stopsArray) {
                    if (stopVar.isSelected()) {
                        isStopsNoneSelected = false;
                        selectedStopsList.add(stopVar.getStopType());
                    }
                }
            }
            if (!isStopsNoneSelected) {
                for (FilterStopSelectionItem selectedVar : stopsArray) {
                    if (!selectedVar.isSelected()) {
                        for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                            OriginDestinationOptionBean odoBean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                            ArrayList<Integer> selectedList = new ArrayList<>();
                            String refNum = odoBean.getRefNum();
                            String currentStopType = odoBean.getFsDataBeanArrayList().get(0).getSq();
                            if (refNum.equalsIgnoreCase("2")) {
                                selectedList.add(Integer.parseInt(currentStopType));
                            }
                            if (checkStopsList(selectedStopsList, selectedList).isEmpty()) {
                                pricedItineraryObjectArrayList.remove(i);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        selectedAirlineItem(position);
    }

    @Override
    public void onSelectedItemClick(int position, int type) {
        int durationInMinutes = 0;
        String departureTime = "";
        String arrivalTime = "";
        for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
            if (tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getRefNum().equalsIgnoreCase("1")) {
                firstAiOdo = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
            }
        }
        OriginDestinationOptionBean secondOdo = pricedItineraryObjectArrayList.get(position).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
        if (firstAiOdo != null) {
            departureTime = firstAiOdo.getFsDataBeanArrayList().get(firstAiOdo.getFsDataBeanArrayList().size() - 1).getArdt();
            arrivalTime = secondOdo.getFsDataBeanArrayList().get(0).getDdt();
            durationInMinutes = Utils.getJourneyDifferenceTime(departureTime, arrivalTime);
            if (durationInMinutes < Constants.TIME_DIFFERENCE_MINUTES) {
                displayErrorMessage(getResources().getString(R.string.label_time_difference_error_msg));
                return;
            }
        }
        if (!pricedItineraryObjectArrayList.get(position).isFareCombinationRs()) {
            handleCheckAvailabilityRQ(position, false);
        } else {
            handleCheckAvailabilityRQ(position, true);
        }
    }

    private void handleCheckAvailabilityRQ(int position, final boolean isFareCombin) {
        String requestJsonData = "";
        requestJsonData = handleFareRQRequest(position, isFareCombin);
        Utils.printMessage(TAG, "requestJsonData : " + requestJsonData);
        if (isInternetPresent) {
            showLoading();
            if (isFareCombin) {
                HTTPAsync async = new HTTPAsync(FlightReturnResultsActivity.this, FlightReturnResultsActivity.this, Constants.FARE_COMBINATION_AVAILABILITY_RQ,
                        "", requestJsonData, GET_FARE_RULE_RESULTS, HTTPAsync.METHOD_POST);
                async.execute();
            } else {
                HTTPAsync async = new HTTPAsync(FlightReturnResultsActivity.this, FlightReturnResultsActivity.this, Constants.FARE_RQ_URL,
                        "", requestJsonData, GET_FARE_RULE_RESULTS, HTTPAsync.METHOD_POST);
                async.execute();
            }
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(regularFace);
            errorDescriptionText.setTypeface(regularFace);
            searchButton.setTypeface(regularFace);
            final String finalRequestJsonData = requestJsonData;
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(FlightReturnResultsActivity.this);
                    if (isInternetPresent) {
                        headerViewLayout.removeView(errorView);
                        showLoading();
                        if (isFareCombin) {
                            HTTPAsync async = new HTTPAsync(FlightReturnResultsActivity.this, FlightReturnResultsActivity.this, Constants.FARE_COMBINATION_AVAILABILITY_RQ,
                                    "", finalRequestJsonData, GET_FARE_RULE_RESULTS, HTTPAsync.METHOD_POST);
                            async.execute();
                        } else {
                            HTTPAsync async = new HTTPAsync(FlightReturnResultsActivity.this, FlightReturnResultsActivity.this, Constants.FARE_RQ_URL,
                                    "", finalRequestJsonData, GET_FARE_RULE_RESULTS, HTTPAsync.METHOD_POST);
                            async.execute();
                        }
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            headerViewLayout.addView(errorView);
        }
    }

    private String handleFareRQRequest(int position, boolean isFareCombination) {
        JSONObject finalObj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        try {
            JSONObject mainJSON = new JSONObject();
            JSONObject sourceObj = new JSONObject();
            sourceObj = Utils.getAppSettingData(sourceObj, FlightReturnResultsActivity.this);
            sourceObj.put("isoCurrency", "SAR");
            sourceObj.put("echoToken", randomNumber);
            sourceObj.put("timeStamp", timeStamp);
            sourceObj.put("clientId", Constants.CLIENT_ID);
            sourceObj.put("clientSecret", Constants.CLIENT_SECRET);
            sourceObj.put("accessToken", Utils.getRequestAccessToken(FlightReturnResultsActivity.this));
            sourceObj.put("groupType", groupType);
            mainJSON.put("source", sourceObj);
            JSONArray arr = new JSONArray();
            arr.put(createFSObject(firstAiOdo, isFareCombination));
            arr.put(createFSObject(pricedItineraryObjectArrayList.get(position).getAiBean().getOriginDestinationOptionBeanArrayList().get(0), isFareCombination));
            JSONObject aiJson = new JSONObject();
            aiJson.put("dirId", tempPricedItineraryArray.get(0).getAiBean().getDirId());
            JSONObject odoArrObj = new JSONObject();
            odoArrObj.put("odo", arr);
            aiJson.put("odos", odoArrObj);
            JSONObject aipiObject = new JSONObject();
            if (isFareCombination) {
                JSONArray itfArr = new JSONArray();
                itfArr.put(buildITFArray(-1, position, isFareCombination));
                itfArr.put(buildITFArray(0, position, isFareCombination));
                aipiObject.put("itf", itfArr);
            } else {
                aipiObject.put("itf", buildITFArray(-1, position, isFareCombination));
            }
            JSONObject pfbdObj = new JSONObject();
            pfbdObj.put("pfbd", buildPFBDArray(position, isFareCombination));
            aipiObject.put("pfbds", pfbdObj);
            if (isFareCombination) {
                aipiObject.put("fq", Utils.checkStringValue(firstAiPiOdo.getFq()));
            } else {
                if (pricedItineraryObjectArrayList.get(position).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).isOtherFlight()) {
                    aipiObject.put("fq", Utils.checkStringValue(pricedItineraryObjectArrayList.get(position).getAipiBean().getFq()));
                } else {
                    aipiObject.put("fq", Utils.checkStringValue(defaultAipiOdo.getFq()));
                }
            }
            mainJSON.put("source", sourceObj);
            mainJSON.put("ai", aiJson);
            mainJSON.put("aipi", aipiObject);
            mainJSON.put("ptq", Singleton.getInstance().ptqArr);
            mainJSON.put("extra", "");
            aipiJSONObject = aipiObject.toString();
            aiJSONObject = aiJson.toString();
            finalObj.put("fareruleRQ", mainJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalObj.toString();
    }

    private JSONObject createFSObject(OriginDestinationOptionBean odoBean, boolean isFareCombination) {
        JSONArray array = new JSONArray();
        for (int i = 0; i < odoBean.getFsDataBeanArrayList().size(); i++) {
            FSDataBean dataBean = odoBean.getFsDataBeanArrayList().get(i);
            JSONObject detailsObj = new JSONObject();
            try {
                detailsObj.put("dap", Utils.checkStringValue(dataBean.getDap()));
                detailsObj.put("dt", Utils.checkStringValue(dataBean.getDt()));
                detailsObj.put("aap", Utils.checkStringValue(dataBean.getAap()));
                detailsObj.put("at", Utils.checkStringValue(dataBean.getAt()));
                detailsObj.put("oal", Utils.checkStringValue(dataBean.getOal()));
                detailsObj.put("mal", Utils.checkStringValue(dataBean.getMal()));
                detailsObj.put("eq", Utils.checkStringValue(dataBean.getEq()));
                detailsObj.put("ct", Utils.checkStringValue(dataBean.getCt()));
                detailsObj.put("dur", Utils.checkStringValue(dataBean.getDur()));
                detailsObj.put("fdur", Utils.checkStringValue(dataBean.getFdur()));
                detailsObj.put("st", Utils.checkStringValue(dataBean.getSt()));
                detailsObj.put("si", Utils.checkStringValue(dataBean.getSi()));
                detailsObj.put("ti", Utils.checkStringValue(dataBean.getTi()));
                detailsObj.put("fi", Utils.checkStringValue(dataBean.getFi()));
                detailsObj.put("comment", Utils.checkStringValue(dataBean.getComment()));
                detailsObj.put("as", Utils.checkStringValue(dataBean.getAs()));
                detailsObj.put("fc", Utils.checkStringValue(dataBean.getFc()));
                detailsObj.put("tofi", Utils.checkStringValue(dataBean.getTofi()));
                detailsObj.put("fn", Utils.checkStringValue(dataBean.getFn()));
                detailsObj.put("sq", Utils.checkStringValue(dataBean.getSq()));
                detailsObj.put("is", Utils.checkStringValue(dataBean.getIs()));
                detailsObj.put("ardt", Utils.checkStringValue(dataBean.getArdt()));
                detailsObj.put("ddt", Utils.checkStringValue(dataBean.getDdt()));
                array.put(detailsObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        JSONObject odoObj = new JSONObject();
        try {
            odoObj.put("refNum", Utils.checkStringValue(odoBean.getRefNum()));
            odoObj.put("rph", Utils.checkStringValue(odoBean.getRph()));
            odoObj.put("mOfcId", Utils.checkStringValue(odoBean.getmOfcId()));
            odoObj.put("fs", array);
            odoObj.put("tt", Utils.checkStringValue(odoBean.getTt()));
            odoObj.put("rs", Utils.checkStringValue(odoBean.getRs()));
            odoObj.put("cb", Utils.checkStringValue(odoBean.getCb()));
            odoObj.put("pt", Utils.checkStringValue(odoBean.getPt()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return odoObj;
    }

    private JSONObject buildITFArray(int firstPos, int position, boolean isFareCombination) {
        JSONObject itfObj = new JSONObject();
        try {
            AirItineraryPricingInfoBean selectedAiPiOdo = null;
            String referenceNumber = "";
            if (firstPos == -1) {
                if (isFareCombination) {
                    selectedAiPiOdo = firstAiPiOdo;
                } else {
                    if (pricedItineraryObjectArrayList.get(position).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).isOtherFlight()) {
                        selectedAiPiOdo = pricedItineraryObjectArrayList.get(position).getAipiBean();
                    } else {
                        selectedAiPiOdo = defaultAipiOdo;
                    }
                }
                referenceNumber = "1";
            } else {
                selectedAiPiOdo = pricedItineraryObjectArrayList.get(position).getAipiBean();
                referenceNumber = "2";
            }
            if (selectedAiPiOdo != null) {
                if (isFareCombination) {
                    itfObj.put("refNum", referenceNumber);
                }
                itfObj.put("bf", Utils.checkStringValue(selectedAiPiOdo.getItfObject().getBf()));
                itfObj.put("tTx", Utils.checkStringValue(selectedAiPiOdo.getItfObject().gettTx()));
                itfObj.put("tSF", Utils.checkStringValue(selectedAiPiOdo.getItfObject().gettSF()));
                itfObj.put("disc", Utils.checkStringValue(selectedAiPiOdo.getItfObject().getDisc()));
                itfObj.put("tFare", Utils.checkStringValue(selectedAiPiOdo.getItfObject().gettFare()));
                itfObj.put("ic", Utils.checkStringValue(selectedAiPiOdo.getItfObject().getIc()));
                itfObj.put("usfc", Utils.checkStringValue(selectedAiPiOdo.getItfObject().getUsfc()));
            }
        } catch (Exception e) {
        }
        return itfObj;
    }

    private JSONArray buildPFBDArray(int position, boolean isFareCombination) {
        JSONArray pfbdArray = new JSONArray();
        try {
            if (isFareCombination) {
                for (int j = 0; j < firstAiPiOdo.getPfbdObjectBeanArrayList().size(); j++) {
                    pfbdArray.put(addPaxPfbdObj("1", firstAiPiOdo.getPfbdObjectBeanArrayList().get(j)));
                }
                for (int j = 0; j < pricedItineraryObjectArrayList.get(position).getAipiBean().getPfbdObjectBeanArrayList().size(); j++) {
                    pfbdArray.put(addPaxPfbdObj("2", pricedItineraryObjectArrayList.get(position).getAipiBean().getPfbdObjectBeanArrayList().get(j)));
                }
            } else {
                if (pricedItineraryObjectArrayList.get(position).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).isOtherFlight()) {
                    for (int i = 0; i < pricedItineraryObjectArrayList.get(position).getAipiBean().getPfbdObjectBeanArrayList().size(); i++) {
                        pfbdArray.put(buildNormalPfbdObj(pricedItineraryObjectArrayList.get(position).getAipiBean().getPfbdObjectBeanArrayList().get(i)));
                    }
                } else {
                    for (int i = 0; i < defaultAipiOdo.getPfbdObjectBeanArrayList().size(); i++) {
                        pfbdArray.put(buildNormalPfbdObj(defaultAipiOdo.getPfbdObjectBeanArrayList().get(i)));
                    }
                }
            }
        } catch (Exception e) {
        }
        return pfbdArray;
    }

    private JSONObject addPaxPfbdObj(String referenceNumber, PFBDObjectBean pfbdObjBean) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("refNum", referenceNumber);
            obj.put("pType", Utils.checkStringValue(pfbdObjBean.getpType()));
            obj.put("qty", Utils.checkStringValue(pfbdObjBean.getQty()));
            obj.put("usfcp", Utils.checkStringValue(pfbdObjBean.getUsfcp()));
            JSONObject pfObj = new JSONObject();
            pfObj.put("pbf", pfbdObjBean.getPassengerFareObjectBean().getPbf());
            JSONArray txArray = new JSONArray();
            for (TaxObjectBean tBean : pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList()) {
                JSONObject tObj = new JSONObject();
                tObj.put("amt", Utils.checkStringValue(tBean.getAmt()));
                tObj.put("tc", Utils.checkStringValue(tBean.getTc()));
                txArray.put(tObj);
            }
            pfObj.put("txs", txArray);
            pfObj.put("ptFare", pfbdObjBean.getPassengerFareObjectBean().getPtFare());
            obj.put("pf", pfObj);
            JSONArray array = new JSONArray();
            if (pfbdObjBean.getFbcArray() != null) {
                for (String str : pfbdObjBean.getFbcArray()) {
                    array.put(str);
                }
            }
            obj.put("fbc", array);
        } catch (Exception e) {
        }
        return obj;
    }

    private JSONObject buildNormalPfbdObj(PFBDObjectBean pfbdObjBean) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("pType", Utils.checkStringValue(pfbdObjBean.getpType()));
            obj.put("qty", Utils.checkStringValue(pfbdObjBean.getQty()));
            obj.put("usfcp", Utils.checkStringValue(pfbdObjBean.getUsfcp()));
            JSONArray array = new JSONArray();
            if (pfbdObjBean.getFbcArray() != null) {
                for (String str : pfbdObjBean.getFbcArray()) {
                    array.put(str);
                }
            }
            obj.put("fbc", array);
            JSONObject pfObj = new JSONObject();
            pfObj.put("pbf", pfbdObjBean.getPassengerFareObjectBean().getPbf());
            JSONArray txArray = new JSONArray();
            for (TaxObjectBean tBean : pfbdObjBean.getPassengerFareObjectBean().getTaxObjectBeanArrayList()) {
                JSONObject tObj = new JSONObject();
                tObj.put("amt", Utils.checkStringValue(tBean.getAmt()));
                tObj.put("tc", Utils.checkStringValue(tBean.getTc()));
                txArray.put(tObj);
            }
            pfObj.put("txs", txArray);
            pfObj.put("ptFare", pfbdObjBean.getPassengerFareObjectBean().getPtFare());
            obj.put("pf", pfObj);
        } catch (Exception e) {
        }
        return obj;

    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private ArrayList<String> checkAirlineList(ArrayList<String> selectedList, ArrayList<String> beanList) {
        List<String> firstList = new ArrayList<>(selectedList);
        List<String> secondList = new ArrayList<>(beanList);
        ArrayList<String> resultList = new ArrayList<>(secondList);
        resultList.retainAll(firstList);
        return resultList;
    }

    private ArrayList<Integer> checkStopsList(ArrayList<Integer> selectedList, ArrayList<Integer> beanList) {
        List<Integer> firstList = new ArrayList<>(selectedList);
        List<Integer> secondList = new ArrayList<>(beanList);
        ArrayList<Integer> resultList = new ArrayList<>(secondList);
        resultList.retainAll(firstList);
        return resultList;
    }

    private void handleDefaultFlightsUi() {
        totalCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.white_color));
        earliestCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_view_bg));
        fastestCountLayout.setBackgroundColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_view_bg));
        totalFlightCountTextView.setTextColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.result_compo_duration));
        flightCountEarlierTextView.setTextColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_header_label_color));
        flightCountFastestTextView.setTextColor(Utils.getColor(FlightReturnResultsActivity.this, R.color.multi_header_label_color));
        totalFlightCountTextView.setText(String.valueOf(totalCount));
        totalFlightCountTextView.append(Utils.addFlightTextField(getString(R.string.label_flights), FlightReturnResultsActivity.this));
        flightCountEarlierTextView.setText(String.valueOf(earliestCount));
        flightCountEarlierTextView.append(Utils.addFlightHoverTextField(getString(R.string.label_earliest), FlightReturnResultsActivity.this));
        flightCountFastestTextView.setText(String.valueOf(fastestCount));
        flightCountFastestTextView.append(Utils.addFlightHoverTextField(getString(R.string.label_fastest), FlightReturnResultsActivity.this));
    }

    private void selectedAirlineItem(int position) {
//        if(isFilterApplied){
//            for (FlightBean bean : flightsArrayList) {
//                bean.setClicked(false);
//                bean.setDataCleared(false);
//            }
//        }
        selectedMal = "";
        FlightBean flightBean = flightsArrayList.get(position);
        boolean isStatus = flightsArrayList.get(position).isClicked();
        if (isStatus) {
            Utils.printMessage(TAG, "Clicked PI size : " + pricedItineraryObjectArrayList.size());
            flightBean.setClicked(false);
            flightBean.setDataCleared(false);
            if (totalFlights) {
                pricedItineraryObjectArrayList.clear();
                for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                    for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                        OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                        if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                            pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                        }
                    }
                }
                handleHorizontalAirlineResults();
            } else if (earliestFlights) {
                pricedItineraryObjectArrayList.clear();
                for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                    for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                        OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                        if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                            int departureTimeVal = Utils.dateFormatToMinutes(odoBean.getFsDataBeanArrayList().get(0).getDdt());
                            if (departureTimeVal == 2) {
                                pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            }
                        }
                    }
                }
            } else if (fastestFlights) {
                pricedItineraryObjectArrayList.clear();
                for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                    for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                        OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                        if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                            int stopType = Integer.parseInt(odoBean.getFsDataBeanArrayList().get(0).getSq());
                            if (stopType == 0) {
                                pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            }
                        }
                    }
                }
                if (pricedItineraryObjectArrayList.size() == 0) {
                    for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                        for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                            OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                            if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                                int stopType = Integer.parseInt(odoBean.getFsDataBeanArrayList().get(0).getSq());
                                if (stopType == 1) {
                                    pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                                }
                            }
                        }
                    }
                }
            }
        } else {
            for (FlightBean bean : flightsArrayList) {
                bean.setClicked(false);
                bean.setDataCleared(false);
            }
            selectedMal = flightsArrayList.get(position).getImage();
            Utils.getSelectedAirlineValues(pricedItineraryObjectArrayList, selectedMal, false);
            flightBean.setClicked(true);
            Utils.printMessage(TAG, "Un Clicked PI size : " + pricedItineraryObjectArrayList.size());
            if (totalFlights) {
                pricedItineraryObjectArrayList.clear();
                if (isFilterApplied) {
                    pricedItineraryObjectArrayList.addAll(tempAirlinePIArrayList);
                } else {
                    for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                        for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                            OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                            if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                                pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            }
                        }
                    }
                }
                handleHorizontalAirlineResults();
                if (pricedItineraryObjectArrayList.size() == 0) {
                    flightBean.setDataCleared(true);
                    displayErrorMessage(getString(R.string.label_no_flights_available));
                    handleFilterResults();
                    for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                        for (int j = 0; j < tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().size(); j++) {
                            OriginDestinationOptionBean odoBean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(j);
                            if (odoBean.getRefNum().equalsIgnoreCase("2")) {
                                pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            }
                        }
                    }
                }
            } else if (earliestFlights) {
                pricedItineraryObjectArrayList.clear();
                for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                    if (flightBean.getImage().equalsIgnoreCase(tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFsDataBeanArrayList().get(0).getMal())) {
                        OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                        if (bean.getRefNum().equalsIgnoreCase("2")) {
                            int departureTimeVal = Utils.dateFormatToMinutes(bean.getFsDataBeanArrayList().get(0).getDdt());
                            if (departureTimeVal == 2) {
                                pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            }
                        }
                    }
                }
            } else if (fastestFlights) {
                pricedItineraryObjectArrayList.clear();
                for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                    if (flightBean.getImage().equalsIgnoreCase(tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFsDataBeanArrayList().get(0).getMal())) {
                        OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                        if (bean.getRefNum().equalsIgnoreCase("2")) {
                            int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                            if (stopType == 0) {
                                pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                            }
                        }
                    }
                }
                if (pricedItineraryObjectArrayList.size() == 0) {
                    for (int i = 0; i < tempPricedItineraryArray.size(); i++) {
                        if (flightBean.getImage().equalsIgnoreCase(tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFsDataBeanArrayList().get(0).getMal())) {
                            OriginDestinationOptionBean bean = tempPricedItineraryArray.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                            if (bean.getRefNum().equalsIgnoreCase("2")) {
                                int stopType = Integer.parseInt(bean.getFsDataBeanArrayList().get(0).getSq());
                                if (stopType == 1) {
                                    pricedItineraryObjectArrayList.add(tempPricedItineraryArray.get(i));
                                }
                            }
                        }
                    }
                }
            }
        }
        mAdapter.notifyDataSetChanged();
        roundTripAdapterList.notifyDataSetChanged();
    }

    private void handleFilterResults() {
        if (isFilterApplied) {
            Singleton.getInstance().refundStatusArrayList.clear();
            Singleton.getInstance().resultStopsArrayList.clear();
            Singleton.getInstance().returnResultAirlinesArrayList.clear();
            Singleton.getInstance().resultAirportsArrayList.clear();
            Singleton.getInstance().firstAirportsArrayList.clear();
            Singleton.getInstance().secondAirportsArrayList.clear();
            Singleton.getInstance().thirdAirportsArrayList.clear();
            Singleton.getInstance().resultTimeArrayList.clear();
            Singleton.getInstance().departureTimeArrayList.clear();
            Singleton.getInstance().arrivalTimeArrayList.clear();
            minValue = null;
            maxValue = null;
            isFilterApplied = false;
            isNonStopChecked = false;
            selectedMal = "";
            for (FlightBean bean : flightsArrayList) {
                bean.setClicked(false);
                bean.setDataCleared(false);
            }
        }
    }

    private void handleHorizontalAirlineResults() {
        boolean isAirlineNoneSelected = true;
        ArrayList<String> selectedAirlineList = new ArrayList<>();
        for (FlightBean airlineVar : flightsArrayList) {
            if (airlineVar.isClicked()) {
                isAirlineNoneSelected = false;
                selectedAirlineList.add(airlineVar.getImage());
            }
        }
        if (!isAirlineNoneSelected) {
            for (FlightBean selectedAirlineVar : flightsArrayList) {
                if (!selectedAirlineVar.isClicked()) {
                    for (int i = pricedItineraryObjectArrayList.size() - 1; i >= 0; i--) {
                        ArrayList<String> airlineList = new ArrayList<>();
                        OriginDestinationOptionBean bean = pricedItineraryObjectArrayList.get(i).getAiBean().getOriginDestinationOptionBeanArrayList().get(0);
                        if (bean.getRefNum().equalsIgnoreCase("2")) {
                            airlineList.add(bean.getFsDataBeanArrayList().get(0).getMal());
                        }
                        if (checkAirlineList(selectedAirlineList, airlineList).isEmpty()) {
                            pricedItineraryObjectArrayList.remove(i);
                        }
                    }
                }
            }
        }
    }

    private class AsyncTaskJson extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            merchandiseDataUI(mainMarchArrayList);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    private void sortByPrice() {
        if (tempPricedItineraryArray.size() > 0) {
            Collections.sort(tempPricedItineraryArray, new Comparator<PricedItineraryObject>() {
                @Override
                public int compare(PricedItineraryObject e1, PricedItineraryObject e2) {
                    int resultVal = 0;
                    String id1 = e1.getAipiBean().getItfObject().getPriceDiff();
                    String id2 = e2.getAipiBean().getItfObject().getPriceDiff();
                    Double price1 = 0.0;
                    Double price2 = 0.0;
                    try {
                        price1 = Double.parseDouble(id1);
                        price2 = Double.parseDouble(id2);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    int result = price1.compareTo(price2);
                    if (result > 0) {
                        resultVal = result;
                    } else if (result < 0) {
                        resultVal = result;
                    } else {
                        Long firstDur = Long.valueOf(e1.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getSegmentDepartureTime());
                        Long secondDur = Long.valueOf(e2.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getSegmentDepartureTime());
                        int durResult = firstDur.compareTo(secondDur);
                        if (durResult > 0) {
                            resultVal = durResult;
                        } else if (durResult < 0) {
                            resultVal = durResult;
                        } else {
                            Long firstFlight = e1.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFlightDuration();
                            Long secondFlight = e2.getAiBean().getOriginDestinationOptionBeanArrayList().get(0).getFlightDuration();
                            resultVal = firstFlight.compareTo(secondFlight);
                        }
                    }
                    return resultVal;
                }
            });
        }
    }

    private void getTokenFromServer() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device", Constants.DEVICE_TYPE);
            obj.put("clientId", Constants.CLIENT_ID);
            obj.put("clientSecret", Constants.CLIENT_SECRET);
            obj.put("grantType", Constants.GRANT_TYPE);
            obj.put("isocountry", Utils.getUserCountry(FlightReturnResultsActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPAsync async = new HTTPAsync(FlightReturnResultsActivity.this, FlightReturnResultsActivity.this,
                Constants.CLIENT_AUTHENTICATION, "", obj.toString(), GET_ACCESS_TOKEN, HTTPAsync.METHOD_POST);
        async.execute();
    }
}
