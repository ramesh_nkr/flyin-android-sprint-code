package com.flyin.bookings;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.model.Singleton;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.RandomKeyGeneration;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

@SuppressWarnings("ALL")
public class FlightSearchFragment extends Fragment {
    private static final String TAG = "FlightSearchFragment";
    private RelativeLayout economyLayout, businessLayout, firstClassLayout, multiWayEconomyLayout, multiWayBusinessLayout, multiWayFirstClassLayout, travellersPickerLayout, multiTravellerPickerLayout, multiInfantViewLayout, loadingViewLayout;
    private LinearLayout roundOneWayLinearLayout, multiLinearLayout, travellersList, slideHeaderLayout;
    private RelativeLayout multiWayFirstJourneyDateLayout, multiWaySecondJourneyDateLayout, multiWayThirdJourneyDateLayout;
    private LinearLayout roundtripbar, onewaybar, multicitybar;
    private LinearLayout departureAirport, arrivalAirport, firstDepartureAirport, firstArrivalAirport, secondDepartureAirport, secondArrivalAirport, thirdDepartureAirport, thirdArrivalAirport;
    private TextView roundJourneyText, singleJourneyText, multiJourneyText, forwardJourneySource, forwardJourneySourceAll, returnJourneyDestination, returnJourneyDestinationAll, economyText, businessText, firstClassText, multiWayFirstJourneyStart, multiWayFirstJourneySource, multiWayFirstJourneySourceAll, multiWayFirstJourneyEnd, multiWayFirstJourneyDestination, multiWayFirstJourneyDestinationAll, /*,multiWaySecondJourneyStart, */
            multiWaySecondJourneySource, multiWaySecondJourneySourceAll, multiWaySecondJourneyEnd, multiWaySecondJourneyDestination, multiWaySecondJourneyDestinationAll, multiWaySecondJourneyDeparture, multiWayThirdJourneySource, multiWayThirdJourneySourceAll, multiWayThirdJourneyDestination, multiWayThirdJourneyDestinationAll, multiWayThirdJourneyDeparture, multiWayEconomyText, multiWayBusinessText, multiWayFirstClassText, travellersPickerText, multiTravellersPickerText, travellerAdultCount, travellerAdultText, travellerChildCount, travellerChildText, travellerInfantCount, travellerInfantText, mTitleTextView, multiTravellerAdultCount, multiTravellerAdultText, multiTravellerChildCount, multiTravellerChildText, multiTravellerInfantCount, multiTravellerInfantText, errorText, errorDescriptionText, minuse_symbol_text;
    private ImageView economyImage, businessImage, firstClassImage, multiWayEconomyImage, multiWayBusinessImage, multiWayFirstClassImage, roundTripSwitchImage, adultPickerIncreaseImage, adultPickerReduceImage, childPickerIncreaseImage, childPickerReduceImage, infantPickerIncreaseImage, infantPickerReduceImage, multiAdultPickerIncreaseImage, multiAdultPickerReduceImage, multiChildPickerIncreaseImage, multiChildPickerReduceImage, multiInfantPickerIncreaseImage, multiInfantPickerReduceImage, multiFirstFlightArrow, multiSecondFlightArrow, multiThirdFlightArrow;
    private Button searchFlightButton, travellerPickerDoneButton, multiTravellerPickerDoneButton;
    private Typeface tf, titleFace, textFace, textBold;
    private static final int SEARCH_FROM_AIRPORT = 3;
    private static final int SEARCH_TO_AIRPORT = 4;
    private static final int SEARCH_FROM_FIRST_AIRPORT = 5;
    private static final int SEARCH_TO_FIRST_AIRPORT = 6;
    private static final int SEARCH_FROM_SECOND_AIRPORT = 7;
    private static final int SEARCH_TO_SECOND_AIRPORT = 8;
    private static final int SEARCH_FROM_THIRD_AIRPORT = 9;
    private static final int SEARCH_TO_THIRD_AIRPORT = 10;
    private int adultCount = 1;
    private int childCount = 0;
    private int infantCount = 0;
    private int defaultMTAdultCount = 1;
    private int defaultMTChildCount = 0;
    private int defaultMTInfantCount = 0;
    private String randomNumber = "";
    private String forwardFlightDate = "", returnFlightDate = "", firstFlightDate = "", secondFlightDate = "", thirdFlightDate = "";
    private String cabinPref = "Economy";
    private String multiCabinPref = "Economy";
    private String tripType = "2";
    private String singleTrip = "1";
    private String roundTrip = "2";
    private String multiTrip = "3";
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    private MyReceiver myReceiver;
    private IntentFilter intentFilter;
    private boolean isArabicLang = false;
    private final static int SINGLE_TRIP = 14;
    private final static int ROUND_TRIP = 15;
    private final static int MULTI_TRIP = 16;
    private int dateSelectedSegment = 1;
    public static int REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    private LinearLayout roundTripLayout, oneWayLayout, multiWayLayout;
    private TextView selectdate_title_text, round_departing_text, round_return_text;
    private RelativeLayout journey_roundTrip_layout;
    private RelativeLayout travellerDetailsLayout;
    private TextView travellerstitle;
    private TextView travellerListAdultCount, travellerListAdultText, travellerListChildCount, travellerListChildText, travellerListInfantCount, travellerListInfantText, multiWayTravellerListAdultCount, multiWayTravellerListAdultText, multiWayTravellerListChildCount, multiWayTravellerListChildText, multiWayTravellerListInfantCount, multiWayTravellerListInfantText;
    private CheckBox stopflightcheckbox;
    private boolean isNonStopChecked = false;
    private CheckBox multiwayStopflightcheckbox;
    private boolean multiWayNonStopChecked = false;
    private TextView first_flightdate_text, second_flightdate_text, third_flightdate_text;
    private TextView first_selectdate_text, second_selectdat_text, third_selectdate_text;
    private TextView mutiway_travellerstitle;
    private RelativeLayout multiWayTravellerList, hearderLayout;
    private Activity mActivity = null;
    private ImageView flightViewBgImage = null;

    public FlightSearchFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_flight_search, container, false);
        textfont_init();
        roundtrip_select_bar_init(view);
        selectdatebar_init(view);
        roudntrip_travellerslayout_init(view);
        stopflightlayout_init(view);
        multitrip_datebars_init(view);
        muititriptrip_travellerslayout_init(view);
        flyinimageview_init(view);
        hearderLayout = (RelativeLayout) view.findViewById(R.id.bakground_image);
        economyLayout = (RelativeLayout) view.findViewById(R.id.economy_layout);
        businessLayout = (RelativeLayout) view.findViewById(R.id.business_layout);
        firstClassLayout = (RelativeLayout) view.findViewById(R.id.firstClass_layout);
        multiLinearLayout = (LinearLayout) view.findViewById(R.id.flight_multiWay_details);
        multiWayEconomyLayout = (RelativeLayout) view.findViewById(R.id.multiWay_economy_layout);
        multiWayBusinessLayout = (RelativeLayout) view.findViewById(R.id.multiWay_business_layout);
        multiWayFirstClassLayout = (RelativeLayout) view.findViewById(R.id.multiWay_firstClass_layout);
        multiWayEconomyImage = (ImageView) view.findViewById(R.id.multiWay_traveller_economy_image);
        multiWayBusinessImage = (ImageView) view.findViewById(R.id.multiWay_traveller_business_image);
        multiWayFirstClassImage = (ImageView) view.findViewById(R.id.multiWay_traveller_firstClass_image);
        roundOneWayLinearLayout = (LinearLayout) view.findViewById(R.id.flight_round_oneway_details);
        economyImage = (ImageView) view.findViewById(R.id.traveller_economy_image);
        businessImage = (ImageView) view.findViewById(R.id.traveller_business_image);
        firstClassImage = (ImageView) view.findViewById(R.id.traveller_firstClass_image);
        roundTripSwitchImage = (ImageView) view.findViewById(R.id.round_trip_image);
        adultPickerIncreaseImage = (ImageView) view.findViewById(R.id.adult_increase_image);
        adultPickerReduceImage = (ImageView) view.findViewById(R.id.adult_reduce_image);
        childPickerIncreaseImage = (ImageView) view.findViewById(R.id.child_increase_image);
        childPickerReduceImage = (ImageView) view.findViewById(R.id.child_reduce_image);
        infantPickerIncreaseImage = (ImageView) view.findViewById(R.id.infant_increase_image);
        infantPickerReduceImage = (ImageView) view.findViewById(R.id.infant_reduce_image);
        multiAdultPickerIncreaseImage = (ImageView) view.findViewById(R.id.multi_adult_increase_image);
        multiAdultPickerReduceImage = (ImageView) view.findViewById(R.id.multi_adult_reduce_image);
        multiChildPickerIncreaseImage = (ImageView) view.findViewById(R.id.multi_child_increase_image);
        multiChildPickerReduceImage = (ImageView) view.findViewById(R.id.multi_child_reduce_image);
        multiInfantPickerIncreaseImage = (ImageView) view.findViewById(R.id.multi_infant_increase_image);
        multiInfantPickerReduceImage = (ImageView) view.findViewById(R.id.multi_infant_reduce_image);
        multiWayFirstJourneyDateLayout = (RelativeLayout) view.findViewById(R.id.multiWay_journey_firstTrip_date);
        multiWaySecondJourneyDateLayout = (RelativeLayout) view.findViewById(R.id.multiWay_second_journey_date_layout);
        multiWayThirdJourneyDateLayout = (RelativeLayout) view.findViewById(R.id.multiWay_third_journey_date_layout);
        travellersPickerLayout = (RelativeLayout) view.findViewById(R.id.travellers_picker_layout);
        multiTravellerPickerLayout = (RelativeLayout) view.findViewById(R.id.multi_travellers_picker_layout);
        multiFirstFlightArrow = (ImageView) view.findViewById(R.id.first_flight_arrow);
        multiSecondFlightArrow = (ImageView) view.findViewById(R.id.second_flight_arrow);
        multiThirdFlightArrow = (ImageView) view.findViewById(R.id.third_flight_arrow);
        departureAirport = (LinearLayout) view.findViewById(R.id.journey_from);
        arrivalAirport = (LinearLayout) view.findViewById(R.id.journey_to);
        firstDepartureAirport = (LinearLayout) view.findViewById(R.id.multiWay_first_journey_from);
        firstArrivalAirport = (LinearLayout) view.findViewById(R.id.multiWay_first_journey_to);
        secondDepartureAirport = (LinearLayout) view.findViewById(R.id.multiWay_second_journey_from);
        secondArrivalAirport = (LinearLayout) view.findViewById(R.id.multiWay_second_journey_to);
        thirdDepartureAirport = (LinearLayout) view.findViewById(R.id.multiWay_third_journey_from);
        thirdArrivalAirport = (LinearLayout) view.findViewById(R.id.multiWay_third_journey_to);
        loadingViewLayout = (RelativeLayout) view.findViewById(R.id.loading_view_layout);
        forwardJourneySource = (TextView) view.findViewById(R.id.journey_source);
        forwardJourneySourceAll = (TextView) view.findViewById(R.id.journey_source_all);
        returnJourneyDestination = (TextView) view.findViewById(R.id.journey_destination);
        returnJourneyDestinationAll = (TextView) view.findViewById(R.id.journey_destination_all);
        economyText = (TextView) view.findViewById(R.id.traveller_economy_text);
        businessText = (TextView) view.findViewById(R.id.traveller_business_text);
        firstClassText = (TextView) view.findViewById(R.id.traveller_firstClass_text);
        multiWayFirstJourneySource = (TextView) view.findViewById(R.id.multiWay_first_journey_source);
        multiWayFirstJourneySourceAll = (TextView) view.findViewById(R.id.multiWay_first_journey_source_all);
        multiWayFirstJourneyDestination = (TextView) view.findViewById(R.id.multiWay_first_journey_destination);
        multiWayFirstJourneyDestinationAll = (TextView) view.findViewById(R.id.multiWay_first_journey_destination_all);
        multiWaySecondJourneySource = (TextView) view.findViewById(R.id.multiWay_second_journey_source);
        multiWaySecondJourneySourceAll = (TextView) view.findViewById(R.id.multiWay_second_journey_source_all);
        multiWaySecondJourneyDestination = (TextView) view.findViewById(R.id.multiWay_second_journey_destination);
        multiWaySecondJourneyDestinationAll = (TextView) view.findViewById(R.id.multiWay_second_journey_destination_all);
        multiWayThirdJourneySource = (TextView) view.findViewById(R.id.multiWay_third_journey_source);
        multiWayThirdJourneySourceAll = (TextView) view.findViewById(R.id.multiWay_third_journey_source_all);
        multiWayThirdJourneyDestination = (TextView) view.findViewById(R.id.multiWay_third_journey_destination);
        multiWayThirdJourneyDestinationAll = (TextView) view.findViewById(R.id.multiWay_third_journey_destination_all);
        multiWayEconomyText = (TextView) view.findViewById(R.id.multiWay_traveller_economy_text);
        multiWayBusinessText = (TextView) view.findViewById(R.id.multiWay_traveller_business_text);
        multiWayFirstClassText = (TextView) view.findViewById(R.id.multiWay_traveller_firstClass_text);
        travellersPickerText = (TextView) view.findViewById(R.id.pick_travellers_header);
        multiTravellersPickerText = (TextView) view.findViewById(R.id.multi_pick_travellers_header);
        travellerAdultCount = (TextView) view.findViewById(R.id.traveller_adult_count);
        travellerAdultText = (TextView) view.findViewById(R.id.traveller_adult_text);
        travellerChildCount = (TextView) view.findViewById(R.id.traveller_child_count);
        travellerChildText = (TextView) view.findViewById(R.id.traveller_child_text);
        travellerInfantCount = (TextView) view.findViewById(R.id.traveller_infant_count);
        travellerInfantText = (TextView) view.findViewById(R.id.traveller_infant_text);
        travellerListAdultCount = (TextView) view.findViewById(R.id.travellers_list_adult_count);
        travellerListAdultText = (TextView) view.findViewById(R.id.travellers_list_adult_text);
        travellerListChildCount = (TextView) view.findViewById(R.id.travellers_list_child_count);
        travellerListChildText = (TextView) view.findViewById(R.id.travellers_list_child_text);
        travellerListInfantCount = (TextView) view.findViewById(R.id.travellers_list_infant_count);
        travellerListInfantText = (TextView) view.findViewById(R.id.travellers_list_infant_text);
        multiTravellerAdultCount = (TextView) view.findViewById(R.id.multi_traveller_adult_count);
        multiTravellerAdultText = (TextView) view.findViewById(R.id.multi_traveller_adult_text);
        multiTravellerChildCount = (TextView) view.findViewById(R.id.multi_traveller_child_count);
        multiTravellerChildText = (TextView) view.findViewById(R.id.multi_traveller_child_text);
        multiTravellerInfantCount = (TextView) view.findViewById(R.id.multi_traveller_infant_count);
        multiTravellerInfantText = (TextView) view.findViewById(R.id.multi_traveller_infant_text);
        searchFlightButton = (Button) view.findViewById(R.id.search_flights);
        travellerPickerDoneButton = (Button) view.findViewById(R.id.pick_travellers_submit_button);
        multiTravellerPickerDoneButton = (Button) view.findViewById(R.id.multi_pick_travellers_submit_button);
        multiWayTravellerListAdultCount = (TextView) view.findViewById(R.id.multiWay_travellers_list_adult_count);
        multiWayTravellerListAdultText = (TextView) view.findViewById(R.id.multiWay_travellers_list_adult_text);
        multiWayTravellerListChildCount = (TextView) view.findViewById(R.id.multiWay_travellers_list_child_count);
        multiWayTravellerListChildText = (TextView) view.findViewById(R.id.multiWay_travellers_list_child_text);
        multiWayTravellerListInfantCount = (TextView) view.findViewById(R.id.multiWay_travellers_list_infant_count);
        multiWayTravellerListInfantText = (TextView) view.findViewById(R.id.multiWay_travellers_list_infant_text);
        slideHeaderLayout = (LinearLayout) view.findViewById(R.id.slide_header_layout);
        flightViewBgImage = (ImageView) view.findViewById(R.id.flight_view_bg_image);
        forwardJourneySource.setTypeface(textBold);
        forwardJourneySourceAll.setTypeface(titleFace);
        returnJourneyDestination.setTypeface(textBold);
        returnJourneyDestinationAll.setTypeface(titleFace);
        economyText.setTypeface(titleFace);
        businessText.setTypeface(titleFace);
        firstClassText.setTypeface(titleFace);
        travellerAdultCount.setTypeface(titleFace);
        travellerAdultText.setTypeface(titleFace);
        travellerChildCount.setTypeface(titleFace);
        travellerChildText.setTypeface(titleFace);
        travellerInfantCount.setTypeface(titleFace);
        travellerInfantText.setTypeface(titleFace);
        multiWayFirstJourneySource.setTypeface(textBold);
        multiWayFirstJourneySourceAll.setTypeface(titleFace);
        multiWayFirstJourneyDestination.setTypeface(textBold);
        multiWayFirstJourneyDestinationAll.setTypeface(titleFace);
        multiWaySecondJourneySource.setTypeface(textBold);
        multiWaySecondJourneySourceAll.setTypeface(titleFace);
        multiWaySecondJourneyDestination.setTypeface(textBold);
        multiWaySecondJourneyDestinationAll.setTypeface(titleFace);
        multiWayThirdJourneySource.setTypeface(textBold);
        multiWayThirdJourneySourceAll.setTypeface(titleFace);
        multiWayThirdJourneyDestination.setTypeface(textBold);
        multiWayThirdJourneyDestinationAll.setTypeface(titleFace);
        multiWayEconomyText.setTypeface(titleFace);
        multiWayBusinessText.setTypeface(titleFace);
        multiWayFirstClassText.setTypeface(titleFace);
        travellersPickerText.setTypeface(tf);
        multiTravellersPickerText.setTypeface(tf);
        multiTravellerAdultCount.setTypeface(titleFace);
        multiTravellerAdultText.setTypeface(titleFace);
        multiTravellerChildCount.setTypeface(titleFace);
        multiTravellerChildText.setTypeface(titleFace);
        multiTravellerInfantCount.setTypeface(titleFace);
        multiTravellerInfantText.setTypeface(titleFace);
        searchFlightButton.setTypeface(tf);
        travellerPickerDoneButton.setTypeface(tf);
        multiTravellerPickerDoneButton.setTypeface(tf);
        travellerListAdultCount.setTypeface(titleFace);
        travellerListAdultText.setTypeface(titleFace);
        travellerListChildCount.setTypeface(titleFace);
        travellerListChildText.setTypeface(titleFace);
        travellerListInfantCount.setTypeface(titleFace);
        travellerListInfantText.setTypeface(titleFace);
        multiWayTravellerListAdultCount.setTypeface(titleFace);
        multiWayTravellerListAdultText.setTypeface(titleFace);
        multiWayTravellerListChildCount.setTypeface(titleFace);
        multiWayTravellerListChildText.setTypeface(titleFace);
        multiWayTravellerListInfantCount.setTypeface(titleFace);
        multiWayTravellerListInfantText.setTypeface(titleFace);
        if (isArabicLang) {
            forwardJourneySource.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.userName_text));
            forwardJourneySourceAll.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            returnJourneyDestination.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.userName_text));
            returnJourneyDestinationAll.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            economyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            businessText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            firstClassText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayFirstJourneySource.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.userName_text));
            multiWayFirstJourneySourceAll.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayFirstJourneyDestination.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.userName_text));
            multiWayFirstJourneyDestinationAll.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWaySecondJourneySource.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.userName_text));
            multiWaySecondJourneySourceAll.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWaySecondJourneyDestination.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.userName_text));
            multiWaySecondJourneyDestinationAll.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayThirdJourneySource.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.userName_text));
            multiWayThirdJourneySourceAll.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayThirdJourneyDestination.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.userName_text));
            multiWayThirdJourneyDestinationAll.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayEconomyText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayBusinessText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayFirstClassText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            searchFlightButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.contact_text));
            travellersPickerText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiTravellersPickerText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            travellerPickerDoneButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiTravellerPickerDoneButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            travellerAdultCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            travellerAdultText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            travellerChildCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            travellerChildText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            travellerInfantCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            travellerInfantText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            multiTravellerAdultCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            multiTravellerAdultText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            multiTravellerChildCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            multiTravellerChildText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            multiTravellerInfantCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            multiTravellerInfantText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.hotel_city_text_size));
            travellerListAdultCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            travellerListAdultText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            travellerListChildCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            travellerListChildText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            travellerListInfantCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            travellerListInfantText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayTravellerListAdultCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayTravellerListAdultText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayTravellerListChildCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayTravellerListChildText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayTravellerListInfantCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiWayTravellerListInfantText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            param.setMargins(0, -5, 0, 0);
            forwardJourneySourceAll.setLayoutParams(param);
            returnJourneyDestinationAll.setLayoutParams(param);
            multiWayFirstJourneySourceAll.setLayoutParams(param);
            multiWayFirstJourneyDestinationAll.setLayoutParams(param);
            multiWaySecondJourneySourceAll.setLayoutParams(param);
            multiWaySecondJourneyDestinationAll.setLayoutParams(param);
            multiWayThirdJourneySourceAll.setLayoutParams(param);
            multiWayThirdJourneyDestinationAll.setLayoutParams(param);
            roundTripSwitchImage.setImageResource(R.drawable.newfromto_arabic);
            multiFirstFlightArrow.setImageResource(R.drawable.newfromto_arabic);
            multiSecondFlightArrow.setImageResource(R.drawable.newfromto_arabic);
            multiThirdFlightArrow.setImageResource(R.drawable.newfromto_arabic);
        }
        stopflightcheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isNonStopChecked) {
                    isNonStopChecked = true;
                } else {
                    isNonStopChecked = false;
                }
            }
        });
        multiwayStopflightcheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!multiWayNonStopChecked) {
                    multiWayNonStopChecked = true;
                } else {
                    multiWayNonStopChecked = false;
                }
            }
        });
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        departureAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchAirportActivity.class);
                mActivity.startActivityForResult(intent, SEARCH_FROM_AIRPORT);
            }
        });
        arrivalAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchAirportActivity.class);
                mActivity.startActivityForResult(intent, SEARCH_TO_AIRPORT);
            }
        });
        roundTripSwitchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!forwardJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                    String fromAirportCode = forwardJourneySource.getText().toString();
                    String fromAirportName = forwardJourneySourceAll.getText().toString();
                    forwardJourneySource.setText(returnJourneyDestination.getText().toString());
                    forwardJourneySourceAll.setText(returnJourneyDestinationAll.getText().toString());
                    returnJourneyDestination.setText(fromAirportCode);
                    returnJourneyDestinationAll.setText(fromAirportName);
                    roundTripSwitchImage.setRotation(180);
                }
            }
        });
        firstDepartureAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchAirportActivity.class);
                mActivity.startActivityForResult(intent, SEARCH_FROM_FIRST_AIRPORT);
            }
        });
        firstArrivalAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchAirportActivity.class);
                mActivity.startActivityForResult(intent, SEARCH_TO_FIRST_AIRPORT);
            }
        });
        multiFirstFlightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!multiWayFirstJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                    String fromAirportCode = multiWayFirstJourneySource.getText().toString();
                    String fromAirportName = multiWayFirstJourneySourceAll.getText().toString();
                    multiWayFirstJourneySource.setText(multiWayFirstJourneyDestination.getText().toString());
                    multiWayFirstJourneySourceAll.setText(multiWayFirstJourneyDestinationAll.getText().toString());
                    multiWayFirstJourneyDestination.setText(fromAirportCode);
                    multiWayFirstJourneyDestinationAll.setText(fromAirportName);
                }
            }
        });
        secondDepartureAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchAirportActivity.class);
                mActivity.startActivityForResult(intent, SEARCH_FROM_SECOND_AIRPORT);
            }
        });
        secondArrivalAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchAirportActivity.class);
                mActivity.startActivityForResult(intent, SEARCH_TO_SECOND_AIRPORT);
            }
        });
        multiSecondFlightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!multiWaySecondJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                    String fromAirportCode = multiWaySecondJourneySource.getText().toString();
                    String fromAirportName = multiWaySecondJourneySourceAll.getText().toString();
                    multiWaySecondJourneySource.setText(multiWaySecondJourneyDestination.getText().toString());
                    multiWaySecondJourneySourceAll.setText(multiWaySecondJourneyDestinationAll.getText().toString());
                    multiWaySecondJourneyDestination.setText(fromAirportCode);
                    multiWaySecondJourneyDestinationAll.setText(fromAirportName);
                }
            }
        });
        thirdDepartureAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchAirportActivity.class);
                mActivity.startActivityForResult(intent, SEARCH_FROM_THIRD_AIRPORT);
            }
        });
        thirdArrivalAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchAirportActivity.class);
                mActivity.startActivityForResult(intent, SEARCH_TO_THIRD_AIRPORT);
            }
        });
        multiThirdFlightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!multiWayThirdJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                    String fromAirportCode = multiWayThirdJourneySource.getText().toString();
                    String fromAirportName = multiWayThirdJourneySourceAll.getText().toString();
                    multiWayThirdJourneySource.setText(multiWayThirdJourneyDestination.getText().toString());
                    multiWayThirdJourneySourceAll.setText(multiWayThirdJourneyDestinationAll.getText().toString());
                    multiWayThirdJourneyDestination.setText(fromAirportCode);
                    multiWayThirdJourneyDestinationAll.setText(fromAirportName);
                }
            }
        });
        economyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travellersPickerLayout.getVisibility() != View.VISIBLE) {
                    cabinPref = "Economy";
                    economyImage.setImageResource(R.drawable.economy_selected);
                    businessImage.setImageResource(R.drawable.business);
                    firstClassImage.setImageResource(R.drawable.firstclass);
                    economyText.setTextColor(Utils.getColor(getActivity(), R.color.user_profile_email_color));
                    businessText.setTextColor(Utils.getColor(getActivity(), R.color.terms_text));
                    firstClassText.setTextColor(Utils.getColor(getActivity(), R.color.terms_text));
                    economyLayout.setBackgroundResource(R.drawable.roundlayoutselected);
                    businessLayout.setBackgroundResource(R.drawable.roundlayout);
                    firstClassLayout.setBackgroundResource(R.drawable.roundlayout);
                }
            }
        });
        businessLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travellersPickerLayout.getVisibility() != View.VISIBLE) {
                    cabinPref = "Business";
                    economyImage.setImageResource(R.drawable.economy);
                    businessImage.setImageResource(R.drawable.business_selected);
                    firstClassImage.setImageResource(R.drawable.firstclass);
                    economyText.setTextColor(Utils.getColor(getActivity(), R.color.terms_text));
                    businessText.setTextColor(Utils.getColor(getActivity(), R.color.user_profile_email_color));
                    firstClassText.setTextColor(Utils.getColor(getActivity(), R.color.terms_text));
                    economyLayout.setBackgroundResource(R.drawable.roundlayout);
                    businessLayout.setBackgroundResource(R.drawable.roundlayoutselected);
                    firstClassLayout.setBackgroundResource(R.drawable.roundlayout);
                }
            }
        });
        firstClassLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travellersPickerLayout.getVisibility() != View.VISIBLE) {
                    cabinPref = "First";
                    economyImage.setImageResource(R.drawable.economy);
                    businessImage.setImageResource(R.drawable.business);
                    firstClassImage.setImageResource(R.drawable.firstclass_selected);
                    economyText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                    businessText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                    firstClassText.setTextColor(Utils.getColor(getActivity(), R.color.user_profile_email_color));
                    economyLayout.setBackgroundResource(R.drawable.roundlayout);
                    businessLayout.setBackgroundResource(R.drawable.roundlayout);
                    firstClassLayout.setBackgroundResource(R.drawable.roundlayoutselected);
                }
            }
        });
        multiWayEconomyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (multiTravellerPickerLayout.getVisibility() != View.VISIBLE) {
                    multiCabinPref = "Economy";
                    multiWayEconomyImage.setImageResource(R.drawable.economy_selected);
                    multiWayBusinessImage.setImageResource(R.drawable.business);
                    multiWayFirstClassImage.setImageResource(R.drawable.firstclass);
                    multiWayEconomyText.setTextColor(Utils.getColor(getActivity(), R.color.user_profile_email_color));
                    multiWayBusinessText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                    multiWayFirstClassText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                    multiWayEconomyLayout.setBackgroundResource(R.drawable.roundlayoutselected);
                    multiWayBusinessLayout.setBackgroundResource(R.drawable.roundlayout);
                    multiWayFirstClassLayout.setBackgroundResource(R.drawable.roundlayout);
                }
            }
        });
        multiWayBusinessLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (multiTravellerPickerLayout.getVisibility() != View.VISIBLE) {
                    multiCabinPref = "Business";
                    multiWayEconomyImage.setImageResource(R.drawable.economy);
                    multiWayBusinessImage.setImageResource(R.drawable.business_selected);
                    multiWayFirstClassImage.setImageResource(R.drawable.firstclass);
                    multiWayEconomyText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                    multiWayBusinessText.setTextColor(Utils.getColor(getActivity(), R.color.user_profile_email_color));
                    multiWayFirstClassText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                    multiWayEconomyLayout.setBackgroundResource(R.drawable.roundlayout);
                    multiWayBusinessLayout.setBackgroundResource(R.drawable.roundlayoutselected);
                    multiWayFirstClassLayout.setBackgroundResource(R.drawable.roundlayout);
                }
            }
        });
        multiWayFirstClassLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (multiTravellerPickerLayout.getVisibility() != View.VISIBLE) {
                    multiCabinPref = "First";
                    multiWayEconomyImage.setImageResource(R.drawable.economy);
                    multiWayBusinessImage.setImageResource(R.drawable.business);
                    multiWayFirstClassImage.setImageResource(R.drawable.firstclass_selected);
                    multiWayEconomyText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                    multiWayBusinessText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                    multiWayFirstClassText.setTextColor(Utils.getColor(getActivity(), R.color.user_profile_email_color));
                    multiWayEconomyLayout.setBackgroundResource(R.drawable.roundlayout);
                    multiWayBusinessLayout.setBackgroundResource(R.drawable.roundlayout);
                    multiWayFirstClassLayout.setBackgroundResource(R.drawable.roundlayoutselected);
                }
            }
        });
        adultPickerIncreaseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAdultIncrement();
            }
        });
        multiAdultPickerIncreaseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAdultIncrement();
            }
        });
        adultPickerReduceImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAdultDecrement();
            }
        });
        multiAdultPickerReduceImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAdultDecrement();
            }
        });
        childPickerIncreaseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChildrenIncrement();
            }
        });
        multiChildPickerIncreaseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChildrenIncrement();
            }
        });
        childPickerReduceImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChildrenDecrement();
            }
        });
        multiChildPickerReduceImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChildrenDecrement();
            }
        });
        infantPickerIncreaseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onInfantIncrement();
            }
        });
        multiInfantPickerIncreaseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onInfantIncrement();
            }
        });
        infantPickerReduceImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onInfantDecrement();
            }
        });
        multiInfantPickerReduceImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onInfantDecrement();
            }
        });
        travellerPickerDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleDoneTraveller();
            }
        });
        multiTravellerPickerDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleDoneTraveller();
            }
        });
        refreshBackground();
        searchFlightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String bookingIdRqJson = handleBookingRQRequest();
//                SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
//                Intent intent = new Intent(getActivity(), TripConfirmationActivity.class);
//                intent.putExtra(Constants.JSON_REQUEST, bookingIdRqJson);
//                intent.putExtra(Constants.USER_EMAIL, pref.getString(Constants.MEMBER_EMAIL, ""));
//                intent.putExtra(Constants.USER_FULL_NAME, "Raghavendra Dabbidi");
//                intent.putExtra(Constants.USER_NAME, "Raghavendra Dabbidi");
//                intent.putExtra(Constants.INFO_SOURCE, "0");
//                startActivity(intent);
                if (travellersPickerLayout.getVisibility() != View.VISIBLE || multiTravellerPickerLayout.getVisibility() != View.VISIBLE) {
                    if (tripType.equalsIgnoreCase(singleTrip)) {
                        if (forwardJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                            displayErrorMessage(getString(R.string.label_err_dept_msg));
                            return;
                        }
                        if (returnJourneyDestination.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                            displayErrorMessage(getString(R.string.label_err_arr_msg));
                            return;
                        }
                        if (forwardJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                            displayErrorMessage(getString(R.string.label_err_dept_msg));
                            return;
                        }
                        if (forwardJourneySource.getText().toString().equalsIgnoreCase(returnJourneyDestination.getText().toString())) {
                            displayErrorMessage(getString(R.string.label_err_same_dept_arr_msg));
                            return;
                        }
                        if (round_departing_text.getText().toString().equalsIgnoreCase(getString(R.string.label_search_screen_flight_depart))) {
                            displayErrorMessage(getString(R.string.label_err_dept_date_msg));
                            return;
                        }
                    } else if (tripType.equalsIgnoreCase(roundTrip)) {
                        if (forwardJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                            displayErrorMessage(getString(R.string.label_err_dept_msg));
                            return;
                        }
                        if (returnJourneyDestination.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                            displayErrorMessage(getString(R.string.label_err_arr_msg));
                            return;
                        }
                        if (forwardJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                            displayErrorMessage(getString(R.string.label_err_dept_msg));
                            return;
                        }
                        if (forwardJourneySource.getText().toString().equalsIgnoreCase(returnJourneyDestination.getText().toString())) {
                            displayErrorMessage(getString(R.string.label_err_same_dept_arr_msg));
                            return;
                        }
                        if (round_departing_text.getText().toString().equalsIgnoreCase(getString(R.string.label_search_screen_flight_depart))) {
                            displayErrorMessage(getString(R.string.label_err_dept_date_msg));
                            return;
                        }
                        if (round_return_text.getText().toString().equalsIgnoreCase(getString(R.string.label_search_screen_flight_return))) {
                            displayErrorMessage(getString(R.string.label_err_arr_date_msg));
                            return;
                        }
                        if (Integer.parseInt(Utils.flightSearchDateDifference(returnFlightDate, forwardFlightDate)) < 0) {
                            displayErrorMessage(getString(R.string.label_err_date_msg));
                            return;
                        }
                    } else if (tripType.equalsIgnoreCase(multiTrip)) {
                        if (multiWayFirstJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                            displayErrorMessage(getString(R.string.label_err_dept_msg));
                            return;
                        }
                        if (multiWayFirstJourneyDestination.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                            displayErrorMessage(getString(R.string.label_err_arr_msg));
                            return;
                        }
                        if (multiWayFirstJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                            displayErrorMessage(getString(R.string.label_err_dept_msg));
                            return;
                        }
                        if (multiWayFirstJourneySource.getText().toString().equalsIgnoreCase(multiWayFirstJourneyDestination.getText().toString())) {
                            displayErrorMessage(getString(R.string.label_err_same_dept_arr_msg));
                            return;
                        }
                        if (first_flightdate_text.getText().toString().equalsIgnoreCase(getString(R.string.label_search_screen_flight_depart))) {
                            displayErrorMessage(getString(R.string.label_err_dept_date_msg));
                            return;
                        }
                        if (!multiWaySecondJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                            if (multiWaySecondJourneyDestination.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                                displayErrorMessage(getString(R.string.label_err_arr_msg));
                                return;
                            }
                            if (multiWaySecondJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                                displayErrorMessage(getString(R.string.label_err_dept_msg));
                                return;
                            }
                            if (multiWaySecondJourneySource.getText().toString().equalsIgnoreCase(multiWaySecondJourneyDestination.getText().toString())) {
                                displayErrorMessage(getString(R.string.label_err_same_dept_arr_msg));
                                return;
                            }
                            if (second_flightdate_text.getText().toString().equalsIgnoreCase(getString(R.string.label_search_screen_flight_depart))) {
                                displayErrorMessage(getString(R.string.label_err_dept_date_msg));
                                return;
                            }
                        }
                        if (!multiWaySecondJourneyDestination.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                            if (multiWaySecondJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                                displayErrorMessage(getString(R.string.label_err_dept_msg));
                                return;
                            }
                            if (multiWaySecondJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                                displayErrorMessage(getString(R.string.label_err_dept_msg));
                                return;
                            }
                            if (multiWaySecondJourneySource.getText().toString().equalsIgnoreCase(multiWaySecondJourneyDestination.getText().toString())) {
                                displayErrorMessage(getString(R.string.label_err_same_dept_arr_msg));
                                return;
                            }
                            if (second_flightdate_text.getText().toString().equalsIgnoreCase(getString(R.string.label_search_screen_flight_depart))) {
                                displayErrorMessage(getString(R.string.label_err_dept_date_msg));
                                return;
                            }
                        }
                        if (!multiWayThirdJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                            if (multiWayThirdJourneyDestination.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                                displayErrorMessage(getString(R.string.label_err_arr_msg));
                                return;
                            }
                            if (multiWayThirdJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                                displayErrorMessage(getString(R.string.label_err_dept_msg));
                                return;
                            }
                            if (multiWayThirdJourneySource.getText().toString().equalsIgnoreCase(multiWayThirdJourneyDestination.getText().toString())) {
                                displayErrorMessage(getString(R.string.label_err_same_dept_arr_msg));
                                return;
                            }
                            if (third_flightdate_text.getText().toString().equalsIgnoreCase(getString(R.string.label_search_screen_flight_depart))) {
                                displayErrorMessage(getString(R.string.label_err_dept_date_msg));
                                return;
                            }
                        }
                        if (!multiWayThirdJourneyDestination.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                            if (multiWayThirdJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                                displayErrorMessage(getString(R.string.label_err_dept_msg));
                                return;
                            }
                            if (multiWayThirdJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_to))) {
                                displayErrorMessage(getString(R.string.label_err_dept_msg));
                                return;
                            }
                            if (multiWayThirdJourneySource.getText().toString().equalsIgnoreCase(multiWayThirdJourneyDestination.getText().toString())) {
                                displayErrorMessage(getString(R.string.label_err_same_dept_arr_msg));
                                return;
                            }
                            if (third_flightdate_text.getText().toString().equalsIgnoreCase(getString(R.string.label_search_screen_flight_depart))) {
                                displayErrorMessage(getString(R.string.label_err_dept_date_msg));
                                return;
                            }
                        }
                    }
                    RandomKeyGeneration rndKeyGeneration = new RandomKeyGeneration();
                    randomNumber = rndKeyGeneration.generateRandomString();
                    String requestJSON = handleSearchFlights();
                    if (tripType.equalsIgnoreCase(singleTrip)) {
                        String adultText = "";
                        if (adultCount > 1) {
                            adultText = travellerAdultCount.getText().toString() + " " + getString(R.string.PAdultLbl);
                        } else {
                            adultText = travellerAdultCount.getText().toString() + " " + getString(R.string.SAdultLbl);
                        }
                        String childText = "";
                        if (childCount > 1) {
                            childText = travellerChildCount.getText().toString() + " " + getString(R.string.PChildLbl);
                        } else if (childCount == 1) {
                            childText = travellerChildCount.getText().toString() + " " + getString(R.string.SChildLbl);
                        }
                        String infantText = "";
                        if (infantCount > 1) {
                            infantText = travellerInfantCount.getText().toString() + " " + getString(R.string.PInfantLbl);
                        } else if (infantCount == 1) {
                            infantText = travellerInfantCount.getText().toString() + " " + getString(R.string.SInfantLbl);
                        }
                        String forwardDetailsMessage = "";
                        if (childText.isEmpty()) {
                            forwardDetailsMessage = Utils.formatDateToString(forwardFlightDate, getActivity()) + " " + adultText + " " + infantText;
                        } else {
                            forwardDetailsMessage = Utils.formatDateToString(forwardFlightDate, getActivity()) + " " + adultText + " " + childText + " " + infantText;
                        }
                        int totalPassengersCount = adultCount + childCount + infantCount;
                        Intent intent = new Intent(getActivity(), FlightForwardResultsActivity.class);
                        intent.putExtra(Constants.JSON_REQUEST, requestJSON);
                        intent.putExtra(Constants.TRIP_TYPE, tripType);
                        intent.putExtra(Constants.SELECTED_CLASS_TYPE, cabinPref);
                        intent.putExtra(Constants.ONE_WAY_NONSTOP_CHECKED, isNonStopChecked);
                        intent.putExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, multiWayNonStopChecked);
                        intent.putExtra(Constants.FORWARD_SOURCE, forwardJourneySource.getText().toString());
                        intent.putExtra(Constants.FORWARD_SOURCE_NAME, forwardJourneySourceAll.getText().toString());
                        intent.putExtra(Constants.FORWARD_DESTINATION, returnJourneyDestination.getText().toString());
                        intent.putExtra(Constants.RETURN_SOURCE_NAME, returnJourneyDestinationAll.getText().toString());
                        intent.putExtra(Constants.FORWARD_DETAILS, forwardDetailsMessage);
                        intent.putExtra(Constants.FORWARD_FLIGHT_DATE, forwardFlightDate);
                        intent.putExtra(Constants.TOTAL_PASSENGERS_COUNT, String.valueOf(totalPassengersCount));
                        intent.putExtra(Constants.DATE_ONEWAY, Utils.formatDateToString(forwardFlightDate, getActivity()));
                        intent.putExtra(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                        mActivity.startActivity(intent);
                    } else if (tripType.equalsIgnoreCase(roundTrip)) {
                        String adultText = "";
                        if (adultCount > 1) {
                            adultText = travellerAdultCount.getText().toString() + " " + getString(R.string.PAdultLbl);
                        } else {
                            adultText = travellerAdultCount.getText().toString() + " " + getString(R.string.SAdultLbl);
                        }
                        String childText = "";
                        if (childCount > 1) {
                            childText = travellerChildCount.getText().toString() + " " + getString(R.string.PChildLbl);
                        } else if (childCount == 1) {
                            childText = travellerChildCount.getText().toString() + " " + getString(R.string.SChildLbl);
                        }
                        String infantText = "";
                        if (infantCount > 1) {
                            infantText = travellerInfantCount.getText().toString() + " " + getString(R.string.PInfantLbl);
                        } else if (infantCount == 1) {
                            infantText = travellerInfantCount.getText().toString() + " " + getString(R.string.SInfantLbl);
                        }
                        String forwardDetailsMessage = "";
                        if (childText.isEmpty()) {
                            forwardDetailsMessage = Utils.formatDateToString(forwardFlightDate, getActivity()) + " - " + Utils.formatDateToString(returnFlightDate, getActivity()) + " " + adultText + " " + infantText;
                        } else {
                            forwardDetailsMessage = Utils.formatDateToString(forwardFlightDate, getActivity()) + " - " + Utils.formatDateToString(returnFlightDate, getActivity()) + " " + adultText + " " + childText + " " + infantText;
                        }
                        int totalPassengersCount = adultCount + childCount + infantCount;
                        Intent intent = new Intent(getActivity(), FlightForwardResultsActivity.class);
                        intent.putExtra(Constants.JSON_REQUEST, requestJSON);
                        intent.putExtra(Constants.TRIP_TYPE, tripType);
                        intent.putExtra(Constants.SELECTED_CLASS_TYPE, cabinPref);
                        intent.putExtra(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                        intent.putExtra(Constants.ONE_WAY_NONSTOP_CHECKED, isNonStopChecked);
                        intent.putExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, multiWayNonStopChecked);
                        intent.putExtra(Constants.FORWARD_SOURCE, forwardJourneySource.getText().toString());
                        intent.putExtra(Constants.FORWARD_SOURCE_NAME, forwardJourneySourceAll.getText().toString());
                        intent.putExtra(Constants.FORWARD_DESTINATION, returnJourneyDestination.getText().toString());
                        intent.putExtra(Constants.RETURN_SOURCE_NAME, returnJourneyDestinationAll.getText().toString());
                        intent.putExtra(Constants.FORWARD_DETAILS, forwardDetailsMessage);
                        intent.putExtra(Constants.FORWARD_FLIGHT_DATE, forwardFlightDate);
                        intent.putExtra(Constants.RETURN_FLIGHT_DATE, returnFlightDate);
                        intent.putExtra(Constants.TOTAL_PASSENGERS_COUNT, String.valueOf(totalPassengersCount));
                        intent.putExtra(Constants.DATE_ONEWAY, Utils.formatDateToString(forwardFlightDate, getActivity()));
                        mActivity.startActivity(intent);
                    } else if (tripType.equalsIgnoreCase(multiTrip)) {
                        int totalPassengersCount = defaultMTAdultCount + defaultMTChildCount + defaultMTInfantCount;
                        String forwardDetailsMessage = Utils.formatDateToString(firstFlightDate, getActivity());
                        Intent intent = new Intent(getActivity(), MultiCityResultsActivity.class);
                        intent.putExtra(Constants.JSON_REQUEST, requestJSON);
                        intent.putExtra(Constants.TRIP_TYPE, tripType);
                        intent.putExtra(Constants.BOOKING_RANDOM_NUMBER, randomNumber);
                        intent.putExtra(Constants.SELECTED_CLASS_TYPE, multiCabinPref);
                        intent.putExtra(Constants.TOTAL_PASSENGERS_COUNT, String.valueOf(totalPassengersCount));
                        intent.putExtra(Constants.ONE_WAY_NONSTOP_CHECKED, isNonStopChecked);
                        intent.putExtra(Constants.MULTI_TRIP_NONSTOP_CHECKED, multiWayNonStopChecked);
                        intent.putExtra(Constants.FORWARD_SOURCE, multiWayFirstJourneySource.getText().toString());
                        intent.putExtra(Constants.FORWARD_DESTINATION, multiWayFirstJourneyDestination.getText().toString());
                        intent.putExtra(Constants.FORWARD_DETAILS, forwardDetailsMessage);
                        intent.putExtra(Constants.FIRST_FLIGHT_DATE, firstFlightDate);
                        intent.putExtra(Constants.MULTI_TRIP_LEG_COUNT, 1);
                        if (!multiWaySecondJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                            forwardDetailsMessage = Utils.formatDateToString(firstFlightDate, getActivity()) + ", " + Utils.formatDateToString(secondFlightDate, getActivity());
                            intent.putExtra(Constants.SECOND_FORWARD_SOURCE, multiWaySecondJourneySource.getText().toString());
                            intent.putExtra(Constants.SECOND_FORWARD_DESTINATION, multiWaySecondJourneyDestination.getText().toString());
                            intent.putExtra(Constants.FORWARD_DETAILS, forwardDetailsMessage);
                            intent.putExtra(Constants.SECOND_FLIGHT_DATE, secondFlightDate);
                            intent.putExtra(Constants.MULTI_TRIP_LEG_COUNT, 2);
                        }
                        if (!multiWayThirdJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                            forwardDetailsMessage = Utils.formatDateToString(firstFlightDate, getActivity()) + ", " + Utils.formatDateToString(secondFlightDate, getActivity()) + ", " + Utils.formatDateToString(thirdFlightDate, getActivity());
                            intent.putExtra(Constants.THIRD_FORWARD_SOURCE, multiWayThirdJourneySource.getText().toString());
                            intent.putExtra(Constants.THIRD_FORWARD_DESTINATION, multiWayThirdJourneyDestination.getText().toString());
                            intent.putExtra(Constants.FORWARD_DETAILS, forwardDetailsMessage);
                            intent.putExtra(Constants.THIRD_FLIGHT_DATE, thirdFlightDate);
                            intent.putExtra(Constants.MULTI_TRIP_LEG_COUNT, 3);
                        }
                        mActivity.startActivity(intent);
                    }
                }
            }
        });
        multiWayFirstJourneyDateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelectedSegment = 1;
                Intent it = new Intent(getActivity(), CalendarDisplayActivity.class);
                it.putExtra(Constants.TRIP_TYPE, 1);
                it.putExtra(Constants.SELECTED_DATE, firstFlightDate);
                mActivity.startActivityForResult(it, MULTI_TRIP);
            }
        });
        multiWaySecondJourneyDateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelectedSegment = 2;
                Intent it = new Intent(getActivity(), CalendarDisplayActivity.class);
                it.putExtra(Constants.TRIP_TYPE, 1);
                it.putExtra(Constants.SELECTED_DATE, secondFlightDate);
                it.putExtra(Constants.DISABLED_DATES, firstFlightDate);
                mActivity.startActivityForResult(it, MULTI_TRIP);
            }
        });
        multiWayThirdJourneyDateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelectedSegment = 3;
                Intent it = new Intent(getActivity(), CalendarDisplayActivity.class);
                it.putExtra(Constants.TRIP_TYPE, 1);
                it.putExtra(Constants.SELECTED_DATE, thirdFlightDate);
                it.putExtra(Constants.DISABLED_DATES, secondFlightDate);
                mActivity.startActivityForResult(it, MULTI_TRIP);
            }
        });
        errorView = (RelativeLayout) view.findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) view.findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(titleFace);
        myReceiver = new MyReceiver();
        intentFilter = new IntentFilter("com.flyin.bookings.ERROR_MESSAGE");
        mActivity.registerReceiver(myReceiver, intentFilter);
//        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
//            if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            } else {
//                requestPermission(getActivity());
//            }
//        }
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.unregisterReceiver(myReceiver);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == SEARCH_FROM_AIRPORT) {
                if (resultCode == mActivity.RESULT_OK) {
                    if (data != null) {
                        String seledctedCode = data.getStringExtra(Constants.FROM_AIRPORT_CODE);
                        forwardJourneySource.setText(seledctedCode);
                        String selectedCityName = data.getStringExtra(Constants.FROM_AIRPORT_CITY);
                        forwardJourneySourceAll.setText(selectedCityName);
                        forwardJourneySourceAll.setVisibility(View.VISIBLE);
                        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                        SharedPreferences.Editor ed = preferences.edit();
                        ed.putString(Constants.FROM_AIRPORT_CODE, seledctedCode);
                        ed.putString(Constants.FROM_AIRPORT, selectedCityName);
                        ed.commit();
                    }
                }
            } else if (requestCode == SEARCH_TO_AIRPORT) {
                if (resultCode == mActivity.RESULT_OK) {
                    if (data != null) {
                        String seledctedCode = data.getStringExtra(Constants.FROM_AIRPORT_CODE);
                        returnJourneyDestination.setText(seledctedCode);
                        String selectedCityName = data.getStringExtra(Constants.FROM_AIRPORT_CITY);
                        returnJourneyDestinationAll.setText(selectedCityName);
                        returnJourneyDestinationAll.setVisibility(View.VISIBLE);
                        SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
                        SharedPreferences.Editor ed = preferences.edit();
                        ed.putString(Constants.TO_AIRPORT_CODE, seledctedCode);
                        ed.putString(Constants.TO_AIRPORT, selectedCityName);
                        ed.commit();
                    }
                }
            } else if (requestCode == SEARCH_FROM_FIRST_AIRPORT) {
                if (resultCode == mActivity.RESULT_OK) {
                    if (data != null) {
                        String seledctedCode = data.getStringExtra(Constants.FROM_AIRPORT_CODE);
                        multiWayFirstJourneySource.setText(seledctedCode);
                        String selectedCityName = data.getStringExtra(Constants.FROM_AIRPORT_CITY);
                        multiWayFirstJourneySourceAll.setText(selectedCityName);
                        multiWayFirstJourneySourceAll.setVisibility(View.VISIBLE);
                    }
                }
            } else if (requestCode == SEARCH_TO_FIRST_AIRPORT) {
                if (resultCode == mActivity.RESULT_OK) {
                    if (data != null) {
                        String seledctedCode = data.getStringExtra(Constants.FROM_AIRPORT_CODE);
                        multiWayFirstJourneyDestination.setText(seledctedCode);
                        String selectedCityName = data.getStringExtra(Constants.FROM_AIRPORT_CITY);
                        multiWayFirstJourneyDestinationAll.setText(selectedCityName);
                        multiWayFirstJourneyDestinationAll.setVisibility(View.VISIBLE);
                    }
                }
            } else if (requestCode == SEARCH_FROM_SECOND_AIRPORT) {
                if (resultCode == mActivity.RESULT_OK) {
                    if (data != null) {
                        String seledctedCode = data.getStringExtra(Constants.FROM_AIRPORT_CODE);
                        String selectedCityName = data.getStringExtra(Constants.FROM_AIRPORT_CITY);
                        multiWaySecondJourneySource.setText(seledctedCode);
                        multiWaySecondJourneySourceAll.setText(selectedCityName);
                        multiWaySecondJourneySourceAll.setVisibility(View.VISIBLE);
                    }
                }
            } else if (requestCode == SEARCH_TO_SECOND_AIRPORT) {
                if (resultCode == mActivity.RESULT_OK) {
                    if (data != null) {
                        String seledctedCode = data.getStringExtra(Constants.FROM_AIRPORT_CODE);
                        multiWaySecondJourneyDestination.setText(seledctedCode);
                        String selectedCityName = data.getStringExtra(Constants.FROM_AIRPORT_CITY);
                        multiWaySecondJourneyDestinationAll.setText(selectedCityName);
                        multiWaySecondJourneyDestinationAll.setVisibility(View.VISIBLE);
                    }
                }
            } else if (requestCode == SEARCH_FROM_THIRD_AIRPORT) {
                if (resultCode == mActivity.RESULT_OK) {
                    if (data != null) {
                        String seledctedCode = data.getStringExtra(Constants.FROM_AIRPORT_CODE);
                        multiWayThirdJourneySource.setText(seledctedCode);
                        String selectedCityName = data.getStringExtra(Constants.FROM_AIRPORT_CITY);
                        multiWayThirdJourneySourceAll.setText(selectedCityName);
                        multiWayThirdJourneySourceAll.setVisibility(View.VISIBLE);
                    }
                }
            } else if (requestCode == SEARCH_TO_THIRD_AIRPORT) {
                if (resultCode == mActivity.RESULT_OK) {
                    if (data != null) {
                        String seledctedCode = data.getStringExtra(Constants.FROM_AIRPORT_CODE);
                        multiWayThirdJourneyDestination.setText(seledctedCode);
                        String selectedCityName = data.getStringExtra(Constants.FROM_AIRPORT_CITY);
                        multiWayThirdJourneyDestinationAll.setText(selectedCityName);
                        multiWayThirdJourneyDestinationAll.setVisibility(View.VISIBLE);
                    }
                }
            }
            if (requestCode == SINGLE_TRIP) {
                if (data != null) {
                    int day = data.getExtras().getInt(Constants.SELECTED_DAY, 0);
                    int month = data.getExtras().getInt(Constants.SELECTED_MONTH, 0);
                    int year = data.getExtras().getInt(Constants.SELECTED_YEAR, 0);
                    forwardFlightDate = year + "-" + month + "-" + day;
                    String convertedFormatforwardFlight = Utils.convertTocalendardate(forwardFlightDate, getActivity());
                    round_departing_text.setText(convertedFormatforwardFlight);
                    selectdate_title_text.setText(getString(R.string.label_search_screen_flight_depart));
                }
            } else if (requestCode == ROUND_TRIP) {
                if (data != null) {
                    int day = data.getExtras().getInt(Constants.DEPARTURE_DAY, 0);
                    int month = data.getExtras().getInt(Constants.DEPARTURE_MONTH, 0);
                    int year = data.getExtras().getInt(Constants.DEPARTURE_YEAR, 0);
                    forwardFlightDate = year + "-" + month + "-" + day;
                    String convertedFormatforwardFlight = Utils.convertTocalendardate(year + "-" + month + "-" + day, getActivity());
                    round_departing_text.setText(convertedFormatforwardFlight);
                    day = data.getExtras().getInt(Constants.RETURN_DAY, 0);
                    month = data.getExtras().getInt(Constants.RETURN_MONTH, 0);
                    year = data.getExtras().getInt(Constants.RETURN_YEAR, 0);
                    returnFlightDate = year + "-" + month + "-" + day;
                    String convertedFormatreturnFlight = Utils.convertTocalendardate(year + "-" + month + "-" + day, getActivity());
                    round_return_text.setText(convertedFormatreturnFlight);
                    selectdate_title_text.setText(getString(R.string.label_search_screen_flight_depart) + " " + getString(R.string.label_date_divider_text) + " " + getString(R.string.label_search_screen_flight_return));
                }
            } else if (requestCode == MULTI_TRIP) {
                if (data != null) {
                    int day = data.getExtras().getInt(Constants.SELECTED_DAY, 0);
                    int month = data.getExtras().getInt(Constants.SELECTED_MONTH, 0);
                    int year = data.getExtras().getInt(Constants.SELECTED_YEAR, 0);
                    if (dateSelectedSegment == 1) {
                        firstFlightDate = year + "-" + month + "-" + day;
                        String convertedFormatfirstFlightDate = Utils.convertTocalendardate(year + "-" + month + "-" + day, getActivity());
                        first_flightdate_text.setText(convertedFormatfirstFlightDate);
                        first_selectdate_text.setText(getString(R.string.label_search_screen_flight_depart));
                        secondFlightDate = "";
                        second_flightdate_text.setText(getString(R.string.label_search_screen_flight_depart));
                        thirdFlightDate = "";
                        third_flightdate_text.setText(getString(R.string.label_search_screen_flight_depart));
                    } else if (dateSelectedSegment == 2) {
                        secondFlightDate = year + "-" + month + "-" + day;
                        String convertedFormatfirstFlightDate = Utils.convertTocalendardate(year + "-" + month + "-" + day, getActivity());
                        second_flightdate_text.setText(convertedFormatfirstFlightDate);
                        second_selectdat_text.setText(getString(R.string.label_search_screen_flight_depart));
                        thirdFlightDate = "";
                        third_flightdate_text.setText(getString(R.string.label_search_screen_flight_depart));
                    } else if (dateSelectedSegment == 3) {
                        thirdFlightDate = year + "-" + month + "-" + day;
                        String convertedFormatfirstFlightDate = Utils.convertTocalendardate(year + "-" + month + "-" + day, getActivity());
                        third_flightdate_text.setText(convertedFormatfirstFlightDate);
                        third_selectdate_text.setText(getString(R.string.label_search_screen_flight_depart));
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    private void onAdultIncrement() {
        if (tripType.equalsIgnoreCase(multiTrip)) {
            if (!checkIsValidRecordsCount(defaultMTAdultCount, defaultMTChildCount, defaultMTInfantCount, false, false)) {
                return;
            }
            defaultMTAdultCount++;
            multiTravellerAdultCount.setText(String.valueOf(defaultMTAdultCount));
            if (defaultMTAdultCount > 1) {
                multiTravellerAdultText.setText(getString(R.string.PAdultLbl));
            } else {
                multiTravellerAdultText.setText(getString(R.string.SAdultLbl));
            }
        } else {
            if (!checkIsValidRecordsCount(adultCount, childCount, infantCount, false, false)) {
                return;
            }
            adultCount++;
            travellerAdultCount.setText(String.valueOf(adultCount));
            if (adultCount > 1) {
                travellerAdultText.setText(getString(R.string.PAdultLbl));
            } else {
                travellerAdultText.setText(getString(R.string.SAdultLbl));
            }
        }
    }

    public void refreshBackground() {
        try {
            SharedPreferences preferences = mActivity.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
            if (preferences.getBoolean(Constants.isFanClub, false)) {
                if (flightViewBgImage != null) {
                    flightViewBgImage.setImageResource(R.drawable.alhilalbg);
                    flightViewBgImage.setColorFilter(ContextCompat.getColor(getContext(), R.color.search_image_bg_color));
                }
            } else {
                if (flightViewBgImage != null) {
                    flightViewBgImage.setImageResource(R.drawable.flighsearchbg);
                    flightViewBgImage.setColorFilter(ContextCompat.getColor(getContext(), R.color.search_screen_bg_color));
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshBackground();
    }

    private void onAdultDecrement() {
        if (tripType.equalsIgnoreCase(multiTrip)) {
            if (defaultMTAdultCount == 1) {
                return;
            }
            if (!checkIsValidRecordsCount(defaultMTAdultCount, defaultMTChildCount, defaultMTInfantCount, true, false)) {
                return;
            }
            defaultMTAdultCount--;
            multiTravellerAdultCount.setText(String.valueOf(defaultMTAdultCount));
            if (defaultMTAdultCount > 1) {
                multiTravellerAdultText.setText(getString(R.string.PAdultLbl));
            } else {
                multiTravellerAdultText.setText(getString(R.string.SAdultLbl));
            }
        } else {
            if (adultCount == 1) {
                return;
            }
            if (!checkIsValidRecordsCount(adultCount, childCount, infantCount, true, false)) {
                return;
            }
            adultCount--;
            travellerAdultCount.setText(String.valueOf(adultCount));
            if (adultCount > 1) {
                travellerAdultText.setText(getString(R.string.PAdultLbl));
            } else {
                travellerAdultText.setText(getString(R.string.SAdultLbl));
            }
        }
    }

    private void onChildrenIncrement() {
        if (tripType.equalsIgnoreCase(multiTrip)) {
            if (!checkIsValidRecordsCount(defaultMTAdultCount, defaultMTChildCount, defaultMTInfantCount, false, false)) {
                return;
            }
            defaultMTChildCount++;
            multiTravellerChildCount.setText(String.valueOf(defaultMTChildCount));
            if (defaultMTChildCount >= 1) {
                multiTravellerChildCount.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                multiTravellerChildText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
            } else {
                multiTravellerChildCount.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
                multiTravellerChildText.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
            }
            if (defaultMTChildCount <= 1) {
                multiTravellerChildText.setText(getString(R.string.SChildLbl));
            } else {
                multiTravellerChildText.setText(getString(R.string.PChildLbl));
            }
        } else {
            if (!checkIsValidRecordsCount(adultCount, childCount, infantCount, false, false)) {
                return;
            }
            childCount++;
            travellerChildCount.setText(String.valueOf(childCount));
            if (childCount >= 1) {
                travellerChildCount.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                travellerChildText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
            } else {
                travellerChildCount.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
                travellerChildText.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
            }
            if (childCount <= 1) {
                travellerChildText.setText(getString(R.string.SChildLbl));
            } else {
                travellerChildText.setText(getString(R.string.PChildLbl));
            }
        }
    }

    private void onChildrenDecrement() {
        if (tripType.equalsIgnoreCase(multiTrip)) {
            if (defaultMTChildCount == 0) {
                return;
            }
            defaultMTChildCount--;
            multiTravellerChildCount.setText(String.valueOf(defaultMTChildCount));
            if (defaultMTChildCount >= 1) {
                multiTravellerChildCount.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                multiTravellerChildText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
            } else {
                multiTravellerChildCount.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
                multiTravellerChildText.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
            }
            if (defaultMTChildCount <= 1) {
                multiTravellerChildText.setText(getString(R.string.SChildLbl));
            } else {
                multiTravellerChildText.setText(getString(R.string.PChildLbl));
            }
        } else {
            if (childCount == 0) {
                return;
            }
            childCount--;
            travellerChildCount.setText(String.valueOf(childCount));
            if (childCount >= 1) {
                travellerChildCount.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                travellerChildText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
            } else {
                travellerChildCount.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
                travellerChildText.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
            }
            if (childCount <= 1) {
                travellerChildText.setText(getString(R.string.SChildLbl));
            } else {
                travellerChildText.setText(getString(R.string.PChildLbl));
            }
        }
    }

    private void onInfantIncrement() {
        if (tripType.equalsIgnoreCase(multiTrip)) {
            if (!checkIsValidRecordsCount(defaultMTAdultCount, defaultMTChildCount, defaultMTInfantCount, true, false)) {
                return;
            }
            defaultMTInfantCount++;
            multiTravellerInfantCount.setText(String.valueOf(defaultMTInfantCount));
            if (defaultMTInfantCount >= 1) {
                multiTravellerInfantCount.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                multiTravellerInfantText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
            } else {
                multiTravellerInfantCount.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
                multiTravellerInfantText.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
            }
            if (defaultMTInfantCount > 1) {
                multiTravellerInfantText.setText(getString(R.string.PInfantLbl));
            } else {
                multiTravellerInfantText.setText(getString(R.string.SInfantLbl));
            }
        } else {
            if (!checkIsValidRecordsCount(adultCount, childCount, infantCount, true, false)) {
                return;
            }
            infantCount++;
            travellerInfantCount.setText(String.valueOf(infantCount));
            if (infantCount >= 1) {
                travellerInfantCount.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                travellerInfantText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
            } else {
                travellerInfantCount.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
                travellerInfantText.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
            }
            if (infantCount > 1) {
                travellerInfantText.setText(getString(R.string.PInfantLbl));
            } else {
                travellerInfantText.setText(getString(R.string.SInfantLbl));
            }
        }
    }

    private void onInfantDecrement() {
        if (tripType.equalsIgnoreCase(multiTrip)) {
            if (defaultMTInfantCount == 0) {
                return;
            }
            defaultMTInfantCount--;
            multiTravellerInfantCount.setText(String.valueOf(defaultMTInfantCount));
            if (defaultMTInfantCount >= 1) {
                multiTravellerInfantCount.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                multiTravellerInfantText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
            } else {
                multiTravellerInfantCount.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
                multiTravellerInfantText.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
            }
            if (defaultMTInfantCount > 1) {
                multiTravellerInfantText.setText(getString(R.string.PInfantLbl));
            } else {
                multiTravellerInfantText.setText(getString(R.string.SInfantLbl));
            }
        } else {
            if (infantCount == 0) {
                return;
            }
            infantCount--;
            travellerInfantCount.setText(String.valueOf(infantCount));
            if (infantCount >= 1) {
                travellerInfantCount.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
                travellerInfantText.setTextColor(Utils.getColor(getActivity(), R.color.contact_header_color));
            } else {
                travellerInfantCount.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
                travellerInfantText.setTextColor(Utils.getColor(getActivity(), R.color.traveller_picker_text_color));
            }
            if (infantCount > 1) {
                travellerInfantText.setText(getString(R.string.PInfantLbl));
            } else {
                travellerInfantText.setText(getString(R.string.SInfantLbl));
            }
        }
    }

    private boolean checkIsValidRecordsCount(int adults, int children, int infant, boolean isInfantsClicked, boolean isAdultDecrement) {
        boolean isValid = true;
        if (adults == infant && isInfantsClicked) {
            displayErrorMessage(getString(R.string.label_err_infants_msg));
            isValid = false;
        } else if (isAdultDecrement) {
            if (adults == infant) {
                displayErrorMessage(getString(R.string.label_err_infants_msg));
                isValid = false;
            } else {
                isValid = true;
            }
        } else if (adults + children >= 9 && !isInfantsClicked) {
            displayErrorMessage(getString(R.string.label_err_tot_trav_msg));
            isValid = false;
        }
        return isValid;
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.setBackgroundResource(R.color.error_message_background);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(mActivity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (errorView != null) {
                            errorView.animate().alpha(0)
                                    .setDuration(mActivity.getResources().getInteger(android.R.integer.config_mediumAnimTime));
                        }
                    } catch (Exception e) {
                    }
                }
            }, 2000);
        }
    }

    private void handleDoneTraveller() {
        if (tripType.equalsIgnoreCase(multiTrip)) {
            multiTravellerPickerLayout.setVisibility(View.GONE);
            multiway_travellerslayout_settext();
        } else {
            roundtrip_travellerslayout_settext();
            travellersPickerLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                return;
            }
        }
    }

    private static void requestPermission(final Context context) {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            new AlertDialog.Builder(context)
                    .setMessage(context.getString(R.string.permission_storage))
                    .setPositiveButton(R.string.label_selected_flight_message, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                    }).show();
        } else {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.flyin.bookings.ERROR_MESSAGE")) {
                String type = "";
                if (intent.hasExtra("type")) {
                    type = intent.getStringExtra("type");
                }
                if (type.equalsIgnoreCase("success")) {
                    displaySuccessMessage(intent.getStringExtra("message"), true);
                } else {
                    displaySuccessMessage(intent.getStringExtra("message"), false);
                }
            }
        }
    }

    private void displaySuccessMessage(String message, boolean isSucessMessage) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = mActivity.getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            if (isSucessMessage) {
                errorView.setBackgroundResource(R.color.success_message_background);
            } else {
                errorView.setBackgroundResource(R.color.error_message_background);
            }
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(mActivity.getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(mActivity.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private String handleSearchFlights() {
        JSONObject maJSON = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        try {
            JSONObject mainObj = new JSONObject();
            JSONObject sourceObj = new JSONObject();
            sourceObj = Utils.getAppSettingData(sourceObj, getActivity());
            sourceObj.put("isoCurrency", "SAR");
            sourceObj.put("echoToken", randomNumber);
            sourceObj.put("timeStamp", timeStamp);
            sourceObj.put("clientId", Constants.CLIENT_ID);
            sourceObj.put("clientSecret", Constants.CLIENT_SECRET);
            sourceObj.put("accessToken", Utils.getRequestAccessToken(getActivity()));
            sourceObj.put("groupType", groupType);
            mainObj.put("source", sourceObj);
            JSONArray arr = new JSONArray();
            JSONObject tripObj = new JSONObject();
            if (tripType.equalsIgnoreCase(singleTrip)) {
                tripObj = new JSONObject();
                tripObj.put("dap", forwardJourneySource.getText().toString());
                tripObj.put("dd", Utils.formatDateToServerDateFormat(forwardFlightDate));
                tripObj.put("aap", returnJourneyDestination.getText().toString());
                arr.put(tripObj);
            } else if (tripType.equalsIgnoreCase(roundTrip)) {
                tripObj = new JSONObject();
                tripObj.put("dap", forwardJourneySource.getText().toString());
                tripObj.put("dd", Utils.formatDateToServerDateFormat(forwardFlightDate));
                tripObj.put("aap", returnJourneyDestination.getText().toString());
                arr.put(tripObj);
                tripObj = new JSONObject();
                tripObj.put("dap", returnJourneyDestination.getText().toString());
                tripObj.put("dd", Utils.formatDateToServerDateFormat(returnFlightDate));
                tripObj.put("aap", forwardJourneySource.getText().toString());
                arr.put(tripObj);
            } else if (tripType.equalsIgnoreCase(multiTrip)) {
                tripObj = new JSONObject();
                tripObj.put("dap", multiWayFirstJourneySource.getText().toString());
                tripObj.put("dd", Utils.formatDateToServerDateFormat(firstFlightDate));
                tripObj.put("aap", multiWayFirstJourneyDestination.getText().toString());
                arr.put(tripObj);
                if (!multiWaySecondJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                    tripObj = new JSONObject();
                    tripObj.put("dap", multiWaySecondJourneySource.getText().toString());
                    tripObj.put("dd", Utils.formatDateToServerDateFormat(secondFlightDate));
                    tripObj.put("aap", multiWaySecondJourneyDestination.getText().toString());
                    arr.put(tripObj);
                }
                if (!multiWayThirdJourneySource.getText().toString().equalsIgnoreCase(getString(R.string.label_search_flight_from))) {
                    tripObj = new JSONObject();
                    tripObj.put("dap", multiWayThirdJourneySource.getText().toString());
                    tripObj.put("dd", Utils.formatDateToServerDateFormat(thirdFlightDate));
                    tripObj.put("aap", multiWayThirdJourneyDestination.getText().toString());
                    arr.put(tripObj);
                }
            }
            JSONObject odosObj = new JSONObject();
            odosObj.put("odo", arr);
            mainObj.put("odos", odosObj);
            JSONArray ptqArr = new JSONArray();
            if (tripType.equalsIgnoreCase(singleTrip) || tripType.equalsIgnoreCase(roundTrip)) {
                JSONObject obj1 = new JSONObject();
                obj1.put("ADT", String.valueOf(adultCount));
                ptqArr.put(obj1);
                if (childCount > 0) {
                    JSONObject obj2 = new JSONObject();
                    obj2.put("CHD", String.valueOf(childCount));
                    ptqArr.put(obj2);
                }
                if (infantCount > 0) {
                    JSONObject obj3 = new JSONObject();
                    obj3.put("INF", String.valueOf(infantCount));
                    ptqArr.put(obj3);
                }
            } else if (tripType.equalsIgnoreCase(multiTrip)) {
                JSONObject obj1 = new JSONObject();
                obj1.put("ADT", String.valueOf(defaultMTAdultCount));
                ptqArr.put(obj1);
                if (defaultMTChildCount > 0) {
                    JSONObject obj2 = new JSONObject();
                    obj2.put("CHD", String.valueOf(defaultMTChildCount));
                    ptqArr.put(obj2);
                }
                if (defaultMTInfantCount > 0) {
                    JSONObject obj3 = new JSONObject();
                    obj3.put("INF", String.valueOf(defaultMTInfantCount));
                    ptqArr.put(obj3);
                }
            }
            Singleton.getInstance().ptqArr = ptqArr;
            mainObj.put("ptq", ptqArr);
            if (tripType.equalsIgnoreCase(multiTrip)) {
                mainObj.put("ct", multiCabinPref);
            } else {
                mainObj.put("ct", cabinPref);
            }
            mainObj.put("tt", tripType);
            mainObj.put("apref", "");
            mainObj.put("nonstf", "");
            mainObj.put("skm", "false");
            if (tripType.equalsIgnoreCase(multiTrip)) {
                mainObj.put("fareType", "CMBF_false");
            } else {
                mainObj.put("fareType", "CMBF");
            }
            mainObj.put("extra", "");
            maJSON.put("searchRQ", mainObj);
        } catch (Exception e) {
        }
        return maJSON.toString();
    }

    private void handleFareRequest() {

    }

    private void textfont_init() {
        String fontBold = Constants.FONT_ROBOTO_BOLD;
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        String fontText = Constants.FONT_ROBOTO_LIGHT;
        isArabicLang = false;
        if (Utils.isArabicLangSelected(getActivity())) {
            fontBold = Constants.FONT_DROIDKUFI_BOLD;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            isArabicLang = true;
        }
        tf = Typeface.createFromAsset(mActivity.getAssets(), fontPath);
        titleFace = Typeface.createFromAsset(mActivity.getAssets(), fontTitle);
        textFace = Typeface.createFromAsset(mActivity.getAssets(), fontText);
        textBold = Typeface.createFromAsset(mActivity.getAssets(), fontBold);
    }

    ////////////////////////round_trip_select_bar_init/////////////////////////
    private void roundtrip_select_bar_init(View view) {
        roundTripLayout = (LinearLayout) view.findViewById(R.id.round_trip);
        oneWayLayout = (LinearLayout) view.findViewById(R.id.one_way);
        multiWayLayout = (LinearLayout) view.findViewById(R.id.multi_way);
        roundtripbar = (LinearLayout) view.findViewById(R.id.roundtripbar);
        multicitybar = (LinearLayout) view.findViewById(R.id.multicitybar);
        onewaybar = (LinearLayout) view.findViewById(R.id.onewaybar);
        roundJourneyText = (TextView) view.findViewById(R.id.title_round);
        singleJourneyText = (TextView) view.findViewById(R.id.title_single);
        multiJourneyText = (TextView) view.findViewById(R.id.title_multi);
        roundJourneyText.setTypeface(titleFace);
        singleJourneyText.setTypeface(titleFace);
        multiJourneyText.setTypeface(titleFace);
        roundtrip_select(0);
        roundtrip_select_bar_clicklistener_set();
    }

    private void roundtrip_select(int select) {
        roundJourneyText.setTextColor(Utils.getColor(getActivity(), R.color.white_color));
        singleJourneyText.setTextColor(Utils.getColor(getActivity(), R.color.white_color));
        multiJourneyText.setTextColor(Utils.getColor(getActivity(), R.color.white_color));
        roundtripbar.setVisibility(View.INVISIBLE);
        onewaybar.setVisibility(View.INVISIBLE);
        multicitybar.setVisibility(View.INVISIBLE);
        if (select == 0) {
            roundJourneyText.setTextColor(Utils.getColor(getActivity(), R.color.white_color));
            roundtripbar.setVisibility(View.VISIBLE);
        } else if (select == 1) {
            singleJourneyText.setTextColor(Utils.getColor(getActivity(), R.color.white_color));
            onewaybar.setVisibility(View.VISIBLE);
        } else {
            multiJourneyText.setTextColor(Utils.getColor(getActivity(), R.color.white_color));
            multicitybar.setVisibility(View.VISIBLE);
        }
    }

    private void roundtrip_select_bar_clicklistener_set() {
        roundTripLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                roundtrip_select(0);
                tripType = "2";
                round_return_text.setVisibility(View.VISIBLE);
                minuse_symbol_text.setVisibility(View.VISIBLE);
                stopflightcheckbox.setVisibility(View.VISIBLE);
                selectdate_title_text.setText(getString(R.string.label_select_date));
                round_departing_text.setText(getString(R.string.label_search_screen_flight_depart));
                round_return_text.setText(getString(R.string.label_search_screen_flight_return));
                roundOneWayLinearLayout.setVisibility(View.VISIBLE);
                multiLinearLayout.setVisibility(View.GONE);
                forwardFlightDate = "";
                returnFlightDate = "";
            }
        });
        oneWayLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                roundtrip_select(1);
                tripType = "1";
                round_return_text.setVisibility(View.GONE);
                minuse_symbol_text.setVisibility(View.GONE);
                stopflightcheckbox.setVisibility(View.VISIBLE);
                selectdate_title_text.setText(getString(R.string.label_select_date));
                round_departing_text.setText(getString(R.string.label_search_screen_flight_depart));
                roundOneWayLinearLayout.setVisibility(View.VISIBLE);
                multiLinearLayout.setVisibility(View.GONE);
                forwardFlightDate = "";
            }
        });
        multiWayLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                roundtrip_select(2);
                tripType = "3";
                roundOneWayLinearLayout.setVisibility(View.GONE);
                multiLinearLayout.setVisibility(View.VISIBLE);
                stopflightcheckbox.setVisibility(View.GONE);
                first_selectdate_text.setText(R.string.label_select_date);
                first_flightdate_text.setText(getString(R.string.label_search_screen_flight_depart));
                firstFlightDate = "";
                second_selectdat_text.setText(R.string.label_select_date);
                second_flightdate_text.setText(getString(R.string.label_search_screen_flight_depart));
                secondFlightDate = "";
                third_selectdate_text.setText(R.string.label_select_date);
                third_flightdate_text.setText(getString(R.string.label_search_screen_flight_depart));
                thirdFlightDate = "";
            }
        });
    }

    //////////Round Trip select Datebar init////////////////
    private void selectdatebar_init(View view) {
        journey_roundTrip_layout = (RelativeLayout) view.findViewById(R.id.journey_roundTrip_dates);
        selectdate_title_text = (TextView) view.findViewById(R.id.SelectdatetextView);
        round_departing_text = (TextView) view.findViewById(R.id.round_forward_journey_date);
        round_return_text = (TextView) view.findViewById(R.id.round_return_journey_date);
        minuse_symbol_text = (TextView) view.findViewById(R.id.minuse_symbol_text);
        selectdate_title_text.setTypeface(titleFace);
        round_departing_text.setTypeface(titleFace);
        round_return_text.setTypeface(titleFace);
        journey_roundTrip_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                roundtrip_launch_selectdate_activity();
            }
        });
        if (isArabicLang) {
            selectdate_title_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            round_departing_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            round_return_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            param.setMargins(0, -5, 0, 0);
            round_departing_text.setLayoutParams(param);
            round_return_text.setLayoutParams(param);
            minuse_symbol_text.setLayoutParams(param);
        }
    }

    private void roundtrip_launch_selectdate_activity() {
        if (tripType.equalsIgnoreCase("1")) {
            Intent it = new Intent(getActivity(), CalendarDisplayActivity.class);
            it.putExtra(Constants.TRIP_TYPE, Integer.parseInt(tripType));
            it.putExtra(Constants.SELECTED_DATE, forwardFlightDate);
            mActivity.startActivityForResult(it, SINGLE_TRIP);
        } else if (tripType.equalsIgnoreCase("2")) {
            Intent it = new Intent(getActivity(), CalendarDisplayActivity.class);
            it.putExtra(Constants.TRIP_TYPE, Integer.parseInt(tripType));
            it.putExtra(Constants.SELECTED_DEPARTURE_DATE, forwardFlightDate);
            it.putExtra(Constants.SELECTED_RETURN_DATE, returnFlightDate);
            it.putExtra(Constants.IS_DEPARTURE_SELECTED, true);
            mActivity.startActivityForResult(it, ROUND_TRIP);
        }
    }

    ///////////roundtrip travellerlayout init
    private void roudntrip_travellerslayout_init(View view) {
        travellerDetailsLayout = (RelativeLayout) view.findViewById(R.id.traveller_details);
        travellerDetailsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travellersPickerLayout.setVisibility(View.VISIBLE);
            }
        });
        travellerstitle = (TextView) view.findViewById(R.id.travellerstext);
        travellerstitle.setTypeface(titleFace);
        if (isArabicLang) {
            travellerstitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
        }
    }

    private void roundtrip_travellerslayout_settext() {
        travellerListAdultCount.setText(travellerAdultCount.getText().toString());
        travellerListChildCount.setText(travellerChildCount.getText().toString());
        travellerListInfantCount.setText(travellerInfantCount.getText().toString());
        travellerListAdultText.setText(travellerAdultText.getText().toString() + ",");
        travellerListChildText.setText(travellerChildText.getText().toString() + ",");
        travellerListInfantText.setText(travellerInfantText.getText().toString());
    }

    private void stopflightlayout_init(View view) {
        stopflightcheckbox = (CheckBox) view.findViewById(R.id.stopflightcheckBox);
        stopflightcheckbox.setTypeface(titleFace);
        if (isArabicLang) {
            stopflightcheckbox.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
        }
    }

    private void multitrip_datebars_init(View view) {
        first_selectdate_text = (TextView) view.findViewById(R.id.multiWay_first_select_text);
        first_flightdate_text = (TextView) view.findViewById(R.id.multiWay_first_journey_date);
        second_selectdat_text = (TextView) view.findViewById(R.id.multiWay_second_select_text);
        second_flightdate_text = (TextView) view.findViewById(R.id.multiWay_second_journey_date);
        third_selectdate_text = (TextView) view.findViewById(R.id.multiWay_third_select_text);
        third_flightdate_text = (TextView) view.findViewById(R.id.multiWay_third_journey_date);
        first_selectdate_text.setTypeface(titleFace);
        second_selectdat_text.setTypeface(titleFace);
        third_selectdate_text.setTypeface(titleFace);
        first_flightdate_text.setTypeface(titleFace);
        second_flightdate_text.setTypeface(titleFace);
        third_flightdate_text.setTypeface(titleFace);
        if (isArabicLang) {
            first_selectdate_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            first_flightdate_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            second_selectdat_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            second_flightdate_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            third_selectdate_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            third_flightdate_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            param.setMargins(0, -5, 0, 0);
            first_flightdate_text.setLayoutParams(param);
            second_flightdate_text.setLayoutParams(param);
            third_flightdate_text.setLayoutParams(param);
        }
    }

    private void muititriptrip_travellerslayout_init(View view) {
        multiwayStopflightcheckbox = (CheckBox) view.findViewById(R.id.multiway_stopflightcheckbox);
        mutiway_travellerstitle = (TextView) view.findViewById(R.id.multiway_travellers_title);
        multiWayTravellerList = (RelativeLayout) view.findViewById(R.id.multiWay_travellers_list);
        mutiway_travellerstitle.setTypeface(titleFace);
        multiwayStopflightcheckbox.setTypeface(titleFace);
        multiWayTravellerList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multiTravellerPickerLayout.setVisibility(View.VISIBLE);
            }
        });
        if (isArabicLang) {
            mutiway_travellerstitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
            multiwayStopflightcheckbox.setTextSize(TypedValue.COMPLEX_UNIT_PX, mActivity.getResources().getDimension(R.dimen.arabic_text_size));
        }
    }

    private void multiway_travellerslayout_settext() {
        multiWayTravellerListAdultCount.setText(multiTravellerAdultCount.getText().toString());
        multiWayTravellerListChildCount.setText(multiTravellerChildCount.getText().toString());
        multiWayTravellerListInfantCount.setText(multiTravellerInfantCount.getText().toString());
        multiWayTravellerListAdultText.setText(multiTravellerAdultText.getText().toString() + ",");
        multiWayTravellerListChildText.setText(multiTravellerChildText.getText().toString() + ",");
        multiWayTravellerListInfantText.setText(multiTravellerInfantText.getText().toString());
    }

    private void flyinimageview_init(View view) {
        ImageView fromimageview = (ImageView) view.findViewById(R.id.fromimageview);
        ImageView toimageview = (ImageView) view.findViewById(R.id.toimageview);
        ImageView first_fromimageview = (ImageView) view.findViewById(R.id.first_fromimageview);
        ImageView first_toimageview = (ImageView) view.findViewById(R.id.first_toimageview);
        ImageView second_fromimageview = (ImageView) view.findViewById(R.id.second_fromimageview);
        ImageView second_toimageview = (ImageView) view.findViewById(R.id.second_toimageview);
        ImageView third_fromimageview = (ImageView) view.findViewById(R.id.third_fromimageview);
        ImageView third_toimageview = (ImageView) view.findViewById(R.id.third_toimageview);
        if (isArabicLang) {
            fromimageview.setRotation(270);
            toimageview.setRotation(90);
            first_fromimageview.setRotation(270);
            first_toimageview.setRotation(90);
            second_fromimageview.setRotation(270);
            second_toimageview.setRotation(90);
            third_fromimageview.setRotation(270);
            third_toimageview.setRotation(90);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    private String handleBookingRQRequest() {
        SharedPreferences pref = mActivity.getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(mActivity));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(mActivity));
            mainJSON.put("source", sourceJSON);
            mainJSON.put("customerId", pref.getString(Constants.MEMBER_EMAIL, ""));
            mainJSON.put("fromDate", "");
            mainJSON.put("toDate", "");
            mainJSON.put("referenceNo", "B2C250557");
            mainJSON.put("searchType", "by_reference");
            mainJSON.put("referenceType", "Flight");
            mainJSON.put("recordType", "Only Flight");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }
}
