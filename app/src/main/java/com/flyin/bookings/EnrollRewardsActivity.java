package com.flyin.bookings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class EnrollRewardsActivity extends AppCompatActivity implements AsyncTaskListener {
    private String requestTermsUrl = "";
    private Typeface textFace;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private ImageView errorImage = null;
    private Button searchButton = null;
    private RelativeLayout headerViewLayout = null;
    private LayoutInflater inflater = null;
    private static final int GET_TRAVELER_PROFILE = 1;
    private static final int GET_ENROLL_REWARDS = 2;
    private static final int CHECK_REWARDS = 3;
    private View loadingView = null;
    private static final String TAG = "EnrollRewardsActivity";
    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enroll_rewards);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView flyinRewardsHeader = (TextView) findViewById(R.id.flyin_rewards_header);
        TextView enrollRewardsInfoOne = (TextView) findViewById(R.id.enroll_rewards_one);
        TextView enrollRewardsInfoTwo = (TextView) findViewById(R.id.enroll_rewards_two);
        final CheckBox enrollCheckBox = (CheckBox) findViewById(R.id.enroll_checkbox);
        TextView enrollConditionsText = (TextView) findViewById(R.id.enroll_conditions_text);
        Button enrollButton = (Button) findViewById(R.id.enroll_now_button);
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorView.setVisibility(View.GONE);
        headerViewLayout = (RelativeLayout) findViewById(R.id.header_view_layout);
        requestTermsUrl = Constants.EN_ENROLL_TERMS_URL;
        if (Utils.isArabicLangSelected(EnrollRewardsActivity.this)) {
            requestTermsUrl = Constants.AR_ENROLL_TERMS_URL;
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            flyinRewardsHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.contact_text));
            enrollRewardsInfoOne.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            enrollRewardsInfoTwo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            enrollCheckBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            enrollButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            enrollConditionsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            enrollRewardsInfoTwo.setVisibility(View.GONE);
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textFace = Typeface.createFromAsset(getAssets(), fontText);
        flyinRewardsHeader.setTypeface(tf);
        enrollRewardsInfoOne.setTypeface(textFace);
        enrollRewardsInfoTwo.setTypeface(textFace);
        enrollCheckBox.setTypeface(textFace);
        enrollConditionsText.setTypeface(textFace);
        enrollButton.setTypeface(textFace);
        errorMessageText.setTypeface(textFace);
        enrollConditionsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadTermsPage();
            }
        });
        enrollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enrollCheckBox.isChecked()) {
                    handleProfileData();
                }
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_flight, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_reward_points);
        mTitleText.setTypeface(tf);
        backText.setTypeface(textFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(EnrollRewardsActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }

    private void loadTermsPage() {
        if (Utils.isConnectingToInternet(EnrollRewardsActivity.this)) {
            Intent in = new Intent(EnrollRewardsActivity.this, AboutusActivity.class);
            in.putExtra(Constants.HTML_WEB_VIEW, requestTermsUrl);
            in.putExtra(Constants.ACTIONBAR_HEADER, getString(R.string.label_reward_points));
            startActivity(in);
        } else {
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isConnectingToInternet(EnrollRewardsActivity.this)) {
                        Intent in = new Intent(EnrollRewardsActivity.this, AboutusActivity.class);
                        in.putExtra(Constants.HTML_WEB_VIEW, requestTermsUrl);
                        in.putExtra(Constants.ACTIONBAR_HEADER, getString(R.string.label_reward_points));
                        startActivity(in);
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            headerViewLayout.addView(errorView);
        }
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private void handleProfileData() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        JSONObject mainJSON = new JSONObject();
        try {
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(EnrollRewardsActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(EnrollRewardsActivity.this));
            mainJSON.put("source", sourceJSON);
            mainJSON.put("userUniqueId", pref.getString(Constants.USER_UNIQUE_ID, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        showLoading();
        HTTPAsync async = new HTTPAsync(EnrollRewardsActivity.this, EnrollRewardsActivity.this, Constants.USER_PROFILE,
                "", mainJSON.toString(), GET_TRAVELER_PROFILE, HTTPAsync.METHOD_POST);
        async.execute();
    }

    private void handleEnrollRewards(String profileObj) {
        JSONObject userProfileJSON = null;
        try {
            userProfileJSON = new JSONObject(profileObj);
            userProfileJSON.remove("accessToken");
            JSONObject sourceJSON = new JSONObject();
            sourceJSON.put("device", Constants.DEVICE_TYPE);
            sourceJSON.put("accessToken", Utils.getRequestAccessToken(EnrollRewardsActivity.this));
            sourceJSON.put("clientId", Constants.CLIENT_ID);
            sourceJSON.put("clientSecret", Constants.CLIENT_SECRET);
            sourceJSON.put("isocountry", Utils.getUserCountry(EnrollRewardsActivity.this));
            userProfileJSON.put("source", sourceJSON);
            if (userProfileJSON.has("customerBasicDetails")) {
                JSONObject basicObj = new JSONObject(userProfileJSON.getString("customerBasicDetails"));
                String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
                String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
                String timeStamp = currentDateFormat + "T" + currentTimeFormat;
                basicObj.put("rewardJoinDate", timeStamp);
                basicObj.remove("flyinRewardEnable");
                userProfileJSON.put("customerBasicDetails", basicObj);
            }
            userProfileJSON.remove("passportDetails");
            JSONObject passportObj = new JSONObject();
            userProfileJSON.put("passportDetails", passportObj);
            HTTPAsync async = new HTTPAsync(EnrollRewardsActivity.this, EnrollRewardsActivity.this, Constants.EDIT_PROFILE,
                    "", userProfileJSON.toString(), GET_ENROLL_REWARDS, HTTPAsync.METHOD_POST);
            async.execute();
        } catch (JSONException e) {
        }
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        JSONObject obj = new JSONObject();
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) {
            closeLoading();
            displayEmptyMessage();
        } else {
            if (serviceType == GET_TRAVELER_PROFILE) {
                Utils.printMessage(TAG, "Profile DATA:: " + data);
                try {
                    obj = new JSONObject(data);
                    handleEnrollRewards(obj.toString());
                } catch (Exception e) {
                }
            } else if (serviceType == GET_ENROLL_REWARDS) {
                Utils.printMessage(TAG, "EnrollRewards DATA:: " + data);
                closeLoading();
                try {
                    obj = new JSONObject(data);
                    if (obj.has("status")) {
                        String status = obj.getString("status");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            SharedPreferences preferences = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putBoolean(Constants.IS_REWARDS_ENABLED, true).apply();
                            Intent intent = new Intent(EnrollRewardsActivity.this, RewardsActivity.class);
                            startActivityForResult(intent, CHECK_REWARDS);
                        } else {
                            displayEmptyMessage();
                        }
                    } else {
                        displayEmptyMessage();
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    private void showLoading() {
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(textFace);
        descriptionText.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(EnrollRewardsActivity.this)) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        headerViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        headerViewLayout.removeView(loadingView);
    }

    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.animate().translationY(0).alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate().alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }

    private void displayEmptyMessage() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View errorView = inflater.inflate(R.layout.view_error_response, null);
        errorImage = (ImageView) errorView.findViewById(R.id.error_image);
        errorText = (TextView) errorView.findViewById(R.id.error_text);
        errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
        searchButton = (Button) errorView.findViewById(R.id.search_button);
        errorText.setTypeface(textFace);
        errorDescriptionText.setTypeface(textFace);
        searchButton.setTypeface(textFace);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loadErrorType(Constants.WRONG_ERROR_PAGE);
        errorView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        headerViewLayout.addView(errorView);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CHECK_REWARDS) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    finish();
                }
            }
        }
    }
}
