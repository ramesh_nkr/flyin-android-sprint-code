package com.flyin.bookings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyin.bookings.model.LoyaltyBasicInfoBean;
import com.flyin.bookings.model.LoyaltyBookingDataBean;
import com.flyin.bookings.model.LoyaltyBookingInfoBean;
import com.flyin.bookings.model.LoyaltyDataBean;
import com.flyin.bookings.model.LoyaltyTransactionInfoBean;
import com.flyin.bookings.services.AsyncTaskListener;
import com.flyin.bookings.services.HTTPAsync;
import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

@SuppressWarnings({"ConstantConditions", "deprecation"})
public class RewardsActivity extends AppCompatActivity implements AsyncTaskListener {
    private Typeface textFace;
    private static final String TAG = "RewardsActivity";
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private boolean isArabicLang;
    private Activity mActivity = null;
    private RelativeLayout errorView;
    private TextView loyaltyTypeTextView;
    private TextView rewarPointsTextView;
    private RelativeLayout loadingViewLayout = null;
    private boolean isInternetPresent = false;
    private static final int GET_REWARD_POINTS = 1;
    private LayoutInflater inflater = null;
    private TextView errorText = null;
    private TextView errorDescriptionText = null;
    private ImageView errorImage = null;
    private Button searchButton = null;
    private View loadingView = null;
    public static LoyaltyDataBean loyaltyDataBean = null;
    private ArrayList<String> hotelIdBookingsArray = null;
    private ArrayList<String> totalBookedIDArrList = null;
    private ArrayList<String> totalIdKeyList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards);
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontText = Constants.FONT_ROBOTO_REGULAR;
        isInternetPresent = Utils.isConnectingToInternet(RewardsActivity.this);
        isArabicLang = false;
        errorView = (RelativeLayout) findViewById(R.id.errorView);
        TextView errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        TextView loyaltyTypeTextViewHeader = (TextView) findViewById(R.id.loyalty_type_textview_header);
        TextView rewarPointsTextViewHeader = (TextView) findViewById(R.id.rewardpoints_textview_header);
        loyaltyTypeTextView = (TextView) findViewById(R.id.loyalty_type_textview);
        rewarPointsTextView = (TextView) findViewById(R.id.rewardpoints_textview);
        View leftViewStrip = (View) findViewById(R.id.left_view_strip);
        LinearLayout rightLayoutStrip = (LinearLayout) findViewById(R.id.right_layout_strip);
        loadingViewLayout = (RelativeLayout) findViewById(R.id.loading_view_layout);
        hotelIdBookingsArray = new ArrayList<>();
        totalBookedIDArrList = new ArrayList<>();
        totalIdKeyList = new ArrayList<>();
        leftViewStrip.setBackgroundResource(R.drawable.rewards_left_border);
        rightLayoutStrip.setBackgroundResource(R.drawable.rewards_right_border);
        if (Utils.isArabicLangSelected(RewardsActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontText = Constants.FONT_DROIDKUFI_REGULAR;
            isArabicLang = true;
            leftViewStrip.setBackgroundResource(R.drawable.rewards_right_border);
            rightLayoutStrip.setBackgroundResource(R.drawable.rewards_left_border);
            loyaltyTypeTextViewHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            loyaltyTypeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            rewarPointsTextViewHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
            rewarPointsTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textFace = Typeface.createFromAsset(getAssets(), fontText);
        errorMessageText.setTypeface(textFace);
        loyaltyTypeTextViewHeader.setTypeface(textFace);
        rewarPointsTextViewHeader.setTypeface(textFace);
        loyaltyTypeTextView.setTypeface(textFace);
        rewarPointsTextView.setTypeface(textFace);
        final String requestJSON = handleRewardPoints();
        if (isInternetPresent) {
            showLoading();
            HTTPAsync async = new HTTPAsync(RewardsActivity.this, RewardsActivity.this, Constants.LOYALTY_REWARD_POINTS, "",
                    requestJSON, GET_REWARD_POINTS, HTTPAsync.METHOD_POST);
            async.execute();
        } else {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = Utils.isConnectingToInternet(RewardsActivity.this);
                    if (isInternetPresent) {
                        loadingViewLayout.removeView(errorView);
                        showLoading();
                        HTTPAsync async = new HTTPAsync(RewardsActivity.this, RewardsActivity.this, Constants.LOYALTY_REWARD_POINTS, "",
                                requestJSON, GET_REWARD_POINTS, HTTPAsync.METHOD_POST);
                        async.execute();
                    }
                }
            });
            loadErrorType(Constants.NETWORK_ERROR);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        }
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_flight, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_flyin_rewards);
        mTitleText.setTypeface(tf);
        backText.setTypeface(textFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(RewardsActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = getIntent();
                setResult(RESULT_OK, it);
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
//        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
//        String memberType = null;
//        if (!pref.getString(Constants.LOYALTY_TOTALPOINTS, "").isEmpty()) {
//            if (pref.getString(Constants.LOYALTY_TIER, "").equalsIgnoreCase("S")) {
//                memberType = "Silver";
//            } else if (pref.getString(Constants.LOYALTY_TIER, "").equalsIgnoreCase("G")) {
//                memberType = "Gold";
//            } else if (pref.getString(Constants.LOYALTY_TIER, "").equalsIgnoreCase("P")) {
//                memberType = "Platinum";
//            }
//            loyaltyTypeTextView.setText(memberType);
//            rewarPointsTextView.setText(pref.getString(Constants.LOYALTY_TOTALPOINTS, ""));
//        } else {
//
//        }
        //   handleEarnLoyaltyPoints();
    }

    private String handleEarnLoyaltyPoints() {
        JSONObject finalObj = new JSONObject();
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        String currentTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH).format(new Date());
        String timeStamp = currentDateFormat + "T" + currentTimeFormat;
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String groupType = null;
        if (pref.getBoolean(Constants.isFanClub, false)) {
            groupType = "ALHILAL";
        }
        try {
            JSONObject mainJSON = new JSONObject();
            JSONObject sourceObj = new JSONObject();
            JSONObject flightObj = new JSONObject();
            JSONObject routeObj = new JSONObject();
            sourceObj.put("apikey", Constants.MERCHANDISE_API_KEY);
            sourceObj.put("userid", pref.getString(Constants.MEMBER_EMAIL, ""));
            sourceObj.put("bookingCode", "B2C193299");
            sourceObj.put("product", "flight");
            sourceObj.put("currency", "Sar");
            sourceObj.put("basefareAmount", "1000");
            sourceObj.put("totalAmount", "1000");
            sourceObj.put("companyId", "170");
            flightObj.put("gds", "");
            flightObj.put("airline", "QR");
            flightObj.put("nop", "3");
            flightObj.put("cartype", "cellur");
            routeObj.put("destinationSector", "DXB");
            routeObj.put("destinationCountry", "DUB");
            routeObj.put("sourceCountry", "SDF");
            routeObj.put("sourceSector", "PU");
            flightObj.put("route", routeObj);
            sourceObj.put("flight", flightObj);
            sourceObj.put("bookingDate", "2016-12-30T00:00:00.000Z");
            sourceObj.put("travelPeriodFrom", "2016-12-30T00:00:00.000Z");
            sourceObj.put("travelPeriodTo", "2016-12-30T00:00:00.000Z");
            sourceObj.put("groupType", "ALHILA");
            sourceObj.put("onHold", "0");
            Utils.printMessage("handleEarnLoyaltyPoints", sourceObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalObj.toString();
    }

    @Override
    public void onTaskComplete(String data, int serviceType) {
        Utils.printMessage(TAG, "Rewards Data :: " + data);
        JSONObject obj = null;
        if (data.equalsIgnoreCase("null") || data.equalsIgnoreCase("")) { //|| data.equalsIgnoreCase("{}")
            closeLoading();
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View errorView = inflater.inflate(R.layout.view_error_response, null);
            errorImage = (ImageView) errorView.findViewById(R.id.error_image);
            errorText = (TextView) errorView.findViewById(R.id.error_text);
            errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
            searchButton = (Button) errorView.findViewById(R.id.search_button);
            errorText.setTypeface(textFace);
            errorDescriptionText.setTypeface(textFace);
            searchButton.setTypeface(textFace);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = getIntent();
                    setResult(RESULT_OK, it);
                    finish();
                }
            });
            loadErrorType(Constants.WRONG_ERROR_PAGE);
            errorView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            loadingViewLayout.addView(errorView);
        } else {
            if (serviceType == GET_REWARD_POINTS) {
                try {
                    obj = new JSONObject(data);
                    loyaltyDataBean = new LoyaltyDataBean();
                    if (obj.has("basicInfo")) {
                        LoyaltyBasicInfoBean loyaltyBasicInfo = new LoyaltyBasicInfoBean();
                        if (obj.getJSONObject("basicInfo").has("userid")) {
                            loyaltyBasicInfo.setUserid(obj.getJSONObject("basicInfo").getString("userid"));
                        }
                        if (obj.getJSONObject("basicInfo").has("availablePoints")) {
                            loyaltyBasicInfo.setAvailablePoints(obj.getJSONObject("basicInfo").getInt("availablePoints"));
                        }
                        if (obj.getJSONObject("basicInfo").has("pendingPoints")) {
                            loyaltyBasicInfo.setPendingPoints(obj.getJSONObject("basicInfo").getInt("pendingPoints"));
                        }
                        if (obj.getJSONObject("basicInfo").has("totalPoints")) {
                            loyaltyBasicInfo.setTotalPoints(obj.getJSONObject("basicInfo").getInt("totalPoints"));
                        }
                        if (obj.getJSONObject("basicInfo").has("tier")) {
                            loyaltyBasicInfo.setTier(obj.getJSONObject("basicInfo").getString("tier"));
                        }
                        if (obj.getJSONObject("basicInfo").has("memberSince")) {
                            loyaltyBasicInfo.setMemberSince(obj.getJSONObject("basicInfo").getString("memberSince"));
                        }
                        loyaltyDataBean.setLoyaltyBasicInfoBean(loyaltyBasicInfo);
                        String memberType = "";
                        if (loyaltyDataBean.getLoyaltyBasicInfoBean().getTier().equalsIgnoreCase("S")) {
                            memberType = getString(R.string.label_silver);
                        } else if (loyaltyDataBean.getLoyaltyBasicInfoBean().getTier().equalsIgnoreCase("G")) {
                            memberType = getString(R.string.label_gold);
                        } else if (loyaltyDataBean.getLoyaltyBasicInfoBean().getTier().equalsIgnoreCase("P")) {
                            memberType = getString(R.string.label_platinum);
                        } else if (loyaltyDataBean.getLoyaltyBasicInfoBean().getTier().equalsIgnoreCase("B")) {
                            memberType = getString(R.string.label_bronze);
                        }
                        loyaltyTypeTextView.setText(memberType);
                    }
                    if (obj.has("transactions")) {
                        JSONArray transactionsArr = obj.getJSONArray("transactions");
                        ArrayList<LoyaltyTransactionInfoBean> transactionInfoArrayList = new ArrayList<>();
                        for (int i = 0; i < transactionsArr.length(); i++) {
                            LoyaltyTransactionInfoBean loyaltyTransactionInfo = new LoyaltyTransactionInfoBean();
                            if (transactionsArr.getJSONObject(i).has("product")) {
                                loyaltyTransactionInfo.setProduct(transactionsArr.getJSONObject(i).getString("product"));
                            }
                            if (transactionsArr.getJSONObject(i).has("date")) {
                                loyaltyTransactionInfo.setDate(transactionsArr.getJSONObject(i).getString("date"));
                            }
                            if (transactionsArr.getJSONObject(i).has("transactionType")) {
                                loyaltyTransactionInfo.setTransactionType(transactionsArr.getJSONObject(i).getString("transactionType"));
                            }
                            if (transactionsArr.getJSONObject(i).has("points")) {
                                loyaltyTransactionInfo.setPoints(transactionsArr.getJSONObject(i).getInt("points"));
                            }
                            if (transactionsArr.getJSONObject(i).has("bookingCode")) {
                                loyaltyTransactionInfo.setBookingCode(transactionsArr.getJSONObject(i).getString("bookingCode"));
                            }
                            if (transactionsArr.getJSONObject(i).has("description")) {
                                loyaltyTransactionInfo.setDescription(Utils.checkStringVal(transactionsArr.getJSONObject(i).getString("description")));
                            }
                            transactionInfoArrayList.add(loyaltyTransactionInfo);
                        }
                        loyaltyDataBean.setTransactionInfoList(transactionInfoArrayList);
                    }
                    if (obj.has("bookingData")) {
                        hotelIdBookingsArray.clear();
                        totalBookedIDArrList.clear();
                        JSONObject bookingDataObj = obj.getJSONObject("bookingData");
                        Iterator bookingId = bookingDataObj.keys();
                        JSONArray jsonArray = new JSONArray();
                        while (bookingId.hasNext()) {
                            String key = (String) bookingId.next();
                            JSONObject jsonObj = new JSONObject();
                            jsonObj.put(key, bookingDataObj.get(key));
                            jsonArray.put(jsonObj);
                        }
                        ArrayList<LoyaltyBookingInfoBean> loyaltyBookingInfoBeanArrayList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject listObjMark = jsonArray.getJSONObject(i);
                            Iterator keys = listObjMark.keys();
                            String currentDynamicKey = "";
                            while (keys.hasNext()) {
                                currentDynamicKey = (String) keys.next();
                                totalIdKeyList.add(currentDynamicKey);
                            }
                        }
                        for (int j = 0; j < loyaltyDataBean.getTransactionInfoList().size(); j++) {
                            if (loyaltyDataBean.getTransactionInfoList().get(j).getProduct().equalsIgnoreCase("flight") ||
                                    loyaltyDataBean.getTransactionInfoList().get(j).getProduct().equalsIgnoreCase("hotel") ||
                                    loyaltyDataBean.getTransactionInfoList().get(j).getProduct().equalsIgnoreCase("other") ||
                                    loyaltyDataBean.getTransactionInfoList().get(j).getProduct().equalsIgnoreCase("fph")) {
                                LoyaltyBookingInfoBean loyaltyBookingInfoBean = new LoyaltyBookingInfoBean();
                                loyaltyBookingInfoBean.setBookingCode(loyaltyDataBean.getTransactionInfoList().get(j).getBookingCode());
                                loyaltyBookingInfoBean.setProduct(loyaltyDataBean.getTransactionInfoList().get(j).getProduct());
                                loyaltyBookingInfoBean.setDate(loyaltyDataBean.getTransactionInfoList().get(j).getDate());
                                loyaltyBookingInfoBean.setPoints(loyaltyDataBean.getTransactionInfoList().get(j).getPoints());
                                loyaltyBookingInfoBean.setTransactionType(loyaltyDataBean.getTransactionInfoList().get(j).getTransactionType());
                                loyaltyBookingInfoBean.setDepartureDate(Utils.dateFormatToLoyaltyDurationFormat(loyaltyDataBean.getTransactionInfoList().get(j).getDate()));
                                loyaltyBookingInfoBean.setDescription(loyaltyDataBean.getTransactionInfoList().get(j).getDescription());
                                if (loyaltyDataBean.getTransactionInfoList().get(j).getProduct().equalsIgnoreCase("other")) {
                                    loyaltyBookingInfoBean.setProductCode("3");
                                }
                                for (int k = 0; k < totalIdKeyList.size(); k++) {
                                    JSONObject listObjMark = jsonArray.getJSONObject(k);
                                    if (loyaltyDataBean.getTransactionInfoList().get(j).getBookingCode().equalsIgnoreCase(totalIdKeyList.get(k))) {
                                        JSONObject currentDynamicValue = listObjMark.getJSONObject(totalIdKeyList.get(k));
                                        LoyaltyBookingDataBean bookingDataBean = new LoyaltyBookingDataBean();
                                        if (currentDynamicValue.has("flight")) {
                                            bookingDataBean.setDepartureAirport(Utils.checkStringVal(currentDynamicValue.getJSONObject("flight").getString("departureAirport")));
                                            bookingDataBean.setDepartureDate(Utils.checkStringVal(currentDynamicValue.getJSONObject("flight").getString("departureDate")));
                                            bookingDataBean.setAirline(Utils.checkStringVal(currentDynamicValue.getJSONObject("flight").getString("airline")));
                                            if (currentDynamicValue.getJSONObject("flight").has("arrivalAirport")) {
                                                bookingDataBean.setArrivalAirport(Utils.checkStringVal(currentDynamicValue.getJSONObject("flight").getString("arrivalAirport")));
                                            }
                                            bookingDataBean.setArrivalDate(Utils.checkStringVal(currentDynamicValue.getJSONObject("flight").getString("arrivalDate")));
                                            bookingDataBean.setDepartureAirportName(Utils.decodeUnicode(currentDynamicValue.getJSONObject("flight").getString("departureAirportName")));
                                            if (currentDynamicValue.getJSONObject("flight").has("arrivalAirportName")) {
                                                bookingDataBean.setArrivalAirportName(Utils.decodeUnicode(currentDynamicValue.getJSONObject("flight").getString("arrivalAirportName")));
                                            }
                                            loyaltyBookingInfoBean.setProductCode("1");
                                        }
                                        if (currentDynamicValue.has("hotel")) {
                                            hotelIdBookingsArray.add(currentDynamicValue.getJSONObject("hotel").getString("uniqueId"));
                                            bookingDataBean.setUniqueId(Utils.checkStringVal(currentDynamicValue.getJSONObject("hotel").getString("uniqueId")));
                                            bookingDataBean.setCheckIn(Utils.checkStringVal(currentDynamicValue.getJSONObject("hotel").getString("checkIn")));
                                            bookingDataBean.setCheckOut(Utils.checkStringVal(currentDynamicValue.getJSONObject("hotel").getString("checkOut")));
                                            bookingDataBean.setHotelName(Utils.checkStringVal(currentDynamicValue.getJSONObject("hotel").getString("hotelName")));
                                            bookingDataBean.setImage(Utils.checkStringVal(currentDynamicValue.getJSONObject("hotel").getString("image")));
                                            bookingDataBean.setRating(Utils.checkStringVal(currentDynamicValue.getJSONObject("hotel").getString("rating")));
                                            bookingDataBean.setCityName(Utils.checkStringVal(currentDynamicValue.getJSONObject("hotel").getString("cityName")));
                                            bookingDataBean.setCountryName(Utils.checkStringVal(currentDynamicValue.getJSONObject("hotel").getString("countryName")));
                                            loyaltyBookingInfoBean.setProductCode("2");
                                        }
                                        loyaltyBookingInfoBean.setLoyaltyBookingDataBean(bookingDataBean);
                                    }
                                }
                                if (loyaltyDataBean.getTransactionInfoList().get(j).getProduct().equalsIgnoreCase("fph")) {
                                    loyaltyBookingInfoBean.setProductCode("4");
                                }
                                loyaltyBookingInfoBeanArrayList.add(loyaltyBookingInfoBean);
                            }
                        }
                        Collections.sort(loyaltyBookingInfoBeanArrayList, new Comparator<LoyaltyBookingInfoBean>() {
                            @Override
                            public int compare(LoyaltyBookingInfoBean e1, LoyaltyBookingInfoBean e2) {
                                Long id1 = e1.getDepartureDate();
                                Long id2 = e2.getDepartureDate();
                                return id2.compareTo(id1);
                            }
                        });
                        loyaltyDataBean.setLoyaltyBookingInfoArrList(loyaltyBookingInfoBeanArrayList);
                    }
                    closeLoading();
                    if (loyaltyDataBean.getLoyaltyBasicInfoBean() != null) {
                        rewarPointsTextView.setText(String.valueOf(loyaltyDataBean.getLoyaltyBasicInfoBean().getAvailablePoints()));
                    } else {
                        rewarPointsTextView.setText("0");
                    }
                    setUpRewardsInfo();
                } catch (Exception e) {
                    Utils.printMessage(TAG, "Err msg : " + e.getMessage());
                    closeLoading();
                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View errorView = inflater.inflate(R.layout.view_error_response, null);
                    errorImage = (ImageView) errorView.findViewById(R.id.error_image);
                    errorText = (TextView) errorView.findViewById(R.id.error_text);
                    errorDescriptionText = (TextView) errorView.findViewById(R.id.error_description_text);
                    searchButton = (Button) errorView.findViewById(R.id.search_button);
                    errorText.setTypeface(textFace);
                    errorDescriptionText.setTypeface(textFace);
                    searchButton.setTypeface(textFace);
                    searchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent it = getIntent();
                            setResult(RESULT_OK, it);
                            finish();
                        }
                    });
                    loadErrorType(Constants.WRONG_ERROR_PAGE);
                    errorView.setLayoutParams(new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    loadingViewLayout.addView(errorView);
                }
            }
        }
    }

    @Override
    public void onTaskCancelled(String data, int serviceType) {
        closeLoading();
    }

    /**
     * Adding View pager
     **/
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private SummaryRewardsFragment summaryRewardsFragment;
    private MyStatementFragment myStatementFragment;
    private RecentTransactionsFragment recentTransactionsFragment;
    private ClaimPointsFragment claimPointsFragment;

    /**
     * Set View pager to the Screen
     **/
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        summaryRewardsFragment = new SummaryRewardsFragment();
        myStatementFragment = new MyStatementFragment();
        recentTransactionsFragment = new RecentTransactionsFragment();
        claimPointsFragment = new ClaimPointsFragment();
        adapter.addFragment(summaryRewardsFragment, getString(R.string.label_rewards_summary));
        adapter.addFragment(myStatementFragment, getString(R.string.label_rewards_mystatements));
        adapter.addFragment(recentTransactionsFragment, getString(R.string.label_rewards_recent_trns));
        adapter.addFragment(claimPointsFragment, getString(R.string.label_rewards_claim_points));
        viewPager.setAdapter(adapter);
    }

    private void setupTabText() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText(getString(R.string.label_rewards_summary));
        tabOne.setTypeface(textFace);
        if (isArabicLang) {
            tabOne.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        tabOne.setTextColor(Utils.getColor(RewardsActivity.this, R.color.white_color));
        tabLayout.getTabAt(0).setCustomView(tabOne);
        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText(getString(R.string.label_rewards_mystatements));
        tabTwo.setTypeface(textFace);
        if (isArabicLang) {
            tabTwo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        tabTwo.setTextColor(Utils.getColor(RewardsActivity.this, R.color.rewards_textmember));
        tabLayout.getTabAt(1).setCustomView(tabTwo);
        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText(getString(R.string.label_rewards_recent_trns));
        tabThree.setTypeface(textFace);
        if (isArabicLang) {
            tabThree.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        tabThree.setTextColor(Utils.getColor(RewardsActivity.this, R.color.rewards_textmember));
        tabLayout.getTabAt(2).setCustomView(tabThree);
        TextView tabFour = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour.setText(getString(R.string.label_rewards_claim_points));
        tabFour.setTypeface(textFace);
        if (isArabicLang) {
            tabFour.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
        }
        tabFour.setTextColor(Utils.getColor(RewardsActivity.this, R.color.rewards_textmember));
        tabLayout.getTabAt(3).setCustomView(tabFour);
    }

    private String handleRewardPoints() {
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        String selectedLanguage = pref.getString(Constants.USER_SELECTED_LANGUAGE, "en_GB");
        String userLang = "";
        if (selectedLanguage.equalsIgnoreCase("en_GB")) {
            userLang = "en";
        } else {
            userLang = "ar";
        }
        JSONObject mainJSON = new JSONObject();
        try {
            mainJSON.put("apikey", Constants.MERCHANDISE_API_KEY);
            mainJSON.put("userid", pref.getString(Constants.USER_UNIQUE_ID, ""));
            mainJSON.put("reqType", "A");
            mainJSON.put("lng", userLang);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJSON.toString();
    }

    private void showLoading() {
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.activity_loading_screen, null);
        TextView loadingText = (TextView) loadingView.findViewById(R.id.loading_text);
        TextView descriptionText = (TextView) loadingView.findViewById(R.id.please_wait_text);
        loadingText.setText(R.string.label_wait_please_message);
        loadingText.setTypeface(textFace);
        descriptionText.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(RewardsActivity.this)) {
            loadingText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
        }
        ProgressBar progressBar = (ProgressBar) loadingView.findViewById(R.id.progressbar);
        progressBar.setMax(100);
        progressBar.setProgress(0);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        loadingViewLayout.addView(loadingView);
    }

    private void closeLoading() {
        loadingViewLayout.removeView(loadingView);
    }

    private void loadErrorType(int type) {
        switch (type) {
            case Constants.ERROR_PAGE:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_oops_you_are_lost));
                errorDescriptionText.setText(getString(R.string.label_oops_you_are_lost_message));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.WRONG_ERROR_PAGE:
                errorImage.setImageResource(R.drawable.error);
                errorText.setText(getString(R.string.label_something_went_wrong));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.NETWORK_ERROR:
                errorImage.setImageResource(R.drawable.network);
                errorText.setText(getString(R.string.label_network_error));
                errorDescriptionText.setText(getString(R.string.label_network_error_message));
                searchButton.setText(getString(R.string.label_network_button_text));
                break;
            case Constants.PAYMENT_FAILED:
                errorImage.setImageResource(R.drawable.payment);
                errorText.setText(getString(R.string.label_payment_failed_text));
                errorDescriptionText.setText(getString(R.string.label_something_went_wrong_text));
                searchButton.setText(getString(R.string.label_error_page_return_text));
                break;
            case Constants.RESULT_ERROR:
                errorImage.setImageResource(R.drawable.search_page);
                errorText.setText(getString(R.string.label_no_results_found));
                errorDescriptionText.setText(getString(R.string.label_no_results_found_message));
                searchButton.setText(getString(R.string.label_error_page_search_text));
                break;
            default:
                break;
        }
    }

    private void setUpRewardsInfo() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabText();
        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
                                               @Override
                                               public void onTabSelected(TabLayout.Tab tab) {
                                                   super.onTabSelected(tab);
                                                   if (tab.getPosition() == 0) {
                                                       tabLayout.getTabAt(0).setCustomView(null);
                                                       TextView tabOne = (TextView) LayoutInflater.from(RewardsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabOne.setText(getString(R.string.label_rewards_summary));
                                                       tabOne.setTypeface(textFace);
                                                       if (isArabicLang) {
                                                           tabOne.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabOne.setTextColor(Utils.getColor(RewardsActivity.this, R.color.white_color));
                                                       tabLayout.getTabAt(0).setCustomView(tabOne);
                                                   } else if (tab.getPosition() == 1) {
                                                       tabLayout.getTabAt(1).setCustomView(null);
                                                       TextView tabTwo = (TextView) LayoutInflater.from(RewardsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabTwo.setText(getString(R.string.label_rewards_mystatements));
                                                       tabTwo.setTypeface(textFace);
                                                       if (isArabicLang) {
                                                           tabTwo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabTwo.setTextColor(Utils.getColor(RewardsActivity.this, R.color.white_color));
                                                       tabLayout.getTabAt(1).setCustomView(tabTwo);
                                                   } else if (tab.getPosition() == 2) {
                                                       tabLayout.getTabAt(2).setCustomView(null);
                                                       TextView tabThree = (TextView) LayoutInflater.from(RewardsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabThree.setText(getString(R.string.label_rewards_recent_trns));
                                                       tabThree.setTypeface(textFace);
                                                       if (isArabicLang) {
                                                           tabThree.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabThree.setTextColor(Utils.getColor(RewardsActivity.this, R.color.white_color));
                                                       tabLayout.getTabAt(2).setCustomView(tabThree);
                                                   } else {
                                                       tabLayout.getTabAt(3).setCustomView(null);
                                                       TextView tabFour = (TextView) LayoutInflater.from(RewardsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabFour.setText(getString(R.string.label_rewards_claim_points));
                                                       tabFour.setTypeface(textFace);
                                                       if (isArabicLang) {
                                                           tabFour.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabFour.setTextColor(Utils.getColor(RewardsActivity.this, R.color.white_color));
                                                       tabLayout.getTabAt(3).setCustomView(tabFour);
                                                   }
                                               }

                                               @Override
                                               public void onTabUnselected(TabLayout.Tab tab) {
                                                   super.onTabUnselected(tab);
                                                   if (tab.getPosition() == 0) {
                                                       tabLayout.getTabAt(0).setCustomView(null);
                                                       TextView tabOne = (TextView) LayoutInflater.from(RewardsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabOne.setText(getString(R.string.label_rewards_summary));
                                                       tabOne.setTypeface(textFace);
                                                       if (isArabicLang) {
                                                           tabOne.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabOne.setTextColor(Utils.getColor(RewardsActivity.this, R.color.rewards_textmember));
                                                       tabLayout.getTabAt(0).setCustomView(tabOne);
                                                   } else if (tab.getPosition() == 1) {
                                                       tabLayout.getTabAt(1).setCustomView(null);
                                                       TextView tabTwo = (TextView) LayoutInflater.from(RewardsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabTwo.setText(getString(R.string.label_rewards_mystatements));
                                                       tabTwo.setTypeface(textFace);
                                                       if (isArabicLang) {
                                                           tabTwo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabTwo.setTextColor(Utils.getColor(RewardsActivity.this, R.color.rewards_textmember));
                                                       tabLayout.getTabAt(1).setCustomView(tabTwo);
                                                   } else if (tab.getPosition() == 2) {
                                                       tabLayout.getTabAt(2).setCustomView(null);
                                                       TextView tabThree = (TextView) LayoutInflater.from(RewardsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabThree.setText(getString(R.string.label_rewards_recent_trns));
                                                       tabThree.setTypeface(textFace);
                                                       if (isArabicLang) {
                                                           tabThree.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabThree.setTextColor(Utils.getColor(RewardsActivity.this, R.color.rewards_textmember));
                                                       tabLayout.getTabAt(2).setCustomView(tabThree);
                                                   } else {
                                                       tabLayout.getTabAt(3).setCustomView(null);
                                                       TextView tabFour = (TextView) LayoutInflater.from(RewardsActivity.this).inflate(R.layout.custom_tab, null);
                                                       tabFour.setText(getString(R.string.label_rewards_claim_points));
                                                       tabFour.setTypeface(textFace);
                                                       if (isArabicLang) {
                                                           tabFour.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_text_size));
                                                       }
                                                       tabFour.setTextColor(Utils.getColor(RewardsActivity.this, R.color.rewards_textmember));
                                                       tabLayout.getTabAt(3).setCustomView(tabFour);
                                                   }
                                               }

                                               @Override
                                               public void onTabReselected(TabLayout.Tab tab) {
                                                   super.onTabReselected(tab);
                                               }
                                           }
        );
    }

    @Override
    public void onBackPressed() {
        Intent it = getIntent();
        setResult(RESULT_OK, it);
        finish();
    }
}
