package com.flyin.bookings;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyin.bookings.util.Constants;
import com.flyin.bookings.util.Utils;

public class ContactusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        LinearLayout emailActivity = (LinearLayout) findViewById(R.id.contact_email);
        emailActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactusActivity.this, ContactEmailActivity.class);
                startActivity(intent);
            }
        });
        String fontPath = Constants.FONT_ROBOTO_MEDIUM;
        String fontTitle = Constants.FONT_ROBOTO_REGULAR;
        TextView contactFirstHeader = (TextView) findViewById(R.id.contact_first_header);
        TextView contactNumber = (TextView) findViewById(R.id.contact_number);
        TextView contactSecondHeader = (TextView) findViewById(R.id.contact_second_header);
        TextView emailContactText = (TextView) findViewById(R.id.email_contact_text);
        TextView saudiCallsText = (TextView) findViewById(R.id.contact_saudi_calls_text);
        TextView saudiCallsNumber = (TextView) findViewById(R.id.contact_saudi_calls_number);
        TextView saudiOutCallsText = (TextView) findViewById(R.id.contact_saudi_out_calls_text);
        TextView saudiOutCallsNumber = (TextView) findViewById(R.id.contact_saudi_out_calls_number);
        TextView hotelFirstText = (TextView) findViewById(R.id.contact_hotel_first_text);
        TextView hotelMail = (TextView) findViewById(R.id.contact_hotel_mail);
        TextView wholesaleText = (TextView) findViewById(R.id.contact_wholesale_text);
        TextView wholesaleMail = (TextView) findViewById(R.id.contact_wholesale_mail);
        if (Utils.isArabicLangSelected(ContactusActivity.this)) {
            fontPath = Constants.FONT_DROIDKUFI_REGULAR;
            fontTitle = Constants.FONT_DROIDKUFI_REGULAR;
            hotelMail.setGravity(Gravity.START);
            wholesaleMail.setGravity(Gravity.START);
            findViewById(R.id.contact_email_arrow).setRotation(180);
            contactFirstHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            contactNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_header));
            contactSecondHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.arabic_hotel_header_text));
            emailContactText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            saudiCallsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            saudiCallsNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            saudiOutCallsText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            saudiOutCallsNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            hotelFirstText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            hotelMail.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            wholesaleText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
            wholesaleMail.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.hotel_city_text_size));
        }
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface textFace = Typeface.createFromAsset(getAssets(), fontTitle);
        contactFirstHeader.setTypeface(tf);
        contactNumber.setTypeface(tf);
        contactSecondHeader.setTypeface(tf);
        emailContactText.setTypeface(tf);
        saudiCallsText.setTypeface(textFace);
        saudiCallsNumber.setTypeface(tf);
        saudiOutCallsText.setTypeface(textFace);
        saudiOutCallsNumber.setTypeface(tf);
        hotelFirstText.setTypeface(textFace);
        hotelMail.setTypeface(tf);
        wholesaleText.setTypeface(textFace);
        wholesaleMail.setTypeface(tf);
        contactNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:920025959"));
                startActivity(intent);
            }
        });
        saudiCallsNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:920025959"));
                startActivity(intent);
            }
        });
        saudiOutCallsNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+966112246333"));
                startActivity(intent);
            }
        });
        hotelMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "contact@flyin.com"));
                startActivity(intent);
            }
        });
        wholesaleMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "sales@flyin.com"));
                startActivity(intent);
            }
        });
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mCustomView.findViewById(R.id.home_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.menu_actionbar).setVisibility(View.VISIBLE);
        mCustomView.findViewById(R.id.trip_journey_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.filter_actionbar).setVisibility(View.GONE);
        mCustomView.findViewById(R.id.trip_summary_actionbar).setVisibility(View.GONE);
        TextView mTitleText = (TextView) mCustomView.findViewById(R.id.label_menu);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_home_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_label);
        Button doneButton = (Button) mCustomView.findViewById(R.id.done_button);
        mTitleText.setText(R.string.label_user_contact);
        mTitleText.setTypeface(tf);
        backText.setTypeface(textFace);
        doneButton.setVisibility(View.GONE);
        if (Utils.isArabicLangSelected(ContactusActivity.this)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, -7, 0, 0);
            backText.setLayoutParams(params);
        }
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        if (mActionBar != null) {
            mActionBar.setCustomView(mCustomView, layout);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowCustomEnabled(true);
        }
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    }
}
